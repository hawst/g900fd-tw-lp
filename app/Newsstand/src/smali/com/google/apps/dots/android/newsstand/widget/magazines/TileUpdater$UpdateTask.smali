.class Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "LazyImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private allocationCount:I

.field private allocationFailure:Z

.field private final displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

.field private finishedCovering:Z

.field private final scaledImageArea:Landroid/graphics/RectF;

.field private sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

.field private final sourceBitmaps:[Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

.field private tileConfig:Landroid/graphics/Bitmap$Config;

.field private tileSizeAfter:I

.field private tileSizeBefore:I


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/concurrent/Executor;J)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "priority"    # J

    .prologue
    .line 593
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .line 594
    invoke-direct {p0, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;J)V

    .line 576
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    .line 579
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, [Landroid/graphics/Bitmap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    .line 581
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->scaledImageArea:Landroid/graphics/RectF;

    .line 595
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->onPreExecute()V

    .line 596
    return-void
.end method

.method private getDrawnTileForRegion(Landroid/graphics/RectF;Z)Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    .locals 12
    .param p1, "region"    # Landroid/graphics/RectF;
    .param p2, "ignoreAllocationLimit"    # Z

    .prologue
    .line 622
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v7

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->findTile(Ljava/util/Map;Landroid/graphics/RectF;)Ljava/util/Map$Entry;
    invoke-static {v7, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$400(Ljava/util/Map;Landroid/graphics/RectF;)Ljava/util/Map$Entry;

    move-result-object v2

    .line 623
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    const/4 v5, 0x0

    .line 624
    .local v5, "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    if-eqz v2, :cond_2

    .line 625
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    check-cast v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    .line 626
    .restart local v5    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    .line 627
    .local v3, "oldState":Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    if-eq v3, v7, :cond_0

    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->RELEASE:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    if-ne v3, v7, :cond_1

    .line 628
    :cond_0
    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->STOLEN:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    invoke-interface {v2, v7}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .end local v3    # "oldState":Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;
    :cond_1
    :goto_0
    move-object v7, v5

    .line 666
    :goto_1
    return-object v7

    .line 630
    :cond_2
    if-nez p2, :cond_3

    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationCount:I

    const/16 v8, 0x8

    if-ge v7, v8, :cond_1

    .line 631
    :cond_3
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-direct {p0, v7, v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->getSourceBitmap(IZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 632
    .local v4, "sourceBitmap":Landroid/graphics/Bitmap;
    if-nez v4, :cond_4

    .line 633
    const/4 v7, 0x0

    goto :goto_1

    .line 637
    :cond_4
    const/4 v6, 0x0

    .line 639
    .local v6, "tileBitmap":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v8

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v9

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileConfig:Landroid/graphics/Bitmap$Config;

    const/4 v11, 0x1

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;->getPoolBitmap(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 644
    :goto_2
    if-nez v6, :cond_5

    .line 645
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationFailure:Z

    .line 646
    const/4 v7, 0x0

    goto :goto_1

    .line 640
    :catch_0
    move-exception v1

    .line 642
    .local v1, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v8, "tile alloc failed"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 649
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 650
    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    .end local v5    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    invoke-direct {v5, p1, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;-><init>(Landroid/graphics/RectF;Landroid/graphics/Bitmap;)V

    .line 651
    .restart local v5    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    .line 652
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->scaledImageArea:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->paint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v0, v4, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 654
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getDensity()F

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->setZDepth(F)V

    .line 662
    iget v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationCount:I

    .line 663
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v7

    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->NEW:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    invoke-interface {v7, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getSourceBitmap(IZ)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "sampleSizeIndex"    # I
    .param p2, "purgeable"    # Z

    .prologue
    .line 599
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v1, p1

    .line 600
    .local v0, "source":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 602
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sampleSizeForIndex(I)I
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$100(I)I

    move-result v2

    invoke-virtual {v1, v2, p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getBitmap(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 603
    if-eqz v0, :cond_1

    .line 604
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aput-object v0, v1, p1

    .line 614
    :cond_0
    :goto_0
    return-object v0

    .line 607
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "Couldn\'t retrieve page image bitmap."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 608
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationFailure:Z
    :try_end_0
    .catch Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView$WouldCauseJankException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 610
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private releaseTiles(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 962
    .local p1, "tiles":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    .line 963
    .local v0, "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->bitmapPool:Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/bitmap/CachingBitmapPool;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->release(Lcom/google/apps/dots/android/newsstand/bitmap/BitmapPool;)V

    goto :goto_0

    .line 965
    .end local v0    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->clear()V

    .line 966
    return-void
.end method

.method private tileRect(Landroid/graphics/RectF;FLandroid/graphics/Region;Z)Z
    .locals 22
    .param p1, "viewingRect"    # Landroid/graphics/RectF;
    .param p2, "targetScale"    # F
    .param p3, "coveredRegion"    # Landroid/graphics/Region;
    .param p4, "ignoreAllocationLimit"    # Z

    .prologue
    .line 685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->contentArea:Landroid/graphics/RectF;

    .line 686
    .local v4, "contentArea":Landroid/graphics/RectF;
    invoke-static/range {p1 .. p1}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->isEmpty(Landroid/graphics/RectF;)Z

    move-result v19

    if-nez v19, :cond_0

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->isEmpty(Landroid/graphics/RectF;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 687
    :cond_0
    const/4 v5, 0x1

    .line 751
    :goto_0
    return v5

    .line 699
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v19, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F

    move-result v19

    cmpl-float v19, p2, v19

    if-ltz v19, :cond_3

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v19, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v20, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static/range {v20 .. v20}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F

    move-result v20

    div-float v16, v19, v20

    .line 705
    .local v16, "width":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v19, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v20, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static/range {v20 .. v20}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F

    move-result v20

    div-float v7, v19, v20

    .line 707
    .local v7, "height":F
    const/high16 v19, 0x40000000    # 2.0f

    sub-float v11, v16, v19

    .line 708
    .local v11, "offsetX":F
    const/high16 v19, 0x40000000    # 2.0f

    sub-float v12, v7, v19

    .line 721
    .local v12, "offsetY":F
    :goto_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRectF()Landroid/graphics/RectF;

    move-result-object v15

    .line 722
    .local v15, "tempTileRect":Landroid/graphics/RectF;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRectF()Landroid/graphics/RectF;

    move-result-object v14

    .line 723
    .local v14, "tempRectF":Landroid/graphics/RectF;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRect()Landroid/graphics/Rect;

    move-result-object v13

    .line 725
    .local v13, "tempRect":Landroid/graphics/Rect;
    const/4 v5, 0x1

    .line 726
    .local v5, "finished":Z
    const/high16 v6, 0x3f000000    # 0.5f

    .line 728
    .local v6, "fudge":F
    const/16 v18, 0x0

    .local v18, "y":F
    :goto_2
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v19

    const/high16 v20, 0x3f000000    # 0.5f

    sub-float v19, v19, v20

    cmpg-float v19, v18, v19

    if-gez v19, :cond_2

    .line 729
    const/16 v17, 0x0

    .local v17, "x":F
    :goto_3
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v19

    const/high16 v20, 0x3f000000    # 0.5f

    sub-float v19, v19, v20

    cmpg-float v19, v17, v19

    if-gez v19, :cond_6

    .line 730
    add-float v19, v17, v16

    add-float v20, v18, v7

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 731
    iget v0, v4, Landroid/graphics/RectF;->left:F

    move/from16 v19, v0

    iget v0, v4, Landroid/graphics/RectF;->top:F

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 733
    invoke-virtual {v14, v15}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 734
    move-object/from16 v0, p1

    invoke-static {v14, v0}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->intersectWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p3

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->regionContainsRect(Landroid/graphics/Region;Landroid/graphics/RectF;)Z
    invoke-static {v0, v1, v14}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Landroid/graphics/Region;Landroid/graphics/RectF;)Z

    move-result v19

    if-nez v19, :cond_5

    .line 736
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v15, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->getDrawnTileForRegion(Landroid/graphics/RectF;Z)Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    move-result-object v19

    if-nez v19, :cond_4

    .line 737
    const/4 v5, 0x0

    .line 747
    .end local v17    # "x":F
    :cond_2
    invoke-static {v15}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/RectF;)V

    .line 748
    invoke-static {v14}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/RectF;)V

    .line 749
    invoke-static {v13}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 710
    .end local v5    # "finished":Z
    .end local v6    # "fudge":F
    .end local v7    # "height":F
    .end local v11    # "offsetX":F
    .end local v12    # "offsetY":F
    .end local v13    # "tempRect":Landroid/graphics/Rect;
    .end local v14    # "tempRectF":Landroid/graphics/RectF;
    .end local v15    # "tempTileRect":Landroid/graphics/RectF;
    .end local v16    # "width":F
    .end local v18    # "y":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    move-object/from16 v19, v0

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileSize:I
    invoke-static/range {v19 .. v19}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v8, v19, p2

    .line 711
    .local v8, "maxSize":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v19

    div-float v19, v19, v8

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v9, v0

    .line 712
    .local v9, "ni":I
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v19

    div-float v19, v19, v8

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v10, v0

    .line 715
    .local v10, "nj":I
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v19

    int-to-float v0, v9

    move/from16 v20, v0

    div-float v11, v19, v20

    .line 716
    .restart local v11    # "offsetX":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v19

    int-to-float v0, v10

    move/from16 v20, v0

    div-float v12, v19, v20

    .line 717
    .restart local v12    # "offsetY":F
    const/high16 v19, 0x40000000    # 2.0f

    add-float v16, v11, v19

    .line 718
    .restart local v16    # "width":F
    const/high16 v19, 0x40000000    # 2.0f

    add-float v7, v12, v19

    .restart local v7    # "height":F
    goto/16 :goto_1

    .line 740
    .end local v8    # "maxSize":F
    .end local v9    # "ni":I
    .end local v10    # "nj":I
    .restart local v5    # "finished":Z
    .restart local v6    # "fudge":F
    .restart local v13    # "tempRect":Landroid/graphics/Rect;
    .restart local v14    # "tempRectF":Landroid/graphics/RectF;
    .restart local v15    # "tempTileRect":Landroid/graphics/RectF;
    .restart local v17    # "x":F
    .restart local v18    # "y":F
    :cond_4
    invoke-virtual {v15, v13}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 741
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    .line 729
    :cond_5
    add-float v17, v17, v11

    goto/16 :goto_3

    .line 728
    :cond_6
    add-float v18, v18, v12

    goto/16 :goto_2
.end method

.method private tileWithSource(Landroid/graphics/Region;I)Z
    .locals 7
    .param p1, "coveredRegion"    # Landroid/graphics/Region;
    .param p2, "sampleSizeIndex"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 758
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v6, v6, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->contentArea:Landroid/graphics/RectF;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->regionContainsRect(Landroid/graphics/Region;Landroid/graphics/RectF;)Z
    invoke-static {v5, p1, v6}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Landroid/graphics/Region;Landroid/graphics/RectF;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 779
    :goto_0
    return v3

    .line 762
    :cond_0
    invoke-direct {p0, p2, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->getSourceBitmap(IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 763
    .local v0, "sourceBitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    move v3, v4

    .line 764
    goto :goto_0

    .line 766
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v0, v4, p2

    .line 767
    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$1;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->scaledImageArea:Landroid/graphics/RectF;

    invoke-direct {v2, p0, v4, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;Landroid/graphics/RectF;Landroid/graphics/Bitmap;)V

    .line 773
    .local v2, "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v4

    sget-object v5, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->NEW:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 775
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 776
    .local v1, "tempRect":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v4, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 777
    invoke-virtual {p1, v1}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    .line 778
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/Rect;)V

    goto :goto_0
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 794
    const-string v0, "LIV-update"

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->beginSection(Ljava/lang/String;)I

    .line 796
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->doInBackgroundInternal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 799
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$2;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 806
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/common/util/concurrent/Futures;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 798
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/instrumentation/TraceCompat;->endSection()V

    .line 799
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackgroundInternal()V
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/high16 v12, -0x41000000    # -0.5f

    const/4 v10, 0x0

    .line 811
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getBitmapInfo()Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    move-result-object v8

    iput-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .line 812
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    move-result-object v8

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    invoke-virtual {v8, v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->copyAndClean(Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;)V

    .line 813
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-boolean v8, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->destroyed:Z

    if-eqz v8, :cond_1

    .line 903
    :cond_0
    return-void

    .line 818
    :cond_1
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->globalTransform:Landroid/graphics/Matrix;

    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/util/MatrixUtil;->getMatrixScale(Landroid/graphics/Matrix;)F

    move-result v11

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static {v8, v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$802(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;F)F

    .line 821
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->scaledImageArea:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v11, v11, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->contentArea:Landroid/graphics/RectF;

    invoke-virtual {v8, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 822
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->scaledImageArea:Landroid/graphics/RectF;

    invoke-virtual {v8, v12, v12}, Landroid/graphics/RectF;->inset(FF)V

    .line 824
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileConfig:Landroid/graphics/Bitmap$Config;

    .line 826
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget-boolean v8, v8, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .line 827
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->memClass:I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v8

    const/16 v11, 0x40

    if-ge v8, v11, :cond_2

    .line 828
    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileConfig:Landroid/graphics/Bitmap$Config;

    .line 834
    :cond_2
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Region;->setEmpty()V

    .line 835
    iput-boolean v9, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    .line 837
    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->drawnSizePx:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 839
    .local v0, "drawnSize":I
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-object v6, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->visibleArea:Landroid/graphics/RectF;

    .line 841
    .local v6, "visibleArea":Landroid/graphics/RectF;
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v11

    invoke-static {v8, v11}, Ljava/lang/Math;->min(FF)F

    move-result v8

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F

    move-result v11

    mul-float/2addr v8, v11

    const/high16 v11, 0x40a00000    # 5.0f

    cmpl-float v8, v8, v11

    if-lez v8, :cond_8

    .line 842
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-boolean v8, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->visible:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I

    move-result-object v8

    aget v4, v8, v10

    .line 843
    .local v4, "threshold":I
    :goto_0
    if-ge v0, v4, :cond_8

    .line 844
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->isTooLargeForTexture()Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    .line 845
    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->getNumPixels()I

    move-result v8

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileScreenResThresholdPx:I
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1600(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v11

    if-le v8, v11, :cond_3

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmapInfo:Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;

    iget-boolean v8, v8, Lcom/google/apps/dots/android/newsstand/bitmap/ImageInfo;->hasAlpha:Z

    if-eqz v8, :cond_6

    .line 850
    :cond_3
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;

    move-result-object v11

    invoke-direct {p0, v11, v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileWithSource(Landroid/graphics/Region;I)Z

    move-result v11

    and-int/2addr v8, v11

    iput-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    .line 856
    :goto_1
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->getRectF()Landroid/graphics/RectF;

    move-result-object v7

    .line 857
    .local v7, "visibleTileRect":Landroid/graphics/RectF;
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_4
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 858
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    sget-object v12, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    if-ne v8, v12, :cond_4

    .line 859
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getRegion()Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 860
    invoke-static {v7, v6}, Lcom/google/apps/dots/android/newsstand/util/RectUtil;->intersectWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 861
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;
    invoke-static {v12}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;

    move-result-object v12

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->regionContainsRect(Landroid/graphics/Region;Landroid/graphics/RectF;)Z
    invoke-static {v8, v12, v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$900(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Landroid/graphics/Region;Landroid/graphics/RectF;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 862
    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->RELEASE:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    invoke-interface {v1, v8}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 842
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    .end local v4    # "threshold":I
    .end local v7    # "visibleTileRect":Landroid/graphics/RectF;
    :cond_5
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->invisibleThresholdsPx:[I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I

    move-result-object v8

    aget v4, v8, v10

    goto :goto_0

    .line 852
    .restart local v4    # "threshold":I
    :cond_6
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->scale:F
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)F

    move-result v11

    iget-object v12, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;
    invoke-static {v12}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;

    move-result-object v12

    invoke-direct {p0, v6, v11, v12, v10}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileRect(Landroid/graphics/RectF;FLandroid/graphics/Region;Z)Z

    move-result v11

    and-int/2addr v8, v11

    iput-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    goto :goto_1

    .line 866
    .restart local v7    # "visibleTileRect":Landroid/graphics/RectF;
    :cond_7
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TempPool;->release(Landroid/graphics/RectF;)V

    .line 871
    .end local v4    # "threshold":I
    .end local v7    # "visibleTileRect":Landroid/graphics/RectF;
    :cond_8
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget v8, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->swipeDistance:F

    const v11, 0x3f8ccccd    # 1.1f

    cmpg-float v8, v8, v11

    if-gez v8, :cond_9

    .line 872
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_3
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I

    move-result-object v8

    array-length v8, v8

    if-ge v2, v8, :cond_9

    .line 873
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->displayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    iget-boolean v8, v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->visible:Z

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .line 874
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->visibleThresholdsPx:[I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1400(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I

    move-result-object v8

    aget v4, v8, v2

    .line 875
    .restart local v4    # "threshold":I
    :goto_4
    if-ge v0, v4, :cond_c

    .line 876
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    iget-object v11, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->coveredRegion:Landroid/graphics/Region;
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Landroid/graphics/Region;

    move-result-object v11

    invoke-direct {p0, v11, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileWithSource(Landroid/graphics/Region;I)Z

    move-result v11

    and-int/2addr v8, v11

    iput-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    .line 878
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    if-eqz v8, :cond_c

    .line 887
    .end local v2    # "i":I
    .end local v4    # "threshold":I
    :cond_9
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_a
    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 888
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    sget-object v12, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    if-ne v8, v12, :cond_a

    .line 889
    iget-boolean v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    if-nez v8, :cond_d

    move v3, v9

    .line 890
    .local v3, "steal":Z
    :goto_6
    if-eqz v3, :cond_e

    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->STOLEN:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    :goto_7
    invoke-interface {v1, v8}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    .line 892
    if-eqz v3, :cond_a

    .line 893
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    invoke-virtual {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 894
    .local v5, "tileBitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    array-length v8, v8

    if-ge v2, v8, :cond_a

    .line 895
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aget-object v8, v8, v2

    if-ne v5, v8, :cond_f

    .line 896
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v8

    aput-object v5, v8, v2

    goto :goto_5

    .line 874
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    .end local v3    # "steal":Z
    .end local v5    # "tileBitmap":Landroid/graphics/Bitmap;
    :cond_b
    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->invisibleThresholdsPx:[I
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1500(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[I

    move-result-object v8

    aget v4, v8, v2

    goto :goto_4

    .line 872
    .restart local v4    # "threshold":I
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .end local v2    # "i":I
    .end local v4    # "threshold":I
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;>;"
    :cond_d
    move v3, v10

    .line 889
    goto :goto_6

    .line 890
    .restart local v3    # "steal":Z
    :cond_e
    sget-object v8, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->RELEASE:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    goto :goto_7

    .line 894
    .restart local v2    # "i":I
    .restart local v5    # "tileBitmap":Landroid/graphics/Bitmap;
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_8
.end method

.method protected onPostExecute()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 906
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # setter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->isUpdatingTiles:Z
    invoke-static {v3, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1702(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Z)Z

    .line 908
    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->allocationFailure:Z

    if-eqz v3, :cond_0

    .line 909
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # operator++ for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1808(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    .line 912
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 913
    .local v2, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_5

    .line 915
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 916
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v1

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v3

    aget-object v3, v3, v1

    if-nez v3, :cond_1

    .line 917
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sampleSizeForIndex(I)I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$100(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->releaseBitmap(Landroid/graphics/Bitmap;I)V

    .line 915
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 920
    :cond_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    invoke-static {v3, v6}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 923
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->RELEASE:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->extractTilesWithState(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V
    invoke-static {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1900(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V

    .line 924
    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->releaseTiles(Ljava/util/Collection;)V

    .line 926
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 927
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->NEW:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->extractTilesWithState(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V
    invoke-static {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1900(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V

    .line 928
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->STOLEN:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->extractTilesWithState(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V
    invoke-static {v3, v4, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1900(Ljava/util/Map;Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;Ljava/util/Collection;)V

    .line 929
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->computeTileSizes(Ljava/util/List;)I
    invoke-static {v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/List;)I

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileSizeAfter:I

    .line 930
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->setTiles(Ljava/util/Collection;)V

    .line 933
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 936
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->currentDisplayParams:Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1100(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/DisplayParams;->dirty:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->finishedCovering:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    .line 937
    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateFailuresSinceLastRequest:I
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1800(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)I

    move-result v3

    const/16 v4, 0x14

    if-ge v3, v4, :cond_4

    .line 938
    :cond_3
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->updateTilesRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$2000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/android/libraries/bind/async/DelayedRunnable;

    move-result-object v3

    const-wide/16 v4, 0x14

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 957
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 958
    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->drawnSizePx:Ljava/util/concurrent/atomic/AtomicInteger;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileSizeAfter:I

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileSizeBefore:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 959
    return-void

    .line 942
    .end local v1    # "i":I
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 943
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v3, v1

    .line 944
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_6

    .line 945
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v3

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sampleSizeForIndex(I)I
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$100(I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->releaseBitmap(Landroid/graphics/Bitmap;I)V

    .line 942
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 948
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->sourceBitmaps:[Landroid/graphics/Bitmap;

    invoke-static {v3, v6}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 949
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3, v6}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 952
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->releaseTiles(Ljava/util/Collection;)V

    .line 953
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 954
    iput v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileSizeAfter:I

    .line 955
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v3

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->of()Lcom/google/common/collect/ImmutableSet;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->setTiles(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method public onPreExecute()V
    .locals 4

    .prologue
    .line 783
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 784
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getTiles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;

    .line 785
    .local v0, "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->tileStates:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$300(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Ljava/util/Map;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$TileState;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 788
    .end local v0    # "tile":Lcom/google/apps/dots/android/newsstand/widget/magazines/Tile;
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->sourceBitmapsInTiles:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)[Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 789
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->lazyImageView:Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$200(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;)Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/LazyImageView;->getTiles()Ljava/util/List;

    move-result-object v2

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->computeTileSizes(Ljava/util/List;)I
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;->access$1000(Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater;Ljava/util/List;)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/TileUpdater$UpdateTask;->tileSizeBefore:I

    .line 790
    return-void
.end method
