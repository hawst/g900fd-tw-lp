.class Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;
.super Ljava/lang/Object;
.source "LocalLogHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;

.field final synthetic val$logLevel:I

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->this$0:Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;

    iput p2, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$logLevel:I

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$message:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$tag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 75
    new-instance v1, Ljava/util/logging/LogRecord;

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$logLevel:I

    # invokes: Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->javaLevel(I)Ljava/util/logging/Level;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->access$000(I)Ljava/util/logging/Level;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$message:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Ljava/util/logging/LogRecord;-><init>(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 76
    .local v1, "logRecord":Ljava/util/logging/LogRecord;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler$1;->val$tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/logging/LogRecord;->setSourceClassName(Ljava/lang/String;)V

    .line 77
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/logging/LogRecord;->setMillis(J)V

    .line 78
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/logging/LogRecord;->setThrown(Ljava/lang/Throwable;)V

    .line 79
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v0

    .line 80
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->isEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    # invokes: Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->getLogger(Landroid/content/Context;)Ljava/util/logging/Logger;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;->access$100(Landroid/content/Context;)Ljava/util/logging/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/LogRecord;)V

    .line 83
    :cond_0
    return-void
.end method
