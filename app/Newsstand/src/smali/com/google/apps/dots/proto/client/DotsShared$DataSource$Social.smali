.class public final Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$DataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Social"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;


# instance fields
.field private account_:Ljava/lang/String;

.field private bitField0_:I

.field private name_:Ljava/lang/String;

.field private searchPhrase_:Ljava/lang/String;

.field private socialService_:I

.field private stream_:Ljava/lang/String;

.field private tokenType_:I

.field private token_:Ljava/lang/String;

.field private uri_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8001
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8002
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 8019
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    .line 8038
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    .line 8060
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    .line 8082
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    .line 8104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    .line 8126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    .line 8148
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    .line 8170
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    .line 8002
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    .locals 3

    .prologue
    .line 8206
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8210
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    return-object v0

    .line 8207
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    :catch_0
    move-exception v1

    .line 8208
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7998
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->clone()Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 8215
    if-ne p1, p0, :cond_1

    .line 8224
    :cond_0
    :goto_0
    return v1

    .line 8216
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 8217
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    .line 8218
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8219
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8220
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8221
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8222
    :goto_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8223
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 8224
    :goto_6
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 8218
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    .line 8219
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    .line 8220
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    .line 8221
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    .line 8222
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    .line 8223
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    .line 8224
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_6
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 8272
    const/4 v0, 0x0

    .line 8273
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8274
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    .line 8275
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8277
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 8278
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    .line 8279
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8281
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 8282
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    .line 8283
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8285
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 8286
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    .line 8287
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8289
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 8290
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    .line 8291
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8293
    :cond_4
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 8294
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    .line 8295
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8297
    :cond_5
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 8298
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    .line 8299
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8301
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 8302
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    .line 8303
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8305
    :cond_7
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->cachedSize:I

    .line 8306
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 8229
    const/16 v0, 0x11

    .line 8230
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 8231
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    add-int v0, v1, v3

    .line 8232
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 8233
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 8234
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 8235
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 8236
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 8237
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    if-nez v3, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 8238
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    add-int v0, v1, v2

    .line 8239
    return v0

    .line 8232
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 8233
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 8234
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 8235
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 8236
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 8237
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8314
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 8315
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 8319
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8320
    :sswitch_0
    return-object p0

    .line 8325
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 8326
    .local v1, "temp":I
    if-eqz v1, :cond_1

    if-eq v1, v4, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 8330
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    .line 8331
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8333
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    goto :goto_0

    .line 8338
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    .line 8339
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8343
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    .line 8344
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8348
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    .line 8349
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8353
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    .line 8354
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8358
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    .line 8359
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8363
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    .line 8364
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto :goto_0

    .line 8368
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 8369
    .restart local v1    # "temp":I
    if-eqz v1, :cond_3

    if-ne v1, v4, :cond_4

    .line 8371
    :cond_3
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    .line 8372
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    goto/16 :goto_0

    .line 8374
    :cond_4
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    goto/16 :goto_0

    .line 8315
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7998
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8244
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8245
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->socialService_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 8247
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 8248
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->searchPhrase_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8250
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 8251
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->account_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8253
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 8254
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->name_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8256
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 8257
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->uri_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8259
    :cond_4
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 8260
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->token_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8262
    :cond_5
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 8263
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->stream_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 8265
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    .line 8266
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$DataSource$Social;->tokenType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 8268
    :cond_7
    return-void
.end method
