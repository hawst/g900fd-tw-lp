.class public interface abstract Lcom/google/apps/dots/android/newsstand/provider/blob/BlobFile;
.super Ljava/lang/Object;
.source "BlobFile.java"


# virtual methods
.method public abstract exists()Z
.end method

.method public abstract getMetadata()Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract key()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
.end method

.method public abstract makeAssetFileDescriptor()Landroid/content/res/AssetFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract makeDiskBlob()Lcom/google/apps/dots/android/newsstand/diskcache/DiskBlob;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract makeInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract pin(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract setMetadata(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract touch()Z
.end method

.method public abstract writeStream(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
