.class public Lcom/google/apps/dots/android/newsstand/util/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method public static clamp(FFF)F
    .locals 1
    .param p0, "x"    # F
    .param p1, "lower"    # F
    .param p2, "upper"    # F

    .prologue
    .line 16
    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public static clamp(III)I
    .locals 1
    .param p0, "x"    # I
    .param p1, "lower"    # I
    .param p2, "upper"    # I

    .prologue
    .line 8
    invoke-static {p0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
