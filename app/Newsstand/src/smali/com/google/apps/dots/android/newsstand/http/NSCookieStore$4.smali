.class Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;
.super Ljava/lang/Object;
.source "NSCookieStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->checkAdId(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 253
    :try_start_0
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "checkAdId"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->val$context:Landroid/content/Context;

    .line 255
    invoke-static {v7}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v2

    .line 256
    .local v2, "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$800(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "ad_id"

    const-string v9, ""

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 257
    .local v4, "prevValue":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    .line 258
    .local v3, "noPrevValue":Z
    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    move v1, v5

    .line 259
    .local v1, "idChanged":Z
    :goto_0
    if-nez v3, :cond_0

    if-eqz v1, :cond_1

    .line 260
    :cond_0
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "detected adID change: %s -> %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->prefs:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$800(Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "ad_id"

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 263
    :cond_1
    if-nez v3, :cond_2

    if-eqz v1, :cond_2

    .line 264
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "clearing cookies due to Ad ID change"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore$4;->this$0:Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->removeAll()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_3

    .line 276
    .end local v1    # "idChanged":Z
    .end local v2    # "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .end local v3    # "noPrevValue":Z
    .end local v4    # "prevValue":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .restart local v2    # "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .restart local v3    # "noPrevValue":Z
    .restart local v4    # "prevValue":Ljava/lang/String;
    :cond_3
    move v1, v6

    .line 258
    goto :goto_0

    .line 267
    .end local v2    # "info":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    .end local v3    # "noPrevValue":Z
    .end local v4    # "prevValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "Error checking ad ID: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 269
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 270
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "Error checking ad ID: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 271
    .end local v0    # "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    :catch_2
    move-exception v0

    .line 272
    .local v0, "e":Ljava/io/IOException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "Error checking ad ID: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 273
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 274
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    # getter for: Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/http/NSCookieStore;->access$700()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v7

    const-string v8, "Error checking ad ID: %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v7, v8, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
