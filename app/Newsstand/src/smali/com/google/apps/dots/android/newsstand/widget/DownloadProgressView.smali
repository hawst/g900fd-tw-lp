.class public Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;
.super Lcom/google/android/play/widget/DownloadStatusView;
.source "DownloadProgressView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;
.implements Lcom/google/android/libraries/bind/data/DataView;


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final MIN_DOWNLOAD_FRACTION:F = 0.05f


# instance fields
.field public edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private failed:Z

.field private helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

.field private hideWhenRemoved:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const v2, 0x3d4ccccd    # 0.05f

    iput v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->mMinInProgressDisplayFraction:F

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 52
    .local v1, "resources":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->mArcPaintOnline:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->download_arc_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    new-instance v2, Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/bind/data/DataViewHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 55
    sget-object v2, Lcom/google/android/apps/newsstanddev/R$styleable;->DownloadProgressView:[I

    .line 56
    invoke-virtual {p1, p2, v2, p3, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v2, Lcom/google/android/apps/newsstanddev/R$styleable;->DownloadProgressView_hideWhenRemoved:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->hideWhenRemoved:Z

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private setFailed(Z)V
    .locals 1
    .param p1, "failed"    # Z

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->failed:Z

    if-eq v0, p1, :cond_0

    .line 168
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->failed:Z

    .line 171
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setOnline(Z)V

    .line 173
    :cond_0
    return-void

    .line 171
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateOnStateChanged()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    .line 129
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 130
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_5

    .line 131
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->hideWhenRemoved:Z

    if-eqz v4, :cond_0

    .line 132
    invoke-virtual {p0, v9}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setVisibility(I)V

    .line 134
    :cond_0
    sget v4, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_FAILED:I

    invoke-virtual {v0, v4, v9}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setFailed(Z)V

    .line 136
    sget v4, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNC_STARTED:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v1

    .line 137
    .local v1, "lastSyncStarted":Ljava/lang/Long;
    if-nez v1, :cond_1

    move-wide v4, v6

    :goto_0
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 139
    sget v4, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_LAST_SYNCED:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsLong(I)Ljava/lang/Long;

    move-result-object v2

    .line 140
    .local v2, "lastSynced":Ljava/lang/Long;
    if-nez v2, :cond_2

    move-wide v4, v6

    :goto_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 142
    sget v4, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->DK_SYNC_PROGRESS:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsFloat(I)Ljava/lang/Float;

    move-result-object v3

    .line 143
    .local v3, "syncProgress":Ljava/lang/Float;
    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "updateOnStateChanged with download fraction %f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v9

    invoke-virtual {v4, v5, v8}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    if-eqz v3, :cond_3

    .line 145
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    const v5, 0x3d4ccccd    # 0.05f

    add-float/2addr v4, v5

    const v5, 0x3f866666    # 1.05f

    div-float/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDownloadFraction(F)V

    .line 147
    sget-object v4, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "Download icon update"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    .end local v1    # "lastSyncStarted":Ljava/lang/Long;
    .end local v2    # "lastSynced":Ljava/lang/Long;
    .end local v3    # "syncProgress":Ljava/lang/Float;
    :goto_2
    return-void

    .line 137
    .restart local v1    # "lastSyncStarted":Ljava/lang/Long;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_0

    .line 140
    .restart local v2    # "lastSynced":Ljava/lang/Long;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_1

    .line 148
    .restart local v3    # "syncProgress":Ljava/lang/Float;
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    .line 149
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDownloadFraction(F)V

    goto :goto_2

    .line 151
    :cond_4
    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDownloadFraction(F)V

    goto :goto_2

    .line 154
    .end local v1    # "lastSyncStarted":Ljava/lang/Long;
    .end local v2    # "lastSynced":Ljava/lang/Long;
    .end local v3    # "syncProgress":Ljava/lang/Float;
    :cond_5
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->hideWhenRemoved:Z

    if-eqz v4, :cond_6

    .line 155
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setVisibility(I)V

    .line 157
    :cond_6
    invoke-virtual {p0, v10}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDownloadFraction(F)V

    goto :goto_2
.end method


# virtual methods
.method public clearDataOnDetach(Z)V
    .locals 1
    .param p1, "clearDataOnDetach"    # Z

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->clearDataOnDetach(Z)Z

    .line 178
    return-void
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    .line 188
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/google/android/play/widget/DownloadStatusView;->onAttachedToWindow()V

    .line 91
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onAttachedToWindow()V

    .line 92
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->updateOnStateChanged()V

    .line 126
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/play/widget/DownloadStatusView;->onDetachedFromWindow()V

    .line 97
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onDetachedFromWindow()V

    .line 98
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Lcom/google/android/play/widget/DownloadStatusView;->onFinishTemporaryDetach()V

    .line 109
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onFinishTemporaryDetach()V

    .line 110
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onMeasure(II)V

    .line 115
    invoke-super {p0, p1, p2}, Lcom/google/android/play/widget/DownloadStatusView;->onMeasure(II)V

    .line 116
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/android/play/widget/DownloadStatusView;->onStartTemporaryDetach()V

    .line 103
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataViewHelper;->onStartTemporaryDetach()V

    .line 104
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataViewHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 121
    return-void
.end method

.method public setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 3
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    const/4 v1, 0x0

    .line 70
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    if-eqz v2, :cond_2

    .line 71
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {p1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->helper:Lcom/google/android/libraries/bind/data/DataViewHelper;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataViewHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    if-nez v2, :cond_1

    .line 72
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 73
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->pinnedList()Lcom/google/apps/dots/android/newsstand/sync/PinnedList;

    move-result-object v0

    .line 74
    .local v0, "pinnedList":Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    check-cast v1, Lcom/google/android/libraries/bind/data/Filter;

    invoke-virtual {v0, p1, v1}, Lcom/google/apps/dots/android/newsstand/sync/PinnedList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->setEmptyWhenNone(Z)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 79
    .end local v0    # "pinnedList":Lcom/google/apps/dots/android/newsstand/sync/PinnedList;
    :cond_1
    :goto_0
    return-void

    .line 77
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_0
.end method

.method public setHideWhenRemoved(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->hideWhenRemoved:Z

    if-eq v0, p1, :cond_0

    .line 83
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->hideWhenRemoved:Z

    .line 84
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->updateOnStateChanged()V

    .line 86
    :cond_0
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 63
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/DownloadProgressView;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 64
    return-void

    .line 63
    :cond_0
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    goto :goto_0
.end method
