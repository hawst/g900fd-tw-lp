.class public Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "OffersIntentBuilder.java"


# instance fields
.field private offersFragmentState:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "offersFragmentState"    # Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 22
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->offersFragmentState:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .line 23
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 32
    const-class v1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersActivity;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 33
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "OffersFragment_state"

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->offersFragmentState:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->offersFragmentState:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 35
    return-object v0

    .line 33
    :cond_0
    new-instance v1, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;-><init>()V

    goto :goto_0
.end method

.method public setOfferId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 26
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    invoke-direct {v0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/OffersIntentBuilder;->offersFragmentState:Lcom/google/apps/dots/android/newsstand/home/explore/OffersFragmentState;

    .line 27
    return-object p0
.end method
