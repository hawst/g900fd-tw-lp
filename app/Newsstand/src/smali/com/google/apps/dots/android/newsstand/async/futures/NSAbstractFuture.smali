.class public abstract Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;
.super Ljava/lang/Object;
.source "NSAbstractFuture.java"

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<TV;>;"
    }
.end annotation


# static fields
.field static final GENERIC_CANCELLATION_EXCEPTION:Ljava/util/concurrent/CancellationException;


# instance fields
.field private exception:Ljava/lang/Throwable;

.field private listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private state:I

.field private value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/util/concurrent/CancellationException;

    const-string v1, "Future cancelled"

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->GENERIC_CANCELLATION_EXCEPTION:Ljava/util/concurrent/CancellationException;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 37
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create(I)Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 38
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 39
    return-void
.end method

.method private executionListAdd(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 186
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 188
    .local v0, "queue":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Ljava/lang/Runnable;>;"
    if-eqz v0, :cond_1

    .line 189
    monitor-enter v0

    .line 190
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->runningOn(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 192
    monitor-exit v0

    .line 202
    :goto_0
    return-void

    .line 194
    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    :cond_1
    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private executionListExecute()V
    .locals 2

    .prologue
    .line 207
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 208
    .local v0, "queue":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Ljava/lang/Runnable;>;"
    if-eqz v0, :cond_0

    .line 209
    monitor-enter v0

    .line 210
    :try_start_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    if-nez v1, :cond_1

    .line 211
    monitor-exit v0

    .line 222
    :cond_0
    return-void

    .line 213
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->listeners:Lcom/google/android/libraries/bind/collections/RingBuffer;

    .line 214
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 214
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static getGenericCancellation()Ljava/util/concurrent/CancellationException;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->GENERIC_CANCELLATION_EXCEPTION:Ljava/util/concurrent/CancellationException;

    return-object v0
.end method

.method private syncComplete(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
    .locals 1
    .param p2, "t"    # Ljava/lang/Throwable;
    .param p3, "finalState"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Ljava/lang/Throwable;",
            "I)Z"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    .local p1, "v":Ljava/lang/Object;, "TV;"
    monitor-enter p0

    .line 165
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    if-nez v0, :cond_1

    .line 166
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->value:Ljava/lang/Object;

    .line 168
    and-int/lit8 v0, p3, 0x6

    if-eqz v0, :cond_0

    .line 169
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->getGenericCancellation()Ljava/util/concurrent/CancellationException;

    move-result-object p2

    .end local p2    # "t":Ljava/lang/Throwable;
    :cond_0
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->exception:Ljava/lang/Throwable;

    .line 170
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    .line 171
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 172
    const/4 v0, 0x1

    monitor-exit p0

    .line 174
    :goto_0
    return v0

    .restart local p2    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_0

    .line 175
    .end local p2    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private syncGetValue()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/concurrent/CancellationException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    packed-switch v0, :pswitch_data_0

    .line 146
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error, synchronizer in invalid state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->exception:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->exception:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->value:Ljava/lang/Object;

    return-object v0

    .line 143
    :pswitch_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->getGenericCancellation()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    throw v0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "listener"    # Ljava/lang/Runnable;
    .param p2, "exec"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 108
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->executionListAdd(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 109
    return-void
.end method

.method public cancel(Z)Z
    .locals 2
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    const/4 v1, 0x0

    .line 87
    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-direct {p0, v1, v1, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->syncComplete(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->executionListExecute()V

    .line 89
    if-eqz p1, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->interruptTask()V

    .line 92
    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_1
    return v0

    .line 87
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 94
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    monitor-enter p0

    .line 64
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 67
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->syncGetValue()Ljava/lang/Object;

    move-result-object v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 11
    .param p1, "timeout"    # J
    .param p3, "unit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/TimeoutException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 45
    .local v4, "nanos":J
    monitor-enter p0

    .line 46
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->isDone()Z

    move-result v8

    if-nez v8, :cond_1

    .line 47
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 48
    .local v6, "start":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-ltz v8, :cond_0

    .line 49
    const-wide/32 v8, 0xf4240

    div-long v2, v4, v8

    .line 50
    .local v2, "millis":J
    const-wide/16 v8, 0x1

    add-long/2addr v8, v2

    invoke-virtual {p0, v8, v9}, Ljava/lang/Object;->wait(J)V

    .line 51
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 52
    .local v0, "end":J
    sub-long v8, v0, v6

    sub-long/2addr v4, v8

    .line 53
    goto :goto_0

    .line 54
    .end local v0    # "end":J
    .end local v2    # "millis":J
    :cond_0
    new-instance v8, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v8}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v8

    .line 57
    .end local v6    # "start":J
    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->syncGetValue()Ljava/lang/Object;

    move-result-object v8

    return-object v8
.end method

.method protected interruptTask()V
    .locals 0

    .prologue
    .line 98
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    return-void
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 80
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    monitor-enter p0

    .line 81
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    and-int/lit8 v0, v0, 0x6

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 73
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    monitor-enter p0

    .line 74
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    and-int/lit8 v0, v0, 0x7

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected set(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    const/4 v0, 0x1

    .line 112
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->syncComplete(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->executionListExecute()V

    .line 116
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setException(Ljava/lang/Throwable;)Z
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    const/4 v0, 0x1

    .line 120
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->syncComplete(Ljava/lang/Object;Ljava/lang/Throwable;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->executionListExecute()V

    .line 124
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final wasInterrupted()Z
    .locals 2

    .prologue
    .line 101
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;, "Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture<TV;>;"
    monitor-enter p0

    .line 102
    :try_start_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/NSAbstractFuture;->state:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
