.class public abstract Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;
.super Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;
.source "UndoToastBarOperation.java"


# static fields
.field private static final UNDO_ACTION_RESOURCE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_toast_undo:I

    sput v0, Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;->UNDO_ACTION_RESOURCE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "operationLabel"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/toast/ToastBarOperation;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getActionIconResourceId()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/google/apps/dots/android/newsstand/toast/UndoToastBarOperation;->UNDO_ACTION_RESOURCE_ID:I

    return v0
.end method
