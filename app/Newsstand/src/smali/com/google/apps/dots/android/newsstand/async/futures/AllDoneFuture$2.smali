.class Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;
.super Ljava/lang/Object;
.source "AllDoneFuture.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;-><init>(Ljava/util/Collection;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

.field final synthetic val$futures:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;Ljava/util/Collection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;->val$futures:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 44
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture;->wasInterrupted()Z

    move-result v1

    .line 46
    .local v1, "wasInterrupted":Z
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/AllDoneFuture$2;->val$futures:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 47
    .local v0, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0

    .line 50
    .end local v0    # "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<*>;"
    .end local v1    # "wasInterrupted":Z
    :cond_0
    return-void
.end method
