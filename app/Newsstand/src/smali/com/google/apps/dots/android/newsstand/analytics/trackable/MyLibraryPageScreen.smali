.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;
.source "MyLibraryPageScreen.java"


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;)V
    .locals 2
    .param p1, "libraryPage"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;

    .prologue
    .line 11
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage;->type:Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen;->getLibraryPageTitleFromType(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsScreenBase;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method private static getLibraryPageTitleFromType(Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;

    .prologue
    .line 15
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/MyLibraryPageScreen$1;->$SwitchMap$com$google$apps$dots$android$newsstand$home$library$LibraryPage$Type:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/library/LibraryPage$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 23
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 17
    :pswitch_0
    const-string v0, "MyNews"

    goto :goto_0

    .line 19
    :pswitch_1
    const-string v0, "MyMagazines"

    goto :goto_0

    .line 21
    :pswitch_2
    const-string v0, "MyTopics"

    goto :goto_0

    .line 15
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
