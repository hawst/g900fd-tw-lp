.class Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "PlainEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

.field final synthetic val$menu:Landroid/view/Menu;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;Landroid/view/Menu;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;->val$menu:Landroid/view/Menu;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;->this$0:Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->translateHelper:Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;->access$300(Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment;)Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;->val$menu:Landroid/view/Menu;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/apps/dots/android/newsstand/translation/TranslateMenuHelper;->onPrepareOptionsMenu(Landroid/view/Menu;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Ljava/lang/String;)V

    .line 370
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 366
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/PlainEditionFragment$8;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
