.class public final Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;


# instance fields
.field private bitField0_:I

.field private cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

.field private currentLayoutVersion_:I

.field private demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

.field private disableNewsstand_:Z

.field private enableMagazines_:Z

.field private experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

.field private playCountry_:Ljava/lang/String;

.field private serverCountry_:Ljava/lang/String;

.field private syncFrequencyMillis_:J

.field private userId_:Ljava/lang/String;

.field private warmWelcomeNeeded_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28672
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28673
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 28895
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    .line 28933
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    .line 28955
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    .line 28974
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    .line 29031
    iput-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    .line 29050
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    .line 29072
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    .line 29091
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    .line 28673
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .locals 3

    .prologue
    .line 29133
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29137
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-eqz v2, :cond_0

    .line 29138
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    .line 29140
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-eqz v2, :cond_1

    .line 29141
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->clone()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    .line 29143
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v2, :cond_2

    .line 29144
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 29146
    :cond_2
    return-object v0

    .line 29134
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    :catch_0
    move-exception v1

    .line 29135
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28669
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29151
    if-ne p1, p0, :cond_1

    .line 29164
    :cond_0
    :goto_0
    return v1

    .line 29152
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 29153
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    .line 29154
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-nez v3, :cond_3

    .line 29155
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 29156
    :goto_2
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-nez v3, :cond_3

    .line 29159
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v3, :cond_3

    .line 29160
    :goto_4
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 29162
    :goto_5
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 29164
    goto :goto_0

    .line 29154
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    .line 29155
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    .line 29156
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    .line 29159
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 29160
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    .line 29162
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    .line 29164
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getCacheVersion()Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;
    .locals 1

    .prologue
    .line 28916
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    return-object v0
.end method

.method public getCurrentLayoutVersion()I
    .locals 1

    .prologue
    .line 29074
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    return v0
.end method

.method public getDemographics()Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;
    .locals 1

    .prologue
    .line 28995
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    return-object v0
.end method

.method public getDisableNewsstand()Z
    .locals 1

    .prologue
    .line 28957
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    return v0
.end method

.method public getEnableMagazines()Z
    .locals 1

    .prologue
    .line 28976
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    return v0
.end method

.method public getExperiments()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    .locals 1

    .prologue
    .line 29014
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 29223
    const/4 v0, 0x0

    .line 29224
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 29225
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    .line 29226
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29228
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-eqz v1, :cond_1

    .line 29229
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    .line 29230
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29232
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 29233
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    .line 29234
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29236
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 29237
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    .line 29238
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29240
    :cond_3
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 29241
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    .line 29242
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29244
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-eqz v1, :cond_5

    .line 29245
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    .line 29246
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29248
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v1, :cond_6

    .line 29249
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 29250
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29252
    :cond_6
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_7

    .line 29253
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    .line 29254
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29256
    :cond_7
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_8

    .line 29257
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    .line 29258
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29260
    :cond_8
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 29261
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    .line 29262
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 29264
    :cond_9
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_a

    .line 29265
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    .line 29266
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29268
    :cond_a
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cachedSize:I

    .line 29269
    return v0
.end method

.method public getServerCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29093
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    return-object v0
.end method

.method public getSyncFrequencyMillis()J
    .locals 2

    .prologue
    .line 28897
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    return-wide v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28935
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    return-object v0
.end method

.method public getWarmWelcomeNeeded()Z
    .locals 1

    .prologue
    .line 29033
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    return v0
.end method

.method public hasDemographics()Z
    .locals 1

    .prologue
    .line 29005
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29168
    const/16 v0, 0x11

    .line 29169
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 29170
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    iget-wide v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    const/16 v5, 0x20

    ushr-long/2addr v8, v5

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 29171
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 29172
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v5, v1

    .line 29173
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    add-int v0, v5, v1

    .line 29174
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    if-eqz v1, :cond_3

    move v1, v3

    :goto_3
    add-int v0, v5, v1

    .line 29175
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v5, v1

    .line 29176
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v5, v1

    .line 29177
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    if-eqz v5, :cond_6

    :goto_6
    add-int v0, v1, v3

    .line 29178
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    add-int v0, v3, v1

    .line 29179
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    add-int v0, v1, v3

    .line 29180
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    if-nez v3, :cond_8

    :goto_8
    add-int v0, v1, v2

    .line 29181
    return v0

    .line 29171
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;->hashCode()I

    move-result v1

    goto :goto_0

    .line 29172
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 29173
    goto :goto_2

    :cond_3
    move v1, v4

    .line 29174
    goto :goto_3

    .line 29175
    :cond_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;->hashCode()I

    move-result v1

    goto :goto_4

    .line 29176
    :cond_5
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    move v3, v4

    .line 29177
    goto :goto_6

    .line 29178
    :cond_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    .line 29180
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29277
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 29278
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 29282
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 29283
    :sswitch_0
    return-object p0

    .line 29288
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    .line 29289
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto :goto_0

    .line 29293
    :sswitch_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-nez v1, :cond_1

    .line 29294
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    .line 29296
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 29300
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    .line 29301
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto :goto_0

    .line 29305
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    .line 29306
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto :goto_0

    .line 29310
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    .line 29311
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto :goto_0

    .line 29315
    :sswitch_6
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-nez v1, :cond_2

    .line 29316
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    .line 29318
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 29322
    :sswitch_7
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v1, :cond_3

    .line 29323
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 29325
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 29329
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    .line 29330
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto/16 :goto_0

    .line 29334
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    .line 29335
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto/16 :goto_0

    .line 29339
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    .line 29340
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto/16 :goto_0

    .line 29344
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    .line 29345
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    goto/16 :goto_0

    .line 29278
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28669
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29186
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 29187
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->syncFrequencyMillis_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 29189
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    if-eqz v0, :cond_1

    .line 29190
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->cacheVersion_:Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig$CacheVersion;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 29192
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 29193
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->userId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29195
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 29196
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->disableNewsstand_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 29198
    :cond_3
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 29199
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->enableMagazines_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 29201
    :cond_4
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    if-eqz v0, :cond_5

    .line 29202
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->demographics_:Lcom/google/apps/dots/proto/client/DotsShared$UserDemographics;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 29204
    :cond_5
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-eqz v0, :cond_6

    .line 29205
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->experiments_:Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 29207
    :cond_6
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 29208
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->warmWelcomeNeeded_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 29210
    :cond_7
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 29211
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->playCountry_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29213
    :cond_8
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 29214
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->currentLayoutVersion_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 29216
    :cond_9
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    .line 29217
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientConfig;->serverCountry_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 29219
    :cond_a
    return-void
.end method
