.class Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;
.super Ljava/lang/Object;
.source "SharedPreferencesPrefStore.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/util/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;)Lcom/google/apps/dots/android/newsstand/util/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;

.field final synthetic val$listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field final synthetic val$sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;->this$0:Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;->val$sharedPreferences:Landroid/content/SharedPreferences;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;->val$listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;->val$sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/prefstore/SharedPreferencesPrefStore$3;->val$listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 114
    return-void
.end method
