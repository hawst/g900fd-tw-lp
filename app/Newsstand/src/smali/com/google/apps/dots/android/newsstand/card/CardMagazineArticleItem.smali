.class public Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;
.source "CardMagazineArticleItem.java"


# static fields
.field public static final DK_AUTHOR:I

.field public static final DK_IMAGE_ID:I

.field public static final DK_IS_READ:I

.field public static final DK_STORY_ON_CLICK_LISTENER:I

.field public static final DK_SUBTITLE:I

.field public static final DK_TITLE:I

.field public static final DK_TRANSFORM:I

.field public static final DK_VERSION:I

.field public static final EQUALITY_FIELDS:[I

.field public static final LAYOUTS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_version:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_VERSION:I

    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_title:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_TITLE:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_subtitle:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_SUBTITLE:I

    .line 35
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_author:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_AUTHOR:I

    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_imageId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IMAGE_ID:I

    .line 39
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_transform:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_TRANSFORM:I

    .line 42
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_onClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_STORY_ON_CLICK_LISTENER:I

    .line 46
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardMagazineArticleItem_isRead:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IS_READ:I

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_VERSION:I

    aput v1, v0, v2

    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IS_READ:I

    aput v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->EQUALITY_FIELDS:[I

    .line 52
    new-array v0, v3, [I

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_magazine_article_item:I

    aput v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->LAYOUTS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public static fillInData(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p4, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 71
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_VERSION:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getUpdated()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 73
    iget-object v4, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 74
    .local v4, "postId":Ljava/lang/String;
    move-object v3, p2

    .line 76
    .local v3, "originalEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_TITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 78
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSubtitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 79
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_SUBTITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSubtitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasAuthor()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 84
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_AUTHOR:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAuthor()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary$Author;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 87
    :cond_1
    const/4 v5, 0x0

    .line 88
    .local v5, "scrubberImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    array-length v6, v6

    if-lez v6, :cond_5

    .line 89
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->scrubberImages:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    const/4 v7, 0x0

    aget-object v5, v6, v7

    .line 93
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    .line 94
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IMAGE_ID:I

    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 100
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v6

    if-nez v6, :cond_3

    .line 101
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v1

    .line 102
    .local v1, "imageHeight":I
    invoke-virtual {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v2

    .line 103
    .local v2, "imageWidth":I
    if-le v1, v2, :cond_3

    .line 104
    int-to-float v6, v2

    int-to-float v7, v1

    div-float v0, v6, v7

    .line 105
    .local v0, "cropFraction":F
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_TRANSFORM:I

    new-instance v7, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    invoke-direct {v7}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;-><init>()V

    const v8, 0x477fff00    # 65535.0f

    mul-float/2addr v8, v0

    .line 107
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->fcrop64Bottom(I)Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;

    move-result-object v7

    .line 108
    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/server/Transform$Builder;->build()Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v7

    .line 105
    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 113
    .end local v0    # "cropFraction":F
    .end local v1    # "imageHeight":I
    .end local v2    # "imageWidth":I
    :cond_3
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_STORY_ON_CLICK_LISTENER:I

    new-instance v7, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;

    invoke-direct {v7, p2, v3, p1, v4}, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem$1;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;Ljava/lang/String;)V

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 128
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_IS_READ:I

    invoke-virtual {p3, v4}, Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;->isRead(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 129
    return-void

    .line 80
    .end local v5    # "scrubberImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAbstract()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 81
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardMagazineArticleItem;->DK_SUBTITLE:I

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAbstract()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v6, v7}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 90
    .restart local v5    # "scrubberImage":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    :cond_5
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 91
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v5

    goto :goto_1
.end method
