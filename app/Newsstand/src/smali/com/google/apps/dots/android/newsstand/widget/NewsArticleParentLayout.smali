.class public Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
.super Landroid/widget/FrameLayout;
.source "NewsArticleParentLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$WebViewListener;
    }
.end annotation


# static fields
.field private static final ARTICLE_TAIL_DISPLAY_DELAY_MS:J = 0xc8L

.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final SCROLL_RANGE_POLL_INTERVAL_MS:J = 0x64L

.field private static final SCROLL_RANGE_TOTAL_POLLING_TIME_MS:J

.field private static final SCROLL_RANGE_UPDATE_THRESHOLD_MS:J


# instance fields
.field private articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

.field private articleTailEnabled:Z

.field private articleTailShown:Z

.field private asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field private gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private interceptedTouchDownY:F

.field private isInterceptingScroll:Z

.field private lastKnownScrollRange:I

.field private lastScrollRangeUpdateEpoch:J

.field private meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

.field private pollScrollRangeRunnable:Ljava/lang/Runnable;

.field private renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

.field private webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 38
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_UPDATE_THRESHOLD_MS:J

    .line 51
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_TOTAL_POLLING_TIME_MS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 365
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 365
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 365
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 365
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateArticleTailPosition(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100()J
    .locals 2

    .prologue
    .line 36
    sget-wide v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_UPDATE_THRESHOLD_MS:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->showArticleTail()V

    return-void
.end method

.method static synthetic access$1300()J
    .locals 2

    .prologue
    .line 36
    sget-wide v0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->SCROLL_RANGE_TOTAL_POLLING_TIME_MS:J

    return-wide v0
.end method

.method static synthetic access$1400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->forceUpdateBottomMargin(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->tryScrollBy(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # F

    .prologue
    .line 36
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    return p1
.end method

.method static synthetic access$602(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastKnownScrollRange:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastKnownScrollRange:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;
    .param p1, "x1"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->lastScrollRangeUpdateEpoch:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z

    return v0
.end method

.method private forceUpdateBottomMargin(I)V
    .locals 7
    .param p1, "margin"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 269
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "dots.layout.setBottomMargin(%d);"

    new-array v3, v6, [Ljava/lang/Object;

    .line 270
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "updateMarginScript":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    new-array v2, v6, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->executeStatements([Ljava/lang/String;)V

    .line 276
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z

    if-nez v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->pollScrollRangeRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 279
    :cond_0
    return-void
.end method

.method private isMeterDialogPresent()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isSetup()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setupArticleTail()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 178
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 179
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    instance-of v1, v1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 180
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 181
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/SectionEdition;->getEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    .line 180
    invoke-static {v4, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 182
    :goto_0
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    .line 183
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    if-eqz v1, :cond_3

    .line 184
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 185
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getPostId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    .line 186
    .local v0, "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setVisibility(I)V

    .line 188
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;

    invoke-direct {v2, p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    .end local v0    # "articleLoader":Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 180
    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 182
    invoke-interface {v1}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getOriginalEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    invoke-interface {v4}, Lcom/google/apps/dots/android/newsstand/reading/RenderSource;->getReadingEdition()Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0

    .line 195
    :cond_3
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    goto :goto_1
.end method

.method private setupScrollDelegation()V
    .locals 3

    .prologue
    .line 291
    new-instance v0, Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;)V

    invoke-direct {v0, v1, v2}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    .line 324
    return-void
.end method

.method private showArticleTail()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 284
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailShown:Z

    .line 285
    return-void
.end method

.method private tryScrollBy(II)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x0

    .line 334
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateVerticalScrollRange()I

    move-result v1

    .line 335
    .local v1, "scrollRange":I
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v3, v1, v3

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 337
    .local v0, "bottomOffset":I
    if-gtz v0, :cond_0

    if-gez p2, :cond_3

    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v3

    if-gtz v3, :cond_1

    if-lez p2, :cond_3

    .line 338
    :cond_1
    if-lez p2, :cond_2

    if-le p2, v0, :cond_2

    .line 339
    move p2, v0

    .line 341
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v2, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->scrollBy(II)V

    .line 342
    const/4 v2, 0x1

    .line 344
    :cond_3
    return v2
.end method

.method private updateArticleTailPosition(Z)Z
    .locals 8
    .param p1, "force"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 207
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    if-nez v5, :cond_1

    .line 229
    :cond_0
    :goto_0
    return v4

    .line 211
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->calculateVerticalScrollRange()I

    move-result v2

    .line 218
    .local v2, "scrollRange":I
    if-lez v2, :cond_3

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v5

    if-eq v2, v5, :cond_3

    move v1, v3

    .line 221
    .local v1, "hasAccurateScrollRange":Z
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 222
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 223
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getScrollY()I

    move-result v6

    sub-int v6, v2, v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    .line 221
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 225
    .local v0, "desiredTranslation":I
    if-nez p1, :cond_2

    if-eqz v1, :cond_0

    .line 226
    :cond_2
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    int-to-float v5, v0

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->setY(F)V

    move v4, v3

    .line 227
    goto :goto_0

    .end local v0    # "desiredTranslation":I
    .end local v1    # "hasAccurateScrollRange":Z
    :cond_3
    move v1, v4

    .line 218
    goto :goto_1
.end method

.method private updateBottomMargin()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 242
    iget-boolean v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    if-eqz v5, :cond_0

    .line 244
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->getDefaultArticleMarginBottom()Ljava/lang/String;

    move-result-object v5

    const-string v6, "px"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 243
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 245
    .local v0, "defaultBottomMargin":I
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .line 246
    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ArticleLoadingUtil;->convertPxToViewportPx(F)I

    move-result v5

    add-int v1, v5, v0

    .line 250
    .local v1, "margin":I
    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 251
    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getOnScriptLoadFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    aput-object v6, v5, v4

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->getOnLoadFuture()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    aput-object v4, v5, v3

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->allAsList([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 253
    .local v2, "webviewReadyFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    new-instance v5, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$2;

    invoke-direct {v5, p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;I)V

    invoke-virtual {v4, v2, v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 261
    .end local v0    # "defaultBottomMargin":I
    .end local v1    # "margin":I
    .end local v2    # "webviewReadyFuture":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<Ljava/util/List<Ljava/lang/Object;>;>;"
    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method


# virtual methods
.method public handleWebViewScrollChanged()V
    .locals 4

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/async/JankLock;->pauseTemporarily(J)V

    .line 104
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateArticleTailPosition(Z)Z

    .line 105
    return-void
.end method

.method public handleWebViewSizeChanged()V
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateArticleTailPosition(Z)Z

    .line 100
    :cond_0
    return-void
.end method

.method protected onArticleTailLaidOut()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->updateBottomMargin()Z

    .line 234
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 115
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isMeterDialogPresent()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTailEnabled:Z

    if-eqz v4, :cond_0

    .line 118
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getTranslationY()F

    move-result v5

    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;->getTop()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 146
    :cond_0
    :goto_0
    return v2

    .line 121
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    .line 122
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 126
    :pswitch_1
    iput v7, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    .line 127
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z

    .line 128
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 133
    :pswitch_2
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z

    if-eqz v4, :cond_1

    move v2, v3

    .line 134
    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v0, v4

    .line 138
    .local v0, "scrollDist":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 139
    .local v1, "touchSlop":I
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    cmpl-float v4, v4, v7

    if-lez v4, :cond_0

    if-le v0, v1, :cond_0

    .line 140
    iput-boolean v3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z

    move v2, v3

    .line 141
    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 163
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isMeterDialogPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->gestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v2, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 168
    const/4 v0, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->interceptedTouchDownY:F

    .line 172
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->isInterceptingScroll:Z

    :cond_2
    move v0, v1

    .line 174
    goto :goto_0
.end method

.method public setup(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;Lcom/google/apps/dots/android/newsstand/reading/RenderSource;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 1
    .param p1, "newsWebView"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;
    .param p2, "renderSource"    # Lcom/google/apps/dots/android/newsstand/reading/RenderSource;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .prologue
    .line 87
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->meter_dialog:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->meterDialog:Lcom/google/apps/dots/android/newsstand/widget/MeterDialog;

    .line 88
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->article_tail:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->articleTail:Lcom/google/apps/dots/android/newsstand/widget/ArticleTail;

    .line 89
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->webView:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .line 90
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->renderSource:Lcom/google/apps/dots/android/newsstand/reading/RenderSource;

    .line 91
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    .line 92
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->setupArticleTail()V

    .line 93
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsArticleParentLayout;->setupScrollDelegation()V

    .line 94
    return-void
.end method
