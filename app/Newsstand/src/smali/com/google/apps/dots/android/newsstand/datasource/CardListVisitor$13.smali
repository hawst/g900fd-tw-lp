.class Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;
.source "CardListVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeWebPageCardData(Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

.field final synthetic val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    .prologue
    .line 914
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsEventProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
    .locals 5

    .prologue
    .line 917
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleSeenEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getPublisher()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    .line 918
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->val$webPageSummary:Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;->getWebPageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/WebArticleSeenEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 914
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor$13;->get()Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;

    move-result-object v0

    return-object v0
.end method
