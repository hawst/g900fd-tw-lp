.class public final Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsSyncV3.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsSyncV3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Root"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;


# instance fields
.field public rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

.field private self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 32
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 10
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .locals 5

    .prologue
    .line 45
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v3, :cond_0

    .line 50
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    move-result-object v3

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 52
    :cond_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 53
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 54
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 55
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v3, v3, v2

    if-eqz v3, :cond_1

    .line 56
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    move-result-object v4

    aput-object v4, v3, v2

    .line 54
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 60
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    :cond_2
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->clone()Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 67
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    .line 68
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 69
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 68
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getSelf()Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v2, :cond_0

    .line 103
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 104
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 106
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v2, :cond_2

    .line 107
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 108
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    if-eqz v0, :cond_1

    .line 109
    const/4 v5, 0x2

    .line 110
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 107
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->cachedSize:I

    .line 115
    return v1
.end method

.method public hasSelf()Z
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 73
    const/16 v1, 0x11

    .line 74
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 75
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 76
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 82
    :cond_0
    return v1

    .line 75
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;->hashCode()I

    move-result v2

    goto :goto_0

    .line 78
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 79
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 79
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 123
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 124
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 128
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 129
    :sswitch_0
    return-object p0

    .line 134
    :sswitch_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-nez v5, :cond_1

    .line 135
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    .line 137
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 141
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 142
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-nez v5, :cond_3

    move v1, v4

    .line 143
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 144
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v5, :cond_2

    .line 145
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    :cond_2
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .line 148
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 149
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    aput-object v6, v5, v1

    .line 150
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 151
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 142
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v1, v5

    goto :goto_1

    .line 154
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_4
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;-><init>()V

    aput-object v6, v5, v1

    .line 155
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 124
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    if-eqz v1, :cond_0

    .line 88
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->self_:Lcom/google/apps/dots/proto/client/DotsSyncV3$Link;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    if-eqz v1, :cond_2

    .line 91
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;->rootNode:[Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 92
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    if-eqz v0, :cond_1

    .line 93
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 91
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;
    :cond_2
    return-void
.end method
