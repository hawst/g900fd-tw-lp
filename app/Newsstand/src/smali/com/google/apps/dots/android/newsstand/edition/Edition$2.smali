.class final Lcom/google/apps/dots/android/newsstand/edition/Edition$2;
.super Ljava/lang/Object;
.source "Edition.java"

# interfaces
.implements Lcom/google/common/base/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/Edition;->createEditionByAppFamily(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Function",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;",
        "Lcom/google/apps/dots/android/newsstand/edition/Edition;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$userToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$2;->val$userToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 4
    .param p1, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 317
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    if-eqz v2, :cond_0

    .line 318
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 320
    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 322
    .local v0, "appId":Ljava/lang/String;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appSummaryStore()Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/Edition$2;->val$userToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/store/cache/AppSummaryStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->nullingGet(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 323
    .local v1, "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    if-eqz v1, :cond_1

    .line 324
    invoke-static {v1, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->fromSummaries(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    .line 327
    .end local v0    # "appId":Ljava/lang/String;
    .end local v1    # "appSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 314
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition$2;->apply(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    return-object v0
.end method
