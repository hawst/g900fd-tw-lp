.class public Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;
.super Ljava/lang/Object;
.source "ReadingActivityTracker.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private currentPages:Ljava/util/BitSet;

.field private pagesRead:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/BitSet;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;->currentPages:Ljava/util/BitSet;

    .line 20
    return-void
.end method


# virtual methods
.method public resetPagesRead()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/reading/ReadingActivityTracker;->pagesRead:I

    .line 28
    return-void
.end method
