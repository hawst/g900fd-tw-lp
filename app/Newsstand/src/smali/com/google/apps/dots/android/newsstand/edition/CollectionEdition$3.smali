.class Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "CollectionEdition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->addSyncOnSuccessCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final synthetic val$editionToSync:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;->this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;->val$editionToSync:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 175
    # getter for: Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Starting sync-on-refresh for %s."

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;->val$editionToSync:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$3;->val$editionToSync:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->syncEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 177
    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setAnyFreshness(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 178
    invoke-virtual {v0, v5}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->setUserRequested(Z)Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/SyncerIntentBuilder;->start()V

    .line 180
    return-void
.end method
