.class public Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;
.super Landroid/content/ContentProvider;
.source "NSContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;
    }
.end annotation


# instance fields
.field private attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

.field private nsStoreProvidelet:Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

.field private sqlProvidelet:Lcom/google/apps/dots/android/newsstand/provider/Providelet;

.field private syncedFileProvidelet:Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;

.field private final transactionDataProvider:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$1;-><init>(Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private getMatch(Landroid/net/Uri;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 180
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/Contract;->match(Landroid/net/Uri;)I

    move-result v0

    .line 181
    .local v0, "result":I
    if-gez v0, :cond_0

    .line 182
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 184
    :cond_0
    return v0
.end method

.method private getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    .locals 2
    .param p1, "match"    # I

    .prologue
    .line 188
    packed-switch p1, :pswitch_data_0

    .line 200
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported uri: "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    .line 198
    :goto_0
    return-object v0

    .line 193
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->sqlProvidelet:Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    goto :goto_0

    .line 195
    :pswitch_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->syncedFileProvidelet:Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;

    goto :goto_0

    .line 198
    :pswitch_4
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->nsStoreProvidelet:Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

    goto :goto_0

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public static isNoPumpFd(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 219
    const-string v1, "noPumpFd"

    invoke-static {p0, v1}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->getBooleanQueryParameterQuietly(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    .line 220
    .local v0, "noPumpFd":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private notifyIfNeeded()V
    .locals 6

    .prologue
    .line 205
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;

    .line 206
    .local v2, "transactionData":Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;
    iget-boolean v4, v2, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->inTransaction:Z

    if-eqz v4, :cond_0

    .line 216
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 211
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .line 212
    .local v1, "sendToNetwork":Z
    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 213
    .local v3, "uri":Landroid/net/Uri;
    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_1

    .line 215
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v4, v2, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v1

    .line 100
    .local v1, "match":I
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v0

    .line 101
    .local v0, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;

    .line 102
    .local v7, "transactionData":Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;
    iget-object v4, v7, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    iget-boolean v2, v7, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->inTransaction:Z

    if-nez v2, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v2, p1

    move-object v3, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->bulkInsert(ILandroid/net/Uri;[Landroid/content/ContentValues;Ljava/util/Set;Z)I

    move-result v6

    .line 104
    .local v6, "result":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->notifyIfNeeded()V

    .line 105
    return v6

    .line 102
    .end local v6    # "result":I
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v1

    .line 114
    .local v1, "match":I
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v0

    .line 115
    .local v0, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;

    .line 116
    .local v7, "transactionData":Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;
    iget-object v5, v7, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 117
    invoke-interface/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)I

    move-result v6

    .line 118
    .local v6, "result":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->notifyIfNeeded()V

    .line 119
    return v6
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 153
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/Contract;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 163
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;->getContentType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 158
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->syncedFileProvidelet:Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;->getContentType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->nsStoreProvidelet:Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;->getContentType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v0

    .line 88
    .local v0, "match":I
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v1

    .line 89
    .local v1, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;

    iget-object v3, v3, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    invoke-interface {v1, v0, p1, p2, v3}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->insert(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/util/Set;)Landroid/net/Uri;

    move-result-object v2

    .line 90
    .local v2, "result":Landroid/net/Uri;
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->notifyIfNeeded()V

    .line 91
    return-object v2
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 74
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->setup(Landroid/content/Context;)V

    .line 76
    new-instance v1, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    invoke-direct {v1, v0}, Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->attachmentsProvidelet:Lcom/google/apps/dots/android/newsstand/provider/AttachmentProvidelet;

    .line 77
    new-instance v1, Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->syncedFileProvidelet:Lcom/google/apps/dots/android/newsstand/provider/SyncedFileProvidelet;

    .line 78
    new-instance v1, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->nsStoreProvidelet:Lcom/google/apps/dots/android/newsstand/provider/NSStoreProvidelet;

    .line 79
    const/4 v1, 0x1

    return v1
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v0

    .line 175
    .local v0, "match":I
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v1

    .line 176
    .local v1, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    invoke-interface {v1, v0, p1, p2, p0}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->openAssetFile(ILandroid/net/Uri;Ljava/lang/String;Landroid/content/ContentProvider;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    return-object v2
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v1

    .line 144
    .local v1, "match":I
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v0

    .local v0, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 145
    invoke-interface/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getMatch(Landroid/net/Uri;)I

    move-result v1

    .line 128
    .local v1, "match":I
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->getProvidelet(I)Lcom/google/apps/dots/android/newsstand/provider/Providelet;

    move-result-object v0

    .line 129
    .local v0, "providelet":Lcom/google/apps/dots/android/newsstand/provider/Providelet;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->transactionDataProvider:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;

    .line 130
    .local v8, "transactionData":Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;
    iget-object v6, v8, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider$TransactionData;->notifyUris:Ljava/util/Set;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 131
    invoke-interface/range {v0 .. v6}, Lcom/google/apps/dots/android/newsstand/provider/Providelet;->update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)I

    move-result v7

    .line 132
    .local v7, "result":I
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/provider/NSContentProvider;->notifyIfNeeded()V

    .line 133
    return v7
.end method
