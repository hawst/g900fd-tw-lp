.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "OfferActionEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;
    }
.end annotation


# instance fields
.field private final appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

.field private final appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

.field private final readingScreen:Ljava/lang/String;

.field private final type:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;)V
    .locals 0
    .param p1, "readingScreen"    # Ljava/lang/String;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .param p3, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p4, "type"    # Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->readingScreen:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 30
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 31
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->type:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    .line 32
    return-void
.end method

.method private static getActionForOfferActionType(Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    .prologue
    .line 65
    sget-object v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$1;->$SwitchMap$com$google$apps$dots$android$newsstand$analytics$trackable$OfferActionEvent$OfferActionType:[I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 67
    :pswitch_0
    const-string v0, "Offer Accepted"

    .line 71
    :goto_0
    return-object v0

    .line 69
    :pswitch_1
    const-string v0, "Offer Declined"

    goto :goto_0

    .line 71
    :pswitch_2
    const-string v0, "Offer Edition Clicked"

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 3
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->type:Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->getActionForOfferActionType(Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent$OfferActionType;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 51
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 52
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppFamilyName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 53
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 54
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAppName(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 55
    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getStoreType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setStoreType(I)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 56
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 37
    const-string v0, "Internal"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 38
    return-object p1
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OfferActionEvent;->readingScreen:Ljava/lang/String;

    return-object v0
.end method
