.class Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "ExploreSingleTopicFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->updateAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

.field final synthetic val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

.field final synthetic val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;Ljava/util/concurrent/Executor;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;
    .param p2, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 4
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 157
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->shelf_detailed_header_first:I

    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 159
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 160
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TRANSITION_NAME:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->reloado_title:I

    .line 161
    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 162
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TEXT_COLOR:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->play_header_list_banner_text_color:I

    .line 163
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 162
    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 169
    :cond_0
    :goto_0
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_BUTTON_TEXT:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    sget v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    sget v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 172
    .local v0, "shelfType":I
    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 173
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->val$expandTopicsShelfListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 179
    .end local v0    # "shelfType":I
    :cond_1
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 165
    :cond_2
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_TITLE_TEXT_COLOR:I

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->play_primary_text:I

    .line 166
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 165
    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 174
    .restart local v0    # "shelfType":I
    :cond_3
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 175
    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicFragment$3;->val$expandNewsShelfListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method
