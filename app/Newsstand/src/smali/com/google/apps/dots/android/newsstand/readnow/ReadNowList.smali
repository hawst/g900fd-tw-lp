.class public Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;
.super Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;
.source "ReadNowList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;
    }
.end annotation


# instance fields
.field private cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->appContext:Landroid/content/Context;

    return-object v0
.end method

.method private static preferenceKeyIsForFeatureCard(Ljava/lang/String;)Z
    .locals 1
    .param p0, "optPreferenceKey"    # Ljava/lang/String;

    .prologue
    .line 94
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getFeatureCardPreferenceKeys()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cardListBuilder()Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    .locals 4

    .prologue
    .line 48
    new-instance v0, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->appContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;-><init>(Landroid/content/Context;)V

    .line 50
    .local v0, "cardListBuilder":Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->rawCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 53
    new-instance v1, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 54
    .local v1, "refreshButton":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->LAYOUT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 55
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->EQUALITY_FIELDS:[I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 56
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardRefreshButton;->DK_ON_CLICK_LISTENER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$1;

    invoke-direct {v3, p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$1;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 62
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/card/CardGroup;->addFooter(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 64
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->cardGroup:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 65
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 66
    return-object v0
.end method

.method protected getObservedPreferenceKeys()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getInstance()Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/card/NewFeatureCardHelper;->getFeatureCardPreferenceKeys()Ljava/util/List;

    move-result-object v0

    .line 79
    .local v0, "featureCardPreferenceKeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->getObservedPreferenceKeys()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 81
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    return-object v1
.end method

.method protected onPreferencesChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "optPreferenceKey"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->onPreferencesChanged(Ljava/lang/String;)V

    .line 87
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->preferenceAffectsApiUri(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->preferenceKeyIsForFeatureCard(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->invalidateData()V

    .line 91
    :cond_1
    return-void
.end method

.method public rawCardList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->rawCardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 72
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->makeEnsureNoWarmWelcomeCardIfOfferCardFilter()Lcom/google/android/libraries/bind/data/Filter;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method protected visitor(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
    .locals 6
    .param p1, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p2, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p3, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 101
    new-instance v0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;->appContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList$ReadNowEditionCardListVisitor;-><init>(Lcom/google/apps/dots/android/newsstand/readnow/ReadNowList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    return-object v0
.end method
