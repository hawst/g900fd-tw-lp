.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;
.super Ljava/lang/Object;
.source "NewsArticlePagerFragment.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->setupAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 265
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->state()Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;

    iget-object v0, v1, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 266
    .local v0, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :goto_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment$3;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 267
    # getter for: Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->pagerAdapter:Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;)Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/data/NSFragmentDataStatePagerAdapter;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    .line 266
    invoke-static {v1, v0, v3, v2}, Lcom/google/apps/dots/android/newsstand/card/ActionMessage;->getSpecificErrorConfiguration(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/Throwable;Ljava/lang/Runnable;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    return-object v1

    .end local v0    # "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    move-object v0, v2

    .line 265
    goto :goto_0
.end method
