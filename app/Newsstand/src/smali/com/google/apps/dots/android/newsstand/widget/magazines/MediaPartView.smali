.class public Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;
.super Lcom/google/apps/dots/android/newsstand/widget/MediaView;
.source "MediaPartView.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidget;


# static fields
.field private static final UPDATE_PROGRESS_POLL_INTERVAL_MS:I = 0x32


# instance fields
.field private final activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

.field private final enableEvents:Z

.field private lastTriggeredProgressPct:I

.field private final nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

.field private final nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

.field private final partId:Ljava/lang/String;

.field private final updateProgressRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nbContext"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;
    .param p3, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;
    .param p4, "mediaPart"    # Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;
    .param p5, "options"    # Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 31
    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getAsyncToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-direct {p0, p1, v0, p5}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;-><init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/widget/MediaView$Options;)V

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->lastTriggeredProgressPct:I

    .line 48
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$2;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->updateProgressRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 32
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    .line 33
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    .line 34
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    .line 35
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p4}, Lcom/google/apps/dots/proto/client/DotsNativeBody$NativeBodyPart;->getPartId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->configureEvents(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)V

    .line 38
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    .line 40
    new-instance v0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$1;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;Landroid/view/View;Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;ZZ)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    .line 46
    return-void

    :cond_1
    move v0, v5

    .line 33
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->updateProgress()V

    return-void
.end method

.method private configureEvents(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;)V
    .locals 4
    .param p1, "partId"    # Ljava/lang/String;
    .param p2, "eventDispatcher"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;
    .param p3, "eventScope"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 136
    sget-object v0, Lcom/google/apps/dots/shared/EventCode;->MEDIA_DO_START:Lcom/google/apps/dots/shared/EventCode;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    .line 137
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$3;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$3;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V

    .line 136
    invoke-virtual {p2, v0, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 143
    sget-object v0, Lcom/google/apps/dots/shared/EventCode;->MEDIA_DO_STOP:Lcom/google/apps/dots/shared/EventCode;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    .line 144
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$4;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$4;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V

    .line 143
    invoke-virtual {p2, v0, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 151
    sget-object v0, Lcom/google/apps/dots/shared/EventCode;->MEDIA_DO_RESUME:Lcom/google/apps/dots/shared/EventCode;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    .line 152
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$5;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$5;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V

    .line 151
    invoke-virtual {p2, v0, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 158
    sget-object v0, Lcom/google/apps/dots/shared/EventCode;->MEDIA_DO_PAUSE:Lcom/google/apps/dots/shared/EventCode;

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    .line 159
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$6;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView$6;-><init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;)V

    .line 158
    invoke-virtual {p2, v0, p3, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->addCallback(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventScope;Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher$EventCallback;)V

    .line 165
    return-void
.end method

.method private postUpdateProgress()V
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->updateProgressRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v2, 0x32

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 59
    return-void
.end method

.method private resetProgress()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->lastTriggeredProgressPct:I

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->updateProgressRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->cancel()V

    .line 132
    return-void
.end method

.method private triggerProgress(I)V
    .locals 5
    .param p1, "progressPct"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->MEDIA_ON_PROGRESS:Lcom/google/apps/dots/shared/EventCode;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 125
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 126
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->lastTriggeredProgressPct:I

    .line 127
    return-void
.end method

.method private updateProgress()V
    .locals 6

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->isMediaPlayable()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 105
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->getCurrentPosition()I

    move-result v3

    .line 106
    .local v3, "position":I
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->getDuration()I

    move-result v1

    .line 107
    .local v1, "duration":I
    if-lez v1, :cond_0

    if-ltz v3, :cond_0

    .line 108
    mul-int/lit8 v5, v3, 0x64

    div-int v4, v5, v1

    .line 110
    .local v4, "progressPct":I
    iget v5, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->lastTriggeredProgressPct:I

    sub-int v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->signum(I)I

    move-result v0

    .line 111
    .local v0, "di":I
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->lastTriggeredProgressPct:I

    .local v2, "i":I
    :goto_0
    if-eq v2, v4, :cond_0

    .line 112
    add-int v5, v2, v0

    invoke-direct {p0, v5}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->triggerProgress(I)V

    .line 111
    add-int/2addr v2, v0

    goto :goto_0

    .line 115
    .end local v0    # "di":I
    .end local v2    # "i":I
    .end local v4    # "progressPct":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->isPlaying()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->postUpdateProgress()V

    .line 121
    .end local v1    # "duration":I
    .end local v3    # "position":I
    :cond_1
    :goto_1
    return-void

    .line 119
    :cond_2
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->resetProgress()V

    goto :goto_1
.end method


# virtual methods
.method public isLoadComplete()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method protected onCompleted()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onCompleted()V

    .line 90
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    .line 91
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->triggerProgress(I)V

    .line 93
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 203
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onDetachedFromWindow()V

    .line 204
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0, p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->onDestroyed(Landroid/view/View;)V

    .line 205
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 185
    invoke-super/range {p0 .. p5}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onLayout(ZIIII)V

    .line 186
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onLayout()V

    .line 187
    return-void
.end method

.method protected onPaused()V
    .locals 5

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onPaused()V

    .line 82
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->MEDIA_ON_PAUSE:Lcom/google/apps/dots/shared/EventCode;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method

.method protected onStarted()V
    .locals 5

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStarted()V

    .line 64
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->MEDIA_ON_START:Lcom/google/apps/dots/shared/EventCode;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->postUpdateProgress()V

    .line 68
    :cond_0
    return-void
.end method

.method protected onStopped()V
    .locals 5

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onStopped()V

    .line 73
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nbContext:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;

    invoke-interface {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyContext;->getEventDispatcher()Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/shared/EventCode;->MEDIA_ON_STOP:Lcom/google/apps/dots/shared/EventCode;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->partId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/shared/EventCode;->forPart([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/EventDispatcher;->dispatch(Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->resetProgress()V

    .line 77
    :cond_0
    return-void
.end method

.method public onTransformChanged()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onTransformChanged()V

    .line 180
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 191
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 192
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onVisibilityChanged()V

    .line 193
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->onWindowVisibilityChanged(I)V

    .line 198
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->activatorHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/ActivatorHelper;->onWindowVisibilityChanged()V

    .line 199
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/MediaView;->seekTo(I)V

    .line 98
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->enableEvents:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->isMediaPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->updateProgress()V

    .line 101
    :cond_0
    return-void
.end method

.method public setContentArea(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/MediaPartView;->nativeWidgetHelper:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeWidgetHelper;->setContentArea(FFFF)V

    .line 175
    return-void
.end method
