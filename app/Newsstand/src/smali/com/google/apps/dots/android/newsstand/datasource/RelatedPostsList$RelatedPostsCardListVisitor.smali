.class public Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.source "RelatedPostsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RelatedPostsCardListVisitor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 6
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    .line 56
    sget v2, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;-><init>(Landroid/content/Context;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 57
    return-void
.end method


# virtual methods
.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    const-string v0, "%s %s - %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "[Related]"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    .line 77
    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 76
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected makeNewsPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;ZZ)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "postSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .param p2, "includeSourceData"    # Z
    .param p3, "darkTheme"    # Z

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->useDarkTheme:Z
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Z

    move-result v1

    invoke-super {p0, p1, p2, v1}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->makeNewsPostCardData(Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;ZZ)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 64
    .local v0, "cardData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 65
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/card/CardUtil;->toCompactLayout(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 64
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 66
    return-object v0
.end method

.method protected readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList$RelatedPostsCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/RelatedPostsList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->relatedPostsEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    move-result-object v0

    return-object v0
.end method
