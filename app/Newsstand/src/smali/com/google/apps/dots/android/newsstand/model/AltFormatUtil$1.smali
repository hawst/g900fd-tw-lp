.class final Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;
.super Ljava/lang/Object;
.source "AltFormatUtil.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormats(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$fragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

.field final synthetic val$jumpToAltFormats:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$jumpToAltFormats:Ljava/util/List;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$fragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "item"    # I

    .prologue
    .line 105
    if-ltz p2, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$jumpToAltFormats:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$fragment:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil$1;->val$jumpToAltFormats:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;

    # invokes: Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->jumpToAltFormat(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V
    invoke-static {v1, v2, v0}, Lcom/google/apps/dots/android/newsstand/model/AltFormatUtil;->access$000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$AltFormat;)V

    .line 108
    :cond_0
    return-void
.end method
