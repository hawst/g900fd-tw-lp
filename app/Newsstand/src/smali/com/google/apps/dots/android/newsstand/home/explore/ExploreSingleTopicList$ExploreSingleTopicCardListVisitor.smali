.class public Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;
.super Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;
.source "ExploreSingleTopicList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ExploreSingleTopicCardListVisitor"
.end annotation


# instance fields
.field currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

.field currentNewsShelfCard:Lcom/google/android/libraries/bind/data/Data;

.field currentShelfItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field currentTopicsShelfCard:Lcom/google/android/libraries/bind/data/Data;

.field private shelfHeaderAdded:Z

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V
    .locals 6
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "asyncToken"    # Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    .param p4, "readStateCollection"    # Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;
    .param p5, "librarySnapshot"    # Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 109
    sget v2, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;-><init>(Landroid/content/Context;ILcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/readstates/ReadStateCollection;Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;)V

    .line 102
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    .line 114
    return-void
.end method

.method private isInNewsShelf()Z
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInTopicsShelf()Z
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getType()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private limitRowsForGroupIfNecessary(Lcom/google/android/libraries/bind/card/GridGroup;Lcom/google/android/libraries/bind/data/Data;I)V
    .locals 6
    .param p1, "sourceGroup"    # Lcom/google/android/libraries/bind/card/GridGroup;
    .param p2, "header"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "numShelfEntries"    # I

    .prologue
    const/4 v5, 0x4

    .line 284
    invoke-virtual {p1, p3}, Lcom/google/android/libraries/bind/card/GridGroup;->getNumberOfColumns(I)I

    move-result v0

    .line 285
    .local v0, "numColumns":I
    int-to-float v2, p3

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 287
    .local v1, "numShelfRows":I
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v2, v3, :cond_0

    if-le v1, v5, :cond_0

    .line 291
    sget v2, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_BUTTON_TEXT:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$300(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->more_button:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 292
    sget v2, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->DK_SHELF_TYPE:I

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 293
    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/card/GridGroup;->setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 295
    :cond_0
    return-void
.end method

.method private shouldShowMagazinesShelf()Z
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInMagazinesShelf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowNewsShelf()Z
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInNewsShelf()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 149
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 150
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NEWS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowTopicsShelf()Z
    .locals 2

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInTopicsShelf()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 155
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->NONE:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 156
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->expandedShelfType:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$000(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;->TOPICS:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExpandedShelfType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private useFirstShelfLayoutIfNecessary(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "header"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->shelfHeaderAdded:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    .line 352
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->LAYOUT:I

    if-ne v0, v1, :cond_0

    .line 353
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v1, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->LAYOUT_FIRST:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 355
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->shelfHeaderAdded:Z

    .line 356
    return-void
.end method


# virtual methods
.method public exit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 161
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 187
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/datasource/CardListVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    .line 188
    return-void

    .line 164
    :sswitch_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentAppFamilySummary()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentAppSummary()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeCardSourceListItem(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 165
    .local v0, "editionCard":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 167
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0

    .line 177
    .end local v0    # "editionCard":Lcom/google/android/libraries/bind/data/Data;
    :sswitch_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->shouldShowMagazinesShelf()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeAndAddMagazinesGroup()V

    .line 184
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 179
    :cond_3
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->shouldShowNewsShelf()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 180
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeAndAddNewsGroup()V

    goto :goto_1

    .line 181
    :cond_4
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->shouldShowTopicsShelf()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 182
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeAndAddTopicsGroup()V

    goto :goto_1

    .line 161
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic exit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->exit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V

    return-void
.end method

.method protected getAnalyticsScreenName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 347
    const-string v0, "Explore - "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->topic:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$700(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;

    move-result-object v0

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreTopic;->name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected isInMagazinesShelf()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v3

    if-nez v3, :cond_1

    .line 129
    :cond_0
    :goto_0
    return v1

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfSummary()Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;->getType()I

    move-result v0

    .line 129
    .local v0, "type":I
    if-eq v0, v2, :cond_2

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method protected makeAndAddMagazinesGroup()V
    .locals 8

    .prologue
    .line 298
    new-instance v2, Lcom/google/android/libraries/bind/data/DataList;

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 300
    .local v2, "issueList":Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/play/MarketInfo;->areMagazinesAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 317
    :goto_0
    return-void

    .line 305
    :cond_0
    new-instance v3, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v3, v2}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    .line 307
    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$400(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/newsstanddev/R$integer;->num_magazine_cols:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 306
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v3

    const/4 v4, 0x1

    .line 308
    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/card/GridGroup;->setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v1

    .line 309
    .local v1, "issueGroup":Lcom/google/android/libraries/bind/card/CardGroup;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v3, :cond_1

    .line 310
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->useFirstShelfLayoutIfNecessary(Lcom/google/android/libraries/bind/data/Data;)V

    .line 311
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 312
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 314
    :cond_1
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$500(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 315
    .local v0, "groupData":Lcom/google/android/libraries/bind/data/Data;
    iget v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    const-string v4, "auto_"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->nextRowIndex()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xb

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 316
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method

.method protected makeAndAddNewsGroup()V
    .locals 9

    .prologue
    .line 246
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentNewsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 247
    new-instance v3, Lcom/google/android/libraries/bind/data/DataList;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 249
    .local v3, "sourceList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v4, v3}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    sget v5, Lcom/google/android/apps/newsstanddev/R$dimen;->extra_large_column_width:I

    .line 250
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v2

    .line 252
    .local v2, "sourceGroup":Lcom/google/android/libraries/bind/card/GridGroup;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentNewsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 253
    .local v1, "header":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->useFirstShelfLayoutIfNecessary(Lcom/google/android/libraries/bind/data/Data;)V

    .line 254
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/card/GridGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 255
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p0, v2, v1, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->limitRowsForGroupIfNecessary(Lcom/google/android/libraries/bind/card/GridGroup;Lcom/google/android/libraries/bind/data/Data;I)V

    .line 256
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentNewsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 258
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$100(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 259
    .local v0, "groupData":Lcom/google/android/libraries/bind/data/Data;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    const-string v5, "auto_"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->nextRowIndex()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xb

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 260
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 261
    return-void

    .line 246
    .end local v0    # "groupData":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "header":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "sourceGroup":Lcom/google/android/libraries/bind/card/GridGroup;
    .end local v3    # "sourceList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected makeAndAddTopicsGroup()V
    .locals 9

    .prologue
    .line 264
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentTopicsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkState(Z)V

    .line 265
    new-instance v3, Lcom/google/android/libraries/bind/data/DataList;

    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/libraries/bind/data/DataList;-><init>(ILjava/util/List;)V

    .line 267
    .local v3, "sourceList":Lcom/google/android/libraries/bind/data/DataList;
    new-instance v4, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;

    invoke-direct {v4, v3}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    sget v5, Lcom/google/android/apps/newsstanddev/R$dimen;->extra_large_column_width:I

    .line 268
    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/card/NSGridGroup;->setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v2

    .line 270
    .local v2, "sourceGroup":Lcom/google/android/libraries/bind/card/GridGroup;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentTopicsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 271
    .local v1, "header":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->useFirstShelfLayoutIfNecessary(Lcom/google/android/libraries/bind/data/Data;)V

    .line 272
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p0, v2, v1, v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->limitRowsForGroupIfNecessary(Lcom/google/android/libraries/bind/card/GridGroup;Lcom/google/android/libraries/bind/data/Data;I)V

    .line 273
    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/card/GridGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 274
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentTopicsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 276
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->cardListBuilder:Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$200(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/apps/dots/android/newsstand/card/NSCardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 277
    .local v0, "groupData":Lcom/google/android/libraries/bind/data/Data;
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    const-string v5, "auto_"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->nextRowIndex()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xb

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 278
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->addToResults(Lcom/google/android/libraries/bind/data/Data;)V

    .line 279
    return-void

    .line 264
    .end local v0    # "groupData":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "header":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "sourceGroup":Lcom/google/android/libraries/bind/card/GridGroup;
    .end local v3    # "sourceList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected makeInternationalLinkNode(Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 7
    .param p1, "exploreLinkSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->getDescription()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeCommonCardData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 223
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->primaryKey:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->nextRowIndex()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 224
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$layout;->card_international_curations_list_item:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 225
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_NAME:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 226
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_ICON_ID:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_language_32dp:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 227
    sget v2, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_BACKGROUND:I

    sget v3, Lcom/google/android/apps/newsstanddev/R$color;->card_international_curation_icon_background:I

    .line 228
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 227
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 229
    sget v2, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_USE_ROUNDED_SOURCE_ICON:I

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 230
    sget v2, Lcom/google/apps/dots/android/newsstand/widget/RoundedCacheableAttachmentView;->DK_ROUNDED_SOURCE_ICON_INSET:I

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$dimen;->card_source_list_item_topic_icon_inset:I

    .line 231
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 230
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 232
    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->DK_SOURCE_CLICKHANDLER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 242
    return-object v0
.end method

.method protected makeMagazineShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;
    .locals 5
    .param p1, "shelfSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .prologue
    .line 320
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMerchandisingShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 323
    .local v0, "card":Lcom/google/android/libraries/bind/data/Data;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->hasId()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentExploreGroupSummary()Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreGroupSummary;->getId()Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "categoryId":Ljava/lang/String;
    :goto_0
    sget v2, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_BUTTON_TEXT:I

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->this$0:Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->appContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;->access$600(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/newsstanddev/R$string;->shop_button:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 327
    sget v2, Lcom/google/apps/dots/android/newsstand/card/ShelfDetailedHeader;->DK_ON_CLICK_LISTENER:I

    new-instance v3, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;

    invoke-direct {v3, p0, v1}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor$2;-><init>(Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 338
    return-object v0

    .line 324
    .end local v1    # "categoryId":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected readingEdition()Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "exploreLinkSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInTopicsShelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeInternationalLinkNode(Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_0
    return-void
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)V
    .locals 1
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "shelfSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInNewsShelf()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMerchandisingShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentNewsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInTopicsShelf()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMerchandisingShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentTopicsShelfCard:Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInMagazinesShelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMagazineShelfCard(Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentMagazineShelfCard:Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0
.end method

.method protected visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 2
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;
    .param p2, "offerSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->isInMagazinesShelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->currentShelfItems:Ljava/util/List;

    invoke-virtual {p0, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->makeMagazineOfferCard(Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_0
    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$MerchandisingShelfSummary;)V

    return-void
.end method

.method protected bridge synthetic visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/explore/ExploreSingleTopicList$ExploreSingleTopicCardListVisitor;->visit(Lcom/google/apps/dots/android/newsstand/model/nodes/ContinuationTraverser$ContinuationTraversal;Lcom/google/apps/dots/proto/client/DotsShared$OfferSummary;)V

    return-void
.end method
