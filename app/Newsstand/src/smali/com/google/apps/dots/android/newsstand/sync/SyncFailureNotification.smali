.class public Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;
.super Ljava/lang/Object;
.source "SyncFailureNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;
    }
.end annotation


# direct methods
.method private static setNotificationShownTime(Ljava/lang/String;)V
    .locals 4
    .param p0, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setLong(Ljava/lang/String;J)V

    .line 44
    return-void
.end method

.method private static shouldShowNotification(Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;)Z
    .locals 6
    .param p0, "reason"    # Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    .prologue
    .line 38
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->prefKey:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 39
    .local v0, "lastTimeShown":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iget-wide v4, p0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->frequencyOfNotificationInMillis:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static showOutOfStorageNotification(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 65
    new-instance v5, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v5, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget v6, Lcom/google/android/apps/newsstanddev/R$drawable;->stat_notify_manage:I

    .line 66
    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    .line 67
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const/4 v6, 0x1

    .line 68
    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const-string v6, "err"

    .line 69
    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->sync_failed_notification_title:I

    .line 70
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    sget v6, Lcom/google/android/apps/newsstanddev/R$string;->sync_failed_notification_body:I

    .line 71
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 74
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->nonActivityMake(Landroid/content/Context;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/apps/dots/android/newsstand/home/HomePage;->MY_LIBRARY_PAGE:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    .line 75
    invoke-virtual {v5, v6}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomePage(Lcom/google/apps/dots/android/newsstand/home/HomePage;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v1

    .line 76
    .local v1, "intentBuilder":Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;
    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v4

    .line 78
    .local v4, "resultIntent":Landroid/content/Intent;
    invoke-static {p0, v8, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 80
    const-string v5, "notification"

    .line 81
    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 82
    .local v3, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 83
    .local v2, "notification":Landroid/app/Notification;
    iget v5, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x4

    or-int/lit8 v5, v5, 0x10

    iput v5, v2, Landroid/app/Notification;->flags:I

    .line 85
    sget-object v5, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->OUT_OF_STORAGE:Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->ordinal()I

    move-result v5

    invoke-virtual {v3, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 86
    return-void
.end method

.method public static showSyncFailedNotificationIfNecessary(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "reason"    # Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;
    .param p2, "userRequested"    # Z

    .prologue
    .line 52
    if-nez p2, :cond_0

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;->shouldShowNotification(Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 56
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$1;->$SwitchMap$com$google$apps$dots$android$newsstand$sync$SyncFailureNotification$SyncFailedReason:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 58
    :pswitch_0
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;->showOutOfStorageNotification(Landroid/content/Context;)V

    .line 59
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification$SyncFailedReason;->prefKey:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/sync/SyncFailureNotification;->setNotificationShownTime(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
