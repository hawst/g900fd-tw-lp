.class public Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "UserAwareOnPageChangeListener.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private dragIsUserDriven:Ljava/lang/Boolean;

.field private dragWasUserDriven:Z

.field private final viewPager:Landroid/support/v4/view/NSViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/view/NSViewPager;)V
    .locals 0
    .param p1, "viewPager"    # Landroid/support/v4/view/NSViewPager;

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->viewPager:Landroid/support/v4/view/NSViewPager;

    .line 21
    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 6
    .param p1, "state"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "onPageScrollStateChanged - state: %d"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 27
    packed-switch p1, :pswitch_data_0

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 29
    :pswitch_0
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->viewPager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->isFakeDragging()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragIsUserDriven:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 33
    :pswitch_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragIsUserDriven:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragIsUserDriven:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragWasUserDriven:Z

    .line 35
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragIsUserDriven:Ljava/lang/Boolean;

    goto :goto_0

    .line 27
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPageScrolled(IFI)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 43
    sget-object v0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onPageScrolled - position: %d, offset: %f, offsetPixels: %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 44
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 43
    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    sget-object v2, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v3, "onPageSelected - position: %d"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragWasUserDriven:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->viewPager:Landroid/support/v4/view/NSViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/NSViewPager;->isSettingCurrentItem()Z

    move-result v2

    if-nez v2, :cond_0

    .line 52
    .local v0, "wasUserDriven":Z
    :goto_0
    iput-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->dragWasUserDriven:Z

    .line 53
    invoke-virtual {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/UserAwareOnPageChangeListener;->onPageSelected(IZ)V

    .line 54
    return-void

    .end local v0    # "wasUserDriven":Z
    :cond_0
    move v0, v1

    .line 51
    goto :goto_0
.end method

.method public onPageSelected(IZ)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "userDriven"    # Z

    .prologue
    .line 57
    return-void
.end method
