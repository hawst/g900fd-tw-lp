.class public final enum Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;
.super Ljava/lang/Enum;
.source "HomePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/home/HomePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

.field public static final enum EXPLORE:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

.field public static final enum MY_LIBRARY:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

.field public static final enum READ_NOW:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

.field public static final enum SAVED:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    const-string v1, "READ_NOW"

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->READ_NOW:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 31
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    const-string v1, "MY_LIBRARY"

    invoke-direct {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->MY_LIBRARY:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 32
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    const-string v1, "SAVED"

    invoke-direct {v0, v1, v4}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->SAVED:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 33
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    const-string v1, "EXPLORE"

    invoke-direct {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->EXPLORE:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->READ_NOW:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->MY_LIBRARY:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->SAVED:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->EXPLORE:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->$VALUES:[Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->$VALUES:[Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    return-object v0
.end method
