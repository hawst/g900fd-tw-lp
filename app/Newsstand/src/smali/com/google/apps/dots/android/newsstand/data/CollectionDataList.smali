.class public abstract Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
.super Lcom/google/apps/dots/android/newsstand/data/EventDataList;
.source "CollectionDataList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;
    }
.end annotation


# instance fields
.field private final account:Landroid/accounts/Account;

.field private apiUri:Ljava/lang/String;

.field private currentVersion:Lcom/google/apps/dots/android/newsstand/store/Version;

.field private final currentVersionLock:Ljava/util/concurrent/locks/Lock;

.field private currentVersionUri:Ljava/lang/String;

.field private getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private getFreshTimeoutMs:J

.field private showStaleOnGetFreshTimeout:Z


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "primaryKey"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;-><init>(I)V

    .line 46
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 59
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->account:Landroid/accounts/Account;

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->setDirty(Z)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->setApiUri(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getApiUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->handleNewVersion(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshTimeoutMs:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->showStaleOnGetFreshTimeout:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Lcom/google/apps/dots/android/newsstand/store/Version;)Lcom/google/apps/dots/android/newsstand/store/Version;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersion:Lcom/google/apps/dots/android/newsstand/store/Version;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionUri:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method private checkStoreForNewVersion(Ljava/lang/String;)V
    .locals 3
    .param p1, "newVersionUri"    # Ljava/lang/String;

    .prologue
    .line 278
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    .line 279
    .local v0, "token":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->mutationStore()Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;

    invoke-direct {v2, p0, p1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$2;-><init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 286
    return-void
.end method

.method private getApiUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    const-string v0, ""

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->setApiUri(Ljava/lang/String;Z)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    return-object v0
.end method

.method private handleNewVersion(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V
    .locals 4
    .param p1, "newVersionUri"    # Ljava/lang/String;
    .param p2, "newVersion"    # Lcom/google/apps/dots/android/newsstand/store/Version;

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "Maybe handling new version: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    :try_start_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getApiUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersion:Lcom/google/apps/dots/android/newsstand/store/Version;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionUri:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersion:Lcom/google/apps/dots/android/newsstand/store/Version;

    .line 295
    invoke-virtual {p2, v0}, Lcom/google/apps/dots/android/newsstand/store/Version;->newerThan(Lcom/google/apps/dots/android/newsstand/store/Version;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->invalidateData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 307
    :cond_2
    return-void

    .line 302
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->currentVersionLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private setApiUri(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "newApiUri"    # Ljava/lang/String;
    .param p2, "invalidate"    # Z

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->removeEventUriToWatch(Landroid/net/Uri;)V

    .line 126
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    sget-object v0, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->apiUri:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->contentUri(Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->addEventUriToWatch(Landroid/net/Uri;)V

    .line 130
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "setting URI: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    if-eqz p2, :cond_2

    .line 133
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->invalidateData()V

    .line 137
    :cond_2
    return-void
.end method


# virtual methods
.method public freshen()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 94
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v2, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->freshen(ZJZ)V

    .line 95
    return-void
.end method

.method public freshen(ZJZ)V
    .locals 2
    .param p1, "clearData"    # Z
    .param p2, "timeoutMs"    # J
    .param p4, "showStaleOnTimeout"    # Z

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0, v1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 78
    :cond_0
    iput-wide p2, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshTimeoutMs:J

    .line 79
    iput-boolean p4, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->showStaleOnGetFreshTimeout:Z

    .line 80
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getFreshOnNextRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->invalidateData()V

    .line 82
    return-void
.end method

.method public invalidateApiUri(Landroid/accounts/Account;)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 106
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncUtil;->mainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$1;

    invoke-direct {v1, p0, p1}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$1;-><init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;Landroid/accounts/Account;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList$SyncRefreshTask;-><init>(Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;)V

    return-object v0
.end method

.method protected abstract onCreateApiUri(Landroid/accounts/Account;)Ljava/lang/String;
.end method

.method protected onEvent(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 8
    .param p1, "contentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .local p2, "extras":Ljava/util/Map;, "Ljava/util/Map<**>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 255
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getApiUri()Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "refreshApiUri":Ljava/lang/String;
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/provider/DatabaseConstants$NSStoreUris;->parse(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v1

    .line 257
    .local v1, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    iget-object v3, v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->id:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v3

    const-string v4, "Handling notification on collection URI: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 262
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->getVersion(Ljava/util/Map;)Lcom/google/apps/dots/android/newsstand/store/Version;

    move-result-object v2

    .line 263
    .local v2, "version":Lcom/google/apps/dots/android/newsstand/store/Version;
    if-eqz v2, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->logd()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v3

    const-string v4, "Parsed version: %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    invoke-direct {p0, v0, v2}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->handleNewVersion(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/store/Version;)V

    .line 274
    .end local v2    # "version":Lcom/google/apps/dots/android/newsstand/store/Version;
    :goto_0
    return-void

    .line 268
    .restart local v2    # "version":Lcom/google/apps/dots/android/newsstand/store/Version;
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->checkStoreForNewVersion(Ljava/lang/String;)V

    goto :goto_0

    .line 272
    .end local v2    # "version":Lcom/google/apps/dots/android/newsstand/store/Version;
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onEvent(Landroid/net/Uri;Ljava/util/Map;)V

    goto :goto_0
.end method

.method protected onRegisterForInvalidation()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onRegisterForInvalidation()V

    .line 149
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->getApiUri()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->checkStoreForNewVersion(Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onUnregisterForInvalidation()V

    .line 155
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/CollectionDataList;->setDirty(Z)V

    .line 156
    return-void
.end method

.method protected abstract processResponse(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            "Lcom/google/apps/dots/android/newsstand/async/AsyncToken;",
            "Lcom/google/apps/dots/proto/client/DotsSyncV3$Root;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end method

.method protected shouldStorePermanently()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method protected update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "newSnapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "change"    # Lcom/google/android/libraries/bind/data/DataChange;
    .param p3, "optNewDataVersion"    # Ljava/lang/Integer;

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 87
    return-void
.end method
