.class Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "NativeBodyDotsWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->notifyFinishLoading()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Float;)V
    .locals 4
    .param p1, "pageFraction"    # Ljava/lang/Float;

    .prologue
    const/4 v3, 0x1

    .line 340
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrolledToPageLocation:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->access$500(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->pagingScroller:Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->access$600(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/android/newsstand/widget/magazines/ScrollablePartView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromFraction(Ljava/lang/Float;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v1

    # invokes: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->scrollToPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V
    invoke-static {v0, v1, v3}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->access$700(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;Lcom/google/apps/dots/android/newsstand/article/PageLocation;Z)V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    # getter for: Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->articleEventHandler:Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;->access$800(Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;)Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->this$0:Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/google/apps/dots/android/newsstand/widget/ArticleEventHandler;->onArticlePageChanged(Landroid/view/View;IIZ)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 337
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/widget/magazines/NativeBodyDotsWidget$3;->onSuccess(Ljava/lang/Float;)V

    return-void
.end method
