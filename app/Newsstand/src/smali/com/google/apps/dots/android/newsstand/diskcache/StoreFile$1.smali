.class Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$1;
.super Ljava/lang/Object;
.source "StoreFile.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;->computeSplitPoint()Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
        "Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$1;->this$0:Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 147
    check-cast p1, Ljava/util/Map$Entry;

    check-cast p2, Ljava/util/Map$Entry;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/diskcache/StoreFile$1;->compare(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I

    move-result v0

    return v0
.end method

.method public compare(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;",
            ">;",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;",
            "Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "lhs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    .local p2, "rhs":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;>;"
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;->compareTo(Lcom/google/apps/dots/android/newsstand/diskcache/ByteArray;)I

    move-result v0

    return v0
.end method
