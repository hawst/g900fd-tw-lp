.class public final Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppFamilySummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;


# instance fields
.field private analyticsId_:Ljava/lang/String;

.field private appAnalyticsId_:Ljava/lang/String;

.field public appFamilyId:Ljava/lang/String;

.field private appStoreUrl_:Ljava/lang/String;

.field private bitField0_:I

.field private category_:I

.field public childId:[Ljava/lang/String;

.field public curationFlag:[I

.field private dataCollectionAskDelay_:J

.field private dataCollectionPolicy_:I

.field private description_:Ljava/lang/String;

.field private editionSourceFeedUrl_:Ljava/lang/String;

.field private heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private iconAttachmentId_:Ljava/lang/String;

.field private iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private issueType_:I

.field private leadCurationClientEntityId_:Ljava/lang/String;

.field private longShareUrl_:Ljava/lang/String;

.field private marketUrl_:Ljava/lang/String;

.field private meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

.field private name_:Ljava/lang/String;

.field private previewAttachmentId_:Ljava/lang/String;

.field private pricingLearnMoreLabel_:Ljava/lang/String;

.field private pricingLearnMoreUrl_:Ljava/lang/String;

.field private pricingModel_:I

.field private privacyPolicy_:Ljava/lang/String;

.field private shortShareUrl_:Ljava/lang/String;

.field private storeType_:I

.field private untranslatedAppFamilyId_:Ljava/lang/String;

.field private updateTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6786
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 6787
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 6797
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 6800
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6822
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    .line 6825
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    .line 6844
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    .line 6866
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    .line 6888
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    .line 6907
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    .line 6967
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    .line 6989
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    .line 7011
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    .line 7033
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    .line 7055
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    .line 7077
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    .line 7099
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    .line 7121
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    .line 7140
    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    .line 7159
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    .line 7181
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    .line 7200
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    .line 7219
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    .line 7257
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    .line 7279
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    .line 7301
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    .line 7304
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 7326
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    .line 6787
    return-void
.end method


# virtual methods
.method public clearUntranslatedAppFamilyId()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1

    .prologue
    .line 6816
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6817
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    .line 6818
    return-object p0
.end method

.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 3

    .prologue
    .line 7386
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7390
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 7391
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    .line 7393
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_1

    .line 7394
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7396
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_2

    .line 7397
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7399
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v2, :cond_3

    .line 7400
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->clone()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 7402
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v2, v2

    if-lez v2, :cond_4

    .line 7403
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    .line 7405
    :cond_4
    return-object v0

    .line 7387
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :catch_0
    move-exception v1

    .line 7388
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6783
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 7410
    if-ne p1, p0, :cond_1

    .line 7441
    :cond_0
    :goto_0
    return v1

    .line 7411
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 7412
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 7413
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7414
    :goto_2
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    .line 7415
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7417
    :goto_3
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7418
    :goto_4
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7420
    :goto_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_9

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 7421
    :goto_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_3

    .line 7422
    :goto_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_b

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7423
    :goto_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7424
    :goto_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_d

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7425
    :goto_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7426
    :goto_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    if-nez v3, :cond_f

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7427
    :goto_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7428
    :goto_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7429
    :goto_e
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7432
    :goto_f
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_13

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v3, :cond_3

    .line 7436
    :goto_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7437
    :goto_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    if-nez v3, :cond_15

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7438
    :goto_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    .line 7439
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v3, :cond_16

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 7440
    :goto_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    if-nez v3, :cond_17

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 7441
    goto/16 :goto_0

    .line 7413
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 7414
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2

    .line 7415
    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    .line 7417
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    .line 7418
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    .line 7420
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7421
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7422
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    .line 7423
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    .line 7424
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    .line 7425
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    .line 7426
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    .line 7427
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    .line 7428
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    .line 7429
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    .line 7432
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 7436
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    .line 7437
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    .line 7438
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_12

    .line 7439
    :cond_16
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 7440
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_13

    :cond_17
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    .line 7441
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0
.end method

.method public getAppAnalyticsId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7101
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 6890
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6868
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeroShotImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 6950
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getIconAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6909
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 6931
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getLeadCurationClientEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7306
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLongShareUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7013
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getMeteredPolicy()Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;
    .locals 1

    .prologue
    .line 7240
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6846
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviewAttachmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6969
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getPricingLearnMoreLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7281
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    return-object v0
.end method

.method public getPricingLearnMoreUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7259
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 7584
    const/4 v2, 0x0

    .line 7585
    .local v2, "size":I
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 7586
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7587
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_1

    .line 7588
    const/4 v0, 0x0

    .line 7589
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v1, v5, v4

    .line 7591
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 7589
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 7593
    .end local v1    # "element":Ljava/lang/String;
    :cond_0
    add-int/2addr v2, v0

    .line 7594
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 7596
    .end local v0    # "dataSize":I
    :cond_1
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_2

    .line 7597
    const/4 v4, 0x3

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    .line 7598
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 7600
    :cond_2
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_3

    .line 7601
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    .line 7602
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7604
    :cond_3
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_4

    .line 7605
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    .line 7606
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7608
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-eqz v4, :cond_5

    .line 7609
    const/4 v4, 0x6

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    .line 7610
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 7612
    :cond_5
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_6

    .line 7613
    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    .line 7614
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7616
    :cond_6
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_7

    .line 7617
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    .line 7618
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7620
    :cond_7
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_8

    .line 7621
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    .line 7622
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7624
    :cond_8
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_9

    .line 7625
    const/16 v4, 0xa

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    .line 7626
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7628
    :cond_9
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_a

    .line 7629
    const/16 v4, 0xb

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    .line 7630
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7632
    :cond_a
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_b

    .line 7633
    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    .line 7634
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7636
    :cond_b
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_c

    .line 7637
    const/16 v4, 0xd

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    .line 7638
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7640
    :cond_c
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_d

    .line 7641
    const/16 v4, 0xe

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    .line 7642
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 7644
    :cond_d
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    if-eqz v4, :cond_e

    .line 7645
    const/16 v4, 0xf

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    .line 7646
    invoke-static {v4, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    .line 7648
    :cond_e
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    if-eqz v4, :cond_f

    .line 7649
    const/16 v4, 0x10

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    .line 7650
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7652
    :cond_f
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_10

    .line 7653
    const/16 v4, 0x11

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    .line 7654
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 7656
    :cond_10
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_11

    .line 7657
    const/16 v4, 0x13

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    .line 7658
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7660
    :cond_11
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    if-eqz v4, :cond_12

    .line 7661
    const/16 v4, 0x14

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    .line 7662
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 7664
    :cond_12
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    if-eqz v4, :cond_13

    .line 7665
    const/16 v4, 0x15

    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    .line 7666
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v2, v4

    .line 7668
    :cond_13
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v4, :cond_14

    .line 7669
    const/16 v4, 0x16

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 7670
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7672
    :cond_14
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v4, :cond_15

    .line 7673
    const/16 v4, 0x17

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7674
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7676
    :cond_15
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v5, 0x80000

    and-int/2addr v4, v5

    if-eqz v4, :cond_16

    .line 7677
    const/16 v4, 0x18

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    .line 7678
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7680
    :cond_16
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v5, 0x100000

    and-int/2addr v4, v5

    if-eqz v4, :cond_17

    .line 7681
    const/16 v4, 0x19

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    .line 7682
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7684
    :cond_17
    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_18

    .line 7685
    const/16 v4, 0x1a

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 7686
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7688
    :cond_18
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    if-eqz v4, :cond_1a

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v4, v4

    if-lez v4, :cond_1a

    .line 7689
    const/4 v0, 0x0

    .line 7690
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v5, v4

    :goto_1
    if-ge v3, v5, :cond_19

    aget v1, v4, v3

    .line 7692
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 7690
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 7694
    .end local v1    # "element":I
    :cond_19
    add-int/2addr v2, v0

    .line 7695
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 7697
    .end local v0    # "dataSize":I
    :cond_1a
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v4, 0x200000

    and-int/2addr v3, v4

    if-eqz v3, :cond_1b

    .line 7698
    const/16 v3, 0x1c

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 7699
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7701
    :cond_1b
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_1c

    .line 7702
    const/16 v3, 0x1d

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7703
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7705
    :cond_1c
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v4, 0x400000

    and-int/2addr v3, v4

    if-eqz v3, :cond_1d

    .line 7706
    const/16 v3, 0x1e

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    .line 7707
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7709
    :cond_1d
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->cachedSize:I

    .line 7710
    return v2
.end method

.method public getShortShareUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6991
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getStoreType()I
    .locals 1

    .prologue
    .line 7202
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 6827
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    return-wide v0
.end method

.method public hasHeroShotImage()Z
    .locals 1

    .prologue
    .line 6960
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeadCurationClientEntityId()Z
    .locals 2

    .prologue
    .line 7317
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLongShareUrl()Z
    .locals 1

    .prologue
    .line 7024
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMeteredPolicy()Z
    .locals 1

    .prologue
    .line 7250
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPricingLearnMoreUrl()Z
    .locals 2

    .prologue
    .line 7270
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShortShareUrl()Z
    .locals 1

    .prologue
    .line 7002
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUntranslatedAppFamilyId()Z
    .locals 1

    .prologue
    .line 6813
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v3, 0x0

    .line 7445
    const/16 v1, 0x11

    .line 7446
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 7447
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 7448
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 7449
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    if-nez v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    .line 7455
    :cond_0
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 7456
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 7457
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    .line 7458
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    add-int v1, v2, v4

    .line 7459
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_8

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    .line 7460
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_9

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    .line 7461
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_a

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    .line 7462
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    .line 7463
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    if-nez v2, :cond_c

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    .line 7464
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    if-nez v2, :cond_d

    move v2, v3

    :goto_9
    add-int v1, v4, v2

    .line 7465
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    if-nez v2, :cond_e

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    .line 7466
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    if-nez v2, :cond_f

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    .line 7467
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    if-nez v2, :cond_10

    move v2, v3

    :goto_c
    add-int v1, v4, v2

    .line 7468
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    if-nez v2, :cond_11

    move v2, v3

    :goto_d
    add-int v1, v4, v2

    .line 7469
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    add-int v1, v2, v4

    .line 7470
    mul-int/lit8 v2, v1, 0x1f

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    iget-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v1, v2, v4

    .line 7471
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    if-nez v2, :cond_12

    move v2, v3

    :goto_e
    add-int v1, v4, v2

    .line 7472
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    add-int v1, v2, v4

    .line 7473
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    add-int v1, v2, v4

    .line 7474
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    add-int v1, v2, v4

    .line 7475
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v2, :cond_13

    move v2, v3

    :goto_f
    add-int v1, v4, v2

    .line 7476
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    if-nez v2, :cond_14

    move v2, v3

    :goto_10
    add-int v1, v4, v2

    .line 7477
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    if-nez v2, :cond_15

    move v2, v3

    :goto_11
    add-int v1, v4, v2

    .line 7478
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    if-nez v2, :cond_16

    mul-int/lit8 v1, v1, 0x1f

    .line 7484
    :cond_1
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    if-nez v2, :cond_17

    move v2, v3

    :goto_12
    add-int v1, v4, v2

    .line 7485
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    if-nez v4, :cond_18

    :goto_13
    add-int v1, v2, v3

    .line 7486
    return v1

    .line 7447
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_0

    .line 7448
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_1

    .line 7451
    :cond_4
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 7452
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v3

    :goto_15
    add-int v1, v4, v2

    .line 7451
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 7452
    :cond_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_15

    .line 7456
    .end local v0    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_2

    .line 7457
    :cond_7
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_3

    .line 7459
    :cond_8
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_4

    .line 7460
    :cond_9
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_5

    .line 7461
    :cond_a
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto/16 :goto_6

    .line 7462
    :cond_b
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_7

    .line 7463
    :cond_c
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_8

    .line 7464
    :cond_d
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_9

    .line 7465
    :cond_e
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_a

    .line 7466
    :cond_f
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_b

    .line 7467
    :cond_10
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_c

    .line 7468
    :cond_11
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_d

    .line 7471
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_e

    .line 7475
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->hashCode()I

    move-result v2

    goto/16 :goto_f

    .line 7476
    :cond_14
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_10

    .line 7477
    :cond_15
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_11

    .line 7480
    :cond_16
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_16
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 7481
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    aget v4, v4, v0

    add-int v1, v2, v4

    .line 7480
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    .line 7484
    .end local v0    # "i":I
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto/16 :goto_12

    .line 7485
    :cond_18
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto/16 :goto_13
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 13
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v12, 0x12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 7718
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 7719
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 7723
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 7724
    :sswitch_0
    return-object p0

    .line 7729
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    goto :goto_0

    .line 7733
    :sswitch_2
    invoke-static {p1, v12}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 7734
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v1, v5

    .line 7735
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 7736
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7737
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    .line 7738
    :goto_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1

    .line 7739
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 7740
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 7738
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7743
    :cond_1
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto :goto_0

    .line 7747
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    .line 7748
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto :goto_0

    .line 7752
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    .line 7753
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto :goto_0

    .line 7757
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    .line 7758
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto :goto_0

    .line 7762
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 7763
    .local v4, "temp":I
    if-eqz v4, :cond_2

    if-eq v4, v9, :cond_2

    if-eq v4, v10, :cond_2

    if-eq v4, v11, :cond_2

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_2

    const/4 v5, 0x6

    if-eq v4, v5, :cond_2

    const/16 v5, 0xa

    if-eq v4, v5, :cond_2

    const/16 v5, 0x23

    if-eq v4, v5, :cond_2

    const/16 v5, 0xd

    if-eq v4, v5, :cond_2

    const/4 v5, 0x7

    if-eq v4, v5, :cond_2

    const/16 v5, 0x8

    if-eq v4, v5, :cond_2

    const/16 v5, 0xb

    if-eq v4, v5, :cond_2

    const/16 v5, 0xc

    if-eq v4, v5, :cond_2

    const/16 v5, 0x9

    if-eq v4, v5, :cond_2

    const/16 v5, 0xe

    if-eq v4, v5, :cond_2

    const/16 v5, 0xf

    if-eq v4, v5, :cond_2

    const/16 v5, 0x10

    if-eq v4, v5, :cond_2

    const/16 v5, 0x11

    if-eq v4, v5, :cond_2

    if-eq v4, v12, :cond_2

    const/16 v5, 0x13

    if-eq v4, v5, :cond_2

    const/16 v5, 0x14

    if-eq v4, v5, :cond_2

    const/16 v5, 0x15

    if-eq v4, v5, :cond_2

    const/16 v5, 0x16

    if-eq v4, v5, :cond_2

    const/16 v5, 0x17

    if-eq v4, v5, :cond_2

    const/16 v5, 0x18

    if-eq v4, v5, :cond_2

    const/16 v5, 0x19

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1a

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1b

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1c

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1d

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1e

    if-eq v4, v5, :cond_2

    const/16 v5, 0x1f

    if-eq v4, v5, :cond_2

    const/16 v5, 0x20

    if-eq v4, v5, :cond_2

    const/16 v5, 0x21

    if-eq v4, v5, :cond_2

    const/16 v5, 0x22

    if-eq v4, v5, :cond_2

    const/16 v5, 0x24

    if-eq v4, v5, :cond_2

    const/16 v5, 0x25

    if-ne v4, v5, :cond_3

    .line 7801
    :cond_2
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    .line 7802
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x10

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7804
    :cond_3
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    goto/16 :goto_0

    .line 7809
    .end local v4    # "temp":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    .line 7810
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x20

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7814
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    .line 7815
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x40

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7819
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    .line 7820
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x80

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7824
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    .line 7825
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x100

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7829
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    .line 7830
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x200

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7834
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    .line 7835
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x400

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7839
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    .line 7840
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x800

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7844
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 7845
    .restart local v4    # "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v9, :cond_4

    if-ne v4, v10, :cond_5

    .line 7848
    :cond_4
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    .line 7849
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x2000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7851
    :cond_5
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    goto/16 :goto_0

    .line 7856
    .end local v4    # "temp":I
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    .line 7857
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x4000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7861
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    .line 7862
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const v6, 0x8000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7866
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 7867
    .restart local v4    # "temp":I
    if-eqz v4, :cond_6

    if-ne v4, v9, :cond_7

    .line 7869
    :cond_6
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    .line 7870
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x10000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7872
    :cond_7
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    goto/16 :goto_0

    .line 7877
    .end local v4    # "temp":I
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    .line 7878
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v5, v5, 0x1000

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7882
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 7883
    .restart local v4    # "temp":I
    if-eqz v4, :cond_8

    if-eq v4, v9, :cond_8

    if-ne v4, v10, :cond_9

    .line 7886
    :cond_8
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    .line 7887
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x20000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7889
    :cond_9
    iput v8, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    goto/16 :goto_0

    .line 7894
    .end local v4    # "temp":I
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 7895
    .restart local v4    # "temp":I
    if-eq v4, v9, :cond_a

    if-eq v4, v10, :cond_a

    if-ne v4, v11, :cond_b

    .line 7898
    :cond_a
    iput v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    .line 7899
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x40000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7901
    :cond_b
    iput v9, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    goto/16 :goto_0

    .line 7906
    .end local v4    # "temp":I
    :sswitch_15
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-nez v5, :cond_c

    .line 7907
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .line 7909
    :cond_c
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 7913
    :sswitch_16
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_d

    .line 7914
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7916
    :cond_d
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 7920
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    .line 7921
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x80000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7925
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    .line 7926
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x100000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7930
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 7931
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7935
    :sswitch_1a
    const/16 v5, 0xd8

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 7936
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v1, v5

    .line 7937
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 7938
    .local v2, "newArray":[I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    invoke-static {v5, v8, v2, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7939
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    .line 7940
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_e

    .line 7941
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 7942
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 7940
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7945
    :cond_e
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 7949
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    .line 7950
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x200000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7954
    :sswitch_1c
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v5, :cond_f

    .line 7955
    new-instance v5, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v5}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 7957
    :cond_f
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 7961
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    .line 7962
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v6, 0x400000

    or-int/2addr v5, v6

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    goto/16 :goto_0

    .line 7719
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa0 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc2 -> :sswitch_17
        0xca -> :sswitch_18
        0xd2 -> :sswitch_19
        0xd8 -> :sswitch_1a
        0xe2 -> :sswitch_1b
        0xea -> :sswitch_1c
        0xf2 -> :sswitch_1d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6783
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v0

    return-object v0
.end method

.method public setLongShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 7016
    if-nez p1, :cond_0

    .line 7017
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7019
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    .line 7020
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    .line 7021
    return-object p0
.end method

.method public setShortShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6994
    if-nez p1, :cond_0

    .line 6995
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6997
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    .line 6998
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    .line 6999
    return-object p0
.end method

.method public setUntranslatedAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 6805
    if-nez p1, :cond_0

    .line 6806
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6808
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    .line 6809
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    .line 6810
    return-object p0
.end method

.method public setUpdateTime(J)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 6830
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    .line 6831
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    .line 6832
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 7491
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7492
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 7493
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 7494
    .local v0, "element":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7493
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 7497
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 7498
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->updateTime_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 7500
    :cond_1
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 7501
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->name_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7503
    :cond_2
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 7504
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->description_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7506
    :cond_3
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 7507
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->category_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7509
    :cond_4
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_5

    .line 7510
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7512
    :cond_5
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_6

    .line 7513
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->previewAttachmentId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7515
    :cond_6
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_7

    .line 7516
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->shortShareUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7518
    :cond_7
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_8

    .line 7519
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->longShareUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7521
    :cond_8
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_9

    .line 7522
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->marketUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7524
    :cond_9
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_a

    .line 7525
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appStoreUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7527
    :cond_a
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_b

    .line 7528
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->analyticsId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7530
    :cond_b
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    if-eqz v2, :cond_c

    .line 7531
    const/16 v2, 0xe

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionPolicy_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7533
    :cond_c
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    if-eqz v2, :cond_d

    .line 7534
    const/16 v2, 0xf

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->dataCollectionAskDelay_:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 7536
    :cond_d
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const v3, 0x8000

    and-int/2addr v2, v3

    if-eqz v2, :cond_e

    .line 7537
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->privacyPolicy_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7539
    :cond_e
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-eqz v2, :cond_f

    .line 7540
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->issueType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7542
    :cond_f
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    if-eqz v2, :cond_10

    .line 7543
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appAnalyticsId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7545
    :cond_10
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    if-eqz v2, :cond_11

    .line 7546
    const/16 v2, 0x14

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->storeType_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7548
    :cond_11
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    if-eqz v2, :cond_12

    .line 7549
    const/16 v2, 0x15

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingModel_:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7551
    :cond_12
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    if-eqz v2, :cond_13

    .line 7552
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->meteredPolicy_:Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 7554
    :cond_13
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_14

    .line 7555
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->iconImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 7557
    :cond_14
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    if-eqz v2, :cond_15

    .line 7558
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreUrl_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7560
    :cond_15
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    if-eqz v2, :cond_16

    .line 7561
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->pricingLearnMoreLabel_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7563
    :cond_16
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_17

    .line 7564
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->untranslatedAppFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7566
    :cond_17
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v2, v2

    if-lez v2, :cond_18

    .line 7567
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->curationFlag:[I

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_18

    aget v0, v2, v1

    .line 7568
    .local v0, "element":I
    const/16 v4, 0x1b

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 7567
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7571
    .end local v0    # "element":I
    :cond_18
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v2, 0x200000

    and-int/2addr v1, v2

    if-eqz v1, :cond_19

    .line 7572
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->leadCurationClientEntityId_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7574
    :cond_19
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_1a

    .line 7575
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->heroShotImage_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 7577
    :cond_1a
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->bitField0_:I

    const/high16 v2, 0x400000

    and-int/2addr v1, v2

    if-eqz v1, :cond_1b

    .line 7578
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->editionSourceFeedUrl_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 7580
    :cond_1b
    return-void
.end method
