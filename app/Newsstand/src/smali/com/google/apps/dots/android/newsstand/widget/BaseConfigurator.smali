.class public abstract Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "BaseConfigurator.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getHeaderBottomMargin()I
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->header_bottom_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderShadowMode()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeroAnimationMode()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x2

    return v0
.end method

.method protected getTabAccessibilityMode()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method
