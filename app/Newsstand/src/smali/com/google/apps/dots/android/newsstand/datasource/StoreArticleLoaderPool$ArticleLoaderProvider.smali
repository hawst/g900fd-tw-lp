.class public Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;
.super Ljava/lang/Object;
.source "StoreArticleLoaderPool.java"

# interfaces
.implements Lcom/google/android/libraries/bind/util/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArticleLoaderProvider"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/bind/util/Provider",
        "<",
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final appId:Ljava/lang/String;

.field private final isMagazine:Z

.field private final postId:Ljava/lang/String;

.field private final sectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "isMagazine"    # Z
    .param p2, "postId"    # Ljava/lang/String;
    .param p3, "sectionId"    # Ljava/lang/String;
    .param p4, "appId"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->postId:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->sectionId:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->appId:Ljava/lang/String;

    .line 86
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->isMagazine:Z

    .line 87
    return-void
.end method


# virtual methods
.method public get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;
    .locals 4

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->isMagazine:Z

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->postId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->sectionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->appId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool;->getArticleLoader(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoaderPool$ArticleLoaderProvider;->get()Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    move-result-object v0

    return-object v0
.end method
