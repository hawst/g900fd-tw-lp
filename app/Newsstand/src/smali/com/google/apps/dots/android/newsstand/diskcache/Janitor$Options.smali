.class public Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
.super Ljava/lang/Object;
.source "Janitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/Janitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Options"
.end annotation


# instance fields
.field compactFreeSpaceAggressiveThreshold:J

.field compactMinUtilization:F

.field compactMinUtilizationAggressive:F

.field compactWastedBytes:J

.field janitorDelayMs:J

.field lruEnabled:Z

.field lruMaxSize:J

.field lruMinAgeMs:J

.field lruTargetFsHeadroom:J

.field openFileTimeoutMs:J

.field splitMaxBlobs:I

.field splitMaxSize:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->janitorDelayMs:J

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruEnabled:Z

    .line 41
    const-wide/32 v0, 0x3200000

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruTargetFsHeadroom:J

    .line 43
    const-wide/32 v0, 0x1f400000

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruMaxSize:J

    .line 48
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->lruMinAgeMs:J

    .line 51
    const-wide/32 v0, 0x1900000

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->splitMaxSize:J

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->splitMaxBlobs:I

    .line 59
    const-wide/32 v0, 0x1400000

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactFreeSpaceAggressiveThreshold:J

    .line 61
    const-wide/32 v0, 0x100000

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactWastedBytes:J

    .line 63
    const v0, 0x3f28f5c3    # 0.66f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactMinUtilization:F

    .line 65
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->compactMinUtilizationAggressive:F

    .line 68
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;->openFileTimeoutMs:J

    return-void
.end method

.method public static getDefault()Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/diskcache/Janitor$Options;-><init>()V

    return-object v0
.end method
