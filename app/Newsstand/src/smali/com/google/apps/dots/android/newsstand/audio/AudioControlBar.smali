.class public Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
.super Landroid/widget/FrameLayout;
.source "AudioControlBar.java"


# instance fields
.field private final animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

.field private audioPlayerView:Landroid/widget/FrameLayout;

.field private final audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

.field private autoHide:Z

.field private autoHideDuration:J

.field private durationText:Landroid/widget/TextView;

.field private fadeOutThumb:Landroid/animation/ObjectAnimator;

.field private favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

.field private final hideRunnable:Ljava/lang/Runnable;

.field private final hideThumbRunnable:Ljava/lang/Runnable;

.field private interactionRunnable:Ljava/lang/Runnable;

.field private isPlaying:Z

.field private isShowing:Z

.field private isTrackingTouch:Z

.field private owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private playButton:Landroid/widget/ImageButton;

.field private final postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

.field private preparing:Landroid/widget/ProgressBar;

.field private progressText:Landroid/widget/TextView;

.field private readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private seekBar:Landroid/widget/SeekBar;

.field private status:I

.field private titleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    iput-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHide:Z

    .line 64
    iput v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->status:I

    .line 69
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 71
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->user()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 83
    const-wide/16 v6, 0x1388

    iput-wide v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHideDuration:J

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 98
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->audio_player:I

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    .line 100
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->title:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->titleText:Landroid/widget/TextView;

    .line 101
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->progress_text:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->progressText:Landroid/widget/TextView;

    .line 102
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->duration_text:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->durationText:Landroid/widget/TextView;

    .line 103
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->favicon:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 105
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->story_info_overlay:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;

    invoke-direct {v5, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$1;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    .line 106
    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->seekBar:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    .line 120
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->preparing:I

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->preparing:Landroid/widget/ProgressBar;

    .line 122
    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$2;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$2;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    .line 128
    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$3;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$3;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    .line 135
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v2, v5, :cond_0

    .line 136
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const-string v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-static {v2, v5, v6}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 137
    .local v0, "fadeOut":Landroid/animation/ObjectAnimator;
    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$4;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$4;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 143
    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 144
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->fadeOutThumb:Landroid/animation/ObjectAnimator;

    .line 147
    .end local v0    # "fadeOut":Landroid/animation/ObjectAnimator;
    :cond_0
    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$5;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$5;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    iput-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideThumbRunnable:Ljava/lang/Runnable;

    .line 156
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioPlayerView:Landroid/widget/FrameLayout;

    invoke-direct {p0, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setupButtons(Landroid/view/View;)V

    .line 157
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setupSeekBar()V

    .line 159
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isShowing:Z

    .line 160
    return-void

    :cond_1
    move v2, v4

    .line 159
    goto :goto_0

    .line 136
    nop

    :array_0
    .array-data 4
        0xff
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideThumbRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->progressText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->evaluateAudioState(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/async/AsyncScope;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->fadeOutThumb:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->titleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->favicon:Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isPlaying:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isTrackingTouch:Z

    return p1
.end method

.method private cancelAutoHide()V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 372
    return-void
.end method

.method private evaluateAudioState(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 438
    const-string v4, "audio_item"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 439
    const-string v4, "audio_item"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 440
    .local v0, "audio":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    const-string v4, "reading_edition"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 441
    .local v3, "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    const-string v4, "owning_edition"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 442
    .local v2, "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-direct {p0, v0, v3, v2}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updateInfo(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 446
    .end local v0    # "audio":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .end local v2    # "owningEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .end local v3    # "readingEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    const-string v4, "duration"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 447
    const-string v4, "duration"

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updateDuration(I)V

    .line 451
    :cond_1
    const-string v4, "progress"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 452
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isTrackingTouch:Z

    if-nez v4, :cond_2

    .line 453
    const-string v4, "progress"

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updateProgress(I)V

    .line 458
    :cond_2
    const-string v4, "status"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 459
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->status:I

    .line 460
    .local v1, "oldStatus":I
    const-string v4, "status"

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->status:I

    .line 461
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->status:I

    if-ne v4, v1, :cond_4

    .line 501
    .end local v1    # "oldStatus":I
    :cond_3
    :goto_0
    :pswitch_0
    return-void

    .line 464
    .restart local v1    # "oldStatus":I
    :cond_4
    iget v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->status:I

    packed-switch v4, :pswitch_data_0

    .line 497
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->resetControls()V

    .line 498
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hide()V

    goto :goto_0

    .line 466
    :pswitch_1
    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setEnabled(Z)V

    .line 467
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 468
    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updatePlayPauseButton(Z)V

    .line 469
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->scheduleAutoHideIfNeeded()V

    goto :goto_0

    .line 472
    :pswitch_2
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setEnabled(Z)V

    .line 473
    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updatePlayPauseButton(Z)V

    .line 474
    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHide:Z

    if-eqz v4, :cond_3

    .line 475
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show()V

    .line 476
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_5

    .line 477
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 479
    :cond_5
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideThumbRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 483
    :pswitch_3
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setEnabled(Z)V

    .line 484
    iput-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isPlaying:Z

    .line 485
    invoke-direct {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updatePlayPauseButton(Z)V

    goto :goto_0

    .line 488
    :pswitch_4
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setEnabled(Z)V

    .line 489
    iput-boolean v7, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isPlaying:Z

    .line 490
    invoke-direct {p0, v7}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->updatePlayPauseButton(Z)V

    goto :goto_0

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private resetControls()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->preparing:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 336
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    const/16 v1, 0x4b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 338
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 339
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->progressText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->durationText:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    return-void
.end method

.method private scheduleAutoHideIfNeeded()V
    .locals 6

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHide:Z

    if-nez v0, :cond_0

    .line 368
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->cancelAutoHide()V

    .line 366
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHideDuration:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->autoHideDuration:J

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x1388

    goto :goto_1
.end method

.method private setupButtons(Landroid/view/View;)V
    .locals 3
    .param p1, "audioPlayerView"    # Landroid/view/View;

    .prologue
    .line 247
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->play_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    .line 248
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    const/16 v2, 0x4b

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 249
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$8;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 261
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->cancel_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 262
    .local v0, "cancelButton":Landroid/widget/ImageButton;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$9;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$9;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    return-void
.end method

.method private setupSeekBar()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$10;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 299
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 302
    :cond_0
    return-void
.end method

.method private show(Z)V
    .locals 8
    .param p1, "show"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 396
    iget-boolean v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isShowing:Z

    if-ne p1, v6, :cond_0

    .line 428
    :goto_0
    return-void

    .line 399
    :cond_0
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->isShowing:Z

    .line 400
    if-eqz p1, :cond_3

    .line 401
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setVisibility(I)V

    .line 402
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->scheduleAutoHideIfNeeded()V

    .line 406
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 407
    .local v1, "animationHeight":F
    if-eqz p1, :cond_4

    move v4, v1

    .line 408
    .local v4, "startY":F
    :goto_2
    sub-float v3, v1, v4

    .line 409
    .local v3, "endY":F
    if-eqz p1, :cond_1

    move v0, v5

    .line 410
    .local v0, "alpha":F
    :cond_1
    sub-float/2addr v5, v0

    invoke-virtual {p0, v5}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setAlpha(F)V

    .line 411
    invoke-virtual {p0, v4}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->setTranslationY(F)V

    .line 412
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 413
    .local v2, "animator":Landroid/view/ViewPropertyAnimator;
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 414
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 415
    invoke-virtual {v5, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 416
    invoke-virtual {v5, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 417
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v5, v6, :cond_2

    .line 418
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 420
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v5

    new-instance v6, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$11;

    invoke-direct {v6, p0, p1}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$11;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;Z)V

    invoke-static {v5, v2, v6}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/view/ViewPropertyAnimator;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 404
    .end local v0    # "alpha":F
    .end local v1    # "animationHeight":F
    .end local v2    # "animator":Landroid/view/ViewPropertyAnimator;
    .end local v3    # "endY":F
    .end local v4    # "startY":F
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v6}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v6

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    .restart local v1    # "animationHeight":F
    :cond_4
    move v4, v0

    .line 407
    goto :goto_2
.end method

.method private updateDuration(I)V
    .locals 4
    .param p1, "durationMs"    # I

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->durationText:Landroid/widget/TextView;

    div-int/lit16 v1, p1, 0x3e8

    int-to-long v2, v1

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 352
    return-void
.end method

.method private updateInfo(Lcom/google/apps/dots/android/newsstand/media/MediaItem;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V
    .locals 4
    .param p1, "audioItem"    # Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    .param p2, "readingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "owningEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 196
    invoke-static {v0, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 197
    invoke-static {v0, p3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 201
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 202
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->owningEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 204
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioItem:Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->restart()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 210
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->postStore()Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v2

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/PostStore;->get(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$6;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 225
    if-eqz p3, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar$7;-><init>(Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private updatePlayPauseButton(Z)V
    .locals 3
    .param p1, "isPlaying"    # Z

    .prologue
    .line 355
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_pause_32dp:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 357
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    .line 358
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->media_pause:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 359
    return-void

    .line 355
    :cond_0
    sget v0, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_play_arrow_32dp:I

    goto :goto_0

    .line 358
    :cond_1
    sget v0, Lcom/google/android/apps/newsstanddev/R$string;->media_play:I

    goto :goto_1
.end method

.method private updateProgress(I)V
    .locals 1
    .param p1, "progressMs"    # I

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 346
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show(Z)V

    .line 392
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 165
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->register(Landroid/content/Context;)V

    .line 166
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 171
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->postScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->stop()Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    .line 172
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->audioReceiver:Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/audio/AudioReceiver;->unregister(Landroid/content/Context;)V

    .line 173
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 174
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 179
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->scheduleAutoHideIfNeeded()V

    .line 180
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->interactionRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->interactionRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 183
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 376
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 383
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 378
    :pswitch_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->animationScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->hideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 381
    :pswitch_1
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->scheduleAutoHideIfNeeded()V

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setEnabled(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 307
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 308
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 309
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->playButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->preparing:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 312
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 313
    return-void

    .line 308
    :cond_0
    const/16 v0, 0x4b

    goto :goto_0

    :cond_1
    move v0, v2

    .line 309
    goto :goto_1

    :cond_2
    move v2, v1

    .line 310
    goto :goto_2
.end method

.method public show()V
    .locals 1

    .prologue
    .line 387
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/audio/AudioControlBar;->show(Z)V

    .line 388
    return-void
.end method
