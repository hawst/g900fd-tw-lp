.class Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;
.super Ljava/lang/Object;
.source "OnboardAuthPage.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->setAuthFuture(Lcom/google/common/util/concurrent/ListenableFuture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 101
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Auth flow failed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    instance-of v1, p1, Ljava/util/concurrent/CancellationException;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->onAuthFailure(Z)V
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$200(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;Z)V

    .line 103
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 95
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$000()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Auth flow succeeded"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;

    # invokes: Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->onAuthSuccess()V
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;->access$100(Lcom/google/apps/dots/android/newsstand/onboard/OnboardAuthPage;)V

    .line 97
    return-void
.end method
