.class Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;
.super Lcom/google/apps/dots/android/newsstand/async/Task;
.source "MutationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/MutationStore;->upload(Ljava/io/File;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/Task",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

.field final synthetic val$mutLogFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/util/concurrent/Executor;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/MutationStore;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/async/Task;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public call()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v9, 0x1

    const/4 v0, 0x0

    .line 271
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->getMutationLog(Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    invoke-static {v1, v8}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$300(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Ljava/io/File;)Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;

    move-result-object v3

    .line 272
    .local v3, "mutationLog":Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;
    if-nez v3, :cond_0

    .line 273
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v8, "%s: nothing to upload"

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    aput-object v10, v9, v0

    invoke-virtual {v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 307
    :goto_0
    return-object v0

    .line 277
    :cond_0
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getBatchEndpointUri()Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "batchUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # invokes: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->isBadMutationLog(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z
    invoke-static {v1, v3}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$900(Lcom/google/apps/dots/android/newsstand/store/MutationStore;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v8, "%s: ignoring bad mutation log"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v0

    invoke-virtual {v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->val$mutLogFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 282
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->retryPolicy:Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$1000(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/apps/dots/android/newsstand/store/MutationRetryPolicy;->isReadyToRetry(Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 286
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$500()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v1

    const-string v8, "%s: not ready to retry"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, v0

    invoke-virtual {v1, v8, v9}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    invoke-static {v11}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_2
    new-instance v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientActionLog;

    invoke-direct {v7}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientActionLog;-><init>()V

    .line 292
    .local v7, "postedLog":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientActionLog;
    iget-object v1, v3, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    array-length v8, v1

    :goto_1
    if-ge v0, v8, :cond_3

    aget-object v6, v1, v0

    .line 293
    .local v6, "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;->clearSimulationHint()Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 292
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 296
    .end local v6    # "action":Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;
    :cond_3
    iget-object v0, v3, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->action:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    iput-object v0, v7, Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientActionLog;->actions:[Lcom/google/apps/dots/proto/client/DotsSyncV3$ClientAction;

    .line 298
    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->COLLECTION_ROOT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    invoke-direct {v0, v2, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;)V

    .line 299
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->freshVersion()Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    .line 300
    invoke-static {v7}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->postData([B)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;->ASAP:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;

    .line 303
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->priority(Lcom/google/apps/dots/android/newsstand/store/StoreRequest$Priority;)Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    move-result-object v5

    .line 306
    .local v5, "storeRequest":Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->accountNameManager()Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/auth/AccountNameManager;->getCurrentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/auth/AuthHelper;->accountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    .line 307
    .local v4, "account":Landroid/accounts/Account;
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->this$0:Lcom/google/apps/dots/android/newsstand/store/MutationStore;

    .line 308
    # getter for: Lcom/google/apps/dots/android/newsstand/store/MutationStore;->nsStore:Lcom/google/apps/dots/android/newsstand/store/NSStore;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore;->access$600(Lcom/google/apps/dots/android/newsstand/store/MutationStore;)Lcom/google/apps/dots/android/newsstand/store/NSStore;

    move-result-object v0

    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken(Landroid/accounts/Account;)Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/apps/dots/android/newsstand/store/NSStore;->submit(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    new-instance v0, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;Ljava/lang/String;Lcom/google/apps/dots/proto/client/DotsClient$MutationLog;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)V

    .line 307
    invoke-static {v8, v0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/store/MutationStore$5;->call()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
