.class Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;
.super Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;
.source "NewsArticleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->markPostAsReadIfNeeded()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Post;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$ArticleResourceCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 4
    .param p1, "result"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 381
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getIsMetered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->account()Landroid/accounts/Account;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->access$500(Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->readingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    .line 384
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment;->state()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragmentState;->postId:Ljava/lang/String;

    .line 383
    invoke-static {v1, v2, v3, v0}, Lcom/google/apps/dots/android/newsstand/util/ReadStateUtil;->markPostAsRead(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V

    .line 386
    :cond_1
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 378
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticleFragment$7;->onSuccess(Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    return-void
.end method
