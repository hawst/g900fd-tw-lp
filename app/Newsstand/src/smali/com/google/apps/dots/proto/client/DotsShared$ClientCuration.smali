.class public final Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientCuration"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;


# instance fields
.field public entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22633
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22634
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 22637
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22634
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    .locals 5

    .prologue
    .line 22649
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22653
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 22654
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22655
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 22656
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 22657
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v4

    aput-object v4, v3, v2

    .line 22655
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 22650
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 22651
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 22661
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 22630
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 22666
    if-ne p1, p0, :cond_0

    const/4 v1, 0x1

    .line 22669
    :goto_0
    return v1

    .line 22667
    :cond_0
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 22668
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    .line 22669
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 22697
    const/4 v1, 0x0

    .line 22698
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v2, :cond_1

    .line 22699
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 22700
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_0

    .line 22701
    const/4 v5, 0x1

    .line 22702
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 22699
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 22706
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->cachedSize:I

    .line 22707
    return v1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 22673
    const/16 v1, 0x11

    .line 22674
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 22675
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 22681
    :cond_0
    return v1

    .line 22677
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 22678
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 22677
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 22678
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 22715
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 22716
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 22720
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 22721
    :sswitch_0
    return-object p0

    .line 22726
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 22727
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-nez v5, :cond_2

    move v1, v4

    .line 22728
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22729
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v5, :cond_1

    .line 22730
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22732
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    .line 22733
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 22734
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 22735
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 22736
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 22733
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 22727
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v1, v5

    goto :goto_1

    .line 22739
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;-><init>()V

    aput-object v6, v5, v1

    .line 22740
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 22716
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22630
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22686
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    if-eqz v1, :cond_1

    .line 22687
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ClientCuration;->entity:[Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 22688
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    if-eqz v0, :cond_0

    .line 22689
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 22687
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 22693
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;
    :cond_1
    return-void
.end method
