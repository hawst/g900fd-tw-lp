.class Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;
.super Ljava/lang/Object;
.source "StartActivity.java"

# interfaces
.implements Lcom/google/common/util/concurrent/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->navigateToTarget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/FutureCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/share/TargetInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/StartActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 317
    # getter for: Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Unable to find target.  Redirecting to ReadNow"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 318
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfBadShareLink()V

    .line 323
    :goto_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;-><init>()V

    sget-object v2, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;->LOCATION_UNSPECIFIED:Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;

    .line 324
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->setLocation(Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Location;)Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/share/TargetInfo$Builder;->build()Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    move-result-object v1

    .line 323
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->navigateToTarget(Lcom/google/apps/dots/android/newsstand/share/TargetInfo;)V

    .line 325
    return-void

    .line 321
    :cond_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/toast/Toasts;->notifyUserOfContentNotAvailableOffline()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/google/apps/dots/android/newsstand/share/TargetInfo;)V
    .locals 4
    .param p1, "targetInfo"    # Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    .prologue
    .line 311
    # getter for: Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->access$400()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    const-string v1, "Found target %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;->this$0:Lcom/google/apps/dots/android/newsstand/activity/StartActivity;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity;->navigateToTarget(Lcom/google/apps/dots/android/newsstand/share/TargetInfo;)V

    .line 313
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 308
    check-cast p1, Lcom/google/apps/dots/android/newsstand/share/TargetInfo;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/StartActivity$5;->onSuccess(Lcom/google/apps/dots/android/newsstand/share/TargetInfo;)V

    return-void
.end method
