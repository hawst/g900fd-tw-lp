.class public Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;
.super Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;
.source "CardSourceItem.java"


# static fields
.field public static final DK_BACKGROUND_COLOR_RESID:I

.field public static final DK_HIGHLIGHTS_ASPECT_RATIO:I

.field public static final DK_SOURCE_CLICKHANDLER:I

.field public static final DK_SOURCE_ICON_BACKGROUND_RES_ID:I

.field public static final DK_SOURCE_ICON_ID:I

.field public static final DK_SOURCE_ICON_NO_IMAGE:I

.field public static final DK_SOURCE_NAME:I

.field public static final DK_SOURCE_TRANSITION_NAME:I

.field public static final EQUALITY_FIELDS:[I

.field public static final SOLID_COLOR_LAYOUT:I

.field public static final STANDARD_LAYOUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_NAME:I

    .line 24
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceIconId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_ID:I

    .line 26
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceIconNoImage:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_NO_IMAGE:I

    .line 28
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceIconBackground:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_ICON_BACKGROUND_RES_ID:I

    .line 31
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceClickListener:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_CLICKHANDLER:I

    .line 33
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_sourceTransitionName:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_TRANSITION_NAME:I

    .line 34
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_backgroundColorResId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_BACKGROUND_COLOR_RESID:I

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->CardSourceItem_highlightsAspectRatio:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_HIGHLIGHTS_ASPECT_RATIO:I

    .line 38
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_ARCHIVED:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->EQUALITY_FIELDS:[I

    .line 40
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_source_item:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->STANDARD_LAYOUT:I

    .line 41
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->card_source_item_solid_color:I

    sput v0, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->SOLID_COLOR_LAYOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 8
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/card/viewgroup/CardLinearLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 61
    if-nez p1, :cond_2

    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 63
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    instance-of v6, v1, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    if-eqz v6, :cond_1

    .line 64
    sget v6, Lcom/google/android/apps/newsstanddev/R$color;->card_background:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 65
    .local v2, "bgResId":Ljava/lang/Integer;
    if-eqz p1, :cond_0

    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_BACKGROUND_COLOR_RESID:I

    invoke-virtual {p1, v6}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 66
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_BACKGROUND_COLOR_RESID:I

    invoke-virtual {p1, v6}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v2

    .line 69
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    .line 70
    .local v4, "colorStateList":Landroid/content/res/ColorStateList;
    move-object v0, v1

    check-cast v0, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;

    move-object v3, v0

    .line 71
    .local v3, "cardViewBackground":Lcom/google/android/play/cardview/CardViewBackgroundDrawable;
    invoke-virtual {v3, v4}, Lcom/google/android/play/cardview/CardViewBackgroundDrawable;->setBackgroundColorStateList(Landroid/content/res/ColorStateList;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v2    # "bgResId":Ljava/lang/Integer;
    .end local v3    # "cardViewBackground":Lcom/google/android/play/cardview/CardViewBackgroundDrawable;
    .end local v4    # "colorStateList":Landroid/content/res/ColorStateList;
    :cond_1
    :goto_1
    return-void

    .line 61
    .end local v1    # "background":Landroid/graphics/drawable/Drawable;
    :cond_2
    sget v6, Lcom/google/apps/dots/android/newsstand/card/CardSourceItem;->DK_SOURCE_CLICKHANDLER:I

    invoke-virtual {p1, v6}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 72
    .restart local v1    # "background":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "bgResId":Ljava/lang/Integer;
    :catch_0
    move-exception v5

    .line 73
    .local v5, "ex":Landroid/content/res/Resources$NotFoundException;
    const-string v6, "CardSourceItem"

    const-string v7, "Unable to find color state list"

    invoke-static {v6, v7, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
