.class final Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;
.super Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;
.source "SavedPostUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->savePost(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/UncheckedCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1, "optionalResult"    # Ljava/lang/Object;

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->added_to_bookmarks:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil$1;->val$context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 51
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/saved/SavedPostUtil;->syncSavedEdition()V

    .line 52
    return-void
.end method
