.class public final Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Pin"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;


# instance fields
.field private bitField0_:I

.field private pinId_:I

.field private snapshotId_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 33
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    .line 52
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    .line 28
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    .locals 3

    .prologue
    .line 82
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    return-object v0

    .line 83
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->clone()Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 93
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    .line 94
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getPinId()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 120
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    .line 121
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 124
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    .line 125
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->cachedSize:I

    .line 128
    return v0
.end method

.method public getSnapshotId()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 99
    const/16 v0, 0x11

    .line 100
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 101
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    add-int v0, v1, v2

    .line 102
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    add-int v0, v1, v2

    .line 103
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 137
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 141
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 142
    :sswitch_0
    return-object p0

    .line 147
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    .line 148
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    goto :goto_0

    .line 152
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    .line 153
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    goto :goto_0

    .line 137
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;

    move-result-object v0

    return-object v0
.end method

.method public setPinId(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    .line 39
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    .line 40
    return-object p0
.end method

.method public setSnapshotId(I)Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    .line 58
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    .line 59
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->pinId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 111
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 112
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$DiskManifest$BlobEntry$Pin;->snapshotId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 114
    :cond_1
    return-void
.end method
