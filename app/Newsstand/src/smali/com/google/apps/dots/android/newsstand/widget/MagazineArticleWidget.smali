.class public Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;
.super Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;
.source "MagazineArticleWidget.java"


# static fields
.field public static final DOWN_ARROW_ANIMATION_DISTANCE:F = 30.0f

.field public static final DOWN_ARROW_ANIMATION_DURATION:I = 0x3e8


# instance fields
.field private downArrow:Landroid/widget/ImageView;

.field private downArrowAnimator:Landroid/animation/ObjectAnimator;

.field private final enableDownArrow:Z

.field private final usesLegacyLayoutEngine:Z


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;IZZ)V
    .locals 1
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    .param p2, "originalEdition"    # Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;
    .param p3, "viewingEdition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p4, "postIndex"    # I
    .param p5, "usesLegacyLayoutEngine"    # Z
    .param p6, "enableDownArrow"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;I)V

    .line 30
    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->usesLegacyLayoutEngine:Z

    .line 31
    iput-boolean p6, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->enableDownArrow:Z

    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->useHorizontalScrollingHack()V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 48
    return-void
.end method

.method private showDownArrow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 85
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrow:Landroid/widget/ImageView;

    const-string v1, "translationY"

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    .line 90
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 93
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 94
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 95
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 96
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrowAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/animation/AnimationUtil;->startAnimation(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Landroid/animation/Animator;)V

    .line 98
    :cond_0
    return-void

    .line 88
    :array_0
    .array-data 4
        -0x3e100000    # -30.0f
        0x0
    .end array-data
.end method


# virtual methods
.method protected makeBridge(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;)Lcom/google/apps/dots/android/newsstand/bridge/DotsWebViewBridge;
    .locals 6
    .param p1, "activity"    # Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    .prologue
    .line 57
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bridge/MagazineWidgetBridge;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->asyncScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v3

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->originalEdition:Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->viewingEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/bridge/MagazineWidgetBridge;-><init>(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/android/newsstand/edition/NormalEdition;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    return-object v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/apps/dots/android/newsstand/widget/NormalArticleWidget;->onScrollChanged(IIII)V

    .line 75
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->enableDownArrow:Z

    if-eqz v0, :cond_0

    .line 76
    if-nez p2, :cond_1

    .line 77
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->showDownArrow()V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrow:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setupDownArrow(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "downArrow"    # Landroid/widget/ImageView;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->downArrow:Landroid/widget/ImageView;

    .line 67
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->enableDownArrow:Z

    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->showDownArrow()V

    .line 70
    :cond_0
    return-void
.end method

.method protected useHttpContentService()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public usesLegacyLayoutEngine()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/widget/MagazineArticleWidget;->usesLegacyLayoutEngine:Z

    return v0
.end method
