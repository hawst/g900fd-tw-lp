.class public Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;
.super Ljava/lang/Object;
.source "LayoutSelectionUtil.java"


# static fields
.field public static final NORMAL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final NORMAL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final NORMAL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final NORMAL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final PHONE_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final PHONE_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final PHONE_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final PHONE_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final SMALL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final SMALL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final SMALL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field public static final SMALL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

.field private static largeLayouts:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static layoutClasses:Lcom/google/common/collect/ImmutableMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static tallLayouts:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static wideLayouts:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_single_card_row_layout:I

    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 43
    new-array v0, v2, [Ljava/lang/Integer;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 45
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_cards_row_layout:I

    .line 46
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 47
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_column_single_card_layout:I

    .line 48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 51
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_single_card_row_layout:I

    .line 52
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 53
    new-array v0, v2, [Ljava/lang/Integer;

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 55
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_cards_row_layout:I

    .line 56
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 57
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_column_single_card_layout:I

    .line 58
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 61
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_cards_row_layout:I

    .line 62
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 63
    new-array v0, v4, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_big_card_layout:I

    .line 64
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_column_single_card_layout:I

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 67
    new-array v0, v3, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_three_cards_row_layout:I

    .line 68
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 69
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_two_one_layout:I

    .line 70
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_one_two_layout:I

    .line 71
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_wide_normal_layout:I

    .line 72
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_normal_wide_layout:I

    .line 73
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 77
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_single_card_row_layout:I

    .line 78
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;

    .line 79
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowSingleCardRowLayout;-><init>(Landroid/content/Context;)V

    .line 78
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_cards_row_layout:I

    .line 80
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoCardsRowLayout;

    .line 81
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoCardsRowLayout;-><init>(Landroid/content/Context;)V

    .line 80
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_three_cards_row_layout:I

    .line 82
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowThreeCardsRowLayout;

    .line 83
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowThreeCardsRowLayout;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_two_column_single_card_layout:I

    .line 84
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;

    .line 85
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowTwoColumnSingleCardLayout;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_two_one_layout:I

    .line 86
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;

    .line 87
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;-><init>(Landroid/content/Context;)V

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_moneyshot_one_two_layout:I

    .line 88
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;

    .line 89
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowMoneyshotLayout;-><init>(Landroid/content/Context;)V

    .line 88
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_wide_normal_layout:I

    .line 90
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;

    .line 91
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;-><init>(Landroid/content/Context;)V

    .line 90
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_normal_wide_layout:I

    .line 92
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;

    .line 93
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowWideNormalLayout;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->group_row_big_card_layout:I

    .line 94
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowBigCardLayout;

    .line 95
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowBigCardLayout;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->layoutClasses:Lcom/google/common/collect/ImmutableMap;

    .line 99
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item:I

    .line 100
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image:I

    .line 101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_news_item_full_image:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine:I

    .line 102
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_splash_item_magazine:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->largeLayouts:Lcom/google/common/collect/ImmutableMap;

    .line 106
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item:I

    .line 107
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_topic_item_horizontal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    .line 111
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_horizontal:I

    .line 112
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_horizontal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    .line 113
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_horizontal:I

    .line 114
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_offer_item_magazine_horizontal:I

    .line 115
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 114
    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    .line 116
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_one_column:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    .line 117
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->card_warm_welcome_two_column:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->wideLayouts:Lcom/google/common/collect/ImmutableMap;

    .line 121
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->builder()Lcom/google/common/collect/ImmutableMap$Builder;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->tallLayouts:Lcom/google/common/collect/ImmutableMap;

    .line 120
    return-void
.end method

.method public static arrangeLayout(ILjava/util/List;Ljava/util/ArrayList;I)[I
    .locals 2
    .param p0, "layoutResourceId"    # I
    .param p3, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)[I"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    .local p2, "cardListIndices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getLayoutClass(I)Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;

    move-result-object v0

    .line 254
    .local v0, "layoutClass":Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;
    invoke-interface {v0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;->arrangeLayout(Ljava/util/List;Ljava/util/ArrayList;I)[I

    move-result-object v1

    return-object v1
.end method

.method public static getBasicLayoutResourceIds()[Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 129
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 130
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    .line 131
    .local v2, "isLandscape":Z
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 133
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil$1;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 143
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot determine device type."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 130
    .end local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v2    # "isLandscape":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 135
    .restart local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v2    # "isLandscape":Z
    :pswitch_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 140
    :goto_1
    return-object v3

    .line 135
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 137
    :pswitch_1
    if-eqz v2, :cond_2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 140
    :pswitch_2
    if-eqz v2, :cond_3

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_LAND_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_PORT_BASIC_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getContainedCardAt(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p0, "parentView"    # Landroid/view/ViewGroup;
    .param p1, "index"    # I

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 269
    .local v0, "child":Landroid/view/View;
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 270
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 272
    :goto_0
    return-object v1

    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getIdealRowHeight()I
    .locals 4

    .prologue
    .line 210
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$dimen;->action_bar_default_height:I

    .line 211
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v0, v2

    .line 212
    .local v0, "actionBarHeight":F
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->util()Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    sub-float v1, v2, v0

    .line 213
    .local v1, "availableHeight":F
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->getNumRowsPerScreen()F

    move-result v2

    div-float v2, v1, v2

    float-to-int v2, v2

    return v2
.end method

.method private static getLayoutClass(I)Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;
    .locals 2
    .param p0, "resourceId"    # I

    .prologue
    .line 258
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->layoutClasses:Lcom/google/common/collect/ImmutableMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/grouprow/GroupRowLayout;

    return-object v0
.end method

.method public static getNumRowsPerScreen()F
    .locals 5

    .prologue
    .line 171
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 172
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v2

    .line 173
    .local v2, "orientation":Lcom/google/apps/dots/shared/Orientation;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 175
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil$1;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 186
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot determine device type."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 177
    :pswitch_0
    sget-object v3, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v2, v3, :cond_0

    const v3, 0x3f666666    # 0.9f

    .line 183
    :goto_0
    return v3

    .line 177
    :cond_0
    const/high16 v3, 0x3fc00000    # 1.5f

    goto :goto_0

    .line 180
    :pswitch_1
    sget-object v3, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v2, v3, :cond_1

    const/high16 v3, 0x3fa00000    # 1.25f

    goto :goto_0

    :cond_1
    const v3, 0x3fcccccd    # 1.6f

    goto :goto_0

    .line 183
    :pswitch_2
    sget-object v3, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v2, v3, :cond_2

    const v3, 0x40066666    # 2.1f

    goto :goto_0

    :cond_2
    const/high16 v3, 0x40600000    # 3.5f

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getOversizedLayoutResourceIds()[Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 151
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 152
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v3

    sget-object v4, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    .line 153
    .local v2, "isLandscape":Z
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 155
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil$1;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 166
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot determine device type."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 152
    .end local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v2    # "isLandscape":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 157
    .restart local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v2    # "isLandscape":Z
    :pswitch_0
    if-eqz v2, :cond_1

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    .line 163
    :goto_1
    return-object v3

    .line 157
    :cond_1
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->PHONE_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 160
    :pswitch_1
    if-eqz v2, :cond_2

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->SMALL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 163
    :pswitch_2
    if-eqz v2, :cond_3

    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_LAND_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->NORMAL_TABLET_PORT_OVERSIZED_LAYOUT_RES_IDS:[Ljava/lang/Integer;

    goto :goto_1

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getResizedLayoutId(ILjava/lang/Integer;)Ljava/lang/Integer;
    .locals 2
    .param p0, "sourceLayoutId"    # I
    .param p1, "cardSize"    # Ljava/lang/Integer;

    .prologue
    .line 222
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 232
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 224
    :pswitch_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->largeLayouts:Lcom/google/common/collect/ImmutableMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 230
    :goto_0
    return-object v0

    .line 226
    :pswitch_1
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->wideLayouts:Lcom/google/common/collect/ImmutableMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0

    .line 228
    :pswitch_2
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil;->tallLayouts:Lcom/google/common/collect/ImmutableMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0

    .line 230
    :pswitch_3
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getTypicalNumColumnsPerScreen()I
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 191
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 192
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getOrientation(Landroid/content/Context;)Lcom/google/apps/dots/shared/Orientation;

    move-result-object v5

    sget-object v6, Lcom/google/apps/dots/shared/Orientation;->LANDSCAPE:Lcom/google/apps/dots/shared/Orientation;

    if-ne v5, v6, :cond_0

    move v2, v3

    .line 193
    .local v2, "isLandscape":Z
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->getDeviceCategory(Landroid/content/Context;)Lcom/google/apps/dots/shared/DeviceCategory;

    move-result-object v0

    .line 195
    .local v0, "category":Lcom/google/apps/dots/shared/DeviceCategory;
    sget-object v5, Lcom/google/apps/dots/android/newsstand/card/LayoutSelectionUtil$1;->$SwitchMap$com$google$apps$dots$shared$DeviceCategory:[I

    invoke-virtual {v0}, Lcom/google/apps/dots/shared/DeviceCategory;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 201
    if-eqz v2, :cond_2

    :goto_1
    move v3, v4

    :goto_2
    return v3

    .line 192
    .end local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .end local v2    # "isLandscape":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 197
    .restart local v0    # "category":Lcom/google/apps/dots/shared/DeviceCategory;
    .restart local v2    # "isLandscape":Z
    :pswitch_0
    if-eqz v2, :cond_1

    const/4 v3, 0x3

    goto :goto_2

    :cond_1
    move v3, v4

    goto :goto_2

    :cond_2
    move v4, v3

    .line 201
    goto :goto_1

    .line 195
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
