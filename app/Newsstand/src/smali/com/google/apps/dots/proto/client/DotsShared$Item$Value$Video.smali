.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Video"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;


# instance fields
.field private bitField0_:I

.field private height_:I

.field private serviceId_:Ljava/lang/String;

.field private serviceType_:I

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12843
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12844
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 12854
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    .line 12873
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    .line 12895
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    .line 12914
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    .line 12844
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    .locals 3

    .prologue
    .line 12966
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 12970
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v2, :cond_0

    .line 12971
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12973
    :cond_0
    return-object v0

    .line 12967
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    :catch_0
    move-exception v1

    .line 12968
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12840
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 12978
    if-ne p1, p0, :cond_1

    .line 12985
    :cond_0
    :goto_0
    return v1

    .line 12979
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 12980
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .line 12981
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 12982
    :goto_1
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 12985
    goto :goto_0

    .line 12981
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    .line 12982
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 12985
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 12916
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 13020
    const/4 v0, 0x0

    .line 13021
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 13022
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    .line 13023
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13025
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 13026
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    .line 13027
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13029
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 13030
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    .line 13031
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13033
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 13034
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    .line 13035
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13037
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v1, :cond_4

    .line 13038
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13039
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13041
    :cond_4
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->cachedSize:I

    .line 13042
    return v0
.end method

.method public getServiceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12875
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceType()I
    .locals 1

    .prologue
    .line 12856
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    return v0
.end method

.method public getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;
    .locals 1

    .prologue
    .line 12935
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 12897
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    return v0
.end method

.method public hasThumbnail()Z
    .locals 1

    .prologue
    .line 12945
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 12989
    const/16 v0, 0x11

    .line 12990
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 12991
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    add-int v0, v1, v3

    .line 12992
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 12993
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    add-int v0, v1, v3

    .line 12994
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    add-int v0, v1, v3

    .line 12995
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 12996
    return v0

    .line 12992
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 12995
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 13050
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 13051
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 13055
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 13056
    :sswitch_0
    return-object p0

    .line 13061
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 13062
    .local v1, "temp":I
    if-eq v1, v3, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 13064
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    .line 13065
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    goto :goto_0

    .line 13067
    :cond_2
    iput v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    goto :goto_0

    .line 13072
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    .line 13073
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    goto :goto_0

    .line 13077
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    .line 13078
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    goto :goto_0

    .line 13082
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    .line 13083
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    goto :goto_0

    .line 13087
    :sswitch_5
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-nez v2, :cond_3

    .line 13088
    new-instance v2, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-direct {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;-><init>()V

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .line 13090
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 13051
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12840
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13001
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13002
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 13004
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13005
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->serviceId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 13007
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13008
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 13010
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13011
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 13013
    :cond_3
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    if-eqz v0, :cond_4

    .line 13014
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 13016
    :cond_4
    return-void
.end method
