.class public final Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsNativeBody.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsNativeBody;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaDetails"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;


# instance fields
.field private bitField0_:I

.field private enableControls_:Z

.field private forceFullscreen_:Z

.field private videoFieldId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2220
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2221
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2226
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    .line 2248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    .line 2267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    .line 2221
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    .locals 3

    .prologue
    .line 2298
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2302
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    return-object v0

    .line 2299
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    :catch_0
    move-exception v1

    .line 2300
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2217
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->clone()Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2307
    if-ne p1, p0, :cond_1

    .line 2310
    :cond_0
    :goto_0
    return v1

    .line 2308
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2309
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    .line 2310
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getEnableControls()Z
    .locals 1

    .prologue
    .line 2269
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    return v0
.end method

.method public getForceFullscreen()Z
    .locals 1

    .prologue
    .line 2250
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 2339
    const/4 v0, 0x0

    .line 2340
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2341
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    .line 2342
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2344
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2345
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    .line 2346
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2348
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2349
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    .line 2350
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2352
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->cachedSize:I

    .line 2353
    return v0
.end method

.method public getVideoFieldId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2228
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2316
    const/16 v0, 0x11

    .line 2317
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 2318
    mul-int/lit8 v4, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v4, v1

    .line 2319
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 2320
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 2321
    return v0

    .line 2318
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    move v1, v3

    .line 2319
    goto :goto_1

    :cond_2
    move v2, v3

    .line 2320
    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2362
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2366
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2367
    :sswitch_0
    return-object p0

    .line 2372
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    .line 2373
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    goto :goto_0

    .line 2377
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    .line 2378
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    goto :goto_0

    .line 2382
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    .line 2383
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    goto :goto_0

    .line 2362
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2217
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2326
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2327
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->videoFieldId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2329
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2330
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->forceFullscreen_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2332
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2333
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsNativeBody$MediaDetails;->enableControls_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2335
    :cond_2
    return-void
.end method
