.class Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "CollectionEdition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;->refresh(Landroid/content/Context;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

.field final synthetic val$editionCardList:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

.field final synthetic val$freshenFuture:Lcom/google/common/util/concurrent/SettableFuture;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;->this$0:Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;->val$editionCardList:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;->val$freshenFuture:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 125
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;->val$editionCardList:Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;

    invoke-virtual {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/EditionCardList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 127
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/CollectionEdition$2;->val$freshenFuture:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 129
    :cond_0
    return-void
.end method
