.class Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    .prologue
    .line 421
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    iget-boolean v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$failAtEnd:Z

    if-nez v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$result:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->set(Ljava/lang/Object;)Z

    .line 430
    :goto_0
    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$result:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Timeout of %d ms exceeded."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13$1;->this$0:Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;

    iget-wide v6, v5, Lcom/google/apps/dots/android/newsstand/async/futures/Async$13;->val$durationMs:J

    .line 428
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
