.class public abstract Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;
.super Ljava/lang/Object;
.source "PreferenceInitializer.java"


# instance fields
.field protected final prefKey:Ljava/lang/String;

.field protected prefListenerHandler:Lcom/google/apps/dots/android/newsstand/util/Disposable;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;->prefKey:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method protected initialize()V
    .locals 5

    .prologue
    .line 19
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer$1;-><init>(Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;)V

    .line 25
    .local v0, "listener":Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;->prefKey:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->registerListener(Lcom/google/apps/dots/android/newsstand/preference/PreferenceListener;[Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/util/Disposable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;->prefListenerHandler:Lcom/google/apps/dots/android/newsstand/util/Disposable;

    .line 27
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/preference/PreferenceInitializer;->update()V

    .line 28
    return-void
.end method

.method protected abstract update()V
.end method
