.class public Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
.super Ljava/lang/Object;
.source "MyMagazinesFragmentState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public isArchiveShowing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0
    .param p1, "isArchiveShowing"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    .line 18
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 28
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 29
    check-cast v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;

    .line 30
    .local v0, "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    iget-boolean v3, v0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 32
    .end local v0    # "state":Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 22
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "myMagazinesFragment: archiveShowing %s}"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    .line 23
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 22
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/MyMagazinesFragmentState;->isArchiveShowing:Z

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    return-void
.end method
