.class public Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
.super Ljava/lang/Object;
.source "TimestampedLruCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected final cache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<TK;",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "maxSize"    # I

    .prologue
    .line 38
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;

    invoke-direct {v0, p0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$1;-><init>(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;
    .param p1, "x1"    # Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;)TV;"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p1, "wrapper":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;->value:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "evicted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTK;TV;TV;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p2, "key":Ljava/lang/Object;, "TK;"
    .local p3, "oldValue":Ljava/lang/Object;, "TV;"
    .local p4, "newValue":Ljava/lang/Object;, "TV;"
    return-void
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-direct {v1, p2}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->size()I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)I"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    const/4 v0, 0x1

    return v0
.end method

.method public snapshot()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v2

    .line 80
    .local v2, "wrapperMap":Ljava/util/Map;, "Ljava/util/Map<TK;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;>;"
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 81
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 82
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;

    invoke-direct {p0, v3}, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->unwrap(Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 84
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper<TV;>;>;"
    :cond_0
    return-object v1
.end method

.method public wrapperSnapshot()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<TK;",
            "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache$Wrapper",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;, "Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache<TK;TV;>;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/bitmap/TimestampedLruCache;->cache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
