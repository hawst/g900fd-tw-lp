.class public Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;
.super Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;
.source "MyNewsTranslateDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/translation/TranslateDialog;-><init>()V

    return-void
.end method

.method public static show(Landroid/support/v4/app/FragmentActivity;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 21
    new-instance v1, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;

    invoke-direct {v1}, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;-><init>()V

    .line 23
    .local v1, "dialog":Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 24
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "TranslateDialog_edition"

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 25
    iget-object v2, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasLanguageCode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    const-string v2, "TranslateDialog_editionLanguageCode"

    iget-object v3, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;->setArguments(Landroid/os/Bundle;)V

    .line 30
    const-string v2, "TranslateDialog"

    invoke-static {p0, v1, v2}, Lcom/google/apps/dots/android/newsstand/util/AndroidUtil;->showSupportDialogCarefully(Landroid/support/v4/app/FragmentActivity;Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected translateEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;Ljava/lang/String;)V
    .locals 4
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->userWriteToken()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v1

    .line 36
    .local v1, "asyncToken":Lcom/google/apps/dots/android/newsstand/async/AsyncToken;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v0

    .line 37
    .local v0, "activity":Lcom/google/apps/dots/android/newsstand/activity/NSActivity;
    invoke-virtual {p1, v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummaryFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;

    invoke-direct {v3, p0, v0, v1, p2}, Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog$1;-><init>(Lcom/google/apps/dots/android/newsstand/translation/MyNewsTranslateDialog;Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 45
    return-void
.end method
