.class final enum Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
.super Ljava/lang/Enum;
.source "Syncer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/sync/Syncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TaskType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum NEWS_OTHER_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum NEWS_PRIMARY_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

.field public static final enum SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;


# instance fields
.field final isAttachment:Z

.field final maxFailures:I

.field final requiredProgress:F

.field final taskPriorityMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x5

    const/4 v14, 0x3

    const/high16 v13, 0x41200000    # 10.0f

    const/4 v12, 0x1

    const/4 v2, 0x0

    .line 167
    new-instance v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v1, "BRANCH"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 168
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "COLLECTION"

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move v6, v12

    move v10, v2

    move v11, v2

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 169
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "BLOB"

    const/4 v6, 0x2

    const/high16 v7, 0x40000000    # 2.0f

    const-wide/16 v8, 0x1388

    const/4 v10, 0x2

    move v11, v2

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 171
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "SOURCE_ICON"

    const/high16 v7, 0x40a00000    # 5.0f

    const-wide/16 v8, 0x1f40

    move v6, v14

    move v10, v14

    move v11, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 173
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "MAGAZINE_IMAGE"

    const/4 v6, 0x4

    const-wide/16 v8, 0x2710

    move v7, v13

    move v10, v2

    move v11, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 174
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "NEWS_PRIMARY_IMAGE"

    const-wide/16 v8, 0x2710

    move v6, v15

    move v7, v13

    move v10, v14

    move v11, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_PRIMARY_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 175
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "NEWS_OTHER_IMAGE"

    const/4 v6, 0x6

    const-wide/16 v8, 0x4e20

    move v7, v13

    move v10, v15

    move v11, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_OTHER_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 176
    new-instance v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    const-string v5, "OTHER_ATTACHMENT"

    const/4 v6, 0x7

    const-wide/16 v8, 0x7530

    move v7, v13

    move v10, v15

    move v11, v12

    invoke-direct/range {v4 .. v11}, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;-><init>(Ljava/lang/String;IFJIZ)V

    sput-object v4, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    .line 163
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BRANCH:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->COLLECTION:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v1, v0, v12

    const/4 v1, 0x2

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->BLOB:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->SOURCE_ICON:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v1, v0, v14

    const/4 v1, 0x4

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->MAGAZINE_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_PRIMARY_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v1, v0, v15

    const/4 v1, 0x6

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->NEWS_OTHER_IMAGE:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->OTHER_ATTACHMENT:Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFJIZ)V
    .locals 0
    .param p3, "requiredProgress"    # F
    .param p4, "taskPriorityMs"    # J
    .param p6, "maxFailures"    # I
    .param p7, "isAttachment"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FJIZ)V"
        }
    .end annotation

    .prologue
    .line 189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 190
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->requiredProgress:F

    .line 191
    iput-wide p4, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->taskPriorityMs:J

    .line 192
    iput p6, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->maxFailures:I

    .line 193
    iput-boolean p7, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->isAttachment:Z

    .line 194
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 163
    const-class v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->$VALUES:[Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    invoke-virtual {v0}, [Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;

    return-object v0
.end method


# virtual methods
.method public priority()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 186
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/apps/dots/android/newsstand/sync/Syncer$TaskType;->taskPriorityMs:J

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method
