.class Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;
.super Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;
.source "EditableLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->setupListView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;
    .param p2, "arg0"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/BaseReadWriteFilter;-><init>(Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 7
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 176
    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 177
    .local v0, "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v5

    sget-object v6, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->READ_NOW:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    if-ne v5, v6, :cond_1

    move v2, v3

    .line 178
    .local v2, "isReadNow":Z
    :goto_0
    sget v5, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_IS_PURCHASED:I

    invoke-virtual {p1, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v1

    .line 179
    .local v1, "isPurchased":Z
    sget v6, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->DK_IS_EDITABLE:I

    if-nez v2, :cond_2

    move v5, v3

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {p1, v6, v5}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 180
    sget v5, Lcom/google/apps/dots/android/newsstand/card/EditableCardListView;->DK_IS_REMOVABLE:I

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    move v4, v3

    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p1, v5, v4}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 181
    return v3

    .end local v1    # "isPurchased":Z
    .end local v2    # "isReadNow":Z
    :cond_1
    move v2, v4

    .line 177
    goto :goto_0

    .restart local v1    # "isPurchased":Z
    .restart local v2    # "isReadNow":Z
    :cond_2
    move v5, v4

    .line 179
    goto :goto_1
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 3
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/ondevice/OnDeviceUiHelper;->isOnDeviceOnly()Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 188
    .local v0, "addMoreCard":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/apps/dots/android/newsstand/datasource/ApplicationList;->DK_EDITION:I

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->addMoreDummyEdition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;->access$300(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 189
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->LAYOUT:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 190
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 191
    sget v1, Lcom/google/apps/dots/android/newsstand/card/CardAddMore;->DK_ON_CLICK_LISTENER:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5$1;-><init>(Lcom/google/apps/dots/android/newsstand/home/library/EditableLibraryFragment$5;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 198
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    .end local v0    # "addMoreCard":Lcom/google/android/libraries/bind/data/Data;
    :cond_0
    return-object p1
.end method
