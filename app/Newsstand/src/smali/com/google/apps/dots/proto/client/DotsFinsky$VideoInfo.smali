.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;


# instance fields
.field private bitField0_:I

.field private durationSeconds_:I

.field private secureUrl_:Ljava/lang/String;

.field private thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

.field private url_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1202
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1203
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1208
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    .line 1230
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    .line 1252
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    .line 1203
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    .locals 3

    .prologue
    .line 1303
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1307
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v2, :cond_0

    .line 1308
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1310
    :cond_0
    return-object v0

    .line 1304
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    :catch_0
    move-exception v1

    .line 1305
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1315
    if-ne p1, p0, :cond_1

    .line 1321
    :cond_0
    :goto_0
    return v1

    .line 1316
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1317
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    .line 1318
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1319
    :goto_2
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 1321
    goto :goto_0

    .line 1318
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    .line 1319
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1321
    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1352
    const/4 v0, 0x0

    .line 1353
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1354
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    .line 1355
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1357
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1358
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    .line 1359
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1361
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1362
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    .line 1363
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1365
    :cond_2
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v1, :cond_3

    .line 1366
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1367
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1369
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->cachedSize:I

    .line 1370
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1325
    const/16 v0, 0x11

    .line 1326
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1327
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 1328
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1329
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    add-int v0, v1, v3

    .line 1330
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 1331
    return v0

    .line 1327
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1328
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1330
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1378
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1379
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1383
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1384
    :sswitch_0
    return-object p0

    .line 1389
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    .line 1390
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    goto :goto_0

    .line 1394
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    .line 1395
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    goto :goto_0

    .line 1399
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    .line 1400
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    goto :goto_0

    .line 1404
    :sswitch_4
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-nez v1, :cond_1

    .line 1405
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    .line 1407
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1379
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1199
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1336
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1337
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->url_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1339
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1340
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->secureUrl_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1342
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1343
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->durationSeconds_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1345
    :cond_2
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    if-eqz v0, :cond_3

    .line 1346
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$VideoInfo;->thumbnail_:Lcom/google/apps/dots/proto/client/DotsFinsky$ImageInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1348
    :cond_3
    return-void
.end method
