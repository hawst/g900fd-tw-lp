.class final Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;->makeExpiringFuture(Lcom/google/common/util/concurrent/ListenableFuture;ZJLjava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cancelOnTimeout:Z

.field final synthetic val$inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

.field final synthetic val$returnVal:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

.field final synthetic val$timeoutExecutor:Ljava/util/concurrent/Executor;

.field final synthetic val$timeoutMs:J


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;JZLcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1

    .prologue
    .line 390
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$timeoutExecutor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$returnVal:Lcom/google/apps/dots/android/newsstand/async/futures/TransformFuture;

    iput-wide p3, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$timeoutMs:J

    iput-boolean p5, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$cancelOnTimeout:Z

    iput-object p6, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$inputFuture:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;->val$timeoutExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;

    invoke-direct {v1, p0}, Lcom/google/apps/dots/android/newsstand/async/futures/Async$12$1;-><init>(Lcom/google/apps/dots/android/newsstand/async/futures/Async$12;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 404
    return-void
.end method
