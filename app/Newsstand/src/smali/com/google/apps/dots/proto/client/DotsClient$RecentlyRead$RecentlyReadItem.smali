.class public final Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsClient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecentlyReadItem"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;


# instance fields
.field private bitField0_:I

.field private edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

.field private lastRead_:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1471
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1472
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1496
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    .line 1472
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    .locals 3

    .prologue
    .line 1526
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1530
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v2, :cond_0

    .line 1531
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->clone()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    move-result-object v2

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1533
    :cond_0
    return-object v0

    .line 1527
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    :catch_0
    move-exception v1

    .line 1528
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1468
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->clone()Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1538
    if-ne p1, p0, :cond_1

    .line 1541
    :cond_0
    :goto_0
    return v1

    .line 1539
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1540
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    .line 1541
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v3, :cond_3

    :goto_1
    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    iget-wide v6, v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getEdition()Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;
    .locals 1

    .prologue
    .line 1479
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    return-object v0
.end method

.method public getLastRead()J
    .locals 2

    .prologue
    .line 1498
    iget-wide v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 1565
    const/4 v0, 0x0

    .line 1566
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v1, :cond_0

    .line 1567
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1568
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1570
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 1571
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    .line 1572
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1574
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->cachedSize:I

    .line 1575
    return v0
.end method

.method public hasEdition()Z
    .locals 1

    .prologue
    .line 1489
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastRead()Z
    .locals 1

    .prologue
    .line 1506
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 1546
    const/16 v0, 0x11

    .line 1547
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 1548
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 1549
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    iget-wide v4, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1550
    return v0

    .line 1548
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1583
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1584
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1588
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1589
    :sswitch_0
    return-object p0

    .line 1594
    :sswitch_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-nez v1, :cond_1

    .line 1595
    new-instance v1, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-direct {v1}, Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;-><init>()V

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1597
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1601
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    .line 1602
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    goto :goto_0

    .line 1584
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1468
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;

    move-result-object v0

    return-object v0
.end method

.method public setEdition(Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    .locals 1
    .param p1, "value"    # Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .prologue
    .line 1482
    if-nez p1, :cond_0

    .line 1483
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1485
    :cond_0
    iput-object p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    .line 1486
    return-object p0
.end method

.method public setLastRead(J)Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 1501
    iput-wide p1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    .line 1502
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    .line 1503
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    if-eqz v0, :cond_0

    .line 1556
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->edition_:Lcom/google/apps/dots/proto/client/DotsClient$EditionProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1558
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 1559
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/apps/dots/proto/client/DotsClient$RecentlyRead$RecentlyReadItem;->lastRead_:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1561
    :cond_1
    return-void
.end method
