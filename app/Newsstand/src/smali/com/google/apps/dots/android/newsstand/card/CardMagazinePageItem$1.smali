.class final Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "CardMagazinePageItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem;->makeOnClickListener(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field final synthetic val$pageNumber:I

.field final synthetic val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;I)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iput p3, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$pageNumber:I

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 124
    new-instance v1, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/ArticleClickEvent;->track(Z)V

    .line 126
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->image:I

    .line 127
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;

    .line 128
    .local v0, "attachmentView":Lcom/google/apps/dots/android/newsstand/widget/CacheableAttachmentView;
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-direct {v1, p2, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;-><init>(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$postSummary:Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    .line 129
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/card/CardMagazinePageItem$1;->val$pageNumber:I

    .line 130
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/article/PageLocation;->fromNumber(Ljava/lang/Integer;)Lcom/google/apps/dots/android/newsstand/article/PageLocation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setPageLocation(Lcom/google/apps/dots/android/newsstand/article/PageLocation;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v1

    const/4 v2, 0x0

    .line 131
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setInLiteMode(Z)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v1

    .line 132
    invoke-virtual {v1, v0}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->setTransitionElement(Landroid/view/View;)Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;

    move-result-object v1

    const/16 v2, 0xc8

    .line 133
    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/MagazineReadingIntentBuilder;->startForResult(I)V

    .line 134
    return-void
.end method
