.class Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;
.super Lcom/google/android/libraries/bind/data/BaseDataSetObserver;
.source "MagazineReadingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/BaseDataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 175
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    const/4 v3, -0x2

    # setter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->lastVisualPageSelected:I
    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$002(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)I

    .line 177
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->resolvePostIdIfNeeded()Z
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$100(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Z

    .line 178
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->gotoRightArticle()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$200(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 179
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->markPostAsReadIfNeeded()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$300(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 183
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateAltFormats()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$400(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 184
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->maybeConfigureOrientationPreference()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$500(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 186
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->updateMenu()V
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$600(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)V

    .line 189
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->getRelativePageView(I)Landroid/view/View;
    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$700(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;I)Landroid/view/View;

    move-result-object v0

    .line 190
    .local v0, "centerWidget":Landroid/view/View;
    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    if-eqz v2, :cond_0

    .line 191
    check-cast v0, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;

    .end local v0    # "centerWidget":Landroid/view/View;
    invoke-interface {v0, v4}, Lcom/google/apps/dots/android/newsstand/widget/UserVisibilityHandler;->setUserVisibleHint(Z)V

    .line 196
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postData()Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$800(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    sget v3, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->DK_SECTION_SUMMARY:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;

    .line 198
    .local v1, "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;->hasCorrespondingImageSectionId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 200
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->pager:Landroid/support/v4/view/NSViewPager;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$1000(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Landroid/support/v4/view/NSViewPager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->postSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    invoke-static {v3}, Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;->access$900(Lcom/google/apps/dots/android/newsstand/reading/MagazineReadingFragment;)Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/NSViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 203
    .end local v1    # "sectionSummary":Lcom/google/apps/dots/proto/client/DotsShared$SectionSummary;
    :cond_1
    return-void
.end method
