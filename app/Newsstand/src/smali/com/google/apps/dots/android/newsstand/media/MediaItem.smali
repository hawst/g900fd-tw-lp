.class public Lcom/google/apps/dots/android/newsstand/media/MediaItem;
.super Ljava/lang/Object;
.source "MediaItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final fieldId:Ljava/lang/String;

.field public final offset:I

.field public final postId:Ljava/lang/String;

.field public final value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaItem$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;)V
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;
    .param p2, "offset"    # I
    .param p3, "fieldId"    # Ljava/lang/String;
    .param p4, "value"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    .line 31
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    .line 33
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 55
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;

    .line 57
    .local v0, "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    .line 58
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 61
    .end local v0    # "mediaItem":Lcom/google/apps/dots/android/newsstand/media/MediaItem;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 37
    const-string v0, "postId: %s, fieldId: %s, offset: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->postId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->offset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->value:Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 76
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaItem;->fieldId:Ljava/lang/String;

    goto :goto_0
.end method
