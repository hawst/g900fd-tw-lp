.class public Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
.super Ljava/lang/Object;
.source "EditionSummary.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final READ_NOW:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

.field public static final SAVED:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# instance fields
.field public final appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

.field public final appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

.field public final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->READ_NOW_EDITION:Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->READ_NOW:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 39
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/edition/Edition;->SAVED_EDITION:Lcom/google/apps/dots/android/newsstand/edition/SavedEdition;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->SAVED:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 233
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$1;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$1;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V
    .locals 0
    .param p1, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p2, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p3, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 49
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 50
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 51
    return-void
.end method

.method public static editionSummary(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 4
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 55
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    if-eqz v0, :cond_0

    .line 56
    check-cast p0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    .end local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->topicEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 57
    .restart local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_0
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    if-eqz v0, :cond_1

    .line 58
    check-cast p0, Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;

    .end local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->newsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    goto :goto_0

    .line 59
    .restart local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_1
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    if-eqz v0, :cond_2

    .line 60
    check-cast p0, Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;

    .end local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->magazineEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v0

    goto :goto_0

    .line 61
    .restart local p0    # "edition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    :cond_2
    instance-of v0, p0, Lcom/google/apps/dots/android/newsstand/edition/ReadNowEdition;

    if-eqz v0, :cond_3

    .line 62
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->READ_NOW:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    goto :goto_0

    .line 64
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported Edition type: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getTranslatedAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .locals 6
    .param p0, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    .param p1, "translatedAppFamilyId"    # Ljava/lang/String;
    .param p2, "translatedAppId"    # Ljava/lang/String;
    .param p3, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 274
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasUntranslatedAppFamilyId()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    const/4 v4, 0x1

    .line 274
    :goto_0
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 277
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v3

    .line 278
    .local v3, "translatedAppFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iput-object p1, v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 279
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setUpdateTime(J)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 281
    sget-object v4, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 282
    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->clearUntranslatedAppFamilyId()Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 285
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasShortShareUrl()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 286
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getShortShareUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 287
    .local v2, "shareUri":Landroid/net/Uri;
    const-string v4, "translate"

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->clearQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 288
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setShortShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 290
    .end local v2    # "shareUri":Landroid/net/Uri;
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasLongShareUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 291
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getShortShareUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 292
    .restart local v2    # "shareUri":Landroid/net/Uri;
    const-string v4, "translate"

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->clearQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 293
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setLongShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 316
    .end local v2    # "shareUri":Landroid/net/Uri;
    :cond_2
    :goto_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 317
    .local v0, "childAppIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    iput-object v4, v3, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->childId:[Ljava/lang/String;

    .line 320
    return-object v3

    .line 275
    .end local v0    # "childAppIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "translatedAppFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 296
    .restart local v3    # "translatedAppFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    :cond_4
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setUntranslatedAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 300
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasShortShareUrl()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 301
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getShortShareUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 302
    .restart local v2    # "shareUri":Landroid/net/Uri;
    const-string v4, "translate"

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->clearQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 303
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "translate"

    .line 304
    invoke-virtual {v4, v5, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 305
    .local v1, "newShareUrl":Ljava/lang/String;
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setShortShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 307
    .end local v1    # "newShareUrl":Ljava/lang/String;
    .end local v2    # "shareUri":Landroid/net/Uri;
    :cond_5
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->hasLongShareUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 308
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getLongShareUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 309
    .restart local v2    # "shareUri":Landroid/net/Uri;
    const-string v4, "translate"

    invoke-static {v2, v4}, Lcom/google/apps/dots/android/newsstand/util/UriUtil;->clearQueryParameter(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 310
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "translate"

    .line 311
    invoke-virtual {v4, v5, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 312
    .restart local v1    # "newShareUrl":Ljava/lang/String;
    invoke-virtual {v3, v1}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->setLongShareUrl(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    goto :goto_1
.end method

.method private static getTranslatedAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .locals 4
    .param p0, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p1, "translatedAppFamilyId"    # Ljava/lang/String;
    .param p2, "translatedAppId"    # Ljava/lang/String;
    .param p3, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 337
    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    .line 338
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 337
    :goto_0
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkArgument(Z)V

    .line 340
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v0

    .line 341
    .local v0, "translatedAppSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    iput-object p2, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    .line 342
    iput-object p1, v0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    .line 343
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->setUpdateTime(J)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 345
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v1

    .line 346
    invoke-virtual {v1, p2}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->setAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v1

    .line 345
    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->setAppType(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 348
    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->UNDEFINED_LANGUAGE_CODE:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 349
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clearTranslationCode()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 350
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clearUntranslatedAppFamilyId()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 351
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->clearUntranslatedAppId()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 358
    :goto_1
    return-object v0

    .line 338
    .end local v0    # "translatedAppSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 353
    .restart local v0    # "translatedAppSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    :cond_2
    invoke-virtual {v0, p3}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->setTranslationCode(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 354
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->setUntranslatedAppFamilyId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    .line 355
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->setUntranslatedAppId(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    goto :goto_1
.end method

.method public static magazineEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/MagazineEdition;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 89
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method

.method public static newsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/NewsEdition;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 82
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static {p2}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method

.method public static relatedPostsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/RelatedPostsEdition;

    .prologue
    const/4 v1, 0x0

    .line 77
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method

.method public static searchPostsEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 2
    .param p0, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    .prologue
    const/4 v1, 0x0

    .line 73
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method

.method public static topicEditionSummary(Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 1
    .param p0, "topicEdition"    # Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;
    .param p1, "appSummary"    # Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    .param p2, "appFamilySummary"    # Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .prologue
    .line 69
    new-instance v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 220
    instance-of v2, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 221
    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .line 222
    .local v0, "other":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v3, v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    .line 223
    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 225
    .end local v0    # "other":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    :cond_0
    return v1
.end method

.method public getTranslatedEditionSummary(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .locals 6
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v5, p1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTranslatedEdition(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v0

    .line 189
    .local v0, "newEdition":Lcom/google/apps/dots/android/newsstand/edition/Edition;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    invoke-static {v5, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "translatedAppFamilyId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v5, v5, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appId:Ljava/lang/String;

    invoke-static {v5, p1}, Lcom/google/apps/dots/android/newsstand/model/ObjectId;->addOrReplaceTargetTranslationLanguage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 195
    .local v3, "translatedAppId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-static {v5, v1, v3, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->getTranslatedAppFamilySummary(Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    move-result-object v2

    .line 199
    .local v2, "translatedAppFamilySummary":Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;
    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v5, v1, v3, p1}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->getTranslatedAppSummary(Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    move-result-object v4

    .line 203
    .local v4, "translatedAppSummary":Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;
    new-instance v5, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {v5, v0, v4, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;-><init>(Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;)V

    return-object v5
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public iconSource()Lcom/google/apps/dots/android/newsstand/icon/IconSource;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/apps/dots/android/newsstand/icon/IconSource",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 118
    sget-object v6, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$2;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 164
    new-instance v6, Ljava/lang/IllegalStateException;

    invoke-direct {v6}, Ljava/lang/IllegalStateException;-><init>()V

    throw v6

    .line 122
    :pswitch_0
    sget-object v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;->NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    .line 162
    :goto_0
    return-object v6

    .line 124
    :pswitch_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "coverAttachmentId":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 126
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    invoke-direct {v6, v2}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_0
    sget-object v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;->NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    goto :goto_0

    .line 130
    .end local v2    # "coverAttachmentId":Ljava/lang/String;
    :pswitch_2
    sget-object v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;->SEARCH_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    goto :goto_0

    .line 132
    :pswitch_3
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    .line 133
    .local v1, "context":Landroid/content/Context;
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    sget v7, Lcom/google/android/apps/newsstanddev/R$drawable;->ic_categories_highlights:I

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 134
    invoke-virtual {v8, v1, v9}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v8

    invoke-direct {v6, v7, v9, v8}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(IZI)V

    goto :goto_0

    .line 136
    .end local v1    # "context":Landroid/content/Context;
    :pswitch_4
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v3, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;

    .line 137
    .local v3, "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v3, v6, v9}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->actionBarColor(Landroid/content/Context;Z)I

    move-result v0

    .line 139
    .local v0, "actionBarColor":I
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasIconImage()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 140
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v9, v0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;ZI)V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v4

    .line 143
    .local v4, "curationIconAttachmentId":Ljava/lang/String;
    invoke-static {v4}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 144
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    invoke-direct {v6, v4, v9, v0}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;ZI)V

    goto :goto_0

    .line 148
    :cond_2
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Lcom/google/apps/dots/android/newsstand/icon/ColoredInitialsIcon;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 151
    .end local v0    # "actionBarColor":I
    .end local v3    # "curatedTopicEdition":Lcom/google/apps/dots/android/newsstand/edition/CuratedTopicEdition;
    .end local v4    # "curationIconAttachmentId":Ljava/lang/String;
    :pswitch_5
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getAppType()Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary$AppType;->getType()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_3

    .line 152
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    iget-object v7, v7, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->appFamilyId:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/ColorHelper;->getRssDrawableId(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v6, v7}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(I)V

    goto/16 :goto_0

    .line 154
    :cond_3
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v5

    .line 155
    .local v5, "iconAttachmentId":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 156
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    invoke-direct {v6, v5}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :cond_4
    iget-object v6, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-virtual {v6}, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->getIconAttachmentId()Ljava/lang/String;

    move-result-object v5

    .line 159
    invoke-static {v5}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 160
    new-instance v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;

    invoke-direct {v6, v5}, Lcom/google/apps/dots/android/newsstand/icon/IconId;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 162
    :cond_5
    sget-object v6, Lcom/google/apps/dots/android/newsstand/icon/IconId;->NO_ICON:Lcom/google/apps/dots/android/newsstand/icon/IconId;

    goto/16 :goto_0

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public isTranslated()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasTranslationCode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsTranslation()Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTranslationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->hasLanguageCode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public title(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    sget-object v0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary$2;->$SwitchMap$com$google$apps$dots$android$newsstand$util$ProtoEnum$EditionType:[I

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getType()Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$EditionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 113
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 98
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->highlights_edition_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    .line 100
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->saved_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 105
    :pswitch_2
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :pswitch_3
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/TopicEdition;->getEntity()Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$ClientEntity;->getDescription()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_4
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    check-cast v0, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/edition/SearchPostsEdition;->getQuery()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    :pswitch_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->related_posts_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 214
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appSummary:Lcom/google/apps/dots/proto/client/DotsShared$ApplicationSummary;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/util/ProtoUtil;->encodeBase64(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    return-void
.end method
