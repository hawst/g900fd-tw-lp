.class public Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;
.source "NSErrorViewProvider.java"


# instance fields
.field protected errorMessageData:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSBaseErrorViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getErrorMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSErrorViewProvider;->errorMessageData:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method
