.class public final Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;


# instance fields
.field private appFamilyId_:Ljava/lang/String;

.field private appId_:Ljava/lang/String;

.field private bitField0_:I

.field private isArchived_:Z

.field private isPurchased_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6563
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6564
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 6569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    .line 6591
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    .line 6613
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    .line 6632
    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    .line 6564
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    .locals 3

    .prologue
    .line 6664
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6668
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    return-object v0

    .line 6665
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    :catch_0
    move-exception v1

    .line 6666
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6560
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6673
    if-ne p1, p0, :cond_1

    .line 6677
    :cond_0
    :goto_0
    return v1

    .line 6674
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 6675
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    .line 6676
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 6677
    :goto_2
    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    iget-boolean v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 6676
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    .line 6677
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2
.end method

.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6593
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    return-object v0
.end method

.method public getIsArchived()Z
    .locals 1

    .prologue
    .line 6634
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    return v0
.end method

.method public getIsPurchased()Z
    .locals 1

    .prologue
    .line 6615
    iget-boolean v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 6710
    const/4 v0, 0x0

    .line 6711
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6712
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    .line 6713
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6715
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6716
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    .line 6717
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6719
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6720
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    .line 6721
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6723
    :cond_2
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6724
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    .line 6725
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6727
    :cond_3
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->cachedSize:I

    .line 6728
    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6683
    const/16 v0, 0x11

    .line 6684
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 6685
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 6686
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    if-nez v5, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 6687
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    add-int v0, v2, v1

    .line 6688
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    if-eqz v2, :cond_3

    :goto_3
    add-int v0, v1, v3

    .line 6689
    return v0

    .line 6685
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 6686
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    move v1, v4

    .line 6687
    goto :goto_2

    :cond_3
    move v3, v4

    .line 6688
    goto :goto_3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6736
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 6737
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 6741
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6742
    :sswitch_0
    return-object p0

    .line 6747
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    .line 6748
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    goto :goto_0

    .line 6752
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    .line 6753
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    goto :goto_0

    .line 6757
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    .line 6758
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    goto :goto_0

    .line 6762
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    .line 6763
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    goto :goto_0

    .line 6737
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6560
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;

    move-result-object v0

    return-object v0
.end method

.method public setIsArchived(Z)Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 6637
    iput-boolean p1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    .line 6638
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    .line 6639
    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6694
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6695
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appFamilyId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6697
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6698
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->appId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 6700
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6701
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isPurchased_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 6703
    :cond_2
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6704
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$PurchaseSummary;->isArchived_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 6706
    :cond_3
    return-void
.end method
