.class Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;
.super Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;
.source "ArticleTailList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->makeEditionCard(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

.field final synthetic val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/widget/SafeOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 151
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->val$post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->getAnalyticsScreenName(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->access$400(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;-><init>(Ljava/lang/String;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/EditionClickEvent;->track(Z)V

    .line 152
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    invoke-direct {v0, p2}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;->this$1:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .line 153
    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->setEdition(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/navigation/EditionIntentBuilder;->start()V

    .line 155
    return-void
.end method
