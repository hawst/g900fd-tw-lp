.class public Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;
.super Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;
.source "ArticleTailList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ArticleSubscriptionListRefreshTask"
.end annotation


# instance fields
.field private librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)V
    .locals 1
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .line 103
    sget-object v0, Lcom/google/apps/dots/android/newsstand/async/Queues;->DISK:Lcom/google/apps/dots/android/newsstand/async/Queue;

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/data/AsyncTokenRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    .line 104
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->getLibrarySnapshot()Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    .line 105
    return-void
.end method

.method static synthetic access$400(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;
    .param p1, "x1"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->getAnalyticsScreenName(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAnalyticsScreenName(Lcom/google/apps/dots/proto/client/DotsShared$Post;)Ljava/lang/String;
    .locals 4
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 172
    if-eqz p1, :cond_0

    .line 173
    const-string v0, "%s %s - %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "[Related]"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 174
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getAppName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 173
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeEditionCard(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)Lcom/google/android/libraries/bind/data/Data;
    .locals 10
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    .param p2, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p3, "useDarkTheme"    # Z

    .prologue
    const/4 v5, 0x0

    .line 130
    new-instance v7, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v7}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 131
    .local v7, "editionCard":Lcom/google/android/libraries/bind/data/Data;
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->appFamilySummary:Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;

    iget-object v9, v0, Lcom/google/apps/dots/proto/client/DotsShared$AppFamilySummary;->appFamilyId:Ljava/lang/String;

    .line 132
    .local v9, "id":Ljava/lang/String;
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_ROW_ID:I

    invoke-virtual {v7, v0, v9}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 133
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget-object v1, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->LAYOUTS:[I

    aget v1, v1, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 134
    sget v0, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->EQUALITY_FIELDS:[I
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$200()[I

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isSubscribed(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v2

    .line 137
    .local v2, "subscribed":Z
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->librarySnapshot:Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/edition/LibrarySnapshot;->isPurchased(Lcom/google/apps/dots/android/newsstand/edition/Edition;)Z

    move-result v3

    .line 138
    .local v3, "purchased":Z
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_IS_SUBSCRIBED:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->appContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$300(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iget-object v4, v1, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->account:Landroid/accounts/Account;

    new-instance v8, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;

    invoke-direct {v8, p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    move-object v1, p1

    move v6, p3

    invoke-static/range {v0 .. v8}, Lcom/google/apps/dots/android/newsstand/card/CardSourceListItem;->fillInData(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZLandroid/accounts/Account;ZZLcom/google/android/libraries/bind/data/Data;Landroid/view/View$OnClickListener;)V

    .line 160
    sget v0, Lcom/google/apps/dots/android/newsstand/card/viewgroup/BindingCardViewGroup;->DK_ANALYTICS_EVENT_PROVIDER:I

    new-instance v1, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;

    invoke-direct {v1, p0, p2}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask$2;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 168
    return-object v7
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v4

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->editionSummary(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    move-result-object v1

    .line 110
    .local v1, "editionSummary":Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;
    const/4 v3, 0x0

    .line 111
    .local v3, "useDarkTheme":Z
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 112
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getFormTemplateId()Ljava/lang/String;

    move-result-object v2

    .line 113
    .local v2, "formId":Ljava/lang/String;
    const-string v4, "photo_default"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "photo_gallery"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "video_default"

    .line 114
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 115
    :cond_0
    const/4 v3, 0x1

    .line 118
    .end local v2    # "formId":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 119
    const/4 v0, 0x0

    .line 125
    :cond_2
    :goto_0
    return-object v0

    .line 121
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 122
    .local v0, "cardList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz v1, :cond_2

    .line 123
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    # getter for: Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;
    invoke-static {v4}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->access$100(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;

    move-result-object v4

    invoke-direct {p0, v1, v4, v3}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;->makeEditionCard(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;Lcom/google/apps/dots/proto/client/DotsShared$Post;Z)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
