.class Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;
.super Ljava/lang/Object;
.source "NewsWebView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 436
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->connectivityManager()Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/net/NSConnectivityManager;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->destroyScope:Lcom/google/apps/dots/android/newsstand/async/AsyncScope;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/async/AsyncScope;->token()Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;->this$0:Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView;->onScriptLoadFuture:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6$1;-><init>(Lcom/google/apps/dots/android/newsstand/widget/NewsWebView$6;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/async/AsyncToken;->addCallback(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 448
    :cond_0
    return-void
.end method
