.class public Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "WebPartActivity.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private webPartFragment:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>(Z)V

    .line 23
    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->webPartFragment:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->webPartFragment:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->webPartFragment:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->web_part_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->setContentView(I)V

    .line 37
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->web_part_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->webPartFragment:Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartFragment;

    .line 40
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    sget-object v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v1, "onNewIntent"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 47
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 48
    return-void
.end method
