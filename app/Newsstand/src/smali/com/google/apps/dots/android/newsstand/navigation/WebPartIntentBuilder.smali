.class public Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "WebPartIntentBuilder.java"


# instance fields
.field private appId:Ljava/lang/String;

.field private fieldId:Ljava/lang/String;

.field private localUrl:Ljava/lang/String;

.field private postId:Ljava/lang/String;

.field private sectionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 14
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->appId:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->sectionId:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->postId:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->fieldId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->localUrl:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 7

    .prologue
    .line 51
    const-class v1, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartActivity;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->makeIntent(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v6

    .line 52
    .local v6, "intent":Landroid/content/Intent;
    new-instance v0, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->appId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->sectionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->postId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->fieldId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->localUrl:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .local v0, "webPartState":Lcom/google/apps/dots/android/newsstand/activity/magazines/WebPartState;
    const-string v1, "WebPartFragment_state"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 54
    return-object v6
.end method

.method public setAppId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->appId:Ljava/lang/String;

    .line 26
    return-object p0
.end method

.method public setFieldId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->fieldId:Ljava/lang/String;

    .line 41
    return-object p0
.end method

.method public setLocalUrl(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
    .locals 0
    .param p1, "localUrl"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->localUrl:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public setPostId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
    .locals 0
    .param p1, "postId"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->postId:Ljava/lang/String;

    .line 36
    return-object p0
.end method

.method public setSectionId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/WebPartIntentBuilder;->sectionId:Ljava/lang/String;

    .line 31
    return-object p0
.end method
