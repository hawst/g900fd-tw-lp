.class public final Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Html"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;


# instance fields
.field private bitField0_:I

.field private value_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11372
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11373
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 11378
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    .line 11373
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    .locals 3

    .prologue
    .line 11410
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 11414
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    return-object v0

    .line 11411
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    :catch_0
    move-exception v1

    .line 11412
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11369
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 11419
    if-ne p1, p0, :cond_1

    .line 11422
    :cond_0
    :goto_0
    return v1

    .line 11420
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 11421
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .line 11422
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 11441
    const/4 v0, 0x0

    .line 11442
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 11443
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    .line 11444
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11446
    :cond_0
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->cachedSize:I

    .line 11447
    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11380
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 11426
    const/16 v0, 0x11

    .line 11427
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 11428
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 11429
    return v0

    .line 11428
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 11456
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 11460
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11461
    :sswitch_0
    return-object p0

    .line 11466
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    .line 11467
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->bitField0_:I

    goto :goto_0

    .line 11456
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11369
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11434
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 11435
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->value_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 11437
    :cond_0
    return-void
.end method
