.class public Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;
.super Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;
.source "NSEmptyViewProvider.java"


# instance fields
.field protected emptyMessageData:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V
    .locals 0
    .param p1, "headerType"    # Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/data/NSBaseEmptyViewProvider;-><init>(Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getEmptyMessageData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;->emptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method public setEmptyMessageData(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;
    .locals 0
    .param p1, "emptyMessageData"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/data/NSEmptyViewProvider;->emptyMessageData:Lcom/google/android/libraries/bind/data/Data;

    .line 23
    return-object p0
.end method
