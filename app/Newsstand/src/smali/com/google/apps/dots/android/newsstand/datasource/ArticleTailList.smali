.class public Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;
.super Lcom/google/apps/dots/android/newsstand/data/EventDataList;
.source "ArticleTailList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;
    }
.end annotation


# static fields
.field public static final DK_IS_SUBSCRIBED:I

.field public static final DK_ROW_ID:I

.field private static final EQUALITY_FIELDS:[I


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

.field private final edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

.field private final post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

.field private final subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailList_rowId:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_ROW_ID:I

    .line 43
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->ArticleTailList_isSubscribed:I

    sput v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_IS_SUBSCRIBED:I

    .line 45
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_IS_SUBSCRIBED:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->EQUALITY_FIELDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/apps/dots/android/newsstand/edition/Edition;Lcom/google/apps/dots/proto/client/DotsShared$Post;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "edition"    # Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .param p3, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .prologue
    .line 54
    sget v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->DK_ROW_ID:I

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;-><init>(I)V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->appContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    .line 57
    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    .line 61
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/datasource/DataSources;->combinedSubscriptionsDataList()Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    .line 62
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$1;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$1;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 68
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->setDirty(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->startAutoRefresh()Lcom/google/android/libraries/bind/data/DataList;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/android/newsstand/edition/Edition;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->post:Lcom/google/apps/dots/proto/client/DotsShared$Post;

    return-object v0
.end method

.method static synthetic access$200()[I
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->EQUALITY_FIELDS:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->appContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected equalityFields()[I
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->EQUALITY_FIELDS:[I

    return-object v0
.end method

.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList$ArticleSubscriptionListRefreshTask;-><init>(Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;)V

    return-object v0
.end method

.method protected onRegisterForInvalidation()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onRegisterForInvalidation()V

    .line 75
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 76
    return-void
.end method

.method protected onUnregisterForInvalidation()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/data/EventDataList;->onUnregisterForInvalidation()V

    .line 81
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->combinedSubscriptionsList:Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->subscriptionsObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/home/library/news/CombinedSubscriptionsList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/datasource/ArticleTailList;->setDirty(Z)V

    .line 83
    return-void
.end method
