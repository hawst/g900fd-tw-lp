.class Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;
.super Ljava/lang/Object;
.source "NSOnboardHostFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$PageTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->setUpPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final contentWidthPx:I

.field final peekWidthPx:I

.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)V
    .locals 2
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->play_onboard__onboard_tutorial_page_peek_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->peekWidthPx:I

    .line 133
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/newsstanddev/R$dimen;->play_onboard__onboard_tutorial_page_bg_diameter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->contentWidthPx:I

    return-void
.end method


# virtual methods
.method public transformPage(Landroid/view/View;F)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # F

    .prologue
    const/4 v10, 0x0

    .line 138
    instance-of v7, p1, Lcom/google/android/play/onboard/OnboardTutorialPage;

    if-eqz v7, :cond_3

    .line 140
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$000(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/play/onboard/OnboardPager;->getWidth()I

    move-result v4

    .line 143
    .local v4, "pagerWidthPx":I
    iget v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->contentWidthPx:I

    sub-int v7, v4, v7

    div-int/lit8 v7, v7, 0x2

    iget v8, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->peekWidthPx:I

    add-int v3, v7, v8

    .line 145
    .local v3, "offsetPx":I
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$100(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getViewPosition(Landroid/view/View;)I

    move-result v6

    .line 148
    .local v6, "visualIndex":I
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .line 149
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$300(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$200(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;

    move-result-object v8

    add-int/lit8 v9, v6, -0x1

    invoke-static {v8, v9}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v2

    .line 152
    .local v2, "leftView":Landroid/view/View;
    iget-object v7, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    .line 153
    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;
    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$500(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;

    move-result-object v7

    iget-object v8, p0, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;
    invoke-static {v8}, Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;->access$400(Lcom/google/apps/dots/android/newsstand/onboard/NSOnboardHostFragment;)Lcom/google/android/play/onboard/OnboardPagerAdapter;

    move-result-object v8

    add-int/lit8 v9, v6, 0x1

    invoke-static {v8, v9}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v5

    .line 155
    .local v5, "rightView":Landroid/view/View;
    const/high16 v7, -0x40800000    # -1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-static {p2, v7, v8}, Lcom/google/apps/dots/android/newsstand/util/MathUtil;->clamp(FFF)F

    move-result v0

    .line 156
    .local v0, "clampedPosition":F
    const/4 v1, 0x0

    .line 158
    .local v1, "dX":F
    instance-of v7, v2, Lcom/google/android/play/onboard/OnboardTutorialPage;

    if-eqz v7, :cond_0

    cmpl-float v7, v0, v10

    if-gtz v7, :cond_1

    :cond_0
    instance-of v7, v5, Lcom/google/android/play/onboard/OnboardTutorialPage;

    if-eqz v7, :cond_2

    cmpg-float v7, v0, v10

    if-gez v7, :cond_2

    .line 160
    :cond_1
    neg-int v7, v3

    int-to-float v7, v7

    mul-float v1, v7, v0

    .line 162
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 164
    .end local v0    # "clampedPosition":F
    .end local v1    # "dX":F
    .end local v2    # "leftView":Landroid/view/View;
    .end local v3    # "offsetPx":I
    .end local v4    # "pagerWidthPx":I
    .end local v5    # "rightView":Landroid/view/View;
    .end local v6    # "visualIndex":I
    :cond_3
    return-void
.end method
