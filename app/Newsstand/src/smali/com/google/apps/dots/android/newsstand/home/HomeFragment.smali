.class public Lcom/google/apps/dots/android/newsstand/home/HomeFragment;
.super Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment",
        "<",
        "Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x0

    const-string v1, "HomeFragment_state"

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->home_fragment:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;-><init>(Landroid/os/Parcelable;Ljava/lang/String;I)V

    .line 33
    return-void
.end method

.method private sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V
    .locals 2
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    .prologue
    .line 93
    new-instance v0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;

    iget-object v1, p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;-><init>(Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/HomePageScreen;->track(Z)V

    .line 94
    return-void
.end method

.method private updatePage(Z)V
    .locals 6
    .param p1, "force"    # Z

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 61
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 62
    .local v3, "preExistingFragment":Landroid/support/v4/app/Fragment;
    if-eqz p1, :cond_2

    const/4 v2, 0x0

    .line 63
    .local v2, "newFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    if-nez v2, :cond_1

    .line 64
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 65
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->getFragment(Landroid/content/Context;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 69
    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->setupSubFragmentStateIfNeeded(Landroid/support/v4/app/Fragment;)V

    .line 70
    sget v5, Lcom/google/android/apps/newsstanddev/R$id;->home_page_container:I

    .line 71
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v4, v4, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->name()Ljava/lang/String;

    move-result-object v4

    .line 70
    invoke-virtual {v1, v5, v2, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 72
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 74
    .end local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_1
    return-void

    .end local v2    # "newFragment":Landroid/support/v4/app/Fragment;
    :cond_2
    move-object v2, v3

    .line 62
    goto :goto_0
.end method


# virtual methods
.method protected getHasOptionsMenu()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v1

    .line 109
    .local v1, "fragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 110
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v3, v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v2

    .line 114
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public handleUpcomingResult(Landroid/os/Bundle;)Z
    .locals 3
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 84
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->state()Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v2, v2, Lcom/google/apps/dots/android/newsstand/home/HomePage;->type:Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/home/HomePage$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 85
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    instance-of v2, v0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;

    if-eqz v2, :cond_0

    .line 86
    check-cast v0, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/StatefulFragment;->handleUpcomingResult(Landroid/os/Bundle;)Z

    move-result v2

    .line 88
    :goto_0
    return v2

    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/fragment/RestorableFragment;->handleUpcomingResult(Landroid/os/Bundle;)Z

    move-result v2

    goto :goto_0
.end method

.method protected logd()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-object v0
.end method

.method protected onViewCreated(Landroid/view/View;)V
    .locals 0
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 42
    return-void
.end method

.method protected restoreIntents(Landroid/app/Activity;Landroid/os/Parcelable;)Ljava/util/List;
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "state"    # Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Parcelable;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    instance-of v0, p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    invoke-direct {v2, p1}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;-><init>(Landroid/app/Activity;)V

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    .line 100
    .end local p2    # "state":Landroid/os/Parcelable;
    invoke-virtual {v2, p2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->setHomeFragmentState(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/navigation/HomeIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 99
    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .restart local p2    # "state":Landroid/os/Parcelable;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic updateViews(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    check-cast p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    invoke-virtual {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->updateViews(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V

    return-void
.end method

.method protected updateViews(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V
    .locals 5
    .param p1, "state"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;
    .param p2, "oldState"    # Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 46
    if-eqz p2, :cond_4

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->account:Landroid/accounts/Account;

    invoke-virtual {v3, v4}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v0, v2

    .line 47
    .local v0, "accountChanged":Z
    :goto_0
    if-eqz p2, :cond_0

    iget-object v3, p2, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    iget-object v4, p1, Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;->homePage:Lcom/google/apps/dots/android/newsstand/home/HomePage;

    invoke-virtual {v3, v4}, Lcom/google/apps/dots/android/newsstand/home/HomePage;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    .line 48
    .local v1, "pageChanged":Z
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    .line 49
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->updatePage(Z)V

    .line 50
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/home/HomeFragment;->sendAnalyticsEvent(Lcom/google/apps/dots/android/newsstand/home/HomeFragmentState;)V

    .line 52
    :cond_3
    return-void

    .end local v0    # "accountChanged":Z
    .end local v1    # "pageChanged":Z
    :cond_4
    move v0, v1

    .line 46
    goto :goto_0
.end method
