.class public Lcom/google/apps/dots/android/newsstand/util/MeteredUtil;
.super Ljava/lang/Object;
.source "MeteredUtil.java"


# direct methods
.method public static getMeteredStatusText(Landroid/content/Context;ILcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remainingArticles"    # I
    .param p2, "meteredPolicy"    # Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .prologue
    .line 21
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->free_articles_left_period:I

    .line 22
    .local v1, "time":I
    iget-object v2, p2, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getUnit()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 36
    :goto_0
    iget v2, p2, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    if-gtz v2, :cond_0

    .line 37
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->premium_content:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "html":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    return-object v2

    .line 24
    .end local v0    # "html":Ljava/lang/String;
    :pswitch_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->free_articles_left_today:I

    .line 25
    goto :goto_0

    .line 27
    :pswitch_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->free_articles_left_week:I

    .line 28
    goto :goto_0

    .line 30
    :pswitch_2
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->free_articles_left_month:I

    .line 31
    goto :goto_0

    .line 33
    :pswitch_3
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->free_articles_left_year:I

    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v1, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getMeteredStatusTextPast(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "meteredPolicy"    # Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .prologue
    .line 43
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->zero_free_articles_left_period:I

    .line 44
    .local v1, "time":I
    iget-object v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->period:Lcom/google/apps/dots/proto/client/DotsShared$Period;

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getUnit()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 58
    :goto_0
    iget v2, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    if-gtz v2, :cond_0

    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/newsstanddev/R$string;->premium_content:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "html":Ljava/lang/String;
    :goto_1
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    return-object v2

    .line 46
    .end local v0    # "html":Ljava/lang/String;
    :pswitch_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->zero_free_articles_left_today:I

    .line 47
    goto :goto_0

    .line 49
    :pswitch_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->zero_free_articles_left_week:I

    .line 50
    goto :goto_0

    .line 52
    :pswitch_2
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->zero_free_articles_left_month:I

    .line 53
    goto :goto_0

    .line 55
    :pswitch_3
    sget v1, Lcom/google/android/apps/newsstanddev/R$plurals;->zero_free_articles_left_year:I

    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    .line 61
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 60
    invoke-virtual {v2, v1, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getPeriodDuration(Lcom/google/apps/dots/proto/client/DotsShared$Period;)J
    .locals 8
    .param p0, "period"    # Lcom/google/apps/dots/proto/client/DotsShared$Period;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 76
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getCount()I

    move-result v4

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getUnit()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 80
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 81
    .local v1, "now":Landroid/text/format/Time;
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 82
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 83
    .local v0, "midnight":Landroid/text/format/Time;
    iget v4, v1, Landroid/text/format/Time;->monthDay:I

    iget v5, v1, Landroid/text/format/Time;->month:I

    iget v6, v1, Landroid/text/format/Time;->year:I

    invoke-virtual {v0, v4, v5, v6}, Landroid/text/format/Time;->set(III)V

    .line 84
    invoke-virtual {v1, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 109
    .end local v0    # "midnight":Landroid/text/format/Time;
    .end local v1    # "now":Landroid/text/format/Time;
    :goto_0
    return-wide v4

    .line 87
    :cond_0
    const-wide/16 v2, 0x0

    .line 88
    .local v2, "unitDuration":J
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getUnit()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 106
    const-wide/16 v4, 0x0

    goto :goto_0

    .line 90
    :pswitch_0
    const-wide v2, 0x757b12c00L

    .line 109
    :goto_1
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Period;->getCount()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v4, v2

    goto :goto_0

    .line 93
    :pswitch_1
    const-wide v2, 0x9a7ec800L

    .line 94
    goto :goto_1

    .line 96
    :pswitch_2
    const-wide/32 v2, 0x240c8400

    .line 97
    goto :goto_1

    .line 99
    :pswitch_3
    const-wide/32 v2, 0x5265c00

    .line 100
    goto :goto_1

    .line 102
    :pswitch_4
    const-wide/16 v2, 0x3e8

    .line 103
    goto :goto_1

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getSubscribeHint(Landroid/content/Context;Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;)Landroid/text/Spanned;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "meteredPolicy"    # Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;

    .prologue
    .line 66
    iget v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$MeteredPolicy;->maxArticleCount:I

    if-gtz v1, :cond_0

    .line 67
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->subscription_required:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "html":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    return-object v1

    .line 68
    .end local v0    # "html":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->subscribe_and_keep_reading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static showPinningMeteredContentDialog(Landroid/app/Activity;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 114
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->sections_only_available_to_paid_subscribers:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 116
    sget v1, Lcom/google/android/apps/newsstanddev/R$string;->ok:I

    new-instance v2, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil$1;

    invoke-direct {v2}, Lcom/google/apps/dots/android/newsstand/util/MeteredUtil$1;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 122
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 123
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 124
    return-void
.end method
