.class public Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;
.super Ljava/net/CookieHandler;
.source "NSDefaultCookieManager.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final NO_COOKIES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final defaultHandler:Ljava/net/CookieHandler;

.field private final delegateThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/net/CookieHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 25
    invoke-static {}, Lcom/google/common/collect/ImmutableMap;->of()Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->NO_COOKIES:Ljava/util/Map;

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/net/CookieHandler;)V
    .locals 1
    .param p1, "defaultHandler"    # Ljava/net/CookieHandler;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/net/CookieHandler;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->defaultHandler:Ljava/net/CookieHandler;

    .line 32
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->delegateThreadLocal:Ljava/lang/ThreadLocal;

    .line 33
    return-void
.end method


# virtual methods
.method public callWithCookieHandler(Ljava/net/CookieHandler;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 2
    .param p1, "handler"    # Ljava/net/CookieHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/net/CookieHandler;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    .local p2, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->delegateThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 71
    invoke-interface {p2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    .line 72
    .local v0, "value":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->delegateThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->remove()V

    .line 73
    return-object v0
.end method

.method public get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;
    .locals 5
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "requestHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 38
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->delegateThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/CookieHandler;

    .line 39
    .local v0, "delegate":Ljava/net/CookieHandler;
    if-eqz v0, :cond_0

    .line 40
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "delegating get - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    invoke-virtual {v0, p1, p2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 47
    :goto_0
    return-object v1

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->defaultHandler:Ljava/net/CookieHandler;

    if-eqz v1, :cond_1

    .line 43
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "default get - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 44
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->defaultHandler:Ljava/net/CookieHandler;

    invoke-virtual {v1, p1, p2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 46
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "NO COOKIES GET - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->NO_COOKIES:Ljava/util/Map;

    goto :goto_0
.end method

.method public put(Ljava/net/URI;Ljava/util/Map;)V
    .locals 5
    .param p1, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URI;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 53
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->delegateThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/CookieHandler;

    .line 54
    .local v0, "delegate":Ljava/net/CookieHandler;
    if-eqz v0, :cond_0

    .line 55
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "delegating put - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    invoke-virtual {v0, p1, p2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 63
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->defaultHandler:Ljava/net/CookieHandler;

    if-eqz v1, :cond_1

    .line 58
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "default put - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->defaultHandler:Ljava/net/CookieHandler;

    invoke-virtual {v1, p1, p2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    goto :goto_0

    .line 61
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/http/NSDefaultCookieManager;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "NO COOKIES PUT - %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
