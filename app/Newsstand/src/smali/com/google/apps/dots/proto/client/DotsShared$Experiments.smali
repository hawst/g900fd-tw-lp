.class public final Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Experiments"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Experiments;


# instance fields
.field private bitField0_:I

.field private experimentIds_:Ljava/lang/String;

.field public lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28502
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28503
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 28508
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    .line 28530
    sget-object v0, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 28503
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    .locals 5

    .prologue
    .line 28544
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28548
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 28549
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    iput-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 28550
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 28551
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 28552
    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    move-result-object v4

    aput-object v4, v3, v2

    .line 28550
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 28545
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 28546
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 28556
    .end local v1    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28499
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->clone()Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28561
    if-ne p1, p0, :cond_1

    .line 28565
    :cond_0
    :goto_0
    return v1

    .line 28562
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 28563
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    .line 28564
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 28565
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 28564
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1
.end method

.method public getExperimentIds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28510
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 28597
    const/4 v1, 0x0

    .line 28598
    .local v1, "size":I
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 28599
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    .line 28600
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 28602
    :cond_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-eqz v2, :cond_2

    .line 28603
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 28604
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    if-eqz v0, :cond_1

    .line 28605
    const/4 v5, 0x2

    .line 28606
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 28603
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 28610
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :cond_2
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->cachedSize:I

    .line 28611
    return v1
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 28569
    const/16 v1, 0x11

    .line 28570
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 28571
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    add-int v1, v4, v2

    .line 28572
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-nez v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    .line 28578
    :cond_0
    return v1

    .line 28571
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 28574
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 28575
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 28574
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28575
    :cond_3
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Experiments;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 28619
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 28620
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 28624
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 28625
    :sswitch_0
    return-object p0

    .line 28630
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    .line 28631
    iget v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->bitField0_:I

    or-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->bitField0_:I

    goto :goto_0

    .line 28635
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 28636
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-nez v5, :cond_2

    move v1, v4

    .line 28637
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 28638
    .local v2, "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-eqz v5, :cond_1

    .line 28639
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 28641
    :cond_1
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    .line 28642
    :goto_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 28643
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;-><init>()V

    aput-object v6, v5, v1

    .line 28644
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 28645
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 28642
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 28636
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :cond_2
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v1, v5

    goto :goto_1

    .line 28648
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :cond_3
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    new-instance v6, Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    invoke-direct {v6}, Lcom/google/apps/dots/proto/client/DotsShared$Lab;-><init>()V

    aput-object v6, v5, v1

    .line 28649
    iget-object v5, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 28620
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28499
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$Experiments;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28583
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 28584
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->experimentIds_:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 28586
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    if-eqz v1, :cond_2

    .line 28587
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$Experiments;->lab:[Lcom/google/apps/dots/proto/client/DotsShared$Lab;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 28588
    .local v0, "element":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    if-eqz v0, :cond_1

    .line 28589
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 28587
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 28593
    .end local v0    # "element":Lcom/google/apps/dots/proto/client/DotsShared$Lab;
    :cond_2
    return-void
.end method
