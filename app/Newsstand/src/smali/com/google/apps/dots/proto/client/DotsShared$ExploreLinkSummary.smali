.class public final Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExploreLinkSummary"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;


# instance fields
.field private bitField0_:I

.field private description_:Ljava/lang/String;

.field private entityId_:Ljava/lang/String;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25240
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25241
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 25252
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    .line 25271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    .line 25293
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    .line 25241
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    .locals 3

    .prologue
    .line 25327
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25331
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    return-object v0

    .line 25328
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    :catch_0
    move-exception v1

    .line 25329
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25237
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->clone()Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25336
    if-ne p1, p0, :cond_1

    .line 25341
    :cond_0
    :goto_0
    return v1

    .line 25337
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 25338
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    .line 25339
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 25340
    :goto_1
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 25341
    goto :goto_0

    .line 25339
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    .line 25340
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    .line 25341
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25295
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    return-object v0
.end method

.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25273
    iget-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 25368
    const/4 v0, 0x0

    .line 25369
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 25370
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    .line 25371
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25373
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 25374
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    .line 25375
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25377
    :cond_1
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 25378
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    .line 25379
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25381
    :cond_2
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->cachedSize:I

    .line 25382
    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 25254
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 25345
    const/16 v0, 0x11

    .line 25346
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 25347
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    add-int v0, v1, v3

    .line 25348
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 25349
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 25350
    return v0

    .line 25348
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 25349
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25390
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 25391
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 25395
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 25396
    :sswitch_0
    return-object p0

    .line 25401
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 25402
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 25405
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    .line 25406
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    goto :goto_0

    .line 25408
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    goto :goto_0

    .line 25413
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    .line 25414
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    goto :goto_0

    .line 25418
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    .line 25419
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    goto :goto_0

    .line 25391
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25237
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25355
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 25356
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 25358
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 25359
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->entityId_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 25361
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 25362
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$ExploreLinkSummary;->description_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 25364
    :cond_2
    return-void
.end method
