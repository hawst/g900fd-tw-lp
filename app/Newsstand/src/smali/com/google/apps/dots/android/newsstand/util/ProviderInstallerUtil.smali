.class public Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;
.super Ljava/lang/Object;
.source "ProviderInstallerUtil.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

.field private static final installed:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    const-class v0, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 18
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->installed:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static installIfNeeded()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 27
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkNotMainThread()V

    .line 28
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->installed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    :try_start_0
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/security/ProviderInstaller;->installIfNeeded(Landroid/content/Context;)V

    .line 31
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->installed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "ProviderInstaller failed with exception: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    .end local v0    # "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    :catch_1
    move-exception v0

    .line 39
    .local v0, "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProviderInstallerUtil;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v2, "ProviderInstaller failed with exception: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/common/GooglePlayServicesRepairableException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
