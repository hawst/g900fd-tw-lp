.class public Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;
.super Ljava/lang/Object;
.source "ItemJsonSerializer.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final postDataCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->postDataCache:Landroid/support/v4/util/LruCache;

    .line 43
    return-void
.end method

.method private encodeItems(Ljava/lang/Iterable;Ljava/util/Map;)Lorg/codehaus/jackson/node/ArrayNode;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$Item;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/codehaus/jackson/node/ArrayNode;"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "items":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lcom/google/apps/dots/proto/client/DotsShared$Item;>;"
    .local p2, "fieldTitles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v4, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v4}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v0

    .line 322
    .local v0, "array":Lorg/codehaus/jackson/node/ArrayNode;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .line 323
    .local v1, "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encode(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    .line 324
    .local v2, "itemObject":Lorg/codehaus/jackson/node/ObjectNode;
    if-eqz p2, :cond_0

    .line 326
    iget-object v5, v1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 327
    .local v3, "title":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 328
    const-string v5, "title"

    invoke-virtual {v2, v5, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .end local v3    # "title":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, v2}, Lorg/codehaus/jackson/node/ArrayNode;->add(Lorg/codehaus/jackson/JsonNode;)V

    goto :goto_0

    .line 333
    .end local v1    # "item":Lcom/google/apps/dots/proto/client/DotsShared$Item;
    .end local v2    # "itemObject":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_1
    return-object v0
.end method

.method private encodePostData(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 11
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p2, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    .line 235
    sget-object v6, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v6}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v3

    .line 236
    .local v3, "object":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v6, "postId"

    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v1, 0x0

    .line 238
    .local v1, "fieldTitles":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 239
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 240
    iget-object v7, p2, Lcom/google/apps/dots/proto/client/DotsShared$Form;->field:[Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;

    array-length v8, v7

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v8, :cond_0

    aget-object v0, v7, v6

    .line 241
    .local v0, "field":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->getFieldId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v1, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 244
    .end local v0    # "field":Lcom/google/apps/dots/proto/client/DotsShared$Form$Field;
    :cond_0
    iget-object v6, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->item:[Lcom/google/apps/dots/proto/client/DotsShared$Item;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {p0, v6, v1}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeItems(Ljava/lang/Iterable;Ljava/util/Map;)Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v2

    .line 245
    .local v2, "items":Lorg/codehaus/jackson/node/ArrayNode;
    const-string v6, "items"

    invoke-virtual {v3, v6, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 247
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasSummary()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 248
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v4

    .line 249
    .local v4, "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    sget-object v6, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v6}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v5

    .line 250
    .local v5, "summary":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasPrimaryImage()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 251
    const-string v6, "primaryImage"

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getPrimaryImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 253
    :cond_1
    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->hasSourceIconId()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 254
    const-string v6, "sourceIconId"

    invoke-virtual {v4}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getSourceIconId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_2
    const-string v6, "summary"

    invoke-virtual {v3, v6, v5}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 259
    .end local v4    # "postSummary":Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;
    .end local v5    # "summary":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->hasTextDirection()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 260
    const-string v6, "textDirection"

    .line 261
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getTextDirection()I

    move-result v7

    invoke-static {v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$TextDirection;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 260
    invoke-virtual {v3, v6, v7}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_4
    return-object v3
.end method

.method private static makePostDataKey(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Ljava/lang/String;
    .locals 4
    .param p0, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p1, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    .line 204
    invoke-static {p0}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    .line 207
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 208
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getUpdated()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->when:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 209
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$Post;->getTranslationCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/apps/dots/proto/client/DotsShared$Form;->formId:Ljava/lang/String;

    .line 210
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 211
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Form;->getUpdated()Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/apps/dots/proto/client/DotsShared$UserWhen;->when:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public encode(Lcom/google/apps/dots/proto/client/DotsShared$Item;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 9
    .param p1, "item"    # Lcom/google/apps/dots/proto/client/DotsShared$Item;

    .prologue
    .line 156
    sget-object v4, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v4}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 157
    .local v0, "object":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v4, "fieldId"

    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->fieldId:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v4, "origin"

    iget v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->origin:I

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemOrigin;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v4, "type"

    invoke-static {p1}, Lcom/google/apps/dots/android/newsstand/model/ItemUtil;->getItemType(Lcom/google/apps/dots/proto/client/DotsShared$Item;)I

    move-result v5

    invoke-static {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$ItemType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    sget-object v4, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v4}, Lorg/codehaus/jackson/node/JsonNodeFactory;->arrayNode()Lorg/codehaus/jackson/node/ArrayNode;

    move-result-object v3

    .line 162
    .local v3, "values":Lorg/codehaus/jackson/node/ArrayNode;
    iget-object v5, p1, Lcom/google/apps/dots/proto/client/DotsShared$Item;->value:[Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_b

    aget-object v1, v5, v4

    .line 163
    .local v1, "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    sget-object v7, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v7}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    .line 164
    .local v2, "valueObj":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasAudio()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 165
    const-string v7, "audio"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getAudio()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeAudio(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 167
    :cond_0
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasDateTime()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 168
    const-string v7, "dateTime"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getDateTime()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeDateTime(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 170
    :cond_1
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasHtml()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 171
    const-string v7, "html"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getHtml()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeHtml(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 173
    :cond_2
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasImage()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 174
    const-string v7, "image"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getImage()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 176
    :cond_3
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasLocation()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 177
    const-string v7, "location"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getLocation()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeLocation(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 179
    :cond_4
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasNumber()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 180
    const-string v7, "number"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getNumber()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeNumber(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 182
    :cond_5
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasText()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 183
    const-string v7, "text"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getText()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeText(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 185
    :cond_6
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasUrl()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 186
    const-string v7, "url"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getUrl()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeUrl(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 188
    :cond_7
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasVideo()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 189
    const-string v7, "video"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 191
    :cond_8
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasStreamingVideo()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 192
    const-string v7, "streamingVideo"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getStreamingVideo()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeStreamingVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 194
    :cond_9
    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->hasProduct()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 195
    const-string v7, "product"

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;->getProduct()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeProduct(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 197
    :cond_a
    invoke-virtual {v3, v2}, Lorg/codehaus/jackson/node/ArrayNode;->add(Lorg/codehaus/jackson/JsonNode;)V

    .line 162
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 199
    .end local v1    # "value":Lcom/google/apps/dots/proto/client/DotsShared$Item$Value;
    .end local v2    # "valueObj":Lorg/codehaus/jackson/node/ObjectNode;
    :cond_b
    const-string v4, "values"

    invoke-virtual {v0, v4, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 200
    return-object v0
.end method

.method public encodeAudio(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "audio"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;

    .prologue
    .line 46
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 47
    .local v0, "audioObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "attachmentId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getAttachmentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v1, "originalUri"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getOriginalUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "duration"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getDuration()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 50
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->hasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const-string v1, "thumbnail"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 53
    :cond_0
    const-string v1, "caption"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Audio;->getCaption()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-object v0
.end method

.method public encodeDateTime(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 4
    .param p1, "dateTime"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;

    .prologue
    .line 58
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 59
    .local v0, "dateTimeObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "value"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->getValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "relDate"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$DateTime;->getValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/apps/dots/android/newsstand/util/StringUtil;->relativePastTimeString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-object v0
.end method

.method public encodeHtml(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "html"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;

    .prologue
    .line 65
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 66
    .local v0, "htmlObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "value"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Html;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-object v0
.end method

.method public encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "image"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    .prologue
    .line 71
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 72
    .local v0, "imageObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "attachmentId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttachmentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v1, "originalUri"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getOriginalUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v1, "width"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 75
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 76
    const-string v1, "caption"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getCaption()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "attribution"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;->getAttribution()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-object v0
.end method

.method public encodeLocation(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 4
    .param p1, "location"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;

    .prologue
    .line 82
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 83
    .local v0, "locObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "latitude"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;D)V

    .line 84
    const-string v1, "longitude"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;D)V

    .line 85
    const-string v1, "address"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v1, "radius"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getRadius()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;D)V

    .line 87
    const-string v2, "unit"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getUnit()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const-string v1, "MILES"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->hasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const-string v1, "thumbnail"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Location;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 91
    :cond_0
    return-object v0

    .line 87
    :cond_1
    const-string v1, "KILOMETERS"

    goto :goto_0
.end method

.method public encodeNumber(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 4
    .param p1, "number"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;

    .prologue
    .line 95
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 96
    .local v0, "numberObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "value"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Number;->getValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;J)V

    .line 97
    return-object v0
.end method

.method public encodeProduct(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "product"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;

    .prologue
    .line 138
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 139
    .local v0, "prodObj":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->hasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    const-string v1, "thumbnail"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 142
    :cond_0
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    const-string v1, "url"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->getUrl()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeUrl(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 145
    :cond_1
    const-string v1, "name"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v1, "description"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v1, "query"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Product;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-object v0
.end method

.method public encodeStreamingVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "video"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;

    .prologue
    .line 126
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 127
    .local v0, "videoObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "attachmentId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getAttachmentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v1, "originalUri"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getOriginalUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v1, "width"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 130
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 131
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->hasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const-string v1, "thumbnail"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$StreamingVideo;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 134
    :cond_0
    return-object v0
.end method

.method public encodeText(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "text"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;

    .prologue
    .line 101
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 102
    .local v0, "textObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "value"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Text;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    return-object v0
.end method

.method public encodeUrl(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 3
    .param p1, "url"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;

    .prologue
    .line 107
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 108
    .local v0, "urlObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v1, "href"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Url;->getHref()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-object v0
.end method

.method public encodeVideo(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;)Lorg/codehaus/jackson/node/ObjectNode;
    .locals 4
    .param p1, "video"    # Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;

    .prologue
    .line 113
    sget-object v1, Lorg/codehaus/jackson/node/JsonNodeFactory;->instance:Lorg/codehaus/jackson/node/JsonNodeFactory;

    invoke-virtual {v1}, Lorg/codehaus/jackson/node/JsonNodeFactory;->objectNode()Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v0

    .line 114
    .local v0, "videoObj":Lorg/codehaus/jackson/node/ObjectNode;
    const-string v2, "serviceType"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getServiceType()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    const-string v1, "YOUTUBE"

    :goto_0
    invoke-virtual {v0, v2, v1}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "serviceId"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getServiceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v1, "width"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 118
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;I)V

    .line 119
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->hasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    const-string v1, "thumbnail"

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Video;->getThumbnail()Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodeImage(Lcom/google/apps/dots/proto/client/DotsShared$Item$Value$Image;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/node/ObjectNode;->put(Ljava/lang/String;Lorg/codehaus/jackson/JsonNode;)Lorg/codehaus/jackson/JsonNode;

    .line 122
    :cond_0
    return-object v0

    .line 114
    :cond_1
    const-string v1, "VIMEO"

    goto :goto_0
.end method

.method public getEncodedPostData(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Ljava/lang/String;
    .locals 8
    .param p1, "post"    # Lcom/google/apps/dots/proto/client/DotsShared$Post;
    .param p2, "form"    # Lcom/google/apps/dots/proto/client/DotsShared$Form;

    .prologue
    .line 217
    const-string v3, "ItemJsonSerializer.getEncodedPostData()"

    invoke-static {v3}, Landroid/os/StrictMode;->noteSlowCall(Ljava/lang/String;)V

    .line 218
    if-nez p2, :cond_1

    .line 221
    sget-object v3, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v4, "Requesting an post without a form for postId: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/google/apps/dots/proto/client/DotsShared$Post;->postId:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    const/4 v2, 0x0

    .line 231
    :cond_0
    :goto_0
    return-object v2

    .line 224
    :cond_1
    invoke-static {p1, p2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->makePostDataKey(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->postDataCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 226
    .local v2, "result":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 227
    invoke-direct {p0, p1, p2}, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->encodePostData(Lcom/google/apps/dots/proto/client/DotsShared$Post;Lcom/google/apps/dots/proto/client/DotsShared$Form;)Lorg/codehaus/jackson/node/ObjectNode;

    move-result-object v1

    .line 228
    .local v1, "node":Lorg/codehaus/jackson/node/ObjectNode;
    invoke-virtual {v1}, Lorg/codehaus/jackson/node/ObjectNode;->toString()Ljava/lang/String;

    move-result-object v2

    .line 229
    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/model/ItemJsonSerializer;->postDataCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, v0, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
