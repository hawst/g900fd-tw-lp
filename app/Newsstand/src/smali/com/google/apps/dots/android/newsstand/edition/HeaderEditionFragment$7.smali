.class Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;
.super Lcom/google/apps/dots/android/newsstand/async/NullingCallback;
.source "HeaderEditionFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->updateActionBarTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/apps/dots/android/newsstand/async/NullingCallback",
        "<",
        "Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

.field final synthetic val$appContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/async/NullingCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 3
    .param p1, "editionSummary"    # Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    .prologue
    .line 390
    if-eqz p1, :cond_1

    .line 391
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->val$appContext:Landroid/content/Context;

    invoke-virtual {p1, v2}, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->title(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 392
    .local v0, "editionTitle":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getNSActivity()Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/apps/dots/android/newsstand/util/A11yUtil;->updateContentDescriptionForActivityContentView(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Ljava/lang/String;)V

    .line 400
    .end local v0    # "editionTitle":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 394
    :cond_1
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$800(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 395
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    # invokes: Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->edition()Lcom/google/apps/dots/android/newsstand/edition/Edition;
    invoke-static {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->access$800(Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;)Lcom/google/apps/dots/android/newsstand/edition/Edition;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/Edition;->getTitleHint()Ljava/lang/String;

    move-result-object v1

    .line 396
    .local v1, "editionTitleHint":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 397
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->this$0:Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/activity/NSActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 387
    check-cast p1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/edition/HeaderEditionFragment$7;->onSuccess(Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V

    return-void
.end method
