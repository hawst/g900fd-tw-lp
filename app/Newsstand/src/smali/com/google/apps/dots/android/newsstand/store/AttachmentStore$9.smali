.class Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;
.super Ljava/lang/Object;
.source "AttachmentStore.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->getDefaultTransform(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
        "Lcom/google/apps/dots/android/newsstand/server/Transform;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

.field final synthetic val$attachmentObjectId:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

.field final synthetic val$onlyIfCached:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;ZLcom/google/apps/dots/android/newsstand/async/AsyncToken;Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$onlyIfCached:Z

    iput-object p3, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    iput-object p4, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$attachmentObjectId:Lcom/google/apps/dots/proto/client/DotsShared$ObjectIdProto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/android/newsstand/server/Transform;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 522
    .line 523
    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getSyncImageTransform()I

    move-result v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->fromProto(I)Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    move-result-object v0

    .line 524
    .local v0, "syncImageTransform":Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ZOOMABLE:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    if-ne v0, v1, :cond_0

    .line 525
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->zoomTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$800(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 533
    :goto_0
    return-object v1

    .line 526
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    if-ne v0, v1, :cond_1

    .line 527
    sget-object v1, Lcom/google/apps/dots/android/newsstand/server/Transform;->ORIGINAL:Lcom/google/apps/dots/android/newsstand/server/Transform;

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 528
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;->COVER:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$SyncImageTransform;

    if-ne v0, v1, :cond_2

    .line 529
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->coverTransform:Lcom/google/apps/dots/android/newsstand/server/Transform;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$900(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/server/Transform;

    move-result-object v1

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->immediateFuture(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 533
    :cond_2
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$onlyIfCached:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 535
    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$1000(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getFormId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;->getAvailable(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 536
    :goto_1
    new-instance v2, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;

    invoke-direct {v2, p0}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9$1;-><init>(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;)V

    .line 533
    invoke-static {v1, v2}, Lcom/google/apps/dots/android/newsstand/async/futures/Async;->transform(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/base/Function;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0

    .line 535
    :cond_3
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->this$0:Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;

    .line 536
    # getter for: Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->formStore:Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;->access$1000(Lcom/google/apps/dots/android/newsstand/store/AttachmentStore;)Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-virtual {p1}, Lcom/google/apps/dots/proto/client/DotsShared$Section;->getFormId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/apps/dots/android/newsstand/store/cache/FormStore;->getAny(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 518
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/AttachmentStore$9;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
