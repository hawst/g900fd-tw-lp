.class public Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;
.super Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;
.source "OnboardQuizSubmittedEvent.java"


# instance fields
.field private final itemsAccepted:I

.field private final itemsRejected:I

.field private final itemsTotal:I

.field private final screen:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;III)V
    .locals 1
    .param p1, "screen"    # Ljava/lang/String;
    .param p2, "itemsAccepted"    # I
    .param p3, "itemsRejected"    # I
    .param p4, "itemsTotal"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase;-><init>()V

    .line 27
    const-string v0, "Onboard-Curations-Quiz"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Onboard-Magazines-Offers"

    .line 28
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 27
    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 29
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->screen:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsAccepted:I

    .line 31
    iput p3, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsRejected:I

    .line 32
    iput p4, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsTotal:I

    .line 33
    return-void

    .line 28
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected fillAnalyticsEvent(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 2
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 48
    const-string v0, "Onboard Quiz Submitted"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setAction(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 49
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsAccepted:I

    if-ltz v0, :cond_0

    .line 50
    const-string v0, "ItemsAccepted"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsAccepted:I

    .line 51
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsRejected:I

    if-ltz v0, :cond_1

    .line 54
    const-string v0, "ItemsRejected"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsRejected:I

    .line 55
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_1
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsTotal:I

    if-ltz v0, :cond_2

    .line 58
    const-string v0, "ItemsTotal"

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->itemsTotal:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->appendNameValuePair(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    return-object p1
.end method

.method protected fillAnalyticsEventCategory(Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;Z)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .locals 1
    .param p1, "event"    # Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;
    .param p2, "publisherEvent"    # Z

    .prologue
    .line 66
    const-string v0, "Internal"

    invoke-virtual {p1, v0}, Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;->setCategory(Ljava/lang/String;)Lcom/google/apps/dots/proto/client/DotsShared$AnalyticsEvent;

    .line 67
    return-object p1
.end method

.method protected getPublisherTrackingId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 37
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScreen()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/apps/dots/android/newsstand/analytics/trackable/AnalyticsBase$AnalyticsEventResolveException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/analytics/trackable/OnboardQuizSubmittedEvent;->screen:Ljava/lang/String;

    return-object v0
.end method
