.class public Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;
.super Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;
.source "RoundTopicLogo.java"


# instance fields
.field private backgroundColor:I

.field private final fallbackColor:I

.field private initialBackgroundColor:I

.field private initialInitials:Ljava/lang/String;

.field private initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/widget/NSRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$layout;->round_topic_logo:I

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->fallbackColor:I

    .line 39
    sget-object v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundTopicLogo:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 41
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->initialsView:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    .line 43
    if-nez v0, :cond_0

    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->fallbackColor:I

    .line 44
    :goto_0
    iput v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialBackgroundColor:I

    .line 45
    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    iput-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialInitials:Ljava/lang/String;

    .line 46
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    iget v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialBackgroundColor:I

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setBackgroundColor(I)V

    .line 49
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialInitials:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->setInitials(Ljava/lang/CharSequence;)V

    .line 50
    return-void

    .line 43
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundTopicLogo_backgroundColor:I

    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->fallbackColor:I

    .line 44
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    goto :goto_0

    .line 45
    :cond_1
    sget v1, Lcom/google/android/apps/newsstanddev/R$styleable;->RoundTopicLogo_initials:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->backgroundColor:I

    return v0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 57
    iget v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->backgroundColor:I

    if-eq v0, p1, :cond_0

    .line 58
    iput p1, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->backgroundColor:I

    .line 59
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->updateInitialsBackground()V

    .line 61
    :cond_0
    return-void
.end method

.method public setInitials(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "initials"    # Ljava/lang/CharSequence;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method protected updateInitialsBackground()V
    .locals 4

    .prologue
    .line 68
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    sget v3, Lcom/google/android/apps/newsstanddev/R$drawable;->solid_color_circle_bg:I

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->setBackgroundResource(I)V

    .line 69
    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->initialsView:Lcom/google/apps/dots/android/newsstand/widget/NSTextView;

    invoke-virtual {v2}, Lcom/google/apps/dots/android/newsstand/widget/NSTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 70
    .local v0, "background":Landroid/graphics/drawable/LayerDrawable;
    sget v2, Lcom/google/android/apps/newsstanddev/R$id;->solid_circle:I

    .line 71
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 72
    .local v1, "circle":Landroid/graphics/drawable/GradientDrawable;
    iget v2, p0, Lcom/google/apps/dots/android/newsstand/widget/RoundTopicLogo;->backgroundColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 73
    return-void
.end method
