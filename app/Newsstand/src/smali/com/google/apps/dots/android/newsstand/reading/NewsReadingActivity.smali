.class public Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;
.super Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;
.source "NewsReadingActivity.java"


# instance fields
.field private newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;-><init>()V

    return-void
.end method

.method private handleExtras(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 49
    if-nez p1, :cond_0

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->handleExtras(Landroid/os/Bundle;)Z

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarDisplayOptions()I
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0xc

    return v0
.end method

.method public getHelpFeedbackInfo()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getHelpFeedbackInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getIAPConversionEventProvider()Lcom/google/apps/dots/android/newsstand/analytics/trackable/InAppPurchaseConversionEventProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPrimaryVisibleFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    return-object v0
.end method

.method public getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->getPrimaryVisibleFragment()Lcom/google/apps/dots/android/newsstand/fragment/NSFragment;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;->handleOnBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-super {p0}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->news_reading_activity:I

    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->setContentView(I)V

    .line 30
    sget v0, Lcom/google/android/apps/newsstanddev/R$id;->news_article_pager_fragment:I

    invoke-static {p0, v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getFragment(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    iput-object v0, p0, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->newsArticlePagerFragment:Lcom/google/apps/dots/android/newsstand/reading/NewsArticlePagerFragment;

    .line 31
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/apps/dots/android/newsstand/reading/NewsReadingActivity;->handleExtras(Landroid/os/Bundle;)V

    .line 37
    invoke-super {p0, p1}, Lcom/google/apps/dots/android/newsstand/activity/NavigationDrawerActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 38
    return-void
.end method
