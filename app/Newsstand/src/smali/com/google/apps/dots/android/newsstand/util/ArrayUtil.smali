.class public Lcom/google/apps/dots/android/newsstand/util/ArrayUtil;
.super Lcom/google/android/play/utils/collections/Arrays;
.source "ArrayUtil.java"


# direct methods
.method public static final indexOf([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .param p0, "array"    # [Ljava/lang/Object;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 16
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    .line 17
    aget-object v0, p0, v1

    .line 18
    .local v0, "current":Ljava/lang/Object;
    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    .end local v0    # "current":Ljava/lang/Object;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 16
    .restart local v0    # "current":Ljava/lang/Object;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 22
    .end local v0    # "current":Ljava/lang/Object;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method
