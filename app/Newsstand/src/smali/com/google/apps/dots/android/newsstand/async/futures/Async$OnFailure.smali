.class public abstract Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;
.super Ljava/lang/Object;
.source "Async.java"

# interfaces
.implements Lcom/google/apps/dots/android/newsstand/async/futures/FTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/async/futures/Async;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnFailure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/apps/dots/android/newsstand/async/futures/FTransform",
        "<TI;TI;>;"
    }
.end annotation


# instance fields
.field future:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure<TI;>;"
    .local p1, "future":Lcom/google/common/util/concurrent/ListenableFuture;, "Lcom/google/common/util/concurrent/ListenableFuture<+TI;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 122
    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+TI;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;, "Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure<TI;>;"
    .local p1, "input":Ljava/lang/Object;, "TI;"
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/async/futures/Async$OnFailure;->future:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0
.end method
