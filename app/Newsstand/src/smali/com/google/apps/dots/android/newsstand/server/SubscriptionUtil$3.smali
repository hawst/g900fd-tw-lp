.class final Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;
.super Ljava/lang/Object;
.source "SubscriptionUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil;->removeSubscription(Lcom/google/apps/dots/android/newsstand/activity/NSActivity;Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;


# direct methods
.method constructor <init>(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;->val$account:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;->val$account:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/server/SubscriptionUtil$3;->val$editionSummary:Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;

    iget-object v1, v1, Lcom/google/apps/dots/android/newsstand/edition/EditionSummary;->edition:Lcom/google/apps/dots/android/newsstand/edition/Edition;

    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/util/OffersUtil;->clearOffersLocallyAcceptedForEdition(Landroid/accounts/Account;Lcom/google/apps/dots/android/newsstand/edition/Edition;)V

    .line 161
    return-void
.end method
