.class public Lcom/google/apps/dots/android/newsstand/logging/Logd;
.super Lcom/google/android/libraries/bind/logging/Logd;
.source "Logd.java"


# static fields
.field private static localLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;

    invoke-direct {v0}, Lcom/google/apps/dots/android/newsstand/logging/LocalLogHandler;-><init>()V

    sput-object v0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->localLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/logging/Logd;-><init>(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public static get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/google/apps/dots/android/newsstand/logging/Logd;"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 22
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;

    .line 27
    :goto_0
    return-object v1

    .line 25
    :cond_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/logging/Logd;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;-><init>(Ljava/lang/String;)V

    .line 26
    .local v0, "logger":Lcom/google/apps/dots/android/newsstand/logging/Logd;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 27
    goto :goto_0
.end method

.method private static varargs safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .param p0, "optThrowable"    # Ljava/lang/Throwable;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 89
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p2

    if-nez v1, :cond_2

    .end local p1    # "msg":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "formatted":Ljava/lang/String;
    if-eqz p0, :cond_1

    .line 92
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :cond_1
    return-object v0

    .line 90
    .end local v0    # "formatted":Ljava/lang/String;
    .restart local p1    # "msg":Ljava/lang/String;
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public varargs di(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/newsstanddev/R$bool;->enable_internal_logging:I

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    sget-object v0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_1
    return-void
.end method

.method public bridge synthetic enable()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->enable()Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method public enable()Lcom/google/apps/dots/android/newsstand/logging/Logd;
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/libraries/bind/logging/Logd;->enable()Lcom/google/android/libraries/bind/logging/Logd;

    .line 37
    return-object p0
.end method

.method public varargs l(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->l(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method public varargs l(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x4

    .line 50
    invoke-static {p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->localLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public varargs li(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->li(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    return-void
.end method

.method public varargs li(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x4

    .line 64
    invoke-static {p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$bool;->enable_internal_logging:I

    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/NSDepend;->getBooleanResource(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    :cond_0
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_1
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->localLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public varargs ll(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->ll(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method public varargs ll(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v3, 0x4

    .line 78
    invoke-static {p1, p2, p3}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "msg":Ljava/lang/String;
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-object v1, Lcom/google/apps/dots/android/newsstand/logging/Logd;->localLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/logging/Logd;->tag:Ljava/lang/String;

    invoke-interface {v1, v3, v2, v0}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 81
    return-void
.end method
