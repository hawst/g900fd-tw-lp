.class Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;
.super Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;
.source "PostUrlMatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;->apply(Lcom/google/apps/dots/android/newsstand/store/MutationResponse;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;

.field final synthetic val$result:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;[Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;->this$0:Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;->val$result:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/model/nodes/SimpleNodeVisitor;-><init>()V

    return-void
.end method


# virtual methods
.method public visit(Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;)V
    .locals 3
    .param p1, "traversal"    # Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;
    .param p2, "node"    # Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;

    .prologue
    .line 41
    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;->this$0:Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;

    iget-object v0, v0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1;->val$url:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->getExternalPostUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/model/PostUrlMatcher$1$1;->val$result:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/apps/dots/proto/client/DotsSyncV3$Node;->getPostSummary()Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;

    move-result-object v2

    iget-object v2, v2, Lcom/google/apps/dots/proto/client/DotsShared$PostSummary;->postId:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 44
    invoke-interface {p1}, Lcom/google/apps/dots/android/newsstand/model/nodes/NodeTraversal;->finish()V

    .line 47
    :cond_0
    return-void
.end method
