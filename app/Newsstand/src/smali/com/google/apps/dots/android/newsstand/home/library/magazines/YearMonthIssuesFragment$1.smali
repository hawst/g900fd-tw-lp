.class Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;
.super Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;
.source "YearMonthIssuesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->setupHeaderListLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-direct {p0, p2}, Lcom/google/apps/dots/android/newsstand/widget/BaseConfigurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 89
    new-instance v0, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 90
    .local v0, "background":Landroid/view/View;
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$color;->app_color_material:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 91
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    sget-object v2, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->EDITION_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v3, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {v3}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    invoke-virtual {p2}, Landroid/view/ViewGroup;->requestLayout()V

    .line 94
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 84
    sget v0, Lcom/google/android/apps/newsstanddev/R$layout;->year_month_issues_fragment_content:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 85
    return-void
.end method

.method protected getHeaderHeight()I
    .locals 2

    .prologue
    .line 98
    sget-object v0, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->LIBRARY_HEADER:Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment$1;->this$0:Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/home/library/magazines/YearMonthIssuesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/card/CardHeaderSpacer$HeaderType;->getHeightPx(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method
