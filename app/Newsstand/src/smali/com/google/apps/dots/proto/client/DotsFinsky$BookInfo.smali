.class public final Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsFinsky.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsFinsky;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BookInfo"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;


# instance fields
.field public authorName:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 488
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 492
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    .line 489
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    .locals 3

    .prologue
    .line 504
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_0

    .line 509
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    invoke-virtual {v2}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    .line 511
    :cond_0
    return-object v0

    .line 505
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    :catch_0
    move-exception v1

    .line 506
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 485
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->clone()Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 516
    if-ne p1, p0, :cond_0

    const/4 v1, 0x1

    .line 519
    :goto_0
    return v1

    .line 517
    :cond_0
    instance-of v1, p1, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 518
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    .line 519
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    iget-object v2, v0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    .line 545
    const/4 v2, 0x0

    .line 546
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 547
    const/4 v0, 0x0

    .line 548
    .local v0, "dataSize":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 550
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v0, v6

    .line 548
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 552
    .end local v1    # "element":Ljava/lang/String;
    :cond_0
    add-int/2addr v2, v0

    .line 553
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 555
    .end local v0    # "dataSize":I
    :cond_1
    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->cachedSize:I

    .line 556
    return v2
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 523
    const/16 v1, 0x11

    .line 524
    .local v1, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/lit16 v1, v2, 0x20f

    .line 525
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    if-nez v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    .line 531
    :cond_0
    return v1

    .line 527
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 528
    mul-int/lit8 v3, v1, 0x1f

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    add-int v1, v3, v2

    .line 527
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 528
    :cond_2
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 565
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 569
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 570
    :sswitch_0
    return-object p0

    .line 575
    :sswitch_1
    const/16 v4, 0xa

    invoke-static {p1, v4}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 576
    .local v0, "arrayLength":I
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v1, v4

    .line 577
    .local v1, "i":I
    add-int v4, v1, v0

    new-array v2, v4, [Ljava/lang/String;

    .line 578
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    invoke-static {v4, v6, v2, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 579
    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    .line 580
    :goto_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_1

    .line 581
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 582
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 580
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 585
    :cond_1
    iget-object v4, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_0

    .line 565
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 485
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 536
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 537
    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsFinsky$BookInfo;->authorName:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 538
    .local v0, "element":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 537
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 541
    .end local v0    # "element":Ljava/lang/String;
    :cond_0
    return-void
.end method
