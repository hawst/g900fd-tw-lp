.class public final Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DotsShared.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Screenshot"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;


# instance fields
.field private bitField0_:I

.field private device_:I

.field private url_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23765
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    sput-object v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->EMPTY_ARRAY:[Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23766
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 23776
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    .line 23795
    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    .line 23766
    return-void
.end method


# virtual methods
.method public final clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    .locals 3

    .prologue
    .line 23828
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23832
    .local v0, "cloned":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    return-object v0

    .line 23829
    .end local v0    # "cloned":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    :catch_0
    move-exception v1

    .line 23830
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23762
    invoke-virtual {p0}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->clone()Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23837
    if-ne p1, p0, :cond_1

    .line 23841
    :cond_0
    :goto_0
    return v1

    .line 23838
    :cond_1
    instance-of v3, p1, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 23839
    check-cast v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    .line 23840
    .local v0, "other":Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    iget v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    iget v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    .line 23841
    goto :goto_0

    .line 23840
    :cond_4
    iget-object v3, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    .line 23841
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 23864
    const/4 v0, 0x0

    .line 23865
    .local v0, "size":I
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 23866
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    .line 23867
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23869
    :cond_0
    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 23870
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    .line 23871
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23873
    :cond_1
    iput v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->cachedSize:I

    .line 23874
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 23845
    const/16 v0, 0x11

    .line 23846
    .local v0, "result":I
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 23847
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    add-int v0, v1, v2

    .line 23848
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 23849
    return v0

    .line 23848
    :cond_0
    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23882
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 23883
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 23887
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 23888
    :sswitch_0
    return-object p0

    .line 23893
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 23894
    .local v1, "temp":I
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 23896
    :cond_1
    iput v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    .line 23897
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    goto :goto_0

    .line 23899
    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    goto :goto_0

    .line 23904
    .end local v1    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    .line 23905
    iget v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    goto :goto_0

    .line 23883
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23762
    invoke-virtual {p0, p1}, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23854
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 23855
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->device_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 23857
    :cond_0
    iget v0, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 23858
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/apps/dots/proto/client/DotsShared$WebPageSummary$Screenshot;->url_:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 23860
    :cond_1
    return-void
.end method
