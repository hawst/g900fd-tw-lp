.class public interface abstract Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache$AvailabilityListener;
.super Ljava/lang/Object;
.source "DiskCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/dots/android/newsstand/diskcache/DiskCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AvailabilityListener"
.end annotation


# virtual methods
.method public abstract onAvailable()V
.end method

.method public abstract onClosed()V
.end method

.method public abstract onUnmounted()V
.end method
