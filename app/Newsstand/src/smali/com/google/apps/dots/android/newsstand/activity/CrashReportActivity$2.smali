.class Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;
.super Ljava/lang/Object;
.source "CrashReportActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 54
    new-instance v0, Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    invoke-direct {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->setRestorableState(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    # getter for: Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->exitOnDismiss:Z
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->access$000(Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity$2;->this$0:Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;

    invoke-virtual {v0}, Lcom/google/apps/dots/android/newsstand/activity/CrashReportActivity;->finish()V

    .line 58
    :cond_0
    return-void
.end method
