.class public Lcom/google/apps/dots/android/newsstand/store/CachePolicy;
.super Ljava/lang/Object;
.source "CachePolicy.java"


# static fields
.field private static final LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;


# instance fields
.field private final prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;

    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/apps/dots/android/newsstand/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    return-void
.end method

.method public constructor <init>(Lcom/google/apps/dots/android/newsstand/preference/Preferences;)V
    .locals 0
    .param p1, "prefs"    # Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    .line 25
    return-void
.end method

.method private isCachedVersionStale(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Z
    .locals 12
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;
    .param p2, "existingMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->isImmutableType(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    const/4 v4, 0x0

    .line 59
    :goto_0
    return v4

    .line 50
    :cond_0
    iget-object v4, p0, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->prefs:Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    invoke-virtual {v4}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getDesignerMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 51
    const/4 v4, 0x1

    goto :goto_0

    .line 53
    :cond_1
    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->expiration:Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 54
    iget-object v4, p2, Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;->expiration:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x7d0

    add-long v0, v4, v6

    .line 55
    .local v0, "expiration":J
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/util/ClientTimeUtil;->serverNow()J

    move-result-wide v2

    .line 56
    .local v2, "now":J
    sget-object v4, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->LOGD:Lcom/google/apps/dots/android/newsstand/logging/Logd;

    const-string v5, "%s: expires %.2f mins in future"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    sub-long v8, v0, v2

    long-to-double v8, v8

    const-wide v10, 0x40ed4c0000000000L    # 60000.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/apps/dots/android/newsstand/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    cmp-long v4, v0, v2

    if-gez v4, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 59
    .end local v0    # "expiration":J
    .end local v2    # "now":J
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isImmutableType(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)Z
    .locals 2
    .param p1, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    .line 63
    iget-object v0, p1, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->type:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    sget-object v1, Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;->ATTACHMENT:Lcom/google/apps/dots/android/newsstand/util/ProtoEnum$LinkType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public mayUseCachedVersion(Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;Lcom/google/apps/dots/android/newsstand/store/StoreRequest;)I
    .locals 3
    .param p1, "cachedVersionMetadata"    # Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;
    .param p2, "request"    # Lcom/google/apps/dots/android/newsstand/store/StoreRequest;

    .prologue
    const/4 v0, 0x1

    .line 29
    if-eqz p1, :cond_0

    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->postData:[B

    if-eqz v1, :cond_2

    .line 30
    :cond_0
    const/4 v0, 0x2

    .line 40
    :cond_1
    :goto_0
    return v0

    .line 33
    :cond_2
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->versionConstraint:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->REALLY_FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    if-eq v1, v2, :cond_1

    .line 36
    iget-object v1, p2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest;->versionConstraint:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    sget-object v2, Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;->FRESH:Lcom/google/apps/dots/android/newsstand/store/StoreRequest$VersionConstraint;

    if-ne v1, v2, :cond_3

    .line 37
    invoke-direct {p0, p2, p1}, Lcom/google/apps/dots/android/newsstand/store/CachePolicy;->isCachedVersionStale(Lcom/google/apps/dots/android/newsstand/store/StoreRequest;Lcom/google/apps/dots/android/newsstand/diskcache/BlobMetadata;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 40
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
