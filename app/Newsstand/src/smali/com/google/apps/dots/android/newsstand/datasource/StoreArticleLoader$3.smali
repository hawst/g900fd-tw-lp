.class Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;
.super Ljava/lang/Object;
.source "StoreArticleLoader.java"

# interfaces
.implements Lcom/google/common/util/concurrent/AsyncFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->getArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/AsyncFunction",
        "<",
        "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
        "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

.field final synthetic val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iput-object p2, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1, "section"    # Lcom/google/apps/dots/proto/client/DotsShared$Section;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/proto/client/DotsShared$Section;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/google/apps/dots/proto/client/DotsShared$DisplayTemplate$Template;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->useLegacyLayout(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z
    invoke-static {v0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$200(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/proto/client/DotsShared$Section;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFutureLegacy(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$300(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->this$0:Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->val$asyncToken:Lcom/google/apps/dots/android/newsstand/async/AsyncToken;

    # invokes: Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->createArticleTemplateFuture(Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;
    invoke-static {v0, v1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;->access$400(Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader;Lcom/google/apps/dots/android/newsstand/async/AsyncToken;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 159
    check-cast p1, Lcom/google/apps/dots/proto/client/DotsShared$Section;

    invoke-virtual {p0, p1}, Lcom/google/apps/dots/android/newsstand/datasource/StoreArticleLoader$3;->apply(Lcom/google/apps/dots/proto/client/DotsShared$Section;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
