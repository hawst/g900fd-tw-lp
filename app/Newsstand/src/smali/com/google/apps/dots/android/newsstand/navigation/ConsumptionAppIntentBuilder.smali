.class public abstract Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
.super Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;
.source "ConsumptionAppIntentBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
        "<*>;>",
        "Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;"
    }
.end annotation


# instance fields
.field protected backendDocId:Ljava/lang/String;

.field protected docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

.field protected isPreview:Z

.field protected parentBackendDocId:Ljava/lang/String;

.field protected webUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/NavigationIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 28
    return-void
.end method

.method protected static final addAccountExtra(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->prefs()Lcom/google/apps/dots/android/newsstand/preference/Preferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/preference/Preferences;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 143
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 144
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    :cond_0
    return-void
.end method

.method public static forDocType(Landroid/app/Activity;Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "docType"    # Lcom/google/apps/dots/android/newsstand/util/DocType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/apps/dots/android/newsstand/util/DocType;",
            ")",
            "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v1, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder$1;->$SwitchMap$com$google$apps$dots$android$newsstand$util$DocType:[I

    invoke-virtual {p1}, Lcom/google/apps/dots/android/newsstand/util/DocType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 133
    const/4 v1, 0x0

    .line 135
    :goto_0
    return-object v1

    .line 108
    :pswitch_0
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/MusicAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 135
    .local v0, "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    :goto_1
    invoke-virtual {v0, p1}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;

    move-result-object v1

    goto :goto_0

    .line 115
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    :pswitch_1
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/VideosAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 116
    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    goto :goto_1

    .line 119
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    :pswitch_2
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/BooksAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 120
    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    goto :goto_1

    .line 125
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    :pswitch_3
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/NewsstandAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 126
    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    goto :goto_1

    .line 129
    .end local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    :pswitch_4
    new-instance v0, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;

    invoke-direct {v0, p0}, Lcom/google/apps/dots/android/newsstand/navigation/LaunchAppIntentBuilder;-><init>(Landroid/app/Activity;)V

    .line 130
    .restart local v0    # "builder":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<*>;"
    goto :goto_1

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 68
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->webUri:Landroid/net/Uri;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->backendDocId:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->buildViewCollectionIntent()Landroid/content/Intent;

    move-result-object v0

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->setFlags(Landroid/content/Intent;)V

    .line 75
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->webUri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 78
    new-instance v1, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->activity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;-><init>(Landroid/app/Activity;)V

    iget-object v2, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->webUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->setUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/apps/dots/android/newsstand/navigation/ViewActionIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 79
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string v1, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    move-object v1, v0

    .line 89
    :goto_1
    return-object v1

    .line 71
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->buildViewItemIntent()Landroid/content/Intent;

    move-result-object v0

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 83
    :cond_2
    invoke-static {}, Lcom/google/apps/dots/android/newsstand/NSDepend;->appContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/newsstanddev/R$string;->iap_failure_to_open:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 85
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected abstract buildViewCollectionIntent()Landroid/content/Intent;
.end method

.method protected abstract buildViewItemIntent()Landroid/content/Intent;
.end method

.method public setBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 0
    .param p1, "backendDocId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->backendDocId:Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public setDocType(Lcom/google/apps/dots/android/newsstand/util/DocType;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 0
    .param p1, "docType"    # Lcom/google/apps/dots/android/newsstand/util/DocType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/apps/dots/android/newsstand/util/DocType;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->docType:Lcom/google/apps/dots/android/newsstand/util/DocType;

    .line 44
    return-object p0
.end method

.method protected setFlags(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 93
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->isPreview:Z

    if-eqz v0, :cond_0

    .line 94
    const/high16 v0, 0x80000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setIsPreview(Z)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 0
    .param p1, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iput-boolean p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->isPreview:Z

    .line 62
    return-object p0
.end method

.method public setParentBackendDocId(Ljava/lang/String;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 0
    .param p1, "parentBackendDocId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->parentBackendDocId:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public setWebUri(Landroid/net/Uri;)Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;
    .locals 0
    .param p1, "webUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;, "Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder<TT;>;"
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/navigation/ConsumptionAppIntentBuilder;->webUri:Landroid/net/Uri;

    .line 38
    return-object p0
.end method
