.class Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;
.super Lcom/google/apps/dots/android/newsstand/animation/BaseAnimatorListener;
.source "MediaDrawerPagerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandCaption(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

.field final synthetic val$expand:Z


# direct methods
.method constructor <init>(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    iput-boolean p2, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->val$expand:Z

    invoke-direct {p0}, Lcom/google/apps/dots/android/newsstand/animation/BaseAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v3, 0x0

    .line 346
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$800(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 347
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$800(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 348
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x2

    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->val$expand:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->expanded_caption:I

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 349
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->progressInfo:Landroid/view/View;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$800(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 351
    iget-boolean v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->val$expand:Z

    if-eqz v1, :cond_1

    .line 352
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$700(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    :goto_1
    return-void

    .line 348
    :cond_0
    sget v1, Lcom/google/android/apps/newsstanddev/R$id;->caption:I

    goto :goto_0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$600(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x4

    .line 337
    iget-boolean v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->val$expand:Z

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->caption:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$600(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 342
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment$6;->this$0:Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;

    # getter for: Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->expandedCaption:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;->access$700(Lcom/google/apps/dots/android/newsstand/media/MediaDrawerPagerFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
