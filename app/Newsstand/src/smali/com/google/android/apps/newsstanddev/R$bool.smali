.class public final Lcom/google/android/apps/newsstanddev/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final beep_on_gcm:I = 0x7f0c000a

.field public static final dump_hprof_on_uncaught_exception:I = 0x7f0c0009

.field public static final enable_custom_crash_report:I = 0x7f0c001a

.field public static final enable_debug_analytics:I = 0x7f0c0007

.field public static final enable_debugger_attach_pause:I = 0x7f0c000f

.field public static final enable_demo_menu_item:I = 0x7f0c0014

.field public static final enable_developer_options:I = 0x7f0c000c

.field public static final enable_dogfood_warnings:I = 0x7f0c0015

.field public static final enable_internal_analytics_proto_reporting:I = 0x7f0c0012

.field public static final enable_internal_analytics_reporting:I = 0x7f0c0011

.field public static final enable_internal_logging:I = 0x7f0c001b

.field public static final enable_large_heap:I = 0x7f0c0018

.field public static final enable_magazines_market_override:I = 0x7f0c0016

.field public static final enable_publisher_analytics_reporting:I = 0x7f0c0010

.field public static final enable_strict_mode_and_checker:I = 0x7f0c000e

.field public static final enable_systrace:I = 0x7f0c001c

.field public static final enable_webview_debugging:I = 0x7f0c001d

.field public static final fail_on_analytics_errors:I = 0x7f0c000d

.field public static final force_google_accounts_to_dogfood:I = 0x7f0c0008

.field public static final magazines_market_override_magazines_available:I = 0x7f0c0017

.field public static final submit_local_logs:I = 0x7f0c0019
