.class public final Lcom/google/android/apps/newsstanddev/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final action_bar_hint_text:I = 0x7f09009f

.field public static final action_message_text:I = 0x7f090112

.field public static final actionbar_grey:I = 0x7f09009b

.field public static final app_color_material:I = 0x7f0900f4

.field public static final app_color_material_dark:I = 0x7f0900f6

.field public static final card_background:I = 0x7f0900a8

.field public static final card_background_aqua:I = 0x7f0900e0

.field public static final card_background_aqua_dark:I = 0x7f0900e1

.field public static final card_background_blue1:I = 0x7f0900e2

.field public static final card_background_blue1_dark:I = 0x7f0900e3

.field public static final card_background_blue2:I = 0x7f0900e4

.field public static final card_background_blue2_dark:I = 0x7f0900e5

.field public static final card_background_brown:I = 0x7f0900dc

.field public static final card_background_brown_dark:I = 0x7f0900dd

.field public static final card_background_dark:I = 0x7f0900ac

.field public static final card_background_green:I = 0x7f0900de

.field public static final card_background_green_dark:I = 0x7f0900df

.field public static final card_background_light_green:I = 0x7f0900f0

.field public static final card_background_light_green_dark:I = 0x7f0900f1

.field public static final card_background_maroon:I = 0x7f0900ea

.field public static final card_background_maroon_dark:I = 0x7f0900eb

.field public static final card_background_navy:I = 0x7f0900e6

.field public static final card_background_navy_dark:I = 0x7f0900e7

.field public static final card_background_orange:I = 0x7f0900ee

.field public static final card_background_orange_dark:I = 0x7f0900ef

.field public static final card_background_purple:I = 0x7f0900e8

.field public static final card_background_purple_dark:I = 0x7f0900e9

.field public static final card_background_red:I = 0x7f0900ec

.field public static final card_background_red_dark:I = 0x7f0900ed

.field public static final card_background_unsubscribed_text:I = 0x7f0900f3

.field public static final card_international_curation_icon_background:I = 0x7f0900d0

.field public static final card_label_color_default:I = 0x7f0900da

.field public static final card_label_color_default_dark:I = 0x7f0900db

.field public static final card_label_text_white:I = 0x7f0900c2

.field public static final card_list_view_bg_dark:I = 0x7f090125

.field public static final card_list_view_bg_light:I = 0x7f090124

.field public static final card_source_list_item_title_text:I = 0x7f0900cd

.field public static final card_source_text:I = 0x7f090134

.field public static final card_tag_label_color_unsubscribed:I = 0x7f0900d4

.field public static final card_text:I = 0x7f090135

.field public static final card_text_normal:I = 0x7f0900b3

.field public static final card_time_text:I = 0x7f090136

.field public static final download_arc_color:I = 0x7f09010f

.field public static final editable_cardlist_text_highlight:I = 0x7f090108

.field public static final explore_topics_background_featured:I = 0x7f090102

.field public static final explore_topics_background_offers:I = 0x7f090104

.field public static final explore_topics_background_store:I = 0x7f090103

.field public static final home_background:I = 0x7f090099

.field public static final home_background_dark:I = 0x7f09009a

.field public static final image_not_available:I = 0x7f0900cb

.field public static final image_rotator_overlay_color:I = 0x7f090121

.field public static final magazine_offer_color_background:I = 0x7f09011b

.field public static final news_offer_color_background:I = 0x7f09011c

.field public static final play_header_list_banner_text_color:I = 0x7f090051

.field public static final play_primary_text:I = 0x7f09014a

.field public static final play_secondary_text:I = 0x7f09014b

.field public static final progress_dots_active:I = 0x7f0900fd

.field public static final progress_dots_inactive:I = 0x7f0900fc

.field public static final shelf_header_title_color:I = 0x7f0900d8

.field public static final shelf_header_title_color_dark:I = 0x7f0900d9

.field public static final topic_image_rotator_overlay_color:I = 0x7f090122
