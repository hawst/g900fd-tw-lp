.class public final Lcom/google/android/apps/newsstanddev/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final actionable_toast_bar_display_duration_long_ms:I = 0x7f0b000a

.field public static final actionable_toast_bar_display_duration_short_ms:I = 0x7f0b0009

.field public static final actionable_toast_bar_display_iap_long_ms:I = 0x7f0b000c

.field public static final actionable_toast_bar_display_iap_short_ms:I = 0x7f0b000b

.field public static final actionable_toast_bar_fade_duration_ms:I = 0x7f0b000d

.field public static final header_overlay_alpha_value:I = 0x7f0b0011

.field public static final magazine_toc_pair_of_images_num_cols:I = 0x7f0b0016

.field public static final magazine_toc_single_image_num_cols:I = 0x7f0b0017

.field public static final new_feature_card_fequency_minutes:I = 0x7f0b0008

.field public static final num_magazine_cols:I = 0x7f0b0018

.field public static final num_source_shortcut_cols:I = 0x7f0b0019

.field public static final onboard_quiz_curation_overlay_opacity_percent:I = 0x7f0b000f

.field public static final onboard_quiz_magazine_overlay_opacity_percent:I = 0x7f0b0010

.field public static final subscribed_topic_black_percentage:I = 0x7f0b0013

.field public static final unsubscribed_topic_black_percentage:I = 0x7f0b0012
