.class public final Lcom/google/android/apps/newsstanddev/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final about_preference_key:I = 0x7f0a0086

.field public static final add_account:I = 0x7f0a00d6

.field public static final add_subscription_failed:I = 0x7f0a018e

.field public static final add_to_archive_failed:I = 0x7f0a01c2

.field public static final add_to_library:I = 0x7f0a0202

.field public static final added_to_bookmarks:I = 0x7f0a0192

.field public static final almost_developer_mode_toast:I = 0x7f0a0181

.field public static final always_show_google_sold_ads_key:I = 0x7f0a0097

.field public static final analytics_application_id:I = 0x7f0a0041

.field public static final app_name:I = 0x7f0a00ce

.field public static final archive_hide_button_description:I = 0x7f0a01c8

.field public static final archive_issue:I = 0x7f0a01c1

.field public static final archive_magazine_remove_pin_toast_text:I = 0x7f0a01c6

.field public static final archive_magazine_toast_text:I = 0x7f0a01c4

.field public static final archive_show_button_description:I = 0x7f0a01c7

.field public static final article_tail_header:I = 0x7f0a0219

.field public static final article_text_size_large:I = 0x7f0a0169

.field public static final article_text_size_normal:I = 0x7f0a0168

.field public static final article_text_size_preference_key:I = 0x7f0a007b

.field public static final article_text_size_small:I = 0x7f0a0167

.field public static final articles:I = 0x7f0a011b

.field public static final asset_libs_dir:I = 0x7f0a0076

.field public static final audio_error:I = 0x7f0a0129

.field public static final auth_problem_message:I = 0x7f0a00fc

.field public static final authentication_failure:I = 0x7f0a00df

.field public static final authentication_failure_error:I = 0x7f0a00e0

.field public static final books_app_uri:I = 0x7f0a0061

.field public static final cancel:I = 0x7f0a00c8

.field public static final cancel_metered_subscription_title:I = 0x7f0a015d

.field public static final cancel_metered_subscription_warning:I = 0x7f0a015e

.field public static final client_config_version:I = 0x7f0a0046

.field public static final content_authority:I = 0x7f0a003f

.field public static final content_error_domode_downloaded_not_synced:I = 0x7f0a01a6

.field public static final content_error_generic:I = 0x7f0a01a2

.field public static final content_error_layout_engine:I = 0x7f0a01a4

.field public static final content_error_network:I = 0x7f0a01a3

.field public static final content_error_offline:I = 0x7f0a019b

.field public static final content_error_offline_downloadable_not_downloaded:I = 0x7f0a019c

.field public static final content_error_server:I = 0x7f0a01a1

.field public static final continue_reading:I = 0x7f0a00f2

.field public static final continue_reading_label:I = 0x7f0a01b5

.field public static final country_override_key:I = 0x7f0a0091

.field public static final crash_report_message:I = 0x7f0a00d2

.field public static final custom_base_url_key:I = 0x7f0a008d

.field public static final custom_guc_url_key:I = 0x7f0a008e

.field public static final debug_analytics_id:I = 0x7f0a0042

.field public static final default_environment:I = 0x7f0a0047

.field public static final default_show_onboard_quiz_preference:I = 0x7f0a0079

.field public static final designer_mode_key:I = 0x7f0a0088

.field public static final designer_mode_toast:I = 0x7f0a0184

.field public static final developer_mode_toast:I = 0x7f0a0182

.field public static final developer_preference_key:I = 0x7f0a0087

.field public static final dex_output_prefix:I = 0x7f0a0078

.field public static final dex_storage_dir:I = 0x7f0a0077

.field public static final download_content:I = 0x7f0a021f

.field public static final download_via_wifi_only_preference_key:I = 0x7f0a007e

.field public static final download_when_charging_toast_text:I = 0x7f0a01e7

.field public static final download_when_connected_toast_text:I = 0x7f0a01e6

.field public static final download_when_on_wifi_toast_text:I = 0x7f0a01e5

.field public static final download_while_charging_only_preference_key:I = 0x7f0a007f

.field public static final downloaded_only:I = 0x7f0a021c

.field public static final downloaded_only_upper:I = 0x7f0a021d

.field public static final downloading_category_key:I = 0x7f0a007d

.field public static final draggable:I = 0x7f0a00c0

.field public static final edition_added:I = 0x7f0a018a

.field public static final edition_removed:I = 0x7f0a018b

.field public static final empty_list_caption_bookmarks:I = 0x7f0a01a0

.field public static final empty_list_caption_edition:I = 0x7f0a01ee

.field public static final empty_list_caption_my_magazines:I = 0x7f0a019f

.field public static final empty_list_caption_offers:I = 0x7f0a0205

.field public static final empty_list_caption_offline:I = 0x7f0a0223

.field public static final empty_list_caption_search:I = 0x7f0a019d

.field public static final enable_all_debug_loggers_key:I = 0x7f0a0098

.field public static final enable_sync_dialog_body:I = 0x7f0a0100

.field public static final enable_sync_dialog_title:I = 0x7f0a00ff

.field public static final expando_hero:I = 0x7f0a001d

.field public static final expando_splash:I = 0x7f0a001e

.field public static final explore_title:I = 0x7f0a00e6

.field public static final feature_card_text_mini_cards:I = 0x7f0a01f8

.field public static final feature_card_text_translation:I = 0x7f0a01f6

.field public static final feature_card_text_widget:I = 0x7f0a01ed

.field public static final feature_card_title_translation:I = 0x7f0a01f5

.field public static final feature_card_title_widget:I = 0x7f0a01ec

.field public static final featured:I = 0x7f0a0121

.field public static final feeds:I = 0x7f0a011c

.field public static final finsky_app_store_uri:I = 0x7f0a0063

.field public static final finsky_my_apps_uri:I = 0x7f0a0064

.field public static final gcm_sender_id:I = 0x7f0a0043

.field public static final general_preference_category_key:I = 0x7f0a007a

.field public static final go_to_play_store:I = 0x7f0a015f

.field public static final google_privacy_policy_url:I = 0x7f0a0072

.field public static final help_feedback:I = 0x7f0a00f9

.field public static final help_root_topic_uri:I = 0x7f0a006c

.field public static final highlights_edition_title:I = 0x7f0a00eb

.field public static final iap_failure_to_open:I = 0x7f0a013e

.field public static final iap_open_app:I = 0x7f0a014f

.field public static final iap_open_book:I = 0x7f0a0153

.field public static final iap_open_edition:I = 0x7f0a014e

.field public static final iap_open_magazine:I = 0x7f0a014d

.field public static final iap_open_movie:I = 0x7f0a0151

.field public static final iap_open_music:I = 0x7f0a0150

.field public static final iap_open_thing:I = 0x7f0a014c

.field public static final iap_open_tv_show:I = 0x7f0a0152

.field public static final iap_success:I = 0x7f0a0146

.field public static final iap_success_apps:I = 0x7f0a014a

.field public static final iap_success_books:I = 0x7f0a0147

.field public static final iap_success_movies:I = 0x7f0a0148

.field public static final iap_success_music:I = 0x7f0a0149

.field public static final iap_success_newsstand:I = 0x7f0a014b

.field public static final image_not_available_offline:I = 0x7f0a0195

.field public static final image_sync_type_all_images:I = 0x7f0a00b8

.field public static final image_sync_type_images_in_bg:I = 0x7f0a00b4

.field public static final image_sync_type_key:I = 0x7f0a009b

.field public static final image_sync_type_no_images:I = 0x7f0a00b6

.field public static final image_sync_type_primary_images:I = 0x7f0a00b7

.field public static final internal_labs_preference_key:I = 0x7f0a0099

.field public static final language_chinese_simplified:I = 0x7f0a00cf

.field public static final language_chinese_traditional:I = 0x7f0a00d0

.field public static final language_reset:I = 0x7f0a01f2

.field public static final launcher_app_name:I = 0x7f0a00cd

.field public static final load_extra_js_key:I = 0x7f0a0092

.field public static final loading:I = 0x7f0a00c6

.field public static final magazine_article_accessibility_warning:I = 0x7f0a01fa

.field public static final magazine_issue_name_format:I = 0x7f0a01b4

.field public static final magazine_lite_only_body:I = 0x7f0a01b3

.field public static final magazine_lite_only_title:I = 0x7f0a01b2

.field public static final magazine_reading_activity_hero:I = 0x7f0a00c2

.field public static final magazine_version_download_button:I = 0x7f0a01b1

.field public static final magazine_version_title:I = 0x7f0a01ad

.field public static final magazines:I = 0x7f0a011a

.field public static final magazines_market_category_uri:I = 0x7f0a0069

.field public static final magazines_market_title_uri:I = 0x7f0a0068

.field public static final magazines_market_uri:I = 0x7f0a0067

.field public static final magazines_select_text_article_dialog_title:I = 0x7f0a01ac

.field public static final magazines_title:I = 0x7f0a00e9

.field public static final magazines_toggle_button_hide_archive:I = 0x7f0a01ca

.field public static final magazines_toggle_button_show_archive:I = 0x7f0a01c9

.field public static final magazines_toggle_to_lite_mode:I = 0x7f0a01ab

.field public static final magazines_toggle_to_print_mode:I = 0x7f0a01aa

.field public static final manage_subscriptions_uri:I = 0x7f0a006d

.field public static final media_pause:I = 0x7f0a0130

.field public static final media_play:I = 0x7f0a0131

.field public static final media_progress:I = 0x7f0a0194

.field public static final mini_cards:I = 0x7f0a01f7

.field public static final more_button:I = 0x7f0a011f

.field public static final move_to_top:I = 0x7f0a00f5

.field public static final my_library_title:I = 0x7f0a00e2

.field public static final my_news_get_started_title:I = 0x7f0a010a

.field public static final needed_to_rotate_orientation:I = 0x7f0a01a7

.field public static final new_magazine_downloadable_notification:I = 0x7f0a01cb

.field public static final new_multi_magazine_downloadable_notification_title:I = 0x7f0a01cd

.field public static final new_multi_magazine_notification_content:I = 0x7f0a01cf

.field public static final news:I = 0x7f0a0119

.field public static final news_market_title_uri:I = 0x7f0a006a

.field public static final news_title:I = 0x7f0a00e8

.field public static final no_account_selected:I = 0x7f0a00fd

.field public static final no_google_account_error:I = 0x7f0a00d4

.field public static final no_network_connection:I = 0x7f0a00dc

.field public static final no_thanks:I = 0x7f0a0203

.field public static final nonhttp_url:I = 0x7f0a0126

.field public static final offer_expiration_time:I = 0x7f0a0209

.field public static final offer_widget_reason:I = 0x7f0a0204

.field public static final offers_title:I = 0x7f0a0201

.field public static final ok:I = 0x7f0a00c7

.field public static final onboard_button_done:I = 0x7f0a0214

.field public static final onboard_quiz_curations_title:I = 0x7f0a020f

.field public static final onboard_quiz_interstitial_caption:I = 0x7f0a0211

.field public static final onboard_quiz_magazine_offers_title:I = 0x7f0a0210

.field public static final onboard_quiz_not_selected_description:I = 0x7f0a0216

.field public static final onboard_quiz_selected_description:I = 0x7f0a0215

.field public static final onboard_tutorial_page_0_body:I = 0x7f0a020b

.field public static final onboard_tutorial_page_0_title:I = 0x7f0a020a

.field public static final onboard_tutorial_page_1_body:I = 0x7f0a020d

.field public static final onboard_tutorial_page_1_title:I = 0x7f0a020c

.field public static final open_source_licenses_preference_key:I = 0x7f0a0082

.field public static final open_source_licenses_url:I = 0x7f0a0073

.field public static final pdflibclass:I = 0x7f0a0075

.field public static final pdflibjar:I = 0x7f0a0074

.field public static final play_onboard_button_next:I = 0x7f0a0029

.field public static final premium_content:I = 0x7f0a0154

.field public static final privacy_policy_preference_key:I = 0x7f0a0084

.field public static final read_new_subscription_now_prompt:I = 0x7f0a01fe

.field public static final read_now_title:I = 0x7f0a00e1

.field public static final reading_activity_hero:I = 0x7f0a00c3

.field public static final reading_position_sync_preference_key:I = 0x7f0a0090

.field public static final recommended_magazines:I = 0x7f0a0123

.field public static final related_posts_header:I = 0x7f0a0218

.field public static final related_posts_title:I = 0x7f0a0217

.field public static final release:I = 0x7f0a00d1

.field public static final reloado_actionbar:I = 0x7f0a00c5

.field public static final reloado_title:I = 0x7f0a00c4

.field public static final remove_content:I = 0x7f0a0220

.field public static final remove_download_warning_title:I = 0x7f0a021a

.field public static final remove_from_archive_failed:I = 0x7f0a01c3

.field public static final remove_hotspot:I = 0x7f0a00c1

.field public static final remove_source:I = 0x7f0a01ff

.field public static final remove_subscription_failed:I = 0x7f0a018f

.field public static final remove_topic:I = 0x7f0a0200

.field public static final removed_from_bookmarks:I = 0x7f0a0193

.field public static final report:I = 0x7f0a00d3

.field public static final reset_warm_welcome_cards_key:I = 0x7f0a0095

.field public static final reset_warm_welcome_cards_toast:I = 0x7f0a00ab

.field public static final retry:I = 0x7f0a019a

.field public static final saved_title:I = 0x7f0a00e5

.field public static final sd_card_not_available_dialog_body:I = 0x7f0a01e0

.field public static final sd_card_not_available_dialog_title:I = 0x7f0a01df

.field public static final searchview_fragment_title:I = 0x7f0a01fb

.field public static final sections_only_available_to_paid_subscribers:I = 0x7f0a0161

.field public static final server_type_key:I = 0x7f0a008c

.field public static final settings:I = 0x7f0a0164

.field public static final share:I = 0x7f0a01d0

.field public static final share_article_dialog_title:I = 0x7f0a0136

.field public static final share_article_format_html:I = 0x7f0a01db

.field public static final share_article_format_html_no_edition_name:I = 0x7f0a01d8

.field public static final share_article_long_format_text:I = 0x7f0a01da

.field public static final share_article_long_format_text_no_edition_name:I = 0x7f0a01d7

.field public static final share_article_short_format_text:I = 0x7f0a01d9

.field public static final share_article_short_format_text_no_edition_name:I = 0x7f0a01d6

.field public static final share_article_subject_format:I = 0x7f0a01dc

.field public static final share_edition_dialog_title:I = 0x7f0a0135

.field public static final share_edition_format_html:I = 0x7f0a01d3

.field public static final share_edition_long_format_text:I = 0x7f0a01d2

.field public static final share_edition_not_found:I = 0x7f0a0137

.field public static final share_edition_short_format_text:I = 0x7f0a01d1

.field public static final share_link_title_html:I = 0x7f0a01dd

.field public static final share_link_title_text:I = 0x7f0a01de

.field public static final share_source_html:I = 0x7f0a01d5

.field public static final share_source_text:I = 0x7f0a01d4

.field public static final shop_button:I = 0x7f0a0124

.field public static final show_notifications_preference_key:I = 0x7f0a007c

.field public static final show_onboard_quiz_key:I = 0x7f0a0094

.field public static final show_onboard_tutorial_key:I = 0x7f0a0093

.field public static final simulate_crash_event_key:I = 0x7f0a0096

.field public static final sort_by:I = 0x7f0a01bb

.field public static final star_rating:I = 0x7f0a01b9

.field public static final start_background_sync_key:I = 0x7f0a009e

.field public static final start_offline:I = 0x7f0a00dd

.field public static final start_offline_updated_version:I = 0x7f0a00de

.field public static final store:I = 0x7f0a0122

.field public static final subscribe:I = 0x7f0a0120

.field public static final subscribe_and_keep_reading:I = 0x7f0a0158

.field public static final subscription_required:I = 0x7f0a0156

.field public static final subscription_required_to_read_offline:I = 0x7f0a0160

.field public static final sync_failed_notification_body:I = 0x7f0a00db

.field public static final sync_failed_notification_title:I = 0x7f0a00da

.field public static final sync_info_key:I = 0x7f0a009f

.field public static final sync_minimum_storage_mags_key:I = 0x7f0a009c

.field public static final sync_minimum_storage_news_key:I = 0x7f0a009d

.field public static final terms_of_service_preference_key:I = 0x7f0a0083

.field public static final terms_of_service_url:I = 0x7f0a006f

.field public static final topics:I = 0x7f0a011d

.field public static final topics_title:I = 0x7f0a00e7

.field public static final translate:I = 0x7f0a01ef

.field public static final translate_to:I = 0x7f0a01f4

.field public static final translating:I = 0x7f0a01f1

.field public static final trending_time:I = 0x7f0a00ef

.field public static final try_again:I = 0x7f0a0199

.field public static final turning_off_downloaded_only_upper:I = 0x7f0a021e

.field public static final unarchive_issue:I = 0x7f0a01c0

.field public static final unarchive_magazine_toast_text:I = 0x7f0a01c5

.field public static final undo:I = 0x7f0a01fc

.field public static final undo_translation:I = 0x7f0a01f0

.field public static final unsubscribe:I = 0x7f0a018d

.field public static final upgrade_required_short_message:I = 0x7f0a00fb

.field public static final upgrade_status_bar_notification_body:I = 0x7f0a00d8

.field public static final upgrade_status_bar_notification_title:I = 0x7f0a00d7

.field public static final upgrade_version:I = 0x7f0a0045

.field public static final usage_policy_preference_key:I = 0x7f0a0085

.field public static final use_external_storage_preference_key:I = 0x7f0a0080

.field public static final user_agent:I = 0x7f0a0044

.field public static final video_error:I = 0x7f0a0133

.field public static final visit_my_library:I = 0x7f0a0206

.field public static final wait_until_online:I = 0x7f0a00fa

.field public static final warm_welcome_body_bookmarks:I = 0x7f0a0116

.field public static final warm_welcome_body_empty_my_magazines:I = 0x7f0a0106

.field public static final warm_welcome_body_my_magazines:I = 0x7f0a0104

.field public static final warm_welcome_body_my_news:I = 0x7f0a010b

.field public static final warm_welcome_body_my_topics:I = 0x7f0a0112

.field public static final warm_welcome_body_offline_my_magazines_new:I = 0x7f0a0109

.field public static final warm_welcome_body_offline_my_news_new:I = 0x7f0a0110

.field public static final warm_welcome_body_offline_my_topics:I = 0x7f0a0114

.field public static final warm_welcome_button_dismiss:I = 0x7f0a0101

.field public static final warm_welcome_button_empty_my_magazines:I = 0x7f0a0107

.field public static final warm_welcome_title_bookmarks:I = 0x7f0a0115

.field public static final warm_welcome_title_empty_my_magazines:I = 0x7f0a0105

.field public static final warm_welcome_title_my_magazines:I = 0x7f0a0103

.field public static final warm_welcome_title_my_topics:I = 0x7f0a0111

.field public static final warm_welcome_title_offline_my_magazines:I = 0x7f0a0108

.field public static final warm_welcome_title_offline_my_news:I = 0x7f0a010e

.field public static final warm_welcome_title_offline_my_topics:I = 0x7f0a0113

.field public static final web_content:I = 0x7f0a00ee

.field public static final widget_no_posts:I = 0x7f0a01ea

.field public static final widget_offline_not_downloaded:I = 0x7f0a01eb

.field public static final widget_tap_to_sign_in:I = 0x7f0a01e8

.field public static final wifi_only_download_override_dialog_button_download_later:I = 0x7f0a01e4

.field public static final wifi_only_download_override_dialog_button_download_now:I = 0x7f0a01e3

.field public static final wifi_only_download_override_dialog_message:I = 0x7f0a01e2

.field public static final wifi_only_download_override_dialog_title:I = 0x7f0a01e1

.field public static final year_and_month:I = 0x7f0a01be

.field public static final year_and_month_range:I = 0x7f0a01bf

.field public static final zooming_is_unavailable:I = 0x7f0a0128
