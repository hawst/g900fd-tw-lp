.class public final Lcom/google/android/apps/newsstanddev/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_default_elevation:I = 0x7f0d00c2

.field public static final action_bar_default_height:I = 0x7f0d00c1

.field public static final article_image_size_compact:I = 0x7f0d00f3

.field public static final audio_player_favicon_size:I = 0x7f0d0142

.field public static final card_inner_content_padding:I = 0x7f0d0103

.field public static final card_list_view_padding:I = 0x7f0d00d1

.field public static final card_offer_item_news_logo_size:I = 0x7f0d00e6

.field public static final card_offer_item_topic_logo_size:I = 0x7f0d00e5

.field public static final card_source_list_item_topic_icon_inset:I = 0x7f0d00d5

.field public static final default_padding:I = 0x7f0d00bd

.field public static final dialog_source_icon_size:I = 0x7f0d0120

.field public static final edition_header_logo_size:I = 0x7f0d014a

.field public static final explore_topics_title_shadow_radius:I = 0x7f0d0150

.field public static final extra_large_column_width:I = 0x7f0d00ca

.field public static final header_bottom_margin:I = 0x7f0d0149

.field public static final header_spacer_edition:I = 0x7f0d0147

.field public static final header_spacer_none:I = 0x7f0d0148

.field public static final intrinsic_card_padding:I = 0x7f0d00f8

.field public static final magazine_edition_header_cover_height:I = 0x7f0d0153

.field public static final magazine_edition_header_spacer_height:I = 0x7f0d0151

.field public static final magazines_native_body_swipe_distance:I = 0x7f0d0145

.field public static final minimum_fling_velocity:I = 0x7f0d0143

.field public static final my_sources_column_width:I = 0x7f0d00ed

.field public static final pause_queues_scroll_distance:I = 0x7f0d0146

.field public static final play_onboard__onboard_tutorial_page_bg_diameter:I = 0x7f0d0072

.field public static final play_onboard__onboard_tutorial_page_peek_width:I = 0x7f0d0071

.field public static final progress_dots_max_segment_size:I = 0x7f0d00c3

.field public static final simulated_fling_velocity:I = 0x7f0d0144
