.class public final Lcom/google/android/apps/newsstanddev/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final action_message:I = 0x7f04001a

.field public static final action_message_fragment:I = 0x7f04001b

.field public static final actionable_toast_bar:I = 0x7f04001c

.field public static final archive_toggle:I = 0x7f04001d

.field public static final article_tail:I = 0x7f04001e

.field public static final article_tail_entity_item:I = 0x7f04001f

.field public static final audio_button:I = 0x7f040020

.field public static final audio_fragment:I = 0x7f040022

.field public static final audio_notification:I = 0x7f040023

.field public static final audio_notification_expanded:I = 0x7f040024

.field public static final audio_player:I = 0x7f040025

.field public static final bind__card_empty:I = 0x7f040027

.field public static final bind__grid_group_row:I = 0x7f040029

.field public static final card_add_more:I = 0x7f04002c

.field public static final card_continue_reading:I = 0x7f04002d

.field public static final card_header_spacer:I = 0x7f04002e

.field public static final card_highlights_item:I = 0x7f04002f

.field public static final card_icon:I = 0x7f040030

.field public static final card_international_curations_list_item:I = 0x7f040031

.field public static final card_magazine_article_item:I = 0x7f040032

.field public static final card_magazine_item:I = 0x7f040033

.field public static final card_magazine_page_item:I = 0x7f040034

.field public static final card_magazine_pages_item:I = 0x7f040035

.field public static final card_magazine_purchaseable_item:I = 0x7f040036

.field public static final card_magazine_spread_item:I = 0x7f040037

.field public static final card_news_item:I = 0x7f040038

.field public static final card_news_item_compact:I = 0x7f040039

.field public static final card_news_item_full_image:I = 0x7f04003a

.field public static final card_news_item_full_image_compact:I = 0x7f04003b

.field public static final card_news_item_no_image:I = 0x7f04003c

.field public static final card_news_item_no_image_compact:I = 0x7f04003d

.field public static final card_offer_item:I = 0x7f04003e

.field public static final card_offer_item_compact:I = 0x7f040040

.field public static final card_offer_item_horizontal:I = 0x7f040041

.field public static final card_offer_item_magazine:I = 0x7f040042

.field public static final card_offer_item_magazine_compact:I = 0x7f040043

.field public static final card_offer_item_magazine_horizontal:I = 0x7f040044

.field public static final card_refresh_button:I = 0x7f040046

.field public static final card_source_item:I = 0x7f040049

.field public static final card_source_item_solid_color:I = 0x7f04004a

.field public static final card_source_list_item:I = 0x7f04004b

.field public static final card_splash_item_magazine:I = 0x7f04004c

.field public static final card_splash_item_magazine_compact:I = 0x7f04004d

.field public static final card_topic_item:I = 0x7f04004e

.field public static final card_topic_item_compact:I = 0x7f040050

.field public static final card_topic_item_horizontal:I = 0x7f040051

.field public static final card_topic_list_item:I = 0x7f040052

.field public static final card_topic_list_item_compact:I = 0x7f040053

.field public static final card_warm_welcome_one_column:I = 0x7f040055

.field public static final card_warm_welcome_two_column:I = 0x7f040056

.field public static final confidentiality_notice_activity:I = 0x7f040057

.field public static final editable_cardlist_remove:I = 0x7f040058

.field public static final editable_library_fragment:I = 0x7f040059

.field public static final edition_pager_background:I = 0x7f04005a

.field public static final edition_pager_content:I = 0x7f04005b

.field public static final edition_pager_fragment:I = 0x7f04005c

.field public static final edition_pager_hero:I = 0x7f04005d

.field public static final empty_bg_and_card_warm_welcome_one_col:I = 0x7f04005e

.field public static final empty_bg_and_card_warm_welcome_two_col:I = 0x7f04005f

.field public static final explore_content:I = 0x7f040060

.field public static final explore_fragment:I = 0x7f040061

.field public static final explore_single_topic_activity:I = 0x7f040062

.field public static final explore_single_topic_background:I = 0x7f040063

.field public static final explore_single_topic_content:I = 0x7f040064

.field public static final explore_single_topic_fragment:I = 0x7f040065

.field public static final explore_single_topic_fragment_content:I = 0x7f040066

.field public static final explore_single_topic_header_activity:I = 0x7f040067

.field public static final explore_topics_list_item:I = 0x7f040068

.field public static final explore_topics_special_item:I = 0x7f040069

.field public static final group_row_big_card_layout:I = 0x7f04006a

.field public static final group_row_moneyshot_one_two_layout:I = 0x7f04006b

.field public static final group_row_moneyshot_two_one_layout:I = 0x7f04006c

.field public static final group_row_normal_wide_layout:I = 0x7f04006d

.field public static final group_row_single_card_row_layout:I = 0x7f04006e

.field public static final group_row_three_cards_row_layout:I = 0x7f04006f

.field public static final group_row_two_cards_row_layout:I = 0x7f040070

.field public static final group_row_two_column_single_card_layout:I = 0x7f040071

.field public static final group_row_wide_normal_layout:I = 0x7f040072

.field public static final header_edition_activity:I = 0x7f040073

.field public static final header_plain_edition_activity:I = 0x7f040074

.field public static final header_plain_edition_content:I = 0x7f040075

.field public static final home_activity:I = 0x7f040076

.field public static final home_fragment:I = 0x7f040077

.field public static final image_rotator_background:I = 0x7f04007a

.field public static final loading_article_widget:I = 0x7f04007c

.field public static final magazine_edition_activity:I = 0x7f04007d

.field public static final magazine_edition_background:I = 0x7f04007e

.field public static final magazine_edition_content:I = 0x7f04007f

.field public static final magazine_edition_fragment:I = 0x7f040080

.field public static final magazine_edition_hero:I = 0x7f040081

.field public static final magazine_reading_activity:I = 0x7f040082

.field public static final magazine_reading_fragment:I = 0x7f040083

.field public static final magazine_version_download_body:I = 0x7f040084

.field public static final media_drawer_activity:I = 0x7f040087

.field public static final media_drawer_pager_fragment:I = 0x7f040088

.field public static final media_item_fragment:I = 0x7f040089

.field public static final media_item_image:I = 0x7f04008a

.field public static final meter_dialog_body:I = 0x7f04008b

.field public static final meter_dialog_header:I = 0x7f04008c

.field public static final meter_dialog_header_inaccessible:I = 0x7f04008d

.field public static final my_library_content:I = 0x7f04008e

.field public static final my_library_fragment:I = 0x7f04008f

.field public static final my_magazines_fragment:I = 0x7f040090

.field public static final news_article_fragment:I = 0x7f040091

.field public static final news_article_pager_fragment:I = 0x7f040092

.field public static final news_reading_activity:I = 0x7f040093

.field public static final news_widget:I = 0x7f040094

.field public static final news_widget_compact:I = 0x7f040097

.field public static final offer_line:I = 0x7f04009e

.field public static final offers_activity:I = 0x7f04009f

.field public static final offers_fragment:I = 0x7f0400a0

.field public static final offers_fragment_content:I = 0x7f0400a1

.field public static final onboard_auth_page:I = 0x7f0400a2

.field public static final onboard_quiz_curation_item:I = 0x7f0400a4

.field public static final onboard_quiz_magazine_item:I = 0x7f0400a5

.field public static final onboard_tutorial_background:I = 0x7f0400a6

.field public static final onboard_tutorial_page:I = 0x7f0400a7

.field public static final plain_edition_activity:I = 0x7f0400a9

.field public static final plain_edition_fragment:I = 0x7f0400aa

.field public static final pull_dialog:I = 0x7f0400d2

.field public static final remove_download_warning_body:I = 0x7f0400d5

.field public static final round_topic_logo:I = 0x7f0400d6

.field public static final saved_fragment:I = 0x7f0400d7

.field public static final search_activity:I = 0x7f0400d8

.field public static final search_fragment:I = 0x7f0400d9

.field public static final settings_activity:I = 0x7f0400da

.field public static final shelf_detailed_header:I = 0x7f0400db

.field public static final shelf_detailed_header_first:I = 0x7f0400dc

.field public static final shelf_header:I = 0x7f0400dd

.field public static final shelf_header_first:I = 0x7f0400de

.field public static final start_activity:I = 0x7f0400e0

.field public static final subscription_button:I = 0x7f0400e1

.field public static final title_issues_activity:I = 0x7f0400e3

.field public static final title_issues_fragment:I = 0x7f0400e4

.field public static final title_issues_fragment_content:I = 0x7f0400e5

.field public static final topic_logo:I = 0x7f0400e6

.field public static final translate_dialog:I = 0x7f0400e8

.field public static final web_article_fragment:I = 0x7f0400ea

.field public static final web_part_activity:I = 0x7f0400eb

.field public static final web_part_fragment:I = 0x7f0400ec

.field public static final year_month_issues_activity:I = 0x7f0400ed

.field public static final year_month_issues_fragment:I = 0x7f0400ee

.field public static final year_month_issues_fragment_content:I = 0x7f0400ef
