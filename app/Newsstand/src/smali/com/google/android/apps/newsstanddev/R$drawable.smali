.class public final Lcom/google/android/apps/newsstanddev/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_white_background:I = 0x7f020034

.field public static final audio_meter_animation_1:I = 0x7f020036

.field public static final audio_meter_animation_2:I = 0x7f020037

.field public static final audio_meter_animation_3:I = 0x7f020038

.field public static final default_gray_with_gray_highlight:I = 0x7f02005f

.field public static final drag_shadow:I = 0x7f020062

.field public static final drop_shadow:I = 0x7f020064

.field public static final ic_add_circle_card_24dp:I = 0x7f020071

.field public static final ic_add_circle_outline_wht_24dp:I = 0x7f020072

.field public static final ic_archive_hide:I = 0x7f020074

.field public static final ic_archive_show:I = 0x7f020075

.field public static final ic_categories_highlights:I = 0x7f020078

.field public static final ic_check_circle_card_24dp:I = 0x7f020079

.field public static final ic_check_circle_wht_24dp:I = 0x7f02007a

.field public static final ic_chevron_end_wht_24dp:I = 0x7f02007e

.field public static final ic_circular_overflow_dark:I = 0x7f020081

.field public static final ic_circular_overflow_gray:I = 0x7f020082

.field public static final ic_circular_overflow_white_100:I = 0x7f020083

.field public static final ic_circular_overflow_white_50:I = 0x7f020084

.field public static final ic_drawer_bookmarks:I = 0x7f02008a

.field public static final ic_drawer_bookmarks_selected:I = 0x7f02008b

.field public static final ic_drawer_explore:I = 0x7f02008c

.field public static final ic_drawer_explore_selected:I = 0x7f02008d

.field public static final ic_drawer_mylibrary:I = 0x7f02008e

.field public static final ic_drawer_mylibrary_selected:I = 0x7f02008f

.field public static final ic_drawer_readnow:I = 0x7f020090

.field public static final ic_drawer_readnow_selected:I = 0x7f020091

.field public static final ic_empty_bookmark:I = 0x7f020093

.field public static final ic_empty_download:I = 0x7f020094

.field public static final ic_empty_error:I = 0x7f020095

.field public static final ic_empty_news:I = 0x7f020096

.field public static final ic_empty_search:I = 0x7f020097

.field public static final ic_feature:I = 0x7f02009d

.field public static final ic_language_32dp:I = 0x7f0200a1

.field public static final ic_launcher_translate:I = 0x7f0200a2

.field public static final ic_notification:I = 0x7f0200ab

.field public static final ic_offers:I = 0x7f0200ac

.field public static final ic_pause_32dp:I = 0x7f0200ad

.field public static final ic_pause_full_32dp:I = 0x7f0200ae

.field public static final ic_play_arrow_32dp:I = 0x7f0200b0

.field public static final ic_play_arrow_full_32dp:I = 0x7f0200b2

.field public static final ic_rss_blue:I = 0x7f0200be

.field public static final ic_rss_default:I = 0x7f0200bf

.field public static final ic_rss_green:I = 0x7f0200c0

.field public static final ic_rss_orange:I = 0x7f0200c1

.field public static final ic_rss_purple:I = 0x7f0200c2

.field public static final ic_rss_red:I = 0x7f0200c3

.field public static final ic_search_wht_24dp:I = 0x7f0200c8

.field public static final ic_store:I = 0x7f0200cb

.field public static final ic_toast_undo:I = 0x7f0200cc

.field public static final ic_wwc_shop:I = 0x7f0200d4

.field public static final illo_on_boarding_1_scaleable:I = 0x7f0200da

.field public static final illo_on_boarding_2_scaleable:I = 0x7f0200db

.field public static final illo_wwc_bookmarks_scaleable:I = 0x7f0200de

.field public static final illo_wwc_minicards_scaleable:I = 0x7f0200e1

.field public static final illo_wwc_mymagazines_scaleable:I = 0x7f0200e4

.field public static final illo_wwc_mynews_scaleable:I = 0x7f0200e7

.field public static final illo_wwc_mytopics_scaleable:I = 0x7f0200ea

.field public static final illo_wwc_offline_scaleable:I = 0x7f0200ed

.field public static final illo_wwc_translate_scaleable:I = 0x7f0200f0

.field public static final illo_wwc_widget_scaleable:I = 0x7f0200f3

.field public static final image_loading:I = 0x7f0200f5

.field public static final image_not_found:I = 0x7f0200f7

.field public static final indicator_playing_peak_meter_1:I = 0x7f0200f8

.field public static final magazine_mode_toggle_image_lite:I = 0x7f020108

.field public static final magazine_mode_toggle_image_print:I = 0x7f020109

.field public static final magazine_mode_toggle_lite:I = 0x7f02010a

.field public static final magazine_mode_toggle_print:I = 0x7f02010b

.field public static final search_plate_background:I = 0x7f020165

.field public static final solid_color_circle_bg:I = 0x7f020168

.field public static final stat_notify_manage:I = 0x7f020169

.field public static final switch_bg:I = 0x7f02016a

.field public static final switch_thumb:I = 0x7f02016f

.field public static final white_outline_circle_bg:I = 0x7f02017e
