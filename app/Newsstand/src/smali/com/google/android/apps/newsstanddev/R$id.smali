.class public final Lcom/google/android/apps/newsstanddev/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/newsstanddev/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ActionMessage_actionOnClickListener:I = 0x7f0e015c

.field public static final ActionMessage_actionText:I = 0x7f0e015b

.field public static final ActionMessage_iconDrawable:I = 0x7f0e0159

.field public static final ActionMessage_messageText:I = 0x7f0e015a

.field public static final ActionMessage_onClickListener:I = 0x7f0e0158

.field public static final ActionMessage_topPadding:I = 0x7f0e015d

.field public static final AppFamilyList_appFamilyId:I = 0x7f0e0028

.field public static final AppFamilyList_appFamilySummary:I = 0x7f0e0029

.field public static final AppFamilyList_description:I = 0x7f0e002c

.field public static final AppFamilyList_iconId:I = 0x7f0e002b

.field public static final AppFamilyList_magazineIssues:I = 0x7f0e002f

.field public static final AppFamilyList_name:I = 0x7f0e002a

.field public static final AppFamilyList_numIssues:I = 0x7f0e002e

.field public static final AppFamilyList_updated:I = 0x7f0e002d

.field public static final ApplicationList_appFamilyId:I = 0x7f0e0034

.field public static final ApplicationList_appFamilySummary:I = 0x7f0e0032

.field public static final ApplicationList_appId:I = 0x7f0e0033

.field public static final ApplicationList_appSummary:I = 0x7f0e0031

.field public static final ApplicationList_category:I = 0x7f0e003d

.field public static final ApplicationList_contentDescription:I = 0x7f0e003c

.field public static final ApplicationList_description:I = 0x7f0e0037

.field public static final ApplicationList_edition:I = 0x7f0e0030

.field public static final ApplicationList_iconId:I = 0x7f0e0036

.field public static final ApplicationList_isArchived:I = 0x7f0e003a

.field public static final ApplicationList_isPurchased:I = 0x7f0e0039

.field public static final ApplicationList_publicationDate:I = 0x7f0e0038

.field public static final ApplicationList_title:I = 0x7f0e0035

.field public static final ApplicationList_updated:I = 0x7f0e003b

.field public static final ArchiveToggle_onClickListener:I = 0x7f0e003e

.field public static final ArchiveToggle_showDefault:I = 0x7f0e003f

.field public static final ArticleTailEntityItem_sourceBackgroundId:I = 0x7f0e011b

.field public static final ArticleTailEntityItem_sourceClickListener:I = 0x7f0e011c

.field public static final ArticleTailEntityItem_sourceInitials:I = 0x7f0e011a

.field public static final ArticleTailEntityItem_sourceName:I = 0x7f0e0119

.field public static final ArticleTailList_isSubscribed:I = 0x7f0e0118

.field public static final ArticleTailList_rowId:I = 0x7f0e0117

.field public static final BaseCardListVisitor_resourceBundle:I = 0x7f0e0043

.field public static final CardAddMore_onClickListener:I = 0x7f0e0044

.field public static final CardBase_analyticsEvent:I = 0x7f0e004f

.field public static final CardContinueReading_labelAndTitle:I = 0x7f0e0050

.field public static final CardContinueReading_onClickListener:I = 0x7f0e0051

.field public static final CardHeaderSpacer_height:I = 0x7f0e0052

.field public static final CardIcon_aspectRatio:I = 0x7f0e004b

.field public static final CardIcon_background:I = 0x7f0e0049

.field public static final CardIcon_contentDescription:I = 0x7f0e004c

.field public static final CardIcon_imageId:I = 0x7f0e0046

.field public static final CardIcon_onClickListener:I = 0x7f0e004a

.field public static final CardIcon_text:I = 0x7f0e0047

.field public static final CardIcon_textColor:I = 0x7f0e0048

.field public static final CardIcon_transitionName:I = 0x7f0e004d

.field public static final CardListVisitor_defaultPrimaryKey:I = 0x7f0e004e

.field public static final CardMagazineArticleItem_author:I = 0x7f0e0056

.field public static final CardMagazineArticleItem_imageId:I = 0x7f0e0057

.field public static final CardMagazineArticleItem_isRead:I = 0x7f0e005a

.field public static final CardMagazineArticleItem_onClickListener:I = 0x7f0e0059

.field public static final CardMagazineArticleItem_subtitle:I = 0x7f0e0055

.field public static final CardMagazineArticleItem_title:I = 0x7f0e0054

.field public static final CardMagazineArticleItem_transform:I = 0x7f0e0058

.field public static final CardMagazineArticleItem_version:I = 0x7f0e0053

.field public static final CardMagazineItem_clickListener:I = 0x7f0e005f

.field public static final CardMagazineItem_contentDescription:I = 0x7f0e0060

.field public static final CardMagazineItem_coverAttachmentId:I = 0x7f0e005c

.field public static final CardMagazineItem_coverHeightToWidthRatio:I = 0x7f0e005e

.field public static final CardMagazineItem_issueName:I = 0x7f0e005b

.field public static final CardMagazineItem_sourceTransitionName:I = 0x7f0e0061

.field public static final CardMagazineItem_transitionName:I = 0x7f0e005d

.field public static final CardMagazinePageItem_imageAspectRatio:I = 0x7f0e0069

.field public static final CardMagazinePageItem_imageId:I = 0x7f0e0063

.field public static final CardMagazinePageItem_isInitialPost:I = 0x7f0e006a

.field public static final CardMagazinePageItem_onClickListener:I = 0x7f0e0065

.field public static final CardMagazinePageItem_transform:I = 0x7f0e0064

.field public static final CardMagazinePageItem_version:I = 0x7f0e0062

.field public static final CardMagazinePagesItem_eitherIsInitialPost:I = 0x7f0e0077

.field public static final CardMagazinePagesItem_imageAspectRatio:I = 0x7f0e0074

.field public static final CardMagazinePagesItem_leftImageId:I = 0x7f0e006c

.field public static final CardMagazinePagesItem_leftIsInitialPost:I = 0x7f0e0075

.field public static final CardMagazinePagesItem_leftOnClickListener:I = 0x7f0e006d

.field public static final CardMagazinePagesItem_rightImageId:I = 0x7f0e0070

.field public static final CardMagazinePagesItem_rightIsInitialPost:I = 0x7f0e0076

.field public static final CardMagazinePagesItem_rightOnClickListener:I = 0x7f0e0071

.field public static final CardMagazinePagesItem_version:I = 0x7f0e006b

.field public static final CardMagazinePurchaseableItem_clickListener:I = 0x7f0e007a

.field public static final CardMagazinePurchaseableItem_coverAttachmentId:I = 0x7f0e0078

.field public static final CardMagazinePurchaseableItem_coverHeightToWidthRatio:I = 0x7f0e0079

.field public static final CardMagazinePurchaseableItem_issueName:I = 0x7f0e007c

.field public static final CardMagazinePurchaseableItem_magazineName:I = 0x7f0e007b

.field public static final CardMagazinePurchaseableItem_price:I = 0x7f0e007e

.field public static final CardMagazinePurchaseableItem_rating:I = 0x7f0e007d

.field public static final CardNewsItem_abstract:I = 0x7f0e0093

.field public static final CardNewsItem_audioOnClickListener:I = 0x7f0e009c

.field public static final CardNewsItem_audioPostId:I = 0x7f0e009b

.field public static final CardNewsItem_cardBackground:I = 0x7f0e00a4

.field public static final CardNewsItem_entityId:I = 0x7f0e0098

.field public static final CardNewsItem_entityIsSubscribed:I = 0x7f0e0097

.field public static final CardNewsItem_entityOnClickListener:I = 0x7f0e0096

.field public static final CardNewsItem_galleryOnClickListener:I = 0x7f0e009f

.field public static final CardNewsItem_genomeEntity:I = 0x7f0e0095

.field public static final CardNewsItem_hasGallery:I = 0x7f0e009e

.field public static final CardNewsItem_headlineTextColor:I = 0x7f0e00a5

.field public static final CardNewsItem_imageAspectRatio:I = 0x7f0e008b

.field public static final CardNewsItem_imageAttribution:I = 0x7f0e008c

.field public static final CardNewsItem_imageEligibleForHeader:I = 0x7f0e008d

.field public static final CardNewsItem_imageHeight:I = 0x7f0e0089

.field public static final CardNewsItem_imageId:I = 0x7f0e0088

.field public static final CardNewsItem_imageWidth:I = 0x7f0e008a

.field public static final CardNewsItem_isFullImage:I = 0x7f0e00a0

.field public static final CardNewsItem_isRead:I = 0x7f0e009d

.field public static final CardNewsItem_isVideo:I = 0x7f0e0099

.field public static final CardNewsItem_numAudio:I = 0x7f0e009a

.field public static final CardNewsItem_showLock:I = 0x7f0e00a1

.field public static final CardNewsItem_sourceIconId:I = 0x7f0e008e

.field public static final CardNewsItem_sourceIsSubscribed:I = 0x7f0e0090

.field public static final CardNewsItem_sourceName:I = 0x7f0e008f

.field public static final CardNewsItem_sourceOnClickListener:I = 0x7f0e0092

.field public static final CardNewsItem_sourceTextColor:I = 0x7f0e00a6

.field public static final CardNewsItem_sourceTransitionName:I = 0x7f0e00a3

.field public static final CardNewsItem_storyOnClickListener:I = 0x7f0e0094

.field public static final CardNewsItem_time:I = 0x7f0e0091

.field public static final CardNewsItem_timeTextColor:I = 0x7f0e00a7

.field public static final CardNewsItem_title:I = 0x7f0e0087

.field public static final CardNewsItem_version:I = 0x7f0e0086

.field public static final CardOfferItem_description:I = 0x7f0e00b8

.field public static final CardOfferItem_expirationTime:I = 0x7f0e00bd

.field public static final CardOfferItem_footerBackgroundColor:I = 0x7f0e00c1

.field public static final CardOfferItem_negativeIcon:I = 0x7f0e00bf

.field public static final CardOfferItem_negativeOnClickListener:I = 0x7f0e00ba

.field public static final CardOfferItem_negativeText:I = 0x7f0e00bc

.field public static final CardOfferItem_positiveIcon:I = 0x7f0e00be

.field public static final CardOfferItem_positiveOnClickListener:I = 0x7f0e00b9

.field public static final CardOfferItem_positiveText:I = 0x7f0e00bb

.field public static final CardOfferItem_sourceAspectRatio:I = 0x7f0e00b6

.field public static final CardOfferItem_sourceIconId:I = 0x7f0e00b4

.field public static final CardOfferItem_sourceIconSize:I = 0x7f0e00b5

.field public static final CardOfferItem_splashAttachmentId:I = 0x7f0e00b3

.field public static final CardOfferItem_splashOnClickListener:I = 0x7f0e00c0

.field public static final CardOfferItem_splashSubtitle:I = 0x7f0e00b2

.field public static final CardOfferItem_splashTitle:I = 0x7f0e00b1

.field public static final CardOfferItem_title:I = 0x7f0e00b7

.field public static final CardRefreshButton_onClickListener:I = 0x7f0e00a8

.field public static final CardSourceItem_backgroundColorResId:I = 0x7f0e00c8

.field public static final CardSourceItem_highlightsAspectRatio:I = 0x7f0e00c9

.field public static final CardSourceItem_sourceClickListener:I = 0x7f0e00c6

.field public static final CardSourceItem_sourceIconBackground:I = 0x7f0e00c5

.field public static final CardSourceItem_sourceIconId:I = 0x7f0e00c3

.field public static final CardSourceItem_sourceIconNoImage:I = 0x7f0e00c4

.field public static final CardSourceItem_sourceName:I = 0x7f0e00c2

.field public static final CardSourceItem_sourceTransitionName:I = 0x7f0e00c7

.field public static final CardSourceListItem_cardBackground:I = 0x7f0e00ca

.field public static final CardSourceListItem_cardTransitionName:I = 0x7f0e00d9

.field public static final CardSourceListItem_sourceBackground:I = 0x7f0e00ce

.field public static final CardSourceListItem_sourceClickListener:I = 0x7f0e00d4

.field public static final CardSourceListItem_sourceDescription:I = 0x7f0e00d2

.field public static final CardSourceListItem_sourceDescriptionTextColor:I = 0x7f0e00d3

.field public static final CardSourceListItem_sourceIconId:I = 0x7f0e00cd

.field public static final CardSourceListItem_sourceIconTransitionName:I = 0x7f0e00d8

.field public static final CardSourceListItem_sourceInitials:I = 0x7f0e00cf

.field public static final CardSourceListItem_sourceInitialsBackground:I = 0x7f0e00d0

.field public static final CardSourceListItem_sourceInitialsTextColor:I = 0x7f0e00d1

.field public static final CardSourceListItem_sourceName:I = 0x7f0e00cb

.field public static final CardSourceListItem_sourceNameTextColor:I = 0x7f0e00cc

.field public static final CardSourceListItem_subscribeClickHandler:I = 0x7f0e00d5

.field public static final CardSourceListItem_subscribeDescription:I = 0x7f0e00d7

.field public static final CardSourceListItem_subscribeIcon:I = 0x7f0e00d6

.field public static final CardSplashItem_hintText:I = 0x7f0e00ad

.field public static final CardSplashItem_sourceAspectRatio:I = 0x7f0e00ae

.field public static final CardSplashItem_sourceIconId:I = 0x7f0e00ab

.field public static final CardSplashItem_sourceName:I = 0x7f0e00ac

.field public static final CardSplashItem_splashAttachmentId:I = 0x7f0e00aa

.field public static final CardSplashItem_splashOnClickListener:I = 0x7f0e00b0

.field public static final CardSplashItem_time:I = 0x7f0e00af

.field public static final CardTopicItem_articleArray:I = 0x7f0e00de

.field public static final CardTopicItem_backgroundColor:I = 0x7f0e00e0

.field public static final CardTopicItem_entityImageId:I = 0x7f0e00dc

.field public static final CardTopicItem_entityOnClickListener:I = 0x7f0e00dd

.field public static final CardTopicItem_initials:I = 0x7f0e00db

.field public static final CardTopicItem_readStateArray:I = 0x7f0e00df

.field public static final CardTopicItem_title:I = 0x7f0e00da

.field public static final CardTopicItem_topicIconId:I = 0x7f0e00e1

.field public static final CardTopicListItem_isRead:I = 0x7f0e00e6

.field public static final CardTopicListItem_primaryImage:I = 0x7f0e00e5

.field public static final CardTopicListItem_sourceName:I = 0x7f0e00e3

.field public static final CardTopicListItem_storyOnClickListener:I = 0x7f0e00e4

.field public static final CardTopicListItem_title:I = 0x7f0e00e2

.field public static final CardWarmWelcome_body:I = 0x7f0e00ef

.field public static final CardWarmWelcome_button1IconDrawableId:I = 0x7f0e00f2

.field public static final CardWarmWelcome_button1OnClickListener:I = 0x7f0e00f3

.field public static final CardWarmWelcome_button1Text:I = 0x7f0e00f1

.field public static final CardWarmWelcome_button2IconDrawableId:I = 0x7f0e00f5

.field public static final CardWarmWelcome_button2OnClickListener:I = 0x7f0e00f6

.field public static final CardWarmWelcome_button2Text:I = 0x7f0e00f4

.field public static final CardWarmWelcome_logoDrawableId:I = 0x7f0e00f0

.field public static final CardWarmWelcome_title:I = 0x7f0e00ee

.field public static final EditionPagerFragment_pagerTitle:I = 0x7f0e00f8

.field public static final ExploreSingleTopicList_shelfType:I = 0x7f0e00f7

.field public static final ExploreTopicsListItem_entityImageId:I = 0x7f0e0101

.field public static final ExploreTopicsListItem_onClickListener:I = 0x7f0e0100

.field public static final ExploreTopicsListItem_topicName:I = 0x7f0e00ff

.field public static final ExploreTopicsSpecialItem_background:I = 0x7f0e0107

.field public static final ExploreTopicsSpecialItem_iconDrawable:I = 0x7f0e0105

.field public static final ExploreTopicsSpecialItem_onClickListener:I = 0x7f0e0104

.field public static final ExploreTopicsSpecialItem_title:I = 0x7f0e0106

.field public static final ImageRotatorView_animateImage:I = 0x7f0e010c

.field public static final ImageRotatorView_imageId:I = 0x7f0e010b

.field public static final KeepEditionMenuHelper_editionPinned:I = 0x7f0e010d

.field public static final LibraryPageList_libraryPage:I = 0x7f0e0109

.field public static final LibraryPageList_libraryTitle:I = 0x7f0e010a

.field public static final MagazineEditionCardList_primaryKey:I = 0x7f0e0163

.field public static final MagazineListUtil_endMonth:I = 0x7f0e0160

.field public static final MagazineListUtil_month:I = 0x7f0e015f

.field public static final MagazineListUtil_numIssues:I = 0x7f0e0162

.field public static final MagazineListUtil_year:I = 0x7f0e015e

.field public static final MagazineListUtil_yearMonthKey:I = 0x7f0e0161

.field public static final MagazineReadingFragment_sectionSummary:I = 0x7f0e0143

.field public static final MediaItemsSource_audioId:I = 0x7f0e0111

.field public static final MediaItemsSource_audioIndex:I = 0x7f0e010f

.field public static final MediaItemsSource_mediaItem:I = 0x7f0e010e

.field public static final MediaItemsSource_postId:I = 0x7f0e0110

.field public static final MyMagazinesFragment_groupArchiveId:I = 0x7f0e0113

.field public static final NewFeatureCard_excludeDevicesList:I = 0x7f0e00fd

.field public static final NewFeatureCard_image:I = 0x7f0e00f9

.field public static final NewFeatureCard_showPrefKey:I = 0x7f0e00fc

.field public static final NewFeatureCard_text:I = 0x7f0e00fb

.field public static final NewFeatureCard_title:I = 0x7f0e00fa

.field public static final NormalArticleWidget_articleLoader:I = 0x7f0e0114

.field public static final NormalArticleWidget_articleVersion:I = 0x7f0e0115

.field public static final NormalArticleWidget_isMetered:I = 0x7f0e0116

.field public static final OnboardQuizItem_clickListener:I = 0x7f0e0127

.field public static final OnboardQuizItem_contentDescription:I = 0x7f0e012a

.field public static final OnboardQuizItem_defaultOn:I = 0x7f0e0129

.field public static final OnboardQuizItem_initials:I = 0x7f0e0125

.field public static final OnboardQuizItem_itemId:I = 0x7f0e0121

.field public static final OnboardQuizItem_selected:I = 0x7f0e0126

.field public static final OnboardQuizItem_selectionColorResId:I = 0x7f0e0128

.field public static final OnboardQuizItem_sourceAspectRatio:I = 0x7f0e0123

.field public static final OnboardQuizItem_sourceIconId:I = 0x7f0e0122

.field public static final OnboardQuizItem_title:I = 0x7f0e0124

.field public static final PinnedList_edition:I = 0x7f0e012f

.field public static final PinnedList_lastSyncStarted:I = 0x7f0e012c

.field public static final PinnedList_lastSynced:I = 0x7f0e012b

.field public static final PinnedList_syncFailed:I = 0x7f0e012e

.field public static final PinnedList_syncProgress:I = 0x7f0e012d

.field public static final RawOffersList_offerId:I = 0x7f0e0130

.field public static final RawOffersList_offerSummary:I = 0x7f0e0131

.field public static final RawOffersList_storeType:I = 0x7f0e0132

.field public static final ReadStateList_isMeteredRead:I = 0x7f0e0145

.field public static final ReadStateList_postId:I = 0x7f0e0146

.field public static final ReadStateList_postReadState:I = 0x7f0e0147

.field public static final ReadingFragment_backgroundColor:I = 0x7f0e013c

.field public static final ReadingFragment_linkViewPostTitle:I = 0x7f0e013d

.field public static final ReadingFragment_linkViewPostUrl:I = 0x7f0e013e

.field public static final ReadingFragment_linkViewPublisher:I = 0x7f0e013f

.field public static final ReadingFragment_postId:I = 0x7f0e0135

.field public static final ReadingFragment_postIndex:I = 0x7f0e0137

.field public static final ReadingFragment_postOriginalEdition:I = 0x7f0e013a

.field public static final ReadingFragment_postSummary:I = 0x7f0e0136

.field public static final ReadingFragment_postUpdated:I = 0x7f0e013b

.field public static final ReadingFragment_shareParams:I = 0x7f0e0139

.field public static final ReadingFragment_viewType:I = 0x7f0e0138

.field public static final RecentlyReadList_edition:I = 0x7f0e0134

.field public static final RecentlyReadList_lastRead:I = 0x7f0e0133

.field public static final RoundedCacheableAttachmentView_roundedSourceIconBackground:I = 0x7f0e0141

.field public static final RoundedCacheableAttachmentView_roundedSourceIconInset:I = 0x7f0e0142

.field public static final RoundedCacheableAttachmentView_useRoundedSourceIcon:I = 0x7f0e0140

.field public static final SectionList_sectionId:I = 0x7f0e0148

.field public static final SectionList_sectionName:I = 0x7f0e0149

.field public static final SectionList_sectionSummary:I = 0x7f0e014a

.field public static final ShelfDetailedHeader_buttonText:I = 0x7f0e0150

.field public static final ShelfDetailedHeader_onClickListener:I = 0x7f0e014b

.field public static final ShelfDetailedHeader_subtitle:I = 0x7f0e014f

.field public static final ShelfDetailedHeader_title:I = 0x7f0e014c

.field public static final ShelfDetailedHeader_titleTextColor:I = 0x7f0e014d

.field public static final ShelfDetailedHeader_titleTransitionName:I = 0x7f0e014e

.field public static final ShelfHeader_buttonText:I = 0x7f0e0155

.field public static final ShelfHeader_onClickListener:I = 0x7f0e0156

.field public static final ShelfHeader_title:I = 0x7f0e0152

.field public static final ShelfHeader_titleTextColor:I = 0x7f0e0153

.field public static final ShelfHeader_titleTransitionName:I = 0x7f0e0154

.field public static final accent_layer:I = 0x7f0e02f7

.field public static final accept_button:I = 0x7f0e024c

.field public static final action_message:I = 0x7f0e0201

.field public static final action_trigger:I = 0x7f0e02dd

.field public static final actionable_toast_bar:I = 0x7f0e017b

.field public static final actionable_toast_bar_action_text:I = 0x7f0e0204

.field public static final actionable_toast_bar_description_text:I = 0x7f0e0202

.field public static final actionable_toast_bar_separator:I = 0x7f0e0203

.field public static final alt_play_background:I = 0x7f0e02bd

.field public static final article_container:I = 0x7f0e0248

.field public static final article_tail:I = 0x7f0e0265

.field public static final article_title:I = 0x7f0e0218

.field public static final attribution:I = 0x7f0e023e

.field public static final audioNotification:I = 0x7f0e0172

.field public static final audioNotificationExpanded:I = 0x7f0e0173

.field public static final audio_active:I = 0x7f0e0209

.field public static final audio_control_bar:I = 0x7f0e0211

.field public static final audio_fragment:I = 0x7f0e0255

.field public static final audio_inactive:I = 0x7f0e0208

.field public static final audio_meter_1:I = 0x7f0e020c

.field public static final audio_meter_2:I = 0x7f0e020a

.field public static final audio_meter_3:I = 0x7f0e020b

.field public static final background:I = 0x7f0e0226

.field public static final background_container:I = 0x7f0e02bc

.field public static final background_image:I = 0x7f0e0258

.field public static final bg_preview_image:I = 0x7f0e026c

.field public static final buttons:I = 0x7f0e020d

.field public static final byline:I = 0x7f0e029c

.field public static final cancel_button:I = 0x7f0e0210

.field public static final caption:I = 0x7f0e0278

.field public static final card:I = 0x7f0e01da

.field public static final card_0:I = 0x7f0e0176

.field public static final card_1:I = 0x7f0e0177

.field public static final card_2:I = 0x7f0e0178

.field public static final card_3:I = 0x7f0e0179

.field public static final card_4:I = 0x7f0e017a

.field public static final card_list:I = 0x7f0e0207

.field public static final checkbox:I = 0x7f0e01ea

.field public static final color_layer:I = 0x7f0e02f8

.field public static final colored_overlay:I = 0x7f0e0262

.field public static final content_textbox:I = 0x7f0e024b

.field public static final default_language:I = 0x7f0e02ed

.field public static final dialog_body:I = 0x7f0e02de

.field public static final dialog_header:I = 0x7f0e02dc

.field public static final disk:I = 0x7f0e02fa

.field public static final down_arrow:I = 0x7f0e0268

.field public static final down_buttons:I = 0x7f0e0288

.field public static final drawer:I = 0x7f0e0253

.field public static final duration_text:I = 0x7f0e0220

.field public static final edition_fragment:I = 0x7f0e025b

.field public static final edition_language:I = 0x7f0e02ee

.field public static final edition_subtitle:I = 0x7f0e02e1

.field public static final edition_title:I = 0x7f0e02e0

.field public static final empty_view_text:I = 0x7f0e0295

.field public static final entityImageView:I = 0x7f0e0259

.field public static final entityImageViewBackground:I = 0x7f0e02e9

.field public static final error:I = 0x7f0e027d

.field public static final expanded_caption:I = 0x7f0e0277

.field public static final favicon:I = 0x7f0e0213

.field public static final genome_text:I = 0x7f0e0237

.field public static final gradient_text_container:I = 0x7f0e023a

.field public static final header_image_rotator:I = 0x7f0e024e

.field public static final header_list_layout:I = 0x7f0e0250

.field public static final home_fragment:I = 0x7f0e025e

.field public static final home_page_container:I = 0x7f0e025f

.field public static final icon:I = 0x7f0e01ae

.field public static final image:I = 0x7f0e01e5

.field public static final image_0:I = 0x7f0e0260

.field public static final image_1:I = 0x7f0e0261

.field public static final image_header_logo:I = 0x7f0e0251

.field public static final image_loading:I = 0x7f0e0291

.field public static final image_loading_text:I = 0x7f0e0293

.field public static final info:I = 0x7f0e0276

.field public static final initialsView:I = 0x7f0e02e2

.field public static final learn_more:I = 0x7f0e0280

.field public static final leftImage:I = 0x7f0e022e

.field public static final link_widget:I = 0x7f0e02f1

.field public static final load_error:I = 0x7f0e0267

.field public static final loading:I = 0x7f0e0263

.field public static final loading_view:I = 0x7f0e02e6

.field public static final magazine_accessibility_warning:I = 0x7f0e0269

.field public static final magazine_expando_hero:I = 0x7f0e017f

.field public static final magazine_reading_fragment:I = 0x7f0e026b

.field public static final main_fragment:I = 0x7f0e0254

.field public static final mediaItemContainer:I = 0x7f0e027c

.field public static final media_drawer_pager_fragment:I = 0x7f0e0274

.field public static final menu_add_edition:I = 0x7f0e0309

.field public static final menu_demo:I = 0x7f0e02fd

.field public static final menu_helpfeedback:I = 0x7f0e0168

.field public static final menu_keep_on_device:I = 0x7f0e030b

.field public static final menu_magazine_alt_formats:I = 0x7f0e02ff

.field public static final menu_manage_subscriptions:I = 0x7f0e0300

.field public static final menu_mini_cards:I = 0x7f0e030c

.field public static final menu_refresh:I = 0x7f0e0308

.field public static final menu_remove_edition:I = 0x7f0e030d

.field public static final menu_save:I = 0x7f0e0303

.field public static final menu_search:I = 0x7f0e0166

.field public static final menu_see_original_article:I = 0x7f0e0305

.field public static final menu_settings:I = 0x7f0e0167

.field public static final menu_share:I = 0x7f0e0302

.field public static final menu_sort:I = 0x7f0e0301

.field public static final menu_switch_mode:I = 0x7f0e02fe

.field public static final menu_sync_on_device:I = 0x7f0e02fc

.field public static final menu_translate:I = 0x7f0e0306

.field public static final menu_undo_translate:I = 0x7f0e0307

.field public static final menu_unsave:I = 0x7f0e0304

.field public static final meter_dialog:I = 0x7f0e0266

.field public static final meter_number:I = 0x7f0e0286

.field public static final meter_number_container:I = 0x7f0e0285

.field public static final meter_number_decremented:I = 0x7f0e0287

.field public static final meter_status_text:I = 0x7f0e0284

.field public static final news_article_pager:I = 0x7f0e028e

.field public static final news_article_pager_fragment:I = 0x7f0e028f

.field public static final next:I = 0x7f0e0296

.field public static final next_button:I = 0x7f0e0216

.field public static final next_button_disabled:I = 0x7f0e021d

.field public static final next_icon:I = 0x7f0e0297

.field public static final next_progress:I = 0x7f0e0298

.field public static final offer_line:I = 0x7f0e0282

.field public static final offer_line_progress:I = 0x7f0e0283

.field public static final offers:I = 0x7f0e027e

.field public static final offers_fragment:I = 0x7f0e029d

.field public static final offers_progress:I = 0x7f0e027f

.field public static final overlay:I = 0x7f0e0264

.field public static final pager:I = 0x7f0e026d

.field public static final pager_drop_shadow:I = 0x7f0e0275

.field public static final plain_edition_fragment:I = 0x7f0e025d

.field public static final play_button:I = 0x7f0e020f

.field public static final play_header_listview:I = 0x7f0e0006

.field public static final play_header_spacer:I = 0x7f0e0008

.field public static final preparing:I = 0x7f0e020e

.field public static final previous:I = 0x7f0e0299

.field public static final previous_button:I = 0x7f0e0215

.field public static final previous_button_disabled:I = 0x7f0e021c

.field public static final previous_icon:I = 0x7f0e029a

.field public static final previous_progress:I = 0x7f0e029b

.field public static final price:I = 0x7f0e0233

.field public static final primary_image:I = 0x7f0e0242

.field public static final progressDotContainer:I = 0x7f0e0175

.field public static final progressDots:I = 0x7f0e027b

.field public static final progressText:I = 0x7f0e027a

.field public static final progress_info:I = 0x7f0e0279

.field public static final progress_text:I = 0x7f0e021f

.field public static final psv:I = 0x7f0e0281

.field public static final publisher:I = 0x7f0e0219

.field public static final read_article:I = 0x7f0e0289

.field public static final read_button:I = 0x7f0e028b

.field public static final reading_expando_hero:I = 0x7f0e017e

.field public static final retry_button:I = 0x7f0e02e7

.field public static final rightImage:I = 0x7f0e0230

.field public static final round_topic_header_logo:I = 0x7f0e0252

.field public static final search_fragment:I = 0x7f0e02e3

.field public static final search_plate:I = 0x7f0e01fa

.field public static final search_src_text:I = 0x7f0e01fb

.field public static final section_pager:I = 0x7f0e024f

.field public static final seekBar:I = 0x7f0e0223

.field public static final shelf_header_button:I = 0x7f0e017d

.field public static final shelf_header_title:I = 0x7f0e017c

.field public static final solid_circle:I = 0x7f0e02fb

.field public static final source_icon:I = 0x7f0e0228

.field public static final source_icon_no_image:I = 0x7f0e0245

.field public static final source_name:I = 0x7f0e0229

.field public static final spinner:I = 0x7f0e0224

.field public static final story_info_overlay:I = 0x7f0e0222

.field public static final subscribe:I = 0x7f0e028a

.field public static final subscribe_hint:I = 0x7f0e028c

.field public static final tab_bar:I = 0x7f0e02c1

.field public static final tagAlphaAnimator:I = 0x7f0e016d

.field public static final tagHandlesAlpha:I = 0x7f0e016e

.field public static final tagNBBuildContext:I = 0x7f0e0171

.field public static final time:I = 0x7f0e023d

.field public static final title:I = 0x7f0e01e8

.field public static final titleView:I = 0x7f0e02eb

.field public static final title_issues_fragment:I = 0x7f0e02e8

.field public static final title_textbox:I = 0x7f0e024a

.field public static final toggle_image_view:I = 0x7f0e0205

.field public static final toggle_text_view:I = 0x7f0e0206

.field public static final toolbar:I = 0x7f0e0270

.field public static final toolbar_container:I = 0x7f0e026e

.field public static final toolbar_shadow:I = 0x7f0e026f

.field public static final transition_image:I = 0x7f0e0257

.field public static final translation_languages_spinner:I = 0x7f0e02f0

.field public static final version_spinner:I = 0x7f0e0273

.field public static final videoPlayer:I = 0x7f0e0174

.field public static final web_part_fragment:I = 0x7f0e02f2

.field public static final widget_article_layout:I = 0x7f0e0290

.field public static final widget_empty_layout:I = 0x7f0e0294

.field public static final year_month_issues_fragment:I = 0x7f0e02f4
