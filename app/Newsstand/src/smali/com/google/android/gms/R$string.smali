.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final common_google_play_services_enable_button:I = 0x7f0a000b

.field public static final common_google_play_services_enable_text:I = 0x7f0a000a

.field public static final common_google_play_services_enable_title:I = 0x7f0a0009

.field public static final common_google_play_services_install_button:I = 0x7f0a0008

.field public static final common_google_play_services_install_text_phone:I = 0x7f0a0006

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0a0007

.field public static final common_google_play_services_install_title:I = 0x7f0a0005

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0a0011

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0a0010

.field public static final common_google_play_services_network_error_text:I = 0x7f0a000f

.field public static final common_google_play_services_network_error_title:I = 0x7f0a000e

.field public static final common_google_play_services_unknown_issue:I = 0x7f0a0012

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0a0015

.field public static final common_google_play_services_unsupported_text:I = 0x7f0a0014

.field public static final common_google_play_services_unsupported_title:I = 0x7f0a0013

.field public static final common_google_play_services_update_button:I = 0x7f0a0016

.field public static final common_google_play_services_update_text:I = 0x7f0a000d

.field public static final common_google_play_services_update_title:I = 0x7f0a000c
