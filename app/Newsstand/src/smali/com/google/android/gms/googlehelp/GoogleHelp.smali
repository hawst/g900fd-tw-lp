.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/googlehelp/GoogleHelp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field aaA:Z

.field aaB:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field aaC:Ljava/lang/String;

.field aaD:Ljava/lang/String;

.field aaE:Landroid/os/Bundle;

.field aaF:Landroid/graphics/Bitmap;

.field aaG:Z

.field aaH:Ljava/lang/String;

.field aaI:Ljava/lang/String;

.field aaJ:Landroid/net/Uri;

.field aaK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field aaL:I

.field aaM:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field aaw:Ljava/lang/String;

.field aax:Landroid/accounts/Account;

.field aay:Landroid/os/Bundle;

.field aaz:Z

.field final xK:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V
    .locals 1
    .param p1, "versionCode"    # I
    .param p2, "helpCenterContext"    # Ljava/lang/String;
    .param p3, "googleAccount"    # Landroid/accounts/Account;
    .param p4, "psdBundle"    # Landroid/os/Bundle;
    .param p5, "searchEnabled"    # Z
    .param p6, "metricsReportingEnabled"    # Z
    .param p8, "sendToEmailAddress"    # Ljava/lang/String;
    .param p9, "emailSubject"    # Ljava/lang/String;
    .param p10, "feedbackPsdBundle"    # Landroid/os/Bundle;
    .param p11, "screenShot"    # Landroid/graphics/Bitmap;
    .param p12, "supportContentApiAuthEnabled"    # Z
    .param p13, "apiKey"    # Ljava/lang/String;
    .param p14, "apiDebugOption"    # Ljava/lang/String;
    .param p15, "fallbackSupportUri"    # Landroid/net/Uri;
    .param p17, "helpActivityTheme"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/accounts/Account;",
            "Landroid/os/Bundle;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Landroid/graphics/Bitmap;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "supportPhoneNumbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p16, "overflowMenuItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;>;"
    .local p18, "offlineSuggestions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/OfflineSuggestion;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->xK:I

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaw:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aax:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aay:Landroid/os/Bundle;

    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaz:Z

    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaA:Z

    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaB:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaC:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaD:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaE:Landroid/os/Bundle;

    iput-object p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaF:Landroid/graphics/Bitmap;

    iput-boolean p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaG:Z

    iput-object p13, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaH:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaI:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaJ:Landroid/net/Uri;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaK:Ljava/util/List;

    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaL:I

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaM:Ljava/util/List;

    return-void
.end method

.method static b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    move v1, v0

    move-object v0, p0

    :goto_0
    const/high16 v2, 0x40000

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    shr-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    shr-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    move v1, v0

    move-object v0, p0

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 19
    .param p0, "helpcenterContext"    # Ljava/lang/String;

    .prologue
    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    const/16 v17, 0x0

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v2, p0

    invoke-direct/range {v0 .. v18}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/graphics/Bitmap;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "fallbackSupportUri"    # Landroid/net/Uri;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaJ:Landroid/net/Uri;

    return-object p0
.end method

.method public setFeedbackProductSpecificData(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaE:Landroid/os/Bundle;

    return-object p0
.end method

.method public setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "googleAccount"    # Landroid/accounts/Account;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aax:Landroid/accounts/Account;

    return-object p0
.end method

.method public setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 1
    .param p1, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->aaF:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/a;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    return-void
.end method
