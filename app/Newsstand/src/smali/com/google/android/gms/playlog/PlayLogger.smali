.class public Lcom/google/android/gms/playlog/PlayLogger;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;
    }
.end annotation


# instance fields
.field private final alP:Lcom/google/android/gms/internal/nj;

.field private alQ:Lcom/google/android/gms/internal/nh;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logSource"    # I
    .param p3, "uploadAccountName"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;

    .prologue
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/playlog/PlayLogger;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logSource"    # I
    .param p3, "uploadAccountName"    # Ljava/lang/String;
    .param p4, "loggingId"    # Ljava/lang/String;
    .param p5, "listener"    # Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;

    .prologue
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/playlog/PlayLogger;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "logSource"    # I
    .param p3, "uploadAccountName"    # Ljava/lang/String;
    .param p4, "loggingId"    # Ljava/lang/String;
    .param p5, "listener"    # Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;
    .param p6, "logAndroidId"    # Z

    .prologue
    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/nh;

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/nh;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/playlog/PlayLogger;->alQ:Lcom/google/android/gms/internal/nh;

    new-instance v0, Lcom/google/android/gms/internal/nj;

    new-instance v1, Lcom/google/android/gms/internal/ng;

    invoke-direct {v1, p5}, Lcom/google/android/gms/internal/ng;-><init>(Lcom/google/android/gms/playlog/PlayLogger$LoggerCallbacks;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/nj;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ng;)V

    iput-object v0, p0, Lcom/google/android/gms/playlog/PlayLogger;->alP:Lcom/google/android/gms/internal/nj;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public varargs logEvent(JLjava/lang/String;[B[Ljava/lang/String;)V
    .locals 9
    .param p1, "eventTime"    # J
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "sourceExtensionBytes"    # [B
    .param p5, "extras"    # [Ljava/lang/String;

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/playlog/PlayLogger;->alP:Lcom/google/android/gms/internal/nj;

    iget-object v7, p0, Lcom/google/android/gms/playlog/PlayLogger;->alQ:Lcom/google/android/gms/internal/nh;

    new-instance v1, Lcom/google/android/gms/internal/nd;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/internal/nd;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0, v7, v1}, Lcom/google/android/gms/internal/nj;->b(Lcom/google/android/gms/internal/nh;Lcom/google/android/gms/internal/nd;)V

    return-void
.end method

.method public varargs logEvent(Ljava/lang/String;[B[Ljava/lang/String;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "sourceExtensionBytes"    # [B
    .param p3, "extras"    # [Ljava/lang/String;

    .prologue
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/playlog/PlayLogger;->logEvent(JLjava/lang/String;[B[Ljava/lang/String;)V

    return-void
.end method

.method public varargs logEvent(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "extras"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/playlog/PlayLogger;->logEvent(Ljava/lang/String;[B[Ljava/lang/String;)V

    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/playlog/PlayLogger;->alP:Lcom/google/android/gms/internal/nj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/nj;->start()V

    return-void
.end method

.method public stop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/playlog/PlayLogger;->alP:Lcom/google/android/gms/internal/nj;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/nj;->stop()V

    return-void
.end method
