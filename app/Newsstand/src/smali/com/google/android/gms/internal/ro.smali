.class public final Lcom/google/android/gms/internal/ro;
.super Ljava/lang/Object;


# static fields
.field public static final aBe:[I

.field public static final aBf:[J

.field public static final aBg:[F

.field public static final aBh:[D

.field public static final aBi:[Z

.field public static final aBj:[Ljava/lang/String;

.field public static final aBk:[[B

.field public static final aBl:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBe:[I

    new-array v0, v1, [J

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBf:[J

    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBg:[F

    new-array v0, v1, [D

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBh:[D

    new-array v0, v1, [Z

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBi:[Z

    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBj:[Ljava/lang/String;

    new-array v0, v1, [[B

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBk:[[B

    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/internal/ro;->aBl:[B

    return-void
.end method

.method static z(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method
