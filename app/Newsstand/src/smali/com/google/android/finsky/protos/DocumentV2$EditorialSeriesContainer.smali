.class public final Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditorialSeriesContainer"
.end annotation


# instance fields
.field public colorThemeArgb:Ljava/lang/String;

.field public episodeSubtitle:Ljava/lang/String;

.field public episodeTitle:Ljava/lang/String;

.field public hasColorThemeArgb:Z

.field public hasEpisodeSubtitle:Z

.field public hasEpisodeTitle:Z

.field public hasSeriesSubtitle:Z

.field public hasSeriesTitle:Z

.field public seriesSubtitle:Ljava/lang/String;

.field public seriesTitle:Ljava/lang/String;

.field public videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4077
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4078
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->clear()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 4079
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4082
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    .line 4083
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    .line 4084
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    .line 4085
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    .line 4086
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    .line 4087
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    .line 4088
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    .line 4089
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    .line 4090
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    .line 4091
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    .line 4092
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 4093
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->cachedSize:I

    .line 4094
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4128
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4129
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 4130
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    .line 4131
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4133
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 4134
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    .line 4135
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4137
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 4138
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    .line 4139
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4141
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 4142
    :cond_6
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    .line 4143
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4145
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 4146
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    .line 4147
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4149
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 4150
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 4151
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    aget-object v0, v3, v1

    .line 4152
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v0, :cond_a

    .line 4153
    const/4 v3, 0x6

    .line 4154
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4150
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4158
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .end local v1    # "i":I
    :cond_b
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 4166
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4167
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4171
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4172
    :sswitch_0
    return-object p0

    .line 4177
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    .line 4178
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    goto :goto_0

    .line 4182
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    .line 4183
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    goto :goto_0

    .line 4187
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    .line 4188
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    goto :goto_0

    .line 4192
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    .line 4193
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    goto :goto_0

    .line 4197
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    .line 4198
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    goto :goto_0

    .line 4202
    :sswitch_6
    const/16 v5, 0x32

    .line 4203
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4204
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v5, :cond_2

    move v1, v4

    .line 4205
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 4207
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v1, :cond_1

    .line 4208
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4210
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4211
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;-><init>()V

    aput-object v5, v2, v1

    .line 4212
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4213
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 4210
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4204
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v1, v5

    goto :goto_1

    .line 4216
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;-><init>()V

    aput-object v5, v2, v1

    .line 4217
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4218
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    goto :goto_0

    .line 4167
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4037
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4100
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4101
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4103
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasSeriesSubtitle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 4104
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4106
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeTitle:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 4107
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4109
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasEpisodeSubtitle:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 4110
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4112
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->hasColorThemeArgb:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 4113
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4115
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 4116
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 4117
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    aget-object v0, v2, v1

    .line 4118
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    if-eqz v0, :cond_a

    .line 4119
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4116
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4123
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .end local v1    # "i":I
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4124
    return-void
.end method
