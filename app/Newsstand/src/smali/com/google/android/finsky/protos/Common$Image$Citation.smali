.class public final Lcom/google/android/finsky/protos/Common$Image$Citation;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Citation"
.end annotation


# instance fields
.field public hasTitleLocalized:Z

.field public hasUrl:Z

.field public titleLocalized:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1220
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1221
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Image$Citation;->clear()Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1222
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Image$Citation;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1225
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    .line 1226
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasTitleLocalized:Z

    .line 1227
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    .line 1228
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasUrl:Z

    .line 1229
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->cachedSize:I

    .line 1230
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1247
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1248
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasTitleLocalized:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1249
    :cond_0
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    .line 1250
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1252
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1253
    :cond_2
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    .line 1254
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1256
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Citation;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1264
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1265
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1269
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1270
    :sswitch_0
    return-object p0

    .line 1275
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    .line 1276
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasTitleLocalized:Z

    goto :goto_0

    .line 1280
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    .line 1281
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasUrl:Z

    goto :goto_0

    .line 1265
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0x62 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1195
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Image$Citation;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Citation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1236
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasTitleLocalized:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1237
    :cond_0
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->titleLocalized:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1239
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->hasUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1240
    :cond_2
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image$Citation;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1242
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1243
    return-void
.end method
