.class public final Lcom/google/android/finsky/protos/DocumentV2$Annotations;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Annotations"
.end annotation


# instance fields
.field public badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

.field public hasOfferNote:Z

.field public hasPrivacyPolicyUrl:Z

.field public link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

.field public offerNote:Ljava/lang/String;

.field public optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

.field public overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

.field public plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

.field public privacyPolicyUrl:Ljava/lang/String;

.field public promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

.field public sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

.field public template:Lcom/google/android/finsky/protos/DocumentV2$Template;

.field public warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 777
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 778
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 779
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 782
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 783
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 784
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 785
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 786
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 787
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 788
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 789
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 790
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 791
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 792
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 793
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 794
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 795
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 796
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 797
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 798
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 799
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 800
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 801
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    .line 802
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 803
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 804
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 805
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 806
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    .line 807
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 808
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 809
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->cachedSize:I

    .line 810
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 931
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 932
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_0

    .line 933
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 934
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 936
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1

    .line 937
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 938
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 940
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v3, :cond_2

    .line 941
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 942
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 944
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 945
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 946
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v3, v1

    .line 947
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 948
    const/4 v3, 0x4

    .line 949
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 945
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 953
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_5

    .line 954
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 955
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 957
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_6

    .line 958
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 959
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 961
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v3, :cond_7

    .line 962
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 963
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 965
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 966
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 967
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 968
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 969
    const/16 v3, 0x8

    .line 970
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 966
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 974
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 975
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 976
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v3, v1

    .line 977
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 978
    const/16 v3, 0x9

    .line 979
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 975
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 983
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_c

    .line 984
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 985
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 987
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_d

    .line 988
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 989
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 991
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_e

    .line 992
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 993
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 995
    :cond_e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 996
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v3, v3

    if-ge v1, v3, :cond_10

    .line 997
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v3, v1

    .line 998
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 999
    const/16 v3, 0xd

    .line 1000
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 996
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1004
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 1005
    :cond_11
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 1006
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1008
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_14

    .line 1009
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_14

    .line 1010
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 1011
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 1012
    const/16 v3, 0x10

    .line 1013
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1009
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1017
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v3, :cond_15

    .line 1018
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 1019
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1021
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 1022
    :cond_16
    const/16 v3, 0x12

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 1023
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1025
    :cond_17
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v3, :cond_18

    .line 1026
    const/16 v3, 0x13

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1027
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1029
    :cond_18
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v3, :cond_19

    .line 1030
    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1031
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1033
    :cond_19
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-lez v3, :cond_1b

    .line 1034
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v3, v3

    if-ge v1, v3, :cond_1b

    .line 1035
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v3, v1

    .line 1036
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 1037
    const/16 v3, 0x15

    .line 1038
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1034
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1042
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1c

    .line 1043
    const/16 v3, 0x16

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1044
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1046
    :cond_1c
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1d

    .line 1047
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1048
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1050
    :cond_1d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v3, :cond_1e

    .line 1051
    const/16 v3, 0x18

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1052
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1054
    :cond_1e
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-lez v3, :cond_20

    .line 1055
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v3, v3

    if-ge v1, v3, :cond_20

    .line 1056
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v3, v1

    .line 1057
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 1058
    const/16 v3, 0x19

    .line 1059
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1055
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1063
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_21

    .line 1064
    const/16 v3, 0x1a

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1065
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1067
    :cond_21
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 1075
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1076
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1080
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1081
    :sswitch_0
    return-object p0

    .line 1086
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1

    .line 1087
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1089
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1093
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_2

    .line 1094
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1096
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1100
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-nez v5, :cond_3

    .line 1101
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1103
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1107
    :sswitch_4
    const/16 v5, 0x22

    .line 1108
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1109
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_5

    move v1, v4

    .line 1110
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1112
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v1, :cond_4

    .line 1113
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1115
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1116
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1117
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1118
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1115
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1109
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v1, v5

    goto :goto_1

    .line 1121
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    aput-object v5, v2, v1

    .line 1122
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1123
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    goto :goto_0

    .line 1127
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_7

    .line 1128
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1130
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1134
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_8

    .line 1135
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1137
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1141
    :sswitch_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-nez v5, :cond_9

    .line 1142
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Template;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 1144
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1148
    :sswitch_8
    const/16 v5, 0x42

    .line 1149
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1150
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_b

    move v1, v4

    .line 1151
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1153
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_a

    .line 1154
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1156
    :cond_a
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 1157
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1158
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1159
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1156
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1150
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_3

    .line 1162
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_c
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1163
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1164
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1168
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_9
    const/16 v5, 0x4a

    .line 1169
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1170
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v5, :cond_e

    move v1, v4

    .line 1171
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 1173
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v1, :cond_d

    .line 1174
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1176
    :cond_d
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 1177
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1178
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1179
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1176
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1170
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v1, v5

    goto :goto_5

    .line 1182
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_f
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    aput-object v5, v2, v1

    .line 1183
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1184
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    goto/16 :goto_0

    .line 1188
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :sswitch_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_10

    .line 1189
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 1191
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1195
    :sswitch_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_11

    .line 1196
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1198
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1202
    :sswitch_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_12

    .line 1203
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1205
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1209
    :sswitch_d
    const/16 v5, 0x6a

    .line 1210
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1211
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-nez v5, :cond_14

    move v1, v4

    .line 1212
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 1214
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v1, :cond_13

    .line 1215
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1217
    :cond_13
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_15

    .line 1218
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1219
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1220
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1217
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1211
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_14
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v1, v5

    goto :goto_7

    .line 1223
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :cond_15
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;-><init>()V

    aput-object v5, v2, v1

    .line 1224
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1225
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    goto/16 :goto_0

    .line 1229
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 1230
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    goto/16 :goto_0

    .line 1234
    :sswitch_f
    const/16 v5, 0x82

    .line 1235
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1236
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_17

    move v1, v4

    .line 1237
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1239
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_16

    .line 1240
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1242
    :cond_16
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_18

    .line 1243
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1244
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1245
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1242
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1236
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_9

    .line 1248
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_18
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1249
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1250
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1254
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-nez v5, :cond_19

    .line 1255
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    .line 1257
    :cond_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1261
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 1262
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    goto/16 :goto_0

    .line 1266
    :sswitch_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-nez v5, :cond_1a

    .line 1267
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1269
    :cond_1a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1273
    :sswitch_13
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-nez v5, :cond_1b

    .line 1274
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 1276
    :cond_1b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1280
    :sswitch_14
    const/16 v5, 0xaa

    .line 1281
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1282
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-nez v5, :cond_1d

    move v1, v4

    .line 1283
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    .line 1285
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v1, :cond_1c

    .line 1286
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1288
    :cond_1c
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_1e

    .line 1289
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1290
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1291
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1288
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 1282
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1d
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v1, v5

    goto :goto_b

    .line 1294
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :cond_1e
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;-><init>()V

    aput-object v5, v2, v1

    .line 1295
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1296
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    goto/16 :goto_0

    .line 1300
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    :sswitch_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_1f

    .line 1301
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1303
    :cond_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1307
    :sswitch_16
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_20

    .line 1308
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1310
    :cond_20
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1314
    :sswitch_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v5, :cond_21

    .line 1315
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 1317
    :cond_21
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1321
    :sswitch_18
    const/16 v5, 0xca

    .line 1322
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1323
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-nez v5, :cond_23

    move v1, v4

    .line 1324
    .restart local v1    # "i":I
    :goto_d
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 1326
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v1, :cond_22

    .line 1327
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1329
    :cond_22
    :goto_e
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_24

    .line 1330
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1331
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1332
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1329
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1323
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_23
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v1, v5

    goto :goto_d

    .line 1335
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :cond_24
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;-><init>()V

    aput-object v5, v2, v1

    .line 1336
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1337
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    goto/16 :goto_0

    .line 1341
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    :sswitch_19
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_25

    .line 1342
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1344
    :cond_25
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1076
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc2 -> :sswitch_17
        0xca -> :sswitch_18
        0xd2 -> :sswitch_19
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 683
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 816
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_0

    .line 817
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 819
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1

    .line 820
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 822
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v2, :cond_2

    .line 823
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 825
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 826
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 827
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v0, v2, v1

    .line 828
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    if-eqz v0, :cond_3

    .line 829
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 826
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 833
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_5

    .line 834
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 836
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_6

    .line 837
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 839
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v2, :cond_7

    .line 840
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 842
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 843
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 844
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 845
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_8

    .line 846
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 843
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 850
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 851
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 852
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v0, v2, v1

    .line 853
    .restart local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-eqz v0, :cond_a

    .line 854
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 851
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 858
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v1    # "i":I
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_c

    .line 859
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 861
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_d

    .line 862
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 864
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_e

    .line 865
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 867
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 868
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 869
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->promotedDoc:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    aget-object v0, v2, v1

    .line 870
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    if-eqz v0, :cond_f

    .line 871
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 868
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 875
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .end local v1    # "i":I
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasOfferNote:Z

    if-nez v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 876
    :cond_11
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 878
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 879
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 880
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 881
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 882
    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 879
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 886
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    if-eqz v2, :cond_15

    .line 887
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->oBSOLETEReason:Lcom/google/android/finsky/protos/DocumentV2$OBSOLETE_Reason;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 889
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->hasPrivacyPolicyUrl:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 890
    :cond_16
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 892
    :cond_17
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v2, :cond_18

    .line 893
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 895
    :cond_18
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v2, :cond_19

    .line 896
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 898
    :cond_19
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 899
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 900
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    aget-object v0, v2, v1

    .line 901
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    if-eqz v0, :cond_1a

    .line 902
    const/16 v2, 0x15

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 899
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 906
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v1    # "i":I
    :cond_1b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1c

    .line 907
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 909
    :cond_1c
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1d

    .line 910
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRateAndReview:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 912
    :cond_1d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v2, :cond_1e

    .line 913
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 915
    :cond_1e
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-lez v2, :cond_20

    .line 916
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    array-length v2, v2

    if-ge v1, v2, :cond_20

    .line 917
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    aget-object v0, v2, v1

    .line 918
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    if-eqz v0, :cond_1f

    .line 919
    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 916
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 923
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .end local v1    # "i":I
    :cond_20
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_21

    .line 924
    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 926
    :cond_21
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 927
    return-void
.end method
