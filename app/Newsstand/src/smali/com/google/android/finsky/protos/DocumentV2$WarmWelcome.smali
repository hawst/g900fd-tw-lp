.class public final Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WarmWelcome"
.end annotation


# instance fields
.field public action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3755
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3756
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->clear()Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3757
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    .locals 1

    .prologue
    .line 3760
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3761
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->cachedSize:I

    .line 3762
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 3781
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3782
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3783
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 3784
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v3, v1

    .line 3785
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3786
    const/4 v3, 0x1

    .line 3787
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3783
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3791
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3799
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3800
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3804
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3805
    :sswitch_0
    return-object p0

    .line 3810
    :sswitch_1
    const/16 v5, 0xa

    .line 3811
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3812
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-nez v5, :cond_2

    move v1, v4

    .line 3813
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3815
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v1, :cond_1

    .line 3816
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3818
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3819
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3820
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3821
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3818
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3812
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v1, v5

    goto :goto_1

    .line 3824
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3825
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3826
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    goto :goto_0

    .line 3800
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3735
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3768
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3769
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 3770
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v2, v1

    .line 3771
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3772
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3769
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3776
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3777
    return-void
.end method
