.class public final Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ArtistDetails"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;


# instance fields
.field public detailsUrl:Ljava/lang/String;

.field public externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

.field public hasDetailsUrl:Z

.field public hasName:Z

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2422
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2423
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2424
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 2

    .prologue
    .line 2400
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v0, :cond_1

    .line 2401
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 2403
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v0, :cond_0

    .line 2404
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2406
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2408
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    return-object v0

    .line 2406
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2427
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    .line 2428
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    .line 2429
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 2430
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    .line 2431
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2432
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->cachedSize:I

    .line 2433
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2453
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2454
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2455
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    .line 2456
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2458
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2459
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 2460
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2462
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-eqz v1, :cond_4

    .line 2463
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2464
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2466
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2474
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2475
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2479
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2480
    :sswitch_0
    return-object p0

    .line 2485
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    .line 2486
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    goto :goto_0

    .line 2490
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 2491
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    goto :goto_0

    .line 2495
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-nez v1, :cond_1

    .line 2496
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2498
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2475
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2394
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2439
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2440
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2442
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2443
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2445
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-eqz v0, :cond_4

    .line 2446
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2448
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2449
    return-void
.end method
