.class public final Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "PlusProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PlusProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlusProfileResponse"
.end annotation


# instance fields
.field public hasIsGplusUser:Z

.field public isGplusUser:Z

.field public partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->clear()Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    iput-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->hasIsGplusUser:Z

    .line 43
    iput-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->cachedSize:I

    .line 45
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 65
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 66
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 68
    invoke-static {v3, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_1

    .line 71
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 72
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->hasIsGplusUser:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    if-eq v1, v3, :cond_3

    .line 75
    :cond_2
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    .line 76
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 87
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 91
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    :sswitch_0
    return-object p0

    .line 97
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v1, :cond_1

    .line 98
    new-instance v1, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 104
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_2

    .line 105
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 107
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    .line 112
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->hasIsGplusUser:Z

    goto :goto_0

    .line 87
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->plusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 57
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->hasIsGplusUser:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    if-eq v0, v2, :cond_3

    .line 58
    :cond_2
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->isGplusUser:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 60
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 61
    return-void
.end method
