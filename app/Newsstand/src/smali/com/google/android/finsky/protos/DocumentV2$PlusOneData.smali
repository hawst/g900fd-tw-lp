.class public final Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlusOneData"
.end annotation


# instance fields
.field public circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public circlesTotal:J

.field public hasCirclesTotal:Z

.field public hasSetByUser:Z

.field public hasTotal:Z

.field public oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

.field public setByUser:Z

.field public total:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1503
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1504
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->clear()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1505
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1508
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    .line 1509
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    .line 1510
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    .line 1511
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    .line 1512
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    .line 1513
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    .line 1514
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1515
    invoke-static {}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;->emptyArray()[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 1516
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->cachedSize:I

    .line 1517
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1553
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1554
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    if-eqz v3, :cond_1

    .line 1555
    :cond_0
    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    .line 1556
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1558
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 1559
    :cond_2
    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    .line 1560
    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1562
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    if-nez v3, :cond_4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    .line 1563
    :cond_4
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    .line 1564
    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1566
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 1567
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 1568
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v3, v1

    .line 1569
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_6

    .line 1570
    const/4 v3, 0x4

    .line 1571
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1567
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1575
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 1576
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 1577
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 1578
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 1579
    const/4 v3, 0x5

    .line 1580
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1576
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1584
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1592
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1593
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1597
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1598
    :sswitch_0
    return-object p0

    .line 1603
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    .line 1604
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    goto :goto_0

    .line 1608
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    .line 1609
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    goto :goto_0

    .line 1613
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    .line 1614
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    goto :goto_0

    .line 1618
    :sswitch_4
    const/16 v5, 0x22

    .line 1619
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1620
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v5, :cond_2

    move v1, v4

    .line 1621
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 1623
    .local v2, "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v1, :cond_1

    .line 1624
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1626
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1627
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 1628
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1629
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1626
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1620
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v1, v5

    goto :goto_1

    .line 1632
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 1633
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1634
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    goto :goto_0

    .line 1638
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :sswitch_5
    const/16 v5, 0x2a

    .line 1639
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1640
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 1641
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1643
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 1644
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1646
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1647
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1648
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1649
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1646
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1640
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 1652
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1653
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1654
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1593
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1468
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 1523
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    if-eqz v2, :cond_1

    .line 1524
    :cond_0
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1526
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 1527
    :cond_2
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1529
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_5

    .line 1530
    :cond_4
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1532
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1533
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 1534
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v2, v1

    .line 1535
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_6

    .line 1536
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1533
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1540
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1541
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 1542
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 1543
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 1544
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1541
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1548
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1549
    return-void
.end method
