.class public final Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupLicenseKey"
.end annotation


# instance fields
.field public dasherCustomerId:J

.field public docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

.field public hasDasherCustomerId:Z

.field public hasLicensedOfferType:Z

.field public hasRentalPeriodDays:Z

.field public hasType:Z

.field public licensedOfferType:I

.field public rentalPeriodDays:I

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3640
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3641
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->clear()Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    .line 3642
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3645
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    .line 3646
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    .line 3647
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    .line 3648
    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    .line 3649
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    .line 3650
    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    .line 3651
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    .line 3652
    iput v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    .line 3653
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    .line 3654
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->cachedSize:I

    .line 3655
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 3681
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3682
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 3683
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    .line 3684
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3686
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    if-eqz v1, :cond_2

    .line 3687
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    .line 3688
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3690
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    if-eqz v1, :cond_4

    .line 3691
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    .line 3692
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3694
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    if-nez v1, :cond_5

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    if-eqz v1, :cond_6

    .line 3695
    :cond_5
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    .line 3696
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3698
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    if-nez v1, :cond_7

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    if-eqz v1, :cond_8

    .line 3699
    :cond_7
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    .line 3700
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3702
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseKey;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3710
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3711
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3715
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3716
    :sswitch_0
    return-object p0

    .line 3721
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    .line 3722
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    goto :goto_0

    .line 3726
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    if-nez v1, :cond_1

    .line 3727
    new-instance v1, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    .line 3729
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3733
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    .line 3734
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    goto :goto_0

    .line 3738
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    .line 3739
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    goto :goto_0

    .line 3743
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    .line 3744
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    goto :goto_0

    .line 3711
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3604
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseKey;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3661
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasDasherCustomerId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3662
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->dasherCustomerId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 3664
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    if-eqz v0, :cond_2

    .line 3665
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->docid:Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3667
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasLicensedOfferType:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    if-eqz v0, :cond_4

    .line 3668
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->licensedOfferType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3670
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasType:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    if-eqz v0, :cond_6

    .line 3671
    :cond_5
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3673
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->hasRentalPeriodDays:Z

    if-nez v0, :cond_7

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    if-eqz v0, :cond_8

    .line 3674
    :cond_7
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseKey;->rentalPeriodDays:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3676
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3677
    return-void
.end method
