.class public final Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SeriesAntenna"
.end annotation


# instance fields
.field public colorThemeArgb:Ljava/lang/String;

.field public episodeSubtitle:Ljava/lang/String;

.field public episodeTitle:Ljava/lang/String;

.field public hasColorThemeArgb:Z

.field public hasEpisodeSubtitle:Z

.field public hasEpisodeTitle:Z

.field public hasSeriesSubtitle:Z

.field public hasSeriesTitle:Z

.field public sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

.field public seriesSubtitle:Ljava/lang/String;

.field public seriesTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3309
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3310
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->clear()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3311
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 3314
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    .line 3315
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    .line 3316
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    .line 3317
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    .line 3318
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    .line 3319
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    .line 3320
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    .line 3321
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    .line 3322
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    .line 3323
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    .line 3324
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3325
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3326
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->cachedSize:I

    .line 3327
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3359
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3360
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3361
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    .line 3362
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3364
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3365
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    .line 3366
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3368
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3369
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    .line 3370
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3372
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3373
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    .line 3374
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3376
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3377
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    .line 3378
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3380
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_a

    .line 3381
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3382
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3384
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_b

    .line 3385
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3386
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3388
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3397
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3401
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3402
    :sswitch_0
    return-object p0

    .line 3407
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    .line 3408
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    goto :goto_0

    .line 3412
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    .line 3413
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    goto :goto_0

    .line 3417
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    .line 3418
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    goto :goto_0

    .line 3422
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    .line 3423
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    goto :goto_0

    .line 3427
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    .line 3428
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    goto :goto_0

    .line 3432
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v1, :cond_1

    .line 3433
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3435
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3439
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-nez v1, :cond_2

    .line 3440
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 3442
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3397
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3266
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3333
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3334
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3336
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasSeriesSubtitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3337
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->seriesSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3339
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeTitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3340
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3342
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasEpisodeSubtitle:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3343
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeSubtitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3345
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->hasColorThemeArgb:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 3346
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3348
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v0, :cond_a

    .line 3349
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3351
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v0, :cond_b

    .line 3352
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3354
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3355
    return-void
.end method
