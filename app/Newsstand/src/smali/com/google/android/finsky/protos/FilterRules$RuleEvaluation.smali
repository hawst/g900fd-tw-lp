.class public final Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RuleEvaluation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;


# instance fields
.field public actualBoolValue:[Z

.field public actualDoubleValue:[D

.field public actualLongValue:[J

.field public actualStringValue:[Ljava/lang/String;

.field public rule:Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 706
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 707
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 2

    .prologue
    .line 679
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_1

    .line 680
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 682
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_0

    .line 683
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 685
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    return-object v0

    .line 685
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 1

    .prologue
    .line 710
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 711
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    .line 712
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 713
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BOOLEAN_ARRAY:[Z

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 714
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_DOUBLE_ARRAY:[D

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 715
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->cachedSize:I

    .line 716
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 753
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v5

    .line 754
    .local v5, "size":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v6, :cond_0

    .line 755
    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 756
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v6

    add-int/2addr v5, v6

    .line 758
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 759
    const/4 v0, 0x0

    .line 760
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 761
    .local v1, "dataSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_2

    .line 762
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v2, v6, v4

    .line 763
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 764
    add-int/lit8 v0, v0, 0x1

    .line 766
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v1, v6

    .line 761
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 769
    .end local v2    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v5, v1

    .line 770
    mul-int/lit8 v6, v0, 0x1

    add-int/2addr v5, v6

    .line 772
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-lez v6, :cond_5

    .line 773
    const/4 v1, 0x0

    .line 774
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-ge v4, v6, :cond_4

    .line 775
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v2, v6, v4

    .line 777
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v1, v6

    .line 774
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 779
    .end local v2    # "element":J
    :cond_4
    add-int/2addr v5, v1

    .line 780
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 782
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    if-lez v6, :cond_6

    .line 783
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x1

    .line 784
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 785
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 787
    .end local v1    # "dataSize":I
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    if-lez v6, :cond_7

    .line 788
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 789
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 790
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 792
    .end local v1    # "dataSize":I
    :cond_7
    return v5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 800
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 801
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 805
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 806
    :sswitch_0
    return-object p0

    .line 811
    :sswitch_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v8, :cond_1

    .line 812
    new-instance v8, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 814
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 818
    :sswitch_2
    const/16 v8, 0x12

    .line 819
    invoke-static {p1, v8}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 820
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-nez v8, :cond_3

    move v1, v7

    .line 821
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 822
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 823
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 825
    :cond_2
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_4

    .line 826
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 827
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 825
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 820
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 830
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 831
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    goto :goto_0

    .line 835
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v8, 0x18

    .line 836
    invoke-static {p1, v8}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 837
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_6

    move v1, v7

    .line 838
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 839
    .local v4, "newArray":[J
    if-eqz v1, :cond_5

    .line 840
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 842
    :cond_5
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_7

    .line 843
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 844
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 842
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 837
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_3

    .line 847
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 848
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    goto/16 :goto_0

    .line 852
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 853
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 855
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 856
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 857
    .local v5, "startPos":I
    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_8

    .line 858
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    .line 859
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 861
    :cond_8
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 862
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_a

    move v1, v7

    .line 863
    .restart local v1    # "i":I
    :goto_6
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 864
    .restart local v4    # "newArray":[J
    if-eqz v1, :cond_9

    .line 865
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 867
    :cond_9
    :goto_7
    array-length v8, v4

    if-ge v1, v8, :cond_b

    .line 868
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 867
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 862
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_a
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_6

    .line 870
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_b
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 871
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 875
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[J
    .end local v5    # "startPos":I
    :sswitch_5
    const/16 v8, 0x20

    .line 876
    invoke-static {p1, v8}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 877
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_d

    move v1, v7

    .line 878
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 879
    .local v4, "newArray":[Z
    if-eqz v1, :cond_c

    .line 880
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 882
    :cond_c
    :goto_9
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_e

    .line 883
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 884
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 882
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 877
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_d
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_8

    .line 887
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 888
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    goto/16 :goto_0

    .line 892
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 893
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 895
    .restart local v3    # "limit":I
    const/4 v0, 0x0

    .line 896
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 897
    .restart local v5    # "startPos":I
    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_f

    .line 898
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    .line 899
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 901
    :cond_f
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 902
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_11

    move v1, v7

    .line 903
    .restart local v1    # "i":I
    :goto_b
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 904
    .restart local v4    # "newArray":[Z
    if-eqz v1, :cond_10

    .line 905
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 907
    :cond_10
    :goto_c
    array-length v8, v4

    if-ge v1, v8, :cond_12

    .line 908
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 907
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 902
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_11
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_b

    .line 910
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_12
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 911
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 915
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[Z
    .end local v5    # "startPos":I
    :sswitch_7
    const/16 v8, 0x29

    .line 916
    invoke-static {p1, v8}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 917
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_14

    move v1, v7

    .line 918
    .restart local v1    # "i":I
    :goto_d
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 919
    .local v4, "newArray":[D
    if-eqz v1, :cond_13

    .line 920
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 922
    :cond_13
    :goto_e
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_15

    .line 923
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 924
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 922
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 917
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_14
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_d

    .line 927
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 928
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    goto/16 :goto_0

    .line 932
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 933
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 934
    .restart local v3    # "limit":I
    div-int/lit8 v0, v2, 0x8

    .line 935
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_17

    move v1, v7

    .line 936
    .restart local v1    # "i":I
    :goto_f
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 937
    .restart local v4    # "newArray":[D
    if-eqz v1, :cond_16

    .line 938
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 940
    :cond_16
    :goto_10
    array-length v8, v4

    if-ge v1, v8, :cond_18

    .line 941
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 940
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 935
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_17
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_f

    .line 943
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_18
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 944
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 801
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x29 -> :sswitch_7
        0x2a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 673
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 722
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_0

    .line 723
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 725
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 726
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 727
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 728
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 729
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 726
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 733
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-lez v2, :cond_3

    .line 734
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 735
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 734
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 738
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-lez v2, :cond_4

    .line 739
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 740
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    aget-boolean v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 739
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 743
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-lez v2, :cond_5

    .line 744
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 745
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 744
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 748
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 749
    return-void
.end method
