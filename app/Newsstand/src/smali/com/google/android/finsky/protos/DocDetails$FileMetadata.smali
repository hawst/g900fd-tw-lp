.class public final Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileMetadata"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;


# instance fields
.field public fileType:I

.field public hasFileType:Z

.field public hasSize:Z

.field public hasSplitId:Z

.field public hasVersionCode:Z

.field public size:J

.field public splitId:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 710
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 711
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->clear()Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    .line 712
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .locals 2

    .prologue
    .line 683
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    if-nez v0, :cond_1

    .line 684
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 686
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    if-nez v0, :cond_0

    .line 687
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    .line 689
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    return-object v0

    .line 689
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 715
    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    .line 716
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasFileType:Z

    .line 717
    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    .line 718
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasVersionCode:Z

    .line 719
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    .line 720
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSize:Z

    .line 721
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    .line 722
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSplitId:Z

    .line 723
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->cachedSize:I

    .line 724
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 747
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 748
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasFileType:Z

    if-eqz v1, :cond_1

    .line 749
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    .line 750
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 752
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasVersionCode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    if-eqz v1, :cond_3

    .line 753
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    .line 754
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 756
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSize:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 757
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    .line 758
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 760
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSplitId:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 761
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    .line 762
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 764
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 772
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 773
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 777
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 778
    :sswitch_0
    return-object p0

    .line 783
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 784
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 788
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    .line 789
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasFileType:Z

    goto :goto_0

    .line 795
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    .line 796
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasVersionCode:Z

    goto :goto_0

    .line 800
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    .line 801
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSize:Z

    goto :goto_0

    .line 805
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    .line 806
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSplitId:Z

    goto :goto_0

    .line 773
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 784
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 672
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 730
    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasFileType:Z

    if-eqz v0, :cond_1

    .line 731
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->fileType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 733
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasVersionCode:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    if-eqz v0, :cond_3

    .line 734
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 736
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSize:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 737
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->size:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 739
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->hasSplitId:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 740
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->splitId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 742
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 743
    return-void
.end method
