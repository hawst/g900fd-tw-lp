.class public final Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MagazineDetails"
.end annotation


# instance fields
.field public deliveryFrequencyDescription:Ljava/lang/String;

.field public deviceAvailabilityDescriptionHtml:Ljava/lang/String;

.field public hasDeliveryFrequencyDescription:Z

.field public hasDeviceAvailabilityDescriptionHtml:Z

.field public hasParentDetailsUrl:Z

.field public hasPsvDescription:Z

.field public parentDetailsUrl:Ljava/lang/String;

.field public psvDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3970
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3971
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    .line 3972
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3975
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 3976
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasParentDetailsUrl:Z

    .line 3977
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    .line 3978
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeviceAvailabilityDescriptionHtml:Z

    .line 3979
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    .line 3980
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasPsvDescription:Z

    .line 3981
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    .line 3982
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeliveryFrequencyDescription:Z

    .line 3983
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->cachedSize:I

    .line 3984
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4007
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4008
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasParentDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4009
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4010
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4012
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeviceAvailabilityDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4013
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    .line 4014
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4016
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasPsvDescription:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4017
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    .line 4018
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4020
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeliveryFrequencyDescription:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 4021
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    .line 4022
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4024
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4032
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4033
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4037
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4038
    :sswitch_0
    return-object p0

    .line 4043
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4044
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasParentDetailsUrl:Z

    goto :goto_0

    .line 4048
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    .line 4049
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeviceAvailabilityDescriptionHtml:Z

    goto :goto_0

    .line 4053
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    .line 4054
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasPsvDescription:Z

    goto :goto_0

    .line 4058
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    .line 4059
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeliveryFrequencyDescription:Z

    goto :goto_0

    .line 4033
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3937
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3990
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasParentDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3991
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->parentDetailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3993
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeviceAvailabilityDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3994
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3996
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasPsvDescription:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3997
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3999
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->hasDeliveryFrequencyDescription:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4000
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4002
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4003
    return-void
.end method
