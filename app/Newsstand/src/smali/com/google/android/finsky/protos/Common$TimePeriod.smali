.class public final Lcom/google/android/finsky/protos/Common$TimePeriod;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimePeriod"
.end annotation


# instance fields
.field public count:I

.field public hasCount:Z

.field public hasUnit:Z

.field public unit:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2192
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2193
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$TimePeriod;->clear()Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2194
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$TimePeriod;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2197
    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    .line 2198
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    .line 2199
    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    .line 2200
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    .line 2201
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->cachedSize:I

    .line 2202
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2219
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2220
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    if-eqz v1, :cond_1

    .line 2221
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    .line 2222
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2224
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    if-eqz v1, :cond_3

    .line 2225
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    .line 2226
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2228
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$TimePeriod;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2237
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2241
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2242
    :sswitch_0
    return-object p0

    .line 2247
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2248
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2257
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    .line 2258
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    goto :goto_0

    .line 2264
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    .line 2265
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    goto :goto_0

    .line 2237
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 2248
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2157
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$TimePeriod;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$TimePeriod;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2208
    iget v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    if-eqz v0, :cond_1

    .line 2209
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2211
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    if-eqz v0, :cond_3

    .line 2212
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2214
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2215
    return-void
.end method
