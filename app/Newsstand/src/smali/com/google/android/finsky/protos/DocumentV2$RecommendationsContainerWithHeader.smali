.class public final Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecommendationsContainerWithHeader"
.end annotation


# instance fields
.field public primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4317
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4318
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 4319
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    .locals 1

    .prologue
    .line 4322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4323
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4324
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->cachedSize:I

    .line 4325
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4347
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4348
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_0

    .line 4349
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4350
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4352
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 4353
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 4354
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 4355
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_1

    .line 4356
    const/4 v3, 0x3

    .line 4357
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4353
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4361
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4369
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4370
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4374
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4375
    :sswitch_0
    return-object p0

    .line 4380
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_1

    .line 4381
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4383
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 4387
    :sswitch_2
    const/16 v5, 0x1a

    .line 4388
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4389
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_3

    move v1, v4

    .line 4390
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4392
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_2

    .line 4393
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4395
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 4396
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 4397
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4398
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 4395
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4389
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_1

    .line 4401
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 4402
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4403
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto :goto_0

    .line 4370
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4294
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4331
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_0

    .line 4332
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4334
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 4335
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 4336
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 4337
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_1

    .line 4338
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4335
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4342
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4343
    return-void
.end method
