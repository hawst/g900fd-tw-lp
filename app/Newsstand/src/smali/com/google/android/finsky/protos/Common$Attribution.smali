.class public final Lcom/google/android/finsky/protos/Common$Attribution;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Attribution"
.end annotation


# instance fields
.field public hasLicenseTitle:Z

.field public hasLicenseUrl:Z

.field public hasSourceTitle:Z

.field public hasSourceUrl:Z

.field public licenseTitle:Ljava/lang/String;

.field public licenseUrl:Ljava/lang/String;

.field public sourceTitle:Ljava/lang/String;

.field public sourceUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5599
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 5600
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Attribution;->clear()Lcom/google/android/finsky/protos/Common$Attribution;

    .line 5601
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Attribution;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5604
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    .line 5605
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    .line 5606
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    .line 5607
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    .line 5608
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    .line 5609
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    .line 5610
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    .line 5611
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    .line 5612
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->cachedSize:I

    .line 5613
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5636
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5637
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5638
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    .line 5639
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5641
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5642
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    .line 5643
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5645
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 5646
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    .line 5647
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5649
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 5650
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    .line 5651
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5653
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Attribution;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 5661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5662
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5666
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5667
    :sswitch_0
    return-object p0

    .line 5672
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    .line 5673
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    goto :goto_0

    .line 5677
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    .line 5678
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    goto :goto_0

    .line 5682
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    .line 5683
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    goto :goto_0

    .line 5687
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    .line 5688
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    goto :goto_0

    .line 5662
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5566
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Attribution;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Attribution;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5619
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5620
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5622
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5623
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5625
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5626
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5628
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 5629
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5631
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 5632
    return-void
.end method
