.class public final Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Term"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;


# instance fields
.field public body:Ljava/lang/String;

.field public hasBody:Z

.field public hasHeader:Z

.field public header:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3584
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3585
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    .line 3586
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    .locals 2

    .prologue
    .line 3565
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    if-nez v0, :cond_1

    .line 3566
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3568
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    if-nez v0, :cond_0

    .line 3569
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    .line 3571
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3573
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    return-object v0

    .line 3571
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3589
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    .line 3590
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasHeader:Z

    .line 3591
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    .line 3592
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasBody:Z

    .line 3593
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->cachedSize:I

    .line 3594
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3611
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3612
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasHeader:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3613
    :cond_0
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    .line 3614
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3616
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasBody:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3617
    :cond_2
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    .line 3618
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3620
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3628
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3629
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3633
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3634
    :sswitch_0
    return-object p0

    .line 3639
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    .line 3640
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasHeader:Z

    goto :goto_0

    .line 3644
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    .line 3645
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasBody:Z

    goto :goto_0

    .line 3629
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2a -> :sswitch_1
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3559
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3600
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasHeader:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3601
    :cond_0
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->header:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3603
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->hasBody:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3604
    :cond_2
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm$Term;->body:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3606
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3607
    return-void
.end method
