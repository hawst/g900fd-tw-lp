.class public final Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Ownership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Ownership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OwnershipInfo"
.end annotation


# instance fields
.field public autoRenewing:Z

.field public developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

.field public groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

.field public hasAutoRenewing:Z

.field public hasHidden:Z

.field public hasInitiationTimestampMsec:Z

.field public hasLibraryExpirationTimestampMsec:Z

.field public hasPostDeliveryRefundWindowMsec:Z

.field public hasPreordered:Z

.field public hasQuantity:Z

.field public hasRefundTimeoutTimestampMsec:Z

.field public hasValidUntilTimestampMsec:Z

.field public hidden:Z

.field public initiationTimestampMsec:J

.field public libraryExpirationTimestampMsec:J

.field public licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

.field public postDeliveryRefundWindowMsec:J

.field public preordered:Z

.field public quantity:I

.field public refundTimeoutTimestampMsec:J

.field public rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

.field public validUntilTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->clear()Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 75
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 78
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    .line 79
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    .line 80
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    .line 81
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    .line 82
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    .line 83
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    .line 84
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    .line 85
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    .line 86
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    .line 87
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    .line 88
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    .line 89
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    .line 90
    iput-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    .line 91
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    .line 92
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    .line 93
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    .line 94
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    .line 95
    iput-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 96
    iput-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 97
    iput-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 98
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    .line 99
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->cachedSize:I

    .line 101
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 151
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 152
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 153
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    .line 154
    invoke-static {v6, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 157
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    .line 158
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    if-eqz v1, :cond_5

    .line 161
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    .line 162
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 165
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    .line 166
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 169
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    .line 170
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-eqz v1, :cond_a

    .line 173
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    .line 174
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    if-eqz v1, :cond_c

    .line 177
    :cond_b
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    .line 178
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    if-eqz v1, :cond_e

    .line 181
    :cond_d
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    .line 182
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v1, :cond_f

    .line 185
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 186
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-eqz v1, :cond_10

    .line 189
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 190
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-eqz v1, :cond_11

    .line 193
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 194
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    if-nez v1, :cond_12

    iget v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    if-eq v1, v6, :cond_13

    .line 197
    :cond_12
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    .line 198
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    if-nez v1, :cond_14

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_15

    .line 201
    :cond_14
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    .line 202
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_15
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 212
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 213
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 217
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    :sswitch_0
    return-object p0

    .line 223
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    .line 224
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    goto :goto_0

    .line 228
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    .line 229
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    goto :goto_0

    .line 233
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    .line 234
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    goto :goto_0

    .line 238
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    .line 239
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    goto :goto_0

    .line 243
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    .line 244
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    goto :goto_0

    .line 248
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-nez v1, :cond_1

    .line 249
    new-instance v1, Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$SignedData;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    .line 251
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 255
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    .line 256
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    goto :goto_0

    .line 260
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    .line 261
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    goto :goto_0

    .line 265
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-nez v1, :cond_2

    .line 266
    new-instance v1, Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$RentalTerms;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 268
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 272
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-nez v1, :cond_3

    .line 273
    new-instance v1, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    .line 275
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 279
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-nez v1, :cond_4

    .line 280
    new-instance v1, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    .line 282
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 286
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    .line 287
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    goto/16 :goto_0

    .line 291
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    .line 292
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    goto/16 :goto_0

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x70 -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 107
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasInitiationTimestampMsec:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 108
    :cond_0
    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->initiationTimestampMsec:J

    invoke-virtual {p1, v6, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 110
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasValidUntilTimestampMsec:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 111
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->validUntilTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 113
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasAutoRenewing:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    if-eqz v0, :cond_5

    .line 114
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->autoRenewing:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 116
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasRefundTimeoutTimestampMsec:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 117
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->refundTimeoutTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 119
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPostDeliveryRefundWindowMsec:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 120
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->postDeliveryRefundWindowMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 122
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    if-eqz v0, :cond_a

    .line 123
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->developerPurchaseInfo:Lcom/google/android/finsky/protos/Common$SignedData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 125
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasPreordered:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    if-eqz v0, :cond_c

    .line 126
    :cond_b
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->preordered:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 128
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasHidden:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    if-eqz v0, :cond_e

    .line 129
    :cond_d
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hidden:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 131
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v0, :cond_f

    .line 132
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 134
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    if-eqz v0, :cond_10

    .line 135
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->groupLicenseInfo:Lcom/google/android/finsky/protos/GroupLicense$GroupLicenseInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 137
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    if-eqz v0, :cond_11

    .line 138
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->licensedDocumentInfo:Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 140
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasQuantity:Z

    if-nez v0, :cond_12

    iget v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    if-eq v0, v6, :cond_13

    .line 141
    :cond_12
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->quantity:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 143
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->hasLibraryExpirationTimestampMsec:Z

    if-nez v0, :cond_14

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_15

    .line 144
    :cond_14
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;->libraryExpirationTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 146
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 147
    return-void
.end method
