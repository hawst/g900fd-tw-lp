.class public final Lcom/google/android/finsky/protos/FilterRules$Rule;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rule"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;


# instance fields
.field public availabilityProblemType:I

.field public comment:Ljava/lang/String;

.field public constArg:[I

.field public doubleArg:[D

.field public hasAvailabilityProblemType:Z

.field public hasComment:Z

.field public hasIncludeMissingValues:Z

.field public hasKey:Z

.field public hasNegate:Z

.field public hasOperator:Z

.field public hasResponseCode:Z

.field public includeMissingValues:Z

.field public key:I

.field public longArg:[J

.field public negate:Z

.field public operator:I

.field public responseCode:I

.field public stringArg:[Ljava/lang/String;

.field public stringArgHash:[J

.field public subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$Rule;->clear()Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 128
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 2

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v0, :cond_1

    .line 70
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$Rule;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 75
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    .line 132
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    .line 133
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    .line 134
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    .line 135
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    .line 136
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    .line 137
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    .line 138
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    .line 139
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    .line 140
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_DOUBLE_ARRAY:[D

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    .line 141
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    .line 142
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$Rule;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Rule;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 143
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    .line 144
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    .line 145
    iput v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    .line 146
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    .line 147
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    .line 148
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    .line 149
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    .line 150
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    .line 151
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->cachedSize:I

    .line 152
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 220
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v5

    .line 221
    .local v5, "size":I
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    if-eqz v6, :cond_1

    .line 222
    :cond_0
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    .line 223
    invoke-static {v8, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v5, v6

    .line 225
    :cond_1
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    if-ne v6, v8, :cond_2

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    if-eqz v6, :cond_3

    .line 226
    :cond_2
    const/4 v6, 0x2

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    .line 227
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 229
    :cond_3
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    if-ne v6, v8, :cond_4

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    if-eqz v6, :cond_5

    .line 230
    :cond_4
    const/4 v6, 0x3

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    .line 231
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 233
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_8

    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 236
    .local v1, "dataSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_7

    .line 237
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    aget-object v2, v6, v4

    .line 238
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 239
    add-int/lit8 v0, v0, 0x1

    .line 241
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v1, v6

    .line 236
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 244
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v5, v1

    .line 245
    mul-int/lit8 v6, v0, 0x1

    add-int/2addr v5, v6

    .line 247
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    if-lez v6, :cond_a

    .line 248
    const/4 v1, 0x0

    .line 249
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    if-ge v4, v6, :cond_9

    .line 250
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    aget-wide v2, v6, v4

    .line 252
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v1, v6

    .line 249
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 254
    .end local v2    # "element":J
    :cond_9
    add-int/2addr v5, v1

    .line 255
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 257
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    if-lez v6, :cond_b

    .line 258
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 259
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 260
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 262
    .end local v1    # "dataSize":I
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v6, v6

    if-lez v6, :cond_d

    .line 263
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v6, v6

    if-ge v4, v6, :cond_d

    .line 264
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    aget-object v2, v6, v4

    .line 265
    .local v2, "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v2, :cond_c

    .line 266
    const/4 v6, 0x7

    .line 267
    invoke-static {v6, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v6

    add-int/2addr v5, v6

    .line 263
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 271
    .end local v2    # "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    .end local v4    # "i":I
    :cond_d
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    if-ne v6, v8, :cond_e

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    if-eqz v6, :cond_f

    .line 272
    :cond_e
    const/16 v6, 0x8

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    .line 273
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 275
    :cond_f
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    if-nez v6, :cond_10

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_11

    .line 276
    :cond_10
    const/16 v6, 0x9

    iget-object v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    .line 277
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v6

    add-int/2addr v5, v6

    .line 279
    :cond_11
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    if-lez v6, :cond_12

    .line 280
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 281
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 282
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 284
    .end local v1    # "dataSize":I
    :cond_12
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    if-lez v6, :cond_14

    .line 285
    const/4 v1, 0x0

    .line 286
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    if-ge v4, v6, :cond_13

    .line 287
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    aget v2, v6, v4

    .line 289
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v1, v6

    .line 286
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 291
    .end local v2    # "element":I
    :cond_13
    add-int/2addr v5, v1

    .line 292
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 294
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_14
    iget v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    if-ne v6, v8, :cond_15

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    if-eqz v6, :cond_16

    .line 295
    :cond_15
    const/16 v6, 0xc

    iget v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    .line 296
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v6

    add-int/2addr v5, v6

    .line 298
    :cond_16
    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    if-nez v6, :cond_17

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    if-eqz v6, :cond_18

    .line 299
    :cond_17
    const/16 v6, 0xd

    iget-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    .line 300
    invoke-static {v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v6

    add-int/2addr v5, v6

    .line 302
    :cond_18
    return v5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Rule;
    .locals 18
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v10

    .line 311
    .local v10, "tag":I
    sparse-switch v10, :sswitch_data_0

    .line 315
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v15

    if-nez v15, :cond_0

    .line 316
    :sswitch_0
    return-object p0

    .line 321
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    .line 322
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    goto :goto_0

    .line 326
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 327
    .local v14, "value":I
    packed-switch v14, :pswitch_data_0

    goto :goto_0

    .line 341
    :pswitch_0
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    .line 342
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    goto :goto_0

    .line 348
    .end local v14    # "value":I
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 349
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 382
    :pswitch_2
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    .line 383
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    goto :goto_0

    .line 389
    .end local v14    # "value":I
    :sswitch_4
    const/16 v15, 0x22

    .line 390
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v2

    .line 391
    .local v2, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-nez v15, :cond_2

    const/4 v4, 0x0

    .line 392
    .local v4, "i":I
    :goto_1
    add-int v15, v4, v2

    new-array v8, v15, [Ljava/lang/String;

    .line 393
    .local v8, "newArray":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 394
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 396
    :cond_1
    :goto_2
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_3

    .line 397
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v8, v4

    .line 398
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 396
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 391
    .end local v4    # "i":I
    .end local v8    # "newArray":[Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v4, v15

    goto :goto_1

    .line 401
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v8, v4

    .line 402
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    goto/16 :goto_0

    .line 406
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[Ljava/lang/String;
    :sswitch_5
    const/16 v15, 0x28

    .line 407
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v2

    .line 408
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-nez v15, :cond_5

    const/4 v4, 0x0

    .line 409
    .restart local v4    # "i":I
    :goto_3
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 410
    .local v8, "newArray":[J
    if-eqz v4, :cond_4

    .line 411
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413
    :cond_4
    :goto_4
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_6

    .line 414
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 415
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 413
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 408
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v4, v15

    goto :goto_3

    .line 418
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 419
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    goto/16 :goto_0

    .line 423
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 424
    .local v6, "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 426
    .local v7, "limit":I
    const/4 v2, 0x0

    .line 427
    .restart local v2    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v9

    .line 428
    .local v9, "startPos":I
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_7

    .line 429
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    .line 430
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 432
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 433
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-nez v15, :cond_9

    const/4 v4, 0x0

    .line 434
    .restart local v4    # "i":I
    :goto_6
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 435
    .restart local v8    # "newArray":[J
    if-eqz v4, :cond_8

    .line 436
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 438
    :cond_8
    :goto_7
    array-length v15, v8

    if-ge v4, v15, :cond_a

    .line 439
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 438
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 433
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_9
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v4, v15

    goto :goto_6

    .line 441
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_a
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    .line 442
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 446
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[J
    .end local v9    # "startPos":I
    :sswitch_7
    const/16 v15, 0x31

    .line 447
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v2

    .line 448
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-nez v15, :cond_c

    const/4 v4, 0x0

    .line 449
    .restart local v4    # "i":I
    :goto_8
    add-int v15, v4, v2

    new-array v8, v15, [D

    .line 450
    .local v8, "newArray":[D
    if-eqz v4, :cond_b

    .line 451
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 453
    :cond_b
    :goto_9
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_d

    .line 454
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 455
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 453
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 448
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :cond_c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v4, v15

    goto :goto_8

    .line 458
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[D
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 459
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    goto/16 :goto_0

    .line 463
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :sswitch_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 464
    .restart local v6    # "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 465
    .restart local v7    # "limit":I
    div-int/lit8 v2, v6, 0x8

    .line 466
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-nez v15, :cond_f

    const/4 v4, 0x0

    .line 467
    .restart local v4    # "i":I
    :goto_a
    add-int v15, v4, v2

    new-array v8, v15, [D

    .line 468
    .restart local v8    # "newArray":[D
    if-eqz v4, :cond_e

    .line 469
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 471
    :cond_e
    :goto_b
    array-length v15, v8

    if-ge v4, v15, :cond_10

    .line 472
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 471
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 466
    .end local v4    # "i":I
    .end local v8    # "newArray":[D
    :cond_f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v4, v15

    goto :goto_a

    .line 474
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[D
    :cond_10
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    .line 475
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 479
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[D
    :sswitch_9
    const/16 v15, 0x3a

    .line 480
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v2

    .line 481
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v15, :cond_12

    const/4 v4, 0x0

    .line 482
    .restart local v4    # "i":I
    :goto_c
    add-int v15, v4, v2

    new-array v8, v15, [Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 484
    .local v8, "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v4, :cond_11

    .line 485
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 487
    :cond_11
    :goto_d
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_13

    .line 488
    new-instance v15, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v15}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    aput-object v15, v8, v4

    .line 489
    aget-object v15, v8, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 490
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 487
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 481
    .end local v4    # "i":I
    .end local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v4, v15

    goto :goto_c

    .line 493
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :cond_13
    new-instance v15, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v15}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    aput-object v15, v8, v4

    .line 494
    aget-object v15, v8, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 495
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    goto/16 :goto_0

    .line 499
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Rule;
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 500
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_2

    :pswitch_3
    goto/16 :goto_0

    .line 518
    :pswitch_4
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    .line 519
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    goto/16 :goto_0

    .line 525
    .end local v14    # "value":I
    :sswitch_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    .line 526
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    goto/16 :goto_0

    .line 530
    :sswitch_c
    const/16 v15, 0x51

    .line 531
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v2

    .line 532
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-nez v15, :cond_15

    const/4 v4, 0x0

    .line 533
    .restart local v4    # "i":I
    :goto_e
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 534
    .local v8, "newArray":[J
    if-eqz v4, :cond_14

    .line 535
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 537
    :cond_14
    :goto_f
    array-length v15, v8

    add-int/lit8 v15, v15, -0x1

    if-ge v4, v15, :cond_16

    .line 538
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 539
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 537
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 532
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_15
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v4, v15

    goto :goto_e

    .line 542
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 543
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    goto/16 :goto_0

    .line 547
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :sswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v6

    .line 548
    .restart local v6    # "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 549
    .restart local v7    # "limit":I
    div-int/lit8 v2, v6, 0x8

    .line 550
    .restart local v2    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-nez v15, :cond_18

    const/4 v4, 0x0

    .line 551
    .restart local v4    # "i":I
    :goto_10
    add-int v15, v4, v2

    new-array v8, v15, [J

    .line 552
    .restart local v8    # "newArray":[J
    if-eqz v4, :cond_17

    .line 553
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 555
    :cond_17
    :goto_11
    array-length v15, v8

    if-ge v4, v15, :cond_19

    .line 556
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v16

    aput-wide v16, v8, v4

    .line 555
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    .line 550
    .end local v4    # "i":I
    .end local v8    # "newArray":[J
    :cond_18
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v4, v15

    goto :goto_10

    .line 558
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[J
    :cond_19
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    .line 559
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 563
    .end local v2    # "arrayLength":I
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v7    # "limit":I
    .end local v8    # "newArray":[J
    :sswitch_e
    const/16 v15, 0x58

    .line 564
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v6

    .line 565
    .restart local v6    # "length":I
    new-array v13, v6, [I

    .line 566
    .local v13, "validValues":[I
    const/4 v11, 0x0

    .line 567
    .local v11, "validCount":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    move v12, v11

    .end local v11    # "validCount":I
    .local v12, "validCount":I
    :goto_12
    if-ge v4, v6, :cond_1b

    .line 568
    if-eqz v4, :cond_1a

    .line 569
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 571
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 572
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_3

    move v11, v12

    .line 567
    .end local v12    # "validCount":I
    .restart local v11    # "validCount":I
    :goto_13
    add-int/lit8 v4, v4, 0x1

    move v12, v11

    .end local v11    # "validCount":I
    .restart local v12    # "validCount":I
    goto :goto_12

    .line 577
    :pswitch_5
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "validCount":I
    .restart local v11    # "validCount":I
    aput v14, v13, v12

    goto :goto_13

    .line 581
    .end local v11    # "validCount":I
    .end local v14    # "value":I
    .restart local v12    # "validCount":I
    :cond_1b
    if-eqz v12, :cond_0

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-nez v15, :cond_1c

    const/4 v4, 0x0

    .line 583
    :goto_14
    if-nez v4, :cond_1d

    array-length v15, v13

    if-ne v12, v15, :cond_1d

    .line 584
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    goto/16 :goto_0

    .line 582
    :cond_1c
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v4, v15

    goto :goto_14

    .line 586
    :cond_1d
    add-int v15, v4, v12

    new-array v8, v15, [I

    .line 587
    .local v8, "newArray":[I
    if-eqz v4, :cond_1e

    .line 588
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 590
    :cond_1e
    const/4 v15, 0x0

    invoke-static {v13, v15, v8, v4, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 591
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    goto/16 :goto_0

    .line 597
    .end local v4    # "i":I
    .end local v6    # "length":I
    .end local v8    # "newArray":[I
    .end local v12    # "validCount":I
    .end local v13    # "validValues":[I
    :sswitch_f
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v3

    .line 598
    .local v3, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v7

    .line 600
    .restart local v7    # "limit":I
    const/4 v2, 0x0

    .line 601
    .restart local v2    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v9

    .line 602
    .restart local v9    # "startPos":I
    :goto_15
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_1f

    .line 603
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v15

    packed-switch v15, :pswitch_data_4

    goto :goto_15

    .line 608
    :pswitch_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 612
    :cond_1f
    if-eqz v2, :cond_23

    .line 613
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 614
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-nez v15, :cond_21

    const/4 v4, 0x0

    .line 615
    .restart local v4    # "i":I
    :goto_16
    add-int v15, v4, v2

    new-array v8, v15, [I

    .line 616
    .restart local v8    # "newArray":[I
    if-eqz v4, :cond_20

    .line 617
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v15, v0, v8, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 619
    :cond_20
    :goto_17
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v15

    if-lez v15, :cond_22

    .line 620
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 621
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_5

    goto :goto_17

    .line 626
    :pswitch_7
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aput v14, v8, v4

    move v4, v5

    .end local v5    # "i":I
    .restart local v4    # "i":I
    goto :goto_17

    .line 614
    .end local v4    # "i":I
    .end local v8    # "newArray":[I
    .end local v14    # "value":I
    :cond_21
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v4, v15

    goto :goto_16

    .line 630
    .restart local v4    # "i":I
    .restart local v8    # "newArray":[I
    :cond_22
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    .line 632
    .end local v4    # "i":I
    .end local v8    # "newArray":[I
    :cond_23
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 636
    .end local v2    # "arrayLength":I
    .end local v3    # "bytes":I
    .end local v7    # "limit":I
    .end local v9    # "startPos":I
    :sswitch_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    .line 637
    .restart local v14    # "value":I
    packed-switch v14, :pswitch_data_6

    goto/16 :goto_0

    .line 646
    :pswitch_8
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    .line 647
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    goto/16 :goto_0

    .line 653
    .end local v14    # "value":I
    :sswitch_11
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v15

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    .line 654
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    goto/16 :goto_0

    .line 311
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x2a -> :sswitch_6
        0x31 -> :sswitch_7
        0x32 -> :sswitch_8
        0x3a -> :sswitch_9
        0x40 -> :sswitch_a
        0x4a -> :sswitch_b
        0x51 -> :sswitch_c
        0x52 -> :sswitch_d
        0x58 -> :sswitch_e
        0x5a -> :sswitch_f
        0x60 -> :sswitch_10
        0x68 -> :sswitch_11
    .end sparse-switch

    .line 327
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 349
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 500
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 572
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 603
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    .line 621
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 637
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$Rule;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Rule;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 158
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasNegate:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    if-eqz v2, :cond_1

    .line 159
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->negate:Z

    invoke-virtual {p1, v6, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 161
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    if-ne v2, v6, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasOperator:Z

    if-eqz v2, :cond_3

    .line 162
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->operator:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 164
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    if-ne v2, v6, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasKey:Z

    if-eqz v2, :cond_5

    .line 165
    :cond_4
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->key:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 167
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 168
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 169
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArg:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 170
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 171
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 168
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v2, v2

    if-lez v2, :cond_8

    .line 176
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 177
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->longArg:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 180
    .end local v1    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v2, v2

    if-lez v2, :cond_9

    .line 181
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 182
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->doubleArg:[D

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 185
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 186
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 187
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->subrule:[Lcom/google/android/finsky/protos/FilterRules$Rule;

    aget-object v0, v2, v1

    .line 188
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    if-eqz v0, :cond_a

    .line 189
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 186
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 193
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Rule;
    .end local v1    # "i":I
    :cond_b
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    if-ne v2, v6, :cond_c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasResponseCode:Z

    if-eqz v2, :cond_d

    .line 194
    :cond_c
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->responseCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 196
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasComment:Z

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 197
    :cond_e
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->comment:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 199
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v2, v2

    if-lez v2, :cond_10

    .line 200
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 201
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->stringArgHash:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 204
    .end local v1    # "i":I
    :cond_10
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v2, v2

    if-lez v2, :cond_11

    .line 205
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 206
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->constArg:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 209
    .end local v1    # "i":I
    :cond_11
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    if-ne v2, v6, :cond_12

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasAvailabilityProblemType:Z

    if-eqz v2, :cond_13

    .line 210
    :cond_12
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->availabilityProblemType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 212
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->hasIncludeMissingValues:Z

    if-nez v2, :cond_14

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    if-eqz v2, :cond_15

    .line 213
    :cond_14
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Rule;->includeMissingValues:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 215
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 216
    return-void
.end method
