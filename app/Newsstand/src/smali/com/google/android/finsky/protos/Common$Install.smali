.class public final Lcom/google/android/finsky/protos/Common$Install;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Install"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Install;


# instance fields
.field public androidId:J

.field public bundled:Z

.field public hasAndroidId:Z

.field public hasBundled:Z

.field public hasPending:Z

.field public hasVersion:Z

.field public pending:Z

.field public version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4593
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4594
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Install;->clear()Lcom/google/android/finsky/protos/Common$Install;

    .line 4595
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Install;
    .locals 2

    .prologue
    .line 4566
    sget-object v0, Lcom/google/android/finsky/protos/Common$Install;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Install;

    if-nez v0, :cond_1

    .line 4567
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 4569
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Install;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Install;

    if-nez v0, :cond_0

    .line 4570
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Install;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Install;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Install;

    .line 4572
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4574
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Install;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Install;

    return-object v0

    .line 4572
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Install;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4598
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    .line 4599
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->hasAndroidId:Z

    .line 4600
    iput v2, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    .line 4601
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->hasVersion:Z

    .line 4602
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    .line 4603
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->hasBundled:Z

    .line 4604
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    .line 4605
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->hasPending:Z

    .line 4606
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Install;->cachedSize:I

    .line 4607
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 4630
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4631
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->hasAndroidId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 4632
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    .line 4633
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4635
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->hasVersion:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    if-eqz v1, :cond_3

    .line 4636
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    .line 4637
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4639
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->hasBundled:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    if-eqz v1, :cond_5

    .line 4640
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    .line 4641
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4643
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->hasPending:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    if-eqz v1, :cond_7

    .line 4644
    :cond_6
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    .line 4645
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4647
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Install;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 4655
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4656
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4660
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4661
    :sswitch_0
    return-object p0

    .line 4666
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    .line 4667
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Install;->hasAndroidId:Z

    goto :goto_0

    .line 4671
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    .line 4672
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Install;->hasVersion:Z

    goto :goto_0

    .line 4676
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    .line 4677
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Install;->hasBundled:Z

    goto :goto_0

    .line 4681
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    .line 4682
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Install;->hasPending:Z

    goto :goto_0

    .line 4656
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4560
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Install;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Install;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4613
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->hasAndroidId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 4614
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Install;->androidId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 4616
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->hasVersion:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    if-eqz v0, :cond_3

    .line 4617
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Install;->version:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4619
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->hasBundled:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    if-eqz v0, :cond_5

    .line 4620
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->bundled:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4622
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->hasPending:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    if-eqz v0, :cond_7

    .line 4623
    :cond_6
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Install;->pending:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4625
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4626
    return-void
.end method
