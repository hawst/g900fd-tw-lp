.class public final Lcom/google/android/finsky/protos/FilterRules$Availability;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Availability"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    }
.end annotation


# instance fields
.field public availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

.field public availableIfOwned:Z

.field public filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

.field public hasAvailableIfOwned:Z

.field public hasHidden:Z

.field public hasOfferType:Z

.field public hasRestriction:Z

.field public hidden:Z

.field public install:[Lcom/google/android/finsky/protos/Common$Install;

.field public offerType:I

.field public ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

.field public perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

.field public restriction:I

.field public rule:Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1457
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1458
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$Availability;->clear()Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 1459
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$Availability;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1462
    iput v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    .line 1463
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    .line 1464
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1465
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    .line 1466
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    .line 1467
    iput v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    .line 1468
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    .line 1469
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 1470
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    .line 1471
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    .line 1472
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Install;->emptyArray()[Lcom/google/android/finsky/protos/Common$Install;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    .line 1473
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 1474
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1475
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1476
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->cachedSize:I

    .line 1477
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1533
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1534
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    if-ne v3, v5, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    if-eqz v3, :cond_1

    .line 1535
    :cond_0
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    .line 1536
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 1538
    :cond_1
    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    if-ne v3, v5, :cond_2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    if-eqz v3, :cond_3

    .line 1539
    :cond_2
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    .line 1540
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 1542
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v3, :cond_4

    .line 1543
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 1544
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1546
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 1547
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 1548
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    aget-object v0, v3, v1

    .line 1549
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v0, :cond_5

    .line 1550
    const/16 v3, 0x9

    .line 1551
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1547
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1555
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .end local v1    # "i":I
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    if-nez v3, :cond_7

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    if-eqz v3, :cond_8

    .line 1556
    :cond_7
    const/16 v3, 0xd

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    .line 1557
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1559
    :cond_8
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 1560
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v3, v3

    if-ge v1, v3, :cond_a

    .line 1561
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    aget-object v0, v3, v1

    .line 1562
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v0, :cond_9

    .line 1563
    const/16 v3, 0xe

    .line 1564
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1560
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1568
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Install;
    .end local v1    # "i":I
    :cond_a
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v3, :cond_b

    .line 1569
    const/16 v3, 0x10

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1570
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1572
    :cond_b
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-eqz v3, :cond_c

    .line 1573
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 1574
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1576
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 1577
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v3, v3

    if-ge v1, v3, :cond_e

    .line 1578
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    aget-object v0, v3, v1

    .line 1579
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v0, :cond_d

    .line 1580
    const/16 v3, 0x12

    .line 1581
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1577
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1585
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    if-nez v3, :cond_f

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    if-eqz v3, :cond_10

    .line 1586
    :cond_f
    const/16 v3, 0x15

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    .line 1587
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1589
    :cond_10
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x9

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 1597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1598
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1602
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1603
    :sswitch_0
    return-object p0

    .line 1608
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1609
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1627
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    .line 1628
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    goto :goto_0

    .line 1634
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1635
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 1648
    :pswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    .line 1649
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    goto :goto_0

    .line 1655
    .end local v4    # "value":I
    :sswitch_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v6, :cond_1

    .line 1656
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 1658
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1662
    :sswitch_4
    const/16 v6, 0x4b

    .line 1663
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1664
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-nez v6, :cond_3

    move v1, v5

    .line 1665
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    .line 1667
    .local v2, "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v1, :cond_2

    .line 1668
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1670
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 1671
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;-><init>()V

    aput-object v6, v2, v1

    .line 1672
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/android/MessageNano;I)V

    .line 1673
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1670
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1664
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v1, v6

    goto :goto_1

    .line 1676
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :cond_4
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;-><init>()V

    aput-object v6, v2, v1

    .line 1677
    aget-object v6, v2, v1

    invoke-virtual {p1, v6, v8}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/android/MessageNano;I)V

    .line 1678
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    goto :goto_0

    .line 1682
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    .line 1683
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    goto/16 :goto_0

    .line 1687
    :sswitch_6
    const/16 v6, 0x72

    .line 1688
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1689
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-nez v6, :cond_6

    move v1, v5

    .line 1690
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Install;

    .line 1692
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v1, :cond_5

    .line 1693
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1695
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 1696
    new-instance v6, Lcom/google/android/finsky/protos/Common$Install;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Install;-><init>()V

    aput-object v6, v2, v1

    .line 1697
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1698
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1695
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1689
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v1, v6

    goto :goto_3

    .line 1701
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :cond_7
    new-instance v6, Lcom/google/android/finsky/protos/Common$Install;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Install;-><init>()V

    aput-object v6, v2, v1

    .line 1702
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1703
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    goto/16 :goto_0

    .line 1707
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Install;
    :sswitch_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-nez v6, :cond_8

    .line 1708
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 1710
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1714
    :sswitch_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-nez v6, :cond_9

    .line 1715
    new-instance v6, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    .line 1717
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1721
    :sswitch_9
    const/16 v6, 0x92

    .line 1722
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1723
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-nez v6, :cond_b

    move v1, v5

    .line 1724
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    .line 1726
    .local v2, "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v1, :cond_a

    .line 1727
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1729
    :cond_a
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 1730
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;-><init>()V

    aput-object v6, v2, v1

    .line 1731
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1732
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1729
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1723
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v1, v6

    goto :goto_5

    .line 1735
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :cond_c
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;-><init>()V

    aput-object v6, v2, v1

    .line 1736
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1737
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    goto/16 :goto_0

    .line 1741
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    .line 1742
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    goto/16 :goto_0

    .line 1598
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x28 -> :sswitch_1
        0x30 -> :sswitch_2
        0x3a -> :sswitch_3
        0x4b -> :sswitch_4
        0x68 -> :sswitch_5
        0x72 -> :sswitch_6
        0x82 -> :sswitch_7
        0x8a -> :sswitch_8
        0x92 -> :sswitch_9
        0xa8 -> :sswitch_a
    .end sparse-switch

    .line 1609
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1635
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1225
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$Availability;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$Availability;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1483
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    if-ne v2, v4, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasRestriction:Z

    if-eqz v2, :cond_1

    .line 1484
    :cond_0
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1486
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    if-ne v2, v4, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasOfferType:Z

    if-eqz v2, :cond_3

    .line 1487
    :cond_2
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->offerType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1489
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_4

    .line 1490
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1492
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1493
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1494
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->perDeviceAvailabilityRestriction:[Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;

    aget-object v0, v2, v1

    .line 1495
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    if-eqz v0, :cond_5

    .line 1496
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1493
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1500
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$Availability$PerDeviceAvailabilityRestriction;
    .end local v1    # "i":I
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasAvailableIfOwned:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    if-eqz v2, :cond_8

    .line 1501
    :cond_7
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1503
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 1504
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 1505
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->install:[Lcom/google/android/finsky/protos/Common$Install;

    aget-object v0, v2, v1

    .line 1506
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Install;
    if-eqz v0, :cond_9

    .line 1507
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1504
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1511
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Install;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    if-eqz v2, :cond_b

    .line 1512
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->filterInfo:Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1514
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    if-eqz v2, :cond_c

    .line 1515
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->ownershipInfo:Lcom/google/android/finsky/protos/Ownership$OwnershipInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1517
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1518
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 1519
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availabilityProblem:[Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;

    aget-object v0, v2, v1

    .line 1520
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    if-eqz v0, :cond_d

    .line 1521
    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1518
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1525
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$AvailabilityProblem;
    .end local v1    # "i":I
    :cond_e
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hasHidden:Z

    if-nez v2, :cond_f

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    if-eqz v2, :cond_10

    .line 1526
    :cond_f
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/FilterRules$Availability;->hidden:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1528
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1529
    return-void
.end method
