.class public final Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "ResolveLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResolveLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedeemGiftCard"
.end annotation


# instance fields
.field public hasPartnerPayload:Z

.field public hasPrefillCode:Z

.field public partnerPayload:Ljava/lang/String;

.field public prefillCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 473
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->clear()Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    .line 474
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 477
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 478
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPrefillCode:Z

    .line 479
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 480
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPartnerPayload:Z

    .line 481
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->cachedSize:I

    .line 482
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 499
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 500
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPrefillCode:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 501
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 502
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 504
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPartnerPayload:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 505
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 506
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 508
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 516
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 517
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 521
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 522
    :sswitch_0
    return-object p0

    .line 527
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 528
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPrefillCode:Z

    goto :goto_0

    .line 532
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 533
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPartnerPayload:Z

    goto :goto_0

    .line 517
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 447
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPrefillCode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 489
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 491
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->hasPartnerPayload:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 492
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 494
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 495
    return-void
.end method
