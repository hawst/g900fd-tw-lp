.class public final Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionMetadata"
.end annotation


# instance fields
.field public browseUrl:Ljava/lang/String;

.field public descriptionHtml:Ljava/lang/String;

.field public hasBrowseUrl:Z

.field public hasDescriptionHtml:Z

.field public hasHeader:Z

.field public hasListUrl:Z

.field public header:Ljava/lang/String;

.field public listUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->clear()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    .line 514
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 517
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 518
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasHeader:Z

    .line 519
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    .line 520
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasDescriptionHtml:Z

    .line 521
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 522
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasListUrl:Z

    .line 523
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    .line 524
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasBrowseUrl:Z

    .line 525
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->cachedSize:I

    .line 526
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 549
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 550
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasHeader:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 551
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 552
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 554
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasListUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 555
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 556
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 558
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasBrowseUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 559
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    .line 560
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 562
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasDescriptionHtml:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 563
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    .line 564
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 566
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 574
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 575
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 579
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 580
    :sswitch_0
    return-object p0

    .line 585
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 586
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasHeader:Z

    goto :goto_0

    .line 590
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 591
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasListUrl:Z

    goto :goto_0

    .line 595
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    .line 596
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasBrowseUrl:Z

    goto :goto_0

    .line 600
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    .line 601
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasDescriptionHtml:Z

    goto :goto_0

    .line 575
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 479
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasHeader:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 533
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 535
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasListUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 536
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 538
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasBrowseUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 539
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 541
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->hasDescriptionHtml:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 542
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 544
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 545
    return-void
.end method
