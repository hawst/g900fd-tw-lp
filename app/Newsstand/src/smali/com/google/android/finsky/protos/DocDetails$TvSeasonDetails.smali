.class public final Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvSeasonDetails"
.end annotation


# instance fields
.field public broadcaster:Ljava/lang/String;

.field public episodeCount:I

.field public expectedEpisodeCount:I

.field public hasBroadcaster:Z

.field public hasEpisodeCount:Z

.field public hasExpectedEpisodeCount:Z

.field public hasParentDetailsUrl:Z

.field public hasReleaseDate:Z

.field public hasSeasonIndex:Z

.field public parentDetailsUrl:Ljava/lang/String;

.field public releaseDate:Ljava/lang/String;

.field public seasonIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4260
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4261
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    .line 4262
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4265
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4266
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    .line 4267
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    .line 4268
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    .line 4269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    .line 4270
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    .line 4271
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    .line 4272
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    .line 4273
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    .line 4274
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    .line 4275
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    .line 4276
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    .line 4277
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    .line 4278
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4307
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4308
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4309
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4310
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4312
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    if-eqz v1, :cond_3

    .line 4313
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    .line 4314
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4316
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4317
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    .line 4318
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4320
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 4321
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    .line 4322
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4324
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    if-eqz v1, :cond_9

    .line 4325
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    .line 4326
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4328
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    if-eqz v1, :cond_b

    .line 4329
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    .line 4330
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4332
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4340
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4341
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4345
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4346
    :sswitch_0
    return-object p0

    .line 4351
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4352
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    goto :goto_0

    .line 4356
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    .line 4357
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    goto :goto_0

    .line 4361
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    .line 4362
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    goto :goto_0

    .line 4366
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    .line 4367
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    goto :goto_0

    .line 4371
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    .line 4372
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    goto :goto_0

    .line 4376
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    .line 4377
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    goto :goto_0

    .line 4341
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4219
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4284
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4285
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4287
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    if-eqz v0, :cond_3

    .line 4288
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4290
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 4291
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4293
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4294
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4296
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    if-eqz v0, :cond_9

    .line 4297
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4299
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    if-eqz v0, :cond_b

    .line 4300
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4302
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4303
    return-void
.end method
