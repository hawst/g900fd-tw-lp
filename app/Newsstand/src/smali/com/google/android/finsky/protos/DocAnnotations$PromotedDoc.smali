.class public final Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PromotedDoc"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;


# instance fields
.field public descriptionHtml:Ljava/lang/String;

.field public detailsUrl:Ljava/lang/String;

.field public hasDescriptionHtml:Z

.field public hasDetailsUrl:Z

.field public hasSubtitle:Z

.field public hasTitle:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public subtitle:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 657
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->clear()Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 658
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .locals 2

    .prologue
    .line 626
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-nez v0, :cond_1

    .line 627
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 629
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    if-nez v0, :cond_0

    .line 630
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    sput-object v0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    .line 632
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    return-object v0

    .line 632
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 661
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    .line 662
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasTitle:Z

    .line 663
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    .line 664
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasSubtitle:Z

    .line 665
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 666
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    .line 667
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDescriptionHtml:Z

    .line 668
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    .line 669
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDetailsUrl:Z

    .line 670
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->cachedSize:I

    .line 671
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 702
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 703
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 704
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    .line 705
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 707
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasSubtitle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 708
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    .line 709
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 711
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 712
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 713
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 714
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_4

    .line 715
    const/4 v3, 0x3

    .line 716
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 712
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 720
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDescriptionHtml:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 721
    :cond_6
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    .line 722
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 724
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDetailsUrl:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 725
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    .line 726
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 728
    :cond_9
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 736
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 737
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 741
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 742
    :sswitch_0
    return-object p0

    .line 747
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    .line 748
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasTitle:Z

    goto :goto_0

    .line 752
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    .line 753
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasSubtitle:Z

    goto :goto_0

    .line 757
    :sswitch_3
    const/16 v5, 0x1a

    .line 758
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 759
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_2

    move v1, v4

    .line 760
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Image;

    .line 762
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_1

    .line 763
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 765
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 766
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 767
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 768
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 765
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 759
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v5

    goto :goto_1

    .line 771
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 772
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 773
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto :goto_0

    .line 777
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    .line 778
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDescriptionHtml:Z

    goto :goto_0

    .line 782
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    .line 783
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDetailsUrl:Z

    goto :goto_0

    .line 737
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 620
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 677
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 678
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 680
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasSubtitle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 681
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 683
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 684
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 685
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 686
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_4

    .line 687
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 684
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 691
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDescriptionHtml:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 692
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 694
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->hasDetailsUrl:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 695
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 697
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 698
    return-void
.end method
