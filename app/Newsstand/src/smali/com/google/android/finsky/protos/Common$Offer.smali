.class public final Lcom/google/android/finsky/protos/Common$Offer;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Offer"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;


# instance fields
.field public checkoutFlowRequired:Z

.field public convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

.field public currencyCode:Ljava/lang/String;

.field public formattedAmount:Ljava/lang/String;

.field public formattedDescription:Ljava/lang/String;

.field public formattedFullAmount:Ljava/lang/String;

.field public formattedName:Ljava/lang/String;

.field public fullPriceMicros:J

.field public hasCheckoutFlowRequired:Z

.field public hasCurrencyCode:Z

.field public hasFormattedAmount:Z

.field public hasFormattedDescription:Z

.field public hasFormattedFullAmount:Z

.field public hasFormattedName:Z

.field public hasFullPriceMicros:Z

.field public hasLicensedOfferType:Z

.field public hasMicros:Z

.field public hasOfferId:Z

.field public hasOfferType:Z

.field public hasOnSaleDate:Z

.field public hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

.field public hasPreorder:Z

.field public hasPreorderFulfillmentDisplayDate:Z

.field public licensedOfferType:I

.field public micros:J

.field public offerId:Ljava/lang/String;

.field public offerType:I

.field public onSaleDate:J

.field public onSaleDateDisplayTimeZoneOffsetMsec:I

.field public preorder:Z

.field public preorderFulfillmentDisplayDate:J

.field public promotionLabel:[Ljava/lang/String;

.field public rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

.field public subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

.field public subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 627
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 628
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Offer;->clear()Lcom/google/android/finsky/protos/Common$Offer;

    .line 629
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;
    .locals 2

    .prologue
    .line 541
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v0, :cond_1

    .line 542
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 544
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v0, :cond_0

    .line 545
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Offer;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 547
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Offer;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Offer;

    return-object v0

    .line 547
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Offer;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 632
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 633
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    .line 634
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    .line 635
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    .line 636
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 637
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    .line 638
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    .line 639
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    .line 640
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    .line 641
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    .line 642
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 643
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    .line 644
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 645
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    .line 646
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Offer;->emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 647
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 648
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    .line 649
    iput v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 650
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    .line 651
    iput v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    .line 652
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    .line 653
    iput-object v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 654
    iput-object v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 655
    iput-object v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 656
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    .line 657
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    .line 658
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    .line 659
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    .line 660
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 661
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    .line 662
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    .line 663
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    .line 664
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    .line 665
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    .line 666
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    .line 667
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Offer;->cachedSize:I

    .line 668
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    const-wide/16 v8, 0x0

    .line 749
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 750
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    if-nez v5, :cond_0

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    .line 751
    :cond_0
    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 752
    invoke-static {v10, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 754
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 755
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    .line 756
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 758
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 759
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 760
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 762
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 763
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 764
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v2, v5, v3

    .line 765
    .local v2, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_6

    .line 766
    const/4 v5, 0x4

    .line 767
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 763
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 771
    .end local v2    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v3    # "i":I
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    if-nez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v5, :cond_9

    .line 772
    :cond_8
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 773
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 775
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-nez v5, :cond_a

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_b

    .line 776
    :cond_a
    const/4 v5, 0x6

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 777
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 779
    :cond_b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 780
    :cond_c
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 781
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 783
    :cond_d
    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-ne v5, v10, :cond_e

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    if-eqz v5, :cond_f

    .line 784
    :cond_e
    const/16 v5, 0x8

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 785
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 787
    :cond_f
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v5, :cond_10

    .line 788
    const/16 v5, 0x9

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 789
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 791
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    if-nez v5, :cond_11

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_12

    .line 792
    :cond_11
    const/16 v5, 0xa

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 793
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 795
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 796
    const/4 v0, 0x0

    .line 797
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 798
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 799
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 800
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 801
    add-int/lit8 v0, v0, 0x1

    .line 803
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 798
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 806
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 807
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 809
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-eqz v5, :cond_16

    .line 810
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 811
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 813
    :cond_16
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    if-nez v5, :cond_17

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 814
    :cond_17
    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    .line 815
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 817
    :cond_18
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    if-nez v5, :cond_19

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 818
    :cond_19
    const/16 v5, 0xe

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    .line 819
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 821
    :cond_1a
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    if-nez v5, :cond_1b

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    if-eqz v5, :cond_1c

    .line 822
    :cond_1b
    const/16 v5, 0xf

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    .line 823
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 825
    :cond_1c
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    if-nez v5, :cond_1d

    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    if-eqz v5, :cond_1e

    .line 826
    :cond_1d
    const/16 v5, 0x10

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    .line 827
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 829
    :cond_1e
    iget v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    if-ne v5, v10, :cond_1f

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    if-eqz v5, :cond_20

    .line 830
    :cond_1f
    const/16 v5, 0x11

    iget v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    .line 831
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 833
    :cond_20
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-eqz v5, :cond_21

    .line 834
    const/16 v5, 0x12

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 835
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 837
    :cond_21
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    if-nez v5, :cond_22

    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_23

    .line 838
    :cond_22
    const/16 v5, 0x13

    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    .line 839
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 841
    :cond_23
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    if-nez v5, :cond_24

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_25

    .line 842
    :cond_24
    const/16 v5, 0x14

    iget-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    .line 843
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 845
    :cond_25
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 853
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 854
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 858
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 859
    :sswitch_0
    return-object p0

    .line 864
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    .line 865
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    goto :goto_0

    .line 869
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    .line 870
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    goto :goto_0

    .line 874
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 875
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    goto :goto_0

    .line 879
    :sswitch_4
    const/16 v6, 0x22

    .line 880
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 881
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v6, :cond_2

    move v1, v5

    .line 882
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 884
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v1, :cond_1

    .line 885
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 887
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 888
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 889
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 890
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 887
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 881
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v1, v6

    goto :goto_1

    .line 893
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 894
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 895
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    goto :goto_0

    .line 899
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 900
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    goto :goto_0

    .line 904
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    .line 905
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    goto :goto_0

    .line 909
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 910
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    goto/16 :goto_0

    .line 914
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 915
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 928
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 929
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    goto/16 :goto_0

    .line 935
    .end local v4    # "value":I
    :sswitch_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-nez v6, :cond_4

    .line 936
    new-instance v6, Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$RentalTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 938
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 942
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 943
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    goto/16 :goto_0

    .line 947
    :sswitch_b
    const/16 v6, 0x5a

    .line 948
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 949
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-nez v6, :cond_6

    move v1, v5

    .line 950
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 951
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 952
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 954
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 955
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 956
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 954
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 949
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_3

    .line 959
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 960
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    goto/16 :goto_0

    .line 964
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-nez v6, :cond_8

    .line 965
    new-instance v6, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 967
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 971
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    .line 972
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    goto/16 :goto_0

    .line 976
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    .line 977
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    goto/16 :goto_0

    .line 981
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    .line 982
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    goto/16 :goto_0

    .line 986
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    .line 987
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    goto/16 :goto_0

    .line 991
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 992
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto/16 :goto_0

    .line 1005
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    .line 1006
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    goto/16 :goto_0

    .line 1012
    .end local v4    # "value":I
    :sswitch_12
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-nez v6, :cond_9

    .line 1013
    new-instance v6, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 1015
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1019
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    .line 1020
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    goto/16 :goto_0

    .line 1024
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    .line 1025
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    goto/16 :goto_0

    .line 854
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
    .end sparse-switch

    .line 915
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 992
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 535
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Offer;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 9
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 674
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasMicros:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 675
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    invoke-virtual {p1, v8, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 677
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCurrencyCode:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 678
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 680
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 681
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 683
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 684
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 685
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v2, v1

    .line 686
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_6

    .line 687
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 684
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 691
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasCheckoutFlowRequired:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    if-eqz v2, :cond_9

    .line 692
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 694
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFullPriceMicros:Z

    if-nez v2, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_b

    .line 695
    :cond_a
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->fullPriceMicros:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 697
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedFullAmount:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 698
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 700
    :cond_d
    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-ne v2, v8, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferType:Z

    if-eqz v2, :cond_f

    .line 701
    :cond_e
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 703
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    if-eqz v2, :cond_10

    .line 704
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->rentalTerms:Lcom/google/android/finsky/protos/Common$RentalTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 706
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    if-nez v2, :cond_11

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_12

    .line 707
    :cond_11
    const/16 v2, 0xa

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 709
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 710
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 711
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->promotionLabel:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 712
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_13

    .line 713
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 710
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 717
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-eqz v2, :cond_15

    .line 718
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 720
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedName:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 721
    :cond_16
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 723
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedDescription:Z

    if-nez v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 724
    :cond_18
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 726
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorder:Z

    if-nez v2, :cond_1a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    if-eqz v2, :cond_1b

    .line 727
    :cond_1a
    const/16 v2, 0xf

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorder:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 729
    :cond_1b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDateDisplayTimeZoneOffsetMsec:Z

    if-nez v2, :cond_1c

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    if-eqz v2, :cond_1d

    .line 730
    :cond_1c
    const/16 v2, 0x10

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDateDisplayTimeZoneOffsetMsec:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 732
    :cond_1d
    iget v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    if-ne v2, v8, :cond_1e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasLicensedOfferType:Z

    if-eqz v2, :cond_1f

    .line 733
    :cond_1e
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->licensedOfferType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 735
    :cond_1f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-eqz v2, :cond_20

    .line 736
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 738
    :cond_20
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOfferId:Z

    if-nez v2, :cond_21

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 739
    :cond_21
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 741
    :cond_22
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    if-nez v2, :cond_23

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_24

    .line 742
    :cond_23
    const/16 v2, 0x14

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 744
    :cond_24
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 745
    return-void
.end method
