.class public final Lcom/google/android/finsky/protos/DocumentV2$DocV2;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocV2"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# instance fields
.field public aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

.field public annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

.field public availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

.field public backendDocid:Ljava/lang/String;

.field public backendId:I

.field public backendUrl:Ljava/lang/String;

.field public child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

.field public creator:Ljava/lang/String;

.field public descriptionHtml:Ljava/lang/String;

.field public details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

.field public detailsReusable:Z

.field public detailsUrl:Ljava/lang/String;

.field public docType:I

.field public docid:Ljava/lang/String;

.field public hasBackendDocid:Z

.field public hasBackendId:Z

.field public hasBackendUrl:Z

.field public hasCreator:Z

.field public hasDescriptionHtml:Z

.field public hasDetailsReusable:Z

.field public hasDetailsUrl:Z

.field public hasDocType:Z

.field public hasDocid:Z

.field public hasMature:Z

.field public hasPromotionalDescription:Z

.field public hasPurchaseDetailsUrl:Z

.field public hasReviewsUrl:Z

.field public hasServerLogsCookie:Z

.field public hasShareUrl:Z

.field public hasSubtitle:Z

.field public hasTitle:Z

.field public hasTranslatedDescriptionHtml:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public mature:Z

.field public offer:[Lcom/google/android/finsky/protos/Common$Offer;

.field public productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

.field public promotionalDescription:Ljava/lang/String;

.field public purchaseDetailsUrl:Ljava/lang/String;

.field public reviewsUrl:Ljava/lang/String;

.field public serverLogsCookie:[B

.field public shareUrl:Ljava/lang/String;

.field public subtitle:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public translatedDescriptionHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->clear()Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 126
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 129
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    .line 130
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocid:Z

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    .line 132
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendDocid:Z

    .line 133
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    .line 134
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocType:Z

    .line 135
    iput v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    .line 136
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendId:Z

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 138
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTitle:Z

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    .line 140
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasSubtitle:Z

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    .line 142
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasCreator:Z

    .line 143
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    .line 144
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDescriptionHtml:Z

    .line 145
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    .line 146
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTranslatedDescriptionHtml:Z

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    .line 148
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPromotionalDescription:Z

    .line 149
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Offer;->emptyArray()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 150
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 151
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 152
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 153
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    .line 154
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 155
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    .line 156
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 157
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    .line 159
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsUrl:Z

    .line 160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    .line 161
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasShareUrl:Z

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    .line 163
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasReviewsUrl:Z

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    .line 165
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendUrl:Z

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    .line 167
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPurchaseDetailsUrl:Z

    .line 168
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    .line 169
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsReusable:Z

    .line 170
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    .line 171
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasServerLogsCookie:Z

    .line 172
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    .line 173
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasMature:Z

    .line 174
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->cachedSize:I

    .line 175
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 282
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 283
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocid:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 284
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    .line 285
    invoke-static {v5, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 287
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendDocid:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 288
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    .line 289
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 291
    :cond_3
    iget v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    if-ne v3, v5, :cond_4

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocType:Z

    if-eqz v3, :cond_5

    .line 292
    :cond_4
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    .line 293
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 295
    :cond_5
    iget v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendId:Z

    if-eqz v3, :cond_7

    .line 296
    :cond_6
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    .line 297
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 299
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTitle:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 300
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 301
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 303
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasCreator:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 304
    :cond_a
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    .line 305
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 307
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDescriptionHtml:Z

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 308
    :cond_c
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    .line 309
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 311
    :cond_d
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-lez v3, :cond_f

    .line 312
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v3, v3

    if-ge v1, v3, :cond_f

    .line 313
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v3, v1

    .line 314
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_e

    .line 315
    const/16 v3, 0x8

    .line 316
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 312
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 320
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_f
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v3, :cond_10

    .line 321
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 322
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 324
    :cond_10
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_12

    .line 325
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_12

    .line 326
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 327
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_11

    .line 328
    const/16 v3, 0xa

    .line 329
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 325
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 333
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_14

    .line 334
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_14

    .line 335
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 336
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 337
    const/16 v3, 0xb

    .line 338
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 334
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 342
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-eqz v3, :cond_15

    .line 343
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    .line 344
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 346
    :cond_15
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-eqz v3, :cond_16

    .line 347
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 348
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 350
    :cond_16
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-eqz v3, :cond_17

    .line 351
    const/16 v3, 0xe

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 352
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 354
    :cond_17
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v3, :cond_18

    .line 355
    const/16 v3, 0xf

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 356
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 358
    :cond_18
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsUrl:Z

    if-nez v3, :cond_19

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 359
    :cond_19
    const/16 v3, 0x10

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    .line 360
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 362
    :cond_1a
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasShareUrl:Z

    if-nez v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 363
    :cond_1b
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    .line 364
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 366
    :cond_1c
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasReviewsUrl:Z

    if-nez v3, :cond_1d

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 367
    :cond_1d
    const/16 v3, 0x12

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    .line 368
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 370
    :cond_1e
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendUrl:Z

    if-nez v3, :cond_1f

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_20

    .line 371
    :cond_1f
    const/16 v3, 0x13

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    .line 372
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 374
    :cond_20
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPurchaseDetailsUrl:Z

    if-nez v3, :cond_21

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 375
    :cond_21
    const/16 v3, 0x14

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    .line 376
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 378
    :cond_22
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsReusable:Z

    if-nez v3, :cond_23

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    if-eqz v3, :cond_24

    .line 379
    :cond_23
    const/16 v3, 0x15

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    .line 380
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 382
    :cond_24
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasSubtitle:Z

    if-nez v3, :cond_25

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_26

    .line 383
    :cond_25
    const/16 v3, 0x16

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    .line 384
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 386
    :cond_26
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTranslatedDescriptionHtml:Z

    if-nez v3, :cond_27

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    .line 387
    :cond_27
    const/16 v3, 0x17

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    .line 388
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 390
    :cond_28
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasServerLogsCookie:Z

    if-nez v3, :cond_29

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_2a

    .line 391
    :cond_29
    const/16 v3, 0x18

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    .line 392
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 394
    :cond_2a
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    if-eqz v3, :cond_2b

    .line 395
    const/16 v3, 0x19

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    .line 396
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 398
    :cond_2b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasMature:Z

    if-nez v3, :cond_2c

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    if-eqz v3, :cond_2d

    .line 399
    :cond_2c
    const/16 v3, 0x1a

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    .line 400
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 402
    :cond_2d
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPromotionalDescription:Z

    if-nez v3, :cond_2e

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2f

    .line 403
    :cond_2e
    const/16 v3, 0x1b

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    .line 404
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 406
    :cond_2f
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 414
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 415
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 419
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 420
    :sswitch_0
    return-object p0

    .line 425
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    .line 426
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocid:Z

    goto :goto_0

    .line 430
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    .line 431
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendDocid:Z

    goto :goto_0

    .line 435
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 436
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 469
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    .line 470
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocType:Z

    goto :goto_0

    .line 476
    .end local v4    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 477
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 489
    :pswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    .line 490
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendId:Z

    goto :goto_0

    .line 496
    .end local v4    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 497
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTitle:Z

    goto :goto_0

    .line 501
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    .line 502
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasCreator:Z

    goto :goto_0

    .line 506
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    .line 507
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDescriptionHtml:Z

    goto :goto_0

    .line 511
    :sswitch_8
    const/16 v6, 0x42

    .line 512
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 513
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v6, :cond_2

    move v1, v5

    .line 514
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Offer;

    .line 516
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v1, :cond_1

    .line 517
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 519
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 520
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 521
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 522
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 513
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v1, v6

    goto :goto_1

    .line 525
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    aput-object v6, v2, v1

    .line 526
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 527
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    goto/16 :goto_0

    .line 531
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Offer;
    :sswitch_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-nez v6, :cond_4

    .line 532
    new-instance v6, Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/FilterRules$Availability;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    .line 534
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 538
    :sswitch_a
    const/16 v6, 0x52

    .line 539
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 540
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v6, :cond_6

    move v1, v5

    .line 541
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Common$Image;

    .line 543
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_5

    .line 544
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 546
    :cond_5
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_7

    .line 547
    new-instance v6, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v6, v2, v1

    .line 548
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 549
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 546
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 540
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v6

    goto :goto_3

    .line 552
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_7
    new-instance v6, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v6, v2, v1

    .line 553
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 554
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto/16 :goto_0

    .line 558
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_b
    const/16 v6, 0x5a

    .line 559
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 560
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v6, :cond_9

    move v1, v5

    .line 561
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 563
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_8

    .line 564
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 566
    :cond_8
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 567
    new-instance v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v6, v2, v1

    .line 568
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 569
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 566
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 560
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v6

    goto :goto_5

    .line 572
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_a
    new-instance v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v6, v2, v1

    .line 573
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 574
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 578
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-nez v6, :cond_b

    .line 579
    new-instance v6, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    .line 581
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 585
    :sswitch_d
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-nez v6, :cond_c

    .line 586
    new-instance v6, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 588
    :cond_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 592
    :sswitch_e
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-nez v6, :cond_d

    .line 593
    new-instance v6, Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Rating$AggregateRating;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 595
    :cond_d
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 599
    :sswitch_f
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-nez v6, :cond_e

    .line 600
    new-instance v6, Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocumentV2$Annotations;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 602
    :cond_e
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 606
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    .line 607
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsUrl:Z

    goto/16 :goto_0

    .line 611
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    .line 612
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasShareUrl:Z

    goto/16 :goto_0

    .line 616
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    .line 617
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasReviewsUrl:Z

    goto/16 :goto_0

    .line 621
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    .line 622
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendUrl:Z

    goto/16 :goto_0

    .line 626
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    .line 627
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPurchaseDetailsUrl:Z

    goto/16 :goto_0

    .line 631
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    .line 632
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsReusable:Z

    goto/16 :goto_0

    .line 636
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    .line 637
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasSubtitle:Z

    goto/16 :goto_0

    .line 641
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    .line 642
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTranslatedDescriptionHtml:Z

    goto/16 :goto_0

    .line 646
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    .line 647
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 651
    :sswitch_19
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    if-nez v6, :cond_f

    .line 652
    new-instance v6, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    .line 654
    :cond_f
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 658
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    .line 659
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasMature:Z

    goto/16 :goto_0

    .line 663
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    .line 664
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPromotionalDescription:Z

    goto/16 :goto_0

    .line 415
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd0 -> :sswitch_1a
        0xda -> :sswitch_1b
    .end sparse-switch

    .line 436
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 477
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 181
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocid:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 182
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 184
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendDocid:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 185
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 187
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    if-ne v2, v4, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocType:Z

    if-eqz v2, :cond_5

    .line 188
    :cond_4
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 190
    :cond_5
    iget v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendId:Z

    if-eqz v2, :cond_7

    .line 191
    :cond_6
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 193
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTitle:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 194
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 196
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasCreator:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 197
    :cond_a
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 199
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDescriptionHtml:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 200
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 202
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 203
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 204
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    aget-object v0, v2, v1

    .line 205
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_e

    .line 206
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 203
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v2, :cond_10

    .line 211
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 213
    :cond_10
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_12

    .line 214
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_12

    .line 215
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 216
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_11

    .line 217
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 214
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 221
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_14

    .line 222
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_14

    .line 223
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 224
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_13

    .line 225
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 222
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 229
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-eqz v2, :cond_15

    .line 230
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 232
    :cond_15
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-eqz v2, :cond_16

    .line 233
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 235
    :cond_16
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-eqz v2, :cond_17

    .line 236
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 238
    :cond_17
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v2, :cond_18

    .line 239
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 241
    :cond_18
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsUrl:Z

    if-nez v2, :cond_19

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 242
    :cond_19
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 244
    :cond_1a
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasShareUrl:Z

    if-nez v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    .line 245
    :cond_1b
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 247
    :cond_1c
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasReviewsUrl:Z

    if-nez v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 248
    :cond_1d
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 250
    :cond_1e
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasBackendUrl:Z

    if-nez v2, :cond_1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 251
    :cond_1f
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 253
    :cond_20
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPurchaseDetailsUrl:Z

    if-nez v2, :cond_21

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 254
    :cond_21
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->purchaseDetailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 256
    :cond_22
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDetailsReusable:Z

    if-nez v2, :cond_23

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    if-eqz v2, :cond_24

    .line 257
    :cond_23
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 259
    :cond_24
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasSubtitle:Z

    if-nez v2, :cond_25

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 260
    :cond_25
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 262
    :cond_26
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasTranslatedDescriptionHtml:Z

    if-nez v2, :cond_27

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    .line 263
    :cond_27
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 265
    :cond_28
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasServerLogsCookie:Z

    if-nez v2, :cond_29

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 266
    :cond_29
    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 268
    :cond_2a
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    if-eqz v2, :cond_2b

    .line 269
    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 271
    :cond_2b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasMature:Z

    if-nez v2, :cond_2c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    if-eqz v2, :cond_2d

    .line 272
    :cond_2c
    const/16 v2, 0x1a

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 274
    :cond_2d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasPromotionalDescription:Z

    if-nez v2, :cond_2e

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 275
    :cond_2e
    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 277
    :cond_2f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 278
    return-void
.end method
