.class public final Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ArtistExternalLinks"
.end annotation


# instance fields
.field public googlePlusProfileUrl:Ljava/lang/String;

.field public hasGooglePlusProfileUrl:Z

.field public hasYoutubeChannelUrl:Z

.field public websiteUrl:[Ljava/lang/String;

.field public youtubeChannelUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2545
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2546
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->clear()Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2547
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2550
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    .line 2551
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    .line 2552
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasGooglePlusProfileUrl:Z

    .line 2553
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    .line 2554
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasYoutubeChannelUrl:Z

    .line 2555
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->cachedSize:I

    .line 2556
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 2581
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 2582
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 2583
    const/4 v0, 0x0

    .line 2584
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 2585
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 2586
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 2587
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 2588
    add-int/lit8 v0, v0, 0x1

    .line 2590
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 2585
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2593
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 2594
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 2596
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasGooglePlusProfileUrl:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2597
    :cond_3
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    .line 2598
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2600
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasYoutubeChannelUrl:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 2601
    :cond_5
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    .line 2602
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2604
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 2612
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2613
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2617
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2618
    :sswitch_0
    return-object p0

    .line 2623
    :sswitch_1
    const/16 v5, 0xa

    .line 2624
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2625
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 2626
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 2627
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 2628
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2630
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2631
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 2632
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 2630
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2625
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 2635
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 2636
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    goto :goto_0

    .line 2640
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    .line 2641
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasGooglePlusProfileUrl:Z

    goto :goto_0

    .line 2645
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    .line 2646
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasYoutubeChannelUrl:Z

    goto :goto_0

    .line 2613
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2517
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2562
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2563
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 2564
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->websiteUrl:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2565
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2566
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2563
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2570
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasGooglePlusProfileUrl:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2571
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->googlePlusProfileUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2573
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->hasYoutubeChannelUrl:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2574
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;->youtubeChannelUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2576
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2577
    return-void
.end method
