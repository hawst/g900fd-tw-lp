.class public final Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecommendationsContainer"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4254
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4255
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;->clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 4256
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;
    .locals 1

    .prologue
    .line 4259
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;->cachedSize:I

    .line 4260
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4268
    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4269
    .local v0, "tag":I
    packed-switch v0, :pswitch_data_0

    .line 4273
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4274
    :pswitch_0
    return-object p0

    .line 4269
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4237
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    move-result-object v0

    return-object v0
.end method
