.class public final Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "PlayPlusProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PlayPlusProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayPlusProfileResponse"
.end annotation


# instance fields
.field public hasIsGplusUser:Z

.field public isGplusUser:Z

.field public userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->clear()Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    .line 34
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    .line 38
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    .line 39
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->hasIsGplusUser:Z

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->cachedSize:I

    .line 41
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 59
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    if-eqz v1, :cond_0

    .line 60
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    .line 61
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->hasIsGplusUser:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    if-eqz v1, :cond_2

    .line 64
    :cond_1
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    .line 65
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 76
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 80
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    :sswitch_0
    return-object p0

    .line 86
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Lcom/google/android/finsky/protos/Document$PlayDocument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Document$PlayDocument;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    .line 94
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->hasIsGplusUser:Z

    goto :goto_0

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->userProfile:Lcom/google/android/finsky/protos/Document$PlayDocument;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 50
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->hasIsGplusUser:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    if-eqz v0, :cond_2

    .line 51
    :cond_1
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PlayPlusProfile$PlayPlusProfileResponse;->isGplusUser:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 53
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 54
    return-void
.end method
