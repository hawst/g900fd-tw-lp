.class public final Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionBanner"
.end annotation


# instance fields
.field public action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

.field public primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3592
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3593
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->clear()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3594
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .locals 1

    .prologue
    .line 3597
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3598
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3599
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3600
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->cachedSize:I

    .line 3601
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 3631
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3632
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3633
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 3634
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v3, v1

    .line 3635
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3636
    const/4 v3, 0x1

    .line 3637
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3633
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3641
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_2

    .line 3642
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3643
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3645
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 3646
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 3647
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 3648
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_3

    .line 3649
    const/4 v3, 0x4

    .line 3650
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3646
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3654
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_4
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3662
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3663
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3667
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3668
    :sswitch_0
    return-object p0

    .line 3673
    :sswitch_1
    const/16 v5, 0xa

    .line 3674
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3675
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-nez v5, :cond_2

    move v1, v4

    .line 3676
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 3678
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v1, :cond_1

    .line 3679
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3681
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3682
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3683
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3684
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3681
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3675
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v1, v5

    goto :goto_1

    .line 3687
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;-><init>()V

    aput-object v5, v2, v1

    .line 3688
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3689
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    goto :goto_0

    .line 3693
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_4

    .line 3694
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3696
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3700
    :sswitch_3
    const/16 v5, 0x22

    .line 3701
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3702
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_6

    move v1, v4

    .line 3703
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 3705
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_5

    .line 3706
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3708
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 3709
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 3710
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3711
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3708
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3702
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 3714
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_7
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 3715
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3716
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 3663
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3566
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3607
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3608
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 3609
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    aget-object v0, v2, v1

    .line 3610
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    if-eqz v0, :cond_0

    .line 3611
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3608
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3615
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_2

    .line 3616
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3618
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3619
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 3620
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 3621
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_3

    .line 3622
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 3619
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3626
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3627
    return-void
.end method
