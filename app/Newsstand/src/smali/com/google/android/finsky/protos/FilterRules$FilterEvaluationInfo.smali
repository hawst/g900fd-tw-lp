.class public final Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FilterEvaluationInfo"
.end annotation


# instance fields
.field public ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 983
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 984
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->clear()Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    .line 985
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;
    .locals 1

    .prologue
    .line 988
    invoke-static {}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->emptyArray()[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 989
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->cachedSize:I

    .line 990
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 1009
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1010
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1011
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 1012
    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    aget-object v0, v3, v1

    .line 1013
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    if-eqz v0, :cond_0

    .line 1014
    const/4 v3, 0x1

    .line 1015
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1011
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1019
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1027
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1028
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1032
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1033
    :sswitch_0
    return-object p0

    .line 1038
    :sswitch_1
    const/16 v5, 0xa

    .line 1039
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1040
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v5, :cond_2

    move v1, v4

    .line 1041
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 1043
    .local v2, "newArray":[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    if-eqz v1, :cond_1

    .line 1044
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1046
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1047
    new-instance v5, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;-><init>()V

    aput-object v5, v2, v1

    .line 1048
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1049
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1046
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1040
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    array-length v1, v5

    goto :goto_1

    .line 1052
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;-><init>()V

    aput-object v5, v2, v1

    .line 1053
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1054
    iput-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    goto :goto_0

    .line 1028
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 963
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 996
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 997
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 998
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$FilterEvaluationInfo;->ruleEvaluation:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    aget-object v0, v2, v1

    .line 999
    .local v0, "element":Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    if-eqz v0, :cond_0

    .line 1000
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 997
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1004
    .end local v0    # "element":Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1005
    return-void
.end method
