.class public final Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReasonPlusOne"
.end annotation


# instance fields
.field public hasLocalizedDescriptionHtml:Z

.field public localizedDescriptionHtml:Ljava/lang/String;

.field public oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

.field public person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2110
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2111
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    .line 2112
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    .locals 1

    .prologue
    .line 2115
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    .line 2117
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2118
    invoke-static {}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;->emptyArray()[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2119
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->cachedSize:I

    .line 2120
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2150
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2151
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2152
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2153
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2155
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 2156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 2157
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v3, v1

    .line 2158
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_2

    .line 2159
    const/4 v3, 0x2

    .line 2160
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2156
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2164
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 2165
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 2166
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 2167
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_4

    .line 2168
    const/4 v3, 0x3

    .line 2169
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2165
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2173
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2182
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2186
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2187
    :sswitch_0
    return-object p0

    .line 2192
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2193
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    goto :goto_0

    .line 2197
    :sswitch_2
    const/16 v5, 0x12

    .line 2198
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2199
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v5, :cond_2

    move v1, v4

    .line 2200
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2202
    .local v2, "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v1, :cond_1

    .line 2203
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2205
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2206
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 2207
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2208
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 2205
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2199
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v1, v5

    goto :goto_1

    .line 2211
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 2212
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2213
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    goto :goto_0

    .line 2217
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :sswitch_3
    const/16 v5, 0x1a

    .line 2218
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2219
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 2220
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2222
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 2223
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2225
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 2226
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 2227
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2228
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 2225
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2219
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 2231
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 2232
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2233
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 2182
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2083
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2126
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->hasLocalizedDescriptionHtml:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2127
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->localizedDescriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2129
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 2130
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 2131
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->oBSOLETEPlusProfile:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v2, v1

    .line 2132
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_2

    .line 2133
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2130
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2137
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 2138
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 2139
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;->person:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 2140
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_4

    .line 2141
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2138
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2145
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2146
    return-void
.end method
