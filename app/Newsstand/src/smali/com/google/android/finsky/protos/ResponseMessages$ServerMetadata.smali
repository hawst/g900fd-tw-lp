.class public final Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "ResponseMessages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResponseMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerMetadata"
.end annotation


# instance fields
.field public hasLatencyMillis:Z

.field public latencyMillis:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->clear()Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 313
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;
    .locals 2

    .prologue
    .line 316
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->hasLatencyMillis:Z

    .line 318
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->cachedSize:I

    .line 319
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 333
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 334
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->hasLatencyMillis:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 335
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    .line 336
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 347
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 351
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 352
    :sswitch_0
    return-object p0

    .line 357
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    .line 358
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->hasLatencyMillis:Z

    goto :goto_0

    .line 347
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->hasLatencyMillis:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 326
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 328
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 329
    return-void
.end method
