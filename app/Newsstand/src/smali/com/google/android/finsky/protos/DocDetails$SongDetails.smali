.class public final Lcom/google/android/finsky/protos/DocDetails$SongDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SongDetails"
.end annotation


# instance fields
.field public albumName:Ljava/lang/String;

.field public badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

.field public details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

.field public displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

.field public hasAlbumName:Z

.field public hasName:Z

.field public hasPreviewUrl:Z

.field public hasTrackNumber:Z

.field public name:Ljava/lang/String;

.field public previewUrl:Ljava/lang/String;

.field public trackNumber:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2241
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2242
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    .line 2243
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2246
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    .line 2247
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasName:Z

    .line 2248
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 2249
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    .line 2250
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasAlbumName:Z

    .line 2251
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    .line 2252
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasTrackNumber:Z

    .line 2253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    .line 2254
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasPreviewUrl:Z

    .line 2255
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2256
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 2257
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->cachedSize:I

    .line 2258
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2290
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2291
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2292
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    .line 2293
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2295
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-eqz v1, :cond_2

    .line 2296
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 2297
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2299
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasAlbumName:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2300
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    .line 2301
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2303
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasTrackNumber:Z

    if-nez v1, :cond_5

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    if-eqz v1, :cond_6

    .line 2304
    :cond_5
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    .line 2305
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2307
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasPreviewUrl:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2308
    :cond_7
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    .line 2309
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2311
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v1, :cond_9

    .line 2312
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2313
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2315
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v1, :cond_a

    .line 2316
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 2317
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2319
    :cond_a
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2328
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2332
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2333
    :sswitch_0
    return-object p0

    .line 2338
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    .line 2339
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasName:Z

    goto :goto_0

    .line 2343
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-nez v1, :cond_1

    .line 2344
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 2346
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2350
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    .line 2351
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasAlbumName:Z

    goto :goto_0

    .line 2355
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    .line 2356
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasTrackNumber:Z

    goto :goto_0

    .line 2360
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    .line 2361
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasPreviewUrl:Z

    goto :goto_0

    .line 2365
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v1, :cond_2

    .line 2366
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2368
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2372
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-nez v1, :cond_3

    .line 2373
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 2375
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2328
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2199
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2264
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2265
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2267
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-eqz v0, :cond_2

    .line 2268
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2270
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasAlbumName:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2271
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->albumName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2273
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasTrackNumber:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    if-eqz v0, :cond_6

    .line 2274
    :cond_5
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2276
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasPreviewUrl:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2277
    :cond_7
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2279
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v0, :cond_9

    .line 2280
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2282
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v0, :cond_a

    .line 2283
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2285
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2286
    return-void
.end method
