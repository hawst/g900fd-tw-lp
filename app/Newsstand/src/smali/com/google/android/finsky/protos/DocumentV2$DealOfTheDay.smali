.class public final Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DealOfTheDay"
.end annotation


# instance fields
.field public colorThemeArgb:Ljava/lang/String;

.field public featuredHeader:Ljava/lang/String;

.field public hasColorThemeArgb:Z

.field public hasFeaturedHeader:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3957
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3958
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->clear()Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3959
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3962
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    .line 3963
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasFeaturedHeader:Z

    .line 3964
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    .line 3965
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasColorThemeArgb:Z

    .line 3966
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->cachedSize:I

    .line 3967
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3984
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3985
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasFeaturedHeader:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3986
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    .line 3987
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3989
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasColorThemeArgb:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3990
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    .line 3991
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3993
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4001
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4002
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4006
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4007
    :sswitch_0
    return-object p0

    .line 4012
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    .line 4013
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasFeaturedHeader:Z

    goto :goto_0

    .line 4017
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    .line 4018
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasColorThemeArgb:Z

    goto :goto_0

    .line 4002
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3932
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3973
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasFeaturedHeader:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3974
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3976
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->hasColorThemeArgb:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3977
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3979
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3980
    return-void
.end method
