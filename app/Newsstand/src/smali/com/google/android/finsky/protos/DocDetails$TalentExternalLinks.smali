.class public final Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TalentExternalLinks"
.end annotation


# instance fields
.field public googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4746
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4747
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->clear()Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    .line 4748
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4751
    invoke-static {}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4752
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4753
    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4754
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->cachedSize:I

    .line 4755
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4780
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4781
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 4782
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 4783
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    aget-object v0, v3, v1

    .line 4784
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    if-eqz v0, :cond_0

    .line 4785
    const/4 v3, 0x1

    .line 4786
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4782
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4790
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_2

    .line 4791
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4792
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4794
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_3

    .line 4795
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4796
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4798
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4806
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4807
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4811
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4812
    :sswitch_0
    return-object p0

    .line 4817
    :sswitch_1
    const/16 v5, 0xa

    .line 4818
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4819
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_2

    move v1, v4

    .line 4820
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4822
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    if-eqz v1, :cond_1

    .line 4823
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4825
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4826
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    aput-object v5, v2, v1

    .line 4827
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4828
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 4825
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4819
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    array-length v1, v5

    goto :goto_1

    .line 4831
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    aput-object v5, v2, v1

    .line 4832
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 4833
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    goto :goto_0

    .line 4837
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_4

    .line 4838
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4840
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 4844
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_5

    .line 4845
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 4847
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 4807
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4720
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4761
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 4762
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 4763
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->websiteUrl:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    aget-object v0, v2, v1

    .line 4764
    .local v0, "element":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    if-eqz v0, :cond_0

    .line 4765
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4762
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4769
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_2

    .line 4770
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->googlePlusProfileUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4772
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_3

    .line 4773
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$TalentExternalLinks;->youtubeChannelUrl:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 4775
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4776
    return-void
.end method
