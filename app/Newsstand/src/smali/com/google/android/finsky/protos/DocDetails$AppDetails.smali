.class public final Lcom/google/android/finsky/protos/DocDetails$AppDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppDetails"
.end annotation


# instance fields
.field public appCategory:[Ljava/lang/String;

.field public appType:Ljava/lang/String;

.field public autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

.field public certificateHash:[Ljava/lang/String;

.field public certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

.field public contentRating:I

.field public declaresIab:Z

.field public developerEmail:Ljava/lang/String;

.field public developerName:Ljava/lang/String;

.field public developerWebsite:Ljava/lang/String;

.field public file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

.field public hasAppType:Z

.field public hasContentRating:Z

.field public hasDeclaresIab:Z

.field public hasDeveloperEmail:Z

.field public hasDeveloperName:Z

.field public hasDeveloperWebsite:Z

.field public hasInstallationSize:Z

.field public hasMajorVersionNumber:Z

.field public hasNumDownloads:Z

.field public hasPackageName:Z

.field public hasRecentChangesHtml:Z

.field public hasTitle:Z

.field public hasUploadDate:Z

.field public hasVariesByAccount:Z

.field public hasVersionCode:Z

.field public hasVersionString:Z

.field public installationSize:J

.field public majorVersionNumber:I

.field public numDownloads:Ljava/lang/String;

.field public oBSOLETEPermission:[Ljava/lang/String;

.field public packageName:Ljava/lang/String;

.field public permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

.field public recentChangesHtml:Ljava/lang/String;

.field public splitId:[Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public uploadDate:Ljava/lang/String;

.field public variesByAccount:Z

.field public versionCode:I

.field public versionString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 931
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .line 932
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 935
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    .line 936
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperName:Z

    .line 937
    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    .line 938
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasMajorVersionNumber:Z

    .line 939
    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    .line 940
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    .line 941
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    .line 942
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString:Z

    .line 943
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    .line 944
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasTitle:Z

    .line 945
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    .line 946
    iput v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    .line 947
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasContentRating:Z

    .line 948
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    .line 949
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    .line 950
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    .line 951
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 952
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    .line 953
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperEmail:Z

    .line 954
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    .line 955
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperWebsite:Z

    .line 956
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    .line 957
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasNumDownloads:Z

    .line 958
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 959
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasPackageName:Z

    .line 960
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    .line 961
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasRecentChangesHtml:Z

    .line 962
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    .line 963
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasUploadDate:Z

    .line 964
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    .line 965
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    .line 966
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasAppType:Z

    .line 967
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .line 968
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    .line 969
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    .line 970
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVariesByAccount:Z

    .line 971
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    .line 972
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    .line 973
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeclaresIab:Z

    .line 974
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    .line 975
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->cachedSize:I

    .line 976
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1099
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1100
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperName:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1101
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    .line 1102
    invoke-static {v10, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1104
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasMajorVersionNumber:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    if-eqz v5, :cond_3

    .line 1105
    :cond_2
    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    .line 1106
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1108
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    if-nez v5, :cond_4

    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    if-eqz v5, :cond_5

    .line 1109
    :cond_4
    const/4 v5, 0x3

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    .line 1110
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1112
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1113
    :cond_6
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    .line 1114
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1116
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasTitle:Z

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1117
    :cond_8
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    .line 1118
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1120
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_c

    .line 1121
    const/4 v0, 0x0

    .line 1122
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1123
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_b

    .line 1124
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1125
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_a

    .line 1126
    add-int/lit8 v0, v0, 0x1

    .line 1128
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1123
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1131
    .end local v2    # "element":Ljava/lang/String;
    :cond_b
    add-int/2addr v4, v1

    .line 1132
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1134
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_c
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasContentRating:Z

    if-nez v5, :cond_d

    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    if-eqz v5, :cond_e

    .line 1135
    :cond_d
    const/16 v5, 0x8

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    .line 1136
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1138
    :cond_e
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    if-nez v5, :cond_f

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_10

    .line 1139
    :cond_f
    const/16 v5, 0x9

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    .line 1140
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1142
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    if-eqz v5, :cond_13

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_13

    .line 1143
    const/4 v0, 0x0

    .line 1144
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 1145
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_12

    .line 1146
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1147
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_11

    .line 1148
    add-int/lit8 v0, v0, 0x1

    .line 1150
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1145
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1153
    .end local v2    # "element":Ljava/lang/String;
    :cond_12
    add-int/2addr v4, v1

    .line 1154
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1156
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_13
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperEmail:Z

    if-nez v5, :cond_14

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 1157
    :cond_14
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    .line 1158
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1160
    :cond_15
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperWebsite:Z

    if-nez v5, :cond_16

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 1161
    :cond_16
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    .line 1162
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1164
    :cond_17
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasNumDownloads:Z

    if-nez v5, :cond_18

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 1165
    :cond_18
    const/16 v5, 0xd

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    .line 1166
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1168
    :cond_19
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasPackageName:Z

    if-nez v5, :cond_1a

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 1169
    :cond_1a
    const/16 v5, 0xe

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 1170
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1172
    :cond_1b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasRecentChangesHtml:Z

    if-nez v5, :cond_1c

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1d

    .line 1173
    :cond_1c
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    .line 1174
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1176
    :cond_1d
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasUploadDate:Z

    if-nez v5, :cond_1e

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1f

    .line 1177
    :cond_1e
    const/16 v5, 0x10

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    .line 1178
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1180
    :cond_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    if-eqz v5, :cond_21

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v5, v5

    if-lez v5, :cond_21

    .line 1181
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v5, v5

    if-ge v3, v5, :cond_21

    .line 1182
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    aget-object v2, v5, v3

    .line 1183
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    if-eqz v2, :cond_20

    .line 1184
    const/16 v5, 0x11

    .line 1185
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1181
    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1189
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .end local v3    # "i":I
    :cond_21
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasAppType:Z

    if-nez v5, :cond_22

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_23

    .line 1190
    :cond_22
    const/16 v5, 0x12

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    .line 1191
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1193
    :cond_23
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    if-eqz v5, :cond_26

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_26

    .line 1194
    const/4 v0, 0x0

    .line 1195
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 1196
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_25

    .line 1197
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1198
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_24

    .line 1199
    add-int/lit8 v0, v0, 0x1

    .line 1201
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1196
    :cond_24
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1204
    .end local v2    # "element":Ljava/lang/String;
    :cond_25
    add-int/2addr v4, v1

    .line 1205
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 1207
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_26
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-eqz v5, :cond_28

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    array-length v5, v5

    if-lez v5, :cond_28

    .line 1208
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    array-length v5, v5

    if-ge v3, v5, :cond_28

    .line 1209
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    aget-object v2, v5, v3

    .line 1210
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    if-eqz v2, :cond_27

    .line 1211
    const/16 v5, 0x14

    .line 1212
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1208
    :cond_27
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1216
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .end local v3    # "i":I
    :cond_28
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVariesByAccount:Z

    if-nez v5, :cond_29

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    if-eq v5, v10, :cond_2a

    .line 1217
    :cond_29
    const/16 v5, 0x15

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    .line 1218
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1220
    :cond_2a
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    if-eqz v5, :cond_2c

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    array-length v5, v5

    if-lez v5, :cond_2c

    .line 1221
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    array-length v5, v5

    if-ge v3, v5, :cond_2c

    .line 1222
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    aget-object v2, v5, v3

    .line 1223
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    if-eqz v2, :cond_2b

    .line 1224
    const/16 v5, 0x16

    .line 1225
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1221
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1229
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    .end local v3    # "i":I
    :cond_2c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-eqz v5, :cond_2f

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2f

    .line 1230
    const/4 v0, 0x0

    .line 1231
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 1232
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_2e

    .line 1233
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1234
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_2d

    .line 1235
    add-int/lit8 v0, v0, 0x1

    .line 1237
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1232
    :cond_2d
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1240
    .end local v2    # "element":Ljava/lang/String;
    :cond_2e
    add-int/2addr v4, v1

    .line 1241
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 1243
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2f
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeclaresIab:Z

    if-nez v5, :cond_30

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    if-eqz v5, :cond_31

    .line 1244
    :cond_30
    const/16 v5, 0x18

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    .line 1245
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 1247
    :cond_31
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    if-eqz v5, :cond_34

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_34

    .line 1248
    const/4 v0, 0x0

    .line 1249
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 1250
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_33

    .line 1251
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1252
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_32

    .line 1253
    add-int/lit8 v0, v0, 0x1

    .line 1255
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1250
    :cond_32
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1258
    .end local v2    # "element":Ljava/lang/String;
    :cond_33
    add-int/2addr v4, v1

    .line 1259
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 1261
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_34
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1269
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1270
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1274
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1275
    :sswitch_0
    return-object p0

    .line 1280
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    .line 1281
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperName:Z

    goto :goto_0

    .line 1285
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    .line 1286
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasMajorVersionNumber:Z

    goto :goto_0

    .line 1290
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    .line 1291
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    goto :goto_0

    .line 1295
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    .line 1296
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString:Z

    goto :goto_0

    .line 1300
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    .line 1301
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasTitle:Z

    goto :goto_0

    .line 1305
    :sswitch_6
    const/16 v5, 0x3a

    .line 1306
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1307
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 1308
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1309
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1310
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1312
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1313
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1314
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1312
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1307
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 1317
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1318
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    goto :goto_0

    .line 1322
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    .line 1323
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasContentRating:Z

    goto :goto_0

    .line 1327
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    .line 1328
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    goto :goto_0

    .line 1332
    :sswitch_9
    const/16 v5, 0x52

    .line 1333
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1334
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 1335
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1336
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 1337
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1339
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1340
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1341
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1339
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1334
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 1344
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1345
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1349
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    .line 1350
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperEmail:Z

    goto/16 :goto_0

    .line 1354
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    .line 1355
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperWebsite:Z

    goto/16 :goto_0

    .line 1359
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    .line 1360
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasNumDownloads:Z

    goto/16 :goto_0

    .line 1364
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 1365
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasPackageName:Z

    goto/16 :goto_0

    .line 1369
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    .line 1370
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasRecentChangesHtml:Z

    goto/16 :goto_0

    .line 1374
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    .line 1375
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasUploadDate:Z

    goto/16 :goto_0

    .line 1379
    :sswitch_10
    const/16 v5, 0x8a

    .line 1380
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1381
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    if-nez v5, :cond_8

    move v1, v4

    .line 1382
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    .line 1384
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    if-eqz v1, :cond_7

    .line 1385
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1387
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 1388
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;-><init>()V

    aput-object v5, v2, v1

    .line 1389
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1390
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1387
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1381
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v1, v5

    goto :goto_5

    .line 1393
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    :cond_9
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$FileMetadata;-><init>()V

    aput-object v5, v2, v1

    .line 1394
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1395
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    goto/16 :goto_0

    .line 1399
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    .line 1400
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasAppType:Z

    goto/16 :goto_0

    .line 1404
    :sswitch_12
    const/16 v5, 0x9a

    .line 1405
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1406
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    if-nez v5, :cond_b

    move v1, v4

    .line 1407
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1408
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_a

    .line 1409
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1411
    :cond_a
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 1412
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1413
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1411
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1406
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_7

    .line 1416
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1417
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1421
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_13
    const/16 v5, 0xa2

    .line 1422
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1423
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-nez v5, :cond_e

    move v1, v4

    .line 1424
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 1426
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    if-eqz v1, :cond_d

    .line 1427
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1429
    :cond_d
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 1430
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;-><init>()V

    aput-object v5, v2, v1

    .line 1431
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1432
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1429
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1423
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    array-length v1, v5

    goto :goto_9

    .line 1435
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    :cond_f
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;-><init>()V

    aput-object v5, v2, v1

    .line 1436
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1437
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    goto/16 :goto_0

    .line 1441
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    .line 1442
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVariesByAccount:Z

    goto/16 :goto_0

    .line 1446
    :sswitch_15
    const/16 v5, 0xb2

    .line 1447
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1448
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    if-nez v5, :cond_11

    move v1, v4

    .line 1449
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .line 1451
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    if-eqz v1, :cond_10

    .line 1452
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1454
    :cond_10
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_12

    .line 1455
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;-><init>()V

    aput-object v5, v2, v1

    .line 1456
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1457
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1454
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 1448
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    array-length v1, v5

    goto :goto_b

    .line 1460
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    :cond_12
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;-><init>()V

    aput-object v5, v2, v1

    .line 1461
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1462
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    goto/16 :goto_0

    .line 1466
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    :sswitch_16
    const/16 v5, 0xba

    .line 1467
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1468
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-nez v5, :cond_14

    move v1, v4

    .line 1469
    .restart local v1    # "i":I
    :goto_d
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1470
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_13

    .line 1471
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1473
    :cond_13
    :goto_e
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_15

    .line 1474
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1475
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1473
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1468
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_14
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_d

    .line 1478
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1479
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1483
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    .line 1484
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeclaresIab:Z

    goto/16 :goto_0

    .line 1488
    :sswitch_18
    const/16 v5, 0xca

    .line 1489
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1490
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    if-nez v5, :cond_17

    move v1, v4

    .line 1491
    .restart local v1    # "i":I
    :goto_f
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 1492
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_16

    .line 1493
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1495
    :cond_16
    :goto_10
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_18

    .line 1496
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1497
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1495
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 1490
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_17
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_f

    .line 1500
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 1501
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1270
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb2 -> :sswitch_15
        0xba -> :sswitch_16
        0xc0 -> :sswitch_17
        0xca -> :sswitch_18
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 825
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 982
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperName:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 983
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerName:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 985
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasMajorVersionNumber:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    if-eqz v2, :cond_3

    .line 986
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->majorVersionNumber:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 988
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    if-nez v2, :cond_4

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    if-eqz v2, :cond_5

    .line 989
    :cond_4
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 991
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 992
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 994
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasTitle:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 995
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 997
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 998
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 999
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appCategory:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1000
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 1001
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 998
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1005
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasContentRating:Z

    if-nez v2, :cond_c

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    if-eqz v2, :cond_d

    .line 1006
    :cond_c
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1008
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    if-nez v2, :cond_e

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 1009
    :cond_e
    const/16 v2, 0x9

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1011
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 1012
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 1013
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1014
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_10

    .line 1015
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1012
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1019
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperEmail:Z

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 1020
    :cond_12
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerEmail:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1022
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeveloperWebsite:Z

    if-nez v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 1023
    :cond_14
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->developerWebsite:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1025
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasNumDownloads:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 1026
    :cond_16
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1028
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasPackageName:Z

    if-nez v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 1029
    :cond_18
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1031
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasRecentChangesHtml:Z

    if-nez v2, :cond_1a

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1032
    :cond_1a
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1034
    :cond_1b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasUploadDate:Z

    if-nez v2, :cond_1c

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1035
    :cond_1c
    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1037
    :cond_1d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v2, v2

    if-lez v2, :cond_1f

    .line 1038
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    array-length v2, v2

    if-ge v1, v2, :cond_1f

    .line 1039
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->file:[Lcom/google/android/finsky/protos/DocDetails$FileMetadata;

    aget-object v0, v2, v1

    .line 1040
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    if-eqz v0, :cond_1e

    .line 1041
    const/16 v2, 0x11

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1038
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1045
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$FileMetadata;
    .end local v1    # "i":I
    :cond_1f
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasAppType:Z

    if-nez v2, :cond_20

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 1046
    :cond_20
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->appType:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1048
    :cond_21
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_23

    .line 1049
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_23

    .line 1050
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateHash:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1051
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_22

    .line 1052
    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1049
    :cond_22
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1056
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_23
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    array-length v2, v2

    if-lez v2, :cond_25

    .line 1057
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    array-length v2, v2

    if-ge v1, v2, :cond_25

    .line 1058
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->permission:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    aget-object v0, v2, v1

    .line 1059
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    if-eqz v0, :cond_24

    .line 1060
    const/16 v2, 0x14

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1057
    :cond_24
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1064
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .end local v1    # "i":I
    :cond_25
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVariesByAccount:Z

    if-nez v2, :cond_26

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    if-eq v2, v6, :cond_27

    .line 1065
    :cond_26
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->variesByAccount:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1067
    :cond_27
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    array-length v2, v2

    if-lez v2, :cond_29

    .line 1068
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    array-length v2, v2

    if-ge v1, v2, :cond_29

    .line 1069
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    aget-object v0, v2, v1

    .line 1070
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    if-eqz v0, :cond_28

    .line 1071
    const/16 v2, 0x16

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1068
    :cond_28
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1075
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    .end local v1    # "i":I
    :cond_29
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2b

    .line 1076
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2b

    .line 1077
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1078
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_2a

    .line 1079
    const/16 v2, 0x17

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1076
    :cond_2a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1083
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasDeclaresIab:Z

    if-nez v2, :cond_2c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    if-eqz v2, :cond_2d

    .line 1084
    :cond_2c
    const/16 v2, 0x18

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1086
    :cond_2d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2f

    .line 1087
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2f

    .line 1088
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->splitId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1089
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_2e

    .line 1090
    const/16 v2, 0x19

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1087
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1094
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1095
    return-void
.end method
