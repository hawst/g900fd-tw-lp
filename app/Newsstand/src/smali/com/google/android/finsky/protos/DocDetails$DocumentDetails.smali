.class public final Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocumentDetails"
.end annotation


# instance fields
.field public albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

.field public appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

.field public artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

.field public bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

.field public magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

.field public personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

.field public songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

.field public subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

.field public talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

.field public tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

.field public tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

.field public tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

.field public videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 66
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .line 70
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    .line 71
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 72
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    .line 73
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    .line 74
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    .line 75
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    .line 76
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    .line 77
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    .line 78
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    .line 79
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    .line 80
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    .line 81
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->cachedSize:I

    .line 83
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 134
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    if-eqz v1, :cond_0

    .line 135
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .line 136
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    if-eqz v1, :cond_1

    .line 139
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    .line 140
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v1, :cond_2

    .line 143
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 144
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    if-eqz v1, :cond_3

    .line 147
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    .line 148
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    if-eqz v1, :cond_4

    .line 151
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    .line 152
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    if-eqz v1, :cond_5

    .line 155
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    .line 156
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    if-eqz v1, :cond_6

    .line 159
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    .line 160
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 162
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    if-eqz v1, :cond_7

    .line 163
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    .line 164
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    if-eqz v1, :cond_8

    .line 167
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    .line 168
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    if-eqz v1, :cond_9

    .line 171
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    .line 172
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    if-eqz v1, :cond_a

    .line 175
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    .line 176
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    if-eqz v1, :cond_b

    .line 179
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    .line 180
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    if-eqz v1, :cond_c

    .line 183
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    .line 184
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 195
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 199
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    :sswitch_0
    return-object p0

    .line 205
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    if-nez v1, :cond_1

    .line 206
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 212
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    if-nez v1, :cond_2

    .line 213
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 219
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v1, :cond_3

    .line 220
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 222
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 226
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    if-nez v1, :cond_4

    .line 227
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$SongDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    .line 229
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 233
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    if-nez v1, :cond_5

    .line 234
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$BookDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    .line 236
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 240
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    if-nez v1, :cond_6

    .line 241
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    .line 243
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 247
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    if-nez v1, :cond_7

    .line 248
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    .line 250
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 254
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    if-nez v1, :cond_8

    .line 255
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    .line 257
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 261
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    if-nez v1, :cond_9

    .line 262
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    .line 264
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 268
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    if-nez v1, :cond_a

    .line 269
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    .line 271
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 275
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    if-nez v1, :cond_b

    .line 276
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    .line 278
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 282
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    if-nez v1, :cond_c

    .line 283
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    .line 285
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 289
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    if-nez v1, :cond_d

    .line 290
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$TalentDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    .line 292
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    if-eqz v0, :cond_1

    .line 93
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v0, :cond_2

    .line 96
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    if-eqz v0, :cond_3

    .line 99
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    if-eqz v0, :cond_4

    .line 102
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 104
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    if-eqz v0, :cond_5

    .line 105
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 107
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    if-eqz v0, :cond_6

    .line 108
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->subscriptionDetails:Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 110
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    if-eqz v0, :cond_7

    .line 111
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 113
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    if-eqz v0, :cond_8

    .line 114
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvShowDetails:Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 116
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    if-eqz v0, :cond_9

    .line 117
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 119
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    if-eqz v0, :cond_a

    .line 120
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 122
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    if-eqz v0, :cond_b

    .line 123
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 125
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    if-eqz v0, :cond_c

    .line 126
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 128
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 129
    return-void
.end method
