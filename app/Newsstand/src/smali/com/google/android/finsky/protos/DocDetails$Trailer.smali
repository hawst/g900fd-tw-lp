.class public final Lcom/google/android/finsky/protos/DocDetails$Trailer;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Trailer"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$Trailer;


# instance fields
.field public duration:Ljava/lang/String;

.field public hasDuration:Z

.field public hasThumbnailUrl:Z

.field public hasTitle:Z

.field public hasTrailerId:Z

.field public hasWatchUrl:Z

.field public thumbnailUrl:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public trailerId:Ljava/lang/String;

.field public watchUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3434
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3435
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$Trailer;->clear()Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 3436
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .locals 2

    .prologue
    .line 3403
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-nez v0, :cond_1

    .line 3404
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3406
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-nez v0, :cond_0

    .line 3407
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$Trailer;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 3409
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3411
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    return-object v0

    .line 3409
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3439
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    .line 3440
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTrailerId:Z

    .line 3441
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    .line 3442
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTitle:Z

    .line 3443
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    .line 3444
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasThumbnailUrl:Z

    .line 3445
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    .line 3446
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasWatchUrl:Z

    .line 3447
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    .line 3448
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasDuration:Z

    .line 3449
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->cachedSize:I

    .line 3450
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3476
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3477
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTrailerId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3478
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    .line 3479
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3481
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3482
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    .line 3483
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3485
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasThumbnailUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3486
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    .line 3487
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3489
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasWatchUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3490
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    .line 3491
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3493
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasDuration:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3494
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    .line 3495
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3497
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3505
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3506
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3510
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3511
    :sswitch_0
    return-object p0

    .line 3516
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    .line 3517
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTrailerId:Z

    goto :goto_0

    .line 3521
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    .line 3522
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTitle:Z

    goto :goto_0

    .line 3526
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    .line 3527
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasThumbnailUrl:Z

    goto :goto_0

    .line 3531
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    .line 3532
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasWatchUrl:Z

    goto :goto_0

    .line 3536
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    .line 3537
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasDuration:Z

    goto :goto_0

    .line 3506
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3397
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$Trailer;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$Trailer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3456
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTrailerId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3457
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->trailerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3459
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasTitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3460
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3462
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasThumbnailUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3463
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3465
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasWatchUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3466
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->watchUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3468
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->hasDuration:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 3469
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$Trailer;->duration:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3471
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3472
    return-void
.end method
