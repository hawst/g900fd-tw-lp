.class public final Lcom/google/android/finsky/protos/Common$RentalTerms;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RentalTerms"
.end annotation


# instance fields
.field public activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public dEPRECATEDActivatePeriodSeconds:I

.field public dEPRECATEDGrantPeriodSeconds:I

.field public grantEndTimeSeconds:J

.field public grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public hasDEPRECATEDActivatePeriodSeconds:Z

.field public hasDEPRECATEDGrantPeriodSeconds:Z

.field public hasGrantEndTimeSeconds:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2033
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2034
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$RentalTerms;->clear()Lcom/google/android/finsky/protos/Common$RentalTerms;

    .line 2035
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$RentalTerms;
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2038
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2039
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2040
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    .line 2041
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    .line 2042
    iput v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    .line 2043
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    .line 2044
    iput v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    .line 2045
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    .line 2046
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->cachedSize:I

    .line 2047
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 2073
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2074
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    if-eqz v1, :cond_1

    .line 2075
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    .line 2076
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2078
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    if-eqz v1, :cond_3

    .line 2079
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    .line 2080
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2082
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v1, :cond_4

    .line 2083
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2084
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2086
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v1, :cond_5

    .line 2087
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2088
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2090
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 2091
    :cond_6
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    .line 2092
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2094
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RentalTerms;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2103
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2107
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2108
    :sswitch_0
    return-object p0

    .line 2113
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    .line 2114
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    goto :goto_0

    .line 2118
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    .line 2119
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    goto :goto_0

    .line 2123
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v1, :cond_1

    .line 2124
    new-instance v1, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2126
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2130
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v1, :cond_2

    .line 2131
    new-instance v1, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2133
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2137
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    .line 2138
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    goto :goto_0

    .line 2103
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1998
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$RentalTerms;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RentalTerms;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2053
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDGrantPeriodSeconds:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    if-eqz v0, :cond_1

    .line 2054
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDGrantPeriodSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2056
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasDEPRECATEDActivatePeriodSeconds:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    if-eqz v0, :cond_3

    .line 2057
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->dEPRECATEDActivatePeriodSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2059
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v0, :cond_4

    .line 2060
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2062
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v0, :cond_5

    .line 2063
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->activatePeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2065
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->hasGrantEndTimeSeconds:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    .line 2066
    :cond_6
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RentalTerms;->grantEndTimeSeconds:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2068
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2069
    return-void
.end method
