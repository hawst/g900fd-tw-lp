.class public final Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubscriptionContentTerms"
.end annotation


# instance fields
.field public requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2696
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2697
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->clear()Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    .line 2698
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;
    .locals 1

    .prologue
    .line 2701
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    .line 2702
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->cachedSize:I

    .line 2703
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2717
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2718
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 2719
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    .line 2720
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2722
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2730
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2731
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2735
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2736
    :sswitch_0
    return-object p0

    .line 2741
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 2742
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    .line 2744
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2731
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2676
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2709
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 2710
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2712
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2713
    return-void
.end method
