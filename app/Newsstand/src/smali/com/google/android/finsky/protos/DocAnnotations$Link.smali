.class public final Lcom/google/android/finsky/protos/DocAnnotations$Link;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Link"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Link;


# instance fields
.field public hasUri:Z

.field public hasUriBackend:Z

.field public resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field public uri:Ljava/lang/String;

.field public uriBackend:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->clear()Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 38
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocAnnotations$Link;

    sput-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->_emptyArray:[Lcom/google/android/finsky/protos/DocAnnotations$Link;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    .line 42
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    .line 43
    iput v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    .line 44
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUriBackend:Z

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->cachedSize:I

    .line 47
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 68
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    .line 70
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v1, :cond_2

    .line 73
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 74
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUriBackend:Z

    if-eqz v1, :cond_4

    .line 77
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    .line 78
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 89
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 93
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    :sswitch_0
    return-object p0

    .line 99
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    .line 100
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    goto :goto_0

    .line 104
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez v2, :cond_1

    .line 105
    new-instance v2, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 111
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 112
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 124
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    .line 125
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUriBackend:Z

    goto :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    .line 112
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v0, :cond_2

    .line 57
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 59
    :cond_2
    iget v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUriBackend:Z

    if-eqz v0, :cond_4

    .line 60
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 62
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 63
    return-void
.end method
