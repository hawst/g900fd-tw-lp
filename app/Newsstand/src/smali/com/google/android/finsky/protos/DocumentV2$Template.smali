.class public final Lcom/google/android/finsky/protos/DocumentV2$Template;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation


# instance fields
.field public actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

.field public addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

.field public containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

.field public dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

.field public editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

.field public emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

.field public myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

.field public nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

.field public rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

.field public rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

.field public recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

.field public recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

.field public seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

.field public singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

.field public tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

.field public trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

.field public warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2869
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2870
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Template;->clear()Lcom/google/android/finsky/protos/DocumentV2$Template;

    .line 2871
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2874
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 2875
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2876
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2877
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2878
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2879
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2880
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2881
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2882
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 2883
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 2884
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 2885
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 2886
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 2887
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 2888
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 2889
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 2890
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 2891
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 2892
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 2893
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 2894
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 2895
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 2896
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 2897
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->cachedSize:I

    .line 2898
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2978
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2979
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v1, :cond_0

    .line 2980
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 2981
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2983
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_1

    .line 2984
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2985
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2987
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_2

    .line 2988
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2989
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2991
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_3

    .line 2992
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2993
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2995
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_4

    .line 2996
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 2997
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2999
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_5

    .line 3000
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3001
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3003
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_6

    .line 3004
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3005
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3007
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v1, :cond_7

    .line 3008
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 3009
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3011
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v1, :cond_8

    .line 3012
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3013
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3015
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v1, :cond_9

    .line 3016
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3017
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3019
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v1, :cond_a

    .line 3020
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 3021
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3023
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v1, :cond_b

    .line 3024
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 3025
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3027
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v1, :cond_c

    .line 3028
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 3029
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3031
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v1, :cond_d

    .line 3032
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 3033
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3035
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v1, :cond_e

    .line 3036
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 3037
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3039
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v1, :cond_f

    .line 3040
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 3041
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3043
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v1, :cond_10

    .line 3044
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 3045
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3047
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v1, :cond_11

    .line 3048
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3049
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3051
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v1, :cond_12

    .line 3052
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3053
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3055
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v1, :cond_13

    .line 3056
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 3057
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3059
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v1, :cond_14

    .line 3060
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 3061
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3063
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v1, :cond_15

    .line 3064
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 3065
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3067
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v1, :cond_16

    .line 3068
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 3069
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3071
    :cond_16
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3079
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3080
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3084
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3085
    :sswitch_0
    return-object p0

    .line 3090
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-nez v1, :cond_1

    .line 3091
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    .line 3093
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3097
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_2

    .line 3098
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3100
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3104
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_3

    .line 3105
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3107
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3111
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_4

    .line 3112
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3114
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3118
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_5

    .line 3119
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3121
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3125
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_6

    .line 3126
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3128
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 3132
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_7

    .line 3133
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3135
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3139
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-nez v1, :cond_8

    .line 3140
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    .line 3142
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3146
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-nez v1, :cond_9

    .line 3147
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    .line 3149
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3153
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-nez v1, :cond_a

    .line 3154
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3156
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3160
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-nez v1, :cond_b

    .line 3161
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    .line 3163
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3167
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-nez v1, :cond_c

    .line 3168
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    .line 3170
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3174
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-nez v1, :cond_d

    .line 3175
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    .line 3177
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3181
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-nez v1, :cond_e

    .line 3182
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    .line 3184
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3188
    :sswitch_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-nez v1, :cond_f

    .line 3189
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    .line 3191
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3195
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-nez v1, :cond_10

    .line 3196
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    .line 3198
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3202
    :sswitch_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-nez v1, :cond_11

    .line 3203
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    .line 3205
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3209
    :sswitch_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-nez v1, :cond_12

    .line 3210
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    .line 3212
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3216
    :sswitch_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-nez v1, :cond_13

    .line 3217
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    .line 3219
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3223
    :sswitch_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-nez v1, :cond_14

    .line 3224
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 3226
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3230
    :sswitch_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-nez v1, :cond_15

    .line 3231
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    .line 3233
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3237
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-nez v1, :cond_16

    .line 3238
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    .line 3240
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3244
    :sswitch_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-nez v1, :cond_17

    .line 3245
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    .line 3247
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 3080
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2783
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Template;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2904
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v0, :cond_0

    .line 2905
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2907
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_1

    .line 2908
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2910
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_2

    .line 2911
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphic4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2913
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_3

    .line 2914
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2916
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_4

    .line 2917
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicUpperLeftTitle2X1:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2919
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_5

    .line 2920
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileDetailsReflectedGraphic2X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2922
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_6

    .line 2923
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileFourBlock4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2925
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v0, :cond_7

    .line 2926
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2928
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v0, :cond_8

    .line 2929
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2931
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    if-eqz v0, :cond_9

    .line 2932
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->tileGraphicColoredTitle4X2:Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2934
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v0, :cond_a

    .line 2935
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2937
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v0, :cond_b

    .line 2938
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2940
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v0, :cond_c

    .line 2941
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2943
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v0, :cond_d

    .line 2944
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2946
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v0, :cond_e

    .line 2947
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2949
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v0, :cond_f

    .line 2950
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2952
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v0, :cond_10

    .line 2953
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2955
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v0, :cond_11

    .line 2956
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2958
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v0, :cond_12

    .line 2959
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2961
    :cond_12
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v0, :cond_13

    .line 2962
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2964
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v0, :cond_14

    .line 2965
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2967
    :cond_14
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v0, :cond_15

    .line 2968
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2970
    :cond_15
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    if-eqz v0, :cond_16

    .line 2971
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Template;->singleCardContainer:Lcom/google/android/finsky/protos/DocumentV2$SingleCardContainer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2973
    :cond_16
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2974
    return-void
.end method
