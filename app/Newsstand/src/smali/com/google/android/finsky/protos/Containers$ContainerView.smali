.class public final Lcom/google/android/finsky/protos/Containers$ContainerView;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Containers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Containers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContainerView"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Containers$ContainerView;


# instance fields
.field public hasListUrl:Z

.field public hasSelected:Z

.field public hasServerLogsCookie:Z

.field public hasTitle:Z

.field public listUrl:Ljava/lang/String;

.field public selected:Z

.field public serverLogsCookie:[B

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 262
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerView;->clear()Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 263
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Containers$ContainerView;
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->_emptyArray:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-nez v0, :cond_1

    .line 235
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->_emptyArray:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-nez v0, :cond_0

    .line 238
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Containers$ContainerView;

    sput-object v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->_emptyArray:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    .line 240
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Containers$ContainerView;->_emptyArray:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    return-object v0

    .line 240
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Containers$ContainerView;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    .line 267
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasSelected:Z

    .line 268
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    .line 269
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasTitle:Z

    .line 270
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    .line 271
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasListUrl:Z

    .line 272
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    .line 273
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasServerLogsCookie:Z

    .line 274
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->cachedSize:I

    .line 275
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 298
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 299
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasSelected:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    if-eqz v1, :cond_1

    .line 300
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    .line 301
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasTitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 304
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    .line 305
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasListUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 308
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    .line 309
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasServerLogsCookie:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 312
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    .line 313
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Containers$ContainerView;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 324
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 328
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 329
    :sswitch_0
    return-object p0

    .line 334
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    .line 335
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasSelected:Z

    goto :goto_0

    .line 339
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    .line 340
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasTitle:Z

    goto :goto_0

    .line 344
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    .line 345
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasListUrl:Z

    goto :goto_0

    .line 349
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    .line 350
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasServerLogsCookie:Z

    goto :goto_0

    .line 324
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Containers$ContainerView;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Containers$ContainerView;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasSelected:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 284
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasTitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 285
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 287
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasListUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 288
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->listUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 290
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->hasServerLogsCookie:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 291
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Containers$ContainerView;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 293
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 294
    return-void
.end method
