.class public final Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GroupLicenseDocid"
.end annotation


# instance fields
.field public backend:I

.field public backendDocid:Ljava/lang/String;

.field public hasBackend:Z

.field public hasBackendDocid:Z

.field public hasType:Z

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->clear()Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    .line 443
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 446
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    .line 447
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackendDocid:Z

    .line 448
    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    .line 449
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasType:Z

    .line 450
    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    .line 451
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackend:Z

    .line 452
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->cachedSize:I

    .line 453
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 473
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 474
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackendDocid:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 475
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    .line 476
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 478
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasType:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    if-eqz v1, :cond_3

    .line 479
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    .line 480
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 482
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackend:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    if-eqz v1, :cond_5

    .line 483
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    .line 484
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 486
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 494
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 495
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 499
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 500
    :sswitch_0
    return-object p0

    .line 505
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    .line 506
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackendDocid:Z

    goto :goto_0

    .line 510
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    .line 511
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasType:Z

    goto :goto_0

    .line 515
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    .line 516
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackend:Z

    goto :goto_0

    .line 495
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 412
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackendDocid:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 460
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backendDocid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 462
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasType:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    if-eqz v0, :cond_3

    .line 463
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 465
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->hasBackend:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    if-eqz v0, :cond_5

    .line 466
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$GroupLicenseDocid;->backend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 468
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 469
    return-void
.end method
