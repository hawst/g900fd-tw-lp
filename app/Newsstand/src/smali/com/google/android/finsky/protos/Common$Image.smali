.class public final Lcom/google/android/finsky/protos/Common$Image;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/Common$Image$Citation;,
        Lcom/google/android/finsky/protos/Common$Image$Dimension;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Common$Image;


# instance fields
.field public altTextLocalized:Ljava/lang/String;

.field public attribution:Lcom/google/android/finsky/protos/Common$Attribution;

.field public autogen:Z

.field public citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

.field public dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

.field public durationSeconds:I

.field public fillColorRgb:Ljava/lang/String;

.field public hasAltTextLocalized:Z

.field public hasAutogen:Z

.field public hasDurationSeconds:Z

.field public hasFillColorRgb:Z

.field public hasImageType:Z

.field public hasImageUrl:Z

.field public hasPositionInSequence:Z

.field public hasSecureUrl:Z

.field public hasSupportsFifeUrlOptions:Z

.field public imageType:I

.field public imageUrl:Ljava/lang/String;

.field public positionInSequence:I

.field public secureUrl:Ljava/lang/String;

.field public supportsFifeUrlOptions:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1359
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1360
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Image;->clear()Lcom/google/android/finsky/protos/Common$Image;

    .line 1361
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Common$Image;
    .locals 2

    .prologue
    .line 1303
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v0, :cond_1

    .line 1304
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1306
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v0, :cond_0

    .line 1307
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Image;

    sput-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    .line 1309
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1311
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Common$Image;->_emptyArray:[Lcom/google/android/finsky/protos/Common$Image;

    return-object v0

    .line 1309
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Image;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1364
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 1365
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    .line 1366
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    .line 1367
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    .line 1368
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1369
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1370
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    .line 1371
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    .line 1372
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    .line 1373
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    .line 1374
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    .line 1375
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    .line 1376
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    .line 1377
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    .line 1378
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    .line 1379
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    .line 1380
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    .line 1381
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    .line 1382
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    .line 1383
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    .line 1384
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1385
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image;->cachedSize:I

    .line 1386
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1433
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1434
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    if-eqz v1, :cond_1

    .line 1435
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 1436
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1438
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-eqz v1, :cond_2

    .line 1439
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1440
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1442
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1443
    :cond_3
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1444
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1446
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1447
    :cond_5
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    .line 1448
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1450
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1451
    :cond_7
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    .line 1452
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1454
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    if-nez v1, :cond_9

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    if-eqz v1, :cond_a

    .line 1455
    :cond_9
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    .line 1456
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1458
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-nez v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v1, :cond_c

    .line 1459
    :cond_b
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    .line 1460
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1462
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-eqz v1, :cond_d

    .line 1463
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1464
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1466
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    if-nez v1, :cond_e

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    if-eqz v1, :cond_f

    .line 1467
    :cond_e
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    .line 1468
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1470
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1471
    :cond_10
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    .line 1472
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1474
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    if-nez v1, :cond_12

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    if-eqz v1, :cond_13

    .line 1475
    :cond_12
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    .line 1476
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1478
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-eqz v1, :cond_14

    .line 1479
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    .line 1480
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1482
    :cond_14
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1490
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1491
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1495
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1496
    :sswitch_0
    return-object p0

    .line 1501
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1502
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1516
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 1517
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    goto :goto_0

    .line 1523
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-nez v2, :cond_1

    .line 1524
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image$Dimension;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1526
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    const/4 v3, 0x2

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/android/MessageNano;I)V

    goto :goto_0

    .line 1530
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 1531
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    goto :goto_0

    .line 1535
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    .line 1536
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    goto :goto_0

    .line 1540
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    .line 1541
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    goto :goto_0

    .line 1545
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    .line 1546
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    goto :goto_0

    .line 1550
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    .line 1551
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    goto :goto_0

    .line 1555
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-nez v2, :cond_2

    .line 1556
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image$Citation;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image$Citation;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    .line 1558
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    const/16 v3, 0xa

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/android/MessageNano;I)V

    goto :goto_0

    .line 1562
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    .line 1563
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    goto :goto_0

    .line 1567
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    .line 1568
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    goto :goto_0

    .line 1572
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    .line 1573
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    goto/16 :goto_0

    .line 1577
    :sswitch_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-nez v2, :cond_3

    .line 1578
    new-instance v2, Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Attribution;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    .line 1580
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 1491
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x53 -> :sswitch_8
        0x70 -> :sswitch_9
        0x7a -> :sswitch_a
        0x80 -> :sswitch_b
        0x8a -> :sswitch_c
    .end sparse-switch

    .line 1502
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1044
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Image;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1392
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    if-eqz v0, :cond_1

    .line 1393
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1395
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-eqz v0, :cond_2

    .line 1396
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1398
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1399
    :cond_3
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1401
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAltTextLocalized:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1402
    :cond_5
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->altTextLocalized:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1404
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSecureUrl:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1405
    :cond_7
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->secureUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1407
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasPositionInSequence:Z

    if-nez v0, :cond_9

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    if-eqz v0, :cond_a

    .line 1408
    :cond_9
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->positionInSequence:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1410
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v0, :cond_c

    .line 1411
    :cond_b
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1413
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    if-eqz v0, :cond_d

    .line 1414
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->citation:Lcom/google/android/finsky/protos/Common$Image$Citation;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1416
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasDurationSeconds:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    if-eqz v0, :cond_f

    .line 1417
    :cond_e
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image;->durationSeconds:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1419
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasFillColorRgb:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1420
    :cond_10
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1422
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->hasAutogen:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    if-eqz v0, :cond_13

    .line 1423
    :cond_12
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image;->autogen:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1425
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    if-eqz v0, :cond_14

    .line 1426
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Image;->attribution:Lcom/google/android/finsky/protos/Common$Attribution;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1428
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1429
    return-void
.end method
