.class public final Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReasonUserAction"
.end annotation


# instance fields
.field public hasLocalizedDescriptionHtml:Z

.field public localizedDescriptionHtml:Ljava/lang/String;

.field public person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2363
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2364
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    .line 2365
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;
    .locals 1

    .prologue
    .line 2368
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2369
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->hasLocalizedDescriptionHtml:Z

    .line 2371
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->cachedSize:I

    .line 2372
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2389
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2390
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_0

    .line 2391
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2392
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2394
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->hasLocalizedDescriptionHtml:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2395
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2396
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2398
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2407
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2411
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2412
    :sswitch_0
    return-object p0

    .line 2417
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_1

    .line 2418
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2420
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2424
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    .line 2425
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->hasLocalizedDescriptionHtml:Z

    goto :goto_0

    .line 2407
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2339
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2378
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    .line 2379
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->person:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2381
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->hasLocalizedDescriptionHtml:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2382
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$ReasonUserAction;->localizedDescriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2384
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2385
    return-void
.end method
