.class public final Lcom/google/android/finsky/protos/Common$MonthAndDay;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MonthAndDay"
.end annotation


# instance fields
.field public day:I

.field public hasDay:Z

.field public hasMonth:Z

.field public month:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2309
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2310
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$MonthAndDay;->clear()Lcom/google/android/finsky/protos/Common$MonthAndDay;

    .line 2311
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$MonthAndDay;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2314
    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    .line 2315
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    .line 2316
    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    .line 2317
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    .line 2318
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->cachedSize:I

    .line 2319
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2336
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2337
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    if-eqz v1, :cond_1

    .line 2338
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    .line 2339
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2341
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    if-eqz v1, :cond_3

    .line 2342
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    .line 2343
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2345
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$MonthAndDay;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2353
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2354
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2358
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2359
    :sswitch_0
    return-object p0

    .line 2364
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    .line 2365
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    goto :goto_0

    .line 2369
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    .line 2370
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    goto :goto_0

    .line 2354
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2284
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$MonthAndDay;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$MonthAndDay;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2325
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasMonth:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    if-eqz v0, :cond_1

    .line 2326
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->month:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 2328
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->hasDay:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    if-eqz v0, :cond_3

    .line 2329
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$MonthAndDay;->day:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 2331
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2332
    return-void
.end method
