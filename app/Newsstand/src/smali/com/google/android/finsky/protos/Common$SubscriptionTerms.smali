.class public final Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubscriptionTerms"
.end annotation


# instance fields
.field public formattedPriceWithRecurrencePeriod:Ljava/lang/String;

.field public hasFormattedPriceWithRecurrencePeriod:Z

.field public recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

.field public replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

.field public seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

.field public trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2527
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2528
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->clear()Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    .line 2529
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2532
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2533
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2534
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 2535
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    .line 2536
    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2537
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Docid;->emptyArray()[Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    .line 2538
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->cachedSize:I

    .line 2539
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2570
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2571
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v3, :cond_0

    .line 2572
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2573
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2575
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v3, :cond_1

    .line 2576
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2577
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2579
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2580
    :cond_2
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 2581
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2583
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-eqz v3, :cond_4

    .line 2584
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2585
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2587
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 2588
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 2589
    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    aget-object v0, v3, v1

    .line 2590
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v0, :cond_5

    .line 2591
    const/4 v3, 0x5

    .line 2592
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2588
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2596
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v1    # "i":I
    :cond_6
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionTerms;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2604
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2605
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2609
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2610
    :sswitch_0
    return-object p0

    .line 2615
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v5, :cond_1

    .line 2616
    new-instance v5, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2618
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2622
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-nez v5, :cond_2

    .line 2623
    new-instance v5, Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$TimePeriod;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2625
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2629
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 2630
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    goto :goto_0

    .line 2634
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-nez v5, :cond_3

    .line 2635
    new-instance v5, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    .line 2637
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 2641
    :sswitch_5
    const/16 v5, 0x2a

    .line 2642
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2643
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_5

    move v1, v4

    .line 2644
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Docid;

    .line 2646
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v1, :cond_4

    .line 2647
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2649
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 2650
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    aput-object v5, v2, v1

    .line 2651
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2652
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 2649
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2643
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v1, v5

    goto :goto_1

    .line 2655
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Docid;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    aput-object v5, v2, v1

    .line 2656
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 2657
    iput-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    goto/16 :goto_0

    .line 2605
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2494
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2545
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v2, :cond_0

    .line 2546
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->recurringPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2548
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    if-eqz v2, :cond_1

    .line 2549
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->trialPeriod:Lcom/google/android/finsky/protos/Common$TimePeriod;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2551
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->hasFormattedPriceWithRecurrencePeriod:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2552
    :cond_2
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2554
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    if-eqz v2, :cond_4

    .line 2555
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->seasonalSubscriptionInfo:Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2557
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2558
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2559
    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->replaceDocid:[Lcom/google/android/finsky/protos/Common$Docid;

    aget-object v0, v2, v1

    .line 2560
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Docid;
    if-eqz v0, :cond_5

    .line 2561
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2558
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2565
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v1    # "i":I
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2566
    return-void
.end method
