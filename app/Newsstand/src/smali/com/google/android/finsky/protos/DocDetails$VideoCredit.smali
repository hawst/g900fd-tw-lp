.class public final Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoCredit"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;


# instance fields
.field public credit:Ljava/lang/String;

.field public creditType:I

.field public hasCredit:Z

.field public hasCreditType:Z

.field public name:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3269
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 3270
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3271
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 2

    .prologue
    .line 3247
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v0, :cond_1

    .line 3248
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3250
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v0, :cond_0

    .line 3251
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3253
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3255
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    return-object v0

    .line 3253
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3274
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    .line 3275
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    .line 3276
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    .line 3277
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    .line 3278
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    .line 3279
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->cachedSize:I

    .line 3280
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 3305
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 3306
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    if-eqz v5, :cond_1

    .line 3307
    :cond_0
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    .line 3308
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 3310
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3311
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    .line 3312
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3314
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 3315
    const/4 v0, 0x0

    .line 3316
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 3317
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 3318
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3319
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 3320
    add-int/lit8 v0, v0, 0x1

    .line 3322
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3317
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3325
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v1

    .line 3326
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3328
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 3336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3337
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3341
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3342
    :sswitch_0
    return-object p0

    .line 3347
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3348
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 3353
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    .line 3354
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    goto :goto_0

    .line 3360
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    .line 3361
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    goto :goto_0

    .line 3365
    :sswitch_3
    const/16 v6, 0x1a

    .line 3366
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3367
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-nez v6, :cond_2

    move v1, v5

    .line 3368
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 3369
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 3370
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3372
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 3373
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 3374
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3372
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3367
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 3377
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 3378
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    goto :goto_0

    .line 3337
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 3348
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3235
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3286
    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    if-eqz v2, :cond_1

    .line 3287
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3289
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3290
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3292
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 3293
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 3294
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 3295
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 3296
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3293
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3300
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 3301
    return-void
.end method
