.class public final Lcom/google/android/finsky/protos/Document$PlayDocument;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Document.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayDocument"
.end annotation


# instance fields
.field public detailsUrl:Ljava/lang/String;

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public docidStr:Ljava/lang/String;

.field public hasDetailsUrl:Z

.field public hasDocidStr:Z

.field public hasSubtitle:Z

.field public hasTitle:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public subtitle:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Document$PlayDocument;->clear()Lcom/google/android/finsky/protos/Document$PlayDocument;

    .line 49
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Document$PlayDocument;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    .line 54
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDocidStr:Z

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    .line 56
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasTitle:Z

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    .line 58
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasSubtitle:Z

    .line 59
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    .line 61
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDetailsUrl:Z

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->cachedSize:I

    .line 63
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 98
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v3, :cond_0

    .line 99
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 100
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 102
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDocidStr:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 103
    :cond_1
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    .line 104
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 106
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasTitle:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 107
    :cond_3
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    .line 108
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 110
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasSubtitle:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 111
    :cond_5
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    .line 112
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 114
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_8

    .line 115
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_8

    .line 116
    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 117
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_7

    .line 118
    const/4 v3, 0x5

    .line 119
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 115
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_8
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDetailsUrl:Z

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 124
    :cond_9
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    .line 125
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 127
    :cond_a
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Document$PlayDocument;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 135
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 136
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 140
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 141
    :sswitch_0
    return-object p0

    .line 146
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v5, :cond_1

    .line 147
    new-instance v5, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 149
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 153
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    .line 154
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDocidStr:Z

    goto :goto_0

    .line 158
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    .line 159
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasTitle:Z

    goto :goto_0

    .line 163
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    .line 164
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasSubtitle:Z

    goto :goto_0

    .line 168
    :sswitch_5
    const/16 v5, 0x2a

    .line 169
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 170
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_3

    move v1, v4

    .line 171
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Image;

    .line 173
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_2

    .line 174
    iget-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 176
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 177
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 178
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 179
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 170
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v5

    goto :goto_1

    .line 182
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 183
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 184
    iput-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto :goto_0

    .line 188
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    .line 189
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDetailsUrl:Z

    goto/16 :goto_0

    .line 136
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Document$PlayDocument;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Document$PlayDocument;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v2, :cond_0

    .line 70
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 72
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDocidStr:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 73
    :cond_1
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->docidStr:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasTitle:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 76
    :cond_3
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasSubtitle:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 79
    :cond_5
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->subtitle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 81
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 82
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 83
    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 84
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_7

    .line 85
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 82
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->hasDetailsUrl:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 90
    :cond_9
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Document$PlayDocument;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 92
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 93
    return-void
.end method
