.class public final Lcom/google/android/finsky/protos/DocDetails$AppPermission;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppPermission"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;


# instance fields
.field public hasKey:Z

.field public hasPermissionRequired:Z

.field public key:Ljava/lang/String;

.field public permissionRequired:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1657
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1658
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->clear()Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 1659
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 2

    .prologue
    .line 1638
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-nez v0, :cond_1

    .line 1639
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1641
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    if-nez v0, :cond_0

    .line 1642
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    .line 1644
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1646
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    return-object v0

    .line 1644
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1662
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    .line 1663
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    .line 1664
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    .line 1665
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    .line 1666
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->cachedSize:I

    .line 1667
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1684
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1685
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1686
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    .line 1687
    invoke-static {v3, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1689
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    if-eq v1, v3, :cond_3

    .line 1690
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    .line 1691
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppPermission;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1701
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1702
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1706
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1707
    :sswitch_0
    return-object p0

    .line 1712
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    .line 1713
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    goto :goto_0

    .line 1717
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    .line 1718
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    goto :goto_0

    .line 1702
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1632
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$AppPermission;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1673
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasKey:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1674
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->key:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1676
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->hasPermissionRequired:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    if-eq v0, v2, :cond_3

    .line 1677
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$AppPermission;->permissionRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1679
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1680
    return-void
.end method
