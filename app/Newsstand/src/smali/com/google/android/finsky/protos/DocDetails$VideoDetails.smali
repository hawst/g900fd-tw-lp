.class public final Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoDetails"
.end annotation


# instance fields
.field public audioLanguage:[Ljava/lang/String;

.field public captionLanguage:[Ljava/lang/String;

.field public contentRating:Ljava/lang/String;

.field public credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

.field public dislikes:J

.field public duration:Ljava/lang/String;

.field public genre:[Ljava/lang/String;

.field public hasContentRating:Z

.field public hasDislikes:Z

.field public hasDuration:Z

.field public hasLikes:Z

.field public hasReleaseDate:Z

.field public likes:J

.field public releaseDate:Ljava/lang/String;

.field public rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

.field public trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2879
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2880
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    .line 2881
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2884
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 2885
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    .line 2886
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    .line 2887
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    .line 2888
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    .line 2889
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    .line 2890
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    .line 2891
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    .line 2892
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    .line 2893
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    .line 2894
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    .line 2895
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    .line 2896
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$Trailer;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 2897
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 2898
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    .line 2899
    sget-object v0, Lcom/google/protobuf/nano/android/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    .line 2900
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->cachedSize:I

    .line 2901
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 2975
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 2976
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v5, v5

    if-lez v5, :cond_1

    .line 2977
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 2978
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    aget-object v2, v5, v3

    .line 2979
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v2, :cond_0

    .line 2980
    const/4 v5, 0x1

    .line 2981
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2977
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2985
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .end local v3    # "i":I
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2986
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    .line 2987
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2989
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2990
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    .line 2991
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2993
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 2994
    :cond_6
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    .line 2995
    invoke-static {v5, v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2997
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    if-nez v5, :cond_8

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_9

    .line 2998
    :cond_8
    const/4 v5, 0x5

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    .line 2999
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 3001
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    if-nez v5, :cond_a

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_b

    .line 3002
    :cond_a
    const/4 v5, 0x6

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    .line 3003
    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 3005
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_e

    .line 3006
    const/4 v0, 0x0

    .line 3007
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 3008
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_d

    .line 3009
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3010
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_c

    .line 3011
    add-int/lit8 v0, v0, 0x1

    .line 3013
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3008
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3016
    .end local v2    # "element":Ljava/lang/String;
    :cond_d
    add-int/2addr v4, v1

    .line 3017
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3019
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v5, v5

    if-lez v5, :cond_10

    .line 3020
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v5, v5

    if-ge v3, v5, :cond_10

    .line 3021
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    aget-object v2, v5, v3

    .line 3022
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v2, :cond_f

    .line 3023
    const/16 v5, 0x8

    .line 3024
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3020
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3028
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .end local v3    # "i":I
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v5, v5

    if-lez v5, :cond_12

    .line 3029
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v5, v5

    if-ge v3, v5, :cond_12

    .line 3030
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    aget-object v2, v5, v3

    .line 3031
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v2, :cond_11

    .line 3032
    const/16 v5, 0x9

    .line 3033
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3029
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 3037
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .end local v3    # "i":I
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 3038
    const/4 v0, 0x0

    .line 3039
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 3040
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 3041
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3042
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 3043
    add-int/lit8 v0, v0, 0x1

    .line 3045
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3040
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 3048
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 3049
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3051
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-eqz v5, :cond_18

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_18

    .line 3052
    const/4 v0, 0x0

    .line 3053
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 3054
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 3055
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3056
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_16

    .line 3057
    add-int/lit8 v0, v0, 0x1

    .line 3059
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3054
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 3062
    .end local v2    # "element":Ljava/lang/String;
    :cond_17
    add-int/2addr v4, v1

    .line 3063
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3065
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_18
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 3073
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3074
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3078
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3079
    :sswitch_0
    return-object p0

    .line 3084
    :sswitch_1
    const/16 v5, 0xa

    .line 3085
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3086
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v5, :cond_2

    move v1, v4

    .line 3087
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3089
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v1, :cond_1

    .line 3090
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3092
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3093
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;-><init>()V

    aput-object v5, v2, v1

    .line 3094
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3095
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3092
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3086
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v1, v5

    goto :goto_1

    .line 3098
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;-><init>()V

    aput-object v5, v2, v1

    .line 3099
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3100
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    goto :goto_0

    .line 3104
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    .line 3105
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    goto :goto_0

    .line 3109
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    .line 3110
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    goto :goto_0

    .line 3114
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    .line 3115
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    goto :goto_0

    .line 3119
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    .line 3120
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    goto :goto_0

    .line 3124
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    .line 3125
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    goto :goto_0

    .line 3129
    :sswitch_7
    const/16 v5, 0x3a

    .line 3130
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3131
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 3132
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3133
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 3134
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3136
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 3137
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3138
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3136
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3131
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 3141
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3142
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3146
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    const/16 v5, 0x42

    .line 3147
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3148
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-nez v5, :cond_8

    move v1, v4

    .line 3149
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 3151
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v1, :cond_7

    .line 3152
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3154
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 3155
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$Trailer;-><init>()V

    aput-object v5, v2, v1

    .line 3156
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3157
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3154
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 3148
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v1, v5

    goto :goto_5

    .line 3160
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :cond_9
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$Trailer;-><init>()V

    aput-object v5, v2, v1

    .line 3161
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3162
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    goto/16 :goto_0

    .line 3166
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :sswitch_9
    const/16 v5, 0x4a

    .line 3167
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3168
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-nez v5, :cond_b

    move v1, v4

    .line 3169
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 3171
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v1, :cond_a

    .line 3172
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3174
    :cond_a
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 3175
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;-><init>()V

    aput-object v5, v2, v1

    .line 3176
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3177
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3174
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 3168
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v1, v5

    goto :goto_7

    .line 3180
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :cond_c
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;-><init>()V

    aput-object v5, v2, v1

    .line 3181
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 3182
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    goto/16 :goto_0

    .line 3186
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :sswitch_a
    const/16 v5, 0x52

    .line 3187
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3188
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-nez v5, :cond_e

    move v1, v4

    .line 3189
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3190
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_d

    .line 3191
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3193
    :cond_d
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 3194
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3195
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3193
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 3188
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_9

    .line 3198
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3199
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3203
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_b
    const/16 v5, 0x5a

    .line 3204
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3205
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-nez v5, :cond_11

    move v1, v4

    .line 3206
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3207
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_10

    .line 3208
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3210
    :cond_10
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_12

    .line 3211
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3212
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 3210
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 3205
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_b

    .line 3215
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3216
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3074
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2824
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 2907
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2908
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 2909
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    aget-object v0, v2, v1

    .line 2910
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v0, :cond_0

    .line 2911
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2908
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2915
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2916
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2918
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2919
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2921
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2922
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2924
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    if-nez v2, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_9

    .line 2925
    :cond_8
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2927
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    if-nez v2, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_b

    .line 2928
    :cond_a
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2930
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 2931
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 2932
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2933
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_c

    .line 2934
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2931
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2938
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 2939
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 2940
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    aget-object v0, v2, v1

    .line 2941
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v0, :cond_e

    .line 2942
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2939
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2946
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .end local v1    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 2947
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 2948
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    aget-object v0, v2, v1

    .line 2949
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v0, :cond_10

    .line 2950
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2947
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2954
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .end local v1    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 2955
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 2956
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2957
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 2958
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2955
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2962
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 2963
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 2964
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2965
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_14

    .line 2966
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2963
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2970
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2971
    return-void
.end method
