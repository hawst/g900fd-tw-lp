.class public final Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvEpisodeDetails"
.end annotation


# instance fields
.field public episodeIndex:I

.field public hasEpisodeIndex:Z

.field public hasParentDetailsUrl:Z

.field public hasReleaseDate:Z

.field public parentDetailsUrl:Ljava/lang/String;

.field public releaseDate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4425
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 4426
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    .line 4427
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4430
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4431
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasParentDetailsUrl:Z

    .line 4432
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    .line 4433
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasEpisodeIndex:Z

    .line 4434
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    .line 4435
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasReleaseDate:Z

    .line 4436
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->cachedSize:I

    .line 4437
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4457
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4458
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasParentDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4459
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4460
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4462
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasEpisodeIndex:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    if-eqz v1, :cond_3

    .line 4463
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    .line 4464
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4466
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasReleaseDate:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4467
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    .line 4468
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4470
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4479
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4483
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4484
    :sswitch_0
    return-object p0

    .line 4489
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4490
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasParentDetailsUrl:Z

    goto :goto_0

    .line 4494
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    .line 4495
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasEpisodeIndex:Z

    goto :goto_0

    .line 4499
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    .line 4500
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasReleaseDate:Z

    goto :goto_0

    .line 4479
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4396
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4443
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasParentDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4444
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->parentDetailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4446
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasEpisodeIndex:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    if-eqz v0, :cond_3

    .line 4447
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4449
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->hasReleaseDate:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 4450
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4452
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 4453
    return-void
.end method
