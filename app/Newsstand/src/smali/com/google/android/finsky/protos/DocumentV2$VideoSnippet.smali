.class public final Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoSnippet"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;


# instance fields
.field public description:Ljava/lang/String;

.field public hasDescription:Z

.field public hasTitle:Z

.field public image:[Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5155
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 5156
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->clear()Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 5157
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 2

    .prologue
    .line 5133
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v0, :cond_1

    .line 5134
    sget-object v1, Lcom/google/protobuf/nano/android/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 5136
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    if-nez v0, :cond_0

    .line 5137
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    .line 5139
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5141
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    return-object v0

    .line 5139
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5160
    invoke-static {}, Lcom/google/android/finsky/protos/Common$Image;->emptyArray()[Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .line 5161
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    .line 5162
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    .line 5163
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    .line 5164
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    .line 5165
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->cachedSize:I

    .line 5166
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 5191
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 5192
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 5193
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 5194
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v3, v1

    .line 5195
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_0

    .line 5196
    const/4 v3, 0x1

    .line 5197
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5193
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5201
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 5202
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    .line 5203
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5205
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 5206
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    .line 5207
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 5209
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 5217
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 5218
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 5222
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5223
    :sswitch_0
    return-object p0

    .line 5228
    :sswitch_1
    const/16 v5, 0xa

    .line 5229
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 5230
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_2

    move v1, v4

    .line 5231
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Common$Image;

    .line 5233
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_1

    .line 5234
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5236
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 5237
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 5238
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 5239
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 5236
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5230
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v1, v5

    goto :goto_1

    .line 5242
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    aput-object v5, v2, v1

    .line 5243
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 5244
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    goto :goto_0

    .line 5248
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Common$Image;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    .line 5249
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    goto :goto_0

    .line 5253
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    .line 5254
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    goto :goto_0

    .line 5218
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5127
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5172
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 5173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 5174
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    aget-object v0, v2, v1

    .line 5175
    .local v0, "element":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v0, :cond_0

    .line 5176
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 5173
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5180
    .end local v0    # "element":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasTitle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 5181
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5183
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->hasDescription:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 5184
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5186
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 5187
    return-void
.end method
