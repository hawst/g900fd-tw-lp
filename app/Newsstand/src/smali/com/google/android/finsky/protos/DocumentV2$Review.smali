.class public final Lcom/google/android/finsky/protos/DocumentV2$Review;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Review"
.end annotation


# instance fields
.field public author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public authorName:Ljava/lang/String;

.field public comment:Ljava/lang/String;

.field public commentId:Ljava/lang/String;

.field public deviceName:Ljava/lang/String;

.field public documentVersion:Ljava/lang/String;

.field public hasAuthorName:Z

.field public hasComment:Z

.field public hasCommentId:Z

.field public hasDeviceName:Z

.field public hasDocumentVersion:Z

.field public hasReplyText:Z

.field public hasReplyTimestampMsec:Z

.field public hasSource:Z

.field public hasStarRating:Z

.field public hasTimestampMsec:Z

.field public hasTitle:Z

.field public hasUrl:Z

.field public oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

.field public replyText:Ljava/lang/String;

.field public replyTimestampMsec:J

.field public sentiment:Lcom/google/android/finsky/protos/Common$Image;

.field public source:Ljava/lang/String;

.field public starRating:I

.field public timestampMsec:J

.field public title:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2518
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 2519
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$Review;->clear()Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 2520
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2523
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    .line 2524
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasCommentId:Z

    .line 2525
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2526
    iput v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 2527
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    .line 2528
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    .line 2529
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    .line 2530
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTitle:Z

    .line 2531
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    .line 2532
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasComment:Z

    .line 2533
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    .line 2534
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasUrl:Z

    .line 2535
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    .line 2536
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasSource:Z

    .line 2537
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    .line 2538
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDocumentVersion:Z

    .line 2539
    iput-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    .line 2540
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    .line 2541
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    .line 2542
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDeviceName:Z

    .line 2543
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    .line 2544
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    .line 2545
    iput-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    .line 2546
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    .line 2547
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2548
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    .line 2549
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasAuthorName:Z

    .line 2550
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->cachedSize:I

    .line 2551
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2607
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2608
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasAuthorName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2609
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    .line 2610
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2612
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2613
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    .line 2614
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2616
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasSource:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2617
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    .line 2618
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2620
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDocumentVersion:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2621
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    .line 2622
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2624
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    if-nez v1, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 2625
    :cond_8
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    .line 2626
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2628
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    if-eqz v1, :cond_b

    .line 2629
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 2630
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2632
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTitle:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2633
    :cond_c
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    .line 2634
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2636
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasComment:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 2637
    :cond_e
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    .line 2638
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2640
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasCommentId:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 2641
    :cond_10
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    .line 2642
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2644
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDeviceName:Z

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 2645
    :cond_12
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    .line 2646
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2648
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 2649
    :cond_14
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    .line 2650
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2652
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    if-nez v1, :cond_16

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_17

    .line 2653
    :cond_16
    const/16 v1, 0x1e

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    .line 2654
    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2656
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v1, :cond_18

    .line 2657
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2658
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2660
    :cond_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_19

    .line 2661
    const/16 v1, 0x21

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2662
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2664
    :cond_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_1a

    .line 2665
    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    .line 2666
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2668
    :cond_1a
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2676
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2677
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2681
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2682
    :sswitch_0
    return-object p0

    .line 2687
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    .line 2688
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasAuthorName:Z

    goto :goto_0

    .line 2692
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    .line 2693
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasUrl:Z

    goto :goto_0

    .line 2697
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    .line 2698
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasSource:Z

    goto :goto_0

    .line 2702
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    .line 2703
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDocumentVersion:Z

    goto :goto_0

    .line 2707
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    .line 2708
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    goto :goto_0

    .line 2712
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 2713
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    goto :goto_0

    .line 2717
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    .line 2718
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTitle:Z

    goto :goto_0

    .line 2722
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    .line 2723
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasComment:Z

    goto :goto_0

    .line 2727
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    .line 2728
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasCommentId:Z

    goto :goto_0

    .line 2732
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    .line 2733
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDeviceName:Z

    goto :goto_0

    .line 2737
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    .line 2738
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    goto :goto_0

    .line 2742
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    .line 2743
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    goto :goto_0

    .line 2747
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v1, :cond_1

    .line 2748
    new-instance v1, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 2750
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 2754
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_2

    .line 2755
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 2757
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 2761
    :sswitch_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_3

    .line 2762
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    .line 2764
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto/16 :goto_0

    .line 2677
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x9a -> :sswitch_a
        0xea -> :sswitch_b
        0xf0 -> :sswitch_c
        0xfa -> :sswitch_d
        0x10a -> :sswitch_e
        0x112 -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2444
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$Review;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 2557
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasAuthorName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2558
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->authorName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2560
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2561
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2563
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasSource:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2564
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->source:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2566
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDocumentVersion:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2567
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->documentVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2569
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTimestampMsec:Z

    if-nez v0, :cond_8

    iget-wide v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 2570
    :cond_8
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->timestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2572
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasStarRating:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    if-eqz v0, :cond_b

    .line 2573
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2575
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasTitle:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2576
    :cond_c
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2578
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasComment:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 2579
    :cond_e
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2581
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasCommentId:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2582
    :cond_10
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2584
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasDeviceName:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 2585
    :cond_12
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->deviceName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2587
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 2588
    :cond_14
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2590
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyTimestampMsec:Z

    if-nez v0, :cond_16

    iget-wide v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_17

    .line 2591
    :cond_16
    const/16 v0, 0x1e

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->replyTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2593
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v0, :cond_18

    .line 2594
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->oBSOLETEPlusProfile:Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2596
    :cond_18
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_19

    .line 2597
    const/16 v0, 0x21

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->author:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2599
    :cond_19
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_1a

    .line 2600
    const/16 v0, 0x22

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$Review;->sentiment:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 2602
    :cond_1a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 2603
    return-void
.end method
