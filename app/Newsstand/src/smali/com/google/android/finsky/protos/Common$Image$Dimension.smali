.class public final Lcom/google/android/finsky/protos/Common$Image$Dimension;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Dimension"
.end annotation


# instance fields
.field public aspectRatio:I

.field public hasAspectRatio:Z

.field public hasHeight:Z

.field public hasWidth:Z

.field public height:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1091
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1092
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Image$Dimension;->clear()Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 1093
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Image$Dimension;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1096
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 1097
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    .line 1098
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 1099
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    .line 1100
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    .line 1101
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    .line 1102
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->cachedSize:I

    .line 1103
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1123
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1124
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    if-eqz v1, :cond_1

    .line 1125
    :cond_0
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 1126
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1128
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    if-eqz v1, :cond_3

    .line 1129
    :cond_2
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 1130
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1132
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    if-eqz v1, :cond_5

    .line 1133
    :cond_4
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    .line 1134
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1136
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Dimension;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1145
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1149
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1150
    :sswitch_0
    return-object p0

    .line 1155
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 1156
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    goto :goto_0

    .line 1160
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 1161
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    goto :goto_0

    .line 1165
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1166
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1173
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    .line 1174
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    goto :goto_0

    .line 1145
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x20 -> :sswitch_2
        0x90 -> :sswitch_3
    .end sparse-switch

    .line 1166
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1062
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Image$Dimension;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Dimension;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1109
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    if-eqz v0, :cond_1

    .line 1110
    :cond_0
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1112
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    if-eqz v0, :cond_3

    .line 1113
    :cond_2
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1115
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    if-eqz v0, :cond_5

    .line 1116
    :cond_4
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1118
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1119
    return-void
.end method
