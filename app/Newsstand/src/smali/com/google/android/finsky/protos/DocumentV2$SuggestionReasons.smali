.class public final Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SuggestionReasons"
.end annotation


# instance fields
.field public neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

.field public positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

.field public reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1699
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    .line 1700
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->clear()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1701
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1704
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1705
    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1706
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$Reason;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    .line 1707
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->cachedSize:I

    .line 1708
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 1733
    invoke-super {p0}, Lcom/google/protobuf/nano/android/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1734
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 1735
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 1736
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    aget-object v0, v3, v1

    .line 1737
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v0, :cond_0

    .line 1738
    const/4 v3, 0x2

    .line 1739
    invoke-static {v3, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1735
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1743
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v3, :cond_2

    .line 1744
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1745
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1747
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v3, :cond_3

    .line 1748
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1749
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/android/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1751
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1759
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1760
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1764
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/android/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1765
    :sswitch_0
    return-object p0

    .line 1770
    :sswitch_1
    const/16 v5, 0x12

    .line 1771
    invoke-static {p1, v5}, Lcom/google/protobuf/nano/android/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1772
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    if-nez v5, :cond_2

    move v1, v4

    .line 1773
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$Reason;

    .line 1775
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v1, :cond_1

    .line 1776
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1778
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1779
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Reason;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Reason;-><init>()V

    aput-object v5, v2, v1

    .line 1780
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1781
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readTag()I

    .line 1778
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1772
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Reason;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v1, v5

    goto :goto_1

    .line 1784
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Reason;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Reason;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Reason;-><init>()V

    aput-object v5, v2, v1

    .line 1785
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 1786
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    goto :goto_0

    .line 1790
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Reason;
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-nez v5, :cond_4

    .line 1791
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1793
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1797
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-nez v5, :cond_5

    .line 1798
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    .line 1800
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V

    goto :goto_0

    .line 1760
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/android/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1673
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->mergeFrom(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1714
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1715
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 1716
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    aget-object v0, v2, v1

    .line 1717
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v0, :cond_0

    .line 1718
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1715
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1722
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v2, :cond_2

    .line 1723
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1725
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v2, :cond_3

    .line 1726
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->positiveDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/android/MessageNano;)V

    .line 1728
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/android/MessageNano;->writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V

    .line 1729
    return-void
.end method
