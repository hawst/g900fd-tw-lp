.class Lcom/google/android/play/drawer/PlayDrawerAdapter$9;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field final synthetic val$secondaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 599
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iput-object p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;->val$secondaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;->val$secondaryAction:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    invoke-interface {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 605
    :cond_0
    return-void
.end method
