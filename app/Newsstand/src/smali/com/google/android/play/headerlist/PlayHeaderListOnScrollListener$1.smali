.class Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;
.super Landroid/database/DataSetObserver;
.source "PlayHeaderListOnScrollListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->reset(Z)V
    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;Z)V

    .line 60
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncCurrentListViewOnNextScroll()V

    .line 61
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener$1;->onChanged()V

    .line 66
    return-void
.end method
