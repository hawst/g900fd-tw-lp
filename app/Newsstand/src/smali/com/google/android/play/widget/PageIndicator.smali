.class public Lcom/google/android/play/widget/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "PageIndicator.java"


# instance fields
.field private mSelectedPage:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/widget/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    .line 37
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->init()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    .line 42
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->init()V

    .line 43
    return-void
.end method

.method private addDot()Landroid/widget/ImageView;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 88
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 89
    .local v2, "view":Landroid/widget/ImageView;
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 90
    sget v3, Lcom/google/android/play/R$drawable;->play_onboard_page_indicator_dot:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 94
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/R$dimen;->play_onboard__page_indicator_dot_diameter:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    div-int/lit8 v0, v3, 0x2

    .line 96
    .local v0, "margin":I
    const/16 v3, 0x10

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 97
    invoke-virtual {v1, v0, v5, v0, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 98
    invoke-virtual {p0, v2, v1}, Lcom/google/android/play/widget/PageIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    return-object v2
.end method

.method private setDotState(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;
    .locals 3
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "selected"    # Z

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 104
    .local v1, "dot":Landroid/graphics/drawable/GradientDrawable;
    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/play/R$color;->play_onboard__page_indicator_dot_active:I

    .line 107
    .local v0, "colorResId":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 108
    return-object p1

    .line 104
    .end local v0    # "colorResId":I
    :cond_0
    sget v0, Lcom/google/android/play/R$color;->play_onboard__page_indicator_dot_inactive:I

    goto :goto_0
.end method

.method private updateContentDescription()V
    .locals 5

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$string;->play_content_description_page_indicator:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getSelectedPage()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getPageCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 112
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/PageIndicator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method


# virtual methods
.method public getPageCount()I
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getSelectedPage()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    return v0
.end method

.method protected init()V
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/PageIndicator;->setGravity(I)V

    .line 47
    return-void
.end method

.method public setPageCount(I)V
    .locals 6
    .param p1, "newCount"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 60
    if-ltz p1, :cond_1

    move v2, v3

    :goto_0
    const-string v5, "numPages must be >=0"

    invoke-static {v2, v5}, Lcom/google/android/play/utils/Assertions;->checkArgument(ZLjava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v0

    .line 62
    .local v0, "currentCount":I
    if-ge p1, v0, :cond_2

    .line 63
    sub-int v2, v0, p1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/play/widget/PageIndicator;->removeViews(II)V

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/widget/PageIndicator;->updateContentDescription()V

    .line 70
    return-void

    .end local v0    # "currentCount":I
    :cond_1
    move v2, v4

    .line 60
    goto :goto_0

    .line 64
    .restart local v0    # "currentCount":I
    :cond_2
    if-le p1, v0, :cond_0

    .line 65
    move v1, v0

    .local v1, "i":I
    :goto_1
    if-ge v1, p1, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/google/android/play/widget/PageIndicator;->addDot()Landroid/widget/ImageView;

    move-result-object v5

    iget v2, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    if-ne v1, v2, :cond_3

    move v2, v3

    :goto_2
    invoke-direct {p0, v5, v2}, Lcom/google/android/play/widget/PageIndicator;->setDotState(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v2, v4

    .line 66
    goto :goto_2
.end method

.method public setSelectedPage(I)V
    .locals 4
    .param p1, "page"    # I

    .prologue
    .line 77
    iget v2, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    if-eq v2, p1, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v0

    .line 79
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 80
    invoke-virtual {p0, v1}, Lcom/google/android/play/widget/PageIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-ne v1, p1, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/google/android/play/widget/PageIndicator;->setDotState(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;

    .line 79
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 82
    :cond_1
    iput p1, p0, Lcom/google/android/play/widget/PageIndicator;->mSelectedPage:I

    .line 83
    invoke-direct {p0}, Lcom/google/android/play/widget/PageIndicator;->updateContentDescription()V

    .line 85
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_2
    return-void
.end method
