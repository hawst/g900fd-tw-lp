.class Lcom/google/android/play/drawer/ShrinkingItem;
.super Landroid/widget/LinearLayout;
.source "ShrinkingItem.java"


# instance fields
.field private mAnimatedHeightFraction:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/play/drawer/ShrinkingItem;->mAnimatedHeightFraction:F

    .line 28
    return-void
.end method


# virtual methods
.method public getAnimatedHeightFraction()F
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/play/drawer/ShrinkingItem;->mAnimatedHeightFraction:F

    return v0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/play/drawer/ShrinkingItem;->getMeasuredHeight()I

    move-result v1

    .line 44
    .local v1, "totalHeight":I
    iget v2, p0, Lcom/google/android/play/drawer/ShrinkingItem;->mAnimatedHeightFraction:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/play/drawer/ShrinkingItem;->mAnimatedHeightFraction:F

    int-to-float v3, v1

    mul-float/2addr v2, v3

    .line 45
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 46
    .local v0, "h":I
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/play/drawer/ShrinkingItem;->setMeasuredDimension(II)V

    .line 47
    return-void

    .end local v0    # "h":I
    :cond_0
    move v0, v1

    .line 45
    goto :goto_0
.end method

.method public setAnimatedHeightFraction(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/google/android/play/drawer/ShrinkingItem;->mAnimatedHeightFraction:F

    .line 33
    invoke-virtual {p0}, Lcom/google/android/play/drawer/ShrinkingItem;->requestLayout()V

    .line 34
    return-void
.end method
