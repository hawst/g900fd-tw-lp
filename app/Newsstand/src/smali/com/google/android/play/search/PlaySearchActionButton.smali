.class public Lcom/google/android/play/search/PlaySearchActionButton;
.super Landroid/widget/ImageView;
.source "PlaySearchActionButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/search/PlaySearchActionButton$Listener;
    }
.end annotation


# instance fields
.field private mClearDrawable:Landroid/graphics/drawable/Drawable;

.field private mCurrentMode:I

.field private mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;

.field private mMicDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$drawable;->play_ic_clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mClearDrawable:Landroid/graphics/drawable/Drawable;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$drawable;->ic_mic_dark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mMicDrawable:Landroid/graphics/drawable/Drawable;

    .line 68
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/PlaySearchActionButton$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchActionButton;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/search/PlaySearchActionButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchActionButton;

    .prologue
    .line 17
    iget v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    return v0
.end method

.method private setMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I

    .line 110
    if-nez p1, :cond_1

    .line 111
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const/4 v1, 0x0

    .line 116
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v0, -0x1

    .line 117
    .local v0, "accessibilityString":I
    const/4 v2, 0x1

    if-ne p1, v2, :cond_3

    .line 118
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mClearDrawable:Landroid/graphics/drawable/Drawable;

    .line 119
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_clear:I

    .line 125
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 128
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    goto :goto_0

    .line 120
    :cond_3
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 121
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mMicDrawable:Landroid/graphics/drawable/Drawable;

    .line 122
    sget v0, Lcom/google/android/play/R$string;->play_accessibility_search_plate_voice_search_button:I

    goto :goto_1
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishInflate()V

    .line 74
    new-instance v0, Lcom/google/android/play/search/PlaySearchActionButton$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/PlaySearchActionButton$1;-><init>(Lcom/google/android/play/search/PlaySearchActionButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public setListener(Lcom/google/android/play/search/PlaySearchActionButton$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    .line 93
    return-void
.end method

.method public switchToMode(I)V
    .locals 2
    .param p1, "searchMode"    # I

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 99
    if-ne p1, v0, :cond_0

    .line 100
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    .line 106
    :goto_0
    return-void

    .line 101
    :cond_0
    if-ne p1, v1, :cond_1

    .line 102
    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    goto :goto_0

    .line 104
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setMode(I)V

    goto :goto_0
.end method
