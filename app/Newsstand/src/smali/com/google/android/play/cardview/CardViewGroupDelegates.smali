.class public Lcom/google/android/play/cardview/CardViewGroupDelegates;
.super Ljava/lang/Object;
.source "CardViewGroupDelegates.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;,
        Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;
    }
.end annotation


# static fields
.field public static final IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

.field public static final NO_CARD_BG_IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    const-string v0, "L"

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 43
    :cond_0
    new-instance v0, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;

    invoke-direct {v0, v2}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupL;-><init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V

    sput-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    .line 49
    :goto_0
    new-instance v0, Lcom/google/android/play/cardview/CardViewGroupDelegates$1;

    invoke-direct {v0}, Lcom/google/android/play/cardview/CardViewGroupDelegates$1;-><init>()V

    sput-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->NO_CARD_BG_IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-void

    .line 45
    :cond_1
    new-instance v0, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;

    invoke-direct {v0, v2}, Lcom/google/android/play/cardview/CardViewGroupDelegates$CardViewGroupEclairMr1;-><init>(Lcom/google/android/play/cardview/CardViewGroupDelegates$1;)V

    sput-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    goto :goto_0
.end method
