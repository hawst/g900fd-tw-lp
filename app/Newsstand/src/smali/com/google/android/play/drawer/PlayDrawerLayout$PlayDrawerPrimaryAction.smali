.class public Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
.super Ljava/lang/Object;
.source "PlayDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/drawer/PlayDrawerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlayDrawerPrimaryAction"
.end annotation


# instance fields
.field public final actionSelectedRunnable:Ljava/lang/Runnable;

.field public final actionText:Ljava/lang/String;

.field public final activeIconResId:I

.field public final activeTextColorResId:I

.field public final iconResId:I

.field public final isActive:Z

.field public final isAvailableInDownloadOnly:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V
    .locals 0
    .param p1, "actionText"    # Ljava/lang/String;
    .param p2, "iconResId"    # I
    .param p3, "activeIconResId"    # I
    .param p4, "activeTextColorResId"    # I
    .param p5, "isActive"    # Z
    .param p6, "isAvailableInDownloadOnly"    # Z
    .param p7, "actionSelectedRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionText:Ljava/lang/String;

    .line 184
    iput p2, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    .line 185
    iput p3, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    .line 186
    iput p4, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    .line 187
    iput-boolean p5, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    .line 188
    iput-boolean p6, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isAvailableInDownloadOnly:Z

    .line 189
    iput-object p7, p0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    .line 190
    return-void
.end method
