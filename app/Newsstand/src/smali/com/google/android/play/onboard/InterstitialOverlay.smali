.class public Lcom/google/android/play/onboard/InterstitialOverlay;
.super Landroid/widget/FrameLayout;
.source "InterstitialOverlay.java"


# static fields
.field public static final DEFAULT_ACCENT_COLORS_RES_IDS:[I

.field private static final LAYOUT:I


# instance fields
.field private mColors:[I

.field private mDots:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/widget/PulsatingDotDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private final mRandom:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    sget v0, Lcom/google/android/play/R$layout;->play_onboard_interstitial_overlay:I

    sput v0, Lcom/google/android/play/onboard/InterstitialOverlay;->LAYOUT:I

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/play/R$color;->play_onboard_accent_color_a:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/play/R$color;->play_onboard_accent_color_b:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/play/R$color;->play_onboard_accent_color_c:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/play/R$color;->play_onboard_accent_color_d:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/play/onboard/InterstitialOverlay;->DEFAULT_ACCENT_COLORS_RES_IDS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mRandom:Ljava/util/Random;

    .line 53
    return-void
.end method

.method private getRandomColor()I
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mColors:[I

    iget-object v1, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mRandom:Ljava/util/Random;

    iget-object v2, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mColors:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private static inflate(Landroid/content/Context;)Lcom/google/android/play/onboard/InterstitialOverlay;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/play/onboard/InterstitialOverlay;->LAYOUT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/InterstitialOverlay;

    return-object v0
.end method

.method private initializeDot(Landroid/view/View;)Lcom/google/android/play/widget/PulsatingDotDrawable;
    .locals 8
    .param p1, "cellView"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/play/widget/PulsatingDotDrawable;

    invoke-direct {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->getRandomColor()I

    move-result v1

    const-wide/16 v2, 0x320

    iget-object v4, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mRandom:Ljava/util/Random;

    const/16 v5, 0x320

    .line 98
    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-long v4, v4

    const v6, 0x3dcccccd    # 0.1f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/widget/PulsatingDotDrawable;-><init>(IJJFF)V

    .line 99
    .local v0, "dot":Lcom/google/android/play/widget/PulsatingDotDrawable;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 100
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private initializeDotsIfNeeded()V
    .locals 6

    .prologue
    .line 70
    iget-object v4, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mDots:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 92
    :goto_0
    return-void

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/play/R$string;->play_onboard_interstitial_grid_cell:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "cellTag":Ljava/lang/String;
    new-instance v4, Lcom/google/android/play/onboard/InterstitialOverlay$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/play/onboard/InterstitialOverlay$1;-><init>(Lcom/google/android/play/onboard/InterstitialOverlay;Ljava/lang/String;)V

    invoke-static {p0, v4}, Lcom/google/android/play/onboard/OnboardUtils;->getAllDescendants(Landroid/view/ViewGroup;Lcom/google/android/play/onboard/OnboardUtils$Predicate;)Ljava/util/Collection;

    move-result-object v2

    .line 82
    .local v2, "cells":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    iget-object v4, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mColors:[I

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mColors:[I

    array-length v4, v4

    if-nez v4, :cond_2

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/google/android/play/onboard/InterstitialOverlay;->DEFAULT_ACCENT_COLORS_RES_IDS:[I

    invoke-virtual {p0, v4, v5}, Lcom/google/android/play/onboard/InterstitialOverlay;->setColorResIds(Landroid/content/Context;[I)V

    .line 86
    :cond_2
    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 87
    .local v3, "dots":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/widget/PulsatingDotDrawable;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 88
    .local v0, "cell":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;->initializeDot(Landroid/view/View;)Lcom/google/android/play/widget/PulsatingDotDrawable;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 91
    .end local v0    # "cell":Landroid/view/View;
    :cond_3
    iput-object v3, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mDots:Ljava/util/List;

    goto :goto_0
.end method

.method public static makeInstanceWithColorResIds(Landroid/content/Context;[I)Lcom/google/android/play/onboard/InterstitialOverlay;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "colorResIds"    # [I

    .prologue
    .line 167
    invoke-static {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->inflate(Landroid/content/Context;)Lcom/google/android/play/onboard/InterstitialOverlay;

    move-result-object v0

    .line 168
    .local v0, "overlay":Lcom/google/android/play/onboard/InterstitialOverlay;
    invoke-virtual {v0, p0, p1}, Lcom/google/android/play/onboard/InterstitialOverlay;->setColorResIds(Landroid/content/Context;[I)V

    .line 169
    return-object v0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 58
    invoke-direct {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->initializeDotsIfNeeded()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->startAnimation()V

    .line 60
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->stopAnimation()V

    .line 65
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 66
    return-void
.end method

.method public setCaption(I)V
    .locals 1
    .param p1, "stringResId"    # I

    .prologue
    .line 128
    sget v0, Lcom/google/android/play/R$id;->caption:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 129
    return-void
.end method

.method protected setColorResIds(Landroid/content/Context;[I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "colorResIds"    # [I

    .prologue
    .line 141
    array-length v3, p2

    new-array v0, v3, [I

    .line 142
    .local v0, "colors":[I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 143
    .local v2, "resources":Landroid/content/res/Resources;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p2

    if-ge v1, v3, :cond_0

    .line 144
    aget v3, p2, v1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v0, v1

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;->setColors([I)V

    .line 147
    return-void
.end method

.method protected setColors([I)V
    .locals 0
    .param p1, "colors"    # [I

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mColors:[I

    .line 138
    return-void
.end method

.method public startAnimation()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mDots:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/PulsatingDotDrawable;

    .line 113
    .local v0, "dot":Lcom/google/android/play/widget/PulsatingDotDrawable;
    invoke-virtual {v0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->start()V

    goto :goto_0

    .line 115
    .end local v0    # "dot":Lcom/google/android/play/widget/PulsatingDotDrawable;
    :cond_0
    return-void
.end method

.method public stopAnimation()V
    .locals 3

    .prologue
    .line 118
    iget-object v1, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->mDots:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/PulsatingDotDrawable;

    .line 119
    .local v0, "dot":Lcom/google/android/play/widget/PulsatingDotDrawable;
    invoke-virtual {v0}, Lcom/google/android/play/widget/PulsatingDotDrawable;->stop()V

    goto :goto_0

    .line 121
    .end local v0    # "dot":Lcom/google/android/play/widget/PulsatingDotDrawable;
    :cond_0
    return-void
.end method
