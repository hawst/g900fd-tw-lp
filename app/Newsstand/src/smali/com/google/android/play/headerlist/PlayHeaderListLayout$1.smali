.class Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;
.super Ljava/lang/Object;
.source "PlayHeaderListLayout.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private updatePullToRefreshPosition(IFI)V
    .locals 8
    .param p1, "pagePosition"    # I
    .param p2, "pagePositionOffset"    # F
    .param p3, "pagePositionOffsetPixels"    # I

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    .line 370
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v4, v4, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-nez v4, :cond_1

    .line 423
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 375
    :cond_1
    cmpl-float v4, p2, v7

    if-lez v4, :cond_3

    const/4 v4, 0x1

    :goto_1
    add-int v2, p1, v4

    .line 381
    .local v2, "pullToRefreshAdapterPage":I
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->isRefreshing()Z

    move-result v1

    .line 385
    .local v1, "isRefreshing":Z
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$500(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v4

    if-eq v2, v4, :cond_2

    .line 386
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # setter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I
    invoke-static {v4, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$502(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)I

    .line 387
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v4, v4, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 388
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I
    invoke-static {v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$500(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v5

    .line 387
    invoke-virtual {v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->isPageRefreshing(I)Z

    move-result v1

    .line 389
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 390
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v4, v4, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 391
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;
    invoke-static {v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v5

    .line 390
    invoke-virtual {v4, v5, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->onPullToRefreshDisplayedPageChanged(Landroid/support/v4/widget/SwipeRefreshLayout;I)V

    .line 395
    :cond_2
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v4, v4, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    invoke-virtual {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->getPositionMode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 400
    :pswitch_1
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z
    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$600()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 404
    if-eqz v1, :cond_5

    .line 406
    sub-float v4, v7, p2

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 407
    .local v0, "alpha":F
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$700(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    .line 409
    cmpl-float v4, p2, v7

    if-lez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 410
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$800(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v4

    sub-int v3, v4, p3

    .line 412
    .local v3, "translationX":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$700(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-result-object v4

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationX(F)V

    goto/16 :goto_0

    .line 375
    .end local v0    # "alpha":F
    .end local v1    # "isRefreshing":Z
    .end local v2    # "pullToRefreshAdapterPage":I
    .end local v3    # "translationX":I
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 410
    .restart local v0    # "alpha":F
    .restart local v1    # "isRefreshing":Z
    .restart local v2    # "pullToRefreshAdapterPage":I
    :cond_4
    mul-int/lit8 v3, p3, -0x1

    goto :goto_2

    .line 416
    .end local v0    # "alpha":F
    :cond_5
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$700(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    .line 417
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    invoke-static {v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$700(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationX(F)V

    goto/16 :goto_0

    .line 395
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v2, 0x1

    .line 329
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 332
    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 333
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 334
    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->snapControlsIfNeeded(ZZZ)V
    invoke-static {v0, v2, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$200(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZZZ)V

    .line 340
    :cond_2
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 344
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->updatePullToRefreshPosition(IFI)V

    .line 345
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 349
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateActiveListView()V
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 357
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 360
    :cond_0
    return-void
.end method
