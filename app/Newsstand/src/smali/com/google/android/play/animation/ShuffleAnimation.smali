.class public Lcom/google/android/play/animation/ShuffleAnimation;
.super Ljava/lang/Object;
.source "ShuffleAnimation.java"


# static fields
.field private static final SQRT_2:F

.field public static final SUPPORTS_SHUFFLE_ANIMATION:Z

.field private static sContainerPos:[I

.field private static sViewPos:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 21
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/animation/ShuffleAnimation;->SUPPORTS_SHUFFLE_ANIMATION:Z

    .line 27
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/play/animation/ShuffleAnimation;->SQRT_2:F

    .line 28
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/play/animation/ShuffleAnimation;->sViewPos:[I

    .line 29
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/play/animation/ShuffleAnimation;->sContainerPos:[I

    return-void

    .line 21
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shuffle(Landroid/view/ViewGroup;ZLcom/google/android/play/animation/AnimationGroupEvaluator;)V
    .locals 5
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p1, "directionUp"    # Z
    .param p2, "optShuffleEvaluator"    # Lcom/google/android/play/animation/AnimationGroupEvaluator;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 51
    sget-boolean v4, Lcom/google/android/play/animation/ShuffleAnimation;->SUPPORTS_SHUFFLE_ANIMATION:Z

    if-eqz v4, :cond_0

    if-nez p0, :cond_1

    .line 64
    :cond_0
    return-void

    .line 55
    :cond_1
    new-instance v0, Lcom/google/android/play/animation/AnimationStateCounter;

    invoke-direct {v0, p0}, Lcom/google/android/play/animation/AnimationStateCounter;-><init>(Landroid/view/ViewGroup;)V

    .line 56
    .local v0, "animationStateCounter":Lcom/google/android/play/animation/AnimationStateCounter;
    if-nez p2, :cond_2

    new-instance v3, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;

    invoke-direct {v3}, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;-><init>()V

    .line 60
    .local v3, "shuffleEvaluator":Lcom/google/android/play/animation/AnimationGroupEvaluator;
    :goto_0
    const/4 v2, 0x0

    .local v2, "childIndex":I
    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 61
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 62
    .local v1, "child":Landroid/view/View;
    invoke-static {v1, v0, p0, p1, v3}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffleChild(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;ZLcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 60
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childIndex":I
    .end local v3    # "shuffleEvaluator":Lcom/google/android/play/animation/AnimationGroupEvaluator;
    :cond_2
    move-object v3, p2

    .line 56
    goto :goto_0
.end method

.method public static shuffleAnimator(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/ViewPropertyAnimator;
    .locals 18
    .param p0, "view"    # Landroid/view/View;
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "directionUp"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 123
    sget-boolean v13, Lcom/google/android/play/animation/ShuffleAnimation;->SUPPORTS_SHUFFLE_ANIMATION:Z

    if-nez v13, :cond_1

    .line 124
    const/4 v2, 0x0

    .line 154
    :cond_0
    :goto_0
    return-object v2

    .line 126
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v7

    .line 127
    .local v7, "rootView":Landroid/view/View;
    sget-object v13, Lcom/google/android/play/animation/ShuffleAnimation;->sViewPos:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/view/View;->getLocationInWindow([I)V

    .line 128
    sget-object v13, Lcom/google/android/play/animation/ShuffleAnimation;->sContainerPos:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 129
    sget-object v13, Lcom/google/android/play/animation/ShuffleAnimation;->sViewPos:[I

    const/4 v14, 0x1

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/play/animation/ShuffleAnimation;->sContainerPos:[I

    const/4 v15, 0x1

    aget v14, v14, v15

    sub-int/2addr v13, v14

    int-to-float v13, v13

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v14

    int-to-float v14, v14

    div-float v3, v13, v14

    .line 130
    .local v3, "heightRatio":F
    sget-object v13, Lcom/google/android/play/animation/ShuffleAnimation;->sViewPos:[I

    const/4 v14, 0x0

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/play/animation/ShuffleAnimation;->sContainerPos:[I

    const/4 v15, 0x0

    aget v14, v14, v15

    sub-int/2addr v13, v14

    int-to-float v13, v13

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v14

    int-to-float v14, v14

    div-float v12, v13, v14

    .line 131
    .local v12, "widthRatio":F
    if-nez p2, :cond_2

    .line 132
    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v3, v13, v3

    .line 133
    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v12, v13, v12

    .line 135
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v13

    int-to-float v13, v13

    sget v14, Lcom/google/android/play/animation/ShuffleAnimation;->SQRT_2:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    div-float v6, v13, v14

    .line 137
    .local v6, "maxRotationalHeight":F
    if-eqz p2, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/view/View;->setTranslationY(F)V

    .line 138
    if-eqz p2, :cond_4

    const/high16 v13, 0x41800000    # 16.0f

    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/view/View;->setRotation(F)V

    .line 139
    const-wide/16 v8, 0x96

    .line 140
    .local v8, "maxX":J
    const-wide/16 v14, 0x1f4

    sub-long v10, v14, v8

    .line 141
    .local v10, "maxY":J
    const-wide/16 v14, 0x0

    long-to-float v13, v10

    mul-float/2addr v13, v3

    long-to-float v0, v8

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v13, v13, v16

    float-to-long v0, v13

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 142
    .local v4, "duration":J
    if-eqz p2, :cond_5

    const-wide/16 v14, 0xfa

    :goto_3
    add-long/2addr v14, v4

    const-wide/16 v16, 0x1f4

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 145
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 146
    .local v2, "animator":Landroid/view/ViewPropertyAnimator;
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 147
    if-eqz p2, :cond_6

    const/4 v13, 0x0

    :goto_4
    invoke-virtual {v2, v13}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v14

    if-eqz p2, :cond_7

    const/4 v13, 0x0

    .line 148
    :goto_5
    invoke-virtual {v14, v13}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v13

    .line 149
    invoke-virtual {v13, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 150
    sget v13, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v14, 0x15

    if-lt v13, v14, :cond_0

    if-eqz p2, :cond_0

    .line 151
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x10c000e

    invoke-static {v13, v14}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    .line 137
    .end local v2    # "animator":Landroid/view/ViewPropertyAnimator;
    .end local v4    # "duration":J
    .end local v8    # "maxX":J
    .end local v10    # "maxY":J
    :cond_3
    const/4 v13, 0x0

    goto :goto_1

    .line 138
    :cond_4
    const/4 v13, 0x0

    goto :goto_2

    .line 142
    .restart local v4    # "duration":J
    .restart local v8    # "maxX":J
    .restart local v10    # "maxY":J
    :cond_5
    const-wide/16 v14, 0x15e

    goto :goto_3

    .line 147
    .restart local v2    # "animator":Landroid/view/ViewPropertyAnimator;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v13

    int-to-float v13, v13

    add-float/2addr v13, v6

    goto :goto_4

    :cond_7
    const/high16 v13, 0x41800000    # 16.0f

    goto :goto_5
.end method

.method private static shuffleChild(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;ZLcom/google/android/play/animation/AnimationGroupEvaluator;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "animationStateCounter"    # Lcom/google/android/play/animation/AnimationStateCounter;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "directionUp"    # Z
    .param p4, "shuffleEvaluator"    # Lcom/google/android/play/animation/AnimationGroupEvaluator;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 69
    if-nez p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v3, p0

    check-cast v3, Landroid/view/ViewGroup;

    invoke-interface {p4, v3}, Lcom/google/android/play/animation/AnimationGroupEvaluator;->isAnimationGroup(Landroid/view/ViewGroup;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v2, p0

    .line 73
    check-cast v2, Landroid/view/ViewGroup;

    .line 74
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "childIndex":I
    :goto_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 75
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "child":Landroid/view/View;
    invoke-static {v0, p1, p2, p3, p4}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffleChild(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;ZLcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 74
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childIndex":I
    .end local v2    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffleView(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;Z)V

    goto :goto_0
.end method

.method private static shuffleView(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;Z)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "animationStateCounter"    # Lcom/google/android/play/animation/AnimationStateCounter;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "directionUp"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 92
    invoke-static {p0, p2, p3}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffleAnimator(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 93
    .local v0, "animator":Landroid/view/ViewPropertyAnimator;
    new-instance v1, Lcom/google/android/play/animation/ShuffleAnimation$1;

    invoke-direct {v1, p1, p0}, Lcom/google/android/play/animation/ShuffleAnimation$1;-><init>(Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 104
    return-void
.end method
