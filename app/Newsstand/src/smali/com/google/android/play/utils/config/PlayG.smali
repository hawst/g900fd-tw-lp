.class public Lcom/google/android/play/utils/config/PlayG;
.super Ljava/lang/Object;
.source "PlayG.java"


# static fields
.field public static final GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

.field public static final androidId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final authTokenType:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static bitmapLoaderCacheSizeOverrideMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static bitmapLoaderCacheSizeRatioToScreen:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final clientId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeBackoffMultipler:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final dfeRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final loggingId:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final minImageSizeLimitInLRUCacheBytes:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize2G:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize3G:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize4G:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSizeWifi:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgBackoffMult:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final plusProfileBgTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final protoLogUrlRegexp:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final showStagingData:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final tentativeGcRunnerEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final webpFifeImagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-array v0, v5, [Ljava/lang/String;

    const-string v2, "playcommon"

    aput-object v2, v0, v4

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    .line 29
    const-string v2, "playcommon.mcc_mnc_override"

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 30
    invoke-static {v2, v0}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;

    .line 35
    const-string v0, "playcommon.proto_log_url_regexp"

    const-string v2, ".*"

    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->protoLogUrlRegexp:Lcom/google/android/play/utils/config/GservicesValue;

    .line 41
    const-string v0, "playcommon.dfe_request_timeout_ms"

    const/16 v2, 0x9c4

    .line 42
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 41
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeRequestTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 47
    const-string v0, "playcommon.dfe_max_retries"

    .line 48
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 47
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 53
    const-string v0, "playcommon.dfe_backoff_multiplier"

    .line 54
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 53
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->dfeBackoffMultipler:Lcom/google/android/play/utils/config/GservicesValue;

    .line 59
    const-string v0, "playcommon.plus_profile_bg_timeout_ms"

    const/16 v2, 0x1f40

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 59
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    .line 65
    const-string v0, "playcommon.plus_profile_bg_max_retries"

    .line 66
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 65
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgMaxRetries:Lcom/google/android/play/utils/config/GservicesValue;

    .line 71
    const-string v0, "playcommon.plus_profile_bg_backoff_mult"

    .line 72
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 71
    invoke-static {v0, v2}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->plusProfileBgBackoffMult:Lcom/google/android/play/utils/config/GservicesValue;

    .line 77
    const-string v0, "playcommon.ip_country_override"

    check-cast v1, Ljava/lang/String;

    .line 78
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;

    .line 80
    const-string v0, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 82
    const-string v0, "playcommon.auth_token_type"

    const-string v1, "androidmarket"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->authTokenType:Lcom/google/android/play/utils/config/GservicesValue;

    .line 85
    const-string v0, "logging_id2"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->partnerSetting(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->loggingId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 88
    const-string v0, "market_client_id"

    const-string v1, "am-google"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->partnerSetting(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->clientId:Lcom/google/android/play/utils/config/GservicesValue;

    .line 95
    const-string v0, "playcommon.skip_all_caches"

    .line 96
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    .line 99
    const-string v0, "playcommon.show_staging_data"

    .line 100
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    .line 103
    const-string v0, "playcommon.prex_disabled"

    .line 104
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 110
    const-string v0, "playcommon.tentative_gc_runner_enabled"

    .line 111
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->tentativeGcRunnerEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 116
    const-string v0, "playcommon.bitmap_loader_cache_size_mb"

    const/4 v1, -0x1

    .line 117
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 116
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->bitmapLoaderCacheSizeOverrideMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 119
    const-string v0, "playcommon.bitmap_loader_cache_size_ratio_to_screen"

    const/high16 v1, 0x3fc00000    # 1.5f

    .line 120
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 119
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->bitmapLoaderCacheSizeRatioToScreen:Lcom/google/android/play/utils/config/GservicesValue;

    .line 126
    const-string v0, "playcommon.min_image_size_limit_in_lru_cache_bytes"

    const/high16 v1, 0x80000

    .line 127
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->minImageSizeLimitInLRUCacheBytes:Lcom/google/android/play/utils/config/GservicesValue;

    .line 129
    const-string v0, "playcommon.debug_display_image_sizes"

    .line 130
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 129
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;

    .line 133
    const-string v0, "playcommon.webp_fife_images_enabled"

    .line 134
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->webpFifeImagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 137
    const-string v0, "playcommon.percent_of_image_size_wifi"

    .line 138
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->percentOfImageSizeWifi:Lcom/google/android/play/utils/config/GservicesValue;

    .line 141
    const-string v0, "playcommon.percent_of_image_size_4g"

    const v1, 0x3f666666    # 0.9f

    .line 142
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->percentOfImageSize4G:Lcom/google/android/play/utils/config/GservicesValue;

    .line 145
    const-string v0, "playcommon.percent_of_image_size_3g"

    const/high16 v1, 0x3f400000    # 0.75f

    .line 146
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->percentOfImageSize3G:Lcom/google/android/play/utils/config/GservicesValue;

    .line 149
    const-string v0, "playcommon.percent_of_image_size_2g"

    const v1, 0x3ee66666    # 0.45f

    .line 150
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->percentOfImageSize2G:Lcom/google/android/play/utils/config/GservicesValue;

    .line 155
    const-string v0, "playcommon.main_cache_size_mb"

    .line 156
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 155
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->mainCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 161
    const-string v0, "playcommon.image_cache_size_mb"

    const/4 v1, 0x4

    .line 162
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 161
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->imageCacheSizeMb:Lcom/google/android/play/utils/config/GservicesValue;

    .line 167
    const-string v0, "playcommon.volley_buffer_pool_size_kb"

    const/16 v1, 0x100

    .line 168
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 167
    invoke-static {v0, v1}, Lcom/google/android/play/utils/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/config/PlayG;->volleyBufferPoolSizeKb:Lcom/google/android/play/utils/config/GservicesValue;

    return-void
.end method
