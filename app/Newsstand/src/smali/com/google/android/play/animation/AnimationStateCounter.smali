.class public Lcom/google/android/play/animation/AnimationStateCounter;
.super Ljava/lang/Object;
.source "AnimationStateCounter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;,
        Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;
    }
.end annotation


# static fields
.field private static final SUPPORTS_TRACKING_ANIMATION_STATE:Z


# instance fields
.field private final mAnimatedChildCount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/ViewGroup;",
            "Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;",
            ">;"
        }
    .end annotation
.end field

.field private mIsTracking:Z

.field private final mTrackedViewGroup:Landroid/view/ViewGroup;

.field private mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/animation/AnimationStateCounter;->SUPPORTS_TRACKING_ANIMATION_STATE:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "trackedViewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mIsTracking:Z

    .line 48
    iput-object p1, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    .line 49
    return-void
.end method

.method private decrementAnimatedChildCount(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 278
    iget-object v1, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;

    .line 279
    .local v0, "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    iget v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mAnimatedChildCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mAnimatedChildCount:I

    .line 285
    iget v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mAnimatedChildCount:I

    if-nez v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-boolean v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mOriginalClipChildrenValue:Z

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 289
    :cond_0
    return-void
.end method

.method private incrementAnimatedChildCount(Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;

    .line 269
    .local v0, "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;

    .end local v0    # "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;-><init>(Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationStateCounter$1;)V

    .line 271
    .restart local v0    # "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    iget-object v1, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 274
    :cond_0
    iget v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mAnimatedChildCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mAnimatedChildCount:I

    .line 275
    return-void
.end method

.method private removeInternal(Landroid/view/View;)V
    .locals 3
    .param p1, "animatedChild"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 249
    if-eqz p1, :cond_0

    sget-boolean v2, Lcom/google/android/play/animation/AnimationStateCounter;->SUPPORTS_TRACKING_ANIMATION_STATE:Z

    if-nez v2, :cond_1

    .line 265
    :cond_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 253
    .local v0, "parent":Landroid/view/ViewParent;
    :cond_2
    :goto_0
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 254
    check-cast v1, Landroid/view/ViewGroup;

    .line 255
    .local v1, "parentViewGroup":Landroid/view/ViewGroup;
    invoke-direct {p0, v1}, Lcom/google/android/play/animation/AnimationStateCounter;->decrementAnimatedChildCount(Landroid/view/ViewGroup;)V

    .line 256
    iget-object v2, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    if-eq v1, v2, :cond_0

    .line 259
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 261
    iget-object v2, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    if-ne v0, v2, :cond_2

    .line 262
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    goto :goto_0
.end method


# virtual methods
.method public add(Landroid/view/View;)V
    .locals 2
    .param p1, "animatedChild"    # Landroid/view/View;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    sget-object v1, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    if-ne v0, v1, :cond_0

    .line 203
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot mix startTracking()/stopTracking() with add()/remove()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    sget-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    iput-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    .line 207
    invoke-virtual {p0, p1}, Lcom/google/android/play/animation/AnimationStateCounter;->addInternal(Landroid/view/View;)V

    .line 208
    return-void
.end method

.method public addInternal(Landroid/view/View;)V
    .locals 3
    .param p1, "animatedChild"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 212
    if-eqz p1, :cond_0

    sget-boolean v2, Lcom/google/android/play/animation/AnimationStateCounter;->SUPPORTS_TRACKING_ANIMATION_STATE:Z

    if-nez v2, :cond_1

    .line 231
    :cond_0
    return-void

    .line 219
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 220
    .local v0, "parent":Landroid/view/ViewParent;
    :cond_2
    :goto_0
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 221
    check-cast v1, Landroid/view/ViewGroup;

    .line 222
    .local v1, "parentViewGroup":Landroid/view/ViewGroup;
    invoke-direct {p0, v1}, Lcom/google/android/play/animation/AnimationStateCounter;->incrementAnimatedChildCount(Landroid/view/ViewGroup;)V

    .line 223
    iget-object v2, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    if-eq v1, v2, :cond_0

    .line 226
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 227
    iget-object v2, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    if-ne v0, v2, :cond_2

    .line 228
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    goto :goto_0
.end method

.method public addViewGroup(Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationGroupEvaluator;)V
    .locals 3
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "evaluator"    # Lcom/google/android/play/animation/AnimationGroupEvaluator;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 68
    if-eqz p1, :cond_0

    sget-boolean v2, Lcom/google/android/play/animation/AnimationStateCounter;->SUPPORTS_TRACKING_ANIMATION_STATE:Z

    if-nez v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    if-nez p2, :cond_2

    .line 72
    new-instance p2, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;

    .end local p2    # "evaluator":Lcom/google/android/play/animation/AnimationGroupEvaluator;
    invoke-direct {p2}, Lcom/google/android/play/animation/DefaultAnimationGroupEvaluator;-><init>()V

    .line 74
    .restart local p2    # "evaluator":Lcom/google/android/play/animation/AnimationGroupEvaluator;
    :cond_2
    invoke-interface {p2, p1}, Lcom/google/android/play/animation/AnimationGroupEvaluator;->isAnimationGroup(Landroid/view/ViewGroup;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 77
    invoke-virtual {p0, p1}, Lcom/google/android/play/animation/AnimationStateCounter;->addInternal(Landroid/view/View;)V

    goto :goto_0

    .line 80
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 81
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_4

    .line 83
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "child":Landroid/view/View;
    invoke-virtual {p0, v0, p2}, Lcom/google/android/play/animation/AnimationStateCounter;->addViewGroup(Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 80
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 85
    .restart local v0    # "child":Landroid/view/View;
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/play/animation/AnimationStateCounter;->addInternal(Landroid/view/View;)V

    goto :goto_2
.end method

.method public remove(Landroid/view/View;)V
    .locals 2
    .param p1, "animatedChild"    # Landroid/view/View;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    sget-object v1, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    if-ne v0, v1, :cond_0

    .line 241
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot mix startTracking()/stopTracking() with add()/remove()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/play/animation/AnimationStateCounter;->removeInternal(Landroid/view/View;)V

    .line 245
    return-void
.end method

.method public startTracking()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/animation/AnimationStateCounter;->startTracking(Lcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 95
    return-void
.end method

.method public startTracking(Lcom/google/android/play/animation/AnimationGroupEvaluator;)V
    .locals 2
    .param p1, "evaluator"    # Lcom/google/android/play/animation/AnimationGroupEvaluator;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    sget-object v1, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    if-ne v0, v1, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot mix startTracking()/stopTracking() with add()/remove()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mIsTracking:Z

    if-eqz v0, :cond_1

    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call startTracking() twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mIsTracking:Z

    .line 118
    sget-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    iput-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    .line 119
    iget-object v0, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/play/animation/AnimationStateCounter;->addViewGroup(Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationGroupEvaluator;)V

    .line 120
    return-void
.end method

.method public stopTracking()V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 167
    iget-object v3, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    sget-object v4, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    if-ne v3, v4, :cond_0

    .line 168
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot mix startTracking()/stopTracking() with add()/remove()"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 170
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mIsTracking:Z

    if-nez v3, :cond_1

    .line 171
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Cannot call stopTracking without a call to startTracking"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mAnimatedChildCount:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 177
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 178
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;

    .line 180
    .local v0, "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackedViewGroup:Landroid/view/ViewGroup;

    if-ne v4, v5, :cond_2

    .line 181
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    .line 182
    iget-boolean v4, v0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mOriginalClipChildrenValue:Z

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    goto :goto_0

    .line 185
    .end local v0    # "animationState":Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;>;"
    .end local v2    # "viewGroup":Landroid/view/ViewGroup;
    :cond_3
    iput-boolean v6, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mIsTracking:Z

    .line 186
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/play/animation/AnimationStateCounter;->mTrackingMode:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    .line 187
    return-void
.end method
