.class public Lcom/google/android/play/image/TentativeGcRunner;
.super Ljava/lang/Object;
.source "TentativeGcRunner.java"


# instance fields
.field private mAllocatedSinceLastRun:I

.field private mEnabled:Z

.field private mGcRunnable:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v1, Lcom/google/android/play/image/TentativeGcRunner$1;

    invoke-direct {v1, p0}, Lcom/google/android/play/image/TentativeGcRunner$1;-><init>(Lcom/google/android/play/image/TentativeGcRunner;)V

    iput-object v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mGcRunnable:Ljava/lang/Runnable;

    .line 67
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    .line 71
    .local v0, "processorCount":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_1

    if-le v0, v2, :cond_1

    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->tentativeGcRunnerEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    .line 72
    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mEnabled:Z

    .line 73
    iget-boolean v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mEnabled:Z

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "tentative-gc-runner"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mHandlerThread:Landroid/os/HandlerThread;

    .line 76
    iget-object v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 77
    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/play/image/TentativeGcRunner;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mHandler:Landroid/os/Handler;

    .line 79
    :cond_0
    return-void

    .line 72
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAllocatingSoon(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mEnabled:Z

    if-nez v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mAllocatedSinceLastRun:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mAllocatedSinceLastRun:I

    .line 91
    const v0, 0x14000

    if-le p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mAllocatedSinceLastRun:I

    const/high16 v1, 0x80000

    if-le v0, v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/image/TentativeGcRunner;->mGcRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/image/TentativeGcRunner;->mAllocatedSinceLastRun:I

    goto :goto_0
.end method
