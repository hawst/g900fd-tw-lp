.class public abstract Lcom/google/android/play/transition/delegate/TransitionDelegate;
.super Ljava/lang/Object;
.source "TransitionDelegate.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setupSharedElementCallbacks(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 396
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    move-object v2, p1

    .line 397
    check-cast v2, Landroid/support/v4/app/Fragment;

    new-instance v3, Lcom/google/android/play/transition/delegate/TransitionDelegate$5;

    invoke-direct {v3, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$5;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setEnterSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    move-object v2, p1

    .line 428
    check-cast v2, Landroid/support/v4/app/Fragment;

    new-instance v3, Lcom/google/android/play/transition/delegate/TransitionDelegate$6;

    invoke-direct {v3, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$6;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setEnterSharedElementCallback(Landroid/support/v4/app/SharedElementCallback;)V

    .line 510
    :goto_0
    return-void

    .line 453
    :cond_0
    new-instance v0, Lcom/google/android/play/transition/delegate/TransitionDelegate$7;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$7;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    .line 481
    .local v0, "enterSharedElementCallback":Landroid/app/SharedElementCallback;
    new-instance v1, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    .line 501
    .local v1, "exitSharedElementCallback":Landroid/app/SharedElementCallback;
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 508
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Unknown controller type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 504
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->setEnterSharedElementCallback(Landroid/app/SharedElementCallback;)V

    .line 505
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->setExitSharedElementCallback(Landroid/app/SharedElementCallback;)V

    goto :goto_0

    .line 501
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setupSharedElementTransitions(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 339
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v0

    .line 340
    .local v0, "sharedEnterTransition":Landroid/transition/Transition;
    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setSharedElementEnterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 343
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createSharedElementReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v1

    .line 344
    .local v1, "sharedReturnTransition":Landroid/transition/Transition;
    if-eqz v1, :cond_1

    .line 345
    invoke-virtual {p0, p1, v1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setSharedElementReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 347
    :cond_1
    return-void
.end method

.method private setupViewsTransitions(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v0

    .line 210
    .local v0, "enter":Landroid/transition/Transition;
    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setEnterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 213
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v3

    .line 214
    .local v3, "ret":Landroid/transition/Transition;
    if-eqz v3, :cond_1

    .line 215
    invoke-virtual {p0, p1, v3}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 217
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createExitTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v1

    .line 218
    .local v1, "exit":Landroid/transition/Transition;
    if-eqz v1, :cond_2

    .line 219
    invoke-virtual {p0, p1, v1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setExitTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 221
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->createReenterTransition(Ljava/lang/Object;)Landroid/transition/Transition;

    move-result-object v2

    .line 222
    .local v2, "reenter":Landroid/transition/Transition;
    if-eqz v2, :cond_3

    .line 223
    invoke-virtual {p0, p1, v2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setReenterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 225
    :cond_3
    return-void
.end method

.method private setupWindowProperties(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 584
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getDecorFadeDurationMs()Ljava/lang/Long;

    move-result-object v0

    .line 585
    .local v0, "decorFadeDuration":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 595
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unknown controller type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 589
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v1

    .line 590
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setTransitionBackgroundFadeDuration(J)V

    .line 598
    :cond_0
    return-void

    .line 593
    :pswitch_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Decor fade unsupported in fragments"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 586
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected createEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createExitTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createReenterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 109
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createSharedElementEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 274
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createSharedElementReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 283
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getActivity(Ljava/lang/Object;)Landroid/app/Activity;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/app/Activity;"
        }
    .end annotation

    .prologue
    .line 601
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 608
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :pswitch_0
    check-cast p1, Landroid/app/Activity;

    .line 606
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p1

    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    goto :goto_0

    .line 601
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected abstract getControllerType()I
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 580
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected handleEnterRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 544
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "rejectedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method protected handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    return-void
.end method

.method protected handleEnterSetSharedElementEnd(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 528
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method protected handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 519
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method protected handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 551
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "rejectedViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method protected handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 567
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    return-void
.end method

.method protected handleExitSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    return-void
.end method

.method public final onActivityCreated(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->prepareViewsForTransition(Ljava/lang/Object;)V

    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setupViewsTransitions(Ljava/lang/Object;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setupSharedElementTransitions(Ljava/lang/Object;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setupSharedElementCallbacks(Ljava/lang/Object;)V

    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->setupWindowProperties(Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method protected onEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 248
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 240
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 264
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 256
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onSharedElementEnterTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onSharedElementEnterTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 363
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onSharedElementReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 387
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 0
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 379
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    return-void
.end method

.method protected final setEnterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "enterTransition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/play/transition/delegate/TransitionDelegate$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$1;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 128
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setEnterTransition(Landroid/transition/Transition;)V

    .line 139
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 134
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setEnterTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final setExitTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "exitTransition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setExitTransition(Landroid/transition/Transition;)V

    .line 187
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 182
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setExitTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final setReenterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "reenterTransition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 195
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 204
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setReenterTransition(Landroid/transition/Transition;)V

    .line 206
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 201
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setReenterTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final setReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "returnTransition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/play/transition/delegate/TransitionDelegate$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$2;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 157
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setReturnTransition(Landroid/transition/Transition;)V

    .line 168
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 163
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setReturnTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final setSharedElementEnterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 288
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/play/transition/delegate/TransitionDelegate$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$3;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 299
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setSharedElementEnterTransition(Landroid/transition/Transition;)V

    .line 310
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 305
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setSharedElementEnterTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final setSharedElementReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
    .locals 2
    .param p2, "transition"    # Landroid/transition/Transition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/transition/Transition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 314
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate;, "Lcom/google/android/play/transition/delegate/TransitionDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;-><init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 325
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getControllerType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown controller type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/Window;->setSharedElementReturnTransition(Landroid/transition/Transition;)V

    .line 336
    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    :goto_0
    return-void

    .line 331
    .restart local p1    # "controller":Ljava/lang/Object;, "TT;"
    :pswitch_1
    check-cast p1, Landroid/support/v4/app/Fragment;

    .end local p1    # "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setSharedElementReturnTransition(Ljava/lang/Object;)V

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
