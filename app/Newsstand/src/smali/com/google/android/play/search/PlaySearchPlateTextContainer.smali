.class public Lcom/google/android/play/search/PlaySearchPlateTextContainer;
.super Landroid/widget/FrameLayout;
.source "PlaySearchPlateTextContainer.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;
    }
.end annotation


# instance fields
.field private final mInputManager:Landroid/view/inputmethod/InputMethodManager;

.field private mListener:Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

.field private mSearchBoxIdleText:Landroid/widget/ImageView;

.field private mSearchBoxTextInput:Landroid/widget/EditText;

.field private mSuspendTextChangeCallback:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mListener:Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

    return-object v0
.end method

.method private focusInputBoxAndShowKeyboard()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 122
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 123
    return-void
.end method

.method private unfocusInputBoxAndHideKeyboard()V
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    .line 127
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    .line 126
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 128
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 118
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 113
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 54
    sget v0, Lcom/google/android/play/R$id;->search_box_idle_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    .line 55
    sget v0, Lcom/google/android/play/R$id;->search_box_text_input:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->switchToMode(I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/play/search/PlaySearchPlateTextContainer$1;

    invoke-direct {v1, p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer$1;-><init>(Lcom/google/android/play/search/PlaySearchPlateTextContainer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mListener:Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSuspendTextChangeCallback:Z

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mListener:Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;->onTextChanged(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mListener:Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;

    .line 73
    return-void
.end method

.method public switchToMode(I)V
    .locals 3
    .param p1, "searchMode"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 79
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->unfocusInputBoxAndHideKeyboard()V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxIdleText:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->mSearchBoxTextInput:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 89
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->focusInputBoxAndShowKeyboard()V

    goto :goto_0
.end method
