.class Lcom/google/android/play/drawer/PlayDrawerAdapter$3;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 365
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 367
    .local v0, "isAccountDocLoaded":Z
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 368
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 367
    invoke-interface {v2, v0, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 371
    :cond_0
    return-void
.end method
