.class public Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlayPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/PlayPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p2, "popupActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mContext:Landroid/content/Context;

    .line 134
    iput-object p2, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    .line 135
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 149
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 154
    if-nez p2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/play/R$layout;->abc_popup_menu_item_layout:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 157
    new-instance v1, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;-><init>(Lcom/google/android/play/layout/PlayPopupMenu$1;)V

    .line 158
    .local v1, "viewHolder":Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    sget v2, Lcom/google/android/play/R$id;->title:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->title:Landroid/widget/TextView;
    invoke-static {v1, v2}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->access$102(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 159
    sget v2, Lcom/google/android/play/R$id;->shortcut:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->shortcut:Landroid/widget/TextView;
    invoke-static {v1, v2}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->access$202(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 160
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 163
    .end local v1    # "viewHolder":Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;

    .line 164
    .restart local v1    # "viewHolder":Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    .line 165
    .local v0, "popupAction":Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
    # getter for: Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->title:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->access$100(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    # getter for: Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->title:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->access$100(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    iget-boolean v3, v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mIsEnabled:Z

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 167
    # getter for: Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->shortcut:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->access$200(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    return-object p2
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    .line 174
    .local v0, "action":Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
    iget-boolean v1, v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mIsEnabled:Z

    return v1
.end method

.method public onSelect(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->onSelect()V

    .line 180
    return-void
.end method
