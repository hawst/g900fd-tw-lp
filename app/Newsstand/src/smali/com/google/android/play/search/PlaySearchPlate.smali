.class public Lcom/google/android/play/search/PlaySearchPlate;
.super Landroid/widget/FrameLayout;
.source "PlaySearchPlate.java"

# interfaces
.implements Lcom/google/android/play/search/PlaySearchActionButton$Listener;
.implements Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;
.implements Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;


# instance fields
.field private mActionButton:Lcom/google/android/play/search/PlaySearchActionButton;

.field private mNavButton:Lcom/google/android/play/search/PlaySearchNavigationButton;

.field private mSearchPlateTextContainer:Lcom/google/android/play/search/PlaySearchPlateTextContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method private switchToMode(I)V
    .locals 1
    .param p1, "searchMode"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mNavButton:Lcom/google/android/play/search/PlaySearchNavigationButton;

    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->switchToMode(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mSearchPlateTextContainer:Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->switchToMode(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mActionButton:Lcom/google/android/play/search/PlaySearchActionButton;

    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchActionButton;->switchToMode(I)V

    .line 81
    return-void
.end method


# virtual methods
.method public onBackArrowClicked()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->switchToMode(I)V

    .line 70
    return-void
.end method

.method public onClearClicked()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->switchToMode(I)V

    .line 60
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/google/android/play/R$id;->navigation_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchNavigationButton;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mNavButton:Lcom/google/android/play/search/PlaySearchNavigationButton;

    .line 38
    sget v0, Lcom/google/android/play/R$id;->text_container:I

    .line 39
    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mSearchPlateTextContainer:Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    .line 40
    sget v0, Lcom/google/android/play/R$id;->action_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchActionButton;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mActionButton:Lcom/google/android/play/search/PlaySearchActionButton;

    .line 42
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mNavButton:Lcom/google/android/play/search/PlaySearchNavigationButton;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->setListener(Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mSearchPlateTextContainer:Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->setListener(Lcom/google/android/play/search/PlaySearchPlateTextContainer$Listener;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchPlate;->mActionButton:Lcom/google/android/play/search/PlaySearchActionButton;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/PlaySearchActionButton;->setListener(Lcom/google/android/play/search/PlaySearchActionButton$Listener;)V

    .line 45
    return-void
.end method

.method public onHamburgerClicked()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public onIdleTextClicked()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->switchToMode(I)V

    .line 50
    return-void
.end method

.method public onMicClicked()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 55
    return-void
.end method
