.class public final Lcom/google/android/play/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_list_button_divider:I = 0x7f0e02ba

.field public static final account_name:I = 0x7f0e02b1

.field public static final action_bar:I = 0x7f0e01f2

.field public static final action_bar_container:I = 0x7f0e01f1

.field public static final action_button:I = 0x7f0e02d9

.field public static final action_text:I = 0x7f0e02b3

.field public static final alt_play_background:I = 0x7f0e02bd

.field public static final avatar:I = 0x7f0e02b0

.field public static final background_container:I = 0x7f0e02bc

.field public static final caption:I = 0x7f0e0278

.field public static final content_container:I = 0x7f0e02be

.field public static final controls_container:I = 0x7f0e02bf

.field public static final cover_photo:I = 0x7f0e02b8

.field public static final display_name:I = 0x7f0e02bb

.field public static final end_button:I = 0x7f0e02d1

.field public static final header_shadow:I = 0x7f0e02c6

.field public static final hero_container:I = 0x7f0e02c0

.field public static final li_badge:I = 0x7f0e02ad

.field public static final li_description:I = 0x7f0e02aa

.field public static final li_overflow:I = 0x7f0e02a1

.field public static final li_price:I = 0x7f0e02a6

.field public static final li_rating:I = 0x7f0e02a5

.field public static final li_reason:I = 0x7f0e02af

.field public static final li_reason_1:I = 0x7f0e02a9

.field public static final li_reason_2:I = 0x7f0e02a8

.field public static final li_subtitle:I = 0x7f0e02a7

.field public static final li_thumbnail:I = 0x7f0e02a3

.field public static final li_thumbnail_frame:I = 0x7f0e02a2

.field public static final li_thumbnail_reason:I = 0x7f0e02ae

.field public static final li_title:I = 0x7f0e02a4

.field public static final loading_progress_bar:I = 0x7f0e02ab

.field public static final navigation_button:I = 0x7f0e02d6

.field public static final page_indicator:I = 0x7f0e02d0

.field public static final pager_tab_strip:I = 0x7f0e02c2

.field public static final play_drawer_list:I = 0x7f0e02b2

.field public static final play_header_banner:I = 0x7f0e02c8

.field public static final play_header_list_tab_container:I = 0x7f0e02c4

.field public static final play_header_list_tab_scroll:I = 0x7f0e02c3

.field public static final play_header_listview:I = 0x7f0e0006

.field public static final play_header_spacer:I = 0x7f0e0008

.field public static final play_header_toolbar:I = 0x7f0e02c7

.field public static final play_header_viewpager:I = 0x7f0e0007

.field public static final play_onboard__OnboardPage_pageId:I = 0x7f0e000a

.field public static final play_onboard__OnboardPage_pageInfo:I = 0x7f0e000b

.field public static final play_onboard__OnboardPagerAdapter_pageGenerator:I = 0x7f0e0009

.field public static final play_onboard__OnboardSimpleQuizPage_title:I = 0x7f0e0010

.field public static final play_onboard__OnboardTutorialPage_backgroundColor:I = 0x7f0e000c

.field public static final play_onboard__OnboardTutorialPage_bodyText:I = 0x7f0e000e

.field public static final play_onboard__OnboardTutorialPage_iconDrawableId:I = 0x7f0e000f

.field public static final play_onboard__OnboardTutorialPage_titleText:I = 0x7f0e000d

.field public static final play_onboard_background:I = 0x7f0e0011

.field public static final play_onboard_drops:I = 0x7f0e02ce

.field public static final play_onboard_footer:I = 0x7f0e02cd

.field public static final play_onboard_overlay:I = 0x7f0e0012

.field public static final play_onboard_pager:I = 0x7f0e02cc

.field public static final rating_badge_container:I = 0x7f0e02ac

.field public static final scroll_proxy:I = 0x7f0e02cb

.field public static final search_box_idle_text:I = 0x7f0e02d7

.field public static final search_box_text_input:I = 0x7f0e02d8

.field public static final secondary_profiles:I = 0x7f0e02b7

.field public static final selected_profile:I = 0x7f0e02b6

.field public static final shortcut:I = 0x7f0e01eb

.field public static final splash:I = 0x7f0e02d2

.field public static final start_button:I = 0x7f0e02cf

.field public static final swipe_refresh_layout:I = 0x7f0e02ca

.field public static final swipe_refresh_layout_parent:I = 0x7f0e02c9

.field public static final switch_button:I = 0x7f0e02b4

.field public static final tab_bar:I = 0x7f0e02c1

.field public static final tab_bar_title:I = 0x7f0e02c5

.field public static final text_container:I = 0x7f0e0239

.field public static final title:I = 0x7f0e01e8

.field public static final toggle_account_list_button:I = 0x7f0e02b9
