.class public Lcom/google/android/play/widget/RaindropMaskDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "RaindropMaskDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    }
.end annotation


# instance fields
.field protected final mDropCount:I

.field protected final mDrops:[Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

.field protected final mDurationMs:J

.field protected mErasingPaint:Landroid/graphics/Paint;

.field protected final mInterpolator:Landroid/view/animation/Interpolator;

.field protected mIsAnimating:Z

.field protected mMaskColor:I

.field protected mStartTimeMs:J

.field private mStoppedRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(IIJ)V
    .locals 3
    .param p1, "color"    # I
    .param p2, "dropCount"    # I
    .param p3, "durationMs"    # J

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    .line 54
    iput p2, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDropCount:I

    .line 55
    iput-wide p3, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDurationMs:J

    .line 56
    new-array v0, p2, [Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

    iput-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDrops:[Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->makeInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mErasingPaint:Landroid/graphics/Paint;

    .line 61
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mErasingPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 62
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mErasingPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mErasingPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 65
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    .line 109
    iget-wide v2, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStartTimeMs:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 111
    iget v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 134
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStartTimeMs:J

    sub-long v10, v2, v4

    .line 116
    .local v10, "elapsedMs":J
    iget-wide v2, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDurationMs:J

    cmp-long v0, v10, v2

    if-ltz v0, :cond_1

    const/4 v7, 0x1

    .line 119
    .local v7, "done":Z
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    const/16 v5, 0xff

    const/16 v6, 0x1f

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayerAlpha(FFFFII)I

    .line 122
    iget v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDrops:[Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

    array-length v1, v0

    :goto_2
    if-ge v9, v1, :cond_2

    aget-object v8, v0, v9

    .line 124
    .local v8, "drop":Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    invoke-virtual {p0, p1, v8, v10, v11}, Lcom/google/android/play/widget/RaindropMaskDrawable;->drawDrop(Landroid/graphics/Canvas;Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;J)V

    .line 123
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .end local v7    # "done":Z
    .end local v8    # "drop":Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    :cond_1
    move v7, v9

    .line 116
    goto :goto_1

    .line 126
    .restart local v7    # "done":Z
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 128
    if-eqz v7, :cond_3

    .line 129
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->stop()V

    goto :goto_0

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method protected drawDrop(Landroid/graphics/Canvas;Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;J)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "drop"    # Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    .param p3, "elapsedMs"    # J

    .prologue
    .line 143
    long-to-float v4, p3

    iget-wide v6, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDurationMs:J

    long-to-float v5, v6

    div-float v2, v4, v5

    .line 144
    .local v2, "progress":F
    iget-object v4, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v4, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 145
    .local v1, "interpolated":F
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 146
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    mul-float v3, v1, v4

    .line 147
    .local v3, "radius":F
    iget v4, p2, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;->x:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget v5, p2, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;->y:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mErasingPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v3, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 148
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 168
    const/4 v0, -0x3

    return v0
.end method

.method protected initializeDrops()V
    .locals 10

    .prologue
    const/high16 v9, 0x3fc00000    # 1.5f

    const/high16 v8, 0x3e800000    # 0.25f

    .line 95
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 96
    .local v2, "r":Ljava/util/Random;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDropCount:I

    if-ge v1, v3, :cond_0

    .line 97
    new-instance v0, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

    invoke-direct {v0}, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;-><init>()V

    .line 99
    .local v0, "drop":Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v3

    mul-float/2addr v3, v9

    sub-float/2addr v3, v8

    iput v3, v0, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;->x:F

    .line 100
    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v3

    mul-float/2addr v3, v9

    sub-float/2addr v3, v8

    iput v3, v0, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;->y:F

    .line 101
    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDurationMs:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iput-wide v4, v0, Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;->startOffsetMs:J

    .line 102
    iget-object v3, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mDrops:[Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;

    aput-object v0, v3, v1

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "drop":Lcom/google/android/play/widget/RaindropMaskDrawable$Drop;
    :cond_0
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mIsAnimating:Z

    return v0
.end method

.method protected makeInterpolator()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    return-object v0
.end method

.method public setAlpha(I)V
    .locals 3
    .param p1, "alpha"    # I

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    .line 157
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    .line 158
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 157
    invoke-static {p1, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mMaskColor:I

    .line 159
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 163
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setStoppedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStoppedRunnable:Ljava/lang/Runnable;

    .line 72
    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    iget-wide v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStartTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStartTimeMs:J

    .line 79
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->initializeDrops()V

    .line 81
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mIsAnimating:Z

    .line 82
    invoke-virtual {p0}, Lcom/google/android/play/widget/RaindropMaskDrawable;->invalidateSelf()V

    .line 84
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mIsAnimating:Z

    .line 89
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStoppedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/play/widget/RaindropMaskDrawable;->mStoppedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 92
    :cond_0
    return-void
.end method
