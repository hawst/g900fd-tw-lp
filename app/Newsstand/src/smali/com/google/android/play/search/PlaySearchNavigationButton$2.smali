.class Lcom/google/android/play/search/PlaySearchNavigationButton$2;
.super Ljava/lang/Object;
.source "PlaySearchNavigationButton.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/search/PlaySearchNavigationButton;->changeModeAnimated(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;


# direct methods
.method constructor <init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/search/PlaySearchNavigationButton;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$2;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$2;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mArrowOrBurgerDrawable:Lcom/google/android/play/search/ArrowOrBurgerDrawable;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$200(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/ArrowOrBurgerDrawable;

    move-result-object v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->setHowArrowIsTheBurger(F)V

    .line 144
    return-void
.end method
