.class Lcom/google/android/play/drawer/PlayDrawerAdapter$1;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field final synthetic val$finalCurrentAccountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iput-object p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V
    .locals 6
    .param p1, "plusProfileResponse"    # Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .prologue
    .line 326
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 327
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 330
    .local v1, "prevDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v0, p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 331
    .local v0, "newDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 337
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 338
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # invokes: Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$300(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 340
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$500(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 341
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 342
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;
    invoke-static {v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$400(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/image/BitmapLoader;

    move-result-object v5

    .line 340
    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configure(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 350
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 323
    check-cast p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V

    return-void
.end method
