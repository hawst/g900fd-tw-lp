.class Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;
.super Ljava/lang/Object;
.source "PlayHeaderListTabStrip.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->makeOnTabClickListener(I)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

.field final synthetic val$pagerPosition:I


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->val$pagerPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->val$pagerPosition:I

    invoke-interface {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;->onBeforeTabSelected(I)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mEnablePagerScrollSync:Z
    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$502(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Z)Z

    .line 274
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->val$pagerPosition:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateSelectedTab(IZ)V

    .line 275
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->val$pagerPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 277
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mOnTabSelectedListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;
    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$400(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$2;->val$pagerPosition:I

    invoke-interface {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;->onTabSelected(I)V

    .line 280
    :cond_1
    return-void
.end method
