.class public final Lcom/google/android/play/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final play_avatar_focused_outline:I = 0x7f09004c

.field public static final play_avatar_outline:I = 0x7f090049

.field public static final play_avatar_pressed_fill:I = 0x7f09004a

.field public static final play_avatar_pressed_outline:I = 0x7f09004b

.field public static final play_card_shadow_end_color:I = 0x7f090056

.field public static final play_card_shadow_start_color:I = 0x7f090055

.field public static final play_default_download_arc_color_offline:I = 0x7f09004d

.field public static final play_disabled_grey:I = 0x7f09001b

.field public static final play_dismissed_overlay:I = 0x7f090043

.field public static final play_fg_primary:I = 0x7f090025

.field public static final play_fg_secondary:I = 0x7f090026

.field public static final play_header_list_tab_text_color:I = 0x7f090141

.field public static final play_header_list_tab_underline_color:I = 0x7f09004e

.field public static final play_main_background:I = 0x7f09001f

.field public static final play_onboard__page_indicator_dot_active:I = 0x7f09005b

.field public static final play_onboard__page_indicator_dot_inactive:I = 0x7f09005c

.field public static final play_onboard_accent_color_a:I = 0x7f090061

.field public static final play_onboard_accent_color_b:I = 0x7f090062

.field public static final play_onboard_accent_color_c:I = 0x7f090063

.field public static final play_onboard_accent_color_d:I = 0x7f090064

.field public static final play_onboard_app_color:I = 0x7f090059

.field public static final play_onboard_app_color_dark:I = 0x7f09005a

.field public static final play_onboard_simple_quiz_background:I = 0x7f090060

.field public static final play_reason_separator:I = 0x7f090020

.field public static final play_search_plate_navigation_button_color:I = 0x7f090058

.field public static final play_tab_strip_text_selected:I = 0x7f090024

.field public static final play_transparent:I = 0x7f09001c

.field public static final play_white:I = 0x7f090017
