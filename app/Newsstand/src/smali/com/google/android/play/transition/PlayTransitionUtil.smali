.class public Lcom/google/android/play/transition/PlayTransitionUtil;
.super Ljava/lang/Object;
.source "PlayTransitionUtil.java"


# direct methods
.method public static varargs aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;
    .locals 4
    .param p0, "transitions"    # [Landroid/transition/Transition;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 56
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 57
    .local v0, "set":Landroid/transition/TransitionSet;
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    .line 58
    .local v1, "transition":Landroid/transition/Transition;
    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 60
    .end local v1    # "transition":Landroid/transition/Transition;
    :cond_0
    return-object v0
.end method

.method public static centerViewOnRect(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "target"    # Landroid/graphics/Rect;

    .prologue
    .line 78
    const/4 v5, 0x2

    new-array v0, v5, [I

    .line 79
    .local v0, "locInWindow":[I
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 80
    const/4 v5, 0x0

    aget v5, v0, v5

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int v3, v5, v6

    .line 81
    .local v3, "windowOffsetX":I
    const/4 v5, 0x1

    aget v5, v0, v5

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v4, v5, v6

    .line 83
    .local v4, "windowOffsetY":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    sub-int v1, v5, v3

    .line 84
    .local v1, "startX":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    sub-int v2, v5, v4

    .line 85
    .local v2, "startY":I
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Landroid/view/View;->setRight(I)V

    .line 86
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {p0, v5}, Landroid/view/View;->setBottom(I)V

    .line 87
    invoke-virtual {p0, v1}, Landroid/view/View;->setLeft(I)V

    .line 88
    invoke-virtual {p0, v2}, Landroid/view/View;->setTop(I)V

    .line 89
    return-void
.end method

.method public static setHeaderListLayoutClipChildren(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)V
    .locals 6
    .param p0, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p1, "clipChildren"    # Z

    .prologue
    const/4 v3, 0x0

    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setClipChildren(Z)V

    .line 39
    const/4 v4, 0x4

    new-array v1, v4, [I

    sget v4, Lcom/google/android/play/R$id;->hero_container:I

    aput v4, v1, v3

    const/4 v4, 0x1

    sget v5, Lcom/google/android/play/R$id;->controls_container:I

    aput v5, v1, v4

    const/4 v4, 0x2

    sget v5, Lcom/google/android/play/R$id;->background_container:I

    aput v5, v1, v4

    const/4 v4, 0x3

    sget v5, Lcom/google/android/play/R$id;->alt_play_background:I

    aput v5, v1, v4

    .line 45
    .local v1, "headerListLayoutViewGroupIds":[I
    array-length v4, v1

    :goto_0
    if-ge v3, v4, :cond_0

    aget v2, v1, v3

    .line 46
    .local v2, "id":I
    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 47
    .local v0, "container":Landroid/view/ViewGroup;
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 45
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 49
    .end local v0    # "container":Landroid/view/ViewGroup;
    .end local v2    # "id":I
    :cond_0
    return-void
.end method

.method public static viewBounds(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 68
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
