.class public Lcom/google/android/play/layout/PlayTabStrip;
.super Landroid/widget/LinearLayout;
.source "PlayTabStrip.java"


# instance fields
.field private mIndexForSelection:I

.field private final mSelectedTextColor:I

.field private final mSelectedUnderlinePaint:Landroid/graphics/Paint;

.field private final mSelectedUnderlineThickness:I

.field private mSelectionOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabStrip;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/PlayTabStrip;->setWillNotDraw(Z)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 43
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_tab_strip_selected_underline_height:I

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedUnderlineThickness:I

    .line 45
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    .line 47
    sget v1, Lcom/google/android/play/R$color;->play_tab_strip_text_selected:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedTextColor:I

    .line 48
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 97
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getHeight()I

    move-result v7

    .line 98
    .local v7, "height":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v6

    .line 101
    .local v6, "childCount":I
    if-lez v6, :cond_1

    .line 102
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 103
    .local v13, "selectedTitle":Landroid/view/View;
    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v11

    .line 104
    .local v11, "selectedLeft":I
    invoke-virtual {v13}, Landroid/view/View;->getRight()I

    move-result v12

    .line 105
    .local v12, "selectedRight":I
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    .line 106
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 108
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 109
    .local v10, "nextTitle":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v8

    .line 110
    .local v8, "nextLeft":I
    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v9

    .line 112
    .local v9, "nextRight":I
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    int-to-float v1, v8

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    sub-float v1, v3, v1

    int-to-float v2, v11

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v11, v0

    .line 114
    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    int-to-float v1, v9

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    sub-float v1, v3, v1

    int-to-float v2, v12

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v12, v0

    .line 118
    .end local v8    # "nextLeft":I
    .end local v9    # "nextRight":I
    .end local v10    # "nextTitle":Landroid/view/View;
    :cond_0
    int-to-float v1, v11

    iget v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedUnderlineThickness:I

    sub-int v0, v7, v0

    int-to-float v2, v0

    int-to-float v3, v12

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedUnderlinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 121
    .end local v11    # "selectedLeft":I
    .end local v12    # "selectedRight":I
    .end local v13    # "selectedTitle":Landroid/view/View;
    :cond_1
    return-void
.end method

.method onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    .line 65
    iput p2, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    .line 66
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->syncTitleColor()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->invalidate()V

    .line 68
    return-void
.end method

.method onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    .line 77
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->syncTitleColor()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->invalidate()V

    .line 79
    return-void
.end method

.method syncTitleColor()V
    .locals 10

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v1

    .line 83
    .local v1, "childCount":I
    iget v7, p0, Lcom/google/android/play/layout/PlayTabStrip;->mIndexForSelection:I

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectionOffset:F

    add-float v4, v7, v8

    .line 84
    .local v4, "selectedLocation":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 85
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    .local v0, "child":Landroid/widget/TextView;
    int-to-float v7, v3

    sub-float/2addr v7, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 87
    .local v2, "distanceFromSelected":F
    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v2, v7

    if-ltz v7, :cond_0

    const/16 v6, 0x99

    .line 90
    .local v6, "textColorAlpha":I
    :goto_1
    shl-int/lit8 v7, v6, 0x18

    iget v8, p0, Lcom/google/android/play/layout/PlayTabStrip;->mSelectedTextColor:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    or-int v5, v7, v8

    .line 91
    .local v5, "textColor":I
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 87
    .end local v5    # "textColor":I
    .end local v6    # "textColorAlpha":I
    :cond_0
    const/high16 v7, 0x437f0000    # 255.0f

    const/high16 v8, 0x42cc0000    # 102.0f

    mul-float/2addr v8, v2

    sub-float/2addr v7, v8

    float-to-int v6, v7

    goto :goto_1

    .line 93
    .end local v0    # "child":Landroid/widget/TextView;
    .end local v2    # "distanceFromSelected":F
    :cond_1
    return-void
.end method
