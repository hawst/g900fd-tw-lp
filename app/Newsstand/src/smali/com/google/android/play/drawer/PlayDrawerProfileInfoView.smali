.class Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayDrawerProfileInfoView.java"

# interfaces
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# instance fields
.field private mAccountListButtonDivider:Landroid/view/View;

.field private mAccountListEnabled:Z

.field private mAccountListExpanded:Z

.field private mAccountName:Landroid/widget/TextView;

.field private mDisplayName:Landroid/widget/TextView;

.field private mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mToggleAccountListButton:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setWillNotDraw(Z)V

    .line 59
    return-void
.end method

.method private bindAccountToggler()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 146
    .local v0, "res":Landroid/content/res/Resources;
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    if-eqz v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListButtonDivider:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 148
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 149
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/play/R$drawable;->ic_up_white_16:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 150
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/play/R$string;->play_content_description_hide_account_list_button:I

    .line 151
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 150
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    :goto_0
    return-void

    .line 153
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListButtonDivider:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 156
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/play/R$drawable;->ic_down_white_16:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 157
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/play/R$string;->play_content_description_show_account_list_button:I

    .line 158
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 157
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 160
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListButtonDivider:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 161
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 7
    .param p1, "profileDocV2"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p2, "currentAccountName"    # Ljava/lang/String;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 100
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    sget v5, Lcom/google/android/play/R$color;->play_main_background:I

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    if-nez p1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/play/R$drawable;->ic_profile_none:I

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 109
    .local v0, "avatarBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v4

    .line 110
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 111
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4, v0}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 112
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    sget v5, Lcom/google/android/play/R$drawable;->bg_default_profile_art:I

    invoke-virtual {v4, v5}, Lcom/google/android/play/image/FifeImageView;->setImageResource(I)V

    .line 114
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    .end local v0    # "avatarBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->clearCachedState()V

    .line 124
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->clearCachedState()V

    .line 126
    const/16 v4, 0xf

    .line 127
    invoke-static {p1, v4}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v2

    .line 128
    .local v2, "selectedProfileCoverImage":Lcom/google/android/finsky/protos/Common$Image;
    const/4 v4, 0x4

    .line 129
    invoke-static {p1, v4}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    .line 130
    .local v1, "selectedProfileAvatarImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v3, p1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    .line 132
    .local v3, "selectedProfileDisplayName":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4, p0}, Lcom/google/android/play/image/FifeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 133
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v5, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v6, v2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v4, v5, v6, p3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 135
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v5, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v6, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v4, v5, v6, p3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 137
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 138
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_1
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->drawableStateChanged()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->invalidate()V

    .line 174
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 65
    sget v0, Lcom/google/android/play/R$id;->cover_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 66
    sget v0, Lcom/google/android/play/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    .line 67
    sget v0, Lcom/google/android/play/R$id;->display_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mDisplayName:Landroid/widget/TextView;

    .line 68
    sget v0, Lcom/google/android/play/R$id;->account_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountName:Landroid/widget/TextView;

    .line 69
    sget v0, Lcom/google/android/play/R$id;->account_list_button_divider:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListButtonDivider:Landroid/view/View;

    .line 70
    sget v0, Lcom/google/android/play/R$id;->toggle_account_list_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    .line 71
    return-void
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "coverImageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 178
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 1
    .param p1, "coverImageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 184
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 185
    return-void
.end method

.method public setAccountListEnabled(Z)V
    .locals 1
    .param p1, "accountListEnabled"    # Z

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    if-eq v0, p1, :cond_0

    .line 75
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListEnabled:Z

    .line 76
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->bindAccountToggler()V

    .line 77
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 83
    :cond_0
    return-void
.end method

.method public setAccountListExpanded(Z)V
    .locals 1
    .param p1, "accountListExpanded"    # Z

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    if-eq v0, p1, :cond_0

    .line 87
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mAccountListExpanded:Z

    .line 88
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->bindAccountToggler()V

    .line 90
    :cond_0
    return-void
.end method

.method public setAccountTogglerListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "accountTogglerListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->mToggleAccountListButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    return-void
.end method
