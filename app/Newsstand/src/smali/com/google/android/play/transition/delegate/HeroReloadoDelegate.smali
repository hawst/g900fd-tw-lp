.class public abstract Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;
.super Lcom/google/android/play/transition/delegate/TransitionDelegate;
.source "HeroReloadoDelegate.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/play/transition/delegate/TransitionDelegate",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    invoke-direct {p0}, Lcom/google/android/play/transition/delegate/TransitionDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method protected cardClassTypes(Ljava/lang/Object;)[Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Class;"
        }
    .end annotation

    .prologue
    .line 135
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createEnterTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 41
    .line 42
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    .line 43
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->cardClassTypes(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    .line 41
    invoke-static {v0, v1, v2}, Lcom/google/android/play/transition/HeroReloadoHelper;->enterTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected createReturnTransition(Ljava/lang/Object;)Landroid/transition/Transition;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/transition/Transition;"
        }
    .end annotation

    .prologue
    .line 49
    .line 50
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->cardClassTypes(Ljava/lang/Object;)[Ljava/lang/Class;

    move-result-object v1

    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    .line 49
    invoke-static {v0, v1, v2}, Lcom/google/android/play/transition/HeroReloadoHelper;->returnTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method protected getDecorFadeDurationMs()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    const-wide/16 v0, 0x15e

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->splashView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "splashView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method

.method protected handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    .local p2, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p4, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->handleEnterSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 75
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 77
    const/4 v3, 0x0

    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 81
    .local v0, "heroSnapshot":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->splashView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v3

    .line 82
    invoke-static {v0}, Lcom/google/android/play/transition/PlayTransitionUtil;->viewBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 80
    invoke-static {v3, v4}, Lcom/google/android/play/transition/PlayTransitionUtil;->centerViewOnRect(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 84
    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v3

    .line 87
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->sharedElementView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    .line 88
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->splashView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v5

    .line 89
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->maintainsHeroAspectRatio()Z

    move-result v6

    .line 85
    invoke-static {v3, v4, v5, v6}, Lcom/google/android/play/transition/HeroReloadoHelper;->sharedElementEnterTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/transition/TransitionSet;

    move-result-object v1

    .line 90
    .local v1, "sharedElementEnter":Landroid/transition/Transition;
    invoke-virtual {p0, p1, v1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->setSharedElementEnterTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 93
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v3

    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->sharedElementView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v4

    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->splashView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v5

    .line 96
    invoke-virtual {p0}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->maintainsHeroAspectRatio()Z

    move-result v6

    .line 92
    invoke-static {v3, v4, v5, v6}, Lcom/google/android/play/transition/HeroReloadoHelper;->sharedElementReturnTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/transition/TransitionSet;

    move-result-object v2

    .line 97
    .local v2, "sharedElementReturn":Landroid/transition/Transition;
    invoke-virtual {p0, p1, v2}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->setSharedElementReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 103
    .end local v0    # "heroSnapshot":Landroid/view/View;
    .end local v1    # "sharedElementEnter":Landroid/transition/Transition;
    .end local v2    # "sharedElementReturn":Landroid/transition/Transition;
    :cond_0
    :goto_0
    return-void

    .line 99
    .restart local v0    # "heroSnapshot":Landroid/view/View;
    :cond_1
    const-string v3, "HeroReloadoTransitionDelegate"

    const-string v4, "Unable to set transitions; missing hero snapshot. Check that the hero is visible in the launching activity."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abstract headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/android/play/headerlist/PlayHeaderListLayout;"
        }
    .end annotation
.end method

.method protected maintainsHeroAspectRatio()Z
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected prepareViewsForTransition(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->splashView(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 36
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->getActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$string;->header_list_layout_background:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method protected abstract sharedElementView(Ljava/lang/Object;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/view/View;"
        }
    .end annotation
.end method

.method protected splashView(Ljava/lang/Object;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 125
    .line 126
    .local p0, "this":Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;, "Lcom/google/android/play/transition/delegate/HeroReloadoDelegate<TT;>;"
    .local p1, "controller":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/delegate/HeroReloadoDelegate;->headerListLayout(Ljava/lang/Object;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$id;->background_container:I

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 127
    .local v0, "backgroundContainer":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
