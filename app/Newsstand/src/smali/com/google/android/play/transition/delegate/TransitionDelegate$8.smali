.class Lcom/google/android/play/transition/delegate/TransitionDelegate$8;
.super Landroid/app/SharedElementCallback;
.source "TransitionDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/transition/delegate/TransitionDelegate;->setupSharedElementCallbacks(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

.field final synthetic val$controller:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/transition/delegate/TransitionDelegate;

    .prologue
    .line 481
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$8;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$8;"
    iput-object p1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iput-object p2, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->val$controller:Ljava/lang/Object;

    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 497
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$8;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$8;"
    .local p1, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iget-object v1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->val$controller:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->handleExitRemapSharedElements(Ljava/lang/Object;Ljava/util/List;Ljava/util/Map;)V

    .line 498
    return-void
.end method

.method public onRejectSharedElements(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$8;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$8;"
    .local p1, "rejectedSharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iget-object v1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->val$controller:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->handleExitRejectedSharedElements(Ljava/lang/Object;Ljava/util/List;)V

    .line 493
    return-void
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$8;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$8;"
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iget-object v1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$8;->val$controller:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->handleExitSetSharedElementStart(Ljava/lang/Object;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 488
    return-void
.end method
