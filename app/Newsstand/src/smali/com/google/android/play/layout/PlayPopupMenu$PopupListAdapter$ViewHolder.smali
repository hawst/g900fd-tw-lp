.class Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "PlayPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private shortcut:Landroid/widget/TextView;

.field private title:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/play/layout/PlayPopupMenu$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu$1;

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->shortcut:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter$ViewHolder;->shortcut:Landroid/widget/TextView;

    return-object p1
.end method
