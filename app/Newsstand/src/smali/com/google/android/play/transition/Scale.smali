.class public Lcom/google/android/play/transition/Scale;
.super Landroid/transition/Transition;
.source "Scale.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final sTransitionProperties:[Ljava/lang/String;


# instance fields
.field private final mIsEntering:Z

.field private mMaintainAspectRatio:Z

.field private mOriginatingViewInset:I

.field private mTempLocation:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "play:scale:bounds"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "play:scale:windowX"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "play:scale:windowY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/play/transition/Scale;->sTransitionProperties:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isEntering"    # Z

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/play/transition/Scale;->mTempLocation:[I

    .line 45
    iput-boolean p1, p0, Lcom/google/android/play/transition/Scale;->mIsEntering:Z

    .line 46
    return-void
.end method

.method private captureValues(Landroid/transition/TransitionValues;)V
    .locals 6
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    .line 73
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 74
    .local v1, "view":Landroid/view/View;
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    .line 75
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 76
    .local v0, "outerBounds":Landroid/graphics/Rect;
    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "play:scale:bounds"

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v2, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/play/transition/Scale;->mTempLocation:[I

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 78
    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "play:scale:windowX"

    iget-object v4, p0, Lcom/google/android/play/transition/Scale;->mTempLocation:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "play:scale:windowY"

    iget-object v4, p0, Lcom/google/android/play/transition/Scale;->mTempLocation:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method protected static matchAspectRatio(FLandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 8
    .param p0, "aspectRatio"    # F
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 170
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 171
    .local v0, "adjustedBounds":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 172
    .local v1, "currentAspectRatio":F
    cmpl-float v4, v1, p0

    if-eqz v4, :cond_0

    .line 174
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 175
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p0

    sub-float/2addr v4, v5

    div-float/2addr v4, v6

    float-to-int v3, v4

    .line 176
    .local v3, "widthDiff":I
    iget v4, p1, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v3

    iget v5, p1, Landroid/graphics/Rect;->top:I

    iget v6, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v3

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    .line 177
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 187
    .end local v3    # "widthDiff":I
    :cond_0
    :goto_0
    return-object v0

    .line 180
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p0

    float-to-int v5, v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v6

    float-to-int v2, v4

    .line 182
    .local v2, "heightDiff":I
    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v2

    iget v6, p1, Landroid/graphics/Rect;->right:I

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v2

    .line 183
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/Scale;->captureValues(Landroid/transition/TransitionValues;)V

    .line 85
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/Scale;->captureValues(Landroid/transition/TransitionValues;)V

    .line 90
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 95
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 96
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/play/transition/Scale;->getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public forceMaintainAspectRatio(Z)Lcom/google/android/play/transition/Scale;
    .locals 0
    .param p1, "maintainAspectRatio"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/play/transition/Scale;->mMaintainAspectRatio:Z

    .line 64
    return-object p0
.end method

.method protected getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 19
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 105
    move-object/from16 v0, p2

    iget-object v14, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v15, "play:scale:bounds"

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/Rect;

    .line 106
    .local v11, "startBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    iget-object v14, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v15, "play:scale:bounds"

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    .line 109
    .local v3, "endBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/play/transition/Scale;->mIsEntering:Z

    if-eqz v14, :cond_2

    .line 110
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    invoke-virtual {v11, v14, v15}, Landroid/graphics/Rect;->inset(II)V

    .line 111
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/play/transition/Scale;->mMaintainAspectRatio:Z

    if-eqz v14, :cond_0

    .line 112
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v15

    int-to-float v15, v15

    div-float/2addr v14, v15

    invoke-static {v14, v11}, Lcom/google/android/play/transition/Scale;->matchAspectRatio(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v11

    .line 123
    :cond_0
    :goto_0
    move-object/from16 v0, p3

    iget-object v13, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 126
    .local v13, "view":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/transition/Scale;->getPathMotion()Landroid/transition/PathMotion;

    move-result-object v14

    iget v15, v11, Landroid/graphics/Rect;->left:I

    int-to-float v15, v15

    iget v0, v11, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    iget v0, v3, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    iget v0, v3, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    .line 127
    invoke-virtual/range {v14 .. v18}, Landroid/transition/PathMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v4

    .line 128
    .local v4, "path":Landroid/graphics/Path;
    sget-object v14, Landroid/view/View;->X:Landroid/util/Property;

    sget-object v15, Landroid/view/View;->Y:Landroid/util/Property;

    invoke-static {v13, v14, v15, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;Landroid/util/Property;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 131
    .local v12, "topLeftAnimator":Landroid/animation/ObjectAnimator;
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setPivotX(F)V

    .line 132
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setPivotY(F)V

    .line 135
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v15

    int-to-float v15, v15

    div-float v7, v14, v15

    .line 136
    .local v7, "scaleXStart":F
    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v15

    int-to-float v15, v15

    div-float v10, v14, v15

    .line 137
    .local v10, "scaleYStart":F
    const/high16 v6, 0x3f800000    # 1.0f

    .line 138
    .local v6, "scaleXEnd":F
    const/high16 v9, 0x3f800000    # 1.0f

    .line 141
    .local v9, "scaleYEnd":F
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/play/transition/Scale;->mIsEntering:Z

    if-nez v14, :cond_1

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    if-eqz v14, :cond_1

    .line 143
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    mul-int/lit8 v15, v15, 0x2

    sub-int/2addr v14, v15

    int-to-float v14, v14

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v15

    int-to-float v15, v15

    div-float v6, v14, v15

    .line 145
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    mul-int/lit8 v15, v15, 0x2

    sub-int/2addr v14, v15

    int-to-float v14, v14

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v15

    int-to-float v15, v15

    div-float v9, v14, v15

    .line 149
    :cond_1
    iget v14, v11, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setX(F)V

    .line 150
    iget v14, v11, Landroid/graphics/Rect;->top:I

    int-to-float v14, v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setY(F)V

    .line 151
    invoke-virtual {v13, v7}, Landroid/view/View;->setScaleX(F)V

    .line 152
    invoke-virtual {v13, v10}, Landroid/view/View;->setScaleY(F)V

    .line 154
    const-string v14, "scaleX"

    const/4 v15, 0x2

    new-array v15, v15, [F

    const/16 v16, 0x0

    aput v7, v15, v16

    const/16 v16, 0x1

    aput v6, v15, v16

    invoke-static {v13, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 155
    .local v5, "scaleX":Landroid/animation/ObjectAnimator;
    const-string v14, "scaleY"

    const/4 v15, 0x2

    new-array v15, v15, [F

    const/16 v16, 0x0

    aput v10, v15, v16

    const/16 v16, 0x1

    aput v9, v15, v16

    invoke-static {v13, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 157
    .local v8, "scaleY":Landroid/animation/ObjectAnimator;
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 158
    .local v2, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v14, 0x3

    new-array v14, v14, [Landroid/animation/Animator;

    const/4 v15, 0x0

    aput-object v5, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    const/4 v15, 0x2

    aput-object v12, v14, v15

    invoke-virtual {v2, v14}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 159
    const-wide/16 v14, 0x190

    invoke-virtual {v2, v14, v15}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/transition/Scale;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 161
    return-object v2

    .line 116
    .end local v2    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v4    # "path":Landroid/graphics/Path;
    .end local v5    # "scaleX":Landroid/animation/ObjectAnimator;
    .end local v6    # "scaleXEnd":F
    .end local v7    # "scaleXStart":F
    .end local v8    # "scaleY":Landroid/animation/ObjectAnimator;
    .end local v9    # "scaleYEnd":F
    .end local v10    # "scaleYStart":F
    .end local v12    # "topLeftAnimator":Landroid/animation/ObjectAnimator;
    .end local v13    # "view":Landroid/view/View;
    :cond_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    invoke-virtual {v3, v14, v15}, Landroid/graphics/Rect;->inset(II)V

    .line 117
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/play/transition/Scale;->mMaintainAspectRatio:Z

    if-eqz v14, :cond_0

    .line 118
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v15

    int-to-float v15, v15

    div-float/2addr v14, v15

    invoke-static {v14, v3}, Lcom/google/android/play/transition/Scale;->matchAspectRatio(FLandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/google/android/play/transition/Scale;->sTransitionProperties:[Ljava/lang/String;

    return-object v0
.end method

.method public setOriginatingViewInset(I)Lcom/google/android/play/transition/Scale;
    .locals 0
    .param p1, "inset"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/google/android/play/transition/Scale;->mOriginatingViewInset:I

    .line 54
    return-object p0
.end method
