.class public Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "PlayHeaderListRecyclerViewListener.java"


# instance fields
.field private mAbsoluteY:I

.field private mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v7/widget/RecyclerView$Adapter",
            "<*>;"
        }
    .end annotation
.end field

.field private final mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

.field protected mScrollState:I


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1
    .param p1, "layout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    .line 27
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 28
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 60
    return-void
.end method

.method private reset(Z)V
    .locals 1
    .param p1, "resetAdapter"    # Z

    .prologue
    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    .line 76
    if-eqz p1, :cond_0

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->updateAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 79
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mScrollState:I

    .line 80
    return-void
.end method

.method private updateAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$Adapter",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;, "Landroid/support/v7/widget/RecyclerView$Adapter<*>;"
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-ne v0, p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->unregisterAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    .line 91
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 94
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->reset(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->updateAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mScrollState:I

    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScrollStateChanged(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V

    .line 106
    :cond_0
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->updateAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 111
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    .line 118
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mScrollState:I

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v2, p3, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->onScroll(III)V

    .line 119
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 122
    :cond_0
    return-void

    .line 116
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    goto :goto_0

    .line 118
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->mAbsoluteY:I

    goto :goto_1
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->reset(Z)V

    .line 68
    return-void
.end method
