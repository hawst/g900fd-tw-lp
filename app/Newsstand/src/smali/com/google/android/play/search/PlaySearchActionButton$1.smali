.class Lcom/google/android/play/search/PlaySearchActionButton$1;
.super Ljava/lang/Object;
.source "PlaySearchActionButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/search/PlaySearchActionButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/search/PlaySearchActionButton;


# direct methods
.method constructor <init>(Lcom/google/android/play/search/PlaySearchActionButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/search/PlaySearchActionButton;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    # getter for: Lcom/google/android/play/search/PlaySearchActionButton;->mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchActionButton;->access$000(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    # getter for: Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchActionButton;->access$100(Lcom/google/android/play/search/PlaySearchActionButton;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    # getter for: Lcom/google/android/play/search/PlaySearchActionButton;->mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchActionButton;->access$000(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/search/PlaySearchActionButton$Listener;->onClearClicked()V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    # getter for: Lcom/google/android/play/search/PlaySearchActionButton;->mCurrentMode:I
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchActionButton;->access$100(Lcom/google/android/play/search/PlaySearchActionButton;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton$1;->this$0:Lcom/google/android/play/search/PlaySearchActionButton;

    # getter for: Lcom/google/android/play/search/PlaySearchActionButton;->mListener:Lcom/google/android/play/search/PlaySearchActionButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchActionButton;->access$000(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/PlaySearchActionButton$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/search/PlaySearchActionButton$Listener;->onMicClicked()V

    goto :goto_0
.end method
