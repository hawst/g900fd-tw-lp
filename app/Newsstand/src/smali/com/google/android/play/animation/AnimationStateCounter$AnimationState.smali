.class Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;
.super Ljava/lang/Object;
.source "AnimationStateCounter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/animation/AnimationStateCounter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationState"
.end annotation


# instance fields
.field public mAnimatedChildCount:I

.field public final mOriginalClipChildrenValue:Z


# direct methods
.method private constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getClipChildren()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;->mOriginalClipChildrenValue:Z

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/ViewGroup;Lcom/google/android/play/animation/AnimationStateCounter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # Lcom/google/android/play/animation/AnimationStateCounter$1;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/play/animation/AnimationStateCounter$AnimationState;-><init>(Landroid/view/ViewGroup;)V

    return-void
.end method
