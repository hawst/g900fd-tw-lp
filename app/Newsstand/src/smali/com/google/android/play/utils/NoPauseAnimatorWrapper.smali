.class public Lcom/google/android/play/utils/NoPauseAnimatorWrapper;
.super Landroid/animation/Animator;
.source "NoPauseAnimatorWrapper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private mAnimator:Landroid/animation/Animator;

.field mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 28
    iput-object p1, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    .line 29
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 30
    return-void
.end method


# virtual methods
.method public addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public addPauseListener(Landroid/animation/Animator$AnimatorPauseListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorPauseListener;

    .prologue
    .line 74
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 103
    return-void
.end method

.method public bridge synthetic clone()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->clone()Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/google/android/play/utils/NoPauseAnimatorWrapper;
    .locals 6

    .prologue
    .line 78
    invoke-super {p0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    .line 79
    .local v0, "anim":Lcom/google/android/play/utils/NoPauseAnimatorWrapper;
    iget-object v4, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 80
    iget-object v3, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 81
    .local v3, "oldListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 82
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 83
    .local v2, "numListeners":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 84
    iget-object v4, v0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "i":I
    .end local v2    # "numListeners":I
    .end local v3    # "oldListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    :cond_0
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->clone()Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    move-result-object v0

    return-object v0
.end method

.method public end()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 98
    return-void
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public getListeners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStartDelay()J
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isPaused()Z

    move-result v0

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    return v0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 181
    iget-object v3, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 182
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 183
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 184
    .local v1, "numListeners":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 185
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 192
    iget-object v3, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 193
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 194
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 195
    .local v1, "numListeners":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 196
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 203
    iget-object v3, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 204
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 205
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 206
    .local v1, "numListeners":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 207
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 214
    iget-object v3, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 215
    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 216
    .local v2, "tmpListeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator$AnimatorListener;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 217
    .local v1, "numListeners":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 218
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v3, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    .line 141
    :cond_0
    return-void
.end method

.method public removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mListeners:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public removePauseListener(Landroid/animation/Animator$AnimatorPauseListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorPauseListener;

    .prologue
    .line 157
    return-void
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public setDuration(J)Landroid/animation/Animator;
    .locals 1
    .param p1, "arg0"    # J

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 50
    return-object p0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 1
    .param p1, "arg0"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 56
    return-void
.end method

.method public setStartDelay(J)V
    .locals 1
    .param p1, "arg0"    # J

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 61
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .locals 1
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public setupEndValues()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupEndValues()V

    .line 172
    return-void
.end method

.method public setupStartValues()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupStartValues()V

    .line 167
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;->mAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 93
    return-void
.end method
