.class public abstract Lcom/google/android/play/transition/ForcedVisibilityTransition;
.super Landroid/transition/Transition;
.source "ForcedVisibilityTransition.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final sTransitionProperties:[Ljava/lang/String;


# instance fields
.field private mMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "play:forcedVisibility:visibility"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->sTransitionProperties:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->mMode:I

    .line 37
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 4
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 52
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "play:forcedVisibility:visibility"

    iget v0, p0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->mMode:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x4

    .line 53
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 52
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void

    .line 52
    :cond_0
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 53
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 4
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 46
    iget-object v1, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "play:forcedVisibility:visibility"

    iget v0, p0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->mMode:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v0, 0x4

    .line 47
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 46
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    return-void

    .line 46
    :cond_0
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 47
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    goto :goto_0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->sTransitionProperties:[Ljava/lang/String;

    return-object v0
.end method

.method protected isRevealing()Z
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/play/transition/ForcedVisibilityTransition;->mMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
