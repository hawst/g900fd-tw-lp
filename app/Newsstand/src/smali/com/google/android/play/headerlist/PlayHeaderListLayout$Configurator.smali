.class public abstract Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.super Ljava/lang/Object;
.source "PlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Configurator"
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 682
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->mContext:Landroid/content/Context;

    .line 683
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 686
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->mContext:Landroid/content/Context;

    .line 687
    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 699
    return-void
.end method

.method protected abstract addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
.end method

.method protected addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 762
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    return v0
.end method

.method protected getBackgroundViewParallaxRatio()F
    .locals 1

    .prologue
    .line 708
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method protected getContentProtectionBackground()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 878
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$color;->play_main_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method protected getContentProtectionBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 869
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$color;->play_main_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method protected getContentProtectionMode()I
    .locals 1

    .prologue
    .line 857
    const/4 v0, 0x0

    return v0
.end method

.method protected getCustomTabStrip(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 929
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 779
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract getHeaderHeight()I
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 902
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderShadowMode()I
    .locals 1

    .prologue
    .line 915
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderSpacerId()I
    .locals 1

    .prologue
    .line 833
    sget v0, Lcom/google/android/play/R$id;->play_header_spacer:I

    return v0
.end method

.method protected getHeroAnimationMode()I
    .locals 1

    .prologue
    .line 846
    const/4 v0, 0x0

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 824
    sget v0, Lcom/google/android/play/R$id;->play_header_listview:I

    return v0
.end method

.method protected getTabAccessibilityMode()I
    .locals 1

    .prologue
    .line 942
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract getTabMode()I
.end method

.method protected getTabPaddingMode()I
    .locals 1

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->alwaysUseFloatingBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 889
    const/4 v0, 0x0

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 816
    sget v0, Lcom/google/android/play/R$id;->play_header_viewpager:I

    return v0
.end method

.method protected abstract hasViewPager()Z
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 795
    const/4 v0, 0x0

    return v0
.end method
