.class Lcom/google/android/play/image/BitmapLoader$1$1;
.super Ljava/lang/Object;
.source "BitmapLoader.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/image/BitmapLoader$1;->create()Lcom/android/volley/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/play/image/BitmapLoader$1;


# direct methods
.method constructor <init>(Lcom/google/android/play/image/BitmapLoader$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/google/android/play/image/BitmapLoader$1;

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    iget-object v0, v0, Lcom/google/android/play/image/BitmapLoader$1;->this$0:Lcom/google/android/play/image/BitmapLoader;

    iget-object v1, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    iget-object v1, v1, Lcom/google/android/play/image/BitmapLoader$1;->val$finalModifiedUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    iget-object v2, v2, Lcom/google/android/play/image/BitmapLoader$1;->val$cacheKey:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    iget v3, v3, Lcom/google/android/play/image/BitmapLoader$1;->val$requestWidth:I

    iget-object v4, p0, Lcom/google/android/play/image/BitmapLoader$1$1;->this$1:Lcom/google/android/play/image/BitmapLoader$1;

    iget v4, v4, Lcom/google/android/play/image/BitmapLoader$1;->val$requestHeight:I

    move-object v5, p1

    # invokes: Lcom/google/android/play/image/BitmapLoader;->onGetImageSuccess(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/play/image/BitmapLoader;->access$200(Lcom/google/android/play/image/BitmapLoader;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    .line 305
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 300
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/google/android/play/image/BitmapLoader$1$1;->onResponse(Landroid/graphics/Bitmap;)V

    return-void
.end method
