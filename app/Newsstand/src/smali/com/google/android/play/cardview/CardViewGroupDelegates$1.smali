.class final Lcom/google/android/play/cardview/CardViewGroupDelegates$1;
.super Ljava/lang/Object;
.source "CardViewGroupDelegates.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroupDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/cardview/CardViewGroupDelegates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "cardView"    # Lcom/google/android/play/cardview/CardViewGroup;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    .line 53
    return-void
.end method

.method public setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V
    .locals 0
    .param p1, "cardView"    # Lcom/google/android/play/cardview/CardViewGroup;
    .param p2, "color"    # I

    .prologue
    .line 57
    return-void
.end method

.method public setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V
    .locals 0
    .param p1, "cardView"    # Lcom/google/android/play/cardview/CardViewGroup;
    .param p2, "resourceId"    # I

    .prologue
    .line 61
    return-void
.end method
