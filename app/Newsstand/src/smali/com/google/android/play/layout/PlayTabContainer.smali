.class public Lcom/google/android/play/layout/PlayTabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "PlayTabContainer.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private mLastScrollTo:I

.field private mScrollState:I

.field private mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

.field private final mTitleOffset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabContainer;->setHorizontalScrollBarEnabled(Z)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$dimen;->play_tab_strip_title_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTitleOffset:I

    .line 48
    return-void
.end method

.method private scrollToChild(II)V
    .locals 5
    .param p1, "childIndex"    # I
    .param p2, "extraOffset"    # I

    .prologue
    .line 145
    iget-object v4, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v2

    .line 146
    .local v2, "tabStripChildCount":I
    if-eqz v2, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v4, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v4, p1}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 151
    .local v0, "selectedChild":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 156
    .local v1, "selectedLeft":I
    add-int v3, v1, p2

    .line 157
    .local v3, "targetScrollX":I
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 158
    :cond_2
    iget v4, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTitleOffset:I

    sub-int/2addr v3, v4

    .line 161
    :cond_3
    iget v4, p0, Lcom/google/android/play/layout/PlayTabContainer;->mLastScrollTo:I

    if-eq v3, v4, :cond_0

    .line 165
    iput v3, p0, Lcom/google/android/play/layout/PlayTabContainer;->mLastScrollTo:I

    .line 166
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/play/layout/PlayTabContainer;->scrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 54
    sget v0, Lcom/google/android/play/R$id;->pager_tab_strip:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTabStrip;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    .line 55
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/play/layout/PlayTabContainer;->mScrollState:I

    .line 134
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 118
    iget-object v3, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v2

    .line 119
    .local v2, "tabStripChildCount":I
    if-eqz v2, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/play/layout/PlayTabStrip;->onPageScrolled(IFI)V

    .line 125
    iget-object v3, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v3, p1}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 126
    .local v1, "selectedTitle":Landroid/view/View;
    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 128
    .local v0, "extraOffset":I
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;->scrollToChild(II)V

    goto :goto_0

    .line 127
    .end local v0    # "extraOffset":I
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v0, v3

    goto :goto_1
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->mScrollState:I

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->mTabStrip:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTabStrip;->onPageSelected(I)V

    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;->scrollToChild(II)V

    .line 142
    :cond_0
    return-void
.end method
