.class public interface abstract Lcom/google/android/play/image/FifeImageView$OnLoadedListener;
.super Ljava/lang/Object;
.source "FifeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/image/FifeImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnLoadedListener"
.end annotation


# virtual methods
.method public abstract onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
.end method

.method public abstract onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
.end method
