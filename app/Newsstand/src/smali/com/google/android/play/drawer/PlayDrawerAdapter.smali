.class Lcom/google/android/play/drawer/PlayDrawerAdapter;
.super Landroid/widget/BaseAdapter;
.source "PlayDrawerAdapter.java"


# static fields
.field private static final SUPPORTS_ANIMATIONS:Z


# instance fields
.field private final mAccountDocV2s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/protos/DocumentV2$DocV2;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountListAnimator:Landroid/animation/ObjectAnimator;

.field private mAccountListExpanded:Z

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mContext:Landroid/content/Context;

.field private mCurrentAccountName:Ljava/lang/String;

.field private mDownloadOnlyEnabled:Z

.field private mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

.field private mHasAccounts:Z

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mIsAccountDocLoaded:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mNonCurrentAccountNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

.field private mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

.field private mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

.field private final mPrimaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileContainerPosition:I

.field private final mSecondaryActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

.field private mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

.field private mShowDownloadOnlyToggle:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->SUPPORTS_ANIMATIONS:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;Lcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout;Landroid/widget/ListView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isAccountListExpanded"    # Z
    .param p3, "playDrawerContentClickListener"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .param p4, "playDfeApiProvider"    # Lcom/google/android/play/dfe/api/PlayDfeApiProvider;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "playDrawerLayout"    # Lcom/google/android/play/drawer/PlayDrawerLayout;
    .param p7, "listView"    # Landroid/widget/ListView;

    .prologue
    .line 96
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 78
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    .line 79
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    .line 85
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    .line 86
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    .line 98
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    .line 99
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 100
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    .line 101
    iput-object p5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 102
    iput-object p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    .line 103
    iput-object p6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 104
    iput-object p7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 107
    iput-boolean p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/play/drawer/PlayDrawerAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/image/BitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method private getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 613
    if-nez p1, :cond_0

    .line 615
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_download_toggle:I

    const/4 v3, 0x0

    .line 616
    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .line 620
    .local v0, "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->configure(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;)V

    .line 621
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;

    invoke-direct {v1, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$10;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setOnCheckedChangeListener(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;)V

    .line 633
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setCheckedNoCallbacks(Z)V

    .line 635
    return-object v0

    .end local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    :cond_0
    move-object v0, p1

    .line 631
    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    .restart local v0    # "toggleRow":Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
    goto :goto_0
.end method

.method private getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;
    .locals 7
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .param p4, "active"    # Z
    .param p5, "disabled"    # Z

    .prologue
    const/4 v6, 0x0

    .line 534
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 537
    .local v3, "res":Landroid/content/res/Resources;
    if-eqz p4, :cond_0

    .line 538
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_active:I

    .line 545
    .local v2, "layoutId":I
    :goto_0
    if-eqz p1, :cond_2

    move-object v4, p1

    .line 546
    :goto_1
    check-cast v4, Landroid/widget/TextView;

    move-object v0, v4

    check-cast v0, Landroid/widget/TextView;

    .line 547
    .local v0, "destinationRow":Landroid/widget/TextView;
    iget-object v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    if-lez v4, :cond_5

    .line 550
    if-eqz p4, :cond_3

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    if-lez v4, :cond_3

    .line 551
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeIconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 555
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    if-eqz p5, :cond_4

    .line 556
    const/16 v4, 0x42

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 560
    :goto_3
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v1, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 565
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_4
    if-eqz p4, :cond_6

    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    if-lez v4, :cond_6

    .line 566
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->activeTextColorResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 572
    :goto_5
    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;

    invoke-direct {v4, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$8;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 580
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v0, v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 581
    return-object v0

    .line 539
    .end local v0    # "destinationRow":Landroid/widget/TextView;
    .end local v2    # "layoutId":I
    :cond_0
    if-eqz p5, :cond_1

    .line 540
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_disabled:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 542
    .end local v2    # "layoutId":I
    :cond_1
    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_action_regular:I

    .restart local v2    # "layoutId":I
    goto :goto_0

    .line 545
    :cond_2
    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v5, 0x0

    .line 546
    invoke-virtual {v4, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    .line 553
    .restart local v0    # "destinationRow":Landroid/widget/TextView;
    :cond_3
    iget v4, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->iconResId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_2

    .line 558
    :cond_4
    const/16 v4, 0xff

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_3

    .line 562
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 567
    :cond_6
    if-eqz p5, :cond_7

    .line 568
    sget v4, Lcom/google/android/play/R$color;->play_disabled_grey:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5

    .line 570
    :cond_7
    sget v4, Lcom/google/android/play/R$color;->play_fg_primary:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5
.end method

.method private getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 527
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 529
    .local v0, "spacingView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 527
    .end local v0    # "spacingView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_primary_actions_top_spacing:I

    const/4 v3, 0x0

    .line 528
    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 11
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 301
    if-eqz p1, :cond_0

    move-object v6, p1

    .line 303
    :goto_0
    check-cast v6, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;

    move-object v4, v6

    check-cast v4, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;

    .line 305
    .local v4, "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    invoke-virtual {v4}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->getSelectedProfileView()Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    .line 306
    invoke-virtual {v4}, Lcom/google/android/play/drawer/PlayDrawerProfileContainer;->getSecondaryProfilesContainer()Lcom/google/android/play/drawer/ShrinkingItem;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    .line 307
    iput p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    .line 311
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;

    .line 316
    .local v1, "finalCurrentAccountName":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v10, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v9, v6, v1, v10}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configure(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 322
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v6, v1}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Ljava/lang/String;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v6

    new-instance v9, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;

    invoke-direct {v9, p0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    new-instance v10, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;

    invoke-direct {v10, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$2;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-interface {v6, v9, v10, v7}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 358
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-boolean v9, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    invoke-virtual {v6, v9}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 360
    iget-object v9, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    move v6, v7

    :goto_1
    invoke-virtual {v9, v6}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListEnabled(Z)V

    .line 362
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v7, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;

    invoke-direct {v7, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$3;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v6, v7}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 375
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v6}, Lcom/google/android/play/drawer/ShrinkingItem;->removeAllViews()V

    .line 376
    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    iget-boolean v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v6, :cond_2

    const/high16 v6, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v7, v6}, Lcom/google/android/play/drawer/ShrinkingItem;->setAnimatedHeightFraction(F)V

    .line 378
    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;

    invoke-direct {v3, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    .line 397
    .local v3, "mChildClickListener":Landroid/view/View$OnClickListener;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_3
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 398
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v7, Lcom/google/android/play/R$layout;->play_drawer_account_row:I

    .line 399
    invoke-virtual {v6, v7, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/drawer/PlayDrawerAccountRow;

    .line 401
    .local v5, "secondaryAccountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 403
    .local v0, "accountName":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v7, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v5, v6, v0, v7}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->bind(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V

    .line 405
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->setTag(Ljava/lang/Object;)V

    .line 406
    invoke-virtual {v5, v3}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v6, v5}, Lcom/google/android/play/drawer/ShrinkingItem;->addView(Landroid/view/View;)V

    .line 397
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 301
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "finalCurrentAccountName":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "mChildClickListener":Landroid/view/View$OnClickListener;
    .end local v4    # "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    .end local v5    # "secondaryAccountRow":Lcom/google/android/play/drawer/PlayDrawerAccountRow;
    :cond_0
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v9, Lcom/google/android/play/R$layout;->play_drawer_profile_container:I

    .line 303
    invoke-virtual {v6, v9, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    goto/16 :goto_0

    .restart local v1    # "finalCurrentAccountName":Ljava/lang/String;
    .restart local v4    # "profileContainer":Lcom/google/android/play/drawer/PlayDrawerProfileContainer;
    :cond_1
    move v6, v8

    .line 360
    goto :goto_1

    .line 376
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 413
    .restart local v2    # "i":I
    .restart local v3    # "mChildClickListener":Landroid/view/View$OnClickListener;
    :cond_3
    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    new-instance v7, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;

    invoke-direct {v7, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$5;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    invoke-virtual {v6, v7}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountTogglerListener(Landroid/view/View$OnClickListener;)V

    .line 434
    iget-boolean v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v6, :cond_4

    .line 435
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->loadAllSecondaryAccountDocV2sOnce()V

    .line 438
    :cond_4
    return-object v4
.end method

.method private getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 593
    if-eqz p1, :cond_0

    move-object v1, p1

    .line 595
    :goto_0
    check-cast v1, Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    .line 597
    .local v0, "secondaryRow":Landroid/widget/TextView;
    iget-object v1, p3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;

    invoke-direct {v1, p0, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter$9;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 607
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/play/utils/PlayUtils;->useLtr(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->setTextAlignmentStart(Landroid/widget/TextView;Z)V

    .line 608
    return-object v0

    .line 593
    .end local v0    # "secondaryRow":Landroid/widget/TextView;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_action:I

    const/4 v3, 0x0

    .line 595
    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method private getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 585
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 588
    .local v0, "separatorView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 585
    .end local v0    # "separatorView":Landroid/view/View;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/play/R$layout;->play_drawer_secondary_actions_top_separator:I

    const/4 v3, 0x0

    .line 586
    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private isProfileContainerVisible()Z
    .locals 4

    .prologue
    .line 645
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 646
    .local v0, "firstVisiblePosition":I
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v2, v3, -0x1

    .line 647
    .local v2, "lastVisiblePosition":I
    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-lt v3, v0, :cond_0

    iget v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileContainerPosition:I

    if-gt v3, v2, :cond_0

    const/4 v1, 0x1

    .line 649
    .local v1, "isProfileContainerVisible":Z
    :goto_0
    return v1

    .line 647
    .end local v1    # "isProfileContainerVisible":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadAllSecondaryAccountDocV2sOnce()V
    .locals 6

    .prologue
    .line 442
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 443
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 444
    .local v0, "accountName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 448
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDfeApiProvider:Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    invoke-interface {v2, v0}, Lcom/google/android/play/dfe/api/PlayDfeApiProvider;->getPlayDfeApi(Ljava/lang/String;)Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v2

    new-instance v3, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;

    invoke-direct {v3, p0, v0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$6;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;

    invoke-direct {v4, p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter$7;-><init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    goto :goto_1

    .line 480
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "useLtr"    # Z

    .prologue
    const/4 v0, 0x0

    .line 663
    if-eqz p2, :cond_0

    .line 664
    invoke-virtual {p0, p1, v0, v0, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 668
    :goto_0
    return-void

    .line 666
    :cond_0
    invoke-virtual {p0, v0, v0, p1, v0}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static setTextAlignmentStart(Landroid/widget/TextView;Z)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "useLtr"    # Z

    .prologue
    .line 679
    if-eqz p1, :cond_0

    .line 680
    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 684
    :goto_0
    return-void

    .line 682
    :cond_0
    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method private toggleAccountsList()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 488
    iget-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    .line 490
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z

    move-result v3

    if-nez v3, :cond_1

    .line 494
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 524
    :goto_1
    return-void

    :cond_0
    move v3, v5

    .line 488
    goto :goto_0

    .line 500
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSelectedProfileView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    iget-boolean v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    invoke-virtual {v3, v6}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->setAccountListExpanded(Z)V

    .line 502
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v3}, Lcom/google/android/play/drawer/ShrinkingItem;->getAnimatedHeightFraction()F

    move-result v2

    .line 503
    .local v2, "start":F
    iget-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v3, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    .line 504
    .local v0, "end":F
    :goto_2
    sget-boolean v3, Lcom/google/android/play/drawer/PlayDrawerAdapter;->SUPPORTS_ANIMATIONS:Z

    if-eqz v3, :cond_4

    .line 505
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    const-string v6, "animatedHeightFraction"

    const/4 v7, 0x2

    new-array v7, v7, [F

    aput v2, v7, v5

    aput v0, v7, v4

    .line 506
    invoke-static {v3, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 510
    .local v1, "heightAnimator":Landroid/animation/ObjectAnimator;
    const/high16 v3, 0x43160000    # 150.0f

    sub-float v4, v2, v0

    .line 511
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-long v4, v3

    .line 510
    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 513
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 514
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 516
    :cond_2
    iput-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListAnimator:Landroid/animation/ObjectAnimator;

    .line 518
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 519
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 503
    .end local v0    # "end":F
    .end local v1    # "heightAnimator":Landroid/animation/ObjectAnimator;
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 521
    .restart local v0    # "end":F
    :cond_4
    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryProfilesContainer:Lcom/google/android/play/drawer/ShrinkingItem;

    invoke-virtual {v3, v0}, Lcom/google/android/play/drawer/ShrinkingItem;->setAnimatedHeightFraction(F)V

    goto :goto_1
.end method


# virtual methods
.method public collapseAccountListIfNeeded()V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->toggleAccountsList()V

    .line 156
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 161
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 163
    .local v0, "result":I
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 165
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    add-int/lit8 v0, v0, 0x1

    .line 169
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v1, :cond_0

    .line 170
    add-int/lit8 v0, v0, 0x1

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    return v0

    .line 161
    .end local v0    # "result":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 179
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_2

    .line 180
    if-nez p1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;

    .line 212
    :cond_0
    :goto_0
    return-object v1

    .line 183
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 187
    :cond_2
    if-eqz p1, :cond_0

    .line 190
    add-int/lit8 p1, p1, -0x1

    .line 193
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 194
    .local v0, "primaryActionsCount":I
    if-ge p1, v0, :cond_3

    .line 195
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 197
    :cond_3
    sub-int/2addr p1, v0

    .line 199
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_5

    .line 200
    if-nez p1, :cond_4

    .line 201
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    goto :goto_0

    .line 203
    :cond_4
    add-int/lit8 p1, p1, -0x1

    .line 207
    :cond_5
    if-eqz p1, :cond_0

    .line 210
    add-int/lit8 p1, p1, -0x1

    .line 212
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 217
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 222
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    if-eqz v2, :cond_1

    .line 223
    if-nez p1, :cond_0

    .line 224
    const/4 v2, 0x0

    .line 261
    :goto_0
    return v2

    .line 226
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 229
    :cond_1
    if-nez p1, :cond_2

    .line 230
    const/4 v2, 0x1

    goto :goto_0

    .line 232
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 234
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 235
    .local v1, "primaryActionsCount":I
    if-ge p1, v1, :cond_5

    .line 236
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 237
    .local v0, "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    if-eqz v2, :cond_3

    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isAvailableInDownloadOnly:Z

    if-nez v2, :cond_3

    .line 239
    const/4 v2, 0x4

    goto :goto_0

    .line 240
    :cond_3
    iget-boolean v2, v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->isActive:Z

    if-eqz v2, :cond_4

    .line 241
    const/4 v2, 0x2

    goto :goto_0

    .line 243
    :cond_4
    const/4 v2, 0x3

    goto :goto_0

    .line 246
    .end local v0    # "action":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr p1, v2

    .line 248
    if-nez p1, :cond_6

    .line 249
    const/4 v2, 0x5

    goto :goto_0

    .line 251
    :cond_6
    add-int/lit8 p1, p1, -0x1

    .line 253
    iget-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v2, :cond_8

    .line 254
    if-nez p1, :cond_7

    .line 256
    const/4 v2, 0x7

    goto :goto_0

    .line 258
    :cond_7
    add-int/lit8 p1, p1, -0x1

    .line 261
    :cond_8
    const/4 v2, 0x6

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItemViewType(I)I

    move-result v7

    .line 272
    .local v7, "viewType":I
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    .line 274
    .local v6, "data":Ljava/lang/Object;
    packed-switch v7, :pswitch_data_0

    .line 296
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "View type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :pswitch_0
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 293
    .end local v6    # "data":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 278
    .restart local v6    # "data":Ljava/lang/Object;
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionsTopSpacing(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move-object v3, v6

    .line 280
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    move-object v3, v6

    .line 283
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    move-object v3, v6

    .line 286
    check-cast v3, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getPrimaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;ZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 289
    :pswitch_5
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionsTopSeparator(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 291
    :pswitch_6
    invoke-direct {p0, p2, p3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getDownloadToggleView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 293
    :pswitch_7
    check-cast v6, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .end local v6    # "data":Ljava/lang/Object;
    invoke-direct {p0, p2, p3, v6}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->getSecondaryActionView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 266
    const/16 v0, 0x8

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 640
    const/4 v0, 0x0

    return v0
.end method

.method public isAccountListExpanded()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountListExpanded:Z

    return v0
.end method

.method public updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V
    .locals 7
    .param p1, "currentAccountName"    # Ljava/lang/String;
    .param p2, "accounts"    # [Landroid/accounts/Account;
    .param p4, "downloadSwitchConfig"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "primaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    .local p5, "secondaryActions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119
    array-length v1, p2

    if-lez v1, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mHasAccounts:Z

    .line 121
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccountName:Ljava/lang/String;

    .line 123
    array-length v1, p2

    if-gt v1, v2, :cond_3

    .line 124
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 137
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 138
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPrimaryActions:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 139
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mSecondaryActions:Ljava/util/List;

    invoke-interface {v1, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 141
    iput-object p4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadSwitchConfig:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    .line 142
    if-eqz p4, :cond_5

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    .line 143
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mShowDownloadOnlyToggle:Z

    if-eqz v1, :cond_1

    iget-boolean v3, p4, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->isChecked:Z

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mDownloadOnlyEnabled:Z

    .line 145
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->notifyDataSetChanged()V

    .line 146
    return-void

    :cond_2
    move v1, v3

    .line 119
    goto :goto_0

    .line 126
    :cond_3
    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    .line 127
    array-length v4, p2

    move v1, v3

    :goto_2
    if-ge v1, v4, :cond_0

    aget-object v0, p2, v1

    .line 128
    .local v0, "account":Landroid/accounts/Account;
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 127
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 131
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .end local v0    # "account":Landroid/accounts/Account;
    :cond_5
    move v2, v3

    .line 142
    goto :goto_1
.end method
