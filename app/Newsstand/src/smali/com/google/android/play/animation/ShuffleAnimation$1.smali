.class final Lcom/google/android/play/animation/ShuffleAnimation$1;
.super Lcom/google/android/play/animation/BaseAnimatorListener;
.source "ShuffleAnimation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/animation/ShuffleAnimation;->shuffleView(Landroid/view/View;Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/ViewGroup;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/play/animation/AnimationStateCounter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    iput-object p2, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/play/animation/BaseAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    iget-object v1, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/play/animation/AnimationStateCounter;->remove(Landroid/view/View;)V

    .line 102
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$animationStateCounter:Lcom/google/android/play/animation/AnimationStateCounter;

    iget-object v1, p0, Lcom/google/android/play/animation/ShuffleAnimation$1;->val$view:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/play/animation/AnimationStateCounter;->add(Landroid/view/View;)V

    .line 97
    return-void
.end method
