.class public final Lcom/google/android/play/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f0d009d

.field public static final play_avatar_decoration_threshold_max:I = 0x7f0d002a

.field public static final play_avatar_decoration_threshold_min:I = 0x7f0d0029

.field public static final play_avatar_drop_shadow_max:I = 0x7f0d002e

.field public static final play_avatar_drop_shadow_min:I = 0x7f0d002d

.field public static final play_avatar_noring_outline:I = 0x7f0d002f

.field public static final play_avatar_ring_size_max:I = 0x7f0d002c

.field public static final play_avatar_ring_size_min:I = 0x7f0d002b

.field public static final play_card_default_inset:I = 0x7f0d0049

.field public static final play_card_extra_vspace:I = 0x7f0d000f

.field public static final play_card_overflow_touch_extend:I = 0x7f0d0012

.field public static final play_card_price_icon_gap:I = 0x7f0d001e

.field public static final play_card_price_texts_gap:I = 0x7f0d001f

.field public static final play_card_reason_avatar_large_size:I = 0x7f0d0016

.field public static final play_card_reason_avatar_size:I = 0x7f0d0015

.field public static final play_card_reason_text_extra_margin_left:I = 0x7f0d001a

.field public static final play_download_button_asset_inset:I = 0x7f0d0030

.field public static final play_drawer_max_width:I = 0x7f0d0027

.field public static final play_hairline_separator_thickness:I = 0x7f0d000a

.field public static final play_header_list_banner_height:I = 0x7f0d0031

.field public static final play_header_list_floating_elevation:I = 0x7f0d0037

.field public static final play_header_list_tab_floating_padding:I = 0x7f0d0039

.field public static final play_header_list_tab_strip_height:I = 0x7f0d0032

.field public static final play_header_list_tab_strip_selected_underline_height:I = 0x7f0d0033

.field public static final play_medium_size:I = 0x7f0d0059

.field public static final play_mini_card_content_height:I = 0x7f0d001c

.field public static final play_mini_card_price_threshold:I = 0x7f0d001d

.field public static final play_onboard__onboard_nav_footer_height:I = 0x7f0d007a

.field public static final play_onboard__onboard_simple_quiz_row_spacing:I = 0x7f0d0080

.field public static final play_onboard__page_indicator_dot_diameter:I = 0x7f0d007c

.field public static final play_reason_large_size:I = 0x7f0d0060

.field public static final play_reason_regular_size:I = 0x7f0d005f

.field public static final play_small_card_content_min_height:I = 0x7f0d001b

.field public static final play_star_height_default:I = 0x7f0d0046

.field public static final play_tab_strip_selected_underline_height:I = 0x7f0d0005

.field public static final play_tab_strip_title_offset:I = 0x7f0d0008

.field public static final play_text_view_fadeout:I = 0x7f0d000b

.field public static final play_text_view_fadeout_hint_margin:I = 0x7f0d000c

.field public static final play_text_view_outline:I = 0x7f0d000d
