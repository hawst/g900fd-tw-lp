.class public Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
.super Ljava/lang/Object;
.source "OnboardTutorialPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/onboard/OnboardTutorialPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected mData:Lcom/google/android/libraries/bind/data/Data;

.field protected final mPageCount:I

.field protected final mPageIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageIndex"    # I
    .param p3, "pageCount"    # I

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    .line 169
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mContext:Landroid/content/Context;

    .line 170
    iput p2, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mPageIndex:I

    .line 171
    iput p3, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mPageCount:I

    .line 172
    sget v0, Lcom/google/android/play/R$layout;->play_onboard_tutorial_page:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setViewResId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    .line 173
    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/libraries/bind/data/Data;
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_ID:I

    const-string v2, "tutorial"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mPageIndex:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_INFO:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardPage;->DK_PAGE_INFO:I

    new-instance v2, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;

    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mContext:Landroid/content/Context;

    .line 219
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mPageIndex:I

    iget v5, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mPageCount:I

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;-><init>(Landroid/content/Context;II)V

    .line 218
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method public setBackgroundColor(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 3
    .param p1, "argbInt"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BACKGROUND_COLOR:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 182
    return-object p0
.end method

.method public setBodyText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 1
    .param p1, "stringResId"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setBodyText(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setBodyText(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_BODY_TEXT:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 196
    return-object p0
.end method

.method public setIconDrawableId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 3
    .param p1, "iconDrawableId"    # I

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_ICON_DRAWABLE_ID:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 205
    return-object p0
.end method

.method public setTitleText(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 1
    .param p1, "stringResId"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->setTitleText(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setTitleText(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->DK_TITLE_TEXT:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 187
    return-object p0
.end method

.method public setViewResId(I)Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;
    .locals 3
    .param p1, "layoutResId"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$Builder;->mData:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 177
    return-object p0
.end method
