.class public Lcom/google/android/play/animation/PlayInterpolators;
.super Ljava/lang/Object;
.source "PlayInterpolators.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static sFastOutLinearIn:Landroid/view/animation/Interpolator;

.field private static sFastOutSlowIn:Landroid/view/animation/Interpolator;

.field private static sSlowOutFastIn:Landroid/view/animation/Interpolator;


# direct methods
.method public static fastOutLinearIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutLinearIn:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    .line 31
    const v0, 0x10c000f

    .line 32
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutLinearIn:Landroid/view/animation/Interpolator;

    .line 34
    :cond_0
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutLinearIn:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutSlowIn:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    .line 23
    const v0, 0x10c000d

    .line 24
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutSlowIn:Landroid/view/animation/Interpolator;

    .line 26
    :cond_0
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sFastOutSlowIn:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sSlowOutFastIn:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/google/android/play/animation/PlayInterpolators$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/animation/PlayInterpolators$1;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sSlowOutFastIn:Landroid/view/animation/Interpolator;

    .line 48
    :cond_0
    sget-object v0, Lcom/google/android/play/animation/PlayInterpolators;->sSlowOutFastIn:Landroid/view/animation/Interpolator;

    return-object v0
.end method
