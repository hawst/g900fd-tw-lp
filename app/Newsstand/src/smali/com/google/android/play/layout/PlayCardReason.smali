.class public Lcom/google/android/play/layout/PlayCardReason;
.super Lcom/google/android/play/layout/PlaySeparatorLayout;
.source "PlayCardReason.java"


# instance fields
.field private final mAvatarLargeSize:I

.field private mAvatarMarginLeft:I

.field private final mAvatarRegularSize:I

.field private mAvatarSize:I

.field private mMode:I

.field private mReason:Landroid/widget/TextView;

.field private mReasonImage:Landroid/widget/ImageView;

.field private final mTextLargeSize:I

.field private mTextOnlyMarginLeft:I

.field private final mTextRegularSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardReason;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlaySeparatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_card_reason_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarRegularSize:I

    .line 75
    sget v1, Lcom/google/android/play/R$dimen;->play_card_reason_avatar_large_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarLargeSize:I

    .line 76
    sget v1, Lcom/google/android/play/R$dimen;->play_reason_regular_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mTextRegularSize:I

    .line 77
    sget v1, Lcom/google/android/play/R$dimen;->play_reason_large_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mTextLargeSize:I

    .line 80
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mMode:I

    .line 81
    return-void
.end method

.method private syncWithCurrentSizeMode()V
    .locals 3

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mMode:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarRegularSize:I

    :goto_0
    iput v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarSize:I

    .line 95
    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mMode:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mTextRegularSize:I

    int-to-float v0, v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 97
    return-void

    .line 94
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarLargeSize:I

    goto :goto_0

    .line 95
    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mTextLargeSize:I

    int-to-float v0, v0

    goto :goto_1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/play/layout/PlaySeparatorLayout;->onFinishInflate()V

    .line 87
    sget v0, Lcom/google/android/play/R$id;->li_reason:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    .line 88
    sget v0, Lcom/google/android/play/R$id;->li_thumbnail_reason:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    .line 90
    invoke-direct {p0}, Lcom/google/android/play/layout/PlayCardReason;->syncWithCurrentSizeMode()V

    .line 91
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardReason;->getHeight()I

    move-result v2

    .line 151
    .local v2, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayCardReason;->getPaddingTop()I

    move-result v7

    .line 153
    .local v7, "paddingTop":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    .line 154
    .local v8, "reasonHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    .line 155
    .local v10, "reasonWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-ne v13, v14, :cond_0

    .line 156
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v9, v7, v13

    .line 157
    .local v9, "reasonTop":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardReason;->mTextOnlyMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardReason;->mTextOnlyMarginLeft:I

    add-int/2addr v15, v10

    add-int v16, v9, v8

    move/from16 v0, v16

    invoke-virtual {v13, v14, v9, v15, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 184
    .end local v9    # "reasonTop":I
    :goto_0
    return-void

    .line 160
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    .line 161
    .local v3, "imageHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    .line 162
    .local v5, "imageWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    .line 163
    invoke-virtual {v13}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 164
    .local v4, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    add-int/2addr v13, v5

    iget v14, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v12, v13, v14

    .line 166
    .local v12, "textX":I
    if-le v3, v8, :cond_1

    .line 169
    sub-int v13, v2, v3

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v6, v7, v13

    .line 170
    .local v6, "imageY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    add-int/2addr v15, v5

    add-int v16, v6, v3

    move/from16 v0, v16

    invoke-virtual {v13, v14, v6, v15, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 172
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v11, v7, v13

    .line 173
    .local v11, "reasonY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v14, v12, v10

    add-int v15, v11, v8

    invoke-virtual {v13, v12, v11, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0

    .line 177
    .end local v6    # "imageY":I
    .end local v11    # "reasonY":I
    :cond_1
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v1, v7, v13

    .line 178
    .local v1, "contentY":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    add-int/2addr v15, v5

    add-int v16, v1, v3

    move/from16 v0, v16

    invoke-virtual {v13, v14, v1, v15, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 180
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v14, v12, v10

    add-int v15, v1, v8

    invoke-virtual {v13, v12, v1, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 124
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 125
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardReason;->getPaddingLeft()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardReason;->getPaddingRight()I

    move-result v9

    sub-int v5, v8, v9

    .line 126
    .local v5, "reasonWidth":I
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_0

    const/4 v4, 0x1

    .line 128
    .local v4, "isImageGone":Z
    :goto_0
    if-eqz v4, :cond_1

    .line 129
    iget v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mTextOnlyMarginLeft:I

    sub-int/2addr v5, v8

    .line 139
    :goto_1
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v7}, Landroid/widget/TextView;->measure(II)V

    .line 142
    iget v7, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarSize:I

    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 143
    .local v1, "contentHeight":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardReason;->getPaddingTop()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardReason;->getPaddingBottom()I

    move-result v8

    add-int v2, v7, v8

    .line 145
    .local v2, "height":I
    invoke-virtual {p0, v6, v2}, Lcom/google/android/play/layout/PlayCardReason;->setMeasuredDimension(II)V

    .line 146
    return-void

    .end local v1    # "contentHeight":I
    .end local v2    # "height":I
    .end local v4    # "isImageGone":Z
    :cond_0
    move v4, v7

    .line 126
    goto :goto_0

    .line 132
    .restart local v4    # "isImageGone":Z
    :cond_1
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    .line 133
    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 134
    .local v3, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarSize:I

    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 135
    .local v0, "avatarSpec":I
    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mReasonImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v0, v0}, Landroid/widget/ImageView;->measure(II)V

    .line 136
    iget v8, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarSize:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v8, v9

    iget v9, p0, Lcom/google/android/play/layout/PlayCardReason;->mAvatarMarginLeft:I

    add-int/2addr v8, v9

    sub-int/2addr v5, v8

    goto :goto_1
.end method
