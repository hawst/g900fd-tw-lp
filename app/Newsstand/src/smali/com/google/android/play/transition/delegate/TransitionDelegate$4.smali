.class Lcom/google/android/play/transition/delegate/TransitionDelegate$4;
.super Lcom/google/android/play/transition/BaseTransitionListener;
.source "TransitionDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/transition/delegate/TransitionDelegate;->setSharedElementReturnTransition(Ljava/lang/Object;Landroid/transition/Transition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

.field final synthetic val$controller:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/play/transition/delegate/TransitionDelegate;Ljava/lang/Object;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/transition/delegate/TransitionDelegate;

    .prologue
    .line 314
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$4;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$4;"
    iput-object p1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iput-object p2, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->val$controller:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/play/transition/BaseTransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 322
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$4;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$4;"
    iget-object v0, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iget-object v1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->val$controller:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onSharedElementReturnTransitionEnd(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 323
    return-void
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 317
    .local p0, "this":Lcom/google/android/play/transition/delegate/TransitionDelegate$4;, "Lcom/google/android/play/transition/delegate/TransitionDelegate$4;"
    iget-object v0, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->this$0:Lcom/google/android/play/transition/delegate/TransitionDelegate;

    iget-object v1, p0, Lcom/google/android/play/transition/delegate/TransitionDelegate$4;->val$controller:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/play/transition/delegate/TransitionDelegate;->onSharedElementReturnTransitionStart(Ljava/lang/Object;Landroid/transition/Transition;)V

    .line 318
    return-void
.end method
