.class public final Lcom/google/android/play/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_popup_menu_item_layout:I = 0x7f04000f

.field public static final play_drawer_account_row:I = 0x7f0400b6

.field public static final play_drawer_download_toggle:I = 0x7f0400b8

.field public static final play_drawer_primary_action_active:I = 0x7f0400b9

.field public static final play_drawer_primary_action_disabled:I = 0x7f0400ba

.field public static final play_drawer_primary_action_regular:I = 0x7f0400bb

.field public static final play_drawer_primary_actions_top_spacing:I = 0x7f0400bc

.field public static final play_drawer_profile_container:I = 0x7f0400bd

.field public static final play_drawer_secondary_action:I = 0x7f0400c0

.field public static final play_drawer_secondary_actions_top_separator:I = 0x7f0400c1

.field public static final play_header_list_layout:I = 0x7f0400c2

.field public static final play_header_list_layout_gb:I = 0x7f0400c3

.field public static final play_header_list_tab_text:I = 0x7f0400c5

.field public static final play_onboard_host_fragment:I = 0x7f0400c6

.field public static final play_onboard_interstitial_overlay:I = 0x7f0400c7

.field public static final play_onboard_nav_footer:I = 0x7f0400c9

.field public static final play_onboard_simple_quiz_card_list_view:I = 0x7f0400ca

.field public static final play_onboard_simple_quiz_header:I = 0x7f0400cb

.field public static final play_onboard_tutorial_page:I = 0x7f0400cd
