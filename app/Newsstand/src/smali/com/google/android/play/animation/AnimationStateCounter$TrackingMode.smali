.class final enum Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;
.super Ljava/lang/Enum;
.source "AnimationStateCounter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/animation/AnimationStateCounter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TrackingMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

.field public static final enum TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

.field public static final enum TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    const-string v1, "TRACK_VIEW_GROUP"

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    .line 31
    new-instance v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    const-string v1, "TRACK_LEAF_VIEWS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    sget-object v1, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_VIEW_GROUP:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->TRACK_LEAF_VIEWS:Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->$VALUES:[Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->$VALUES:[Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    invoke-virtual {v0}, [Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/play/animation/AnimationStateCounter$TrackingMode;

    return-object v0
.end method
