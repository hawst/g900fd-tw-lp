.class public Lcom/google/android/play/layout/PlayCardPriceView;
.super Landroid/view/View;
.source "PlayCardPriceView.java"


# instance fields
.field private mCanShowStrikeText:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private final mIconGap:I

.field private mStrikeText:Ljava/lang/String;

.field private final mStrikeTextPaint:Landroid/text/TextPaint;

.field private mStrikeTextWidth:I

.field private mText:Ljava/lang/String;

.field private final mTextBaseline:I

.field private final mTextHeight:I

.field private final mTextPaint:Landroid/text/TextPaint;

.field private final mTextSize:I

.field private final mTextsGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardPriceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 70
    .local v1, "res":Landroid/content/res/Resources;
    sget v2, Lcom/google/android/play/R$dimen;->play_card_price_icon_gap:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIconGap:I

    .line 71
    sget v2, Lcom/google/android/play/R$dimen;->play_card_price_texts_gap:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextsGap:I

    .line 72
    sget v2, Lcom/google/android/play/R$dimen;->play_medium_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextSize:I

    .line 74
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v5}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    .line 75
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, v2, Landroid/text/TextPaint;->density:F

    .line 76
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    iget v3, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 77
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 79
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v5}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    .line 80
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, v2, Landroid/text/TextPaint;->density:F

    .line 81
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    iget v3, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 82
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    sget v3, Lcom/google/android/play/R$color;->play_fg_secondary:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 83
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v5}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    .line 84
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 86
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 87
    .local v0, "fm":Landroid/graphics/Paint$FontMetrics;
    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextHeight:I

    .line 88
    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextBaseline:I

    .line 90
    invoke-virtual {p0, v4}, Lcom/google/android/play/layout/PlayCardPriceView;->setWillNotDraw(Z)V

    .line 91
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 142
    .local v0, "result":Z
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 143
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    const/4 v0, 0x1

    .line 147
    .end local v0    # "result":Z
    :cond_0
    return v0
.end method

.method public getBaseline()I
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextBaseline:I

    add-int/2addr v0, v1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 209
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingLeft()I

    move-result v2

    .line 210
    .local v2, "x":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingTop()I

    move-result v3

    .line 212
    .local v3, "y":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mText:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v0, 0x1

    .line 214
    .local v0, "hasText":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 217
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    .line 218
    .local v1, "iconY":I
    int-to-float v4, v2

    int-to-float v5, v1

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 219
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 220
    neg-int v4, v2

    int-to-float v4, v4

    neg-int v5, v1

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 222
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget v5, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIconGap:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 225
    .end local v1    # "iconY":I
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mCanShowStrikeText:Z

    if-eqz v4, :cond_1

    .line 227
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeText:Ljava/lang/String;

    int-to-float v5, v2

    iget v6, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextBaseline:I

    add-int/2addr v6, v3

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 228
    iget v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextWidth:I

    iget v5, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextsGap:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 231
    :cond_1
    if-eqz v0, :cond_2

    .line 233
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mText:Ljava/lang/String;

    int-to-float v5, v2

    iget v6, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextBaseline:I

    add-int/2addr v6, v3

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 235
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 236
    return-void

    .line 212
    .end local v0    # "hasText":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 152
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    if-ne v9, v10, :cond_4

    .line 153
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    if-nez v9, :cond_4

    move v1, v7

    .line 155
    .local v1, "forceZeroWidth":Z
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 156
    .local v0, "availableWidth":I
    const/4 v6, 0x0

    .line 157
    .local v6, "width":I
    iput v8, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextWidth:I

    .line 158
    iput-boolean v8, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mCanShowStrikeText:Z

    .line 160
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mText:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    move v2, v7

    .line 164
    .local v2, "hasText":Z
    :goto_1
    if-nez v1, :cond_3

    .line 165
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v9, :cond_0

    .line 167
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 168
    if-eqz v2, :cond_0

    .line 169
    iget v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIconGap:I

    add-int/2addr v6, v9

    .line 173
    :cond_0
    if-eqz v2, :cond_1

    .line 175
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextPaint:Landroid/text/TextPaint;

    iget-object v10, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    float-to-int v5, v9

    .line 176
    .local v5, "textWidth":I
    add-int/2addr v6, v5

    .line 179
    .end local v5    # "textWidth":I
    :cond_1
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeText:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 182
    iget-object v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextPaint:Landroid/text/TextPaint;

    iget-object v10, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v9

    float-to-int v9, v9

    iput v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextWidth:I

    .line 184
    iget v10, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mStrikeTextWidth:I

    if-eqz v2, :cond_6

    iget v9, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextsGap:I

    :goto_2
    add-int v4, v10, v9

    .line 185
    .local v4, "requiredWidth":I
    if-lez v0, :cond_7

    add-int v9, v6, v4

    if-gt v9, v0, :cond_7

    .line 186
    add-int/2addr v6, v4

    .line 187
    iput-boolean v7, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mCanShowStrikeText:Z

    .line 194
    .end local v4    # "requiredWidth":I
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingLeft()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingRight()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    .line 199
    :cond_3
    iget-object v7, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    if-nez v7, :cond_8

    iget v3, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextHeight:I

    .line 201
    .local v3, "height":I
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardPriceView;->getPaddingBottom()I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v3, v7

    .line 203
    invoke-virtual {p0, v6, v3}, Lcom/google/android/play/layout/PlayCardPriceView;->setMeasuredDimension(II)V

    .line 204
    return-void

    .end local v0    # "availableWidth":I
    .end local v1    # "forceZeroWidth":Z
    .end local v2    # "hasText":Z
    .end local v3    # "height":I
    .end local v6    # "width":I
    :cond_4
    move v1, v8

    .line 153
    goto :goto_0

    .restart local v0    # "availableWidth":I
    .restart local v1    # "forceZeroWidth":Z
    .restart local v6    # "width":I
    :cond_5
    move v2, v8

    .line 160
    goto :goto_1

    .restart local v2    # "hasText":Z
    :cond_6
    move v9, v8

    .line 184
    goto :goto_2

    .line 189
    .restart local v4    # "requiredWidth":I
    :cond_7
    iput-boolean v8, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mCanShowStrikeText:Z

    goto :goto_3

    .line 199
    .end local v4    # "requiredWidth":I
    :cond_8
    iget v7, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mTextHeight:I

    iget-object v8, p0, Lcom/google/android/play/layout/PlayCardPriceView;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 200
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_4
.end method
