.class public Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;
.super Ljava/lang/Object;
.source "PlayHeaderScrollableContentListener.java"

# interfaces
.implements Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;


# instance fields
.field private mAbsoluteY:I

.field private mAdapter:Landroid/widget/Adapter;

.field private final mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final mObserver:Landroid/database/DataSetObserver;

.field protected mScrollState:I


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1
    .param p1, "layout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    .line 30
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 31
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method private reset(Z)V
    .locals 1
    .param p1, "resetAdapter"    # Z

    .prologue
    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAbsoluteY:I

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->updateAdapter(Landroid/widget/Adapter;)V

    .line 67
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mScrollState:I

    .line 68
    return-void
.end method

.method private updateAdapter(Landroid/widget/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-ne v0, p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 78
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    .line 79
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 82
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    goto :goto_0
.end method


# virtual methods
.method reset()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset(Z)V

    .line 56
    return-void
.end method
