.class public Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
.super Ljava/lang/Object;
.source "PlayPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/PlayPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "PopupAction"
.end annotation


# instance fields
.field public final mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final mId:I

.field public final mIsEnabled:Z

.field public final mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

.field public final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "isEnabled"    # Z
    .param p3, "actionListener"    # Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mId:I

    .line 93
    iput-object p1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;

    .line 94
    iput-boolean p2, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mIsEnabled:Z

    .line 95
    iput-object p3, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    .line 97
    return-void
.end method


# virtual methods
.method public onSelect()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    iget v1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;->onActionSelected(I)V

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    invoke-interface {v0}, Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    goto :goto_0
.end method
