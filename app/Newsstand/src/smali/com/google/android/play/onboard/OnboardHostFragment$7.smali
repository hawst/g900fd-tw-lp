.class Lcom/google/android/play/onboard/OnboardHostFragment$7;
.super Ljava/lang/Object;
.source "OnboardHostFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/onboard/OnboardHostFragment;->startOrHideSplash()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

.field final synthetic val$dropsDrawable:Lcom/google/android/play/widget/RaindropMaskDrawable;


# direct methods
.method constructor <init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/play/widget/RaindropMaskDrawable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/onboard/OnboardHostFragment;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$7;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    iput-object p2, p0, Lcom/google/android/play/onboard/OnboardHostFragment$7;->val$dropsDrawable:Lcom/google/android/play/widget/RaindropMaskDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 630
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$7;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/play/onboard/OnboardHostFragment;->mShowedSplash:Z

    .line 631
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 632
    .local v0, "animation":Landroid/view/animation/AlphaAnimation;
    new-instance v1, Lcom/google/android/play/onboard/OnboardHostFragment$7$1;

    invoke-direct {v1, p0}, Lcom/google/android/play/onboard/OnboardHostFragment$7$1;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment$7;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 650
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 651
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$7;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    iget-object v1, v1, Lcom/google/android/play/onboard/OnboardHostFragment;->mSplash:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 652
    return-void
.end method
