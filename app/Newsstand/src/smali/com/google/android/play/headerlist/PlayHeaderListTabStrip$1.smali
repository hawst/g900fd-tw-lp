.class Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;
.super Ljava/lang/Object;
.source "PlayHeaderListTabStrip.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->updateTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 183
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$100(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 184
    .local v0, "visualPosition":I
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # invokes: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->scrollToVisualPosition(IIZ)V
    invoke-static {v1, v0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$200(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;IIZ)V

    .line 188
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip$1;->this$0:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    # getter for: Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->mTabContainer:Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;
    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->access$300(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 189
    return-void
.end method
