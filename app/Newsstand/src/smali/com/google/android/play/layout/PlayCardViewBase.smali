.class public abstract Lcom/google/android/play/layout/PlayCardViewBase;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayCardViewBase.java"

# interfaces
.implements Lcom/google/android/play/cardview/CardViewGroup;


# static fields
.field protected static final DISABLE_NESTED_FOCUS_TRAVERSAL:Z


# instance fields
.field protected final mAvatarReasonMarginLeft:I

.field protected mBackendId:I

.field private final mCardInset:I

.field protected mDescription:Lcom/google/android/play/layout/PlayTextView;

.field private mDisabledDrawable:Landroid/graphics/drawable/Drawable;

.field protected mIsItemOwned:Z

.field protected mItemBadge:Lcom/google/android/play/layout/PlayTextView;

.field protected mLoadingIndicator:Landroid/view/View;

.field private final mOldOverflowArea:Landroid/graphics/Rect;

.field protected mOverflow:Landroid/widget/ImageView;

.field private final mOverflowArea:Landroid/graphics/Rect;

.field private final mOverflowTouchExtend:I

.field private final mOwnershipRenderingType:I

.field protected mPrice:Lcom/google/android/play/layout/PlayCardPriceView;

.field protected mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field protected mReason1:Lcom/google/android/play/layout/PlayCardReason;

.field protected mReason2:Lcom/google/android/play/layout/PlayCardReason;

.field private final mShowInlineCreatorBadge:Z

.field protected mSubtitle:Lcom/google/android/play/layout/PlayTextView;

.field protected mSupportsSubtitleAndRating:Z

.field protected final mTextOnlyReasonMarginLeft:I

.field protected mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

.field protected mThumbnailAspectRatio:F

.field protected mTitle:Landroid/widget/TextView;

.field private mToDisplayAsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/PlayCardViewBase;->DISABLE_NESTED_FOCUS_TRAVERSAL:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 129
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$dimen;->play_card_overflow_touch_extend:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    .line 133
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    .line 134
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    .line 136
    sget-object v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 138
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_show_inline_creator_badge:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mShowInlineCreatorBadge:Z

    .line 140
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_supports_subtitle_and_rating:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSupportsSubtitleAndRating:Z

    .line 142
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_text_only_reason_margin_left:I

    .line 144
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/play/R$dimen;->play_card_reason_text_extra_margin_left:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 142
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mTextOnlyReasonMarginLeft:I

    .line 146
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_avatar_reason_margin_left:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mAvatarReasonMarginLeft:I

    .line 148
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardBaseView_card_owned_status_rendering_type:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOwnershipRenderingType:I

    .line 151
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$dimen;->play_card_default_inset:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    .line 154
    iget v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v3, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    iget v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mCardInset:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardViewBase;->setForegroundPadding(IIII)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Lcom/google/android/play/cardview/CardViewGroup;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 157
    return-void
.end method


# virtual methods
.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 495
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 499
    .local v0, "result":Z
    iget-boolean v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v4, :cond_1

    .line 500
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    move v1, v2

    .line 501
    .local v1, "shouldMarkAsDisabled":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 502
    invoke-virtual {p1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    move v0, v2

    .line 506
    .end local v0    # "result":Z
    :cond_0
    return v0

    .end local v1    # "shouldMarkAsDisabled":Z
    .restart local v0    # "result":Z
    :cond_1
    move v1, v3

    .line 500
    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 473
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 475
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getWidth()I

    move-result v1

    .line 476
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getHeight()I

    move-result v0

    .line 478
    .local v0, "height":I
    iget-boolean v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-eqz v2, :cond_1

    .line 479
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    .line 480
    new-instance v2, Landroid/graphics/drawable/PaintDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/R$color;->play_dismissed_overlay:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    .line 483
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v5, v5, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 484
    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDisabledDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 486
    :cond_1
    return-void
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method protected measureThumbnailSpanningHeight(I)V
    .locals 8
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 338
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 340
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingTop()I

    move-result v2

    .line 341
    .local v2, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingBottom()I

    move-result v1

    .line 343
    .local v1, "paddingBottom":I
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 344
    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 345
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 346
    sub-int v6, v0, v2

    sub-int/2addr v6, v1

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v6, v7

    .line 348
    .local v3, "thumbnailHeight":I
    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    div-float/2addr v6, v7

    float-to-int v5, v6

    .line 349
    .local v5, "thumbnailWidth":I
    iput v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 353
    .end local v3    # "thumbnailHeight":I
    .end local v5    # "thumbnailWidth":I
    :goto_0
    return-void

    .line 351
    :cond_0
    const/4 v6, 0x0

    iput v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto :goto_0
.end method

.method protected measureThumbnailSpanningWidth(I)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I

    .prologue
    .line 320
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 322
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingLeft()I

    move-result v1

    .line 323
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getPaddingRight()I

    move-result v2

    .line 325
    .local v2, "paddingRight":I
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 326
    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 327
    .local v4, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 328
    sub-int v6, v0, v1

    sub-int/2addr v6, v2

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    .line 330
    .local v5, "thumbnailWidth":I
    iget v6, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnailAspectRatio:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 331
    .local v3, "thumbnailHeight":I
    iput v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 335
    .end local v3    # "thumbnailHeight":I
    .end local v5    # "thumbnailWidth":I
    :goto_0
    return-void

    .line 333
    :cond_0
    const/4 v6, 0x0

    iput v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onAttachedToWindow()V

    .line 163
    invoke-static {}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->getInstance()Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->cardAttachedToWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 164
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onDetachedFromWindow()V

    .line 170
    invoke-static {}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->getInstance()Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/play/layout/PlayCardWindowLifecycleTracker;->cardDetachedFromWindow(Lcom/google/android/play/layout/PlayCardViewBase;)V

    .line 171
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 294
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 296
    sget v0, Lcom/google/android/play/R$id;->li_thumbnail_frame:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 297
    sget v0, Lcom/google/android/play/R$id;->li_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mTitle:Landroid/widget/TextView;

    .line 298
    sget v0, Lcom/google/android/play/R$id;->li_subtitle:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    .line 299
    sget v0, Lcom/google/android/play/R$id;->li_rating:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 300
    sget v0, Lcom/google/android/play/R$id;->li_badge:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mItemBadge:Lcom/google/android/play/layout/PlayTextView;

    .line 301
    sget v0, Lcom/google/android/play/R$id;->li_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    .line 302
    sget v0, Lcom/google/android/play/R$id;->li_overflow:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    .line 303
    sget v0, Lcom/google/android/play/R$id;->li_price:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardPriceView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mPrice:Lcom/google/android/play/layout/PlayCardPriceView;

    .line 304
    sget v0, Lcom/google/android/play/R$id;->li_reason_1:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mReason1:Lcom/google/android/play/layout/PlayCardReason;

    .line 305
    sget v0, Lcom/google/android/play/R$id;->li_reason_2:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardReason;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mReason2:Lcom/google/android/play/layout/PlayCardReason;

    .line 306
    sget v0, Lcom/google/android/play/R$id;->loading_progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mLoadingIndicator:Landroid/view/View;

    .line 310
    sget-boolean v0, Lcom/google/android/play/layout/PlayCardViewBase;->DISABLE_NESTED_FOCUS_TRAVERSAL:Z

    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->setNextFocusRightId(I)V

    .line 312
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 314
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setNextFocusLeftId(I)V

    .line 317
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 512
    invoke-super {p0, p1}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 514
    iget-boolean v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mToDisplayAsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 515
    return-void

    .line 514
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 443
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onLayout(ZIIII)V

    .line 445
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->recomputeOverflowAreaIfNeeded()V

    .line 446
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 417
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onMeasure(II)V

    .line 419
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    .line 420
    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 423
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v1

    .line 424
    .local v1, "descriptionHeight":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 425
    .local v2, "layout":Landroid/text/Layout;
    if-nez v2, :cond_1

    .line 439
    .end local v1    # "descriptionHeight":I
    .end local v2    # "layout":Landroid/text/Layout;
    :cond_0
    :goto_0
    return-void

    .line 428
    .restart local v1    # "descriptionHeight":I
    .restart local v2    # "layout":Landroid/text/Layout;
    :cond_1
    const/4 v3, 0x0

    .local v3, "line":I
    :goto_1
    invoke-virtual {v2}, Landroid/text/Layout;->getLineCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 429
    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v0

    .line 430
    .local v0, "currLineBottom":I
    if-le v0, v1, :cond_3

    .line 434
    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v5, v4}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x4

    goto :goto_2

    .line 428
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method protected final recomputeOverflowAreaIfNeeded()V
    .locals 3

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 456
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 457
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 458
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 459
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowTouchExtend:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 460
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    if-eq v0, v1, :cond_0

    .line 467
    :cond_2
    new-instance v0, Lcom/google/android/play/utils/PlayTouchDelegate;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflow:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/utils/PlayTouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardViewBase;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 468
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOldOverflowArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardViewBase;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 181
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundResource(Lcom/google/android/play/cardview/CardViewGroup;I)V

    .line 186
    return-void
.end method
