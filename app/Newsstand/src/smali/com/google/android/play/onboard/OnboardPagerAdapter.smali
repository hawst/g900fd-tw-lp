.class public Lcom/google/android/play/onboard/OnboardPagerAdapter;
.super Lcom/google/android/libraries/bind/data/DataPagerAdapter;
.source "OnboardPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;
    }
.end annotation


# static fields
.field public static final DK_PAGE_GENERATOR:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardPagerAdapter_pageGenerator:I

    sput v0, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
    .locals 0
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    .line 34
    return-void
.end method


# virtual methods
.method public getPageInfo(I)Lcom/google/android/play/onboard/OnboardPageInfo;
    .locals 2
    .param p1, "logicalPosition"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 61
    .local v0, "page":Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v1, :cond_0

    .line 62
    check-cast v0, Lcom/google/android/play/onboard/OnboardPage;

    .end local v0    # "page":Landroid/view/View;
    invoke-interface {v0}, Lcom/google/android/play/onboard/OnboardPage;->getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;

    move-result-object v1

    .line 64
    :goto_0
    return-object v1

    .restart local v0    # "page":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getView(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 10
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "logicalPosition"    # I
    .param p3, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 38
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-virtual {p3, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v3

    .line 39
    .local v3, "viewResId":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 40
    .local v2, "view":Landroid/view/View;
    if-nez v3, :cond_2

    .line 41
    sget v4, Lcom/google/android/play/onboard/OnboardPagerAdapter;->DK_PAGE_GENERATOR:I

    invoke-virtual {p3, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;

    .line 42
    .local v1, "pageGenerator":Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;
    if-eqz v1, :cond_1

    const/4 v4, 0x1

    :goto_0
    const-string v5, "Missing both view resource ID and page generator"

    invoke-static {v4, v5}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v4, p2, p3}, Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;->makePage(Landroid/content/Context;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v2

    .line 56
    .end local v1    # "pageGenerator":Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;
    :cond_0
    :goto_1
    return-object v2

    .line 42
    .restart local v1    # "pageGenerator":Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 47
    .end local v1    # "pageGenerator":Lcom/google/android/play/onboard/OnboardPagerAdapter$PageGenerator;
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 48
    instance-of v4, v2, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v4, :cond_0

    .line 49
    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    invoke-virtual {p3, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .local v0, "equalityFields":[I
    move-object v4, v2

    .line 50
    check-cast v4, Lcom/google/android/libraries/bind/data/DataView;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v5

    .line 51
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v6

    new-instance v7, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;

    .line 52
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget v9, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    invoke-direct {v7, v8, v9}, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;-><init>(II)V

    .line 50
    invoke-virtual {v5, v6, v7, v0}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/libraries/bind/data/DataView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    goto :goto_1
.end method
