.class public Lcom/google/android/play/onboard/OnboardPager;
.super Lcom/google/android/play/widget/UserAwareViewPager;
.source "OnboardPager.java"


# instance fields
.field protected mAllowSwipeToNext:Z

.field protected mAllowSwipeToPrevious:Z

.field protected mLastX:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/play/widget/UserAwareViewPager;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToNext:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToPrevious:Z

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/widget/UserAwareViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToNext:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToPrevious:Z

    .line 34
    return-void
.end method


# virtual methods
.method protected allowScrolling(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->getCurrentVisualItem()I

    move-result v1

    .line 133
    .local v1, "currentVisualItem":I
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 134
    .local v0, "count":I
    :goto_0
    if-gez p1, :cond_2

    if-lez v1, :cond_2

    .line 135
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->isRtl()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToNext:Z

    .line 140
    :goto_1
    return v2

    .line 133
    .end local v0    # "count":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    goto :goto_0

    .line 135
    .restart local v0    # "count":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToPrevious:Z

    goto :goto_1

    .line 136
    :cond_2
    if-lez p1, :cond_4

    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_4

    .line 137
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->isRtl()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToPrevious:Z

    goto :goto_1

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToNext:Z

    goto :goto_1

    .line 140
    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 62
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/play/onboard/OnboardPager;->shouldAllowTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/play/widget/UserAwareViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/play/widget/UserAwareViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/google/android/play/onboard/OnboardPager;->shouldAllowTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/play/widget/UserAwareViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 87
    :goto_0
    return v1

    .line 80
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/play/widget/UserAwareViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 87
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected pageLeft()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardPager;->allowScrolling(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/play/widget/UserAwareViewPager;->pageLeft()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected pageRight()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardPager;->allowScrolling(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/play/widget/UserAwareViewPager;->pageRight()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restrictFocusToCurrentItem(Ljava/lang/Integer;Z)V
    .locals 9
    .param p1, "optDescendantId"    # Ljava/lang/Integer;
    .param p2, "requestFocus"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->getCurrentVisualItem()I

    move-result v1

    .line 159
    .local v1, "currentVisualItem":I
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->getChildCount()I

    move-result v0

    .line 160
    .local v0, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_5

    .line 161
    invoke-virtual {p0, v3}, Lcom/google/android/play/onboard/OnboardPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 162
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p0, v4}, Lcom/google/android/play/onboard/OnboardPager;->getChildViewPosition(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v5

    .line 163
    .local v5, "visualPosition":Ljava/lang/Integer;
    if-eqz v5, :cond_2

    .line 164
    if-eqz p1, :cond_0

    .line 165
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 167
    :cond_0
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, v1, :cond_3

    const/4 v2, 0x1

    .line 168
    .local v2, "focusable":Z
    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 169
    invoke-virtual {v4, v2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 170
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v7, v8, :cond_1

    .line 171
    if-eqz v2, :cond_4

    move v7, v6

    :goto_2
    invoke-virtual {v4, v7}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 175
    :cond_1
    if-eqz p2, :cond_2

    if-eqz v2, :cond_2

    .line 176
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 160
    .end local v2    # "focusable":Z
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    move v2, v6

    .line 167
    goto :goto_1

    .line 171
    .restart local v2    # "focusable":Z
    :cond_4
    const/4 v7, 0x4

    goto :goto_2

    .line 180
    .end local v2    # "focusable":Z
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "visualPosition":Ljava/lang/Integer;
    :cond_5
    return-void
.end method

.method public setAllowSwipeToNext(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToNext:Z

    .line 38
    return-void
.end method

.method public setAllowSwipeToPrevious(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/play/onboard/OnboardPager;->mAllowSwipeToPrevious:Z

    .line 42
    return-void
.end method

.method protected shouldAllowTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-le v5, v3, :cond_2

    move v1, v3

    .line 105
    .local v1, "multiTouch":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    move v4, v3

    .line 125
    :cond_1
    :goto_2
    :pswitch_1
    return v4

    .end local v1    # "multiTouch":Z
    :cond_2
    move v1, v4

    .line 104
    goto :goto_0

    .line 111
    .restart local v1    # "multiTouch":Z
    :pswitch_2
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iput v4, p0, Lcom/google/android/play/onboard/OnboardPager;->mLastX:F

    goto :goto_1

    .line 115
    :pswitch_3
    if-nez v1, :cond_1

    .line 118
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 119
    .local v2, "x":F
    iget v5, p0, Lcom/google/android/play/onboard/OnboardPager;->mLastX:F

    sub-float v0, v2, v5

    .line 120
    .local v0, "dX":F
    iput v2, p0, Lcom/google/android/play/onboard/OnboardPager;->mLastX:F

    .line 121
    neg-float v5, v0

    invoke-static {v5}, Ljava/lang/Math;->signum(F)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/onboard/OnboardPager;->allowScrolling(I)Z

    move-result v5

    if-nez v5, :cond_0

    goto :goto_2

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
