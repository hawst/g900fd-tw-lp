.class Lcom/google/android/play/transition/CircularReveal$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "CircularReveal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/transition/CircularReveal;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/transition/CircularReveal;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/play/transition/CircularReveal;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/transition/CircularReveal;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/play/transition/CircularReveal$1;->this$0:Lcom/google/android/play/transition/CircularReveal;

    iput-object p2, p0, Lcom/google/android/play/transition/CircularReveal$1;->val$view:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$1;->this$0:Lcom/google/android/play/transition/CircularReveal;

    invoke-virtual {v0}, Lcom/google/android/play/transition/CircularReveal;->isRevealing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$1;->val$view:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$1;->val$view:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 147
    :cond_0
    return-void
.end method
