.class Lcom/google/android/play/search/PlaySearchNavigationButton$1;
.super Ljava/lang/Object;
.source "PlaySearchNavigationButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/search/PlaySearchNavigationButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;


# direct methods
.method constructor <init>(Lcom/google/android/play/search/PlaySearchNavigationButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/search/PlaySearchNavigationButton;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mListener:Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$000(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 86
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mListener:Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$000(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;->onBackArrowClicked()V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mCurrentMode:I
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$100(Lcom/google/android/play/search/PlaySearchNavigationButton;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchNavigationButton$1;->this$0:Lcom/google/android/play/search/PlaySearchNavigationButton;

    # getter for: Lcom/google/android/play/search/PlaySearchNavigationButton;->mListener:Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;
    invoke-static {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton;->access$000(Lcom/google/android/play/search/PlaySearchNavigationButton;)Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/play/search/PlaySearchNavigationButton$Listener;->onHamburgerClicked()V

    goto :goto_0
.end method
