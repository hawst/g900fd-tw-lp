.class Lcom/google/android/play/drawer/PlayDrawerAdapter$4;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileContainer(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .prologue
    .line 378
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 385
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 387
    .local v0, "pos":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerContentClickListener:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    .line 388
    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccountNames:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$800(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 387
    invoke-interface {v2, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;->onSecondaryAccountClicked(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$4;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mPlayDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;
    invoke-static {v1}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$700(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->closeDrawer()V

    .line 391
    :cond_0
    return-void
.end method
