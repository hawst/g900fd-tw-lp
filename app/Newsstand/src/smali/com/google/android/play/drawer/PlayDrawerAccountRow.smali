.class Lcom/google/android/play/drawer/PlayDrawerAccountRow;
.super Landroid/widget/RelativeLayout;
.source "PlayDrawerAccountRow.java"


# instance fields
.field private mAccountName:Landroid/widget/TextView;

.field private mAvatar:Lcom/google/android/play/image/FifeImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 5
    .param p1, "accountDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 44
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->mAccountName:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    if-nez p1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/play/R$drawable;->ic_profile_none:I

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 54
    .local v0, "avatarBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v2

    .line 55
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 56
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->mAvatar:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v2, v0}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 65
    .end local v0    # "avatarBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 59
    :cond_0
    const/4 v2, 0x4

    .line 60
    invoke-static {p1, v2}, Lcom/google/android/play/utils/DocV2Utils;->getFirstImageOfType(Lcom/google/android/finsky/protos/DocumentV2$DocV2;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    .line 61
    .local v1, "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->mAvatar:Lcom/google/android/play/image/FifeImageView;

    iget-object v3, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v4, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v2, v3, v4, p3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 38
    sget v0, Lcom/google/android/play/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->mAvatar:Lcom/google/android/play/image/FifeImageView;

    .line 39
    sget v0, Lcom/google/android/play/R$id;->account_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerAccountRow;->mAccountName:Landroid/widget/TextView;

    .line 40
    return-void
.end method
