.class public abstract Lcom/google/android/play/onboard/OnboardSimpleQuizPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "OnboardSimpleQuizPage.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPage;
.implements Lcom/google/android/play/onboard/OnboardPageInfo;


# static fields
.field public static final DK_TITLE:I


# instance fields
.field protected mAdapter:Lcom/google/android/libraries/bind/data/BindingDataAdapter;

.field protected mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

.field protected mFooterSpacer:Landroid/view/View;

.field protected mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

.field protected mModifiedByUser:Z

.field protected final mSelectedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final mStateKeyModifiedByUser:Ljava/lang/String;

.field protected final mStateKeyPrefix:Ljava/lang/String;

.field protected final mStateKeySelectedItemIds:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/play/R$id;->play_onboard__OnboardSimpleQuizPage_title:I

    sput v0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->DK_TITLE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyPrefix:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyPrefix:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "selectedItemIds"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeySelectedItemIds:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyPrefix:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "modifiedByUser"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyModifiedByUser:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mSelectedItemIds:Ljava/util/Set;

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mModifiedByUser:Z

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$color;->play_onboard_simple_quiz_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 68
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->setBackgroundColor(I)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeCardListView()Lcom/google/android/libraries/bind/card/CardListView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->setUpAdapter()V

    .line 72
    return-void
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 301
    const/4 v0, 0x1

    return v0
.end method

.method public allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 296
    const/4 v0, 0x1

    return v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 278
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    sget v1, Lcom/google/android/play/R$drawable;->ic_chevron_end_wht_24dp:I

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    .line 279
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$string;->play_onboard_button_next:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/play/onboard/OnboardSimpleQuizPage$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage$2;-><init>(Lcom/google/android/play/onboard/OnboardSimpleQuizPage;Lcom/google/android/play/onboard/OnboardHostControl;)V

    .line 280
    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method protected getHeaderText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 193
    new-instance v0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage$1;-><init>(Lcom/google/android/play/onboard/OnboardSimpleQuizPage;Ljava/lang/String;)V

    return-object v0
.end method

.method protected abstract getNumColumns()I
.end method

.method public getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;
    .locals 0

    .prologue
    .line 234
    return-object p0
.end method

.method protected abstract getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;
.end method

.method protected getSelectedItemIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mSelectedItemIds:Ljava/util/Set;

    return-object v0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 273
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method protected makeAdapter()Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    .locals 3

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    new-instance v1, Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/bind/view/ViewHeap;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    return-object v0
.end method

.method protected makeCardGroup()Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 4

    .prologue
    .line 132
    new-instance v1, Lcom/google/android/libraries/bind/card/GridGroup;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/libraries/bind/card/GridGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;Landroid/content/Context;)V

    .line 133
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getNumColumns()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/card/GridGroup;->setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v0

    .line 134
    .local v0, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    return-object v0
.end method

.method protected makeCardListBuilder()Lcom/google/android/libraries/bind/card/CardListBuilder;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/card/CardListBuilder;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected makeCardListView()Lcom/google/android/libraries/bind/card/CardListView;
    .locals 4

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$layout;->play_onboard_simple_quiz_card_list_view:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/CardListView;

    .line 109
    .local v0, "cardListView":Lcom/google/android/libraries/bind/card/CardListView;
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->addView(Landroid/view/View;)V

    .line 110
    return-object v0
.end method

.method protected makeFooterSpacer()Landroid/view/View;
    .locals 6

    .prologue
    .line 148
    const/4 v3, 0x0

    .line 149
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v4, v5}, Lcom/google/android/play/onboard/OnboardPageInfo;->isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 152
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 153
    .local v2, "res":Landroid/content/res/Resources;
    sget v4, Lcom/google/android/play/R$dimen;->play_onboard__onboard_nav_footer_height:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget v5, Lcom/google/android/play/R$dimen;->play_onboard__onboard_simple_quiz_row_spacing:I

    .line 154
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v1, v4, v5

    .line 156
    .local v1, "heightPx":I
    new-instance v3, Landroid/view/View;

    .end local v3    # "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 157
    .restart local v3    # "view":Landroid/view/View;
    new-instance v4, Landroid/widget/AbsListView$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v5, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "heightPx":I
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    return-object v3
.end method

.method protected makeHeaderData()Lcom/google/android/libraries/bind/data/Data;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 180
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v2, Lcom/google/android/play/R$layout;->play_onboard_simple_quiz_header:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 181
    sget v1, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->DK_TITLE:I

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getHeaderText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 182
    return-object v0
.end method

.method public onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 290
    invoke-interface {p1}, Lcom/google/android/play/onboard/OnboardHostControl;->goToPreviousPage()V

    .line 291
    const/4 v0, 0x1

    return v0
.end method

.method public onEnterPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 256
    return-void
.end method

.method public onExitPage(Z)V
    .locals 0
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 259
    return-void
.end method

.method protected onItemClick(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "itemId"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v1

    .line 203
    .local v1, "selectedItemIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 204
    .local v0, "selected":Z
    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 205
    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 206
    const/4 v0, 0x1

    .line 208
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mModifiedByUser:Z

    .line 211
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->requestAccessibilityFocus(Landroid/view/View;)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 215
    return-void
.end method

.method protected requestAccessibilityFocus(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 223
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 224
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 225
    const/16 v0, 0x80

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 226
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 227
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public restoreOnboardState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeySelectedItemIds:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 240
    .local v0, "selectedItemIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 243
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getQuizItemDataList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyModifiedByUser:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mModifiedByUser:Z

    .line 246
    return-void
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeySelectedItemIds:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    .line 251
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getSelectedItemIds()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 250
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mStateKeyModifiedByUser:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mModifiedByUser:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 253
    return-void
.end method

.method public setOnboardHostControl(Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0
    .param p1, "control"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    .line 77
    return-void
.end method

.method protected setUpAdapter()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeCardListBuilder()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v0

    .line 81
    .local v0, "builder":Lcom/google/android/libraries/bind/card/CardListBuilder;
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/card/CardListBuilder;->setTopPaddingEnabled(Z)Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/card/CardListBuilder;->setBottomPaddingEnabled(Z)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeCardGroup()Lcom/google/android/libraries/bind/card/CardGroup;

    move-result-object v1

    .line 84
    .local v1, "group":Lcom/google/android/libraries/bind/card/CardGroup;
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->getHeaderText()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeHeaderData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 88
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/CardListBuilder;->makeCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/card/CardListBuilder;->append(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeAdapter()Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mAdapter:Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 90
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mAdapter:Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->finishUpdate()Lcom/google/android/libraries/bind/card/CardListBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/card/CardListBuilder;->cardList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->makeFooterSpacer()Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mFooterSpacer:Landroid/view/View;

    .line 93
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mFooterSpacer:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 94
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mFooterSpacer:Landroid/view/View;

    .line 95
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    .local v2, "layoutParams":Landroid/widget/AbsListView$LayoutParams;
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    iget-object v4, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mFooterSpacer:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/card/CardListView;->addFooterView(Landroid/view/View;)V

    .line 97
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mFooterSpacer:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    .end local v2    # "layoutParams":Landroid/widget/AbsListView$LayoutParams;
    :cond_1
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;

    iget-object v4, p0, Lcom/google/android/play/onboard/OnboardSimpleQuizPage;->mAdapter:Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    return-void
.end method

.method public shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 268
    const/4 v0, 0x0

    return v0
.end method
