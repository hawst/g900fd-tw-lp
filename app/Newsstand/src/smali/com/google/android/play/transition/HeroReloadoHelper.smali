.class public Lcom/google/android/play/transition/HeroReloadoHelper;
.super Ljava/lang/Object;
.source "HeroReloadoHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public static enterTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardClassTypes"    # [Ljava/lang/Class;
    .param p2, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/transition/HeroReloadoHelper;->windowTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method private static headerListLayoutTransitions(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/Transition;
    .locals 4
    .param p0, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    const/4 v3, 0x1

    .line 124
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 125
    .local v0, "actionBar":Landroid/view/ViewGroup;
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 126
    sget v2, Lcom/google/android/play/R$id;->tab_bar:I

    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 127
    .local v1, "tabBar":Landroid/view/ViewGroup;
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 128
    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    return-object v2
.end method

.method public static returnTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/TransitionSet;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardClassTypes"    # [Ljava/lang/Class;
    .param p2, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/transition/HeroReloadoHelper;->windowTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method public static sharedElementEnterTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/transition/TransitionSet;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hero"    # Landroid/view/View;
    .param p2, "splash"    # Landroid/view/View;
    .param p3, "maintainsHeroAspectRatio"    # Z

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/play/transition/HeroReloadoHelper;->sharedElementTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method public static sharedElementReturnTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;Z)Landroid/transition/TransitionSet;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hero"    # Landroid/view/View;
    .param p2, "splash"    # Landroid/view/View;
    .param p3, "maintainsHeroAspectRatio"    # Z

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/play/transition/HeroReloadoHelper;->sharedElementTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0
.end method

.method private static sharedElementTransition(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "hero"    # Landroid/view/View;
    .param p2, "splash"    # Landroid/view/View;
    .param p3, "maintainsHeroAspectRatio"    # Z
    .param p4, "isEntering"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 56
    new-instance v3, Lcom/google/android/play/transition/Scale;

    invoke-direct {v3, p4}, Lcom/google/android/play/transition/Scale;-><init>(Z)V

    .line 57
    invoke-virtual {v3, p3}, Lcom/google/android/play/transition/Scale;->forceMaintainAspectRatio(Z)Lcom/google/android/play/transition/Scale;

    move-result-object v3

    .line 58
    invoke-virtual {v3, p1}, Lcom/google/android/play/transition/Scale;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 59
    .local v1, "heroTransition":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/ArcMotion;

    invoke-direct {v3}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v1, v3}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 61
    new-instance v6, Lcom/google/android/play/transition/CircularReveal;

    if-eqz p4, :cond_0

    move v3, v4

    :goto_0
    invoke-direct {v6, v3}, Lcom/google/android/play/transition/CircularReveal;-><init>(I)V

    .line 63
    invoke-virtual {v6, p2}, Lcom/google/android/play/transition/CircularReveal;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    .line 65
    .local v2, "splashTransition":Landroid/transition/Transition;
    new-instance v3, Landroid/transition/ChangeBounds;

    invoke-direct {v3}, Landroid/transition/ChangeBounds;-><init>()V

    .line 66
    invoke-virtual {v3, p2}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 69
    .local v0, "changeBounds":Landroid/transition/Transition;
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/transition/Transition;

    const/4 v6, 0x0

    aput-object v1, v3, v6

    aput-object v2, v3, v4

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/google/android/play/transition/PlayTransitionUtil;->aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v4

    if-eqz p4, :cond_1

    .line 71
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    .line 70
    :goto_1
    invoke-virtual {v4, v3}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v3

    if-eqz p4, :cond_2

    const-wide/16 v4, 0x1c2

    .line 73
    :goto_2
    invoke-virtual {v3, v4, v5}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v3

    return-object v3

    .end local v0    # "changeBounds":Landroid/transition/Transition;
    .end local v2    # "splashTransition":Landroid/transition/Transition;
    :cond_0
    move v3, v5

    .line 61
    goto :goto_0

    .line 72
    .restart local v0    # "changeBounds":Landroid/transition/Transition;
    .restart local v2    # "splashTransition":Landroid/transition/Transition;
    :cond_1
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutLinearIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v3

    goto :goto_1

    .line 70
    :cond_2
    const-wide/16 v4, 0x15e

    goto :goto_2
.end method

.method private static windowTransition(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cardClassTypes"    # [Ljava/lang/Class;
    .param p2, "headerListLayout"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p3, "isEntering"    # Z

    .prologue
    const/4 v4, 0x0

    .line 99
    invoke-static {p2}, Lcom/google/android/play/transition/HeroReloadoHelper;->headerListLayoutTransitions(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/transition/Transition;

    move-result-object v1

    .line 101
    .local v1, "headerListLayoutTransitions":Landroid/transition/Transition;
    if-eqz p3, :cond_0

    .line 102
    const/4 v5, 0x1

    new-array v5, v5, [Landroid/transition/Transition;

    aput-object v1, v5, v4

    invoke-static {v5}, Lcom/google/android/play/transition/PlayTransitionUtil;->aggregate([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v4

    .line 103
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v4

    const-wide/16 v6, 0x1c2

    .line 104
    invoke-virtual {v4, v6, v7}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v4

    .line 116
    :goto_0
    return-object v4

    .line 106
    :cond_0
    new-instance v2, Landroid/transition/TransitionSet;

    invoke-direct {v2}, Landroid/transition/TransitionSet;-><init>()V

    .line 107
    .local v2, "set":Landroid/transition/TransitionSet;
    if-eqz p1, :cond_2

    .line 108
    new-instance v3, Landroid/transition/Slide;

    const/16 v5, 0x50

    invoke-direct {v3, v5}, Landroid/transition/Slide;-><init>(I)V

    .line 109
    .local v3, "slide":Landroid/transition/Transition;
    array-length v5, p1

    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v0, p1, v4

    .line 110
    .local v0, "cardClass":Ljava/lang/Class;
    invoke-virtual {v3, v0}, Landroid/transition/Transition;->addTarget(Ljava/lang/Class;)Landroid/transition/Transition;

    .line 109
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 112
    .end local v0    # "cardClass":Ljava/lang/Class;
    :cond_1
    invoke-virtual {v2, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 114
    .end local v3    # "slide":Landroid/transition/Transition;
    :cond_2
    invoke-virtual {v2, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v4

    .line 115
    invoke-static {p0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutLinearIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v4

    const-wide/16 v6, 0x12c

    .line 116
    invoke-virtual {v4, v6, v7}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v4

    goto :goto_0
.end method
