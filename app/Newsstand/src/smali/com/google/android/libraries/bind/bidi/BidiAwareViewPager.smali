.class public Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;
.super Landroid/support/v4/view/ViewPagerShim;
.source "BidiAwareViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPagerShim;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPagerShim;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getCurrentLogicalItem()I
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 34
    .local v0, "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    invoke-super {p0}, Landroid/support/v4/view/ViewPagerShim;->getCurrentItem()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    return v1
.end method

.method public getCurrentVisualItem()I
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/support/v4/view/ViewPagerShim;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public isRtl()Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 28
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 3
    .param p1, "layoutDirection"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPagerShim;->onRtlPropertiesChanged(I)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getCurrentLogicalItem()I

    move-result v0

    .line 58
    .local v0, "currentLogicalItem":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;

    if-ne p1, v2, :cond_1

    :goto_0
    invoke-interface {v1, v2}, Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;->setRtl(Z)V

    .line 63
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentLogicalItem(I)V

    .line 64
    return-void

    .line 59
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCurrentLogicalItem(I)V
    .locals 2
    .param p1, "logicalItem"    # I

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 43
    .local v0, "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    invoke-static {v0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 44
    .local v1, "visualItem":I
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(I)V

    .line 45
    return-void
.end method

.method public setCurrentLogicalItem(IZ)V
    .locals 2
    .param p1, "logicalItem"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    .line 49
    .local v0, "pagerAdapter":Landroid/support/v4/view/PagerAdapter;
    invoke-static {v0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 50
    .local v1, "visualItem":I
    invoke-virtual {p0, v1, p2}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(IZ)V

    .line 51
    return-void
.end method
