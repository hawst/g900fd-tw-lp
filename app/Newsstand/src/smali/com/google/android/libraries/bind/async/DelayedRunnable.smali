.class public Lcom/google/android/libraries/bind/async/DelayedRunnable;
.super Ljava/lang/Object;
.source "DelayedRunnable.java"


# instance fields
.field private final baseRunnable:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field private final lock:Ljava/lang/Object;

.field private scheduledTime:J

.field private final wrapperRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->baseRunnable:Ljava/lang/Runnable;

    .line 28
    iput-object p1, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->handler:Landroid/os/Handler;

    .line 29
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable$1;-><init>(Lcom/google/android/libraries/bind/async/DelayedRunnable;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->wrapperRunnable:Ljava/lang/Runnable;

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->wrapperRunnable:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->lock:Ljava/lang/Object;

    .line 39
    invoke-direct {p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->unschedule()V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/async/DelayedRunnable;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/bind/async/DelayedRunnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->unschedule()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/libraries/bind/async/DelayedRunnable;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->baseRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private isScheduled()Z
    .locals 4

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rescheduleAtTime(J)V
    .locals 5
    .param p1, "uptimeMillis"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->wrapperRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->wrapperRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->unschedule()V

    .line 83
    :cond_0
    return-void
.end method

.method private unschedule()V
    .locals 2

    .prologue
    .line 74
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    .line 75
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->wrapperRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 65
    invoke-direct {p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->unschedule()V

    .line 66
    monitor-exit v1

    .line 67
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public postDelayed(JI)Z
    .locals 7
    .param p1, "delayMillis"    # J
    .param p3, "runMode"    # I

    .prologue
    const/4 v2, 0x1

    .line 46
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    add-long v0, v4, p1

    .line 47
    .local v0, "thisTime":J
    iget-object v3, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->lock:Ljava/lang/Object;

    monitor-enter v3

    .line 48
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->isScheduled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 49
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->rescheduleAtTime(J)V

    .line 50
    const/4 v2, 0x0

    monitor-exit v3

    .line 57
    :goto_0
    return v2

    .line 52
    :cond_0
    const/4 v4, 0x3

    if-eq p3, v4, :cond_2

    if-nez p3, :cond_1

    iget-wide v4, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    cmp-long v4, v0, v4

    if-ltz v4, :cond_2

    :cond_1
    if-ne p3, v2, :cond_3

    iget-wide v4, p0, Lcom/google/android/libraries/bind/async/DelayedRunnable;->scheduledTime:J

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    .line 55
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->rescheduleAtTime(J)V

    .line 57
    :cond_3
    monitor-exit v3

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
