.class Lcom/google/android/libraries/bind/card/CardListBuilder$1;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "CardListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/card/CardListBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/card/CardListBuilder;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/card/CardListBuilder;
    .param p2, "primaryKey"    # I

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->this$0:Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    return-void
.end method

.method private validateFilterIsImmediate(Lcom/google/android/libraries/bind/data/Filter;)V
    .locals 2
    .param p1, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 50
    invoke-interface {p1}, Lcom/google/android/libraries/bind/data/Filter;->executor()Ljava/util/concurrent/Executor;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    if-eq v0, v1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "equalityFields"    # [I
    .param p2, "primaryKey"    # I
    .param p3, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 63
    invoke-direct {p0, p3}, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->validateFilterIsImmediate(Lcom/google/android/libraries/bind/data/Filter;)V

    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/data/DataList;->filter([IILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "equalityFields"    # [I
    .param p2, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 57
    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->validateFilterIsImmediate(Lcom/google/android/libraries/bind/data/Filter;)V

    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public refreshIfFailed(Z)V
    .locals 2
    .param p1, "invalidateData"    # Z

    .prologue
    .line 43
    if-eqz p1, :cond_0

    .line 44
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->this$0:Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->refreshIfFailed()V

    .line 47
    return-void
.end method
