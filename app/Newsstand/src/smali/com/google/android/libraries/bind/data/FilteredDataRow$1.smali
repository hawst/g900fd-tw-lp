.class Lcom/google/android/libraries/bind/data/FilteredDataRow$1;
.super Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;
.source "FilteredDataRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/data/FilteredDataRow;->makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/FilteredDataRow;

.field final synthetic val$rowData:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/FilteredDataRow;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/Filter;Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/data/FilteredDataRow;
    .param p2, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p3, "executor"    # Ljava/util/concurrent/Executor;
    .param p4, "filter"    # Lcom/google/android/libraries/bind/data/Filter;
    .param p5, "sourceList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    iput-object p6, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;->val$rowData:Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/Filter;Lcom/google/android/libraries/bind/data/DataList;)V

    return-void
.end method


# virtual methods
.method protected getSourceData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;->val$rowData:Lcom/google/android/libraries/bind/data/Data;

    if-nez v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataRow;

    # getter for: Lcom/google/android/libraries/bind/data/FilteredDataRow;->emptyWhenNone:Z
    invoke-static {v1}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->access$000(Lcom/google/android/libraries/bind/data/FilteredDataRow;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 63
    :goto_0
    return-object v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 62
    .local v0, "sourceData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;->val$rowData:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
