.class public interface abstract Lcom/google/android/libraries/bind/data/Filter;
.super Ljava/lang/Object;
.source "Filter.java"


# virtual methods
.method public abstract executor()Ljava/util/concurrent/Executor;
.end method

.method public abstract filterPriority()I
.end method

.method public abstract isReadOnly()Z
.end method

.method public abstract load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
.end method

.method public abstract onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V
.end method

.method public abstract onPreFilter()V
.end method

.method public abstract transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end method
