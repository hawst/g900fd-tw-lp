.class public Lcom/google/android/libraries/bind/widget/BindingLinearLayout;
.super Lcom/google/android/libraries/bind/widget/BoundLinearLayout;
.source "BindingLinearLayout.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/BindingViewGroup;


# instance fields
.field protected final bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->setupAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance v0, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->setupAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method private setupAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BindingLinearLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 49
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingLinearLayout_bind__isOwnedByParent:I

    const/4 v3, 0x0

    .line 50
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 51
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingLinearLayout_bind__supportsAnimationCapture:I

    const/4 v3, 0x1

    .line 52
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 51
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setSupportsAnimationCapture(Z)V

    .line 53
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 54
    return-void
.end method


# virtual methods
.method public blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "bitmapRect"    # Landroid/graphics/Rect;
    .param p3, "animationDuration"    # J
    .param p5, "blendMode"    # Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V

    .line 156
    return-void
.end method

.method public captureToBitmap(Landroid/graphics/Bitmap;FF)Z
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # F
    .param p3, "top"    # F

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->captureToBitmap(Landroid/graphics/Bitmap;FF)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->draw(Landroid/graphics/Canvas;)V

    .line 140
    return-void
.end method

.method public getBindingViewGroupHelper()Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    return-object v0
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public isOwnedByParent()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isOwnedByParent()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onAttachedToWindow()V

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onAttachedToWindow()V

    .line 118
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 135
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onDetachedFromWindow()V

    .line 111
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDetachedFromWindow()V

    .line 112
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onFinishInflate()V

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishInflate()V

    .line 106
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onFinishTemporaryDetach()V

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishTemporaryDetach()V

    .line 130
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onMeasure(II)V

    .line 185
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onMeasure(II)V

    .line 186
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->onStartTemporaryDetach()V

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onStartTemporaryDetach()V

    .line 124
    return-void
.end method

.method public prepareForRecycling()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->prepareForRecycling()V

    .line 90
    return-void
.end method

.method public setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V
    .locals 1
    .param p1, "cardGroup"    # Lcom/google/android/libraries/bind/card/CardGroup;
    .param p2, "cardGroupPosition"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V

    .line 95
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 64
    return-void
.end method

.method public final setMeasuredDimensionProxy(II)V
    .locals 0
    .param p1, "measuredWidth"    # I
    .param p2, "measuredHeight"    # I

    .prologue
    .line 179
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->setMeasuredDimension(II)V

    .line 180
    return-void
.end method

.method public setOwnedByParent(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 145
    return-void
.end method

.method public startEditingIfPossible()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->startEditingIfPossible()Z

    move-result v0

    return v0
.end method

.method public final superDrawProxy(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 174
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 175
    return-void
.end method

.method public final updateBoundDataProxy(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 170
    return-void
.end method
