.class public Lcom/google/android/libraries/bind/widget/WidgetUtil;
.super Ljava/lang/Object;
.source "WidgetUtil.java"


# static fields
.field private static tempLocation:[I

.field private static tempRect:Landroid/graphics/Rect;

.field private static tempRect2:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    .line 12
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doViewsIntersect(Landroid/view/View;Landroid/view/View;)Z
    .locals 8
    .param p0, "view1"    # Landroid/view/View;
    .param p1, "view2"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 59
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 60
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v1, v1, v6

    sget-object v2, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v2, v2, v7

    sget-object v3, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v3, v3, v6

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sget-object v4, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v4, v4, v7

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 60
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 62
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 63
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v1, v1, v6

    sget-object v2, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v2, v2, v7

    sget-object v3, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v3, v3, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sget-object v4, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    aget v4, v4, v7

    .line 64
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 63
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 65
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect2:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public static findViewAtPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "x"    # I
    .param p2, "y"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p3, "viewClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v4, 0x0

    .line 38
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    if-ge p1, v5, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    if-lt p2, v5, :cond_2

    :cond_0
    move-object p0, v4

    .line 55
    .end local p0    # "view":Landroid/view/View;
    :cond_1
    :goto_0
    return-object p0

    .line 41
    .restart local p0    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p3, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 44
    instance-of v5, p0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_4

    move-object v3, p0

    .line 45
    check-cast v3, Landroid/view/ViewGroup;

    .line 46
    .local v3, "rootViewGroup":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 47
    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "childView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int v5, p1, v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v6, p2, v6

    invoke-static {v0, v5, v6, p3}, Lcom/google/android/libraries/bind/widget/WidgetUtil;->findViewAtPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .line 50
    .local v2, "result":Ljava/lang/Object;, "TT;"
    if-eqz v2, :cond_3

    move-object p0, v2

    .line 51
    goto :goto_0

    .line 46
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "childView":Landroid/view/View;
    .end local v1    # "i":I
    .end local v2    # "result":Ljava/lang/Object;, "TT;"
    .end local v3    # "rootViewGroup":Landroid/view/ViewGroup;
    :cond_4
    move-object p0, v4

    .line 55
    goto :goto_0
.end method

.method public static findViewAtScreenPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "screenX"    # I
    .param p2, "screenY"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/view/View;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 25
    .local p3, "viewClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v2, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 26
    sget-object v2, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-int v0, p1, v2

    .line 27
    .local v0, "x":I
    sget-object v2, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempLocation:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    sub-int v1, p2, v2

    .line 28
    .local v1, "y":I
    invoke-static {p0, v0, v1, p3}, Lcom/google/android/libraries/bind/widget/WidgetUtil;->findViewAtPosition(Landroid/view/View;IILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public static isVisibleOnScreen(Landroid/view/View;)Z
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/libraries/bind/widget/WidgetUtil;->tempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method
