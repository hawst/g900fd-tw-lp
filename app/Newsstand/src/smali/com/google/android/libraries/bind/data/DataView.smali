.class public interface abstract Lcom/google/android/libraries/bind/data/DataView;
.super Ljava/lang/Object;
.source "DataView.java"


# virtual methods
.method public abstract getData()Lcom/google/android/libraries/bind/data/Data;
.end method

.method public abstract getDataRow()Lcom/google/android/libraries/bind/data/DataList;
.end method

.method public abstract onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
.end method

.method public abstract setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
.end method
