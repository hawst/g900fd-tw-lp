.class public abstract Lcom/google/android/libraries/bind/data/BaseDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "BaseDataSetObserver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onChanged()V
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BaseDataSetObserver;->onChanged()V

    .line 16
    return-void
.end method
