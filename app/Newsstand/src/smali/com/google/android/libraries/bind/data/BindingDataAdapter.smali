.class public Lcom/google/android/libraries/bind/data/BindingDataAdapter;
.super Lcom/google/android/libraries/bind/data/DataAdapter;
.source "BindingDataAdapter.java"


# static fields
.field private static viewResIdMap:Landroid/util/SparseIntArray;

.field private static final viewResIds:[I


# instance fields
.field private final a11yCardCountKey:I

.field private final equalityFieldsKey:I

.field private final listEqualityFields:[I

.field private final listFilter:Lcom/google/android/libraries/bind/data/Filter;

.field private originalDataList:Lcom/google/android/libraries/bind/data/DataList;

.field private final viewGeneratorKey:I

.field private final viewResourceIdKey:I

.field private final viewTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/libraries/bind/R$layout;->bind__card_edit_placeholder:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/libraries/bind/R$layout;->bind__card_list_padding:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIds:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
    .locals 6
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 72
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_GENERATOR:I

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_A11Y_COUNT:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;IIII)V

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;IIII)V
    .locals 4
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "viewResourceIdKey"    # I
    .param p3, "viewGeneratorKey"    # I
    .param p4, "equalityFieldsKey"    # I
    .param p5, "a11yCardCountKey"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    .line 48
    new-instance v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter$1;-><init>(Lcom/google/android/libraries/bind/data/BindingDataAdapter;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->listFilter:Lcom/google/android/libraries/bind/data/Filter;

    .line 61
    sget-object v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "BindingDataAdapter.init() needs to be called before instantiating a BindingDataAdapter!"

    invoke-static {v0, v3}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 63
    iput p2, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    .line 64
    iput p3, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    .line 65
    iput p4, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->equalityFieldsKey:I

    .line 66
    iput p5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->a11yCardCountKey:I

    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [I

    aput p2, v0, v2

    aput p3, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->listEqualityFields:[I

    .line 68
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->setNotifyOnlyIfPrimaryKeyAffected(Z)V

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 61
    goto :goto_0
.end method

.method public static init([I)V
    .locals 4
    .param p0, "layoutResIds"    # [I

    .prologue
    .line 31
    sget-object v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "BindingDataAdapter.init() called more than once!"

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 33
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIds:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 35
    sget-object v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    sget-object v2, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIds:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 31
    .end local v0    # "i":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 37
    .restart local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    :goto_2
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 38
    sget-object v1, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    aget v2, p0, v0

    sget-object v3, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIds:[I

    array-length v3, v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 40
    :cond_2
    return-void
.end method

.method private recomputeViewTypes()V
    .locals 9

    .prologue
    .line 133
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 135
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 136
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 137
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_0

    .line 139
    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 151
    .local v4, "viewType":Ljava/lang/Integer;
    :goto_1
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v4    # "viewType":Ljava/lang/Integer;
    :cond_0
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 142
    .local v3, "viewResId":Ljava/lang/Integer;
    if-nez v3, :cond_1

    .line 144
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 145
    .local v2, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-interface {v2}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getViewResId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 147
    .end local v2    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_1
    sget-object v5, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 148
    .restart local v4    # "viewType":Ljava/lang/Integer;
    if-eqz v4, :cond_2

    const/4 v5, 0x1

    :goto_2
    const-string v7, "Missing mapping for resource type: "

    .line 149
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/libraries/bind/util/Util;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 148
    :goto_3
    invoke-static {v5, v6}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 149
    :cond_3
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 153
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "viewResId":Ljava/lang/Integer;
    .end local v4    # "viewType":Ljava/lang/Integer;
    :cond_4
    return-void
.end method


# virtual methods
.method public dataList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public findRowWithCardId(Ljava/lang/Object;)I
    .locals 7
    .param p1, "cardId"    # Ljava/lang/Object;

    .prologue
    .line 166
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getCount()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 167
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 168
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_2

    .line 169
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v5, v3}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 184
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "i":I
    :goto_1
    return v3

    .line 174
    .restart local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v3    # "i":I
    :cond_0
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 175
    .local v4, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-interface {v4}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardIds()Ljava/util/List;

    move-result-object v2

    .line 176
    .local v2, "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 177
    .local v1, "generatorCardId":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    .line 166
    .end local v1    # "generatorCardId":Ljava/lang/Object;
    .end local v2    # "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v4    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public getA11yRowCount(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    .line 205
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 207
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return v2

    .line 210
    :cond_1
    iget v3, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->a11yCardCountKey:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 211
    .local v0, "count":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewTypes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getRowFirstCardId(I)Ljava/lang/Object;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 188
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->getItem(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 189
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez v0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-object v3

    .line 192
    :cond_1
    iget v4, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 193
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 195
    :cond_2
    iget v4, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 196
    .local v2, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-interface {v2}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardIds()Ljava/util/List;

    move-result-object v1

    .line 197
    .local v1, "generatorCardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 198
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 96
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 97
    .local v4, "viewResId":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 98
    .local v2, "view":Landroid/view/View;
    if-nez v4, :cond_1

    .line 100
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewGeneratorKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 103
    .local v3, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    if-eqz v3, :cond_0

    const/4 v5, 0x1

    :goto_0
    const-string v6, "Missing both view resource ID and view generator"

    invoke-static {v5, v6}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 105
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-interface {v3, p2, p3, v5}, Lcom/google/android/libraries/bind/card/ViewGenerator;->makeView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v2

    .line 117
    .end local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :goto_1
    return-object v2

    .line 103
    .restart local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 108
    .end local v3    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_1
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->makeLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-virtual {v5, v6, p2, v7}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    .line 109
    .local v0, "bindingLayout":Landroid/view/View;
    iget v5, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->equalityFieldsKey:I

    invoke-virtual {p4, v5}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 110
    .local v1, "equalityFields":[I
    instance-of v5, v0, Lcom/google/android/libraries/bind/data/DataView;

    if-eqz v5, :cond_2

    move-object v5, v0

    .line 111
    check-cast v5, Lcom/google/android/libraries/bind/data/DataView;

    iget-object v6, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v7, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 112
    invoke-virtual {v7, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v7

    new-instance v8, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;

    .line 113
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget v10, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResourceIdKey:I

    invoke-direct {v8, v9, v10}, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;-><init>(II)V

    .line 111
    invoke-virtual {v6, v7, v8, v1}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/libraries/bind/data/DataView;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 115
    :cond_2
    move-object v2, v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->viewResIdMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    return v0
.end method

.method protected notifyOnChanged()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->recomputeViewTypes()V

    .line 123
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnChanged()V

    .line 124
    return-void
.end method

.method protected notifyOnInvalidated()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->recomputeViewTypes()V

    .line 129
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnInvalidated()V

    .line 130
    return-void
.end method

.method public setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;
    .locals 3
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-ne p1, v0, :cond_0

    .line 86
    :goto_0
    return-object p0

    .line 81
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 82
    if-eqz p1, :cond_1

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->originalDataList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->listEqualityFields:[I

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->listFilter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object p1

    .line 85
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/data/DataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/DataAdapter;

    goto :goto_0
.end method

.method public bridge synthetic setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/DataAdapter;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/BindingDataAdapter;->setDataList(Lcom/google/android/libraries/bind/data/DataList;)Lcom/google/android/libraries/bind/data/BindingDataAdapter;

    move-result-object v0

    return-object v0
.end method
