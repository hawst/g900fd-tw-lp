.class public Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
.super Ljava/lang/Object;
.source "FragmentAdapterKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/libraries/bind/data/FragmentAdapterKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final key:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/FragmentAdapterKey$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 21
    instance-of v1, p1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 22
    check-cast v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    .line 23
    .local v0, "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    .line 25
    .end local v0    # "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    const-string v0, "key: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/util/ParcelUtil;->writeObjectToParcel(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 46
    return-void
.end method
