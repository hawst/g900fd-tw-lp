.class public Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;
.super Lcom/google/android/libraries/bind/data/RefreshTask;
.source "FilteredDataList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/FilteredDataList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "FilterRefreshTask"
.end annotation


# instance fields
.field protected final filter:Lcom/google/android/libraries/bind/data/Filter;

.field protected final newSourceDataVersion:I

.field protected final sourceException:Lcom/google/android/libraries/bind/data/DataException;

.field protected final sourceSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/Filter;Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "filter"    # Lcom/google/android/libraries/bind/data/Filter;
    .param p4, "sourceList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 129
    if-nez p3, :cond_0

    .end local p2    # "executor":Ljava/util/concurrent/Executor;
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/data/RefreshTask;-><init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;)V

    .line 130
    invoke-virtual {p4}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 131
    invoke-virtual {p4}, Lcom/google/android/libraries/bind/data/DataList;->dataVersion()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->newSourceDataVersion:I

    .line 132
    invoke-virtual {p4}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceException:Lcom/google/android/libraries/bind/data/DataException;

    .line 133
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    .line 134
    return-void

    .line 129
    .restart local p2    # "executor":Ljava/util/concurrent/Executor;
    :cond_0
    invoke-interface {p3}, Lcom/google/android/libraries/bind/data/Filter;->executor()Ljava/util/concurrent/Executor;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method protected getFreshData()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 151
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceException:Lcom/google/android/libraries/bind/data/DataException;

    if-eqz v4, :cond_0

    .line 152
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceException:Lcom/google/android/libraries/bind/data/DataException;

    throw v3

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->getSourceData()Ljava/util/List;

    move-result-object v2

    .line 155
    .local v2, "sourceData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v2, v3

    .line 174
    .end local v2    # "sourceData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_1
    :goto_0
    return-object v2

    .line 158
    .restart local v2    # "sourceData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_2
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    if-eqz v4, :cond_1

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 162
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    if-eqz v4, :cond_6

    .line 163
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 164
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-interface {v5, v0, p0}, Lcom/google/android/libraries/bind/data/Filter;->load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 167
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 169
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v2, v3

    .line 170
    goto :goto_0

    .line 172
    :cond_5
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-interface {v3, v1, p0}, Lcom/google/android/libraries/bind/data/Filter;->transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;

    move-result-object v1

    :cond_6
    move-object v2, v1

    .line 174
    goto :goto_0
.end method

.method protected getSourceData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/Filter;->isReadOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    .line 146
    :goto_0
    return-object v0

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->sourceSnapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    .line 146
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Snapshot;->cloneList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/RefreshTask;->onPreExecute()V

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->filter:Lcom/google/android/libraries/bind/data/Filter;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/Filter;->onPreFilter()V

    .line 142
    :cond_0
    return-void
.end method

.method protected postRefresh(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 2
    .param p1, "snapshot"    # Lcom/google/android/libraries/bind/data/Snapshot;
    .param p2, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->dataList:Lcom/google/android/libraries/bind/data/DataList;

    iget v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$FilterRefreshTask;->newSourceDataVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/libraries/bind/data/DataList;->postRefresh(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 180
    return-void
.end method
