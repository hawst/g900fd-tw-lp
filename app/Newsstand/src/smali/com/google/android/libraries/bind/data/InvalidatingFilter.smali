.class public abstract Lcom/google/android/libraries/bind/data/InvalidatingFilter;
.super Lcom/google/android/libraries/bind/data/BaseFilter;
.source "InvalidatingFilter.java"


# instance fields
.field protected invalidationDataList:Lcom/google/android/libraries/bind/data/DataList;

.field private final isReadOnly:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/async/Queue;Z)V
    .locals 0
    .param p1, "queue"    # Lcom/google/android/libraries/bind/async/Queue;
    .param p2, "isReadOnly"    # Z

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/BaseFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 18
    iput-boolean p2, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->isReadOnly:Z

    .line 19
    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->invalidationDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->invalidationDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 36
    :cond_0
    return-void
.end method

.method public isReadOnly()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->isReadOnly:Z

    return v0
.end method

.method public onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->invalidationDataList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 29
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/InvalidatingFilter;->invalidationDataList:Lcom/google/android/libraries/bind/data/DataList;

    .line 30
    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
