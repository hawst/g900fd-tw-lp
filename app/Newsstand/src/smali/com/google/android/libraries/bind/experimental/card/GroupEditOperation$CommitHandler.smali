.class public interface abstract Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;
.super Ljava/lang/Object;
.source "GroupEditOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommitHandler"
.end annotation


# virtual methods
.method public abstract commitEditOperation(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
.end method

.method public abstract onOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
.end method
