.class public Lcom/google/android/libraries/bind/view/ViewHeap;
.super Ljava/lang/Object;
.source "ViewHeap.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final inUseDebugMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final context:Landroid/content/Context;

.field private final heap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private inflatedCount:I

.field private final inflater:Landroid/view/LayoutInflater;

.field private isDisabledUntilNextGet:Z

.field private final reclaimViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private returnedCount:I

.field private reusedCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->heap:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->reclaimViews:Ljava/util/List;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->isDisabledUntilNextGet:Z

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->context:Landroid/content/Context;

    .line 52
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->inflater:Landroid/view/LayoutInflater;

    .line 63
    return-void
.end method

.method private efficiencyPercent()I
    .locals 3

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->reusedCount:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->reusedCount:I

    iget v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->inflatedCount:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    return v0
.end method

.method private getResourceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "viewResId"    # I

    .prologue
    .line 223
    sget-object v0, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/logging/Logd;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-static {p1}, Lcom/google/android/libraries/bind/util/Util;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private logInflate(I)V
    .locals 5
    .param p1, "viewResId"    # I

    .prologue
    .line 128
    iget v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->inflatedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->inflatedCount:I

    .line 129
    sget-object v0, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "Inflating view of type %s, efficiency: %d %%"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/view/ViewHeap;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 130
    invoke-direct {p0}, Lcom/google/android/libraries/bind/view/ViewHeap;->efficiencyPercent()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 129
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    return-void
.end method

.method private logReuse(IZ)V
    .locals 5
    .param p1, "viewResId"    # I
    .param p2, "fromViewHeap"    # Z

    .prologue
    .line 119
    iget v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->reusedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->reusedCount:I

    .line 120
    if-nez p2, :cond_0

    .line 121
    iget v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->returnedCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->returnedCount:I

    .line 123
    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v1, "Reusing view of type %s, efficiency: %d %%"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/view/ViewHeap;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 124
    invoke-direct {p0}, Lcom/google/android/libraries/bind/view/ViewHeap;->efficiencyPercent()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 123
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    return-void
.end method

.method private transferOwnershipToHeap(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 202
    .local v0, "viewResId":Ljava/lang/Integer;
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->isDisabledUntilNextGet:Z

    if-eqz v2, :cond_1

    .line 203
    sget-object v2, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "The heap is temporarily disabled after being cleared, not recycling view of type %s"

    new-array v3, v3, [Ljava/lang/Object;

    .line 205
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/google/android/libraries/bind/view/ViewHeap;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    .line 203
    invoke-virtual {v2, v5, v3}, Lcom/google/android/libraries/bind/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    :goto_1
    return-void

    .end local v0    # "viewResId":Ljava/lang/Integer;
    :cond_0
    move v2, v4

    .line 200
    goto :goto_0

    .line 208
    .restart local v0    # "viewResId":Ljava/lang/Integer;
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->heap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 209
    .local v1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    if-nez v1, :cond_2

    .line 210
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .restart local v1    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v2, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "Recycled view of type %s, got %d in heap"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/google/android/libraries/bind/view/ViewHeap;->getResourceName(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v2, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    iget-object v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->heap:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v2, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    if-eqz v2, :cond_3

    .line 216
    sget-object v2, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    :goto_2
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Recycling a view we didn\'t create: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 219
    :cond_3
    iget v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->returnedCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->returnedCount:I

    goto :goto_1

    :cond_4
    move v3, v4

    .line 216
    goto :goto_2
.end method

.method private tryRecycleChildren(Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 185
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 186
    .local v0, "childView":Landroid/view/View;
    instance-of v2, v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .line 187
    invoke-interface {v2}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->isOwnedByParent()Z

    move-result v2

    if-nez v2, :cond_1

    .line 188
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 189
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/view/ViewHeap;->recycle(Landroid/view/View;)V

    .line 184
    .end local v0    # "childView":Landroid/view/View;
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 190
    .restart local v0    # "childView":Landroid/view/View;
    :cond_1
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 191
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "childView":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/view/ViewHeap;->tryRecycleChildren(Landroid/view/ViewGroup;)V

    goto :goto_1

    .line 194
    :cond_2
    return-void
.end method


# virtual methods
.method public clearHeap()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->heap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->isDisabledUntilNextGet:Z

    .line 68
    return-void
.end method

.method public get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 10
    .param p1, "viewResId"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 71
    if-eqz p3, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 72
    iput-boolean v6, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->isDisabledUntilNextGet:Z

    .line 73
    if-nez p2, :cond_3

    .line 74
    .local v0, "convertViewResId":Ljava/lang/Integer;
    :goto_1
    sget-object v7, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v8, "convertViewResId: %s"

    new-array v9, v5, [Ljava/lang/Object;

    if-nez v0, :cond_4

    const-string v4, "null"

    .line 75
    :goto_2
    aput-object v4, v9, v6

    .line 74
    invoke-virtual {v7, v8, v9}, Lcom/google/android/libraries/bind/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    if-eqz v0, :cond_5

    .line 78
    instance-of v4, p2, Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    .line 79
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v7, "recycling children"

    new-array v8, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v7, v8}, Lcom/google/android/libraries/bind/logging/Logd;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, p2

    .line 80
    check-cast v4, Landroid/view/ViewGroup;

    invoke-direct {p0, v4}, Lcom/google/android/libraries/bind/view/ViewHeap;->tryRecycleChildren(Landroid/view/ViewGroup;)V

    .line 83
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne p1, v4, :cond_5

    .line 84
    invoke-direct {p0, p1, v6}, Lcom/google/android/libraries/bind/view/ViewHeap;->logReuse(IZ)V

    .line 85
    invoke-virtual {p2, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    if-eqz v4, :cond_1

    .line 87
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    invoke-virtual {v4, p2}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 115
    .end local p2    # "convertView":Landroid/view/View;
    :cond_1
    :goto_3
    return-object p2

    .end local v0    # "convertViewResId":Ljava/lang/Integer;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_2
    move v4, v6

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    move-object v0, v4

    goto :goto_1

    .line 75
    .restart local v0    # "convertViewResId":Ljava/lang/Integer;
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/libraries/bind/view/ViewHeap;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 93
    :cond_5
    iget-object v4, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->heap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 94
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v2, 0x0

    .line 95
    .local v2, "view":Landroid/view/View;
    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 96
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "view":Landroid/view/View;
    check-cast v2, Landroid/view/View;

    .line 98
    .restart local v2    # "view":Landroid/view/View;
    :cond_6
    if-nez v2, :cond_8

    .line 100
    :try_start_0
    iget-object v4, p0, Lcom/google/android/libraries/bind/view/ViewHeap;->inflater:Landroid/view/LayoutInflater;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, p1, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 106
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/view/ViewHeap;->logInflate(I)V

    .line 111
    :goto_4
    invoke-virtual {v2, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    if-eqz v4, :cond_7

    .line 113
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->inUseDebugMap:Ljava/util/WeakHashMap;

    invoke-virtual {v4, v2, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_9

    :goto_5
    invoke-static {v5}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    :cond_7
    move-object p2, v2

    .line 115
    goto :goto_3

    .line 101
    :catch_0
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/google/android/libraries/bind/view/ViewHeap;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v7, "Failed to inflate view resource: %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/libraries/bind/util/Util;->getResourceName(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-virtual {v4, v7, v5}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    throw v1

    .line 109
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_8
    invoke-direct {p0, p1, v5}, Lcom/google/android/libraries/bind/view/ViewHeap;->logReuse(IZ)V

    goto :goto_4

    :cond_9
    move v5, v6

    .line 113
    goto :goto_5
.end method

.method public recycle(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 153
    .local v0, "viewResId":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 157
    :cond_0
    instance-of v1, p1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->isOwnedByParent()Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, p1

    .line 158
    check-cast v1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->prepareForRecycling()V

    .line 161
    :cond_1
    instance-of v1, p1, Lcom/google/android/libraries/bind/card/GroupRowLayout;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 162
    check-cast v1, Lcom/google/android/libraries/bind/card/GroupRowLayout;

    invoke-interface {v1}, Lcom/google/android/libraries/bind/card/GroupRowLayout;->prepareGroupRowForRecycling()V

    .line 165
    :cond_2
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    move-object v1, p1

    .line 167
    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/view/ViewHeap;->tryRecycleChildren(Landroid/view/ViewGroup;)V

    .line 170
    :cond_3
    instance-of v1, p1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    invoke-interface {v1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->isOwnedByParent()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 173
    :cond_4
    invoke-virtual {p1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 177
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/view/ViewHeap;->transferOwnershipToHeap(Landroid/view/View;)V

    goto :goto_0
.end method
