.class final Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState$1;
.super Ljava/lang/Object;
.source "FragmentStateDataPagerAdapter.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 136
    const-class v2, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 137
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 136
    invoke-static {p1, v2}, Lcom/google/android/libraries/bind/util/ParcelUtil;->readObjectFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    .line 138
    .local v0, "key":Ljava/lang/Object;
    const-class v2, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 139
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment$SavedState;

    .line 140
    .local v1, "state":Landroid/support/v4/app/Fragment$SavedState;
    new-instance v2, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    invoke-direct {v2, v0, v1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;-><init>(Ljava/lang/Object;Landroid/support/v4/app/Fragment$SavedState;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 145
    new-array v0, p1, [Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState$1;->newArray(I)[Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    move-result-object v0

    return-object v0
.end method
