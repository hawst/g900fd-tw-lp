.class Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;
.super Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;
.source "CardGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditFilter"
.end annotation


# instance fields
.field private currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

.field private final tempOperations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/libraries/bind/card/CardGroup;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/card/CardGroup;)V
    .locals 1

    .prologue
    .line 652
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    .line 653
    sget-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/data/BaseReadonlyFilter;-><init>(Ljava/util/concurrent/Executor;)V

    .line 650
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    .line 654
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    return-object v0
.end method

.method private applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V
    .locals 7
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .param p3, "committed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 714
    .local p2, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz p1, :cond_0

    .line 715
    iget v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v6, :pswitch_data_0

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 718
    :pswitch_0
    if-nez p3, :cond_0

    .line 719
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v5

    .line 720
    .local v5, "replacePos":I
    if-ltz v5, :cond_0

    .line 721
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 722
    .local v2, "originalCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-direct {p0, v2}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 723
    .local v3, "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {p2, v5, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 729
    .end local v2    # "originalCard":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    .end local v5    # "replacePos":I
    :pswitch_1
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v4

    .line 730
    .local v4, "removePos":I
    if-ltz v4, :cond_0

    .line 731
    invoke-interface {p2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 736
    .end local v4    # "removePos":I
    :pswitch_2
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-direct {p0, p2, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->findId(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 737
    .local v0, "editRow":I
    if-ltz v0, :cond_0

    .line 738
    iget-object v6, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 739
    .local v1, "newPosition":I
    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    .line 740
    .restart local v2    # "originalCard":Lcom/google/android/libraries/bind/data/Data;
    if-eqz p3, :cond_1

    .line 741
    invoke-interface {p2, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 743
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    .line 744
    .restart local v3    # "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    invoke-interface {p2, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 715
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private findId(Ljava/util/List;Ljava/lang/Object;)I
    .locals 4
    .param p2, "id"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 761
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    if-eqz p2, :cond_1

    .line 762
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 763
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v3}, Lcom/google/android/libraries/bind/card/CardGroup;->access$400(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 764
    .local v0, "currentId":Ljava/lang/Object;
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 769
    .end local v0    # "currentId":Ljava/lang/Object;
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 762
    .restart local v0    # "currentId":Ljava/lang/Object;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 769
    .end local v0    # "currentId":Ljava/lang/Object;
    .end local v1    # "i":I
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private makePlaceHolder(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "originalCard"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 752
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 753
    .local v0, "placeHolderCard":Lcom/google/android/libraries/bind/data/Data;
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v1

    .line 754
    .local v1, "primaryKey":I
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 755
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    iget v2, v2, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    sget v3, Lcom/google/android/libraries/bind/experimental/card/CardEditPlaceHolder;->LAYOUT:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 756
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    iget v2, v2, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    sget-object v3, Lcom/google/android/libraries/bind/experimental/card/CardEditPlaceHolder;->EQUALITY_FIELDS:[I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 757
    return-object v0
.end method


# virtual methods
.method public clearCommittedOperations()V
    .locals 5

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 686
    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardGroup;->access$200()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    const-string v1, "Clearing %d temporary operations"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 687
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 689
    :cond_0
    return-void
.end method

.method public commitOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 2

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 679
    .local v0, "committedOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 681
    return-object v0
.end method

.method public editOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 1

    .prologue
    .line 671
    monitor-enter p0

    .line 672
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    monitor-exit p0

    return-object v0

    .line 673
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 5
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 657
    const/4 v0, 0x0

    .line 658
    .local v0, "invalidate":Z
    monitor-enter p0

    .line 659
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 660
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 661
    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardGroup;->access$200()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v1

    const-string v2, "Current operation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 662
    const/4 v0, 0x1

    .line 664
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    if-eqz v0, :cond_1

    .line 666
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {v1}, Lcom/google/android/libraries/bind/card/CardGroup;->access$300(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->invalidateData()V

    .line 668
    :cond_1
    return-void

    .line 664
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 8
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 693
    const/4 v0, 0x0

    .line 694
    .local v0, "editOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    monitor-enter p0

    .line 695
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 696
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 699
    # getter for: Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/CardGroup;->access$200()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v2

    const-string v3, "Playing back %d temporary operations"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 701
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->tempOperations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 702
    .local v1, "operation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-direct {p0, v1, p1, v7}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V

    goto :goto_1

    .line 695
    .end local v1    # "operation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    :cond_1
    :try_start_1
    new-instance v2, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;-><init>(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    move-object v0, v2

    goto :goto_0

    .line 696
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 705
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->allowEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 706
    invoke-direct {p0, v0, p1, v6}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->applyOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;Ljava/util/List;Z)V

    .line 709
    :cond_3
    return-object p1
.end method
