.class public Lcom/google/android/libraries/bind/data/PriorityDataObservable;
.super Ljava/lang/Object;
.source "PriorityDataObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    }
.end annotation


# instance fields
.field private final observerEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/libraries/bind/data/DataObserver;I)Z
    .locals 4
    .param p1, "observer"    # Lcom/google/android/libraries/bind/data/DataObserver;
    .param p2, "priority"    # I

    .prologue
    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Observer is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 38
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    .line 39
    .local v1, "wasEmpty":Z
    new-instance v0, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;

    invoke-direct {v0, p1, p2}, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;-><init>(Lcom/google/android/libraries/bind/data/DataObserver;I)V

    .line 40
    .local v0, "entry":Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 42
    return v1
.end method

.method public notifyDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 4
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 68
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 69
    .local v0, "copy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;

    .line 70
    .local v1, "entry":Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    iget-object v3, v1, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v3, p1}, Lcom/google/android/libraries/bind/data/DataObserver;->onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V

    goto :goto_0

    .line 72
    .end local v1    # "entry":Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    :cond_0
    return-void
.end method

.method public remove(Lcom/google/android/libraries/bind/data/DataObserver;)Z
    .locals 3
    .param p1, "observer"    # Lcom/google/android/libraries/bind/data/DataObserver;

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Observer is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    const/4 v1, 0x0

    .line 63
    :goto_0
    return v1

    .line 57
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 58
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    if-ne v1, p1, :cond_3

    .line 59
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    goto :goto_0

    .line 57
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public size()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "  observers count: %d\n"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/PriorityDataObservable;->observerEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;

    .line 83
    .local v1, "entry":Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    const-string v3, "    "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 87
    .end local v1    # "entry":Lcom/google/android/libraries/bind/data/PriorityDataObservable$ObserverEntry;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
