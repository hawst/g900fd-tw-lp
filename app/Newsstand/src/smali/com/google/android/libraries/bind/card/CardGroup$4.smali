.class Lcom/google/android/libraries/bind/card/CardGroup$4;
.super Ljava/lang/Object;
.source "CardGroup.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataProperty;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/card/CardGroup;->makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/bind/data/DataProperty",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/card/CardGroup;

.field final synthetic val$viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/card/CardGroup;Lcom/google/android/libraries/bind/card/ViewGenerator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 542
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup$4;->this$0:Lcom/google/android/libraries/bind/card/CardGroup;

    iput-object p2, p0, Lcom/google/android/libraries/bind/card/CardGroup$4;->val$viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/Integer;
    .locals 1
    .param p1, "self"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$4;->val$viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 542
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup$4;->apply(Lcom/google/android/libraries/bind/data/Data;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
