.class public Lcom/google/android/libraries/bind/data/DataChange;
.super Ljava/lang/Object;
.source "DataChange.java"


# static fields
.field public static final AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

.field public static final DOESNT_AFFECT_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

.field public static final INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;


# instance fields
.field public final affectsPrimaryKey:Z

.field public final hasRefreshException:Z

.field public final isInvalidation:Z

.field public final refreshException:Lcom/google/android/libraries/bind/data/DataException;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7
    new-instance v0, Lcom/google/android/libraries/bind/data/DataChange;

    invoke-direct {v0, v2, v2}, Lcom/google/android/libraries/bind/data/DataChange;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    .line 8
    new-instance v0, Lcom/google/android/libraries/bind/data/DataChange;

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/data/DataChange;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    .line 9
    new-instance v0, Lcom/google/android/libraries/bind/data/DataChange;

    invoke-direct {v0, v1, v1}, Lcom/google/android/libraries/bind/data/DataChange;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/DataChange;->DOESNT_AFFECT_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataException;)V
    .locals 1
    .param p1, "refreshException"    # Lcom/google/android/libraries/bind/data/DataException;

    .prologue
    const/4 v0, 0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataChange;->isInvalidation:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    .line 33
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataChange;->hasRefreshException:Z

    .line 34
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataChange;->refreshException:Lcom/google/android/libraries/bind/data/DataException;

    .line 35
    return-void
.end method

.method protected constructor <init>(ZZ)V
    .locals 1
    .param p1, "isInvalidation"    # Z
    .param p2, "affectsPrimaryKey"    # Z

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/DataChange;->isInvalidation:Z

    .line 39
    iput-boolean p2, p0, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataChange;->hasRefreshException:Z

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataChange;->refreshException:Lcom/google/android/libraries/bind/data/DataException;

    .line 42
    return-void
.end method

.method public static get(ZZ)Lcom/google/android/libraries/bind/data/DataChange;
    .locals 1
    .param p0, "isInvalidation"    # Z
    .param p1, "affectsPrimaryKey"    # Z

    .prologue
    .line 12
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/libraries/bind/data/DataChange;->AFFECTS_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/libraries/bind/data/DataChange;->DOESNT_AFFECT_PRIMARY_KEY:Lcom/google/android/libraries/bind/data/DataChange;

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    const-string v0, "isInvalidation: %b, affectsPrimaryKey: %b, exception: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/data/DataChange;->isInvalidation:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    .line 47
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataChange;->refreshException:Lcom/google/android/libraries/bind/data/DataException;

    aput-object v3, v1, v2

    .line 46
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
