.class public abstract Lcom/google/android/libraries/bind/card/CardGroup;
.super Ljava/lang/Object;
.source "CardGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;,
        Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;,
        Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    }
.end annotation


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field private static final editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;


# instance fields
.field protected final a11yCardCountKey:I

.field private cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

.field private commitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

.field private editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

.field private editList:Lcom/google/android/libraries/bind/data/DataList;

.field private emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

.field protected final equalityFieldsKey:I

.field private errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

.field private final extraRowMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;>;"
        }
    .end annotation
.end field

.field private final filteredListIndexMap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final footers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private groupId:Ljava/lang/String;

.field private final groupList:Lcom/google/android/libraries/bind/data/DataList;

.field private final groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private final headerIndexMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private hideOnError:Z

.field private final listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

.field private monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

.field private final realCardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field private registered:Z

.field private rowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation
.end field

.field protected final viewGeneratorKey:I

.field protected final viewResourceIdKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/libraries/bind/card/CardGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/CardGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 6
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 154
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    sget v3, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_GENERATOR:I

    sget v4, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_EQUALITY_FIELDS:I

    sget v5, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_A11Y_COUNT:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/card/CardGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;IIII)V

    .line 156
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;IIII)V
    .locals 3
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "viewResourceIdKey"    # I
    .param p3, "viewGeneratorKey"    # I
    .param p4, "equalityFieldsKey"    # I
    .param p5, "a11yCardCountKey"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->footers:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headerIndexMap:Ljava/util/Map;

    .line 115
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    .line 122
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$2;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup$2;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 129
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$3;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup$3;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 143
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 144
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    .line 145
    iput p2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    .line 146
    iput p3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewGeneratorKey:I

    .line 147
    iput p4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    .line 148
    iput p5, p0, Lcom/google/android/libraries/bind/card/CardGroup;->a11yCardCountKey:I

    .line 149
    new-array v0, v1, [I

    aput p2, v0, v2

    new-instance v1, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;

    invoke-direct {v1}, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;-><init>()V

    .line 150
    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    .line 151
    return-void

    :cond_0
    move v0, v2

    .line 143
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/libraries/bind/card/CardGroup;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/CardGroup;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method private addExtraRows()V
    .locals 5

    .prologue
    .line 492
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 493
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 495
    .local v1, "rowIndex":I
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v1, :cond_0

    .line 496
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->extraRowMap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "extra "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 492
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 499
    .end local v1    # "rowIndex":I
    :cond_1
    return-void
.end method

.method private addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "rowToAdd"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "rowId"    # Ljava/lang/String;

    .prologue
    .line 514
    invoke-virtual {p2}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 515
    .local v0, "rowCopy":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {v0, v1, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 516
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 517
    return-void
.end method

.method private addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V
    .locals 8
    .param p1, "position"    # I
    .param p3, "prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 502
    .local p2, "rowsToAdd":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le p1, v2, :cond_1

    .line 511
    :cond_0
    return-void

    .line 506
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 507
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/Data;->copy()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 508
    .local v1, "rowCopy":Lcom/google/android/libraries/bind/data/Data;
    sget v2, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->rowPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 509
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v2, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 506
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getEmptyRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataProvider;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method private getErrorRow()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/DataProvider;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method private makeRowList()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v7

    .line 449
    .local v7, "list":Lcom/google/android/libraries/bind/data/DataList;
    invoke-static {}, Lcom/google/android/libraries/bind/collections/RingBuffer;->create()Lcom/google/android/libraries/bind/collections/RingBuffer;

    move-result-object v1

    .line 450
    .local v1, "externalGroupRows":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;>;"
    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v10

    .line 451
    .local v10, "rowCount":I
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 452
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 453
    const/4 v6, -0x1

    .line 458
    .local v6, "insertAfterCard":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v10, :cond_2

    .line 459
    invoke-virtual {v7, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v9

    .line 460
    .local v9, "row":Lcom/google/android/libraries/bind/data/Data;
    sget v11, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP:I

    invoke-virtual {v9, v11}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v11

    if-nez v11, :cond_0

    sget v11, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_GROUP_PROVIDER:I

    .line 461
    invoke-virtual {v9, v11}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 463
    :cond_0
    new-instance v11, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;

    invoke-direct {v11, v6, v9}, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;-><init>(ILcom/google/android/libraries/bind/data/Data;)V

    invoke-virtual {v1, v11}, Lcom/google/android/libraries/bind/collections/RingBuffer;->addLast(Ljava/lang/Object;)V

    .line 458
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 466
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 467
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 468
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 471
    .end local v9    # "row":Lcom/google/android/libraries/bind/data/Data;
    :cond_2
    iget-object v11, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v12

    invoke-virtual {p0, v11, v12}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRows(Ljava/util/List;I)Ljava/util/List;

    move-result-object v4

    .line 472
    .local v4, "groupRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 473
    .local v8, "outputRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v0, 0x0

    .line 474
    .local v0, "currentCard":I
    const/4 v5, 0x0

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    if-ge v5, v11, :cond_5

    .line 475
    :goto_3
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 476
    invoke-virtual {v1, v13}, Lcom/google/android/libraries/bind/collections/RingBuffer;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;

    .line 477
    .local v2, "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    iget v11, v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;->insertAfterCard:I

    if-lt v11, v0, :cond_4

    .line 483
    .end local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_3
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    .line 484
    .local v3, "groupRow":Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    iget-object v11, v3, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485
    invoke-virtual {v3}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->getCardCount()I

    move-result v11

    add-int/2addr v0, v11

    .line 474
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 480
    .end local v3    # "groupRow":Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
    .restart local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_4
    iget-object v11, v2, Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 481
    invoke-virtual {v1, v13}, Lcom/google/android/libraries/bind/collections/RingBuffer;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 487
    .end local v2    # "externalRow":Lcom/google/android/libraries/bind/card/CardGroup$ExternalGroupRow;
    :cond_5
    return-object v8
.end method


# virtual methods
.method public addFooter(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 1
    .param p1, "footer"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->footers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    return-object p0
.end method

.method public addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 1
    .param p1, "header"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    return-object p0
.end method

.method public addHeader(Lcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 2
    .param p1, "header"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "headerId"    # Ljava/lang/String;

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;->addHeader(Lcom/google/android/libraries/bind/data/Data;)Lcom/google/android/libraries/bind/card/CardGroup;

    .line 176
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headerIndexMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    return-object p0
.end method

.method public allowEditOperation()Z
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-static {v0}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->access$100(Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->allowEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)Z

    move-result v0

    return v0
.end method

.method public allowEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)Z
    .locals 6
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    const/4 v3, 0x0

    .line 334
    if-nez p1, :cond_1

    .line 354
    :cond_0
    :goto_0
    return v3

    .line 337
    :cond_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v5, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 338
    .local v1, "rowPos":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 341
    iget v4, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v4, :pswitch_data_0

    .line 354
    const/4 v3, 0x1

    goto :goto_0

    .line 343
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 345
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_REMOVABLE:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    goto :goto_0

    .line 348
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v5, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 349
    .local v2, "targetData":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v2, :cond_0

    .line 352
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    invoke-virtual {v2, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v3

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public commitEditOperation()V
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->commitOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v0

    .line 318
    .local v0, "committedOperation":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->commitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-interface {v1, v2, v0}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;->commitEditOperation(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 319
    return-void
.end method

.method public findDataViewIndex(Lcom/google/android/libraries/bind/data/DataView;)I
    .locals 4
    .param p1, "dataView"    # Lcom/google/android/libraries/bind/data/DataView;

    .prologue
    const/4 v2, -0x1

    .line 628
    if-nez p1, :cond_1

    .line 641
    :cond_0
    :goto_0
    return v2

    .line 631
    :cond_1
    invoke-interface {p1}, Lcom/google/android/libraries/bind/data/DataView;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 632
    .local v0, "dataRow":Lcom/google/android/libraries/bind/data/DataList;
    if-eqz v0, :cond_0

    .line 636
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v1

    .line 637
    .local v1, "id":Ljava/lang/Object;
    :goto_1
    if-eqz v1, :cond_0

    .line 641
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v2

    goto :goto_0

    .line 636
    .end local v1    # "id":Ljava/lang/Object;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected getCardId(I)Ljava/lang/Object;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 560
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 561
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v3}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 562
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 563
    .local v1, "mappedPosition":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    .line 564
    .local v0, "list":Lcom/google/android/libraries/bind/data/DataList;
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .end local v0    # "list":Lcom/google/android/libraries/bind/data/DataList;
    .end local v1    # "mappedPosition":I
    :cond_0
    move v2, v4

    .line 560
    goto :goto_0

    :cond_1
    move v3, v4

    .line 561
    goto :goto_1
.end method

.method public getEditOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->editOperation()Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v0

    return-object v0
.end method

.method protected getMappedPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method getRows()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/libraries/bind/data/DataException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 396
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 397
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    if-nez v3, :cond_5

    .line 398
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    if-eqz v3, :cond_1

    move v2, v4

    .line 399
    .local v2, "refreshError":Z
    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 400
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->getErrorRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 402
    .local v1, "errorRow":Lcom/google/android/libraries/bind/data/Data;
    if-nez v1, :cond_2

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->hideOnError:Z

    if-nez v3, :cond_2

    .line 405
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->lastRefreshException()Lcom/google/android/libraries/bind/data/DataException;

    move-result-object v3

    throw v3

    .end local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    .end local v2    # "refreshError":Z
    :cond_0
    move v3, v5

    .line 396
    goto :goto_0

    :cond_1
    move v2, v5

    .line 398
    goto :goto_1

    .line 409
    .restart local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    .restart local v2    # "refreshError":Z
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    .line 410
    if-eqz v1, :cond_3

    .line 411
    const-string v3, "errorRow"

    invoke-direct {p0, v5, v1, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    .line 419
    .end local v1    # "errorRow":Lcom/google/android/libraries/bind/data/Data;
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 421
    if-nez v2, :cond_4

    .line 422
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->addExtraRows()V

    .line 425
    :cond_4
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    const-string v4, "header"

    invoke-direct {p0, v5, v3, v4}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 427
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->footers:Ljava/util/List;

    const-string v5, "footer"

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowsAtPosition(ILjava/util/List;Ljava/lang/String;)V

    .line 435
    .end local v2    # "refreshError":Z
    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    return-object v3

    .line 417
    .restart local v2    # "refreshError":Z
    :cond_6
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRowList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    goto :goto_2

    .line 429
    :cond_7
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->getEmptyRow()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 430
    .local v0, "emptyRow":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->hasRefreshedOnce()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 431
    const-string v3, "emptyRow"

    invoke-direct {p0, v5, v0, v3}, Lcom/google/android/libraries/bind/card/CardGroup;->addRowAtPosition(ILcom/google/android/libraries/bind/data/Data;Ljava/lang/String;)V

    goto :goto_3
.end method

.method groupId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 363
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    return-object v0

    .line 362
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public groupList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public invalidateRows()V
    .locals 1

    .prologue
    .line 524
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->checkMainThread()V

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->rowList:Ljava/util/List;

    .line 526
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/card/CardListBuilder;->refresh()V

    .line 529
    :cond_0
    return-void
.end method

.method public isEditable()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected list()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    goto :goto_0
.end method

.method protected makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 1
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "position"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 572
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/bind/card/CardGroup;->makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;Ljava/lang/Integer;)Landroid/view/View;
    .locals 10
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;
    .param p2, "position"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "viewResIdOverride"    # Ljava/lang/Integer;

    .prologue
    .line 578
    iget-object v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->realCardList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge p2, v8, :cond_1

    const/4 v8, 0x1

    :goto_0
    invoke-static {v8}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 579
    if-ltz p2, :cond_2

    const/4 v8, 0x1

    :goto_1
    invoke-static {v8}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 580
    iget-object v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->filteredListIndexMap:Ljava/util/List;

    invoke-interface {v8, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 582
    .local v5, "mappedPosition":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 583
    .local v2, "data":Lcom/google/android/libraries/bind/data/Data;
    if-nez p4, :cond_3

    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    :goto_2
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 585
    .local v7, "viewResId":I
    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->equalityFieldsKey:I

    invoke-virtual {v2, v8}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 586
    .local v3, "equalityFields":[I
    if-nez p4, :cond_4

    new-instance v4, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;

    iget v8, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    invoke-direct {v4, v7, v8}, Lcom/google/android/libraries/bind/data/LayoutResIdFilter;-><init>(II)V

    .line 590
    .local v4, "filter":Lcom/google/android/libraries/bind/data/Filter;
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9, v4, v3}, Lcom/google/android/libraries/bind/data/DataList;->filterRow(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)Lcom/google/android/libraries/bind/data/FilteredDataRow;

    move-result-object v1

    .line 591
    .local v1, "cardDataRow":Lcom/google/android/libraries/bind/data/FilteredDataRow;
    const/4 v8, 0x0

    .line 592
    invoke-virtual {p1, v7, v8, p3}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/BindingViewGroup;

    .local v0, "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    move-object v6, v0

    .line 593
    check-cast v6, Landroid/view/View;

    .line 594
    .local v6, "view":Landroid/view/View;
    invoke-interface {v0, v1}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 595
    invoke-interface {v0, p0, v5}, Lcom/google/android/libraries/bind/data/BindingViewGroup;->setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V

    .line 596
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->isEditable()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 597
    sget-object v8, Lcom/google/android/libraries/bind/card/CardGroup;->editableViewOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v6, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 599
    :cond_0
    return-object v6

    .line 578
    .end local v0    # "bindingViewGroup":Lcom/google/android/libraries/bind/data/BindingViewGroup;
    .end local v1    # "cardDataRow":Lcom/google/android/libraries/bind/data/FilteredDataRow;
    .end local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v3    # "equalityFields":[I
    .end local v4    # "filter":Lcom/google/android/libraries/bind/data/Filter;
    .end local v5    # "mappedPosition":I
    .end local v6    # "view":Landroid/view/View;
    .end local v7    # "viewResId":I
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 579
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .restart local v2    # "data":Lcom/google/android/libraries/bind/data/Data;
    .restart local v5    # "mappedPosition":I
    :cond_3
    move-object v8, p4

    .line 583
    goto :goto_2

    .line 586
    .restart local v3    # "equalityFields":[I
    .restart local v7    # "viewResId":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public makeEditable(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 4
    .param p1, "commitHandler"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->commitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    .line 293
    new-instance v0, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    .line 294
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->filter(Lcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    .line 295
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editList:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewResourceIdKey:I

    aput v3, v1, v2

    new-instance v2, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;

    invoke-direct {v2}, Lcom/google/android/libraries/bind/data/ImmediateReadOnlyFilter;-><init>()V

    .line 296
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    .line 297
    return-object p0
.end method

.method protected makeRowData(II)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "row"    # I
    .param p2, "a11yCardCount"    # I

    .prologue
    .line 532
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 533
    .local v0, "rowData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRowId(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 534
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->a11yCardCountKey:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 535
    return-object v0
.end method

.method protected makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;
    .locals 3
    .param p1, "row"    # I
    .param p2, "viewGenerator"    # Lcom/google/android/libraries/bind/card/ViewGenerator;

    .prologue
    .line 539
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 540
    .local v0, "rowData":Lcom/google/android/libraries/bind/data/Data;
    sget v1, Lcom/google/android/libraries/bind/card/CardListBuilder;->DK_ROW_ID:I

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;->makeRowId(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 541
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->viewGeneratorKey:I

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 542
    iget v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->a11yCardCountKey:I

    new-instance v2, Lcom/google/android/libraries/bind/card/CardGroup$4;

    invoke-direct {v2, p0, p2}, Lcom/google/android/libraries/bind/card/CardGroup$4;-><init>(Lcom/google/android/libraries/bind/card/CardGroup;Lcom/google/android/libraries/bind/card/ViewGenerator;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 547
    return-object v0
.end method

.method protected makeRowId(I)Ljava/lang/String;
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->rowPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract makeRows(Ljava/util/List;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;",
            ">;"
        }
    .end annotation
.end method

.method public onOperationDisallowed()V
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->commitHandler:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;

    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    # getter for: Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->currentOperation:Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    invoke-static {v2}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->access$100(Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;->onOperationDisallowed(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 325
    return-void
.end method

.method register(Lcom/google/android/libraries/bind/card/CardListBuilder;)V
    .locals 3
    .param p1, "cardListBuilder"    # Lcom/google/android/libraries/bind/card/CardListBuilder;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 371
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 372
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 373
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 374
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 375
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 376
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    .line 377
    return-void

    :cond_1
    move v0, v2

    .line 371
    goto :goto_0
.end method

.method public removeHeader(I)V
    .locals 4
    .param p1, "headerIndex"    # I

    .prologue
    .line 184
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headers:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 186
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headerIndexMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 187
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 189
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 193
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 194
    return-void
.end method

.method public removeHeader(Ljava/lang/String;)Z
    .locals 2
    .param p1, "headerId"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->headerIndexMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 202
    .local v0, "headerIndex":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/card/CardGroup;->removeHeader(I)V

    .line 204
    const/4 v1, 0x1

    .line 206
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected rowPrefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->groupId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 1
    .param p1, "operation"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->editFilter:Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/card/CardGroup$EditFilter;->setEditOperation(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V

    .line 308
    return-void
.end method

.method public setEmptyRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 0
    .param p1, "emptyRowProvider"    # Lcom/google/android/libraries/bind/data/DataProvider;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->emptyRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    .line 232
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 233
    return-object p0
.end method

.method public setErrorRowProvider(Lcom/google/android/libraries/bind/data/DataProvider;)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 0
    .param p1, "provider"    # Lcom/google/android/libraries/bind/data/DataProvider;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->errorRowProvider:Lcom/google/android/libraries/bind/data/DataProvider;

    .line 245
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 246
    return-object p0
.end method

.method setGroupId(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupId"    # Ljava/lang/String;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupId:Ljava/lang/String;

    .line 359
    return-void
.end method

.method public setHideOnError(Z)Lcom/google/android/libraries/bind/card/CardGroup;
    .locals 0
    .param p1, "hideOnError"    # Z

    .prologue
    .line 260
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->hideOnError:Z

    .line 261
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 262
    return-object p0
.end method

.method public startEditing(Landroid/view/View;I)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 605
    sget-object v4, Lcom/google/android/libraries/bind/card/CardGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v5, "startEditing position: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 607
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 608
    .local v2, "parent":Landroid/view/ViewParent;
    const/4 v1, 0x0

    .line 609
    .local v1, "editableListView":Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;
    :goto_0
    if-eqz v2, :cond_0

    .line 610
    instance-of v4, v2, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    if-eqz v4, :cond_2

    move-object v1, v2

    .line 611
    check-cast v1, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;

    .line 616
    :cond_0
    if-nez v1, :cond_3

    .line 624
    :cond_1
    :goto_1
    return v3

    .line 614
    :cond_2
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_0

    .line 620
    :cond_3
    iget-object v4, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v4, p2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 621
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v4, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->DK_IS_EDITABLE:I

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 624
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, p2}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, p1, p0, p2, v3}, Lcom/google/android/libraries/bind/experimental/card/EditableAdapterView;->startEditing(Landroid/view/View;Lcom/google/android/libraries/bind/card/CardGroup;ILjava/lang/Object;)Z

    move-result v3

    goto :goto_1
.end method

.method unregister()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 380
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 381
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 382
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->groupListDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->monitoredGroupList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v2, p0, Lcom/google/android/libraries/bind/card/CardGroup;->listDataObserver:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 384
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/card/CardGroup;->registered:Z

    .line 385
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup;->cardListBuilder:Lcom/google/android/libraries/bind/card/CardListBuilder;

    .line 386
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardGroup;->invalidateRows()V

    .line 387
    return-void

    :cond_0
    move v0, v1

    .line 381
    goto :goto_0
.end method
