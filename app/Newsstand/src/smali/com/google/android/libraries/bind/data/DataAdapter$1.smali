.class Lcom/google/android/libraries/bind/data/DataAdapter$1;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "DataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/data/DataAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/DataAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/data/DataAdapter;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataAdapter;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 1
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 47
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->isInvalidation:Z

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnInvalidated()V

    .line 50
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataAdapter;

    # getter for: Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnlyIfPrimaryKeyAffected:Z
    invoke-static {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->access$000(Lcom/google/android/libraries/bind/data/DataAdapter;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataAdapter;->notifyOnChanged()V

    .line 53
    :cond_2
    return-void
.end method
