.class Lcom/google/android/libraries/bind/data/FilteredDataList$1;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "FilteredDataList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/data/FilteredDataList;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Filter;[IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/FilteredDataList;

.field final synthetic val$clearOnInvalidation:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/FilteredDataList;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/data/FilteredDataList;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataList;

    iput-boolean p2, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->val$clearOnInvalidation:Z

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 3
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 47
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->isInvalidation:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->val$clearOnInvalidation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataList;

    iget-boolean v0, v0, Lcom/google/android/libraries/bind/data/FilteredDataList;->stopAutoRefreshAfterRefresh:Z

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataList;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/libraries/bind/data/DataChange;->INVALIDATION:Lcom/google/android/libraries/bind/data/DataChange;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/FilteredDataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataList$1;->this$0:Lcom/google/android/libraries/bind/data/FilteredDataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/FilteredDataList;->invalidateData()V

    .line 51
    return-void
.end method
