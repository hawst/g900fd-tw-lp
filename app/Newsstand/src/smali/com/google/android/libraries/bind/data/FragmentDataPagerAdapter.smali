.class public abstract Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;
.super Landroid/support/v4/app/PagerAdapterShim;
.source "FragmentDataPagerAdapter.java"

# interfaces
.implements Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;


# instance fields
.field protected curTransaction:Landroid/support/v4/app/FragmentTransaction;

.field private currentPrimaryItem:Ljava/lang/Object;

.field protected final fragmentManager:Landroid/support/v4/app/FragmentManager;

.field protected fragments:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private isRtl:Z

.field protected list:Lcom/google/android/libraries/bind/data/DataList;

.field private final observer:Lcom/google/android/libraries/bind/data/DataObserver;

.field private titleKey:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/PagerAdapterShim;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    .line 33
    iput-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->currentPrimaryItem:Ljava/lang/Object;

    .line 43
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter$1;-><init>(Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 52
    return-void
.end method

.method private getCurTransaction()Landroid/support/v4/app/FragmentTransaction;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 240
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method


# virtual methods
.method protected cleanUpInvalidData()V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public abstract createFragment(ILcom/google/android/libraries/bind/data/Data;)Landroid/support/v4/app/Fragment;
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    .line 139
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "key"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 194
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 195
    .local v0, "removedFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_1

    move v1, v2

    :goto_0
    const-string v4, "Tried to destroy a fragment for key: %s that\'s not in the map. Should never happen."

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 198
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getCurTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 200
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p3}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 201
    invoke-virtual {p0, v0, p3}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->saveFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V

    .line 203
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 195
    goto :goto_0
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->curTransaction:Landroid/support/v4/app/FragmentTransaction;

    .line 234
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 236
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getFragment(I)Landroid/support/v4/app/Fragment;
    .locals 2
    .param p1, "visualPosition"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    if-nez p1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->makeLoadingFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 86
    :goto_0
    return-object v1

    .line 83
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 86
    .local v0, "logicalPosition":I
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->createFragment(ILcom/google/android/libraries/bind/data/Data;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 104
    .local v0, "positionForId":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 106
    const/4 v0, -0x2

    .line 108
    .end local v0    # "positionForId":I
    :cond_0
    return v0
.end method

.method public getKey(I)Ljava/lang/Object;
    .locals 1
    .param p1, "visualPosition"    # I

    .prologue
    .line 96
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isInvalidPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 99
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->titleKey:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isInvalidPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/PagerAdapterShim;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 151
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->titleKey:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getStateBundle()Landroid/os/Bundle;
    .locals 11

    .prologue
    .line 250
    const/4 v6, 0x0

    .line 251
    .local v6, "state":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 252
    .local v1, "counter":I
    iget-object v7, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 253
    .local v4, "key":Ljava/lang/Object;
    iget-object v8, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    .line 254
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_1

    .line 255
    if-nez v6, :cond_0

    .line 256
    new-instance v6, Landroid/os/Bundle;

    .end local v6    # "state":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 258
    .restart local v6    # "state":Landroid/os/Bundle;
    :cond_0
    const-string v8, "FragmentDataPagerAdapter_p"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 259
    .local v5, "parcelId":Ljava/lang/String;
    const-string v8, "FragmentDataPagerAdapter_f"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0xb

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 262
    .local v3, "fragmentId":Ljava/lang/String;
    new-instance v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;-><init>(Ljava/lang/Object;)V

    .line 263
    .local v0, "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    invoke-virtual {v6, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 264
    iget-object v8, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v8, v6, v3, v2}, Landroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 266
    .end local v0    # "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    .end local v3    # "fragmentId":Ljava/lang/String;
    .end local v5    # "parcelId":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 267
    goto :goto_0

    .line 268
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "key":Ljava/lang/Object;
    :cond_2
    return-object v6
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 160
    const/4 v0, 0x0

    .line 161
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getKey(I)Ljava/lang/Object;

    move-result-object v1

    .line 162
    .local v1, "key":Ljava/lang/Object;
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isInvalidPosition(I)Z

    move-result v5

    if-nez v5, :cond_3

    const/4 v2, 0x1

    .line 163
    .local v2, "restorable":Z
    :goto_0
    if-eqz v2, :cond_0

    .line 164
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 167
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    if-nez v0, :cond_4

    .line 168
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getFragment(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 169
    iget-object v5, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    if-eqz v2, :cond_1

    .line 171
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->restoreFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V

    .line 173
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getCurTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v6

    invoke-virtual {v5, v6, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 182
    :cond_2
    :goto_1
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 183
    invoke-virtual {v0, v4}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 184
    return-object v1

    .end local v2    # "restorable":Z
    :cond_3
    move v2, v4

    .line 162
    goto :goto_0

    .line 176
    .restart local v2    # "restorable":Z
    :cond_4
    invoke-static {v0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getFragmentSavedFragmentState(Landroid/support/v4/app/Fragment;)Landroid/os/Bundle;

    move-result-object v3

    .line 177
    .local v3, "savedFragmentState":Landroid/os/Bundle;
    if-eqz v3, :cond_2

    .line 178
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    goto :goto_1
.end method

.method protected isInvalidPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 75
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRtl()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isRtl:Z

    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 299
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 300
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    :goto_0
    if-nez v0, :cond_1

    .line 303
    :goto_1
    return v2

    .line 299
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    move-object v0, v1

    goto :goto_0

    .line 303
    .restart local v0    # "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-ne v1, p1, :cond_2

    const/4 v1, 0x1

    :goto_2
    move v2, v1

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method protected makeLoadingFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/libraries/bind/fragment/LoadingFragment;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/fragment/LoadingFragment;-><init>()V

    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->cleanUpInvalidData()V

    .line 312
    invoke-super {p0}, Landroid/support/v4/app/PagerAdapterShim;->notifyDataSetChanged()V

    .line 313
    return-void
.end method

.method protected restoreFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V
    .locals 0
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 190
    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 13
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    const/4 v12, 0x0

    .line 273
    if-eqz p1, :cond_2

    move-object v1, p1

    .line 274
    check-cast v1, Landroid/os/Bundle;

    .line 277
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 278
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 279
    .local v5, "keys":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 280
    .local v4, "key":Ljava/lang/String;
    const-string v9, "FragmentDataPagerAdapter_f"

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 281
    const-string v9, "FragmentDataPagerAdapter_f"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 282
    .local v6, "num":I
    const-string v9, "FragmentDataPagerAdapter_p"

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0xb

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 283
    .local v7, "parcelId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v9, v1, v4}, Landroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 284
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    const/4 v3, 0x0

    .line 285
    .local v3, "fragmentRestored":Z
    if-eqz v2, :cond_1

    .line 286
    invoke-virtual {v2, v12}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 287
    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    .line 288
    .local v0, "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    iget-object v9, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    iget-object v10, v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->key:Ljava/lang/Object;

    invoke-interface {v9, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    const/4 v3, 0x1

    .line 291
    .end local v0    # "adapterKey":Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
    :cond_1
    const-string v9, "Bad fragment at key %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    goto :goto_0

    .line 295
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v3    # "fragmentRestored":Z
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "keys":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    .end local v6    # "num":I
    .end local v7    # "parcelId":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method protected saveFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V
    .locals 0
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 208
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getStateBundle()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 2
    .param p1, "list"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-ne v0, p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 127
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    .line 128
    if-eqz p1, :cond_2

    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 131
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 212
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->currentPrimaryItem:Ljava/lang/Object;

    invoke-virtual {p3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 213
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->currentPrimaryItem:Ljava/lang/Object;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->currentPrimaryItem:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    move-object v1, v2

    .line 214
    .local v1, "oldFragment":Landroid/support/v4/app/Fragment;
    :goto_0
    if-eqz v1, :cond_0

    .line 217
    invoke-virtual {v1, v4}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 218
    invoke-virtual {v1, v4}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 220
    :cond_0
    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->fragments:Ljava/util/Map;

    invoke-interface {v2, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    move-object v0, v2

    .line 221
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    :cond_1
    if-eqz v0, :cond_2

    .line 222
    invoke-virtual {v0, v5}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 223
    invoke-virtual {v0, v5}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 225
    :cond_2
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->currentPrimaryItem:Ljava/lang/Object;

    .line 227
    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v1    # "oldFragment":Landroid/support/v4/app/Fragment;
    :cond_3
    return-void

    :cond_4
    move-object v1, v0

    .line 213
    goto :goto_0
.end method

.method public setRtl(Z)V
    .locals 1
    .param p1, "isRtl"    # Z

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isRtl:Z

    if-eq v0, p1, :cond_0

    .line 62
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->isRtl:Z

    .line 63
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->notifyDataSetChanged()V

    .line 65
    :cond_0
    return-void
.end method

.method public setTitleKey(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "titleKey"    # Ljava/lang/Integer;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->titleKey:Ljava/lang/Integer;

    .line 118
    return-void
.end method
