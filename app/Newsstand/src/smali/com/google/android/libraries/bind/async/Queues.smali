.class public Lcom/google/android/libraries/bind/async/Queues;
.super Ljava/lang/Object;
.source "Queues.java"


# static fields
.field public static final BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

.field public static final BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

.field public static final BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 7
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue;

    const-string v1, "BIND_CPU"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/async/Queue;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_CPU:Lcom/google/android/libraries/bind/async/Queue;

    .line 8
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue;

    const-string v1, "BIND_MAIN"

    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadExecutor()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/Queue;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    sput-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_MAIN:Lcom/google/android/libraries/bind/async/Queue;

    .line 9
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue;

    const-string v1, "BIND_IMMEDIATE"

    .line 10
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->immediateExecutor()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/Queue;-><init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V

    sput-object v0, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
