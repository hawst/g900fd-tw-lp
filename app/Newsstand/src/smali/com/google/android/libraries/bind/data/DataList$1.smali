.class Lcom/google/android/libraries/bind/data/DataList$1;
.super Ljava/lang/Object;
.source "DataList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/data/DataList;->postRefresh(Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/DataList;

.field final synthetic val$change:Lcom/google/android/libraries/bind/data/DataChange;

.field final synthetic val$optDataVersion:Ljava/lang/Integer;

.field final synthetic val$refreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

.field final synthetic val$snapshot:Lcom/google/android/libraries/bind/data/Snapshot;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/RefreshTask;Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataList$1;->this$0:Lcom/google/android/libraries/bind/data/DataList;

    iput-object p2, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$refreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    iput-object p3, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iput-object p4, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$change:Lcom/google/android/libraries/bind/data/DataChange;

    iput-object p5, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$optDataVersion:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$refreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataList$1;->this$0:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    if-ne v0, v1, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList$1;->this$0:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$snapshot:Lcom/google/android/libraries/bind/data/Snapshot;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$change:Lcom/google/android/libraries/bind/data/DataChange;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataList$1;->val$optDataVersion:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/data/DataList;->update(Lcom/google/android/libraries/bind/data/Snapshot;Lcom/google/android/libraries/bind/data/DataChange;Ljava/lang/Integer;)V

    .line 414
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataList$1;->this$0:Lcom/google/android/libraries/bind/data/DataList;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/libraries/bind/data/DataList;->currentRefreshTask:Lcom/google/android/libraries/bind/data/RefreshTask;

    .line 416
    :cond_0
    return-void
.end method
