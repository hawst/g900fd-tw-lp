.class public Lcom/google/android/libraries/bind/logging/Logd;
.super Ljava/lang/Object;
.source "Logd.java"


# static fields
.field private static enableAll:Z

.field protected static existingClassLoggers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/bind/logging/Logd;",
            ">;"
        }
    .end annotation
.end field

.field protected static sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;


# instance fields
.field protected enabled:Z

.field protected tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    .line 15
    new-instance v0, Lcom/google/android/libraries/bind/logging/SystemLogHandler;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/logging/SystemLogHandler;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static enableAll(Z)V
    .locals 0
    .param p0, "enable"    # Z

    .prologue
    .line 34
    sput-boolean p0, Lcom/google/android/libraries/bind/logging/Logd;->enableAll:Z

    .line 35
    return-void
.end method

.method public static get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/google/android/libraries/bind/logging/Logd;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/String;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    return-object v0
.end method

.method public static get(Ljava/lang/String;)Lcom/google/android/libraries/bind/logging/Logd;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 25
    sget-object v1, Lcom/google/android/libraries/bind/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    sget-object v1, Lcom/google/android/libraries/bind/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/logging/Logd;

    .line 30
    :goto_0
    return-object v1

    .line 28
    :cond_0
    new-instance v0, Lcom/google/android/libraries/bind/logging/Logd;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/logging/Logd;-><init>(Ljava/lang/String;)V

    .line 29
    .local v0, "logger":Lcom/google/android/libraries/bind/logging/Logd;
    sget-object v1, Lcom/google/android/libraries/bind/logging/Logd;->existingClassLoggers:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 30
    goto :goto_0
.end method

.method public static isEnableAll()Z
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/google/android/libraries/bind/logging/Logd;->enableAll:Z

    return v0
.end method

.method private static varargs safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 6
    .param p0, "optThrowable"    # Ljava/lang/Throwable;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v1, p2

    if-nez v1, :cond_3

    :cond_0
    move-object v0, p1

    .line 137
    .local v0, "formatted":Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_1

    const-string v0, ""

    .line 139
    :cond_1
    if-eqz p0, :cond_2

    .line 140
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    :cond_2
    return-object v0

    .line 136
    .end local v0    # "formatted":Ljava/lang/String;
    :cond_3
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/logging/Logd;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method

.method public disable()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/logging/Logd;->enabled:Z

    .line 52
    return-object p0
.end method

.method public varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method public e(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v1, 0x0

    .line 109
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    return-void
.end method

.method public varargs e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public enable()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/logging/Logd;->enabled:Z

    .line 47
    return-object p0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public varargs i(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public varargs i(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/logging/Logd;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method

.method public varargs ii(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->ii(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    return-void
.end method

.method public varargs ii(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/logging/Logd;->enabled:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/libraries/bind/logging/Logd;->enableAll:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs v(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/logging/Logd;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method

.method public varargs w(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/libraries/bind/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v1, 0x0

    .line 94
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/logging/Logd;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    return-void
.end method

.method public varargs w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/Object;

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/libraries/bind/logging/Logd;->sysLogHandler:Lcom/google/android/libraries/bind/logging/LogHandler;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/libraries/bind/logging/Logd;->tag:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/google/android/libraries/bind/logging/Logd;->safeFormat(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/logging/LogHandler;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void
.end method
