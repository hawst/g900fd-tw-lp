.class Lcom/google/android/libraries/bind/data/DataPagerAdapter$1;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "DataPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/data/DataPagerAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/DataPagerAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/data/DataPagerAdapter;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 1
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 41
    iget-boolean v0, p1, Lcom/google/android/libraries/bind/data/DataChange;->affectsPrimaryKey:Z

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$1;->this$0:Lcom/google/android/libraries/bind/data/DataPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->notifyDataSetChanged()V

    .line 44
    :cond_0
    return-void
.end method
