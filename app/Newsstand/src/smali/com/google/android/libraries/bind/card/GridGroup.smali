.class public Lcom/google/android/libraries/bind/card/GridGroup;
.super Lcom/google/android/libraries/bind/card/CardGroup;
.source "GridGroup.java"


# static fields
.field public static final DK_COLUMN_SPAN:I

.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;


# instance fields
.field protected context:Landroid/content/Context;

.field protected fixedNumColumns:Ljava/lang/Integer;

.field protected fixedNumColumnsResId:Ljava/lang/Integer;

.field protected maxColumns:I

.field protected maxRows:I

.field protected minColumnWidthPx:Ljava/lang/Integer;

.field protected rowHeightPx:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/libraries/bind/card/GridGroup;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/GridGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 45
    sget v0, Lcom/google/android/libraries/bind/R$id;->GridGroup_columnSpan:I

    sput v0, Lcom/google/android/libraries/bind/card/GridGroup;->DK_COLUMN_SPAN:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Landroid/content/Context;)V
    .locals 1
    .param p1, "groupList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const v0, 0x7fffffff

    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardGroup;-><init>(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 36
    iput v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->maxRows:I

    .line 37
    iput v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->maxColumns:I

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->rowHeightPx:I

    .line 49
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->context:Landroid/content/Context;

    .line 50
    sget v0, Lcom/google/android/libraries/bind/R$dimen;->bind__default_column_width:I

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/GridGroup;->setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;

    .line 51
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/libraries/bind/card/GridGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/libraries/bind/card/GridGroup;Landroid/view/ViewGroup$LayoutParams;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/card/GridGroup;
    .param p1, "x1"    # Landroid/view/ViewGroup$LayoutParams;
    .param p2, "x2"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/card/GridGroup;->makeEmptyView(Landroid/view/ViewGroup$LayoutParams;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private makeEmptyView(Landroid/view/ViewGroup$LayoutParams;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 2
    .param p1, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;
    .param p2, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 228
    sget v0, Lcom/google/android/libraries/bind/card/CardEmpty;->LAYOUT:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1, p1}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getColumnSpanForCard(II)I
    .locals 5
    .param p1, "position"    # I
    .param p2, "numColumns"    # I

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/GridGroup;->getMappedPosition(I)I

    move-result v2

    .line 241
    .local v2, "mappedPosition":I
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/GridGroup;->list()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    .line 242
    .local v1, "data":Lcom/google/android/libraries/bind/data/Data;
    sget v3, Lcom/google/android/libraries/bind/card/GridGroup;->DK_COLUMN_SPAN:I

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    .line 245
    .local v0, "columnSpan":Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 246
    const/4 p2, 0x1

    .line 252
    .end local p2    # "numColumns":I
    :cond_0
    :goto_0
    return p2

    .line 249
    .restart local p2    # "numColumns":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 252
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getNumberOfColumns(I)I
    .locals 4
    .param p1, "numCards"    # I

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/GridGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 151
    .local v1, "viewWidth":I
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/GridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/GridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    div-int v0, v1, v2

    .line 160
    .local v0, "numColumns":I
    :goto_0
    const/4 v2, 0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/libraries/bind/card/GridGroup;->maxColumns:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 161
    return v0

    .line 153
    .end local v0    # "numColumns":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumns:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumns:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "numColumns":I
    goto :goto_0

    .line 155
    .end local v0    # "numColumns":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumnsResId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 156
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/GridGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumnsResId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .restart local v0    # "numColumns":I
    goto :goto_0

    .line 158
    .end local v0    # "numColumns":I
    :cond_2
    move v0, p1

    .restart local v0    # "numColumns":I
    goto :goto_0
.end method

.method protected makeRowViewGenerator(ILjava/util/List;I)Lcom/google/android/libraries/bind/card/ViewGenerator;
    .locals 1
    .param p1, "row"    # I
    .param p3, "nColumns"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lcom/google/android/libraries/bind/card/ViewGenerator;"
        }
    .end annotation

    .prologue
    .line 166
    .local p2, "cards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v0, Lcom/google/android/libraries/bind/card/GridGroup$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lcom/google/android/libraries/bind/card/GridGroup$1;-><init>(Lcom/google/android/libraries/bind/card/GridGroup;Ljava/util/List;II)V

    return-object v0
.end method

.method protected makeRows(Ljava/util/List;I)Ljava/util/List;
    .locals 15
    .param p2, "primaryKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    .line 113
    .local v3, "nCards":I
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/bind/card/GridGroup;->getNumberOfColumns(I)I

    move-result v4

    .line 115
    .local v4, "nColumns":I
    new-instance v8, Ljava/util/ArrayList;

    div-int v10, v3, v4

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    .local v8, "rows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;>;"
    const/4 v6, 0x0

    .line 117
    .local v6, "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 118
    .local v1, "columnsUsedForRow":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 119
    invoke-virtual {p0, v2, v4}, Lcom/google/android/libraries/bind/card/GridGroup;->getColumnSpanForCard(II)I

    move-result v0

    .line 122
    .local v0, "columnSpanForCard":I
    if-eqz v6, :cond_0

    sub-int v10, v4, v1

    if-le v0, v10, :cond_3

    .line 124
    :cond_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    .line 125
    .local v5, "row":I
    iget v10, p0, Lcom/google/android/libraries/bind/card/GridGroup;->maxRows:I

    if-ne v5, v10, :cond_2

    .line 142
    .end local v0    # "columnSpanForCard":I
    .end local v5    # "row":I
    :cond_1
    return-object v8

    .line 131
    .restart local v0    # "columnSpanForCard":I
    .restart local v5    # "row":I
    :cond_2
    const/4 v1, 0x0

    .line 133
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    .restart local v6    # "rowCards":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v10, Lcom/google/android/libraries/bind/card/GridGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v11, "Making generator for row %d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    invoke-virtual {p0, v5, v6, v4}, Lcom/google/android/libraries/bind/card/GridGroup;->makeRowViewGenerator(ILjava/util/List;I)Lcom/google/android/libraries/bind/card/ViewGenerator;

    move-result-object v9

    .line 136
    .local v9, "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    invoke-virtual {p0, v5, v9}, Lcom/google/android/libraries/bind/card/GridGroup;->makeRowData(ILcom/google/android/libraries/bind/card/ViewGenerator;)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    .line 137
    .local v7, "rowData":Lcom/google/android/libraries/bind/data/Data;
    new-instance v10, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;

    invoke-direct {v10, v9, v7}, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;-><init>(Lcom/google/android/libraries/bind/card/ViewGenerator;Lcom/google/android/libraries/bind/data/Data;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    .end local v5    # "row":I
    .end local v7    # "rowData":Lcom/google/android/libraries/bind/data/Data;
    .end local v9    # "viewGenerator":Lcom/google/android/libraries/bind/card/ViewGenerator;
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    add-int/2addr v1, v0

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setColumnWidthPx(I)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 1
    .param p1, "columnWidthPx"    # I

    .prologue
    .line 58
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumns:Ljava/lang/Integer;

    .line 60
    return-object p0
.end method

.method public setColumnWidthResId(I)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 1
    .param p1, "columnWidthResId"    # I

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/GridGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/GridGroup;->setColumnWidthPx(I)Lcom/google/android/libraries/bind/card/GridGroup;

    move-result-object v0

    return-object v0
.end method

.method public setFixedNumColumns(I)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 2
    .param p1, "fixedNumColumns"    # I

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumns:Ljava/lang/Integer;

    .line 65
    iput-object v1, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumnsResId:Ljava/lang/Integer;

    .line 66
    iput-object v1, p0, Lcom/google/android/libraries/bind/card/GridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    .line 67
    return-object p0
.end method

.method public setFixedNumColumnsResId(I)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 2
    .param p1, "fixedNumColumnsResId"    # I

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumnsResId:Ljava/lang/Integer;

    .line 72
    iput-object v1, p0, Lcom/google/android/libraries/bind/card/GridGroup;->fixedNumColumns:Ljava/lang/Integer;

    .line 73
    iput-object v1, p0, Lcom/google/android/libraries/bind/card/GridGroup;->minColumnWidthPx:Ljava/lang/Integer;

    .line 74
    return-object p0
.end method

.method public setMaxRows(I)Lcom/google/android/libraries/bind/card/GridGroup;
    .locals 0
    .param p1, "maxRows"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/google/android/libraries/bind/card/GridGroup;->maxRows:I

    .line 107
    return-object p0
.end method
