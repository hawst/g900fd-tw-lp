.class public abstract Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;
.super Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;
.source "FragmentStateDataPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    }
.end annotation


# instance fields
.field protected savedStates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    .line 28
    return-void
.end method


# virtual methods
.method protected cleanUpInvalidData()V
    .locals 5

    .prologue
    .line 79
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 89
    :cond_1
    return-void

    .line 83
    :cond_2
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 84
    .local v1, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 85
    .local v0, "key":Ljava/lang/Object;
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 86
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected getStateBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 49
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->getStateBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 50
    .local v1, "state":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 51
    if-nez v1, :cond_0

    .line 52
    new-instance v1, Landroid/os/Bundle;

    .end local v1    # "state":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Ljava/lang/ClassLoader;)V

    .line 54
    .restart local v1    # "state":Landroid/os/Bundle;
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    new-array v0, v2, [Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 55
    .local v0, "adapterStates":[Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 56
    const-string v2, "FragmentStateDataPagerAdapter_stateKey"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 58
    .end local v0    # "adapterStates":[Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    :cond_1
    return-object v1
.end method

.method protected restoreFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V
    .locals 4
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 33
    .local v0, "adapterState":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    .line 34
    .local v1, "savedState":Landroid/support/v4/app/Fragment$SavedState;
    :goto_0
    if-eqz v1, :cond_0

    .line 35
    invoke-static {v1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->getSavedStateInternalState(Landroid/support/v4/app/Fragment$SavedState;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 36
    invoke-virtual {p1, v1}, Landroid/support/v4/app/Fragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    .line 38
    :cond_0
    return-void

    .line 33
    .end local v1    # "savedState":Landroid/support/v4/app/Fragment$SavedState;
    :cond_1
    iget-object v1, v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->state:Landroid/support/v4/app/Fragment$SavedState;

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 6
    .param p1, "state"    # Landroid/os/Parcelable;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/data/FragmentDataPagerAdapter;->restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 64
    if-eqz p1, :cond_0

    move-object v2, p1

    .line 65
    check-cast v2, Landroid/os/Bundle;

    .line 66
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "FragmentStateDataPagerAdapter_stateKey"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 67
    .local v1, "adapterStates":[Landroid/os/Parcelable;
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 68
    if-eqz v1, :cond_0

    .line 69
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v1

    if-ge v3, v4, :cond_0

    .line 70
    aget-object v0, v1, v3

    check-cast v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 71
    .local v0, "adapterState":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    iget-object v5, v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->key:Ljava/lang/Object;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 75
    .end local v0    # "adapterState":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    .end local v1    # "adapterStates":[Landroid/os/Parcelable;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "i":I
    :cond_0
    return-void
.end method

.method protected saveFragmentState(Landroid/support/v4/app/Fragment;Ljava/lang/Object;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "key"    # Ljava/lang/Object;

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 43
    invoke-virtual {v1, p1}, Landroid/support/v4/app/FragmentManager;->saveFragmentInstanceState(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;-><init>(Ljava/lang/Object;Landroid/support/v4/app/Fragment$SavedState;)V

    .line 44
    .local v0, "state":Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter;->savedStates:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method
