.class public Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.super Lcom/google/android/libraries/bind/widget/BoundFrameLayout;
.source "BindingFrameLayout.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/BindingViewGroup;


# instance fields
.field protected final bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v1, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;-><init>(Lcom/google/android/libraries/bind/data/DataView;)V

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    .line 35
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BindingFrameLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, "a":Landroid/content/res/TypedArray;
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingFrameLayout_bind__isOwnedByParent:I

    const/4 v3, 0x0

    .line 37
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 36
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 38
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    sget v2, Lcom/google/android/libraries/bind/R$styleable;->BindingFrameLayout_bind__supportsAnimationCapture:I

    const/4 v3, 0x1

    .line 39
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 38
    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setSupportsAnimationCapture(Z)V

    .line 40
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void
.end method


# virtual methods
.method public blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "bitmapRect"    # Landroid/graphics/Rect;
    .param p3, "animationDuration"    # J
    .param p5, "blendMode"    # Lcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->blendCapturedBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/BindingViewGroup$BlendMode;)V

    .line 143
    return-void
.end method

.method public captureToBitmap(Landroid/graphics/Bitmap;FF)Z
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "left"    # F
    .param p3, "top"    # F

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->captureToBitmap(Landroid/graphics/Bitmap;FF)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->draw(Landroid/graphics/Canvas;)V

    .line 127
    return-void
.end method

.method public getBindingViewGroupHelper()Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    return-object v0
.end method

.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getData()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getDataRow()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->getDataRow()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method

.method public isOwnedByParent()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->isOwnedByParent()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onAttachedToWindow()V

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onAttachedToWindow()V

    .line 105
    return-void
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 122
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onDetachedFromWindow()V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onDetachedFromWindow()V

    .line 99
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onFinishInflate()V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishInflate()V

    .line 93
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onFinishTemporaryDetach()V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onFinishTemporaryDetach()V

    .line 117
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onMeasure(II)V

    .line 171
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onMeasure(II)V

    .line 172
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->onStartTemporaryDetach()V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->onStartTemporaryDetach()V

    .line 111
    return-void
.end method

.method public prepareForRecycling()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->prepareForRecycling()V

    .line 77
    return-void
.end method

.method public setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V
    .locals 1
    .param p1, "cardGroup"    # Lcom/google/android/libraries/bind/card/CardGroup;
    .param p2, "cardGroupPosition"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setCardGroup(Lcom/google/android/libraries/bind/card/CardGroup;I)V

    .line 82
    return-void
.end method

.method public setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 1
    .param p1, "dataRow"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setDataRow(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 51
    return-void
.end method

.method public final setMeasuredDimensionProxy(II)V
    .locals 0
    .param p1, "measuredWidth"    # I
    .param p2, "measuredHeight"    # I

    .prologue
    .line 165
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->setMeasuredDimension(II)V

    .line 166
    return-void
.end method

.method public setOwnedByParent(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->setOwnedByParent(Z)V

    .line 132
    return-void
.end method

.method public startEditingIfPossible()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->bindingViewGroupHelper:Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/BindingViewGroupHelper;->startEditingIfPossible()Z

    move-result v0

    return v0
.end method

.method public final superDrawProxy(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BoundFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 161
    return-void
.end method

.method public final updateBoundDataProxy(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 156
    return-void
.end method
