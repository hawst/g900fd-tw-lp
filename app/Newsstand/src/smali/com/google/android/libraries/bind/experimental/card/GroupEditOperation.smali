.class public Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
.super Ljava/lang/Object;
.source "GroupEditOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation$CommitHandler;
    }
.end annotation


# instance fields
.field public final editId:Ljava/lang/Object;

.field public final position:Ljava/lang/Integer;

.field public final type:I


# direct methods
.method protected constructor <init>(ILjava/lang/Object;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "operationType"    # I
    .param p2, "optEditedId"    # Ljava/lang/Object;
    .param p3, "optPosition"    # Ljava/lang/Integer;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    .line 44
    iput-object p2, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    .line 45
    iput-object p3, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;)V
    .locals 1
    .param p1, "from"    # Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iget v0, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    iput v0, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    .line 50
    iget-object v0, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    .line 51
    iget-object v0, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    .line 52
    return-void
.end method

.method public static move(Ljava/lang/Object;I)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 3
    .param p0, "movedId"    # Ljava/lang/Object;
    .param p1, "position"    # I

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;-><init>(ILjava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static remove(Ljava/lang/Object;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 3
    .param p0, "removedId"    # Ljava/lang/Object;

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;-><init>(ILjava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static unchanged(Ljava/lang/Object;)Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    .locals 3
    .param p0, "unchangedId"    # Ljava/lang/Object;

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;-><init>(ILjava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 71
    instance-of v2, p1, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 72
    check-cast v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;

    .line 73
    .local v0, "other":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    iget v2, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    iget v3, v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    .line 74
    invoke-static {v2, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    iget-object v3, v0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    .line 75
    invoke-static {v2, v3}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 77
    .end local v0    # "other":Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    iget v0, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->type:I

    packed-switch v0, :pswitch_data_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 58
    :pswitch_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "OPERATION_TYPE_UNCHANGED - id: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 60
    :pswitch_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "OPERATION_TYPE_REMOVE - id: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 62
    :pswitch_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "OPERATION_TYPE_MOVE - id: %s, position: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->editId:Ljava/lang/Object;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/libraries/bind/experimental/card/GroupEditOperation;->position:Ljava/lang/Integer;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
