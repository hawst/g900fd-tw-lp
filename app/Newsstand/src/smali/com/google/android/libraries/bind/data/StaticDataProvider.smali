.class public final Lcom/google/android/libraries/bind/data/StaticDataProvider;
.super Ljava/lang/Object;
.source "StaticDataProvider.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/DataProvider;


# instance fields
.field private final data:Lcom/google/android/libraries/bind/data/Data;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/StaticDataProvider;->data:Lcom/google/android/libraries/bind/data/Data;

    .line 12
    return-void
.end method


# virtual methods
.method public getData()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/StaticDataProvider;->data:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method
