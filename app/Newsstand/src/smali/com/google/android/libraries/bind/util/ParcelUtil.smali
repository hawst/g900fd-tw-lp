.class public Lcom/google/android/libraries/bind/util/ParcelUtil;
.super Ljava/lang/Object;
.source "ParcelUtil.java"


# direct methods
.method public static readObjectFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;
    .locals 3
    .param p0, "in"    # Landroid/os/Parcel;
    .param p1, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    const/4 v0, 0x1

    .line 31
    const/4 v1, 0x0

    .line 32
    .local v1, "object":Ljava/lang/Object;
    invoke-virtual {p0}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v0, :cond_0

    .line 33
    .local v0, "isParcelable":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 34
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    .line 38
    .end local v1    # "object":Ljava/lang/Object;
    :goto_1
    return-object v1

    .line 32
    .end local v0    # "isParcelable":Z
    .restart local v1    # "object":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    .restart local v0    # "isParcelable":Z
    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    .local v1, "object":Ljava/io/Serializable;
    goto :goto_1
.end method

.method public static writeObjectToParcel(Ljava/lang/Object;Landroid/os/Parcel;I)V
    .locals 2
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 17
    instance-of v0, p0, Landroid/os/Parcelable;

    .line 18
    .local v0, "isParcelable":Z
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 19
    if-eqz v0, :cond_1

    .line 20
    check-cast p0, Landroid/os/Parcelable;

    .end local p0    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 24
    :goto_1
    return-void

    .line 18
    .restart local p0    # "object":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 22
    :cond_1
    check-cast p0, Ljava/io/Serializable;

    .end local p0    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_1
.end method
