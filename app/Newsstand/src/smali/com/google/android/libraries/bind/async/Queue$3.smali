.class Lcom/google/android/libraries/bind/async/Queue$3;
.super Ljava/lang/Object;
.source "Queue.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/async/Queue;->makeExecutor(Z)Ljava/util/concurrent/ThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final createdCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic this$0:Lcom/google/android/libraries/bind/async/Queue;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/async/Queue;)V
    .locals 2
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/async/Queue;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/libraries/bind/async/Queue$3;->this$0:Lcom/google/android/libraries/bind/async/Queue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/async/Queue$3;->createdCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 7
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/libraries/bind/async/Queue$3$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/bind/async/Queue$3$1;-><init>(Lcom/google/android/libraries/bind/async/Queue$3;Ljava/lang/Runnable;)V

    .line 106
    .local v0, "setBackgroundPriority":Ljava/lang/Runnable;
    iget-object v3, p0, Lcom/google/android/libraries/bind/async/Queue$3;->this$0:Lcom/google/android/libraries/bind/async/Queue;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/libraries/bind/async/Queue$3;->createdCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xd

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "threadName":Ljava/lang/String;
    new-instance v1, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/libraries/bind/async/Queue$3;->this$0:Lcom/google/android/libraries/bind/async/Queue;

    iget-object v3, v3, Lcom/google/android/libraries/bind/async/Queue;->threadGroup:Ljava/lang/ThreadGroup;

    invoke-direct {v1, v3, v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 108
    .local v1, "thread":Ljava/lang/Thread;
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/Thread;->setPriority(I)V

    .line 109
    return-object v1
.end method
