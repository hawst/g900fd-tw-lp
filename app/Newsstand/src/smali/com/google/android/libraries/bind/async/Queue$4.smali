.class Lcom/google/android/libraries/bind/async/Queue$4;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "Queue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/async/Queue;->makeExecutor(Z)Ljava/util/concurrent/ThreadPoolExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/async/Queue;

.field final synthetic val$jankLocked:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/async/Queue;IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Z)V
    .locals 12
    .param p1, "this$0"    # Lcom/google/android/libraries/bind/async/Queue;
    .param p2, "x0"    # I
    .param p3, "x1"    # I
    .param p4, "x2"    # J
    .param p6, "x3"    # Ljava/util/concurrent/TimeUnit;
    .param p8, "x5"    # Ljava/util/concurrent/ThreadFactory;

    .prologue
    .line 114
    .local p7, "x4":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Ljava/lang/Runnable;>;"
    iput-object p1, p0, Lcom/google/android/libraries/bind/async/Queue$4;->this$0:Lcom/google/android/libraries/bind/async/Queue;

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/async/Queue$4;->val$jankLocked:Z

    move-object v3, p0

    move v4, p2

    move v5, p3

    move-wide/from16 v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v3 .. v10}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-void
.end method


# virtual methods
.method protected beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Thread;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ThreadPoolExecutor;->beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V

    .line 118
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/async/Queue$4;->val$jankLocked:Z

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/google/android/libraries/bind/async/JankLock;->global:Lcom/google/android/libraries/bind/async/JankLock;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/async/JankLock;->blockUntilJankPermitted()V

    .line 121
    :cond_0
    return-void
.end method
