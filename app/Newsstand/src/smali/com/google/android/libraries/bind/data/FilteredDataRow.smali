.class public Lcom/google/android/libraries/bind/data/FilteredDataRow;
.super Lcom/google/android/libraries/bind/data/FilteredDataList;
.source "FilteredDataRow.java"


# instance fields
.field private emptyWhenNone:Z

.field private final rowId:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/DataList;Ljava/lang/Object;Lcom/google/android/libraries/bind/data/Filter;[I)V
    .locals 6
    .param p1, "sourceList"    # Lcom/google/android/libraries/bind/data/DataList;
    .param p2, "rowId"    # Ljava/lang/Object;
    .param p3, "filter"    # Lcom/google/android/libraries/bind/data/Filter;
    .param p4, "equalityFields"    # [I

    .prologue
    .line 22
    iget v4, p1, Lcom/google/android/libraries/bind/data/DataList;->primaryKey:I

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/data/FilteredDataList;-><init>(Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Filter;[IIZ)V

    .line 27
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->rowId:Ljava/lang/Object;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/libraries/bind/data/FilteredDataRow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/libraries/bind/data/FilteredDataRow;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->emptyWhenNone:Z

    return v0
.end method


# virtual methods
.method protected makeRefreshTask()Lcom/google/android/libraries/bind/data/RefreshTask;
    .locals 8

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->rowId:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v7

    .line 46
    .local v7, "matchingPosition":I
    const/4 v0, -0x1

    if-ne v7, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->emptyWhenNone:Z

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    .line 51
    .local v6, "rowData":Lcom/google/android/libraries/bind/data/Data;
    new-instance v0, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;

    sget-object v3, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->filter:Lcom/google/android/libraries/bind/data/Filter;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->sourceList:Lcom/google/android/libraries/bind/data/DataList;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/bind/data/FilteredDataRow$1;-><init>(Lcom/google/android/libraries/bind/data/FilteredDataRow;Lcom/google/android/libraries/bind/data/DataList;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/Filter;Lcom/google/android/libraries/bind/data/DataList;Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method

.method public setEmptyWhenNone(Z)Lcom/google/android/libraries/bind/data/FilteredDataRow;
    .locals 1
    .param p1, "emptyWhenNone"    # Z

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->emptyWhenNone:Z

    if-eq v0, p1, :cond_0

    .line 36
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/FilteredDataRow;->emptyWhenNone:Z

    .line 37
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/FilteredDataRow;->invalidateData()V

    .line 39
    :cond_0
    return-object p0
.end method
