.class public Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;
.super Ljava/lang/Object;
.source "CardGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GroupRow"
.end annotation


# instance fields
.field private final cardCount:I

.field public final row:Lcom/google/android/libraries/bind/data/Data;

.field private final viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;


# direct methods
.method public constructor <init>(ILcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "cardCount"    # I
    .param p2, "row"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->cardCount:I

    .line 63
    iput-object p2, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/card/ViewGenerator;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "viewGenerator"    # Lcom/google/android/libraries/bind/card/ViewGenerator;
    .param p2, "row"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    .line 68
    iput-object p2, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->row:Lcom/google/android/libraries/bind/data/Data;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->cardCount:I

    .line 70
    return-void
.end method


# virtual methods
.method public getCardCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->cardCount:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardGroup$GroupRow;->viewGenerator:Lcom/google/android/libraries/bind/card/ViewGenerator;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/card/ViewGenerator;->getCardIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
