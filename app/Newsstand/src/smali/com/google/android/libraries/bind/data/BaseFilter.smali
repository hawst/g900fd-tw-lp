.class public abstract Lcom/google/android/libraries/bind/data/BaseFilter;
.super Ljava/lang/Object;
.source "BaseFilter.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Filter;


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "executor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/BaseFilter;->executor:Ljava/util/concurrent/Executor;

    .line 15
    return-void
.end method


# virtual methods
.method public executor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BaseFilter;->executor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public filterPriority()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public load(Lcom/google/android/libraries/bind/data/Data;Lcom/google/android/libraries/bind/data/RefreshTask;)Z
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public onNewDataList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 0
    .param p1, "dataList"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 28
    return-void
.end method

.method public onPreFilter()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public transform(Ljava/util/List;Lcom/google/android/libraries/bind/data/RefreshTask;)Ljava/util/List;
    .locals 0
    .param p2, "refreshTask"    # Lcom/google/android/libraries/bind/data/RefreshTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;",
            "Lcom/google/android/libraries/bind/data/RefreshTask;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/bind/data/Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "inputDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    return-object p1
.end method
