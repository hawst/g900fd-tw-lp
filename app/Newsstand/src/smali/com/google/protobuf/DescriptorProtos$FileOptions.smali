.class public final Lcom/google/protobuf/DescriptorProtos$FileOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$FileOptions$OptimizeMode;,
        Lcom/google/protobuf/DescriptorProtos$FileOptions$CompatibilityLevel;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileOptions;


# instance fields
.field public ccApiCompatibility:I

.field public ccApiVersion:I

.field public ccEnableArenas:Z

.field public ccEnableMaps:Z

.field public ccGenericServices:Z

.field public ccProto1TextFormat:Z

.field public ccProtoArrayCompatible:Z

.field public ccUtf8Verification:Z

.field public deprecated:Z

.field public experimentalStyle:Ljava/lang/String;

.field public goForceProto2Api:Z

.field public goPackage:Ljava/lang/String;

.field public javaAltApiPackage:Ljava/lang/String;

.field public javaApiVersion:I

.field public javaEnableDualGenerateMutableApi:Z

.field public javaGenerateEqualsAndHash:Z

.field public javaGenerateRpcBaseimpl:Z

.field public javaGenericServices:Z

.field public javaJava5Enums:Z

.field public javaMultipleFiles:Z

.field public javaMultipleFilesMutablePackage:Ljava/lang/String;

.field public javaMutableApi:Z

.field public javaOuterClassname:Ljava/lang/String;

.field public javaPackage:Ljava/lang/String;

.field public javaStringCheckUtf8:Z

.field public javaUseJavaproto2:Z

.field public javaUseJavastrings:Z

.field public javascriptPackage:Ljava/lang/String;

.field public optimizeFor:I

.field public pyApiVersion:I

.field public pyGenericServices:Z

.field public szlApiVersion:I

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1920
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$FileOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1921
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1936
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    .line 1939
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    .line 1942
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    .line 1945
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    .line 1948
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    .line 1951
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    .line 1954
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    .line 1957
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    .line 1960
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    .line 1963
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    .line 1966
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    .line 1969
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    .line 1972
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    .line 1975
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    .line 1978
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    .line 1981
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    .line 1984
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    .line 1987
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    .line 1990
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    .line 1993
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    .line 1996
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    .line 1999
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    .line 2002
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    .line 2005
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    .line 2008
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    .line 2011
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    .line 2014
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    .line 2017
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    .line 2020
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    .line 2023
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    .line 2026
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    .line 2029
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    .line 2032
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 1921
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2519
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 2513
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2035
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    .line 2036
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    .line 2037
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    .line 2038
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    .line 2039
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    .line 2040
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    .line 2041
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    .line 2042
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    .line 2043
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    .line 2044
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    .line 2045
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    .line 2046
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    .line 2047
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    .line 2048
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    .line 2049
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    .line 2050
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    .line 2051
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    .line 2052
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    .line 2053
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    .line 2054
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    .line 2055
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    .line 2056
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    .line 2057
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    .line 2058
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    .line 2059
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    .line 2060
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    .line 2061
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    .line 2062
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    .line 2063
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    .line 2064
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    .line 2065
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    .line 2066
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    .line 2067
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 2068
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    .line 2069
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->cachedSize:I

    .line 2070
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2185
    const/4 v1, 0x0

    .line 2186
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2187
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    .line 2188
    invoke-static {v4, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2190
    :cond_0
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    if-eq v2, v5, :cond_1

    .line 2191
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    .line 2192
    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2194
    :cond_1
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    if-eq v2, v5, :cond_2

    .line 2195
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    .line 2196
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2198
    :cond_2
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    if-eq v2, v5, :cond_3

    .line 2199
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    .line 2200
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2202
    :cond_3
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    if-eq v2, v4, :cond_4

    .line 2203
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    .line 2204
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2206
    :cond_4
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    if-eq v2, v4, :cond_5

    .line 2207
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    .line 2208
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2210
    :cond_5
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2211
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    .line 2212
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2214
    :cond_6
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    if-eq v2, v4, :cond_7

    .line 2215
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    .line 2216
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2218
    :cond_7
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    if-eqz v2, :cond_8

    .line 2219
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    .line 2220
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2222
    :cond_8
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2223
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    .line 2224
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2226
    :cond_9
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2227
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    .line 2228
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2230
    :cond_a
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    if-eqz v2, :cond_b

    .line 2231
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    .line 2232
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2234
    :cond_b
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    if-eq v2, v4, :cond_c

    .line 2235
    const/16 v2, 0xe

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    .line 2236
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2238
    :cond_c
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    if-eqz v2, :cond_d

    .line 2239
    const/16 v2, 0xf

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    .line 2240
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 2242
    :cond_d
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    if-eqz v2, :cond_e

    .line 2243
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    .line 2244
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2246
    :cond_e
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    if-eqz v2, :cond_f

    .line 2247
    const/16 v2, 0x11

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    .line 2248
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2250
    :cond_f
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    if-eqz v2, :cond_10

    .line 2251
    const/16 v2, 0x12

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    .line 2252
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2254
    :cond_10
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 2255
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    .line 2256
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2258
    :cond_11
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    if-eqz v2, :cond_12

    .line 2259
    const/16 v2, 0x14

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    .line 2260
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2262
    :cond_12
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    if-eqz v2, :cond_13

    .line 2263
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    .line 2264
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2266
    :cond_13
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    if-eq v2, v4, :cond_14

    .line 2267
    const/16 v2, 0x16

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    .line 2268
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2270
    :cond_14
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    if-eqz v2, :cond_15

    .line 2271
    const/16 v2, 0x17

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    .line 2272
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2274
    :cond_15
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    if-eq v2, v4, :cond_16

    .line 2275
    const/16 v2, 0x18

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    .line 2276
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2278
    :cond_16
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    if-eqz v2, :cond_17

    .line 2279
    const/16 v2, 0x19

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    .line 2280
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2282
    :cond_17
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    if-eqz v2, :cond_18

    .line 2283
    const/16 v2, 0x1a

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    .line 2284
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2286
    :cond_18
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    if-eqz v2, :cond_19

    .line 2287
    const/16 v2, 0x1b

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    .line 2288
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2290
    :cond_19
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    if-eqz v2, :cond_1a

    .line 2291
    const/16 v2, 0x1c

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    .line 2292
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2294
    :cond_1a
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 2295
    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    .line 2296
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2298
    :cond_1b
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    .line 2299
    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    .line 2300
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2302
    :cond_1c
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    if-eqz v2, :cond_1d

    .line 2303
    const/16 v2, 0x1f

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    .line 2304
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2306
    :cond_1d
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    if-eqz v2, :cond_1e

    .line 2307
    const/16 v2, 0x20

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    .line 2308
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2310
    :cond_1e
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    if-eqz v2, :cond_1f

    .line 2311
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    .line 2312
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 2314
    :cond_1f
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_21

    .line 2315
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_21

    aget-object v0, v3, v2

    .line 2316
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_20

    .line 2317
    const/16 v5, 0x3e7

    .line 2318
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 2315
    :cond_20
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2322
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_21
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2323
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->cachedSize:I

    .line 2324
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileOptions;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 2332
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2333
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2337
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    if-nez v6, :cond_1

    .line 2338
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    .line 2341
    :cond_1
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v6, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2343
    :sswitch_0
    return-object p0

    .line 2348
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    goto :goto_0

    .line 2352
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    goto :goto_0

    .line 2356
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    goto :goto_0

    .line 2360
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    goto :goto_0

    .line 2364
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    goto :goto_0

    .line 2368
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    goto :goto_0

    .line 2372
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    goto :goto_0

    .line 2376
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 2377
    .local v4, "temp":I
    if-eq v4, v8, :cond_2

    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-ne v4, v6, :cond_3

    .line 2380
    :cond_2
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    goto :goto_0

    .line 2382
    :cond_3
    iput v8, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    goto :goto_0

    .line 2387
    .end local v4    # "temp":I
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    goto :goto_0

    .line 2391
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    goto :goto_0

    .line 2395
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    goto :goto_0

    .line 2399
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    goto :goto_0

    .line 2403
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    goto :goto_0

    .line 2407
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 2408
    .restart local v4    # "temp":I
    if-eqz v4, :cond_4

    const/16 v6, 0x64

    if-eq v4, v6, :cond_4

    const/16 v6, 0x32

    if-ne v4, v6, :cond_5

    .line 2411
    :cond_4
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    goto/16 :goto_0

    .line 2413
    :cond_5
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    goto/16 :goto_0

    .line 2418
    .end local v4    # "temp":I
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    goto/16 :goto_0

    .line 2422
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    goto/16 :goto_0

    .line 2426
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    goto/16 :goto_0

    .line 2430
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    goto/16 :goto_0

    .line 2434
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    goto/16 :goto_0

    .line 2438
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    goto/16 :goto_0

    .line 2442
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    goto/16 :goto_0

    .line 2446
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    goto/16 :goto_0

    .line 2450
    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    goto/16 :goto_0

    .line 2454
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    goto/16 :goto_0

    .line 2458
    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    goto/16 :goto_0

    .line 2462
    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    goto/16 :goto_0

    .line 2466
    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    goto/16 :goto_0

    .line 2470
    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    goto/16 :goto_0

    .line 2474
    :sswitch_1d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    goto/16 :goto_0

    .line 2478
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    goto/16 :goto_0

    .line 2482
    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    goto/16 :goto_0

    .line 2486
    :sswitch_20
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    goto/16 :goto_0

    .line 2490
    :sswitch_21
    const/16 v6, 0x1f3a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2491
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v6, :cond_7

    move v1, v5

    .line 2492
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 2493
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v6, :cond_6

    .line 2494
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2496
    :cond_6
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 2497
    :goto_2
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_8

    .line 2498
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 2499
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2500
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2497
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2491
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_7
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v6

    goto :goto_1

    .line 2503
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_8
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 2504
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2333
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa0 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb0 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc0 -> :sswitch_17
        0xc8 -> :sswitch_18
        0xd0 -> :sswitch_19
        0xd8 -> :sswitch_1a
        0xe0 -> :sswitch_1b
        0xea -> :sswitch_1c
        0xf2 -> :sswitch_1d
        0xf8 -> :sswitch_1e
        0x100 -> :sswitch_1f
        0x108 -> :sswitch_20
        0x1f3a -> :sswitch_21
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1917
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$FileOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2076
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2077
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaPackage:Ljava/lang/String;

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2079
    :cond_0
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    if-eq v1, v4, :cond_1

    .line 2080
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiVersion:I

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2082
    :cond_1
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    if-eq v1, v4, :cond_2

    .line 2083
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyApiVersion:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2085
    :cond_2
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    if-eq v1, v4, :cond_3

    .line 2086
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaApiVersion:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2088
    :cond_3
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    if-eq v1, v3, :cond_4

    .line 2089
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavaproto2:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2091
    :cond_4
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    if-eq v1, v3, :cond_5

    .line 2092
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaJava5Enums:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2094
    :cond_5
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2095
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaOuterClassname:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2097
    :cond_6
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    if-eq v1, v3, :cond_7

    .line 2098
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->optimizeFor:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2100
    :cond_7
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    if-eqz v1, :cond_8

    .line 2101
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFiles:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2103
    :cond_8
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2104
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goPackage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2106
    :cond_9
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 2107
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javascriptPackage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2109
    :cond_a
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    if-eqz v1, :cond_b

    .line 2110
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateRpcBaseimpl:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2112
    :cond_b
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    if-eq v1, v3, :cond_c

    .line 2113
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->szlApiVersion:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2115
    :cond_c
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    if-eqz v1, :cond_d

    .line 2116
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccApiCompatibility:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2118
    :cond_d
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    if-eqz v1, :cond_e

    .line 2119
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccGenericServices:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2121
    :cond_e
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    if-eqz v1, :cond_f

    .line 2122
    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenericServices:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2124
    :cond_f
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    if-eqz v1, :cond_10

    .line 2125
    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->pyGenericServices:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2127
    :cond_10
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 2128
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaAltApiPackage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2130
    :cond_11
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    if-eqz v1, :cond_12

    .line 2131
    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaGenerateEqualsAndHash:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2133
    :cond_12
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    if-eqz v1, :cond_13

    .line 2134
    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaUseJavastrings:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2136
    :cond_13
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    if-eq v1, v3, :cond_14

    .line 2137
    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProtoArrayCompatible:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2139
    :cond_14
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    if-eqz v1, :cond_15

    .line 2140
    const/16 v1, 0x17

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->deprecated:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2142
    :cond_15
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    if-eq v1, v3, :cond_16

    .line 2143
    const/16 v1, 0x18

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccUtf8Verification:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2145
    :cond_16
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    if-eqz v1, :cond_17

    .line 2146
    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccProto1TextFormat:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2148
    :cond_17
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    if-eqz v1, :cond_18

    .line 2149
    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaEnableDualGenerateMutableApi:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2151
    :cond_18
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    if-eqz v1, :cond_19

    .line 2152
    const/16 v1, 0x1b

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaStringCheckUtf8:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2154
    :cond_19
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    if-eqz v1, :cond_1a

    .line 2155
    const/16 v1, 0x1c

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMutableApi:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2157
    :cond_1a
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 2158
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->javaMultipleFilesMutablePackage:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2160
    :cond_1b
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 2161
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->experimentalStyle:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2163
    :cond_1c
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    if-eqz v1, :cond_1d

    .line 2164
    const/16 v1, 0x1f

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableArenas:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2166
    :cond_1d
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    if-eqz v1, :cond_1e

    .line 2167
    const/16 v1, 0x20

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->ccEnableMaps:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2169
    :cond_1e
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    if-eqz v1, :cond_1f

    .line 2170
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->goForceProto2Api:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2172
    :cond_1f
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v1, :cond_21

    .line 2173
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_21

    aget-object v0, v2, v1

    .line 2174
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_20

    .line 2175
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2173
    :cond_20
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2179
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_21
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2181
    return-void
.end method
