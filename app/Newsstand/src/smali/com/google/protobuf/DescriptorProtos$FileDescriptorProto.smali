.class public final Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileDescriptorProto"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;


# instance fields
.field public dependency:[Ljava/lang/String;

.field public enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

.field public extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

.field public messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

.field public name:Ljava/lang/String;

.field public options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

.field public package_:Ljava/lang/String;

.field public publicDependency:[I

.field public service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

.field public sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

.field public syntax:Ljava/lang/String;

.field public weakDependency:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    .line 120
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    .line 123
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    .line 126
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    .line 129
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 132
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 135
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    .line 138
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 141
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    .line 144
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 491
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 485
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    .line 151
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    .line 152
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    .line 153
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    .line 154
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    .line 155
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 156
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 157
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    .line 158
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 159
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    .line 160
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    .line 161
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    .line 162
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 163
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->cachedSize:I

    .line 164
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 234
    const/4 v2, 0x0

    .line 235
    .local v2, "size":I
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 236
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    .line 237
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 239
    :cond_0
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 240
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    .line 241
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 243
    :cond_1
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v4, v4

    if-lez v4, :cond_3

    .line 244
    const/4 v0, 0x0

    .line 245
    .local v0, "dataSize":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v1, v5, v4

    .line 247
    .local v1, "element":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v0, v7

    .line 245
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 249
    .end local v1    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v2, v0

    .line 250
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 252
    .end local v0    # "dataSize":I
    :cond_3
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v4, :cond_5

    .line 253
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v1, v5, v4

    .line 254
    .local v1, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    if-eqz v1, :cond_4

    .line 255
    const/4 v7, 0x4

    .line 256
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 253
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 260
    .end local v1    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_5
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v4, :cond_7

    .line 261
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v1, v5, v4

    .line 262
    .local v1, "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    if-eqz v1, :cond_6

    .line 263
    const/4 v7, 0x5

    .line 264
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 261
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 268
    .end local v1    # "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_7
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    if-eqz v4, :cond_9

    .line 269
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    array-length v6, v5

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_9

    aget-object v1, v5, v4

    .line 270
    .local v1, "element":Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    if-eqz v1, :cond_8

    .line 271
    const/4 v7, 0x6

    .line 272
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 269
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 276
    .end local v1    # "element":Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    :cond_9
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v4, :cond_b

    .line 277
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v6, v5

    move v4, v3

    :goto_4
    if-ge v4, v6, :cond_b

    aget-object v1, v5, v4

    .line 278
    .local v1, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v1, :cond_a

    .line 279
    const/4 v7, 0x7

    .line 280
    invoke-static {v7, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v7

    add-int/2addr v2, v7

    .line 277
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 284
    .end local v1    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_b
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-eqz v4, :cond_c

    .line 285
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    .line 286
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 288
    :cond_c
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    if-eqz v4, :cond_d

    .line 289
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    .line 290
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v2, v4

    .line 292
    :cond_d
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v4, v4

    if-lez v4, :cond_f

    .line 293
    const/4 v0, 0x0

    .line 294
    .restart local v0    # "dataSize":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v6, v5

    move v4, v3

    :goto_5
    if-ge v4, v6, :cond_e

    aget v1, v5, v4

    .line 296
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v0, v7

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 298
    .end local v1    # "element":I
    :cond_e
    add-int/2addr v2, v0

    .line 299
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 301
    .end local v0    # "dataSize":I
    :cond_f
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v4, v4

    if-lez v4, :cond_11

    .line 302
    const/4 v0, 0x0

    .line 303
    .restart local v0    # "dataSize":I
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v5, v4

    :goto_6
    if-ge v3, v5, :cond_10

    aget v1, v4, v3

    .line 305
    .restart local v1    # "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v6

    add-int/2addr v0, v6

    .line 303
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 307
    .end local v1    # "element":I
    :cond_10
    add-int/2addr v2, v0

    .line 308
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v3, v3

    mul-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    .line 310
    .end local v0    # "dataSize":I
    :cond_11
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 311
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    .line 312
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 314
    :cond_12
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v3}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v3

    add-int/2addr v2, v3

    .line 315
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->cachedSize:I

    .line 316
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 324
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 325
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 329
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 330
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 333
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 335
    :sswitch_0
    return-object p0

    .line 340
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    goto :goto_0

    .line 344
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    goto :goto_0

    .line 348
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 349
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v1, v5

    .line 350
    .local v1, "i":I
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 351
    .local v2, "newArray":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    .line 353
    :goto_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_2

    .line 354
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 355
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 353
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 358
    :cond_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    goto :goto_0

    .line 362
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 363
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-nez v5, :cond_4

    move v1, v4

    .line 364
    .restart local v1    # "i":I
    :goto_2
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 365
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v5, :cond_3

    .line 366
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 368
    :cond_3
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 369
    :goto_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 370
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 371
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 372
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 369
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 363
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v1, v5

    goto :goto_2

    .line 375
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_5
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 376
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 380
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 381
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-nez v5, :cond_7

    move v1, v4

    .line 382
    .restart local v1    # "i":I
    :goto_4
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 383
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v5, :cond_6

    .line 384
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 386
    :cond_6
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 387
    :goto_5
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_8

    .line 388
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 389
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 390
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 387
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 381
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_7
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v1, v5

    goto :goto_4

    .line 393
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_8
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 394
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 398
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 399
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    if-nez v5, :cond_a

    move v1, v4

    .line 400
    .restart local v1    # "i":I
    :goto_6
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    .line 401
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    if-eqz v5, :cond_9

    .line 402
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 404
    :cond_9
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    .line 405
    :goto_7
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_b

    .line 406
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 407
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 408
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 405
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 399
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    :cond_a
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    array-length v1, v5

    goto :goto_6

    .line 411
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    :cond_b
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 412
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 416
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 417
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-nez v5, :cond_d

    move v1, v4

    .line 418
    .restart local v1    # "i":I
    :goto_8
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 419
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v5, :cond_c

    .line 420
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 422
    :cond_c
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 423
    :goto_9
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_e

    .line 424
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 425
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 426
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 417
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_d
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v1, v5

    goto :goto_8

    .line 429
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_e
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 430
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 434
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :sswitch_8
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-nez v5, :cond_f

    .line 435
    new-instance v5, Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-direct {v5}, Lcom/google/protobuf/DescriptorProtos$FileOptions;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    .line 437
    :cond_f
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 441
    :sswitch_9
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    if-nez v5, :cond_10

    .line 442
    new-instance v5, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    invoke-direct {v5}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    .line 444
    :cond_10
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 448
    :sswitch_a
    const/16 v5, 0x50

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 449
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v1, v5

    .line 450
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 451
    .local v2, "newArray":[I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 452
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    .line 453
    :goto_a
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_11

    .line 454
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 455
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 453
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 458
    :cond_11
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 462
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_b
    const/16 v5, 0x58

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 463
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v1, v5

    .line 464
    .restart local v1    # "i":I
    add-int v5, v1, v0

    new-array v2, v5, [I

    .line 465
    .restart local v2    # "newArray":[I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 466
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    .line 467
    :goto_b
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_12

    .line 468
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    .line 469
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 467
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 472
    :cond_12
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    aput v6, v5, v1

    goto/16 :goto_0

    .line 476
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[I
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    goto/16 :goto_0

    .line 325
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->name:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 173
    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 174
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->package_:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 176
    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 177
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->dependency:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 178
    .local v0, "element":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 177
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "element":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v2, :cond_4

    .line 182
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->messageType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 183
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    if-eqz v0, :cond_3

    .line 184
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 182
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 188
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_4
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v2, :cond_6

    .line 189
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v0, v3, v2

    .line 190
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    if-eqz v0, :cond_5

    .line 191
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 189
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 195
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_6
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    if-eqz v2, :cond_8

    .line 196
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->service:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_8

    aget-object v0, v3, v2

    .line 197
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    if-eqz v0, :cond_7

    .line 198
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 196
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 202
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    :cond_8
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v2, :cond_a

    .line 203
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_a

    aget-object v0, v3, v2

    .line 204
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v0, :cond_9

    .line 205
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 203
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 209
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_a
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    if-eqz v2, :cond_b

    .line 210
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$FileOptions;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 212
    :cond_b
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    if-eqz v2, :cond_c

    .line 213
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->sourceCodeInfo:Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 215
    :cond_c
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    if-eqz v2, :cond_d

    .line 216
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->publicDependency:[I

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_d

    aget v0, v3, v2

    .line 217
    .local v0, "element":I
    const/16 v5, 0xa

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 216
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 220
    .end local v0    # "element":I
    :cond_d
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    if-eqz v2, :cond_e

    .line 221
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->weakDependency:[I

    array-length v3, v2

    :goto_6
    if-ge v1, v3, :cond_e

    aget v0, v2, v1

    .line 222
    .restart local v0    # "element":I
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 225
    .end local v0    # "element":I
    :cond_e
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 226
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->syntax:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 228
    :cond_f
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 230
    return-void
.end method
