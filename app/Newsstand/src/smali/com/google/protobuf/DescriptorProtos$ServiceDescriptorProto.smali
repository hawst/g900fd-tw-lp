.class public final Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceDescriptorProto"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;


# instance fields
.field public method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

.field public name:Ljava/lang/String;

.field public options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

.field public stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1495
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1496
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1499
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    .line 1502
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    .line 1505
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    .line 1508
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    .line 1496
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1658
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1652
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1511
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    .line 1512
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    .line 1513
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    .line 1514
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    .line 1515
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1516
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->cachedSize:I

    .line 1517
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1549
    const/4 v1, 0x0

    .line 1550
    .local v1, "size":I
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1551
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    .line 1552
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1554
    :cond_0
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    if-eqz v3, :cond_2

    .line 1555
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 1556
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    if-eqz v0, :cond_1

    .line 1557
    const/4 v6, 0x2

    .line 1558
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 1555
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1562
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    :cond_2
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    if-eqz v3, :cond_3

    .line 1563
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    .line 1564
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1566
    :cond_3
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    if-eqz v3, :cond_5

    .line 1567
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v0, v3, v2

    .line 1568
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    if-eqz v0, :cond_4

    .line 1569
    const/4 v5, 0x4

    .line 1570
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1567
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1574
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    :cond_5
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1575
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->cachedSize:I

    .line 1576
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1584
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1585
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1589
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 1590
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1593
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1595
    :sswitch_0
    return-object p0

    .line 1600
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    goto :goto_0

    .line 1604
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1605
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 1606
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    .line 1607
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    if-eqz v5, :cond_2

    .line 1608
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1610
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    .line 1611
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 1612
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1613
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1614
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1611
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1605
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    array-length v1, v5

    goto :goto_1

    .line 1617
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1618
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1622
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    :sswitch_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    if-nez v5, :cond_5

    .line 1623
    new-instance v5, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    invoke-direct {v5}, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    .line 1625
    :cond_5
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1629
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1630
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    if-nez v5, :cond_7

    move v1, v4

    .line 1631
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    .line 1632
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    if-eqz v5, :cond_6

    .line 1633
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1635
    :cond_6
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    .line 1636
    :goto_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_8

    .line 1637
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1638
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1639
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1636
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1630
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    :cond_7
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    array-length v1, v5

    goto :goto_3

    .line 1642
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    :cond_8
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1643
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1585
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1492
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1523
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1524
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->name:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1526
    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    if-eqz v2, :cond_2

    .line 1527
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->method:[Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 1528
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    if-eqz v0, :cond_1

    .line 1529
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1527
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1533
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    if-eqz v2, :cond_3

    .line 1534
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1536
    :cond_3
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    if-eqz v2, :cond_5

    .line 1537
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->stream:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v0, v2, v1

    .line 1538
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    if-eqz v0, :cond_4

    .line 1539
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1537
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1543
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    :cond_5
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1545
    return-void
.end method
