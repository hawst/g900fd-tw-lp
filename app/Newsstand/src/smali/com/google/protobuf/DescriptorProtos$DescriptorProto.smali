.class public final Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DescriptorProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;


# instance fields
.field public enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

.field public extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

.field public extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

.field public field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

.field public name:Ljava/lang/String;

.field public nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

.field public oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

.field public options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 597
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    .line 600
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 603
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 606
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 609
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 612
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    .line 615
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    .line 618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    .line 500
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 904
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 898
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 621
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    .line 622
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 623
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 624
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 625
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 626
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    .line 627
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    .line 628
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    .line 629
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 630
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->cachedSize:I

    .line 631
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 691
    const/4 v1, 0x0

    .line 692
    .local v1, "size":I
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 693
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    .line 694
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 696
    :cond_0
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v3, :cond_2

    .line 697
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 698
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v0, :cond_1

    .line 699
    const/4 v6, 0x2

    .line 700
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 697
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 704
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_2
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v3, :cond_4

    .line 705
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v0, v4, v3

    .line 706
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    if-eqz v0, :cond_3

    .line 707
    const/4 v6, 0x3

    .line 708
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 705
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 712
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_4
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v3, :cond_6

    .line 713
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_6

    aget-object v0, v4, v3

    .line 714
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    if-eqz v0, :cond_5

    .line 715
    const/4 v6, 0x4

    .line 716
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 713
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 720
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_6
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    if-eqz v3, :cond_8

    .line 721
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    array-length v5, v4

    move v3, v2

    :goto_3
    if-ge v3, v5, :cond_8

    aget-object v0, v4, v3

    .line 722
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    if-eqz v0, :cond_7

    .line 723
    const/4 v6, 0x5

    .line 724
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 721
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 728
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    :cond_8
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v3, :cond_a

    .line 729
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v5, v4

    move v3, v2

    :goto_4
    if-ge v3, v5, :cond_a

    aget-object v0, v4, v3

    .line 730
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v0, :cond_9

    .line 731
    const/4 v6, 0x6

    .line 732
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 729
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 736
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_a
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    if-eqz v3, :cond_b

    .line 737
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    .line 738
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v1, v3

    .line 740
    :cond_b
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    if-eqz v3, :cond_d

    .line 741
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    array-length v4, v3

    :goto_5
    if-ge v2, v4, :cond_d

    aget-object v0, v3, v2

    .line 742
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    if-eqz v0, :cond_c

    .line 743
    const/16 v5, 0x8

    .line 744
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 741
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 748
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    :cond_d
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 749
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->cachedSize:I

    .line 750
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 759
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 763
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 764
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 767
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 769
    :sswitch_0
    return-object p0

    .line 774
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    goto :goto_0

    .line 778
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 779
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 780
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 781
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v5, :cond_2

    .line 782
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 784
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 785
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 786
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 787
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 788
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 785
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 779
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v1, v5

    goto :goto_1

    .line 791
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 792
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 796
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 797
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-nez v5, :cond_6

    move v1, v4

    .line 798
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 799
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v5, :cond_5

    .line 800
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 802
    :cond_5
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    .line 803
    :goto_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 804
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 805
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 806
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 803
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 797
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_6
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v1, v5

    goto :goto_3

    .line 809
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_7
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 810
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 814
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 815
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-nez v5, :cond_9

    move v1, v4

    .line 816
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 817
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v5, :cond_8

    .line 818
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 820
    :cond_8
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    .line 821
    :goto_6
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_a

    .line 822
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 823
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 824
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 821
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 815
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_9
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v1, v5

    goto :goto_5

    .line 827
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_a
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 828
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 832
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 833
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    if-nez v5, :cond_c

    move v1, v4

    .line 834
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    .line 835
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    if-eqz v5, :cond_b

    .line 836
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 838
    :cond_b
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    .line 839
    :goto_8
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_d

    .line 840
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;-><init>()V

    aput-object v6, v5, v1

    .line 841
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 842
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 839
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 833
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    :cond_c
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    array-length v1, v5

    goto :goto_7

    .line 845
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    :cond_d
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;-><init>()V

    aput-object v6, v5, v1

    .line 846
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 850
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 851
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-nez v5, :cond_f

    move v1, v4

    .line 852
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 853
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v5, :cond_e

    .line 854
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 856
    :cond_e
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    .line 857
    :goto_a
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_10

    .line 858
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 859
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 860
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 857
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 851
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_f
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v1, v5

    goto :goto_9

    .line 863
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_10
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 864
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 868
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :sswitch_7
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    if-nez v5, :cond_11

    .line 869
    new-instance v5, Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    invoke-direct {v5}, Lcom/google/protobuf/DescriptorProtos$MessageOptions;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    .line 871
    :cond_11
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 875
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 876
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    if-nez v5, :cond_13

    move v1, v4

    .line 877
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    .line 878
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    if-eqz v5, :cond_12

    .line 879
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 881
    :cond_12
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    .line 882
    :goto_c
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_14

    .line 883
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 884
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 885
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 882
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 876
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    :cond_13
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    array-length v1, v5

    goto :goto_b

    .line 888
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    :cond_14
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 889
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 759
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 496
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 637
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 638
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->name:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 640
    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v2, :cond_2

    .line 641
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->field:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 642
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v0, :cond_1

    .line 643
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 641
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 647
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    if-eqz v2, :cond_4

    .line 648
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->nestedType:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 649
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    if-eqz v0, :cond_3

    .line 650
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 648
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 654
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto;
    :cond_4
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    if-eqz v2, :cond_6

    .line 655
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->enumType:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v0, v3, v2

    .line 656
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    if-eqz v0, :cond_5

    .line 657
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 655
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 661
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    :cond_6
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    if-eqz v2, :cond_8

    .line 662
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extensionRange:[Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_8

    aget-object v0, v3, v2

    .line 663
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    if-eqz v0, :cond_7

    .line 664
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 662
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 668
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$DescriptorProto$ExtensionRange;
    :cond_8
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    if-eqz v2, :cond_a

    .line 669
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->extension:[Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_a

    aget-object v0, v3, v2

    .line 670
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    if-eqz v0, :cond_9

    .line 671
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 669
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 675
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;
    :cond_a
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    if-eqz v2, :cond_b

    .line 676
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$MessageOptions;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 678
    :cond_b
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    if-eqz v2, :cond_d

    .line 679
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->oneofDecl:[Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;

    array-length v3, v2

    :goto_5
    if-ge v1, v3, :cond_d

    aget-object v0, v2, v1

    .line 680
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    if-eqz v0, :cond_c

    .line 681
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 679
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 685
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;
    :cond_d
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$DescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 687
    return-void
.end method
