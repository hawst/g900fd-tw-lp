.class public final Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UninterpretedOption"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# instance fields
.field public aggregateValue:Ljava/lang/String;

.field public doubleValue:D

.field public identifierValue:Ljava/lang/String;

.field public name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

.field public negativeIntValue:J

.field public positiveIntValue:J

.field public stringValue:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4353
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 4354
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 4443
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    .line 4446
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    .line 4449
    iput-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    .line 4452
    iput-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    .line 4455
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    .line 4458
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    .line 4461
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    .line 4354
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4622
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 4616
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 4464
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    .line 4465
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    .line 4466
    iput-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    .line 4467
    iput-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    .line 4468
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    .line 4469
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    .line 4470
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    .line 4471
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    .line 4472
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->cachedSize:I

    .line 4473
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 4510
    const/4 v1, 0x0

    .line 4511
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    if-eqz v2, :cond_1

    .line 4512
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 4513
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    if-eqz v0, :cond_0

    .line 4514
    const/4 v5, 0x2

    .line 4515
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 4512
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4519
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4520
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    .line 4521
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4523
    :cond_2
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 4524
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    .line 4525
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4527
    :cond_3
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_4

    .line 4528
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    .line 4529
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4531
    :cond_4
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_5

    .line 4532
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    .line 4533
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v1, v2

    .line 4535
    :cond_5
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    .line 4536
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    .line 4537
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v2

    add-int/2addr v1, v2

    .line 4539
    :cond_6
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 4540
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    .line 4541
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4543
    :cond_7
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4544
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->cachedSize:I

    .line 4545
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4553
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4554
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4558
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 4559
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    .line 4562
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4564
    :sswitch_0
    return-object p0

    .line 4569
    :sswitch_1
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4570
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    if-nez v5, :cond_3

    move v1, v4

    .line 4571
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    .line 4572
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    if-eqz v5, :cond_2

    .line 4573
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4575
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    .line 4576
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 4577
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;-><init>()V

    aput-object v6, v5, v1

    .line 4578
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4579
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4576
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4570
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    array-length v1, v5

    goto :goto_1

    .line 4582
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;-><init>()V

    aput-object v6, v5, v1

    .line 4583
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4587
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    goto :goto_0

    .line 4591
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    goto :goto_0

    .line 4595
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    goto :goto_0

    .line 4599
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    goto/16 :goto_0

    .line 4603
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    goto/16 :goto_0

    .line 4607
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    goto/16 :goto_0

    .line 4554
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x31 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4350
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 4479
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    if-eqz v1, :cond_1

    .line 4480
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->name:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 4481
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    if-eqz v0, :cond_0

    .line 4482
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4480
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4486
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption$NamePart;
    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4487
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->identifierValue:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4489
    :cond_2
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 4490
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->positiveIntValue:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 4492
    :cond_3
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_4

    .line 4493
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->negativeIntValue:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4495
    :cond_4
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_5

    .line 4496
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->doubleValue:D

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 4498
    :cond_5
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    .line 4499
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->stringValue:[B

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 4501
    :cond_6
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 4502
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->aggregateValue:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4504
    :cond_7
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4506
    return-void
.end method
