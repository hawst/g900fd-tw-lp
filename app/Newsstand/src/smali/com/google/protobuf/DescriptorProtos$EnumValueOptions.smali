.class public final Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnumValueOptions"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;


# instance fields
.field public deprecated:Z

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3321
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3322
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 3325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    .line 3328
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3322
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3429
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 3423
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;
    .locals 1

    .prologue
    .line 3331
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    .line 3332
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    .line 3334
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->cachedSize:I

    .line 3335
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 3357
    const/4 v1, 0x0

    .line 3358
    .local v1, "size":I
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    if-eqz v2, :cond_0

    .line 3359
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    .line 3360
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3362
    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_2

    .line 3363
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 3364
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_1

    .line 3365
    const/16 v5, 0x3e7

    .line 3366
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3363
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3370
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3371
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->cachedSize:I

    .line 3372
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3380
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3381
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3385
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 3386
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    .line 3389
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3391
    :sswitch_0
    return-object p0

    .line 3396
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    goto :goto_0

    .line 3400
    :sswitch_2
    const/16 v5, 0x1f3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3401
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v5, :cond_3

    move v1, v4

    .line 3402
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3403
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v5, :cond_2

    .line 3404
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3406
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3407
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 3408
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v6, v5, v1

    .line 3409
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3410
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3407
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3401
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v5

    goto :goto_1

    .line 3413
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v6, v5, v1

    .line 3414
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3381
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1f3a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3318
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3341
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    if-eqz v1, :cond_0

    .line 3342
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->deprecated:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3344
    :cond_0
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v1, :cond_2

    .line 3345
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 3346
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_1

    .line 3347
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3345
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3351
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_2
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3353
    return-void
.end method
