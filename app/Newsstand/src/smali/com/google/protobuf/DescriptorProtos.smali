.class public interface abstract Lcom/google/protobuf/DescriptorProtos;
.super Ljava/lang/Object;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;,
        Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;,
        Lcom/google/protobuf/DescriptorProtos$StreamOptions;,
        Lcom/google/protobuf/DescriptorProtos$MethodOptions;,
        Lcom/google/protobuf/DescriptorProtos$ServiceOptions;,
        Lcom/google/protobuf/DescriptorProtos$EnumValueOptions;,
        Lcom/google/protobuf/DescriptorProtos$EnumOptions;,
        Lcom/google/protobuf/DescriptorProtos$FieldOptions;,
        Lcom/google/protobuf/DescriptorProtos$MessageOptions;,
        Lcom/google/protobuf/DescriptorProtos$FileOptions;,
        Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$MethodDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$ServiceDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$OneofDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$FieldDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$DescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;,
        Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
    }
.end annotation
