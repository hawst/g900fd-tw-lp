.class public final Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileDescriptorSet"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;


# instance fields
.field public file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 13
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    .line 10
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->cachedSize:I

    .line 19
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 38
    const/4 v1, 0x0

    .line 39
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    if-eqz v2, :cond_1

    .line 40
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 41
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    if-eqz v0, :cond_0

    .line 42
    const/4 v5, 0x1

    .line 43
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 40
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 48
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->cachedSize:I

    .line 49
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 58
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 62
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 63
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    .line 66
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 68
    :sswitch_0
    return-object p0

    .line 73
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 74
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 75
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    .line 76
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    if-eqz v5, :cond_2

    .line 77
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    .line 80
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 81
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 82
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 83
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 74
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    array-length v1, v5

    goto :goto_1

    .line 86
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 87
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 58
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    if-eqz v1, :cond_1

    .line 26
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->file:[Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 27
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    if-eqz v0, :cond_0

    .line 28
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 26
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 32
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FileDescriptorProto;
    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FileDescriptorSet;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 34
    return-void
.end method
