.class public final Lcom/google/protobuf/DescriptorProtos$MethodOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MethodOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$MethodOptions$LogLevel;,
        Lcom/google/protobuf/DescriptorProtos$MethodOptions$Format;,
        Lcom/google/protobuf/DescriptorProtos$MethodOptions$SecurityLevel;,
        Lcom/google/protobuf/DescriptorProtos$MethodOptions$Protocol;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$MethodOptions;


# instance fields
.field public clientLogging:I

.field public clientStreaming:Z

.field public deadline:D

.field public deprecated:Z

.field public duplicateSuppression:Z

.field public endUserCredsRequested:Z

.field public failFast:Z

.field public legacyClientInitialTokens:J

.field public legacyResultType:Ljava/lang/String;

.field public legacyServerInitialTokens:J

.field public legacyStreamType:Ljava/lang/String;

.field public logLevel:I

.field public protocol:I

.field public requestFormat:I

.field public responseFormat:I

.field public securityLabel:Ljava/lang/String;

.field public securityLevel:I

.field public serverLogging:I

.field public serverStreaming:Z

.field public streamType:Ljava/lang/String;

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3583
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/16 v3, 0x100

    const/4 v2, 0x0

    .line 3584
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 3612
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    .line 3615
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    .line 3618
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    .line 3621
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    .line 3624
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    .line 3627
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    .line 3630
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    .line 3633
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    .line 3636
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    .line 3639
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    .line 3642
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    .line 3645
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    .line 3648
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    .line 3651
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    .line 3654
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    .line 3657
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    .line 3660
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    .line 3663
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    .line 3666
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    .line 3669
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    .line 3672
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3584
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$MethodOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4036
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$MethodOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$MethodOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 4030
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$MethodOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$MethodOptions;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/16 v3, 0x100

    const/4 v2, 0x0

    .line 3675
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    .line 3676
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    .line 3677
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    .line 3678
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    .line 3679
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    .line 3680
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    .line 3681
    iput v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    .line 3682
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    .line 3683
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    .line 3684
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    .line 3685
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    .line 3686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    .line 3687
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    .line 3688
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    .line 3689
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    .line 3690
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    .line 3691
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    .line 3692
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    .line 3693
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    .line 3694
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    .line 3695
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    .line 3697
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->cachedSize:I

    .line 3698
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/16 v6, 0x100

    .line 3777
    const/4 v1, 0x0

    .line 3778
    .local v1, "size":I
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    if-eqz v2, :cond_0

    .line 3779
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    .line 3780
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3782
    :cond_0
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_1

    .line 3783
    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    .line 3784
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v1, v2

    .line 3786
    :cond_1
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    if-eqz v2, :cond_2

    .line 3787
    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    .line 3788
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3790
    :cond_2
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    if-eqz v2, :cond_3

    .line 3791
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    .line 3792
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3794
    :cond_3
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    if-eq v2, v6, :cond_4

    .line 3795
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    .line 3796
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeSInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3798
    :cond_4
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    if-eq v2, v6, :cond_5

    .line 3799
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    .line 3800
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeSInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3802
    :cond_5
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    if-eqz v2, :cond_6

    .line 3803
    const/16 v2, 0xd

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    .line 3804
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3806
    :cond_6
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    if-eqz v2, :cond_7

    .line 3807
    const/16 v2, 0xf

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    .line 3808
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3810
    :cond_7
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    if-eqz v2, :cond_8

    .line 3811
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    .line 3812
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3814
    :cond_8
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 3815
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    .line 3816
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3818
    :cond_9
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 3819
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    .line 3820
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3822
    :cond_a
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    if-eqz v2, :cond_b

    .line 3823
    const/16 v2, 0x14

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    .line 3824
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3826
    :cond_b
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    if-eqz v2, :cond_c

    .line 3827
    const/16 v2, 0x15

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    .line 3828
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3830
    :cond_c
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 3831
    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    .line 3832
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3834
    :cond_d
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 3835
    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    .line 3836
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3838
    :cond_e
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_f

    .line 3839
    const/16 v2, 0x18

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    .line 3840
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3842
    :cond_f
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_10

    .line 3843
    const/16 v2, 0x19

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    .line 3844
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3846
    :cond_10
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    if-eqz v2, :cond_11

    .line 3847
    const/16 v2, 0x1a

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    .line 3848
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3850
    :cond_11
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_12

    .line 3851
    const/16 v2, 0x1b

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    .line 3852
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 3854
    :cond_12
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    if-eqz v2, :cond_13

    .line 3855
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    .line 3856
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3858
    :cond_13
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_15

    .line 3859
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_15

    aget-object v0, v3, v2

    .line 3860
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_14

    .line 3861
    const/16 v5, 0x3e7

    .line 3862
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3859
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3866
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_15
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3867
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->cachedSize:I

    .line 3868
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$MethodOptions;
    .locals 11
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 3876
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3877
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3881
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    if-nez v6, :cond_1

    .line 3882
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    .line 3885
    :cond_1
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v6, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3887
    :sswitch_0
    return-object p0

    .line 3892
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3893
    .local v4, "temp":I
    if-eqz v4, :cond_2

    if-ne v4, v8, :cond_3

    .line 3895
    :cond_2
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    goto :goto_0

    .line 3897
    :cond_3
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    goto :goto_0

    .line 3902
    .end local v4    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    goto :goto_0

    .line 3906
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    goto :goto_0

    .line 3910
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    goto :goto_0

    .line 3914
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readSInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    goto :goto_0

    .line 3918
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readSInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    goto :goto_0

    .line 3922
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3923
    .restart local v4    # "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    if-eq v4, v9, :cond_4

    if-ne v4, v10, :cond_5

    .line 3927
    :cond_4
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    goto :goto_0

    .line 3929
    :cond_5
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    goto :goto_0

    .line 3934
    .end local v4    # "temp":I
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3935
    .restart local v4    # "temp":I
    if-eqz v4, :cond_6

    if-ne v4, v8, :cond_7

    .line 3937
    :cond_6
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    goto :goto_0

    .line 3939
    :cond_7
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    goto :goto_0

    .line 3944
    .end local v4    # "temp":I
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3945
    .restart local v4    # "temp":I
    if-eqz v4, :cond_8

    if-ne v4, v8, :cond_9

    .line 3947
    :cond_8
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    goto :goto_0

    .line 3949
    :cond_9
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    goto :goto_0

    .line 3954
    .end local v4    # "temp":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    goto :goto_0

    .line 3958
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    goto/16 :goto_0

    .line 3962
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    goto/16 :goto_0

    .line 3966
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    goto/16 :goto_0

    .line 3970
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    goto/16 :goto_0

    .line 3974
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    goto/16 :goto_0

    .line 3978
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    goto/16 :goto_0

    .line 3982
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    goto/16 :goto_0

    .line 3986
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    goto/16 :goto_0

    .line 3990
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3991
    .restart local v4    # "temp":I
    if-eqz v4, :cond_a

    if-eq v4, v8, :cond_a

    if-eq v4, v9, :cond_a

    if-eq v4, v10, :cond_a

    const/4 v6, 0x4

    if-ne v4, v6, :cond_b

    .line 3996
    :cond_a
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    goto/16 :goto_0

    .line 3998
    :cond_b
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    goto/16 :goto_0

    .line 4003
    .end local v4    # "temp":I
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    goto/16 :goto_0

    .line 4007
    :sswitch_15
    const/16 v6, 0x1f3a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4008
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v6, :cond_d

    move v1, v5

    .line 4009
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4010
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v6, :cond_c

    .line 4011
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4013
    :cond_c
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4014
    :goto_2
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_e

    .line 4015
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 4016
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4017
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4014
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4008
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_d
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v6

    goto :goto_1

    .line 4020
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_e
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 4021
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3877
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x38 -> :sswitch_1
        0x41 -> :sswitch_2
        0x48 -> :sswitch_3
        0x50 -> :sswitch_4
        0x58 -> :sswitch_5
        0x60 -> :sswitch_6
        0x68 -> :sswitch_7
        0x78 -> :sswitch_8
        0x88 -> :sswitch_9
        0x92 -> :sswitch_a
        0x9a -> :sswitch_b
        0xa0 -> :sswitch_c
        0xa8 -> :sswitch_d
        0xb2 -> :sswitch_e
        0xba -> :sswitch_f
        0xc0 -> :sswitch_10
        0xc8 -> :sswitch_11
        0xd0 -> :sswitch_12
        0xd8 -> :sswitch_13
        0x108 -> :sswitch_14
        0x1f3a -> :sswitch_15
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3580
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$MethodOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 10
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/16 v6, 0x100

    .line 3704
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    if-eqz v1, :cond_0

    .line 3705
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->protocol:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3707
    :cond_0
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_1

    .line 3708
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deadline:D

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 3710
    :cond_1
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    if-eqz v1, :cond_2

    .line 3711
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->duplicateSuppression:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3713
    :cond_2
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    if-eqz v1, :cond_3

    .line 3714
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->failFast:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3716
    :cond_3
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    if-eq v1, v6, :cond_4

    .line 3717
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientLogging:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeSInt32(II)V

    .line 3719
    :cond_4
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    if-eq v1, v6, :cond_5

    .line 3720
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverLogging:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeSInt32(II)V

    .line 3722
    :cond_5
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    if-eqz v1, :cond_6

    .line 3723
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLevel:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3725
    :cond_6
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    if-eqz v1, :cond_7

    .line 3726
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->responseFormat:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3728
    :cond_7
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    if-eqz v1, :cond_8

    .line 3729
    const/16 v1, 0x11

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->requestFormat:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3731
    :cond_8
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 3732
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->streamType:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3734
    :cond_9
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 3735
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->securityLabel:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3737
    :cond_a
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    if-eqz v1, :cond_b

    .line 3738
    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->clientStreaming:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3740
    :cond_b
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    if-eqz v1, :cond_c

    .line 3741
    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->serverStreaming:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3743
    :cond_c
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 3744
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyStreamType:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3746
    :cond_d
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 3747
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyResultType:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3749
    :cond_e
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_f

    .line 3750
    const/16 v1, 0x18

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyClientInitialTokens:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3752
    :cond_f
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_10

    .line 3753
    const/16 v1, 0x19

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->legacyServerInitialTokens:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3755
    :cond_10
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    if-eqz v1, :cond_11

    .line 3756
    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->endUserCredsRequested:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3758
    :cond_11
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_12

    .line 3759
    const/16 v1, 0x1b

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->logLevel:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3761
    :cond_12
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    if-eqz v1, :cond_13

    .line 3762
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->deprecated:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3764
    :cond_13
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v1, :cond_15

    .line 3765
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_15

    aget-object v0, v2, v1

    .line 3766
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_14

    .line 3767
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3765
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3771
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_15
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$MethodOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3773
    return-void
.end method
