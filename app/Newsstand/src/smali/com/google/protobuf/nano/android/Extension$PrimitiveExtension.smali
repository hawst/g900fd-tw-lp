.class Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;
.super Lcom/google/protobuf/nano/android/Extension;
.source "Extension.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/nano/android/Extension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PrimitiveExtension"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
        "<TM;>;T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/protobuf/nano/android/Extension",
        "<TM;TT;>;"
    }
.end annotation


# instance fields
.field private final nonPackedTag:I

.field private final packedTag:I


# direct methods
.method public constructor <init>(ILjava/lang/Class;IZII)V
    .locals 6
    .param p1, "type"    # I
    .param p3, "tag"    # I
    .param p4, "repeated"    # Z
    .param p5, "nonPackedTag"    # I
    .param p6, "packedTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<TT;>;IZII)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/protobuf/nano/android/Extension;-><init>(ILjava/lang/Class;IZLcom/google/protobuf/nano/android/Extension$1;)V

    .line 337
    iput p5, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->nonPackedTag:I

    .line 338
    iput p6, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->packedTag:I

    .line 339
    return-void
.end method


# virtual methods
.method protected isMatch(I)Z
    .locals 3
    .param p1, "unknownDataTag"    # I

    .prologue
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 343
    iget-boolean v2, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->repeated:Z

    if-eqz v2, :cond_2

    .line 344
    iget v2, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->nonPackedTag:I

    if-eq p1, v2, :cond_0

    iget v2, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->packedTag:I

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 346
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v2, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    if-ne p1, v2, :cond_3

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method protected readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    .prologue
    .line 353
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    :try_start_0
    iget v1, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    packed-switch v1, :pswitch_data_0

    .line 387
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    iget v2, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Error reading extension field"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 355
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    :try_start_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 385
    :goto_0
    return-object v1

    .line 357
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFloat()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    goto :goto_0

    .line 359
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 361
    :pswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 363
    :pswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 365
    :pswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 367
    :pswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readFixed32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 369
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 371
    :pswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 373
    :pswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    goto :goto_0

    .line 375
    :pswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readUInt32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 377
    :pswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readEnum()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 379
    :pswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readSFixed32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 381
    :pswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readSFixed64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    .line 383
    :pswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readSInt32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 385
    :pswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readSInt64()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    .line 353
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method protected readDataInto(Lcom/google/protobuf/nano/android/UnknownFieldData;Ljava/util/List;)V
    .locals 4
    .param p1, "data"    # Lcom/google/protobuf/nano/android/UnknownFieldData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    .local p2, "resultList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget v2, p1, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    iget v3, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->nonPackedTag:I

    if-ne v2, v3, :cond_1

    .line 399
    iget-object v2, p1, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 411
    :cond_0
    return-void

    .line 401
    :cond_1
    iget-object v2, p1, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    move-result-object v0

    .line 403
    .local v0, "buffer":Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->pushLimit(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    :goto_0
    invoke-virtual {v0}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->isAtEnd()Z

    move-result v2

    if-nez v2, :cond_0

    .line 408
    invoke-virtual {p0, v0}, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 404
    :catch_0
    move-exception v1

    .line 405
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Error reading extension field"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected final writeData(Ljava/lang/Object;)Lcom/google/protobuf/nano/android/UnknownFieldData;
    .locals 26
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 417
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    .line 515
    :pswitch_0
    new-instance v22, Ljava/lang/IllegalArgumentException;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    move/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    const/16 v25, 0x18

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v25, "Unknown type "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    .end local p1    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v8

    .line 519
    .local v8, "e":Ljava/io/IOException;
    new-instance v22, Ljava/lang/IllegalStateException;

    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v22

    .line 419
    .end local v8    # "e":Ljava/io/IOException;
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_1
    :try_start_1
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Double;

    move-object v7, v0

    .line 420
    .local v7, "doubleValue":Ljava/lang/Double;
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeDoubleSizeNoTag(D)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 422
    .local v6, "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeDoubleNoTag(D)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 521
    .end local v7    # "doubleValue":Ljava/lang/Double;
    .end local p1    # "value":Ljava/lang/Object;
    :goto_0
    new-instance v22, Lcom/google/protobuf/nano/android/UnknownFieldData;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    move/from16 v23, v0

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v0, v1, v6}, Lcom/google/protobuf/nano/android/UnknownFieldData;-><init>(I[B)V

    return-object v22

    .line 425
    .end local v6    # "data":[B
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_2
    :try_start_2
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Float;

    move-object v12, v0

    .line 426
    .local v12, "floatValue":Ljava/lang/Float;
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeFloatSizeNoTag(F)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 428
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFloatNoTag(F)V

    goto :goto_0

    .line 431
    .end local v6    # "data":[B
    .end local v12    # "floatValue":Ljava/lang/Float;
    :pswitch_3
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object v14, v0

    .line 432
    .local v14, "int64Value":Ljava/lang/Long;
    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 434
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64NoTag(J)V

    goto :goto_0

    .line 437
    .end local v6    # "data":[B
    .end local v14    # "int64Value":Ljava/lang/Long;
    :pswitch_4
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object/from16 v21, v0

    .line 438
    .local v21, "uint64Value":Ljava/lang/Long;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt64SizeNoTag(J)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 440
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt64NoTag(J)V

    goto :goto_0

    .line 443
    .end local v6    # "data":[B
    .end local v21    # "uint64Value":Ljava/lang/Long;
    :pswitch_5
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object v13, v0

    .line 444
    .local v13, "int32Value":Ljava/lang/Integer;
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 446
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32NoTag(I)V

    goto/16 :goto_0

    .line 449
    .end local v6    # "data":[B
    .end local v13    # "int32Value":Ljava/lang/Integer;
    :pswitch_6
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object v11, v0

    .line 450
    .local v11, "fixed64Value":Ljava/lang/Long;
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeFixed64SizeNoTag(J)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 452
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed64NoTag(J)V

    goto/16 :goto_0

    .line 455
    .end local v6    # "data":[B
    .end local v11    # "fixed64Value":Ljava/lang/Long;
    :pswitch_7
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object v10, v0

    .line 456
    .local v10, "fixed32Value":Ljava/lang/Integer;
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeFixed32SizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 458
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed32NoTag(I)V

    goto/16 :goto_0

    .line 461
    .end local v6    # "data":[B
    .end local v10    # "fixed32Value":Ljava/lang/Integer;
    :pswitch_8
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Boolean;

    move-object v4, v0

    .line 462
    .local v4, "boolValue":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBoolSizeNoTag(Z)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 463
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBoolNoTag(Z)V

    goto/16 :goto_0

    .line 466
    .end local v4    # "boolValue":Ljava/lang/Boolean;
    .end local v6    # "data":[B
    :pswitch_9
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/String;

    move-object/from16 v19, v0

    .line 467
    .local v19, "stringValue":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 469
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeStringNoTag(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 472
    .end local v6    # "data":[B
    .end local v19    # "stringValue":Ljava/lang/String;
    :pswitch_a
    check-cast p1, [B

    .end local p1    # "value":Ljava/lang/Object;
    move-object/from16 v0, p1

    check-cast v0, [B

    move-object v5, v0

    .line 473
    .local v5, "bytesValue":[B
    invoke-static {v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeBytesSizeNoTag([B)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 475
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBytesNoTag([B)V

    goto/16 :goto_0

    .line 478
    .end local v5    # "bytesValue":[B
    .end local v6    # "data":[B
    .restart local p1    # "value":Ljava/lang/Object;
    :pswitch_b
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object/from16 v20, v0

    .line 479
    .local v20, "uint32Value":Ljava/lang/Integer;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt32SizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 481
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt32NoTag(I)V

    goto/16 :goto_0

    .line 484
    .end local v6    # "data":[B
    .end local v20    # "uint32Value":Ljava/lang/Integer;
    :pswitch_c
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object v9, v0

    .line 485
    .local v9, "enumValue":Ljava/lang/Integer;
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeEnumSizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 486
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeEnumNoTag(I)V

    goto/16 :goto_0

    .line 489
    .end local v6    # "data":[B
    .end local v9    # "enumValue":Ljava/lang/Integer;
    :pswitch_d
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object v15, v0

    .line 490
    .local v15, "sfixed32Value":Ljava/lang/Integer;
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSFixed32SizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 492
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSFixed32NoTag(I)V

    goto/16 :goto_0

    .line 496
    .end local v6    # "data":[B
    .end local v15    # "sfixed32Value":Ljava/lang/Integer;
    :pswitch_e
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object/from16 v16, v0

    .line 497
    .local v16, "sfixed64Value":Ljava/lang/Long;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSFixed64SizeNoTag(J)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 499
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSFixed64NoTag(J)V

    goto/16 :goto_0

    .line 503
    .end local v6    # "data":[B
    .end local v16    # "sfixed64Value":Ljava/lang/Long;
    :pswitch_f
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object/from16 v17, v0

    .line 504
    .local v17, "sint32Value":Ljava/lang/Integer;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-static/range {v22 .. v22}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSInt32SizeNoTag(I)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 506
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSInt32NoTag(I)V

    goto/16 :goto_0

    .line 509
    .end local v6    # "data":[B
    .end local v17    # "sint32Value":Ljava/lang/Integer;
    :pswitch_10
    move-object/from16 v0, p1

    check-cast v0, Ljava/lang/Long;

    move-object/from16 v18, v0

    .line 510
    .local v18, "sint64Value":Ljava/lang/Long;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSInt64SizeNoTag(J)I

    move-result v22

    move/from16 v0, v22

    new-array v6, v0, [B

    .line 512
    .restart local v6    # "data":[B
    invoke-static {v6}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v22

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSInt64NoTag(J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 417
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method protected writeDataInto(Ljava/lang/Object;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 526
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;, "Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension<TM;TT;>;"
    .local p1, "array":Ljava/lang/Object;, "TT;"
    .local p2, "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->nonPackedTag:I

    if-ne v7, v8, :cond_0

    .line 528
    invoke-super {p0, p1, p2}, Lcom/google/protobuf/nano/android/Extension;->writeDataInto(Ljava/lang/Object;Ljava/util/List;)V

    .line 686
    :goto_0
    return-void

    .line 529
    :cond_0
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->packedTag:I

    if-ne v7, v8, :cond_3

    .line 532
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    .line 533
    .local v0, "arrayLength":I
    const/4 v2, 0x0

    .line 534
    .local v2, "dataSize":I
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    packed-switch v7, :pswitch_data_0

    .line 592
    :pswitch_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0x28

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Unexpected non-packable type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 537
    :pswitch_1
    move v2, v0

    .line 596
    :cond_1
    :goto_1
    invoke-static {v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeRawVarint32Size(I)I

    move-result v7

    add-int v6, v2, v7

    .line 598
    .local v6, "payloadSize":I
    new-array v1, v6, [B

    .line 599
    .local v1, "data":[B
    invoke-static {v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v5

    .line 601
    .local v5, "output":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    :try_start_0
    invoke-virtual {v5, v2}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 602
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    packed-switch v7, :pswitch_data_1

    .line 674
    :pswitch_2
    new-instance v7, Ljava/lang/IllegalArgumentException;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->type:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0x1b

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Unpackable type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    :catch_0
    move-exception v3

    .line 678
    .local v3, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 542
    .end local v1    # "data":[B
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "output":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .end local v6    # "payloadSize":I
    :pswitch_3
    mul-int/lit8 v2, v0, 0x4

    .line 543
    goto :goto_1

    .line 547
    :pswitch_4
    mul-int/lit8 v2, v0, 0x8

    .line 548
    goto :goto_1

    .line 550
    :pswitch_5
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v0, :cond_1

    .line 551
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-static {v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v2, v7

    .line 550
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 556
    .end local v4    # "i":I
    :pswitch_6
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    if-ge v4, v0, :cond_1

    .line 557
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-static {v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v2, v7

    .line 556
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 562
    .end local v4    # "i":I
    :pswitch_7
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_4
    if-ge v4, v0, :cond_1

    .line 563
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-static {v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt32SizeNoTag(I)I

    move-result v7

    add-int/2addr v2, v7

    .line 562
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 568
    .end local v4    # "i":I
    :pswitch_8
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_5
    if-ge v4, v0, :cond_1

    .line 569
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v7

    add-int/2addr v2, v7

    .line 568
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 574
    .end local v4    # "i":I
    :pswitch_9
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_6
    if-ge v4, v0, :cond_1

    .line 575
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeSInt64SizeNoTag(J)I

    move-result v7

    add-int/2addr v2, v7

    .line 574
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 580
    .end local v4    # "i":I
    :pswitch_a
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_7
    if-ge v4, v0, :cond_1

    .line 581
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeUInt64SizeNoTag(J)I

    move-result v7

    add-int/2addr v2, v7

    .line 580
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 586
    .end local v4    # "i":I
    :pswitch_b
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_8
    if-ge v4, v0, :cond_1

    .line 587
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-static {v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeEnumSizeNoTag(I)I

    move-result v7

    add-int/2addr v2, v7

    .line 586
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 604
    .end local v4    # "i":I
    .restart local v1    # "data":[B
    .restart local v5    # "output":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .restart local v6    # "payloadSize":I
    :pswitch_c
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_9
    if-ge v4, v0, :cond_2

    .line 605
    :try_start_1
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getBoolean(Ljava/lang/Object;I)Z

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeBoolNoTag(Z)V

    .line 604
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 609
    .end local v4    # "i":I
    :pswitch_d
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_a
    if-ge v4, v0, :cond_2

    .line 610
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed32NoTag(I)V

    .line 609
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 614
    .end local v4    # "i":I
    :pswitch_e
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_b
    if-ge v4, v0, :cond_2

    .line 615
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSFixed32NoTag(I)V

    .line 614
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 619
    .end local v4    # "i":I
    :pswitch_f
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_c
    if-ge v4, v0, :cond_2

    .line 620
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getFloat(Ljava/lang/Object;I)F

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFloatNoTag(F)V

    .line 619
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 624
    .end local v4    # "i":I
    :pswitch_10
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_d
    if-ge v4, v0, :cond_2

    .line 625
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeFixed64NoTag(J)V

    .line 624
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 629
    .end local v4    # "i":I
    :pswitch_11
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_e
    if-ge v4, v0, :cond_2

    .line 630
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSFixed64NoTag(J)V

    .line 629
    add-int/lit8 v4, v4, 0x1

    goto :goto_e

    .line 634
    .end local v4    # "i":I
    :pswitch_12
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_f
    if-ge v4, v0, :cond_2

    .line 635
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getDouble(Ljava/lang/Object;I)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeDoubleNoTag(D)V

    .line 634
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 639
    .end local v4    # "i":I
    :pswitch_13
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_10
    if-ge v4, v0, :cond_2

    .line 640
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt32NoTag(I)V

    .line 639
    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    .line 644
    .end local v4    # "i":I
    :pswitch_14
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_11
    if-ge v4, v0, :cond_2

    .line 645
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSInt32NoTag(I)V

    .line 644
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    .line 649
    .end local v4    # "i":I
    :pswitch_15
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_12
    if-ge v4, v0, :cond_2

    .line 650
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt32NoTag(I)V

    .line 649
    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    .line 654
    .end local v4    # "i":I
    :pswitch_16
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_13
    if-ge v4, v0, :cond_2

    .line 655
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeInt64NoTag(J)V

    .line 654
    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    .line 659
    .end local v4    # "i":I
    :pswitch_17
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_14
    if-ge v4, v0, :cond_2

    .line 660
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeSInt64NoTag(J)V

    .line 659
    add-int/lit8 v4, v4, 0x1

    goto :goto_14

    .line 664
    .end local v4    # "i":I
    :pswitch_18
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_15
    if-ge v4, v0, :cond_2

    .line 665
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getLong(Ljava/lang/Object;I)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeUInt64NoTag(J)V

    .line 664
    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    .line 669
    .end local v4    # "i":I
    :pswitch_19
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_16
    if-ge v4, v0, :cond_2

    .line 670
    invoke-static {p1, v4}, Ljava/lang/reflect/Array;->getInt(Ljava/lang/Object;I)I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeEnumNoTag(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 669
    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    .line 680
    :cond_2
    new-instance v7, Lcom/google/protobuf/nano/android/UnknownFieldData;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    invoke-direct {v7, v8, v1}, Lcom/google/protobuf/nano/android/UnknownFieldData;-><init>(I[B)V

    invoke-interface {p2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 682
    .end local v0    # "arrayLength":I
    .end local v1    # "data":[B
    .end local v2    # "dataSize":I
    .end local v4    # "i":I
    .end local v5    # "output":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .end local v6    # "payloadSize":I
    :cond_3
    new-instance v7, Ljava/lang/IllegalArgumentException;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->tag:I

    iget v9, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->nonPackedTag:I

    iget v10, p0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;->packedTag:I

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0x7c

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Unexpected repeated extension tag "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, ", unequal to both non-packed variant "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " and packed variant "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 534
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_a
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_b
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_9
    .end packed-switch

    .line 602
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_12
        :pswitch_f
        :pswitch_16
        :pswitch_18
        :pswitch_13
        :pswitch_10
        :pswitch_d
        :pswitch_c
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_15
        :pswitch_19
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
    .end packed-switch
.end method
