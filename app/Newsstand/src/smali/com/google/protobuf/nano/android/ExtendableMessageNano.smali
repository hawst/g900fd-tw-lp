.class public abstract Lcom/google/protobuf/nano/android/ExtendableMessageNano;
.super Lcom/google/protobuf/nano/android/MessageNano;
.source "ExtendableMessageNano.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
        "<TM;>;>",
        "Lcom/google/protobuf/nano/android/MessageNano;"
    }
.end annotation


# instance fields
.field protected unknownFieldData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    invoke-direct {p0}, Lcom/google/protobuf/nano/android/MessageNano;-><init>()V

    return-void
.end method


# virtual methods
.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 51
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    const/4 v1, 0x0

    .line 52
    .local v1, "size":I
    iget-object v4, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    if-nez v4, :cond_0

    const/4 v3, 0x0

    .line 53
    .local v3, "unknownFieldCount":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_1

    .line 54
    iget-object v4, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/nano/android/UnknownFieldData;

    .line 55
    .local v2, "unknownField":Lcom/google/protobuf/nano/android/UnknownFieldData;
    iget v4, v2, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    invoke-static {v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeRawVarint32Size(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 56
    iget-object v4, v2, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    array-length v4, v4

    add-int/2addr v1, v4

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 52
    .end local v0    # "i":I
    .end local v2    # "unknownField":Lcom/google/protobuf/nano/android/UnknownFieldData;
    .end local v3    # "unknownFieldCount":I
    :cond_0
    iget-object v4, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_0

    .line 58
    .restart local v0    # "i":I
    .restart local v3    # "unknownFieldCount":I
    :cond_1
    return v1
.end method

.method public final getExtension(Lcom/google/protobuf/nano/android/Extension;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    .local p1, "extension":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    iget-object v0, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/android/Extension;->getValueFrom(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final setExtension(Lcom/google/protobuf/nano/android/Extension;Ljava/lang/Object;)Lcom/google/protobuf/nano/android/ExtendableMessageNano;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;TT;>;TT;)TM;"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    .local p1, "extension":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p2, "value":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-virtual {p1, p2, v1}, Lcom/google/protobuf/nano/android/Extension;->setValueTo(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    .line 85
    move-object v0, p0

    .line 86
    .local v0, "typedThis":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "TM;"
    return-object v0
.end method

.method protected final storeUnknownField(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;I)Z
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;
    .param p2, "tag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v2

    .line 106
    .local v2, "startPos":I
    invoke-virtual {p1, p2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 107
    const/4 v3, 0x0

    .line 115
    :goto_0
    return v3

    .line 109
    :cond_0
    iget-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    if-nez v3, :cond_1

    .line 110
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    .line 112
    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getPosition()I

    move-result v1

    .line 113
    .local v1, "endPos":I
    sub-int v3, v1, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->getData(II)[B

    move-result-object v0

    .line 114
    .local v0, "bytes":[B
    iget-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    new-instance v4, Lcom/google/protobuf/nano/android/UnknownFieldData;

    invoke-direct {v4, p2, v0}, Lcom/google/protobuf/nano/android/UnknownFieldData;-><init>(I[B)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public writeTo(Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/google/protobuf/nano/android/ExtendableMessageNano;, "Lcom/google/protobuf/nano/android/ExtendableMessageNano<TM;>;"
    iget-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 64
    .local v2, "unknownFieldCount":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 65
    iget-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/nano/android/UnknownFieldData;

    .line 66
    .local v1, "unknownField":Lcom/google/protobuf/nano/android/UnknownFieldData;
    iget v3, v1, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 67
    iget-object v3, v1, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeRawBytes([B)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63
    .end local v0    # "i":I
    .end local v1    # "unknownField":Lcom/google/protobuf/nano/android/UnknownFieldData;
    .end local v2    # "unknownFieldCount":I
    :cond_0
    iget-object v3, p0, Lcom/google/protobuf/nano/android/ExtendableMessageNano;->unknownFieldData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0

    .line 69
    .restart local v0    # "i":I
    .restart local v2    # "unknownFieldCount":I
    :cond_1
    return-void
.end method
