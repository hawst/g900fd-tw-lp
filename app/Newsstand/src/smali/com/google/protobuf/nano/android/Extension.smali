.class public Lcom/google/protobuf/nano/android/Extension;
.super Ljava/lang/Object;
.source "Extension.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/nano/android/Extension$1;,
        Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
        "<TM;>;T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final TYPE_BOOL:I = 0x8

.field public static final TYPE_BYTES:I = 0xc

.field public static final TYPE_DOUBLE:I = 0x1

.field public static final TYPE_ENUM:I = 0xe

.field public static final TYPE_FIXED32:I = 0x7

.field public static final TYPE_FIXED64:I = 0x6

.field public static final TYPE_FLOAT:I = 0x2

.field public static final TYPE_GROUP:I = 0xa

.field public static final TYPE_INT32:I = 0x5

.field public static final TYPE_INT64:I = 0x3

.field public static final TYPE_MESSAGE:I = 0xb

.field public static final TYPE_SFIXED32:I = 0xf

.field public static final TYPE_SFIXED64:I = 0x10

.field public static final TYPE_SINT32:I = 0x11

.field public static final TYPE_SINT64:I = 0x12

.field public static final TYPE_STRING:I = 0x9

.field public static final TYPE_UINT32:I = 0xd

.field public static final TYPE_UINT64:I = 0x4


# instance fields
.field protected final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected final repeated:Z

.field protected final tag:I

.field protected final type:I


# direct methods
.method private constructor <init>(ILjava/lang/Class;IZ)V
    .locals 0
    .param p1, "type"    # I
    .param p3, "tag"    # I
    .param p4, "repeated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<TT;>;IZ)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput p1, p0, Lcom/google/protobuf/nano/android/Extension;->type:I

    .line 150
    iput-object p2, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    .line 151
    iput p3, p0, Lcom/google/protobuf/nano/android/Extension;->tag:I

    .line 152
    iput-boolean p4, p0, Lcom/google/protobuf/nano/android/Extension;->repeated:Z

    .line 153
    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/Class;IZLcom/google/protobuf/nano/android/Extension$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/lang/Class;
    .param p3, "x2"    # I
    .param p4, "x3"    # Z
    .param p5, "x4"    # Lcom/google/protobuf/nano/android/Extension$1;

    .prologue
    .line 46
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/nano/android/Extension;-><init>(ILjava/lang/Class;IZ)V

    return-void
.end method

.method public static createMessageTyped(ILjava/lang/Class;I)Lcom/google/protobuf/nano/android/Extension;
    .locals 2
    .param p0, "type"    # I
    .param p2, "tag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
            "<TM;>;T:",
            "Lcom/google/protobuf/nano/android/MessageNano;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;I)",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lcom/google/protobuf/nano/android/Extension;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/protobuf/nano/android/Extension;-><init>(ILjava/lang/Class;IZ)V

    return-object v0
.end method

.method public static createPrimitiveTyped(ILjava/lang/Class;I)Lcom/google/protobuf/nano/android/Extension;
    .locals 7
    .param p0, "type"    # I
    .param p2, "tag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
            "<TM;>;T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;I)",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;TT;>;"
        }
    .end annotation

    .prologue
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v4, 0x0

    .line 108
    new-instance v0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;

    move v1, p0

    move-object v2, p1

    move v3, p2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;-><init>(ILjava/lang/Class;IZII)V

    return-object v0
.end method

.method public static createRepeatedMessageTyped(ILjava/lang/Class;I)Lcom/google/protobuf/nano/android/Extension;
    .locals 2
    .param p0, "type"    # I
    .param p2, "tag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
            "<TM;>;T:",
            "Lcom/google/protobuf/nano/android/MessageNano;",
            ">(I",
            "Ljava/lang/Class",
            "<[TT;>;I)",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;[TT;>;"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<[TT;>;"
    new-instance v0, Lcom/google/protobuf/nano/android/Extension;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/protobuf/nano/android/Extension;-><init>(ILjava/lang/Class;IZ)V

    return-object v0
.end method

.method public static createRepeatedPrimitiveTyped(ILjava/lang/Class;III)Lcom/google/protobuf/nano/android/Extension;
    .locals 7
    .param p0, "type"    # I
    .param p2, "tag"    # I
    .param p3, "nonPackedTag"    # I
    .param p4, "packedTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/google/protobuf/nano/android/ExtendableMessageNano",
            "<TM;>;T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;III)",
            "Lcom/google/protobuf/nano/android/Extension",
            "<TM;TT;>;"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;

    const/4 v4, 0x1

    move v1, p0

    move-object v2, p1

    move v3, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/protobuf/nano/android/Extension$PrimitiveExtension;-><init>(ILjava/lang/Class;IZII)V

    return-object v0
.end method


# virtual methods
.method final getValueFrom(Ljava/util/List;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p1, "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    const/4 v3, 0x0

    .line 165
    if-nez p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-object v3

    .line 169
    :cond_1
    iget-boolean v6, p0, Lcom/google/protobuf/nano/android/Extension;->repeated:Z

    if-eqz v6, :cond_4

    .line 171
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 173
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/android/UnknownFieldData;

    .line 174
    .local v0, "data":Lcom/google/protobuf/nano/android/UnknownFieldData;
    iget v6, v0, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    invoke-virtual {p0, v6}, Lcom/google/protobuf/nano/android/Extension;->isMatch(I)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    array-length v6, v6

    if-eqz v6, :cond_2

    .line 175
    invoke-virtual {p0, v0, v4}, Lcom/google/protobuf/nano/android/Extension;->readDataInto(Lcom/google/protobuf/nano/android/UnknownFieldData;Ljava/util/List;)V

    .line 172
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 179
    .end local v0    # "data":Lcom/google/protobuf/nano/android/UnknownFieldData;
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 180
    .local v5, "resultSize":I
    if-eqz v5, :cond_0

    .line 184
    iget-object v6, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    invoke-virtual {v7}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 185
    .local v3, "result":Ljava/lang/Object;, "TT;"
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_0

    .line 186
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v3, v1, v6}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 185
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 191
    .end local v1    # "i":I
    .end local v3    # "result":Ljava/lang/Object;, "TT;"
    .end local v4    # "resultList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v5    # "resultSize":I
    :cond_4
    const/4 v2, 0x0

    .line 192
    .local v2, "lastData":Lcom/google/protobuf/nano/android/UnknownFieldData;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .restart local v1    # "i":I
    :goto_3
    if-nez v2, :cond_6

    if-ltz v1, :cond_6

    .line 193
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/android/UnknownFieldData;

    .line 194
    .restart local v0    # "data":Lcom/google/protobuf/nano/android/UnknownFieldData;
    iget v6, v0, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    invoke-virtual {p0, v6}, Lcom/google/protobuf/nano/android/Extension;->isMatch(I)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    array-length v6, v6

    if-eqz v6, :cond_5

    .line 195
    move-object v2, v0

    .line 192
    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 199
    .end local v0    # "data":Lcom/google/protobuf/nano/android/UnknownFieldData;
    :cond_6
    if-eqz v2, :cond_0

    .line 203
    iget-object v6, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    iget-object v7, v2, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    invoke-static {v7}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/protobuf/nano/android/Extension;->readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    goto/16 :goto_0
.end method

.method protected isMatch(I)Z
    .locals 1
    .param p1, "unknownDataTag"    # I

    .prologue
    .line 157
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    iget v0, p0, Lcom/google/protobuf/nano/android/Extension;->tag:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    .prologue
    .line 209
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    iget-boolean v4, p0, Lcom/google/protobuf/nano/android/Extension;->repeated:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v3

    .line 211
    .local v3, "messageType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    :try_start_0
    iget v4, p0, Lcom/google/protobuf/nano/android/Extension;->type:I

    packed-switch v4, :pswitch_data_0

    .line 221
    new-instance v4, Ljava/lang/IllegalArgumentException;

    iget v5, p0, Lcom/google/protobuf/nano/android/Extension;->type:I

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x18

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Unknown type "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x21

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Error creating instance of class "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 209
    .end local v0    # "e":Ljava/lang/InstantiationException;
    .end local v3    # "messageType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    iget-object v3, p0, Lcom/google/protobuf/nano/android/Extension;->clazz:Ljava/lang/Class;

    goto :goto_0

    .line 213
    .restart local v3    # "messageType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :pswitch_0
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/nano/android/MessageNano;

    .line 214
    .local v1, "group":Lcom/google/protobuf/nano/android/MessageNano;
    iget v4, p0, Lcom/google/protobuf/nano/android/Extension;->tag:I

    invoke-static {v4}, Lcom/google/protobuf/nano/android/WireFormatNano;->getTagFieldNumber(I)I

    move-result v4

    invoke-virtual {p1, v1, v4}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/android/MessageNano;I)V

    .line 219
    .end local v1    # "group":Lcom/google/protobuf/nano/android/MessageNano;
    :goto_1
    return-object v1

    .line 217
    :pswitch_1
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/nano/android/MessageNano;

    .line 218
    .local v2, "message":Lcom/google/protobuf/nano/android/MessageNano;
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/android/MessageNano;)V
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v1, v2

    .line 219
    goto :goto_1

    .line 226
    .end local v2    # "message":Lcom/google/protobuf/nano/android/MessageNano;
    :catch_1
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x21

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Error creating instance of class "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 229
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 230
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Error reading extension field"

    invoke-direct {v4, v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected readDataInto(Lcom/google/protobuf/nano/android/UnknownFieldData;Ljava/util/List;)V
    .locals 1
    .param p1, "data"    # Lcom/google/protobuf/nano/android/UnknownFieldData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p2, "resultList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v0, p1, Lcom/google/protobuf/nano/android/UnknownFieldData;->bytes:[B

    invoke-static {v0}, Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/nano/android/Extension;->readData(Lcom/google/protobuf/nano/android/CodedInputByteBufferNano;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    return-void
.end method

.method final setValueTo(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    .local p2, "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    if-eqz p2, :cond_1

    .line 251
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 252
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/nano/android/UnknownFieldData;

    iget v1, v1, Lcom/google/protobuf/nano/android/UnknownFieldData;->tag:I

    invoke-virtual {p0, v1}, Lcom/google/protobuf/nano/android/Extension;->isMatch(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 251
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 258
    .end local v0    # "i":I
    :cond_1
    if-eqz p1, :cond_3

    .line 259
    if-nez p2, :cond_2

    .line 260
    new-instance p2, Ljava/util/ArrayList;

    .end local p2    # "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .restart local p2    # "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    :cond_2
    iget-boolean v1, p0, Lcom/google/protobuf/nano/android/Extension;->repeated:Z

    if-eqz v1, :cond_6

    .line 263
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/nano/android/Extension;->writeDataInto(Ljava/lang/Object;Ljava/util/List;)V

    .line 271
    :cond_3
    :goto_1
    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 p2, 0x0

    .end local p2    # "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    :cond_5
    return-object p2

    .line 265
    .restart local p2    # "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/protobuf/nano/android/Extension;->writeData(Ljava/lang/Object;)Lcom/google/protobuf/nano/android/UnknownFieldData;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected writeData(Ljava/lang/Object;)Lcom/google/protobuf/nano/android/UnknownFieldData;
    .locals 11
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 278
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    :try_start_0
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension;->type:I

    packed-switch v7, :pswitch_data_0

    .line 296
    new-instance v7, Ljava/lang/IllegalArgumentException;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension;->type:I

    new-instance v9, Ljava/lang/StringBuilder;

    const/16 v10, 0x18

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Unknown type "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :catch_0
    move-exception v2

    .line 300
    .local v2, "e":Ljava/io/IOException;
    new-instance v7, Ljava/lang/IllegalStateException;

    invoke-direct {v7, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 280
    .end local v2    # "e":Ljava/io/IOException;
    :pswitch_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/nano/android/MessageNano;

    move-object v4, v0

    .line 281
    .local v4, "groupValue":Lcom/google/protobuf/nano/android/MessageNano;
    iget v7, p0, Lcom/google/protobuf/nano/android/Extension;->tag:I

    invoke-static {v7}, Lcom/google/protobuf/nano/android/WireFormatNano;->getTagFieldNumber(I)I

    move-result v3

    .line 282
    .local v3, "fieldNumber":I
    invoke-static {v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeGroupSizeNoTag(Lcom/google/protobuf/nano/android/MessageNano;)I

    move-result v7

    invoke-static {v3}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeTagSize(I)I

    move-result v8

    add-int/2addr v7, v8

    new-array v1, v7, [B

    .line 284
    .local v1, "data":[B
    invoke-static {v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v6

    .line 285
    .local v6, "out":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    invoke-virtual {v6, v4}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeGroupNoTag(Lcom/google/protobuf/nano/android/MessageNano;)V

    .line 287
    const/4 v7, 0x4

    invoke-virtual {v6, v3, v7}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeTag(II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 302
    .end local v3    # "fieldNumber":I
    .end local v4    # "groupValue":Lcom/google/protobuf/nano/android/MessageNano;
    .end local v6    # "out":Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;
    :goto_0
    new-instance v7, Lcom/google/protobuf/nano/android/UnknownFieldData;

    iget v8, p0, Lcom/google/protobuf/nano/android/Extension;->tag:I

    invoke-direct {v7, v8, v1}, Lcom/google/protobuf/nano/android/UnknownFieldData;-><init>(I[B)V

    return-object v7

    .line 290
    .end local v1    # "data":[B
    :pswitch_1
    :try_start_2
    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/nano/android/MessageNano;

    move-object v5, v0

    .line 291
    .local v5, "messageValue":Lcom/google/protobuf/nano/android/MessageNano;
    invoke-static {v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->computeMessageSizeNoTag(Lcom/google/protobuf/nano/android/MessageNano;)I

    move-result v7

    new-array v1, v7, [B

    .line 293
    .restart local v1    # "data":[B
    invoke-static {v1}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->newInstance([B)Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/protobuf/nano/android/CodedOutputByteBufferNano;->writeMessageNoTag(Lcom/google/protobuf/nano/android/MessageNano;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected writeDataInto(Ljava/lang/Object;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List",
            "<",
            "Lcom/google/protobuf/nano/android/UnknownFieldData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p0, "this":Lcom/google/protobuf/nano/android/Extension;, "Lcom/google/protobuf/nano/android/Extension<TM;TT;>;"
    .local p1, "array":Ljava/lang/Object;, "TT;"
    .local p2, "unknownFields":Ljava/util/List;, "Ljava/util/List<Lcom/google/protobuf/nano/android/UnknownFieldData;>;"
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    .line 308
    .local v0, "arrayLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 309
    invoke-static {p1, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 310
    .local v1, "element":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 311
    invoke-virtual {p0, v1}, Lcom/google/protobuf/nano/android/Extension;->writeData(Ljava/lang/Object;)Lcom/google/protobuf/nano/android/UnknownFieldData;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 314
    .end local v1    # "element":Ljava/lang/Object;
    :cond_1
    return-void
.end method
