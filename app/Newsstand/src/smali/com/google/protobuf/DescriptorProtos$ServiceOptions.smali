.class public final Lcom/google/protobuf/DescriptorProtos$ServiceOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceOptions"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceOptions;


# instance fields
.field public deprecated:Z

.field public failureDetectionDelay:D

.field public multicastStub:Z

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3437
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3438
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 3441
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    .line 3444
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    .line 3447
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    .line 3450
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3438
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3575
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$ServiceOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 3569
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$ServiceOptions;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3453
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    .line 3454
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    .line 3455
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    .line 3456
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3457
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    .line 3458
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->cachedSize:I

    .line 3459
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 3487
    const/4 v1, 0x0

    .line 3488
    .local v1, "size":I
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 3489
    const/16 v2, 0x10

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    .line 3490
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v1, v2

    .line 3492
    :cond_0
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    if-eqz v2, :cond_1

    .line 3493
    const/16 v2, 0x14

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    .line 3494
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3496
    :cond_1
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    if-eqz v2, :cond_2

    .line 3497
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    .line 3498
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 3500
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_4

    .line 3501
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 3502
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_3

    .line 3503
    const/16 v5, 0x3e7

    .line 3504
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3501
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3508
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_4
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3509
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->cachedSize:I

    .line 3510
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceOptions;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3519
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3523
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 3524
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    .line 3527
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3529
    :sswitch_0
    return-object p0

    .line 3534
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    goto :goto_0

    .line 3538
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    goto :goto_0

    .line 3542
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    goto :goto_0

    .line 3546
    :sswitch_4
    const/16 v5, 0x1f3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3547
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v5, :cond_3

    move v1, v4

    .line 3548
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3549
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v5, :cond_2

    .line 3550
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3552
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3553
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 3554
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v6, v5, v1

    .line 3555
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3556
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3553
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3547
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v5

    goto :goto_1

    .line 3559
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v6, v5, v1

    .line 3560
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3519
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x81 -> :sswitch_1
        0xa0 -> :sswitch_2
        0x108 -> :sswitch_3
        0x1f3a -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3434
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$ServiceOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3465
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 3466
    const/16 v1, 0x10

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->failureDetectionDelay:D

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 3468
    :cond_0
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    if-eqz v1, :cond_1

    .line 3469
    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->multicastStub:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3471
    :cond_1
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    if-eqz v1, :cond_2

    .line 3472
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->deprecated:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 3474
    :cond_2
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v1, :cond_4

    .line 3475
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    .line 3476
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_3

    .line 3477
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3475
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3481
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_4
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$ServiceOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3483
    return-void
.end method
