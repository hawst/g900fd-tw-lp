.class public final Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnumDescriptorProto"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;


# instance fields
.field public name:Ljava/lang/String;

.field public options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

.field public value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1249
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1250
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    .line 1256
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    .line 1259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    .line 1250
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1375
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1369
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1262
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    .line 1263
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    .line 1264
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    .line 1265
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1266
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->cachedSize:I

    .line 1267
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 1292
    const/4 v1, 0x0

    .line 1293
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1294
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    .line 1295
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1297
    :cond_0
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    if-eqz v2, :cond_2

    .line 1298
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 1299
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    if-eqz v0, :cond_1

    .line 1300
    const/4 v5, 0x2

    .line 1301
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1298
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1305
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    :cond_2
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    if-eqz v2, :cond_3

    .line 1306
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    .line 1307
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1309
    :cond_3
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1310
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->cachedSize:I

    .line 1311
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1320
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1324
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 1325
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1328
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1330
    :sswitch_0
    return-object p0

    .line 1335
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    goto :goto_0

    .line 1339
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1340
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 1341
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    .line 1342
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    if-eqz v5, :cond_2

    .line 1343
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1345
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    .line 1346
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 1347
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1348
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1349
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1346
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1340
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    array-length v1, v5

    goto :goto_1

    .line 1352
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;-><init>()V

    aput-object v6, v5, v1

    .line 1353
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1357
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    :sswitch_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    if-nez v5, :cond_5

    .line 1358
    new-instance v5, Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    invoke-direct {v5}, Lcom/google/protobuf/DescriptorProtos$EnumOptions;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    .line 1360
    :cond_5
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1320
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1246
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1273
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1274
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1276
    :cond_0
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    if-eqz v1, :cond_2

    .line 1277
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->value:[Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1278
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    if-eqz v0, :cond_1

    .line 1279
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1277
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1283
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$EnumValueDescriptorProto;
    :cond_2
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    if-eqz v1, :cond_3

    .line 1284
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$EnumOptions;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1286
    :cond_3
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$EnumDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1288
    return-void
.end method
