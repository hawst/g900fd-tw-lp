.class public final Lcom/google/protobuf/DescriptorProtos$FieldOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FieldOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;,
        Lcom/google/protobuf/DescriptorProtos$FieldOptions$JSType;,
        Lcom/google/protobuf/DescriptorProtos$FieldOptions$JType;,
        Lcom/google/protobuf/DescriptorProtos$FieldOptions$CType;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldOptions;


# instance fields
.field public ctype:I

.field public deprecated:Z

.field public deprecatedRawMessage:Z

.field public jstype:I

.field public jtype:I

.field public lazy:Z

.field public packed:Z

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

.field public upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

.field public weak:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2784
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2785
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 2900
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    .line 2903
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    .line 2906
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    .line 2909
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    .line 2912
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    .line 2915
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    .line 2918
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    .line 2921
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    .line 2924
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    .line 2927
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 2785
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3167
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 3161
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2930
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    .line 2931
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    .line 2932
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    .line 2933
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    .line 2934
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    .line 2935
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    .line 2936
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    .line 2937
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    .line 2938
    iput-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    .line 2939
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 2940
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    .line 2941
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->cachedSize:I

    .line 2942
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2992
    const/4 v1, 0x0

    .line 2993
    .local v1, "size":I
    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    if-eqz v3, :cond_0

    .line 2994
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    .line 2995
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 2997
    :cond_0
    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    if-eqz v3, :cond_1

    .line 2998
    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    .line 2999
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 3001
    :cond_1
    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    if-eqz v3, :cond_2

    .line 3002
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    .line 3003
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 3005
    :cond_2
    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    if-eqz v3, :cond_3

    .line 3006
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    .line 3007
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 3009
    :cond_3
    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    if-eqz v3, :cond_4

    .line 3010
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    .line 3011
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 3013
    :cond_4
    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    if-eqz v3, :cond_5

    .line 3014
    const/4 v3, 0x6

    iget v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    .line 3015
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    .line 3017
    :cond_5
    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    if-eqz v3, :cond_6

    .line 3018
    const/16 v3, 0xa

    iget-boolean v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    .line 3019
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 3021
    :cond_6
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    if-eqz v3, :cond_8

    .line 3022
    iget-object v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_8

    aget-object v0, v4, v3

    .line 3023
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    if-eqz v0, :cond_7

    .line 3024
    const/16 v6, 0xb

    .line 3025
    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v1, v6

    .line 3022
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3029
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    :cond_8
    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    if-eqz v3, :cond_9

    .line 3030
    const/16 v3, 0xc

    iget-boolean v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    .line 3031
    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 3033
    :cond_9
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v3, :cond_b

    .line 3034
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_b

    aget-object v0, v3, v2

    .line 3035
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_a

    .line 3036
    const/16 v5, 0x3e7

    .line 3037
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3034
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3041
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_b
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3042
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->cachedSize:I

    .line 3043
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FieldOptions;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 3051
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3052
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3056
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    if-nez v6, :cond_1

    .line 3057
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    .line 3060
    :cond_1
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v6, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3062
    :sswitch_0
    return-object p0

    .line 3067
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3068
    .local v4, "temp":I
    if-eqz v4, :cond_2

    if-eq v4, v8, :cond_2

    if-ne v4, v9, :cond_3

    .line 3071
    :cond_2
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    goto :goto_0

    .line 3073
    :cond_3
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    goto :goto_0

    .line 3078
    .end local v4    # "temp":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    goto :goto_0

    .line 3082
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    goto :goto_0

    .line 3086
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3087
    .restart local v4    # "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    if-ne v4, v9, :cond_5

    .line 3090
    :cond_4
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    goto :goto_0

    .line 3092
    :cond_5
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    goto :goto_0

    .line 3097
    .end local v4    # "temp":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    goto :goto_0

    .line 3101
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3102
    .restart local v4    # "temp":I
    if-eqz v4, :cond_6

    if-eq v4, v8, :cond_6

    if-ne v4, v9, :cond_7

    .line 3105
    :cond_6
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    goto :goto_0

    .line 3107
    :cond_7
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    goto :goto_0

    .line 3112
    .end local v4    # "temp":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    goto :goto_0

    .line 3116
    :sswitch_8
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3117
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    if-nez v6, :cond_9

    move v1, v5

    .line 3118
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    .line 3119
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    if-eqz v6, :cond_8

    .line 3120
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3122
    :cond_8
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    .line 3123
    :goto_2
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 3124
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;-><init>()V

    aput-object v7, v6, v1

    .line 3125
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3126
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3123
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3117
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    :cond_9
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    array-length v1, v6

    goto :goto_1

    .line 3129
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    :cond_a
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;-><init>()V

    aput-object v7, v6, v1

    .line 3130
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3134
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    goto/16 :goto_0

    .line 3138
    :sswitch_a
    const/16 v6, 0x1f3a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3139
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v6, :cond_c

    move v1, v5

    .line 3140
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3141
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v6, :cond_b

    .line 3142
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3144
    :cond_b
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 3145
    :goto_4
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_d

    .line 3146
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 3147
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3148
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3145
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3139
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_c
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v6

    goto :goto_3

    .line 3151
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_d
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 3152
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 3052
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x50 -> :sswitch_7
        0x5a -> :sswitch_8
        0x60 -> :sswitch_9
        0x1f3a -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2781
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$FieldOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2948
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    if-eqz v2, :cond_0

    .line 2949
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->ctype:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2951
    :cond_0
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    if-eqz v2, :cond_1

    .line 2952
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->packed:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2954
    :cond_1
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    if-eqz v2, :cond_2

    .line 2955
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecated:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2957
    :cond_2
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    if-eqz v2, :cond_3

    .line 2958
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jtype:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2960
    :cond_3
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    if-eqz v2, :cond_4

    .line 2961
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->lazy:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2963
    :cond_4
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    if-eqz v2, :cond_5

    .line 2964
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->jstype:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2966
    :cond_5
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    if-eqz v2, :cond_6

    .line 2967
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->weak:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2969
    :cond_6
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    if-eqz v2, :cond_8

    .line 2970
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->upgradedOption:[Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_8

    aget-object v0, v3, v2

    .line 2971
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    if-eqz v0, :cond_7

    .line 2972
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2970
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2976
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$FieldOptions$UpgradedOption;
    :cond_8
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    if-eqz v2, :cond_9

    .line 2977
    const/16 v2, 0xc

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->deprecatedRawMessage:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2979
    :cond_9
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_b

    .line 2980
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v0, v2, v1

    .line 2981
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_a

    .line 2982
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2980
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2986
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_b
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$FieldOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2988
    return-void
.end method
