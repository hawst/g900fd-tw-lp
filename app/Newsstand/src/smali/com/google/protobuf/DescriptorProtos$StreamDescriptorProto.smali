.class public final Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StreamDescriptorProto"
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;


# instance fields
.field public clientMessageType:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

.field public serverMessageType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1793
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1794
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 1797
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    .line 1800
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    .line 1803
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    .line 1806
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    .line 1794
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1912
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1906
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1809
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    .line 1810
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    .line 1811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    .line 1812
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    .line 1813
    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1814
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->cachedSize:I

    .line 1815
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    .line 1839
    const/4 v0, 0x0

    .line 1840
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1841
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    .line 1842
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1844
    :cond_0
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1845
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    .line 1846
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1848
    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1849
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    .line 1850
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1852
    :cond_2
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    if-eqz v1, :cond_3

    .line 1853
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    .line 1854
    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1856
    :cond_3
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1857
    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->cachedSize:I

    .line 1858
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1866
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1867
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1871
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1872
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    .line 1875
    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1877
    :sswitch_0
    return-object p0

    .line 1882
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    goto :goto_0

    .line 1886
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    goto :goto_0

    .line 1890
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    goto :goto_0

    .line 1894
    :sswitch_4
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    if-nez v1, :cond_2

    .line 1895
    new-instance v1, Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    invoke-direct {v1}, Lcom/google/protobuf/DescriptorProtos$StreamOptions;-><init>()V

    iput-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    .line 1897
    :cond_2
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1867
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1790
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1821
    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1822
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1824
    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1825
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->clientMessageType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1827
    :cond_1
    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1828
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->serverMessageType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1830
    :cond_2
    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    if-eqz v0, :cond_3

    .line 1831
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->options:Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1833
    :cond_3
    iget-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamDescriptorProto;->unknownFieldData:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1835
    return-void
.end method
