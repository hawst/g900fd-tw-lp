.class public final Lcom/google/protobuf/DescriptorProtos$StreamOptions;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StreamOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$StreamOptions$TokenUnit;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamOptions;


# instance fields
.field public clientInitialTokens:J

.field public clientLogging:I

.field public deadline:D

.field public deprecated:Z

.field public endUserCredsRequested:Z

.field public failFast:Z

.field public logLevel:I

.field public securityLabel:Ljava/lang/String;

.field public securityLevel:I

.field public serverInitialTokens:J

.field public serverLogging:I

.field public tokenUnit:I

.field public uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4044
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/16 v1, 0x100

    const/4 v2, 0x0

    .line 4045
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 4053
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    .line 4056
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    .line 4059
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    .line 4062
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    .line 4065
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    .line 4068
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    .line 4071
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    .line 4074
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    .line 4077
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    .line 4080
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    .line 4083
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    .line 4086
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    .line 4089
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4045
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamOptions;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4345
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$StreamOptions;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$StreamOptions;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 4339
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$StreamOptions;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$StreamOptions;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/16 v1, 0x100

    const/4 v2, 0x0

    .line 4092
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    .line 4093
    iput-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    .line 4094
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    .line 4095
    iput v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    .line 4096
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    .line 4097
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    .line 4098
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    .line 4099
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    .line 4100
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    .line 4101
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    .line 4102
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    .line 4103
    iput-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    .line 4104
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    .line 4106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->cachedSize:I

    .line 4107
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/16 v7, 0x100

    const/4 v6, 0x2

    .line 4162
    const/4 v1, 0x0

    .line 4163
    .local v1, "size":I
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    .line 4164
    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    .line 4165
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4167
    :cond_0
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_1

    .line 4168
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    .line 4169
    invoke-static {v6, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4171
    :cond_1
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    if-eqz v2, :cond_2

    .line 4172
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    .line 4173
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 4175
    :cond_2
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    if-eqz v2, :cond_3

    .line 4176
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    .line 4177
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 4179
    :cond_3
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 4180
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    .line 4181
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4183
    :cond_4
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    if-eq v2, v7, :cond_5

    .line 4184
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    .line 4185
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 4187
    :cond_5
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    if-eq v2, v7, :cond_6

    .line 4188
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    .line 4189
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 4191
    :cond_6
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_7

    .line 4192
    const/16 v2, 0x8

    iget-wide v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    .line 4193
    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v2

    add-int/2addr v1, v2

    .line 4195
    :cond_7
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    if-eqz v2, :cond_8

    .line 4196
    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    .line 4197
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4199
    :cond_8
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    if-eqz v2, :cond_9

    .line 4200
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    .line 4201
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4203
    :cond_9
    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    if-eq v2, v6, :cond_a

    .line 4204
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    .line 4205
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v1, v2

    .line 4207
    :cond_a
    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    if-eqz v2, :cond_b

    .line 4208
    const/16 v2, 0x21

    iget-boolean v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    .line 4209
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 4211
    :cond_b
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v2, :cond_d

    .line 4212
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_d

    aget-object v0, v3, v2

    .line 4213
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_c

    .line 4214
    const/16 v5, 0x3e7

    .line 4215
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 4212
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4219
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_d
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4220
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->cachedSize:I

    .line 4221
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamOptions;
    .locals 11
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 4229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4230
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4234
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    if-nez v6, :cond_1

    .line 4235
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    .line 4238
    :cond_1
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v6, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 4240
    :sswitch_0
    return-object p0

    .line 4245
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    goto :goto_0

    .line 4249
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    goto :goto_0

    .line 4253
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 4254
    .local v4, "temp":I
    if-eqz v4, :cond_2

    if-ne v4, v8, :cond_3

    .line 4256
    :cond_2
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    goto :goto_0

    .line 4258
    :cond_3
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    goto :goto_0

    .line 4263
    .end local v4    # "temp":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 4264
    .restart local v4    # "temp":I
    if-eqz v4, :cond_4

    if-eq v4, v8, :cond_4

    if-eq v4, v9, :cond_4

    if-ne v4, v10, :cond_5

    .line 4268
    :cond_4
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    goto :goto_0

    .line 4270
    :cond_5
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    goto :goto_0

    .line 4275
    .end local v4    # "temp":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    goto :goto_0

    .line 4279
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    goto :goto_0

    .line 4283
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    goto :goto_0

    .line 4287
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    goto :goto_0

    .line 4291
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    goto :goto_0

    .line 4295
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    goto :goto_0

    .line 4299
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 4300
    .restart local v4    # "temp":I
    if-eqz v4, :cond_6

    if-eq v4, v8, :cond_6

    if-eq v4, v9, :cond_6

    if-eq v4, v10, :cond_6

    const/4 v6, 0x4

    if-ne v4, v6, :cond_7

    .line 4305
    :cond_6
    iput v4, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    goto/16 :goto_0

    .line 4307
    :cond_7
    iput v5, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    goto/16 :goto_0

    .line 4312
    .end local v4    # "temp":I
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    goto/16 :goto_0

    .line 4316
    :sswitch_d
    const/16 v6, 0x1f3a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4317
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-nez v6, :cond_9

    move v1, v5

    .line 4318
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4319
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v6, :cond_8

    .line 4320
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4322
    :cond_8
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    .line 4323
    :goto_2
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 4324
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 4325
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4326
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4323
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4317
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_9
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v1, v6

    goto :goto_1

    .line 4329
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_a
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    new-instance v7, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    invoke-direct {v7}, Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;-><init>()V

    aput-object v7, v6, v1

    .line 4330
    iget-object v6, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    aget-object v6, v6, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 4230
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x41 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x108 -> :sswitch_c
        0x1f3a -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4041
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$StreamOptions;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 10
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, -0x1

    const/16 v4, 0x100

    const/4 v6, 0x2

    .line 4113
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 4114
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientInitialTokens:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4116
    :cond_0
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 4117
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverInitialTokens:J

    invoke-virtual {p1, v6, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4119
    :cond_1
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    if-eqz v1, :cond_2

    .line 4120
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->tokenUnit:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4122
    :cond_2
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    if-eqz v1, :cond_3

    .line 4123
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLevel:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4125
    :cond_3
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 4126
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->securityLabel:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4128
    :cond_4
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    if-eq v1, v4, :cond_5

    .line 4129
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->clientLogging:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4131
    :cond_5
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    if-eq v1, v4, :cond_6

    .line 4132
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->serverLogging:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4134
    :cond_6
    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_7

    .line 4135
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deadline:D

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 4137
    :cond_7
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    if-eqz v1, :cond_8

    .line 4138
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->failFast:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4140
    :cond_8
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    if-eqz v1, :cond_9

    .line 4141
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->endUserCredsRequested:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4143
    :cond_9
    iget v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    if-eq v1, v6, :cond_a

    .line 4144
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->logLevel:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4146
    :cond_a
    iget-boolean v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    if-eqz v1, :cond_b

    .line 4147
    const/16 v1, 0x21

    iget-boolean v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->deprecated:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4149
    :cond_b
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    if-eqz v1, :cond_d

    .line 4150
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->uninterpretedOption:[Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_d

    aget-object v0, v2, v1

    .line 4151
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    if-eqz v0, :cond_c

    .line 4152
    const/16 v4, 0x3e7

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4150
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4156
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$UninterpretedOption;
    :cond_d
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$StreamOptions;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4158
    return-void
.end method
