.class public final Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DescriptorProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protobuf/DescriptorProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SourceCodeInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;


# instance fields
.field public location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4630
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    sput-object v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4631
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 4820
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    .line 4631
    return-void
.end method

.method public static parseFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;
    .locals 1
    .param p0, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4909
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 4903
    new-instance v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    invoke-direct {v0}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    return-object v0
.end method


# virtual methods
.method public final clear()Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;
    .locals 1

    .prologue
    .line 4823
    sget-object v0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;->EMPTY_ARRAY:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    .line 4824
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    .line 4825
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->cachedSize:I

    .line 4826
    return-object p0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    .line 4845
    const/4 v1, 0x0

    .line 4846
    .local v1, "size":I
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    if-eqz v2, :cond_1

    .line 4847
    iget-object v3, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 4848
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    if-eqz v0, :cond_0

    .line 4849
    const/4 v5, 0x1

    .line 4850
    invoke-static {v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v1, v5

    .line 4847
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4854
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    :cond_1
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v2}, Lcom/google/protobuf/nano/WireFormatNano;->computeWireSize(Ljava/util/List;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4855
    iput v1, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->cachedSize:I

    .line 4856
    return v1
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4864
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4865
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4869
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    if-nez v5, :cond_1

    .line 4870
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    .line 4873
    :cond_1
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v5, p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->storeUnknownField(Ljava/util/List;Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4875
    :sswitch_0
    return-object p0

    .line 4880
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4881
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    if-nez v5, :cond_3

    move v1, v4

    .line 4882
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    .line 4883
    .local v2, "newArray":[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    if-eqz v5, :cond_2

    .line 4884
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4886
    :cond_2
    iput-object v2, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    .line 4887
    :goto_2
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 4888
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;-><init>()V

    aput-object v6, v5, v1

    .line 4889
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4890
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4887
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4881
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    :cond_3
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    array-length v1, v5

    goto :goto_1

    .line 4893
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    :cond_4
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    new-instance v6, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    invoke-direct {v6}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;-><init>()V

    aput-object v6, v5, v1

    .line 4894
    iget-object v5, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    aget-object v5, v5, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4865
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4627
    invoke-virtual {p0, p1}, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4832
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    if-eqz v1, :cond_1

    .line 4833
    iget-object v2, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->location:[Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 4834
    .local v0, "element":Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    if-eqz v0, :cond_0

    .line 4835
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4833
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4839
    .end local v0    # "element":Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo$Location;
    :cond_1
    iget-object v1, p0, Lcom/google/protobuf/DescriptorProtos$SourceCodeInfo;->unknownFieldData:Ljava/util/List;

    invoke-static {v1, p1}, Lcom/google/protobuf/nano/WireFormatNano;->writeUnknownFields(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4841
    return-void
.end method
