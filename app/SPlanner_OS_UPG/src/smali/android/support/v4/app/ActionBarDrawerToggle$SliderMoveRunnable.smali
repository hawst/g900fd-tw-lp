.class Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;
.super Ljava/lang/Object;
.source "ActionBarDrawerToggle.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private duration:I

.field private endPos:F

.field private interpolator:Landroid/view/animation/Interpolator;

.field private listener:Landroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;

.field private startPos:F

.field private startTime:J

.field final synthetic this$0:Landroid/support/v4/app/ActionBarDrawerToggle;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/ActionBarDrawerToggle;FFLandroid/view/animation/Interpolator;ILandroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;)V
    .locals 2

    .prologue
    .line 347
    iput-object p1, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->this$0:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    iput p2, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->startPos:F

    .line 349
    iput p3, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->endPos:F

    .line 350
    iput-object p4, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->interpolator:Landroid/view/animation/Interpolator;

    .line 351
    iput p5, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->duration:I

    .line 352
    iput-object p6, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->listener:Landroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;

    .line 354
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->startTime:J

    .line 356
    invoke-virtual {p0}, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->run()V

    .line 357
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 361
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 362
    iget-wide v2, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->startTime:J

    sub-long/2addr v0, v2

    .line 363
    long-to-float v2, v0

    iget v3, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->duration:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 365
    iget v3, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->startPos:F

    iget-object v4, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->interpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v4, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    iget v4, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->endPos:F

    iget v5, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->startPos:F

    sub-float/2addr v4, v5

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    .line 366
    iget-object v3, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->this$0:Landroid/support/v4/app/ActionBarDrawerToggle;

    # getter for: Landroid/support/v4/app/ActionBarDrawerToggle;->mSlider:Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;
    invoke-static {v3}, Landroid/support/v4/app/ActionBarDrawerToggle;->access$400(Landroid/support/v4/app/ActionBarDrawerToggle;)Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/ActionBarDrawerToggle$SlideDrawable;->setPosition(F)V

    .line 367
    iget v2, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->duration:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 368
    iget-object v0, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->this$0:Landroid/support/v4/app/ActionBarDrawerToggle;

    # getter for: Landroid/support/v4/app/ActionBarDrawerToggle;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->access$500(Landroid/support/v4/app/ActionBarDrawerToggle;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/DrawerLayout;->post(Ljava/lang/Runnable;)Z

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->listener:Landroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Landroid/support/v4/app/ActionBarDrawerToggle$SliderMoveRunnable;->listener:Landroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;

    invoke-interface {v0}, Landroid/support/v4/app/ActionBarDrawerToggle$SliderAnimationListener;->onAnimationEnd()V

    goto :goto_0
.end method
