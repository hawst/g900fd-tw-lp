.class public Lcom/autonavi/amap/mapcore/MapLoader;
.super Ljava/lang/Object;
.source "MapLoader.java"


# instance fields
.field createtime:J

.field datasource:I

.field inRequest:Z

.field mCanceled:Z

.field mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

.field mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

.field mapLevel:I

.field public mapTiles:Ljava/util/ArrayList;

.field nextImgDataLength:I

.field recievedDataBuffer:[B

.field recievedDataSize:I

.field recievedHeader:Z


# direct methods
.method public constructor <init>(Lcom/autonavi/amap/mapcore/BaseMapCallImplement;Lcom/autonavi/amap/mapcore/MapCore;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    .line 32
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z

    .line 36
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 37
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 38
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 39
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->inRequest:Z

    .line 40
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    .line 82
    iput-object p1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    .line 83
    iput p3, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    .line 84
    iput-object p2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->createtime:J

    .line 86
    return-void
.end method

.method private processRecivedData()V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 292
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    if-nez v0, :cond_1

    .line 293
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    if-lt v0, v4, :cond_0

    .line 295
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v6}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 296
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    if-lt v0, v1, :cond_0

    .line 302
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v6}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    .line 303
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v5}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v3

    .line 305
    if-nez v3, :cond_6

    .line 306
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 307
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileDataBmp([BII)V

    .line 349
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    iget-object v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v3, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v6, v3}, Lcom/autonavi/amap/mapcore/Convert;->moveArray([BI[BII)V

    .line 353
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 354
    iput v6, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 355
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    goto :goto_0

    .line 309
    :cond_3
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    if-ne v1, v5, :cond_4

    .line 310
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileDataVTmc([BII)V

    goto :goto_1

    .line 312
    :cond_4
    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_5

    .line 313
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedModels([BII)V

    goto :goto_1

    .line 316
    :cond_5
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, v1, v4, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileData([BII)V

    goto :goto_1

    .line 321
    :cond_6
    const/4 v2, 0x0

    .line 323
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    const/16 v5, 0x8

    invoke-direct {v4, v1, v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 325
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v1, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 327
    const/16 v2, 0x80

    new-array v2, v2, [B

    .line 329
    :goto_2
    invoke-virtual {v1, v2}, Ljava/util/zip/GZIPInputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_7

    .line 330
    const/4 v5, 0x0

    invoke-virtual {v0, v2, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 336
    :catch_0
    move-exception v0

    .line 337
    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 340
    if-eqz v1, :cond_2

    .line 341
    :try_start_3
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 343
    :catch_1
    move-exception v0

    goto :goto_1

    .line 332
    :cond_7
    :try_start_4
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 334
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedTileData([BII)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 340
    if-eqz v1, :cond_2

    .line 341
    :try_start_5
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 343
    :catch_2
    move-exception v0

    goto :goto_1

    .line 339
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 340
    :goto_4
    if-eqz v1, :cond_8

    .line 341
    :try_start_6
    invoke-virtual {v1}, Ljava/util/zip/GZIPInputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    .line 345
    :cond_8
    :goto_5
    throw v0

    .line 343
    :catch_3
    move-exception v1

    goto :goto_5

    .line 339
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 336
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method


# virtual methods
.method public OnException(I)V
    .locals 2

    .prologue
    .line 89
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0, p1}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->OnMapLoaderError(I)V

    .line 93
    :cond_0
    return-void
.end method

.method public addReuqestTiles(Lcom/autonavi/amap/mapcore/MapSourceGridData;)V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    return-void
.end method

.method public destory()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 44
    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    .line 45
    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 46
    sget-object v0, Lcom/autonavi/amap/mapcore/VTMCDataCache;->vtmcHs:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 47
    sget-object v0, Lcom/autonavi/amap/mapcore/VTMCDataCache;->vtmcList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 48
    return-void
.end method

.method public doRequest()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/16 v10, 0x3ea

    .line 97
    iput-boolean v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->inRequest:Z

    .line 99
    invoke-static {}, Lcom/amap/api/maps/MapsInitializer;->getNetWorkEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->isRequestValid()Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    goto :goto_0

    .line 108
    :cond_2
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 110
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_e

    .line 115
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 117
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 119
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v0

    move-object v4, v2

    move v2, v0

    move-object v0, v1

    .line 132
    :goto_1
    const/4 v5, 0x0

    .line 136
    :try_start_0
    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 137
    const/16 v7, 0x4e20

    invoke-static {v6, v7}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 138
    const/16 v7, 0x4e20

    invoke-static {v6, v7}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 140
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7, v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    .line 141
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v6

    const-string v8, "User-Agent"

    sget-object v9, Lcom/amap/api/mapcore/l;->c:Ljava/lang/String;

    invoke-interface {v6, v8, v9}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 143
    if-eqz v0, :cond_3

    .line 144
    new-instance v0, Lorg/apache/http/HttpHost;

    invoke-direct {v0, v4, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    .line 145
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const-string v4, "http.route.default-proxy"

    invoke-interface {v2, v4, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 149
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->getGridParma()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v4}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->getMapSvrAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/amapsrv/MPS?"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 155
    if-eqz v7, :cond_a

    .line 156
    invoke-interface {v7, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 157
    const/16 v2, 0xc8

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    if-ne v2, v4, :cond_9

    .line 160
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 163
    :try_start_1
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOpened(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 164
    const/16 v2, 0x800

    new-array v2, v2, [B

    .line 167
    :goto_2
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v6, -0x1

    if-le v4, v6, :cond_4

    .line 168
    invoke-virtual {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->isRequestValid()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-boolean v6, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v6, :cond_7

    .line 186
    :cond_4
    :goto_3
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 187
    if-eqz v1, :cond_5

    .line 189
    :try_start_2
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 195
    :cond_5
    :goto_4
    if-eqz v0, :cond_0

    .line 197
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto/16 :goto_0

    .line 123
    :cond_6
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v4

    .line 124
    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v2

    .line 125
    if-eqz v4, :cond_d

    .line 126
    new-instance v0, Ljava/net/Proxy;

    sget-object v5, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v6, Ljava/net/InetSocketAddress;

    invoke-direct {v6, v4, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v5, v6}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    goto/16 :goto_1

    .line 171
    :cond_7
    add-int/2addr v3, v4

    .line 172
    const/4 v6, 0x0

    :try_start_4
    invoke-virtual {p0, p0, v6, v2, v4}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionRecieveData(Lcom/autonavi/amap/mapcore/MapLoader;I[BI)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 182
    :catch_1
    move-exception v2

    .line 184
    :goto_5
    const/16 v2, 0x3ea

    :try_start_5
    invoke-virtual {p0, v2}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 186
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 187
    if-eqz v1, :cond_8

    .line 189
    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 195
    :cond_8
    :goto_6
    if-eqz v0, :cond_0

    .line 197
    :try_start_7
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_0

    .line 198
    :catch_2
    move-exception v0

    .line 199
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto/16 :goto_0

    .line 175
    :cond_9
    const/16 v0, 0x3ea

    :try_start_8
    invoke-virtual {p0, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    :goto_7
    move-object v0, v1

    goto :goto_3

    .line 179
    :cond_a
    const/16 v0, 0x3ea

    invoke-virtual {p0, v0}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_7

    .line 182
    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_5

    .line 190
    :catch_4
    move-exception v1

    .line 191
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_4

    .line 190
    :catch_5
    move-exception v1

    .line 191
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_6

    .line 186
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_8
    invoke-virtual {p0, p0}, Lcom/autonavi/amap/mapcore/MapLoader;->onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V

    .line 187
    if-eqz v1, :cond_b

    .line 189
    :try_start_9
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 195
    :cond_b
    :goto_9
    if-eqz v2, :cond_c

    .line 197
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 201
    :cond_c
    :goto_a
    throw v0

    .line 190
    :catch_6
    move-exception v1

    .line 191
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_9

    .line 198
    :catch_7
    move-exception v1

    .line 199
    invoke-virtual {p0, v10}, Lcom/autonavi/amap/mapcore/MapLoader;->OnException(I)V

    goto :goto_a

    .line 186
    :catchall_1
    move-exception v2

    move-object v11, v2

    move-object v2, v0

    move-object v0, v11

    goto :goto_8

    :cond_d
    move-object v0, v1

    goto/16 :goto_1

    :cond_e
    move-object v0, v1

    move v2, v3

    move-object v4, v1

    goto/16 :goto_1
.end method

.method public getGridParma()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 51
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 53
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapSourceGridData;->getGridName()Ljava/lang/String;

    move-result-object v0

    .line 55
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 53
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 58
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 62
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    if-nez v0, :cond_2

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=VMMV3&cp=1&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v2

    .line 60
    goto :goto_1

    .line 64
    :cond_2
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=VMMBLDV3&cp=1&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 66
    :cond_3
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=BMPBM&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 68
    :cond_4
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=BMTI&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 70
    :cond_5
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=TMCV&mesh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 72
    :cond_6
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t=VMMV3&type=mod&cp=0&mid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move-object v0, v2

    .line 76
    goto/16 :goto_1
.end method

.method public isRequestValid()Z
    .locals 3

    .prologue
    .line 209
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, v1, v2}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isGridsInScreen(Ljava/util/ArrayList;I)Z

    move-result v0

    return v0
.end method

.method public onConnectionError(Lcom/autonavi/amap/mapcore/MapLoader;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public onConnectionOpened(Lcom/autonavi/amap/mapcore/MapLoader;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 219
    const/high16 v0, 0x40000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 220
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 221
    iput v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 222
    iput-boolean v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 224
    return-void
.end method

.method public onConnectionOver(Lcom/autonavi/amap/mapcore/MapLoader;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 232
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    .line 233
    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 234
    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 235
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v1, v1, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->tileProcessCtrl:Lcom/autonavi/amap/mapcore/d;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 246
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 239
    :goto_0
    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 240
    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    .line 241
    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    iget-object v2, v0, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->tileProcessCtrl:Lcom/autonavi/amap/mapcore/d;

    iget-object v0, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mapTiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore/MapSourceGridData;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/MapSourceGridData;->getKeyGridName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/autonavi/amap/mapcore/d;->a(Ljava/lang/String;)V

    .line 239
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public onConnectionRecieveData(Lcom/autonavi/amap/mapcore/MapLoader;I[BI)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 250
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    invoke-static {p3, v4, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    add-int/2addr v0, p4

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 260
    iget-boolean v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    if-nez v0, :cond_3

    .line 261
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, v4}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    move-result v0

    .line 269
    if-eqz v0, :cond_2

    .line 270
    iput-boolean v5, p1, Lcom/autonavi/amap/mapcore/MapLoader;->mCanceled:Z

    goto :goto_0

    .line 273
    :cond_2
    const/4 v0, 0x4

    .line 274
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 275
    const/16 v0, 0x8

    .line 278
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    iget-object v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    add-int/lit8 v3, p4, -0x8

    invoke-static {v1, v0, v2, v4, v3}, Lcom/autonavi/amap/mapcore/Convert;->moveArray([BI[BII)V

    .line 280
    iget v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataSize:I

    .line 281
    iput v4, p0, Lcom/autonavi/amap/mapcore/MapLoader;->nextImgDataLength:I

    .line 282
    iput-boolean v5, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedHeader:Z

    .line 283
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    .line 288
    :cond_3
    invoke-direct {p0}, Lcom/autonavi/amap/mapcore/MapLoader;->processRecivedData()V

    goto :goto_0
.end method

.method processRecivedModels([BII)V
    .locals 3

    .prologue
    .line 416
    .line 417
    add-int/lit8 v0, p2, 0x1

    aget-byte v1, p1, p2

    .line 418
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v0, v1}, Ljava/lang/String;-><init>([BII)V

    .line 420
    add-int/2addr v0, v1

    .line 421
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 425
    :cond_0
    sub-int v0, p3, p2

    new-array v0, v0, [B

    .line 426
    const/4 v1, 0x0

    sub-int v2, p3, p2

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 428
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileData([BII)V
    .locals 3

    .prologue
    .line 363
    .line 364
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getShort([BI)S

    .line 365
    add-int/lit8 v0, p2, 0x2

    .line 366
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getShort([BI)S

    .line 367
    add-int/lit8 v0, v0, 0x2

    .line 368
    iget-object v1, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v1, v0}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 369
    add-int/lit8 v0, v0, 0x4

    .line 370
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 371
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 373
    add-int/2addr v0, v1

    .line 375
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileDataBmp([BII)V
    .locals 3

    .prologue
    .line 382
    .line 383
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 384
    add-int/lit8 v0, p2, 0x4

    .line 385
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 386
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 387
    add-int/2addr v0, v1

    .line 388
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 392
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method

.method processRecivedTileDataVTmc([BII)V
    .locals 5

    .prologue
    .line 395
    .line 396
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->recievedDataBuffer:[B

    invoke-static {v0, p2}, Lcom/autonavi/amap/mapcore/Convert;->getInt([BI)I

    .line 397
    add-int/lit8 v0, p2, 0x4

    .line 398
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p1, v0

    .line 399
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, p1, v1, v0}, Ljava/lang/String;-><init>([BII)V

    .line 400
    add-int/2addr v0, v1

    .line 401
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCallback:Lcom/autonavi/amap/mapcore/BaseMapCallImplement;

    invoke-virtual {v0}, Lcom/autonavi/amap/mapcore/BaseMapCallImplement;->isMapEngineValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-static {}, Lcom/autonavi/amap/mapcore/VTMCDataCache;->getInstance()Lcom/autonavi/amap/mapcore/VTMCDataCache;

    move-result-object v0

    .line 406
    sub-int v1, p3, p2

    new-array v1, v1, [B

    .line 407
    const/4 v3, 0x0

    sub-int v4, p3, p2

    invoke-static {p1, p2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 409
    invoke-virtual {v0, v2, v1}, Lcom/autonavi/amap/mapcore/VTMCDataCache;->putData(Ljava/lang/String;[B)V

    .line 411
    iget-object v0, p0, Lcom/autonavi/amap/mapcore/MapLoader;->mMapCore:Lcom/autonavi/amap/mapcore/MapCore;

    sub-int v1, p3, p2

    iget v2, p0, Lcom/autonavi/amap/mapcore/MapLoader;->datasource:I

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/autonavi/amap/mapcore/MapCore;->putMapData([BIII)V

    goto :goto_0
.end method
