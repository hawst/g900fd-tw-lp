.class final Lcom/android/calendar/fp;
.super Landroid/os/Handler;
.source "RecipientAdapter.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/fk;


# direct methods
.method private constructor <init>(Lcom/android/calendar/fk;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/fk;Lcom/android/calendar/fl;)V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0, p1}, Lcom/android/calendar/fp;-><init>(Lcom/android/calendar/fk;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 498
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v2, v1}, Lcom/android/calendar/fp;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/calendar/fp;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 500
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/fp;->removeMessages(I)V

    .line 504
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 491
    iget-object v0, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->j(Lcom/android/calendar/fk;)I

    move-result v0

    if-lez v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    iget-object v1, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    invoke-static {v3}, Lcom/android/calendar/fk;->f(Lcom/android/calendar/fk;)Ljava/util/LinkedHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    invoke-static {v4}, Lcom/android/calendar/fk;->g(Lcom/android/calendar/fk;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/fp;->a:Lcom/android/calendar/fk;

    invoke-static {v5}, Lcom/android/calendar/fk;->h(Lcom/android/calendar/fk;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/calendar/fk;->a(Lcom/android/calendar/fk;ZLjava/util/LinkedHashMap;Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/fk;->b(Lcom/android/calendar/fk;Ljava/util/List;)V

    .line 495
    :cond_0
    return-void
.end method
