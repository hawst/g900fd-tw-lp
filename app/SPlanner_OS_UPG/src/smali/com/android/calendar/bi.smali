.class public Lcom/android/calendar/bi;
.super Landroid/os/Handler;
.source "ContactsAsyncHelper.java"


# static fields
.field private static a:Lcom/android/calendar/bi;

.field private static b:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/bi;->a:Lcom/android/calendar/bi;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 126
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ContactsAsyncWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 128
    new-instance v1, Lcom/android/calendar/bl;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/bl;-><init>(Lcom/android/calendar/bi;Landroid/os/Looper;)V

    sput-object v1, Lcom/android/calendar/bi;->b:Landroid/os/Handler;

    .line 129
    return-void
.end method

.method public static final a(Landroid/content/Context;Lcom/android/calendar/event/aw;Ljava/lang/Runnable;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 194
    if-nez p3, :cond_0

    .line 221
    :goto_0
    return-void

    .line 202
    :cond_0
    new-instance v0, Lcom/android/calendar/bk;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/bk;-><init>(Lcom/android/calendar/bj;)V

    .line 203
    iput-object p0, v0, Lcom/android/calendar/bk;->a:Landroid/content/Context;

    .line 204
    iput-object p1, v0, Lcom/android/calendar/bk;->f:Lcom/android/calendar/event/aw;

    .line 205
    iput-object p3, v0, Lcom/android/calendar/bk;->c:Landroid/net/Uri;

    .line 206
    iput-object p2, v0, Lcom/android/calendar/bk;->g:Ljava/lang/Runnable;

    .line 208
    sget-object v1, Lcom/android/calendar/bi;->a:Lcom/android/calendar/bi;

    if-nez v1, :cond_1

    .line 209
    new-instance v1, Lcom/android/calendar/bi;

    invoke-direct {v1}, Lcom/android/calendar/bi;-><init>()V

    sput-object v1, Lcom/android/calendar/bi;->a:Lcom/android/calendar/bi;

    .line 212
    :cond_1
    sget-object v1, Lcom/android/calendar/bi;->b:Landroid/os/Handler;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 213
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 214
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/android/calendar/bi;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 228
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/bk;

    .line 229
    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 233
    :pswitch_0
    iget-object v1, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 234
    iget-object v1, v0, Lcom/android/calendar/bk;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 235
    iget-object v1, v0, Lcom/android/calendar/bk;->b:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 236
    :cond_1
    iget v1, v0, Lcom/android/calendar/bk;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 237
    iget-object v1, v0, Lcom/android/calendar/bk;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 238
    iget-object v1, v0, Lcom/android/calendar/bk;->b:Landroid/widget/ImageView;

    iget v0, v0, Lcom/android/calendar/bk;->d:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 242
    :pswitch_1
    iget-object v1, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 243
    iget-object v2, v0, Lcom/android/calendar/bk;->f:Lcom/android/calendar/event/aw;

    iget-object v1, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    iput-object v1, v2, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    .line 244
    iget-object v1, v0, Lcom/android/calendar/bk;->g:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 245
    iget-object v0, v0, Lcom/android/calendar/bk;->g:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
