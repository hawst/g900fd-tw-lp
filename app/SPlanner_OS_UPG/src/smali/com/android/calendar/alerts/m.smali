.class public Lcom/android/calendar/alerts/m;
.super Landroid/widget/ResourceCursorAdapter;
.source "AlertAdapter.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/alerts/AlertActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/android/calendar/alerts/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/alerts/AlertActivity;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v1, v0}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Z)V

    .line 39
    iput-object v1, p0, Lcom/android/calendar/alerts/m;->b:Lcom/android/calendar/alerts/AlertActivity;

    .line 50
    iput-object p1, p0, Lcom/android/calendar/alerts/m;->b:Lcom/android/calendar/alerts/AlertActivity;

    .line 51
    return-void
.end method


# virtual methods
.method public a(I)J
    .locals 4

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/android/calendar/alerts/m;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 62
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 63
    :cond_0
    const-wide/16 v0, -0x1

    .line 71
    :goto_0
    return-wide v0

    .line 66
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 67
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 68
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 69
    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;Lcom/android/calendar/alerts/o;)V
    .locals 16

    .prologue
    .line 123
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 124
    const/4 v3, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 125
    const/4 v3, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 126
    const/4 v3, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 127
    const/4 v3, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_4

    const/4 v8, 0x1

    .line 128
    :goto_0
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 131
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 132
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 134
    :cond_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/alerts/o;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    .line 139
    if-eqz v8, :cond_1

    .line 140
    const-string v2, "UTC"

    .line 143
    :cond_1
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 145
    iget v3, v3, Landroid/text/format/Time;->isDst:I

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    move v11, v3

    .line 146
    :goto_1
    const/4 v9, 0x0

    .line 147
    if-eqz v8, :cond_2

    .line 148
    const/4 v9, 0x1

    .line 150
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v10}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v13, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 153
    if-nez v8, :cond_3

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 154
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 155
    const/16 v3, 0x20

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    const/4 v4, 0x0

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v11, v4, v5}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    :goto_2
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 162
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/android/calendar/alerts/o;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 166
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->c:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    :goto_3
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    const v3, 0x7f120067

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 174
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/m;->b:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v3, v3, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v3, v14, v15}, Lcom/android/calendar/g/a/a;->b(J)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/m;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_8

    .line 177
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 182
    :goto_4
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/m;->b:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v3, v3, Lcom/android/calendar/alerts/AlertActivity;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    return-void

    .line 127
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 145
    :cond_5
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_1

    .line 157
    :cond_6
    const/16 v3, 0x20

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v2, v11, v4, v5}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 168
    :cond_7
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 179
    :cond_8
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 76
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 77
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/m;->a:Ljava/lang/String;

    const-string v1, "Error: query returned no results"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :goto_0
    return-void

    .line 81
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/o;

    .line 82
    if-nez v0, :cond_2

    .line 83
    new-instance v1, Lcom/android/calendar/alerts/o;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/o;-><init>(Lcom/android/calendar/alerts/n;)V

    .line 84
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 85
    const v0, 0x7f120034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/alerts/o;->a:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f120064

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/alerts/o;->b:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f120066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/alerts/o;->c:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f120067

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/android/calendar/alerts/o;->d:Landroid/widget/CheckBox;

    move-object v0, v1

    .line 92
    :cond_2
    const v1, 0x7f120063

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 94
    const/16 v1, 0xc

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 96
    if-nez v1, :cond_3

    .line 97
    const/4 v1, 0x7

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/hj;->b(I)I

    move-result v1

    .line 101
    :cond_3
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 104
    const v1, 0x7f120065

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 105
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 107
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 111
    :goto_1
    invoke-virtual {p0, p2, p3, v0}, Lcom/android/calendar/alerts/m;->a(Landroid/content/Context;Landroid/database/Cursor;Lcom/android/calendar/alerts/o;)V

    goto :goto_0

    .line 109
    :cond_4
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onContentChanged()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Landroid/widget/ResourceCursorAdapter;->onContentChanged()V

    .line 188
    iget-object v0, p0, Lcom/android/calendar/alerts/m;->b:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->a()V

    .line 189
    return-void
.end method
