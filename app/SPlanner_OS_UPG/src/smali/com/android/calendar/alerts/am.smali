.class Lcom/android/calendar/alerts/am;
.super Landroid/database/ContentObserver;
.source "TaskAlertActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/TaskAlertActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    .prologue
    .line 403
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 404
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    iget-object v1, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/TaskAlertActivity;->c(Lcom/android/calendar/alerts/TaskAlertActivity;)Lcom/android/calendar/alerts/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/alerts/au;->getCount()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    .line 411
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    iget v0, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    if-nez v0, :cond_2

    .line 412
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    .line 419
    :cond_0
    :goto_1
    return-void

    .line 408
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    goto :goto_0

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->a()V

    .line 416
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b()V

    .line 417
    iget-object v0, p0, Lcom/android/calendar/alerts/am;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    goto :goto_1
.end method
