.class public abstract Lcom/android/calendar/alerts/slidingTabView/c;
.super Ljava/lang/Object;
.source "AbstractSlider.java"


# instance fields
.field protected final a:Landroid/widget/ImageView;

.field protected final b:Landroid/widget/ImageView;

.field protected final c:Landroid/widget/ImageView;

.field protected final d:Landroid/widget/ImageView;

.field protected e:I

.field protected f:Landroid/widget/ImageView;

.field protected g:Landroid/widget/ImageView;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;Z)V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v0, 0x0

    const/4 v3, 0x4

    const/4 v2, -0x2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->e:I

    .line 51
    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->h:Z

    .line 63
    iput-boolean p8, p0, Lcom/android/calendar/alerts/slidingTabView/c;->h:Z

    .line 64
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    .line 65
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 66
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 67
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    .line 70
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 71
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 72
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 76
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 77
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    .line 82
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 83
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 90
    if-lez p4, :cond_0

    .line 91
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    .line 92
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 94
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    .line 98
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 99
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 100
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 108
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 111
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 113
    if-eqz p8, :cond_1

    .line 114
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 174
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 175
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 176
    return-void
.end method


# virtual methods
.method protected a(FFJ)Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 180
    invoke-virtual {v0, p3, p4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 181
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 182
    return-object v0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;)V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;)V

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 156
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->f:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;)V

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 159
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;)V

    .line 161
    :cond_3
    return-void
.end method

.method protected a(I)V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 124
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 125
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    goto :goto_0
.end method

.method protected abstract a(IIIIZ)V
.end method

.method protected a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 130
    if-eqz p2, :cond_0

    .line 131
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 133
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 134
    return-void
.end method

.method protected a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    const/high16 v2, 0x3f800000    # 1.0f

    .line 194
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    if-eqz p1, :cond_2

    .line 200
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 208
    if-eqz p1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected b(FFJ)Landroid/view/animation/Animation;
    .locals 9

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const v1, 0x3dcccccd    # 0.1f

    const/4 v5, 0x1

    .line 186
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v2, p1

    move v3, v1

    move v4, p2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 188
    invoke-virtual {v0, p3, p4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 189
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 190
    return-object v0
.end method

.method protected b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 242
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->h:Z

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;Z)V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;Z)V

    .line 246
    return-void
.end method

.method protected b(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 215
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 216
    iput p1, p0, Lcom/android/calendar/alerts/slidingTabView/c;->e:I

    .line 217
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 250
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/c;->b()V

    .line 251
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method protected c(Z)V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 268
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 269
    return-void
.end method

.method protected d()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 272
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 277
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 278
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 280
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 281
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 283
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(FFJ)Landroid/view/animation/Animation;

    move-result-object v0

    .line 284
    invoke-virtual {p0, v2, v2, v4, v5}, Lcom/android/calendar/alerts/slidingTabView/c;->b(FFJ)Landroid/view/animation/Animation;

    move-result-object v1

    .line 285
    new-instance v2, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 286
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 287
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 288
    new-instance v0, Lcom/android/calendar/alerts/slidingTabView/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/slidingTabView/d;-><init>(Lcom/android/calendar/alerts/slidingTabView/c;)V

    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 306
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 307
    return-void
.end method

.method protected abstract d(Z)V
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    return v0
.end method

.method protected f()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 315
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/c;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Landroid/view/View;Z)V

    .line 316
    return-void
.end method
