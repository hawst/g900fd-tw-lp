.class public Lcom/android/calendar/alerts/PopUpActivity;
.super Landroid/app/Activity;
.source "PopUpActivity.java"

# interfaces
.implements Lcom/android/calendar/detail/bp;


# static fields
.field private static C:Lcom/android/calendar/alerts/ai;

.field private static I:Z

.field private static L:I

.field private static M:Z

.field private static final X:[Ljava/lang/String;

.field private static final Y:[Ljava/lang/String;

.field private static final a:Ljava/lang/String;

.field private static b:J

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static e:Ljava/lang/String;

.field private static f:J

.field private static g:J


# instance fields
.field private A:Landroid/widget/RelativeLayout;

.field private B:Lcom/android/calendar/alerts/slidingTabView/a;

.field private D:Lcom/samsung/android/sdk/cover/Scover;

.field private E:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private F:Z

.field private G:Z

.field private H:Z

.field private J:Landroid/os/PowerManager$WakeLock;

.field private K:Landroid/os/PowerManager;

.field private final N:Landroid/os/Handler;

.field private O:J

.field private P:J

.field private Q:J

.field private R:J

.field private S:Z

.field private T:Z

.field private U:Ljava/lang/String;

.field private V:Ljava/util/ArrayList;

.field private W:Lcom/android/calendar/alerts/ae;

.field private final Z:Lcom/android/calendar/alerts/slidingTabView/h;

.field private final aa:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private final ab:Landroid/content/BroadcastReceiver;

.field private final ac:Lcom/android/calendar/alerts/slidingTabView/h;

.field private h:Z

.field private s:J

.field private t:Landroid/widget/LinearLayout;

.field private u:Landroid/widget/LinearLayout;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    const-class v0, Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    .line 118
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    .line 133
    sput-boolean v3, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    .line 163
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "eventLocation"

    aput-object v1, v0, v5

    const-string v1, "allDay"

    aput-object v1, v0, v6

    const-string v1, "begin"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "state"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "alarmTime"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->X:[Ljava/lang/String;

    .line 178
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "task_id"

    aput-object v1, v0, v4

    const-string v1, "reminder_time"

    aput-object v1, v0, v5

    const-string v1, "state"

    aput-object v1, v0, v6

    const-string v1, "subject"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountkey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->Y:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 103
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    .line 105
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->s:J

    .line 107
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->t:Landroid/widget/LinearLayout;

    .line 108
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->u:Landroid/widget/LinearLayout;

    .line 114
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->A:Landroid/widget/RelativeLayout;

    .line 120
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->D:Lcom/samsung/android/sdk/cover/Scover;

    .line 121
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 124
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    .line 127
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->G:Z

    .line 130
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->H:Z

    .line 136
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->K:Landroid/os/PowerManager;

    .line 140
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->N:Landroid/os/Handler;

    .line 146
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->S:Z

    .line 147
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->T:Z

    .line 149
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->U:Ljava/lang/String;

    .line 150
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->V:Ljava/util/ArrayList;

    .line 151
    iput-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->W:Lcom/android/calendar/alerts/ae;

    .line 190
    new-instance v0, Lcom/android/calendar/alerts/w;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/w;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->Z:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 218
    new-instance v0, Lcom/android/calendar/alerts/y;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/y;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->aa:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 239
    new-instance v0, Lcom/android/calendar/alerts/z;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/z;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->ab:Landroid/content/BroadcastReceiver;

    .line 274
    new-instance v0, Lcom/android/calendar/alerts/aa;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/aa;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->ac:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 977
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;)J
    .locals 2

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->O:J

    return-wide v0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;J)J
    .locals 1

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/android/calendar/alerts/PopUpActivity;->s:J

    return-wide p1
.end method

.method private static a(JJJJIZ)Landroid/content/ContentValues;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 729
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 730
    if-eqz p9, :cond_0

    .line 731
    const-string v1, "task_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 733
    const-string v1, "due_date"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 734
    const-string v1, "state"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 735
    const-string v1, "reminder_time"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 736
    const-string v1, "subject"

    sget-object v2, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const-string v1, "reminder_type"

    sget-wide v2, Lcom/android/calendar/alerts/PopUpActivity;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 738
    const-string v1, "accountkey"

    sget-wide v2, Lcom/android/calendar/alerts/PopUpActivity;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 751
    :goto_0
    return-object v0

    .line 741
    :cond_0
    const-string v1, "event_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 742
    const-string v1, "begin"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 743
    const-string v1, "end"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 744
    const-string v1, "alarmTime"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 745
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 746
    const-string v1, "creationTime"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 747
    const-string v1, "receivedTime"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 748
    const-string v1, "notifyTime"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 749
    const-string v1, "state"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 750
    const-string v1, "minutes"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    .line 708
    new-instance v2, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 711
    if-nez p3, :cond_0

    .line 712
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 713
    sget-object v0, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    .line 714
    sget-object v3, Lcom/android/calendar/alerts/PopUpActivity;->X:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v3, v3, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 721
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 722
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v2, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 723
    sget-object v3, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateAlarmState() cntOfUpdatedRow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Uri : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " selection : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    return-void

    .line 716
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 717
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/calendar/alerts/PopUpActivity;->Y:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 718
    sget-object v3, Lcom/android/calendar/alerts/PopUpActivity;->Y:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;JZ)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/alerts/PopUpActivity;->a(JZ)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 819
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.cover.STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 820
    const-string v1, "suppressCoverUI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 821
    const-string v1, "sender"

    sget-object v2, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 822
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 823
    return-void
.end method

.method private a(Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 782
    .line 784
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->V:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 815
    :cond_1
    :goto_0
    return v0

    .line 788
    :cond_2
    const-string v0, ""

    const-string v0, ""

    .line 790
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 791
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 792
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 795
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 796
    iget-object v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 800
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 804
    const v0, 0x7f12005f

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 805
    const v4, 0x7f020004

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 806
    iget-object v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 808
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 809
    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 811
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 812
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 813
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    move v0, v1

    .line 815
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/PopUpActivity;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/android/calendar/alerts/PopUpActivity;->T:Z

    return p1
.end method

.method private static a(Lcom/samsung/android/sdk/cover/Scover;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 764
    .line 765
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/cover/Scover;->isFeatureEnabled(I)Z

    move-result v1

    .line 766
    if-eqz v1, :cond_1

    .line 773
    :cond_0
    :goto_0
    return v0

    .line 769
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/cover/Scover;->isFeatureEnabled(I)Z

    move-result v1

    .line 770
    if-nez v1, :cond_0

    .line 773
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 826
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 827
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 971
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    if-nez v0, :cond_0

    .line 972
    const/4 v0, 0x0

    .line 974
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/alerts/PopUpActivity;)Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->h()V

    return-void
.end method

.method public static c()Z
    .locals 1

    .prologue
    .line 989
    sget-boolean v0, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    return v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->g()V

    return-void
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 325
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 347
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 329
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/alerts/aj;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    .line 335
    if-ne v0, v2, :cond_2

    .line 336
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->A:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 337
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 339
    :cond_2
    if-nez v0, :cond_0

    .line 341
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->A:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 342
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->f()Lcom/android/calendar/alerts/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/ai;->a(Lcom/android/calendar/alerts/af;)V

    .line 343
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->a(Z)V

    .line 344
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->i()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->N:Landroid/os/Handler;

    return-object v0
.end method

.method private f()Lcom/android/calendar/alerts/af;
    .locals 4

    .prologue
    .line 350
    new-instance v0, Lcom/android/calendar/alerts/af;

    invoke-direct {v0}, Lcom/android/calendar/alerts/af;-><init>()V

    .line 351
    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/alerts/af;->a:Ljava/lang/String;

    .line 352
    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->P:J

    iput-wide v2, v0, Lcom/android/calendar/alerts/af;->b:J

    .line 353
    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->Q:J

    iput-wide v2, v0, Lcom/android/calendar/alerts/af;->c:J

    .line 354
    sget-boolean v1, Lcom/android/calendar/alerts/PopUpActivity;->M:Z

    iput-boolean v1, v0, Lcom/android/calendar/alerts/af;->d:Z

    .line 355
    iget-boolean v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    iput-boolean v1, v0, Lcom/android/calendar/alerts/af;->e:Z

    .line 356
    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/alerts/af;->f:Ljava/lang/String;

    .line 357
    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 379
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->dismiss()V

    .line 381
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    .line 382
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->a(Z)V

    .line 384
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->e()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->V:Ljava/util/ArrayList;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 387
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 388
    sget v1, Lcom/android/calendar/alerts/PopUpActivity;->L:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 390
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 391
    const-string v1, "com.android.calendar.CALENDAR_ALARM_FOR_BTSPK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    const-string v1, "action"

    sget-object v2, Lcom/android/calendar/alerts/ad;->b:Lcom/android/calendar/alerts/ad;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 393
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 395
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 396
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->U:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .locals 12

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 400
    invoke-static {p0}, Lcom/android/calendar/hj;->n(Landroid/content/Context;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/android/calendar/alerts/PopUpActivity;->b:J

    .line 401
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/android/calendar/alerts/PopUpActivity;->b:J

    add-long v6, v0, v2

    .line 402
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/app/NotificationManager;

    .line 403
    iget-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->P:J

    iget-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->Q:J

    const/4 v8, 0x0

    iget-boolean v9, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    invoke-static/range {v0 .. v9}, Lcom/android/calendar/alerts/PopUpActivity;->a(JJJJIZ)Landroid/content/ContentValues;

    move-result-object v1

    .line 405
    sget-object v0, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    .line 406
    iget-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    if-eqz v2, :cond_0

    .line 407
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    .line 410
    :cond_0
    sget v2, Lcom/android/calendar/alerts/PopUpActivity;->L:I

    invoke-virtual {v10, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 411
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 412
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;)Lcom/android/calendar/alerts/slidingTabView/e;

    move-result-object v0

    invoke-static {p0, v0, v6, v7}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;J)V

    .line 413
    invoke-static {p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 415
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 416
    const-string v1, "com.android.calendar.CALENDAR_ALARM_FOR_BTSPK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    const-string v1, "action"

    sget-object v2, Lcom/android/calendar/alerts/ad;->c:Lcom/android/calendar/alerts/ad;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 418
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 420
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 421
    return-void
.end method

.method static synthetic j(Lcom/android/calendar/alerts/PopUpActivity;)Lcom/android/calendar/alerts/ae;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->W:Lcom/android/calendar/alerts/ae;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 434
    sput-boolean v3, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    .line 435
    new-instance v0, Lcom/android/calendar/alerts/ae;

    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/ae;-><init>(Lcom/android/calendar/alerts/PopUpActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->W:Lcom/android/calendar/alerts/ae;

    .line 436
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->V:Ljava/util/ArrayList;

    .line 438
    const v0, 0x7f12005d

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->t:Landroid/widget/LinearLayout;

    .line 439
    const v0, 0x7f12005e

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->u:Landroid/widget/LinearLayout;

    .line 441
    const v0, 0x7f12005c

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->v:Landroid/widget/TextView;

    .line 442
    const v0, 0x7f12002d

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->w:Landroid/widget/TextView;

    .line 443
    const v0, 0x7f12003c

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->x:Landroid/widget/TextView;

    .line 444
    const v0, 0x7f120060

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->y:Landroid/widget/TextView;

    .line 445
    const v0, 0x7f120061

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    .line 446
    const v0, 0x7f12005a

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->A:Landroid/widget/RelativeLayout;

    .line 447
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->A:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 450
    invoke-static {p0}, Lcom/android/calendar/dz;->u(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    .line 451
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    if-nez v0, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v0}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->D:Lcom/samsung/android/sdk/cover/Scover;

    .line 456
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->D:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V

    .line 457
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->D:Lcom/samsung/android/sdk/cover/Scover;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/samsung/android/sdk/cover/Scover;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->G:Z

    .line 458
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->G:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    .line 471
    iput-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    .line 472
    new-instance v0, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 473
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 474
    new-instance v1, Lcom/android/calendar/alerts/ai;

    iget-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->D:Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1, p0, v2, v0}, Lcom/android/calendar/alerts/ai;-><init>(Lcom/android/calendar/alerts/PopUpActivity;Lcom/samsung/android/sdk/cover/Scover;Lcom/samsung/android/sdk/cover/ScoverState;)V

    sput-object v1, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    .line 475
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->Z:Lcom/android/calendar/alerts/slidingTabView/h;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/ai;->a(Lcom/android/calendar/alerts/slidingTabView/h;)V

    .line 478
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v1}, Lcom/android/calendar/alerts/ai;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    goto :goto_0

    .line 461
    :catch_0
    move-exception v0

    .line 462
    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iput-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    goto :goto_0

    .line 465
    :catch_1
    move-exception v0

    .line 466
    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iput-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method private k()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 482
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 484
    const-string v0, "event_title"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    .line 485
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 486
    const-string v3, "isTaskAlert"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    .line 488
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    if-nez v0, :cond_2

    .line 489
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f02dd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    .line 494
    :cond_0
    :goto_0
    const v0, 0x7f12005b

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 495
    iget-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    if-nez v3, :cond_3

    .line 496
    const-string v3, "event_location"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/android/calendar/alerts/PopUpActivity;->d:Ljava/lang/String;

    .line 497
    const v3, 0x7f0f01a9

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 503
    :goto_1
    const-string v0, "event_start_time"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->P:J

    .line 504
    const-string v0, "event_end_time"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->Q:J

    .line 505
    const-string v0, "event_id"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    .line 506
    const-string v0, "alert_id"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->O:J

    .line 507
    const-string v0, "reminder_type"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/android/calendar/alerts/PopUpActivity;->f:J

    .line 508
    const-string v0, "accountKey"

    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    sput-wide v4, Lcom/android/calendar/alerts/PopUpActivity;->g:J

    .line 509
    const-string v0, "notification_id"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/calendar/alerts/PopUpActivity;->L:I

    .line 510
    const-string v0, "allDay"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/alerts/PopUpActivity;->M:Z

    .line 511
    const-string v0, "organizer"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->U:Ljava/lang/String;

    .line 512
    const v0, 0x7f120062

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/slidingTabView/a;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->B:Lcom/android/calendar/alerts/slidingTabView/a;

    .line 513
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->B:Lcom/android/calendar/alerts/slidingTabView/a;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->B:Lcom/android/calendar/alerts/slidingTabView/a;

    iget-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->ac:Lcom/android/calendar/alerts/slidingTabView/h;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/a;->setOnTriggerListener(Lcom/android/calendar/alerts/slidingTabView/h;)V

    .line 516
    :cond_1
    invoke-static {p0}, Lcom/android/calendar/g/d;->a(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object v0

    .line 517
    iget-object v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->B:Lcom/android/calendar/alerts/slidingTabView/a;

    invoke-virtual {v2, v0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(Landroid/graphics/Point;)V

    .line 519
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->l()V

    .line 520
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->m()V

    .line 521
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->w:Landroid/widget/TextView;

    sget-object v2, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 523
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->x:Landroid/widget/TextView;

    sget-object v2, Lcom/android/calendar/alerts/PopUpActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 524
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 528
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->N:Landroid/os/Handler;

    new-instance v2, Lcom/android/calendar/alerts/ab;

    invoke-direct {v2, p0}, Lcom/android/calendar/alerts/ab;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 536
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->n()V

    .line 537
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->o()V

    .line 540
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 541
    const-string v2, "com.android.calendar.CALENDAR_ALARM_ALERT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 542
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 545
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 546
    const-string v2, "com.android.calendar.CALENDAR_ALARM_FOR_BTSPK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 547
    const-string v2, "action"

    sget-object v3, Lcom/android/calendar/alerts/ad;->a:Lcom/android/calendar/alerts/ad;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 548
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 550
    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    sget v4, Lcom/android/calendar/alerts/PopUpActivity;->L:I

    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {p0, v2, v3, v4, v0}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JIZ)V

    .line 551
    return-void

    .line 490
    :cond_2
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    if-eqz v0, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f02bf

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 499
    :cond_3
    const/4 v3, 0x0

    sput-object v3, Lcom/android/calendar/alerts/PopUpActivity;->d:Ljava/lang/String;

    .line 500
    const v3, 0x7f0f0428

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 526
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->t:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    :cond_5
    move v0, v1

    .line 550
    goto :goto_3
.end method

.method static synthetic l(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->u:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private l()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 561
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 562
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    .line 564
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    .line 565
    if-ne v0, v9, :cond_0

    if-nez v1, :cond_0

    .line 566
    iget-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    iget-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->P:J

    iget-wide v6, p0, Lcom/android/calendar/alerts/PopUpActivity;->Q:J

    sget-boolean v8, Lcom/android/calendar/alerts/PopUpActivity;->M:Z

    move-object v1, p0

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Ljava/util/Calendar;ZJJZZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->e:Ljava/lang/String;

    .line 572
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->v:Landroid/widget/TextView;

    sget-object v1, Lcom/android/calendar/alerts/PopUpActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 573
    return-void

    .line 569
    :cond_0
    iget-boolean v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->h:Z

    iget-wide v4, p0, Lcom/android/calendar/alerts/PopUpActivity;->P:J

    iget-wide v6, p0, Lcom/android/calendar/alerts/PopUpActivity;->Q:J

    sget-boolean v8, Lcom/android/calendar/alerts/PopUpActivity;->M:Z

    const/4 v9, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Ljava/util/Calendar;ZJJZZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/PopUpActivity;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 580
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 581
    if-ne v0, v1, :cond_0

    .line 582
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->x:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 583
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->x:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 587
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->q()V

    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 590
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->S:Z

    if-eqz v0, :cond_0

    .line 601
    :goto_0
    return-void

    .line 594
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 595
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 596
    const-string v1, "com.samsung.bluetooth.action.STOP_SCHEDULE_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 597
    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 598
    const-string v1, "com.android.calendar.FINISH_POPUP_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 599
    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->ab:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v1, v2}, Lcom/android/calendar/alerts/PopUpActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 600
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->S:Z

    goto :goto_0
.end method

.method private o()V
    .locals 8

    .prologue
    .line 604
    iget-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->s:J

    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 605
    iget-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    iput-wide v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->s:J

    .line 606
    const-string v0, "event_id=?"

    .line 607
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->R:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 610
    const-string v7, "attendeeName ASC, attendeeEmail ASC"

    .line 612
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->W:Lcom/android/calendar/alerts/ae;

    const/16 v1, 0xa

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/alerts/PopUpActivity;->i:[Ljava/lang/String;

    const-string v5, "event_id=?"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/alerts/ae;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/alerts/ac;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/ac;-><init>(Lcom/android/calendar/alerts/PopUpActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 622
    :cond_0
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 667
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/alerts/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 677
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->f()Lcom/android/calendar/alerts/af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/ai;->a(Lcom/android/calendar/alerts/af;)V

    .line 678
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->a()Z

    move-result v0

    .line 679
    if-nez v0, :cond_0

    .line 680
    sget-object v0, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ai;->dismiss()V

    goto :goto_0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 945
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 946
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 954
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/alerts/TextToSpeechService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 952
    const-string v1, "TTS_HOST"

    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 953
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private r()Ljava/lang/String;
    .locals 2

    .prologue
    .line 957
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 959
    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->y:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 960
    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 963
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 964
    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 967
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 299
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->c(Landroid/view/Window;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 425
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 426
    const v0, 0x7f040017

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->setContentView(I)V

    .line 427
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->b(Landroid/view/Window;)V

    .line 428
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->j()V

    .line 429
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->k()V

    .line 430
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->e()V

    .line 431
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 305
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 306
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->a(Landroid/view/Window;)V

    .line 307
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->b(Landroid/view/Window;)V

    .line 309
    const v0, 0x7f040017

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->setContentView(I)V

    .line 311
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->j()V

    .line 312
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->k()V

    .line 313
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->e()V

    .line 315
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->K:Landroid/os/PowerManager;

    .line 316
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->K:Landroid/os/PowerManager;

    const v1, 0x3000000a

    sget-object v2, Lcom/android/calendar/alerts/PopUpActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    .line 318
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 321
    sget-object v0, Lcom/android/calendar/hj;->l:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 322
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 626
    sput-boolean v2, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    .line 627
    iput-object v3, p0, Lcom/android/calendar/alerts/PopUpActivity;->B:Lcom/android/calendar/alerts/slidingTabView/a;

    .line 628
    invoke-direct {p0, v2}, Lcom/android/calendar/alerts/PopUpActivity;->a(Z)V

    .line 630
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->S:Z

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->ab:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 632
    iput-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->S:Z

    .line 635
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->T:Z

    if-nez v0, :cond_1

    .line 636
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 637
    const-string v1, "com.android.calendar.NEED_UPDATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 641
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->H:Z

    if-eqz v0, :cond_2

    .line 642
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->aa:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 643
    iput-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->H:Z

    .line 646
    :cond_2
    invoke-static {p0}, Lcom/android/calendar/g/e;->a(Landroid/content/Context;)V

    .line 647
    sput-object v3, Lcom/android/calendar/alerts/PopUpActivity;->C:Lcom/android/calendar/alerts/ai;

    .line 649
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 650
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 654
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 655
    invoke-virtual {p0, p1}, Lcom/android/calendar/alerts/PopUpActivity;->setIntent(Landroid/content/Intent;)V

    .line 656
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->k()V

    .line 657
    invoke-virtual {p0}, Lcom/android/calendar/alerts/PopUpActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->b(Landroid/view/Window;)V

    .line 658
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->J:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 659
    invoke-direct {p0}, Lcom/android/calendar/alerts/PopUpActivity;->p()V

    .line 660
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    .line 375
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 376
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 362
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 363
    sput-boolean v2, Lcom/android/calendar/alerts/PopUpActivity;->I:Z

    .line 365
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.app.clockpackage.timer.TIMER_STARTED_IN_ALERT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 366
    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->H:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->F:Z

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/android/calendar/alerts/PopUpActivity;->E:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/android/calendar/alerts/PopUpActivity;->aa:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 368
    iput-boolean v2, p0, Lcom/android/calendar/alerts/PopUpActivity;->H:Z

    .line 370
    :cond_0
    return-void
.end method
