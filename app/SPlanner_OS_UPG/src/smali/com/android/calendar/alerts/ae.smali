.class Lcom/android/calendar/alerts/ae;
.super Landroid/content/AsyncQueryHandler;
.source "PopUpActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/PopUpActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/PopUpActivity;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 832
    iput-object p1, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    .line 833
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 834
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 908
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->h(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 911
    const-string v0, ""

    .line 912
    const-string v0, ""

    .line 915
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 916
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 917
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 918
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 919
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 921
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 924
    new-instance v3, Lcom/android/calendar/at;

    invoke-direct {v3, v1, v2, v0}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 925
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->h(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 927
    :cond_1
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 876
    const/4 v1, 0x0

    .line 877
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 878
    iget-object v3, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v4}, Lcom/android/calendar/alerts/PopUpActivity;->i(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    .line 884
    :cond_1
    if-nez v1, :cond_2

    .line 885
    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t find host information from the attendee list[organizer account : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v2}, Lcom/android/calendar/alerts/PopUpActivity;->i(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    :goto_0
    return-void

    .line 891
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 892
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->l(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 896
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    const v2, 0x7f120060

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 897
    iget-object v2, v1, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v1, v1, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 898
    :goto_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "My calendar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 899
    iget-object v1, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v1}, Lcom/android/calendar/alerts/PopUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 901
    :cond_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 903
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    const v1, 0x7f12005f

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/PopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 904
    const v1, 0x7f020082

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 897
    :cond_5
    iget-object v1, v1, Lcom/android/calendar/at;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 930
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    const-string v1, "com.sec.android.app.sns3.linkedin"

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 931
    const/4 v0, 0x0

    .line 940
    :goto_0
    return v0

    .line 934
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 935
    iget-object v1, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->i(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 938
    const-string v2, "emailList"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 939
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    const-string v2, "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/alerts/PopUpActivity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 940
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 838
    if-nez p3, :cond_1

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 847
    sparse-switch p1, :sswitch_data_0

    .line 869
    :cond_2
    :goto_1
    if-eqz p3, :cond_0

    .line 870
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 849
    :sswitch_0
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 850
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->l(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 869
    if-eqz p3, :cond_0

    .line 870
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 853
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->l(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 854
    invoke-direct {p0, p3}, Lcom/android/calendar/alerts/ae;->a(Landroid/database/Cursor;)V

    .line 855
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->h(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/ae;->a(Ljava/util/ArrayList;)V

    .line 856
    invoke-direct {p0}, Lcom/android/calendar/alerts/ae;->a()Z

    .line 858
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->m(Lcom/android/calendar/alerts/PopUpActivity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 869
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_4

    .line 870
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 861
    :sswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 862
    iget-object v0, p0, Lcom/android/calendar/alerts/ae;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->m(Lcom/android/calendar/alerts/PopUpActivity;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 847
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
