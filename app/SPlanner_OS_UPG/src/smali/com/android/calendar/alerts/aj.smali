.class public Lcom/android/calendar/alerts/aj;
.super Ljava/lang/Object;
.source "SViewCoverUtils.java"


# static fields
.field public static a:I

.field private static b:Lcom/samsung/android/sdk/cover/ScoverManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/alerts/aj;->a:I

    return-void
.end method

.method public static a(Lcom/samsung/android/sdk/cover/ScoverState;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 61
    if-nez p0, :cond_0

    .line 84
    :goto_0
    return v0

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 66
    const/4 v0, 0x3

    sput v0, Lcom/android/calendar/alerts/aj;->a:I

    .line 67
    sget v0, Lcom/android/calendar/alerts/aj;->a:I

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowHeight()I

    move-result v1

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowWidth()I

    move-result v2

    .line 73
    const/16 v3, 0x1aa

    if-ne v1, v3, :cond_2

    const/16 v3, 0x3c0

    if-ne v2, v3, :cond_2

    .line 74
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/alerts/aj;->a:I

    .line 84
    :goto_1
    sget v0, Lcom/android/calendar/alerts/aj;->a:I

    goto :goto_0

    .line 75
    :cond_2
    const/16 v3, 0x3ba

    if-ne v1, v3, :cond_3

    const/16 v3, 0x3ea

    if-eq v2, v3, :cond_7

    :cond_3
    const/16 v3, 0x4fc

    if-ne v1, v3, :cond_4

    const/16 v3, 0x548

    if-eq v2, v3, :cond_7

    :cond_4
    const/16 v3, 0x27e

    if-ne v1, v3, :cond_5

    const/16 v3, 0x2a4

    if-eq v2, v3, :cond_7

    :cond_5
    const/16 v3, 0x22c

    if-ne v1, v3, :cond_6

    const/16 v3, 0x24c

    if-eq v2, v3, :cond_7

    :cond_6
    const/16 v3, 0x484

    if-ne v1, v3, :cond_8

    const/16 v1, 0x514

    if-ne v2, v1, :cond_8

    .line 80
    :cond_7
    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/alerts/aj;->a:I

    goto :goto_1

    .line 82
    :cond_8
    sput v0, Lcom/android/calendar/alerts/aj;->a:I

    goto :goto_1
.end method

.method public static a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 94
    if-eq p0, v0, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x6

    if-eq p0, v1, :cond_0

    if-eqz p0, :cond_0

    const/4 v1, 0x7

    if-ne p0, v1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 105
    sget-object v1, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-nez v1, :cond_0

    .line 106
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 109
    :cond_0
    sget-object v1, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v1

    if-nez v1, :cond_2

    .line 116
    :cond_1
    :goto_0
    return v0

    .line 113
    :cond_2
    sget-object v1, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 114
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Lcom/samsung/android/sdk/cover/ScoverState;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 139
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/aj;->b:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    return-object v0
.end method
