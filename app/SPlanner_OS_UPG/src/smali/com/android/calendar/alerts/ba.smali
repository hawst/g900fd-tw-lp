.class public Lcom/android/calendar/alerts/ba;
.super Ljava/lang/Object;
.source "TorchLightManager.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/android/calendar/alerts/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/ba;->a:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/alerts/ba;->b:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 36
    invoke-static {p0}, Lcom/android/calendar/dz;->p(Landroid/content/Context;)Z

    move-result v0

    .line 39
    invoke-static {p0}, Lcom/android/calendar/dz;->q(Landroid/content/Context;)Z

    move-result v1

    .line 40
    sget-boolean v2, Lcom/android/calendar/alerts/ba;->b:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    new-instance v0, Lcom/android/calendar/alerts/bc;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/alerts/bc;-><init>(Lcom/android/calendar/alerts/bb;)V

    .line 45
    invoke-virtual {v0}, Lcom/android/calendar/alerts/bc;->start()V

    .line 47
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/alerts/ba;->a(Z)V

    .line 48
    sget-object v0, Lcom/android/calendar/alerts/ba;->a:Ljava/lang/String;

    const-string v1, "Turned on the Torch Light"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 32
    sput-boolean p0, Lcom/android/calendar/alerts/ba;->b:Z

    .line 33
    return-void
.end method

.method static synthetic b(Z)V
    .locals 0

    .prologue
    .line 17
    invoke-static {p0}, Lcom/android/calendar/alerts/ba;->c(Z)V

    return-void
.end method

.method private static c(Z)V
    .locals 2

    .prologue
    .line 67
    const-string v1, "CLOCK_SET_TORCH_LIGHT"

    if-eqz p0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-static {v1, v0}, Landroid/os/DVFSHelper;->sendCommandToSsrm(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    return-void

    .line 67
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
