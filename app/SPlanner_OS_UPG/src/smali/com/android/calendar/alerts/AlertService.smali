.class public Lcom/android/calendar/alerts/AlertService;
.super Landroid/app/Service;
.source "AlertService.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static f:Ljava/util/ArrayList;


# instance fields
.field private volatile d:Landroid/os/Looper;

.field private volatile e:Lcom/android/calendar/alerts/t;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    const-class v0, Lcom/android/calendar/alerts/AlertService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    .line 88
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "event_id"

    aput-object v1, v0, v4

    const-string v1, "state"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "alarmTime"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "minutes"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "end"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "organizer"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/AlertService;->b:[Ljava/lang/String;

    .line 121
    new-array v0, v5, [Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/alerts/AlertService;->c:[Ljava/lang/String;

    .line 146
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/alerts/AlertService;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 1047
    return-void
.end method

.method private static a(Landroid/database/Cursor;Landroid/content/Context;JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 34

    .prologue
    .line 644
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    .line 645
    new-instance v28, Landroid/util/LongSparseArray;

    invoke-direct/range {v28 .. v28}, Landroid/util/LongSparseArray;-><init>()V

    .line 646
    const/16 v26, 0x0

    move/from16 v15, v26

    .line 656
    :goto_0
    :try_start_0
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 657
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 658
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 659
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 660
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 661
    const/16 v4, 0xb

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 662
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 663
    const/4 v4, 0x5

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 664
    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    const/4 v4, 0x1

    move/from16 v18, v4

    .line 665
    :goto_1
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 666
    const/16 v4, 0xa

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 667
    sget-object v4, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v20

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v29

    .line 668
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 669
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 670
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_6

    const/16 v23, 0x1

    .line 671
    :goto_2
    const/16 v4, 0xc

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 675
    const/4 v4, 0x0

    .line 676
    sget-boolean v5, Lcom/android/calendar/alerts/u;->c:Z

    if-eqz v5, :cond_16

    sub-long v30, p2, v10

    const-wide/32 v32, 0xea60

    div-long v30, v30, v32

    const-wide/16 v32, 0x1

    cmp-long v5, v30, v32

    if-gez v5, :cond_16

    move-object/from16 v5, p1

    .line 681
    invoke-static/range {v5 .. v11}, Lcom/android/calendar/alerts/u;->b(Landroid/content/Context;JJJ)Z

    move-result v5

    .line 683
    if-nez v5, :cond_16

    .line 684
    const/4 v4, 0x1

    move v5, v4

    .line 688
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 689
    const-string v24, "alertCursor result: alarmTime:"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " alertId:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " eventId:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " state: "

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " minutes:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " declined:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " beginTime:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " endTime:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " allDay:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v26, " organizer:"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    sget-boolean v24, Lcom/android/calendar/alerts/u;->c:Z

    if-eqz v24, :cond_0

    .line 696
    const-string v24, " newAlertOverride: "

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 698
    :cond_0
    sget-object v24, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    new-instance v30, Landroid/content/ContentValues;

    invoke-direct/range {v30 .. v30}, Landroid/content/ContentValues;-><init>()V

    .line 701
    const/4 v4, -0x1

    .line 702
    const/16 v24, 0x0

    .line 710
    if-nez v18, :cond_7

    .line 711
    if-eqz v14, :cond_1

    if-eqz v5, :cond_15

    .line 712
    :cond_1
    const/4 v4, 0x1

    .line 713
    add-int/lit8 v5, v15, 0x1

    .line 714
    const/16 v24, 0x1

    .line 719
    const-string v15, "receivedTime"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move/from16 v26, v5

    .line 726
    :goto_4
    const/4 v5, -0x1

    if-eq v4, v5, :cond_14

    .line 727
    const-string v5, "state"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 730
    sget-boolean v5, Lcom/android/calendar/alerts/u;->c:Z

    if-eqz v5, :cond_2

    move-object/from16 v5, p1

    .line 731
    invoke-static/range {v5 .. v11}, Lcom/android/calendar/alerts/u;->c(Landroid/content/Context;JJJ)V

    .line 735
    :cond_2
    :goto_5
    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 738
    const-string v5, "notifyTime"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 742
    :cond_3
    invoke-virtual/range {v30 .. v30}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 743
    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2, v5, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 746
    :cond_4
    const/4 v5, 0x1

    if-eq v4, v5, :cond_8

    move/from16 v15, v26

    .line 747
    goto/16 :goto_0

    .line 664
    :cond_5
    const/4 v4, 0x0

    move/from16 v18, v4

    goto/16 :goto_1

    .line 670
    :cond_6
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 722
    :cond_7
    const/4 v4, 0x2

    move/from16 v26, v15

    goto :goto_4

    .line 750
    :cond_8
    new-instance v10, Lcom/android/calendar/alerts/p;

    move-object/from16 v11, v19

    move-wide v14, v8

    move-wide/from16 v18, v6

    invoke-direct/range {v10 .. v25}, Lcom/android/calendar/alerts/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJJIZZLjava/lang/String;)V

    .line 756
    const/4 v4, 0x0

    .line 757
    if-eqz v23, :cond_13

    .line 758
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    .line 759
    const/4 v5, 0x0

    invoke-static {v5, v8, v9, v4}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v12

    move-object v11, v4

    move-wide v14, v12

    .line 763
    :goto_6
    move-object/from16 v0, v28

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/alerts/p;

    .line 764
    if-eqz v4, :cond_a

    .line 765
    iget-wide v12, v4, Lcom/android/calendar/alerts/p;->d:J

    .line 766
    if-eqz v23, :cond_9

    .line 767
    const/4 v5, 0x0

    iget-wide v12, v4, Lcom/android/calendar/alerts/p;->d:J

    invoke-static {v5, v12, v13, v11}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v12

    .line 772
    :cond_9
    sub-long v12, v12, p2

    .line 773
    sub-long v18, v14, p2

    .line 775
    const-wide/16 v20, 0x0

    cmp-long v5, v18, v20

    if-gez v5, :cond_c

    const-wide/16 v20, 0x0

    cmp-long v5, v12, v20

    if-lez v5, :cond_c

    .line 777
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    const-wide/32 v18, 0xdbba0

    cmp-long v5, v12, v18

    if-gez v5, :cond_b

    const/4 v5, 0x1

    .line 783
    :goto_7
    if-eqz v5, :cond_17

    .line 792
    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 793
    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 794
    sget-object v5, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Dropping alert for recurring event ID:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v0, v4, Lcom/android/calendar/alerts/p;->f:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", startTime:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v0, v4, Lcom/android/calendar/alerts/p;->d:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, " in favor of startTime:"

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v12, v10, Lcom/android/calendar/alerts/p;->d:J

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    :cond_a
    move-object/from16 v0, v28

    invoke-virtual {v0, v6, v7, v10}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 805
    move-wide/from16 v0, v16

    move/from16 v2, v23

    invoke-static {v8, v9, v0, v1, v2}, Lcom/android/calendar/alerts/AlertService;->a(JJZ)J

    move-result-wide v4

    sub-long v4, p2, v4

    .line 807
    cmp-long v4, v14, v4

    if-lez v4, :cond_e

    .line 809
    move-object/from16 v0, p4

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_8
    move/from16 v15, v26

    .line 816
    goto/16 :goto_0

    .line 777
    :cond_b
    const/4 v5, 0x0

    goto :goto_7

    .line 780
    :cond_c
    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(J)J

    move-result-wide v18

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    cmp-long v5, v18, v12

    if-gez v5, :cond_d

    const/4 v5, 0x1

    goto :goto_7

    :cond_d
    const/4 v5, 0x0

    goto :goto_7

    .line 810
    :cond_e
    if-eqz v23, :cond_10

    if-eqz v11, :cond_10

    invoke-static {v14, v15}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 812
    move-object/from16 v0, p5

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_8

    .line 818
    :catchall_0
    move-exception v4

    if-eqz p0, :cond_f

    .line 819
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v4

    .line 814
    :cond_10
    :try_start_1
    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_8

    .line 818
    :cond_11
    if-eqz p0, :cond_12

    .line 819
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->close()V

    .line 822
    :cond_12
    return v15

    :cond_13
    move-object v11, v4

    move-wide v14, v8

    goto/16 :goto_6

    :cond_14
    move v4, v14

    goto/16 :goto_5

    :cond_15
    move/from16 v26, v15

    goto/16 :goto_4

    :cond_16
    move v5, v4

    goto/16 :goto_3

    :cond_17
    move/from16 v15, v26

    goto/16 :goto_0
.end method

.method private static a(JJZ)J
    .locals 6

    .prologue
    const-wide/32 v0, 0xdbba0

    .line 829
    if-eqz p4, :cond_0

    .line 834
    :goto_0
    return-wide v0

    :cond_0
    sub-long v2, p2, p0

    const-wide/16 v4, 0x4

    div-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Lcom/android/calendar/alerts/p;J)J
    .locals 9

    .prologue
    .line 607
    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->d:J

    .line 608
    iget-wide v0, p0, Lcom/android/calendar/alerts/p;->e:J

    .line 609
    iget-boolean v4, p0, Lcom/android/calendar/alerts/p;->i:Z

    if-eqz v4, :cond_0

    .line 610
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 611
    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->d:J

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v2

    .line 612
    iget-wide v4, p0, Lcom/android/calendar/alerts/p;->d:J

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v4, v5, v1}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;JLjava/lang/String;)J

    move-result-wide v0

    .line 616
    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    .line 617
    iget-boolean v6, p0, Lcom/android/calendar/alerts/p;->i:Z

    invoke-static {v2, v3, v0, v1, v6}, Lcom/android/calendar/alerts/AlertService;->a(JJZ)J

    move-result-wide v6

    add-long/2addr v6, v2

    .line 619
    cmp-long v2, v6, p1

    if-lez v2, :cond_2

    .line 620
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 624
    :goto_0
    cmp-long v4, v0, p1

    if-lez v4, :cond_1

    cmp-long v4, v0, v6

    if-lez v4, :cond_1

    .line 625
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 627
    :goto_1
    return-wide v0

    :cond_1
    move-wide v0, v2

    goto :goto_1

    :cond_2
    move-wide v2, v4

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-static {p0, p1}, Lcom/android/calendar/alerts/AlertService;->b(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 885
    .line 886
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 887
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 889
    :cond_0
    return-object p0
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 839
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 840
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/p;

    .line 841
    iget-object v3, v0, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 842
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 843
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    :cond_1
    iget-object v0, v0, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 848
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 996
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 997
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;)Lcom/android/calendar/alerts/slidingTabView/e;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/ContentResolver;Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;)V

    .line 998
    invoke-static {p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 999
    return-void
.end method

.method private static final a(Landroid/content/ContentResolver;Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 1013
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1014
    const-wide/32 v0, 0x5265c00

    sub-long v8, v6, v0

    .line 1015
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "alarmTime"

    aput-object v0, v2, v5

    .line 1021
    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "state=0 AND alarmTime<? AND alarmTime>? AND end>=?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    const/4 v0, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const-string v5, "alarmTime ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1026
    if-nez v4, :cond_0

    .line 1045
    :goto_0
    return-void

    .line 1030
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "missed alarms found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    const-wide/16 v2, -0x1

    .line 1034
    :goto_1
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1035
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1036
    cmp-long v5, v2, v0

    if-eqz v5, :cond_2

    .line 1037
    sget-object v2, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rescheduling missed alarm. alarmTime: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    invoke-static {p1, p2, v0, v1}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    move-wide v2, v0

    .line 1041
    goto :goto_1

    .line 1043
    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-wide v0, v2

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentResolver;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1116
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1117
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1131
    :cond_0
    :goto_0
    return-void

    .line 1122
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1123
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 1126
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/alerts/TextToSpeechService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1127
    const-string v1, "TTS_TIME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1128
    const-string v1, "TTS_TITLE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    const-string v1, "TTS_LOCATION"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1130
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/android/calendar/alerts/s;ZLjava/lang/String;ZLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 895
    iget-object v2, p1, Lcom/android/calendar/alerts/s;->a:Landroid/app/Notification;

    .line 896
    iget v0, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Landroid/app/Notification;->defaults:I

    .line 897
    const-string v0, "event"

    iput-object v0, v2, Landroid/app/Notification;->category:Ljava/lang/String;

    .line 900
    if-nez p2, :cond_2

    .line 902
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 903
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f02dd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 912
    :goto_0
    if-eqz p4, :cond_0

    .line 913
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "defaultVibrate: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    iget v0, v2, Landroid/app/Notification;->haptic:I

    or-int/lit8 v0, v0, 0x12

    iput v0, v2, Landroid/app/Notification;->haptic:I

    .line 917
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 918
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    invoke-static {p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 920
    :cond_1
    iput-object v1, v2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 925
    :cond_2
    :goto_1
    return-void

    .line 905
    :cond_3
    iput-object p3, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    goto :goto_0

    .line 922
    :cond_4
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, v1

    :goto_2
    iput-object v0, v2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    goto :goto_1

    :cond_5
    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Landroid/os/Message;)V
    .locals 6

    .prologue
    .line 265
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 267
    if-nez v0, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    sget-object v2, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "alarmTime"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    if-eqz v1, :cond_0

    .line 277
    sget-object v2, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "processMessage() received action is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 280
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertService;->a()V

    goto :goto_0

    .line 285
    :cond_3
    const-string v2, "android.intent.action.EVENT_REMINDER"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 286
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/calendar/alerts/ba;->a(Z)V

    .line 289
    :cond_4
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v2, "alarmTime"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_6

    .line 291
    :cond_5
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v1, "uri or alarmTime is null, so exit"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 295
    :cond_6
    const-string v0, "android.intent.action.PROVIDER_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "android.intent.action.EVENT_REMINDER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 297
    :cond_7
    invoke-static {p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    goto/16 :goto_0

    .line 300
    :cond_8
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/AlertService;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/os/Message;)V

    return-void
.end method

.method private static a(Lcom/android/calendar/alerts/p;Landroid/content/Context;I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 1074
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_FULL:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v9

    .line 1075
    const-class v0, Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v9, p1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1076
    const/high16 v0, 0x30800000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1081
    invoke-static {p1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 1082
    iget-boolean v1, p0, Lcom/android/calendar/alerts/p;->i:Z

    if-eqz v1, :cond_1

    .line 1083
    const/16 v1, 0x2012

    .line 1084
    const-string v0, "UTC"

    .line 1088
    :goto_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1089
    or-int/lit16 v6, v1, 0x80

    .line 1092
    :goto_1
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1093
    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->d:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1094
    iget v1, v1, Landroid/text/format/Time;->isDst:I

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    move v7, v1

    .line 1095
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->d:J

    iget-wide v4, p0, Lcom/android/calendar/alerts/p;->e:J

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1096
    iget-boolean v1, p0, Lcom/android/calendar/alerts/p;->i:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1097
    const/16 v1, 0x20

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v7, v8, v2}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1100
    :cond_0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1101
    const-string v1, "event_id"

    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->f:J

    invoke-virtual {v9, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1102
    const-string v1, "alert_id"

    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->g:J

    invoke-virtual {v9, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1103
    const-string v1, "event_title"

    iget-object v2, p0, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1104
    const-string v1, "event_location"

    iget-object v2, p0, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1105
    const-string v1, "event_start_date"

    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1106
    const-string v0, "event_start_time"

    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->d:J

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1107
    const-string v0, "event_end_time"

    iget-wide v2, p0, Lcom/android/calendar/alerts/p;->e:J

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1108
    const-string v0, "notification_id"

    invoke-virtual {v9, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1109
    const-string v0, "allDay"

    iget-boolean v1, p0, Lcom/android/calendar/alerts/p;->i:Z

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1110
    const-string v0, "organizer"

    iget-object v1, p0, Lcom/android/calendar/alerts/p;->k:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1111
    invoke-virtual {p1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1112
    return-void

    .line 1086
    :cond_1
    const v1, 0x8011

    goto/16 :goto_0

    :cond_2
    move v7, v8

    .line 1094
    goto/16 :goto_2

    :cond_3
    move v6, v1

    goto/16 :goto_1
.end method

.method private static a(Lcom/android/calendar/alerts/p;Ljava/lang/String;Landroid/content/Context;ZLcom/android/calendar/alerts/r;Lcom/android/calendar/alerts/q;I)V
    .locals 16

    .prologue
    .line 854
    const/4 v14, 0x0

    .line 860
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 861
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/p;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/alerts/p;->d:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/alerts/p;->e:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/alerts/p;->f:J

    invoke-static/range {p4 .. p4}, Lcom/android/calendar/alerts/r;->a(Lcom/android/calendar/alerts/r;)Z

    move-result v13

    move-object/from16 v2, p2

    move-object/from16 v4, p1

    move/from16 v12, p6

    invoke-static/range {v2 .. v14}, Lcom/android/calendar/alerts/AlertReceiver;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJIZI)Lcom/android/calendar/alerts/s;

    move-result-object v3

    .line 865
    const/4 v4, 0x1

    .line 866
    const-string v7, ""

    .line 867
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/alerts/p;->j:Z

    if-eqz v2, :cond_0

    .line 868
    move-object/from16 v0, p4

    iget-boolean v4, v0, Lcom/android/calendar/alerts/r;->a:Z

    .line 872
    invoke-static/range {p4 .. p4}, Lcom/android/calendar/alerts/r;->c(Lcom/android/calendar/alerts/r;)Ljava/lang/String;

    move-result-object v7

    .line 874
    :cond_0
    invoke-static/range {p4 .. p4}, Lcom/android/calendar/alerts/r;->b(Lcom/android/calendar/alerts/r;)Z

    move-result v6

    move-object/from16 v2, p2

    move-object v5, v15

    invoke-static/range {v2 .. v7}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;Lcom/android/calendar/alerts/s;ZLjava/lang/String;ZLjava/lang/String;)V

    .line 877
    move-object/from16 v0, p5

    move/from16 v1, p6

    invoke-virtual {v0, v1, v3}, Lcom/android/calendar/alerts/q;->a(ILcom/android/calendar/alerts/s;)V

    .line 879
    sget-object v3, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Posting individual alarm notification, eventId:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/alerts/p;->f:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", notificationId:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, ", quiet"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_2

    const-string v2, ", high-priority"

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    return-void

    .line 879
    :cond_1
    const-string v2, ", LOUD"

    goto :goto_0

    :cond_2
    const-string v2, ""

    goto :goto_1
.end method

.method static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 552
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p3, :cond_0

    .line 554
    invoke-virtual {p2, v2, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 557
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, p3

    invoke-virtual {p0, v2, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 560
    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 561
    invoke-static {p1, v0}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/util/List;Ljava/util/List;)V

    .line 562
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 564
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 568
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-le v0, p3, :cond_1

    .line 569
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, p3, v0

    .line 574
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 576
    invoke-virtual {p2, v2, v0}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 577
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/util/List;Ljava/util/List;)V

    .line 580
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 582
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)V
    .locals 6

    .prologue
    const/16 v3, 0x2c

    .line 585
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 586
    if-eqz p0, :cond_0

    .line 587
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/p;

    .line 588
    iget-wide v4, v0, Lcom/android/calendar/alerts/p;->f:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 589
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 592
    :cond_0
    if-eqz p1, :cond_1

    .line 593
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/p;

    .line 594
    iget-wide v4, v0, Lcom/android/calendar/alerts/p;->f:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 595
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 598
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    if-ne v0, v3, :cond_2

    .line 599
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 601
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 602
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reached max postings, bumping event IDs {"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "} to digest."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    :cond_3
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 12

    .prologue
    const/16 v8, 0x14

    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 304
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 305
    new-instance v10, Lcom/android/calendar/alerts/q;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-direct {v10, v1}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 307
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 308
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 309
    sget-object v1, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v3, "Beginning updateAlertNotification"

    invoke-static {v1, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v1, "preferences_alerts"

    invoke-interface {v11, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 312
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v1, "alert preference is OFF"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-virtual {v10}, Lcom/android/calendar/alerts/q;->a()V

    move v0, v2

    .line 333
    :goto_0
    return v0

    .line 319
    :cond_0
    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/alerts/AlertService;->b:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(state=? OR state=?) AND alarmTime<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/android/calendar/alerts/AlertService;->c:[Ljava/lang/String;

    const-string v5, "begin ASC, end ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 323
    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 324
    :cond_1
    if-eqz v5, :cond_2

    .line 325
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 328
    :cond_2
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v1, "No fired or scheduled alerts"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-virtual {v10, v9, v8}, Lcom/android/calendar/alerts/q;->a(II)V

    move v0, v9

    .line 330
    goto :goto_0

    .line 333
    :cond_3
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;)Lcom/android/calendar/alerts/slidingTabView/e;

    move-result-object v3

    move-object v1, p0

    move-object v2, v10

    move-object v4, v11

    invoke-static/range {v1 .. v8}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;Lcom/android/calendar/alerts/q;Lcom/android/calendar/alerts/slidingTabView/e;Landroid/content/SharedPreferences;Landroid/database/Cursor;JI)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/android/calendar/alerts/q;Lcom/android/calendar/alerts/slidingTabView/e;Landroid/content/SharedPreferences;Landroid/database/Cursor;JI)Z
    .locals 35

    .prologue
    .line 348
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "alertCursor count:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p4 .. p4}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 352
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 353
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v4, p4

    move-object/from16 v5, p0

    move-wide/from16 v6, p5

    .line 354
    invoke-static/range {v4 .. v10}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/database/Cursor;Landroid/content/Context;JLjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    move-result v18

    .line 357
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-nez v4, :cond_0

    .line 358
    const/4 v4, 0x1

    .line 538
    :goto_0
    return v4

    .line 361
    :cond_0
    const-wide v6, 0x7fffffffffffffffL

    .line 362
    const/16 v17, 0x1

    .line 363
    new-instance v15, Lcom/android/calendar/alerts/r;

    if-nez v18, :cond_7

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v15, v0, v1, v4}, Lcom/android/calendar/alerts/r;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Z)V

    .line 365
    sget-object v5, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "generateAlerts : "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-nez v18, :cond_8

    const-string v4, "QUIET"

    :goto_2
    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    move/from16 v0, p7

    invoke-static {v8, v9, v10, v0}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 372
    const-string v4, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    .line 373
    invoke-virtual {v4}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v31

    .line 375
    invoke-interface/range {p3 .. p3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 376
    const-string v5, "isTaskAlert"

    const/4 v11, 0x0

    invoke-interface {v4, v5, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 377
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 381
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/dz;->r(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 382
    const/4 v4, 0x1

    iput-boolean v4, v15, Lcom/android/calendar/alerts/r;->a:Z

    .line 383
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v5, "ACCESS_CONTROL_ENABLED ON"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_1
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/ba;->a(Landroid/content/Context;)V

    .line 398
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/calendar/alerts/p;

    .line 400
    iget-wide v12, v11, Lcom/android/calendar/alerts/p;->d:J

    iget-boolean v4, v11, Lcom/android/calendar/alerts/p;->i:Z

    iget-object v8, v11, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v4, v8}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 401
    add-int/lit8 v4, v17, 0x1

    .line 404
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->l(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_b

    invoke-static {v15}, Lcom/android/calendar/alerts/r;->a(Lcom/android/calendar/alerts/r;)Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v14, 0x1

    :goto_5
    move-object/from16 v13, p0

    move-object/from16 v16, p1

    invoke-static/range {v11 .. v17}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;Ljava/lang/String;Landroid/content/Context;ZLcom/android/calendar/alerts/r;Lcom/android/calendar/alerts/q;I)V

    .line 406
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/aj;->a(Landroid/content/Context;)Z

    move-result v8

    .line 407
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_3

    if-eqz v31, :cond_2

    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->c()Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->d(Landroid/content/Context;)Z

    move-result v12

    if-nez v12, :cond_3

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/dz;->r(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_4

    :cond_3
    if-eqz v8, :cond_c

    :cond_4
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->l(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 410
    const-string v8, "ril.cdma.inecmmode"

    const-string v12, "false"

    invoke-static {v8, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v12, "false"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 412
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v11, v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;Landroid/content/Context;I)V

    .line 421
    :cond_5
    :goto_6
    iget-boolean v8, v15, Lcom/android/calendar/alerts/r;->a:Z

    if-nez v8, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->k(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 422
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iget v12, v11, Lcom/android/calendar/alerts/p;->h:I

    iget-object v13, v11, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    iget-object v14, v11, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v8, v12, v13, v14}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;Landroid/content/ContentResolver;ILjava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_6
    move-wide/from16 v0, p5

    invoke-static {v11, v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;J)J

    move-result-wide v12

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    move/from16 v17, v4

    .line 428
    goto/16 :goto_4

    .line 363
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 365
    :cond_8
    const-string v4, "SOUND"

    goto/16 :goto_2

    .line 384
    :cond_9
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->i(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 385
    const/4 v4, 0x1

    iput-boolean v4, v15, Lcom/android/calendar/alerts/r;->a:Z

    .line 386
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v5, "isDriveLinkRunning ON"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 387
    :cond_a
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->j(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 388
    const/4 v4, 0x1

    iput-boolean v4, v15, Lcom/android/calendar/alerts/r;->a:Z

    .line 389
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v5, "isCarModeRunning ON"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 404
    :cond_b
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 415
    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/AlertService;->b(Landroid/content/Context;)V

    .line 418
    iget-wide v12, v11, Lcom/android/calendar/alerts/p;->f:J

    const/4 v8, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v12, v13, v1, v8}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JIZ)V

    goto :goto_6

    .line 435
    :cond_d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-wide/from16 v32, v6

    :goto_7
    if-ltz v4, :cond_11

    .line 436
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/calendar/alerts/p;

    .line 439
    iget-wide v6, v11, Lcom/android/calendar/alerts/p;->d:J

    iget-boolean v5, v11, Lcom/android/calendar/alerts/p;->i:Z

    iget-object v8, v11, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v5, v8}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 440
    add-int/lit8 v5, v17, 0x1

    .line 441
    const/4 v14, 0x0

    move-object/from16 v13, p0

    move-object/from16 v16, p1

    invoke-static/range {v11 .. v17}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;Ljava/lang/String;Landroid/content/Context;ZLcom/android/calendar/alerts/r;Lcom/android/calendar/alerts/q;I)V

    .line 443
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_10

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_10

    if-nez v31, :cond_10

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->d(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_10

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->l(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 445
    const-string v6, "ril.cdma.inecmmode"

    const-string v7, "false"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "false"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 447
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v11, v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;Landroid/content/Context;I)V

    .line 456
    :cond_e
    :goto_8
    iget-boolean v6, v15, Lcom/android/calendar/alerts/r;->a:Z

    if-nez v6, :cond_f

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_f

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_f

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->k(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_f

    .line 457
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget v7, v11, Lcom/android/calendar/alerts/p;->h:I

    iget-object v8, v11, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    iget-object v12, v11, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7, v8, v12}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;Landroid/content/ContentResolver;ILjava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_f
    move-wide/from16 v0, p5

    invoke-static {v11, v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Lcom/android/calendar/alerts/p;J)J

    move-result-wide v6

    move-wide/from16 v0, v32

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 435
    add-int/lit8 v4, v4, -0x1

    move/from16 v17, v5

    move-wide/from16 v32, v6

    goto/16 :goto_7

    .line 450
    :cond_10
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/AlertService;->b(Landroid/content/Context;)V

    .line 453
    iget-wide v6, v11, Lcom/android/calendar/alerts/p;->f:J

    const/4 v8, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v6, v7, v1, v8}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JIZ)V

    goto :goto_8

    .line 465
    :cond_11
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 466
    if-lez v11, :cond_19

    .line 467
    invoke-static {v10}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v7

    .line 469
    const/4 v4, 0x1

    if-ne v11, v4, :cond_13

    .line 471
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/alerts/p;

    .line 472
    iget-wide v8, v4, Lcom/android/calendar/alerts/p;->d:J

    iget-boolean v5, v4, Lcom/android/calendar/alerts/p;->i:Z

    iget-object v6, v4, Lcom/android/calendar/alerts/p;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9, v5, v6}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JZLjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 474
    iget-object v0, v4, Lcom/android/calendar/alerts/p;->a:Ljava/lang/String;

    move-object/from16 v20, v0

    iget-wide v0, v4, Lcom/android/calendar/alerts/p;->d:J

    move-wide/from16 v22, v0

    iget-wide v0, v4, Lcom/android/calendar/alerts/p;->e:J

    move-wide/from16 v24, v0

    iget-wide v0, v4, Lcom/android/calendar/alerts/p;->f:J

    move-wide/from16 v26, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v19, p0

    invoke-static/range {v19 .. v30}, Lcom/android/calendar/alerts/AlertReceiver;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JJJIZI)Lcom/android/calendar/alerts/s;

    move-result-object v5

    .line 484
    :goto_9
    if-eqz v5, :cond_15

    .line 485
    const/4 v4, 0x1

    iput-boolean v4, v15, Lcom/android/calendar/alerts/r;->a:Z

    .line 486
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_12
    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/alerts/p;

    .line 487
    iget-boolean v4, v4, Lcom/android/calendar/alerts/p;->j:Z

    if-eqz v4, :cond_12

    .line 488
    const/4 v4, 0x0

    iput-boolean v4, v15, Lcom/android/calendar/alerts/r;->a:Z

    goto :goto_a

    .line 480
    :cond_13
    move-object/from16 v0, p0

    invoke-static {v0, v10, v7}, Lcom/android/calendar/alerts/AlertReceiver;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/android/calendar/alerts/s;

    move-result-object v5

    goto :goto_9

    .line 491
    :cond_14
    iget-boolean v6, v15, Lcom/android/calendar/alerts/r;->a:Z

    invoke-static {v15}, Lcom/android/calendar/alerts/r;->b(Lcom/android/calendar/alerts/r;)Z

    move-result v8

    invoke-static {v15}, Lcom/android/calendar/alerts/r;->c(Lcom/android/calendar/alerts/r;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v4, p0

    invoke-static/range {v4 .. v9}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;Lcom/android/calendar/alerts/s;ZLjava/lang/String;ZLjava/lang/String;)V

    .line 495
    :cond_15
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Quietly posting digest alarm notification, numEvents:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", notificationId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/alerts/q;->a(ILcom/android/calendar/alerts/s;)V

    .line 506
    :goto_b
    move/from16 v0, v17

    move/from16 v1, p7

    if-gt v0, v1, :cond_16

    .line 507
    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, p7

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 508
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Canceling leftover notification IDs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p7

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_16
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v4, v32, v4

    if-gez v4, :cond_1a

    cmp-long v4, v32, p5

    if-lez v4, :cond_1a

    .line 514
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-wide/from16 v2, v32

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/alerts/u;->b(Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;J)V

    .line 516
    sub-long v4, v32, p5

    const-wide/32 v6, 0xea60

    div-long/2addr v4, v6

    .line 517
    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    .line 518
    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 519
    const-string v7, "Scheduling next notification refresh in %d min at: %d:%02d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x1

    iget v5, v6, Landroid/text/format/Time;->hour:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v4

    const/4 v4, 0x2

    iget v5, v6, Landroid/text/format/Time;->minute:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v8, v4

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 521
    sget-object v5, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_17
    :goto_c
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->c(Landroid/content/Context;)V

    .line 528
    if-lez v18, :cond_1b

    invoke-static {v15}, Lcom/android/calendar/alerts/r;->a(Lcom/android/calendar/alerts/r;)Z

    move-result v4

    if-eqz v4, :cond_1b

    const/4 v4, 0x1

    .line 529
    :goto_d
    sget-object v5, Lcom/android/calendar/hj;->I:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v5

    .line 530
    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->b()Z

    move-result v6

    .line 531
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->h(Landroid/content/Context;)Z

    move-result v7

    .line 532
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/aj;->a(Landroid/content/Context;)Z

    move-result v8

    .line 533
    if-nez v6, :cond_18

    if-eqz v31, :cond_18

    if-nez v8, :cond_18

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/dz;->r(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_18

    if-eqz v4, :cond_18

    if-nez v5, :cond_18

    if-nez v7, :cond_18

    .line 534
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/calendar/alerts/AlertActivity;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 535
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 536
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 538
    :cond_18
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 501
    :cond_19
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/calendar/alerts/q;->a(I)V

    .line 502
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v5, "No low priority events, canceling the digest notification."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 522
    :cond_1a
    cmp-long v4, v32, p5

    if-gez v4, :cond_17

    .line 523
    sget-object v4, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v5, "Illegal state: next notification refresh time found to be in the past."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 528
    :cond_1b
    const/4 v4, 0x0

    goto :goto_d
.end method

.method static synthetic a(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 84
    invoke-static {p0, p1}, Lcom/android/calendar/alerts/AlertService;->b(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 1135
    .line 1137
    invoke-static {}, Lcom/android/calendar/dz;->h()Ljava/lang/String;

    move-result-object v3

    .line 1138
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "title LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1141
    if-eqz v0, :cond_1

    .line 1142
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1143
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v8, :cond_0

    .line 1144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1145
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "preferences_alerts_ringtone"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1147
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1150
    :cond_1
    return-object v4
.end method

.method private static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 338
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 339
    const-string v1, "com.android.calendar.CALENDAR_ALARM_FOR_BTSPK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    const-string v1, "action"

    sget-object v2, Lcom/android/calendar/alerts/ad;->d:Lcom/android/calendar/alerts/ad;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 341
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 342
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const-string v1, "Sent broadcast action : com.android.calendar.CALENDAR_ALARM_FOR_BTSPK"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void
.end method

.method private static b(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1154
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1185
    :cond_0
    :goto_0
    return v6

    .line 1158
    :cond_1
    const-string v0, "content://settings/system/notification_sound"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1162
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1163
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 1165
    const-string v0, "external"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1166
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1170
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    const-string v5, "is_notification"

    aput-object v5, v2, v6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1175
    if-nez v1, :cond_3

    move v6, v7

    .line 1176
    goto :goto_0

    .line 1168
    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 1178
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1179
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v6, :cond_4

    .line 1180
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v7

    .line 1181
    goto :goto_0

    .line 1183
    :cond_4
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    .line 1184
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v0

    .line 1185
    goto :goto_0

    :cond_5
    move v0, v7

    .line 1183
    goto :goto_2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1069
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 237
    new-instance v0, Landroid/os/HandlerThread;

    sget-object v1, Lcom/android/calendar/alerts/AlertService;->a:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 238
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 240
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertService;->d:Landroid/os/Looper;

    .line 241
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService;->d:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 242
    new-instance v0, Lcom/android/calendar/alerts/t;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertService;->d:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/t;-><init>(Lcom/android/calendar/alerts/AlertService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertService;->e:Lcom/android/calendar/alerts/t;

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/u;->c(Landroid/content/Context;)V

    .line 247
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService;->d:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1064
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1065
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 251
    if-eqz p1, :cond_0

    .line 252
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertService;->e:Lcom/android/calendar/alerts/t;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/t;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 253
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 254
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 255
    iget-object v1, p0, Lcom/android/calendar/alerts/AlertService;->e:Lcom/android/calendar/alerts/t;

    invoke-virtual {v1, v0}, Lcom/android/calendar/alerts/t;->sendMessage(Landroid/os/Message;)Z

    .line 258
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/AlertService;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 259
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/calendar/alerts/AlertService;->f:Ljava/util/ArrayList;

    .line 261
    :cond_1
    const/4 v0, 0x3

    return v0
.end method
