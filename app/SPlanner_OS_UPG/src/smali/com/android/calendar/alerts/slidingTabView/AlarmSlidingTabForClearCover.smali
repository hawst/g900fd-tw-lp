.class public Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;
.super Lcom/android/calendar/alerts/slidingTabView/a;
.source "AlarmSlidingTabForClearCover.java"


# static fields
.field public static z:I


# instance fields
.field private final A:Landroid/content/Context;

.field public y:Lcom/samsung/android/sdk/cover/ScoverState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/alerts/slidingTabView/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->A:Landroid/content/Context;

    .line 51
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 100
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 4

    .prologue
    const/4 v2, -0x2

    .line 109
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 110
    const v1, 0x7f0200c9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 111
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 112
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 116
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->y:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-static {v1}, Lcom/android/calendar/alerts/aj;->a(Lcom/samsung/android/sdk/cover/ScoverState;)I

    move-result v1

    .line 117
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    .line 120
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    .line 121
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    .line 122
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    .line 123
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    sub-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    .line 144
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->invalidate()V

    .line 145
    return-void

    .line 124
    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    .line 127
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    .line 128
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    .line 129
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    .line 131
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    sub-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x3

    sub-int v0, v1, v0

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    goto :goto_0

    .line 132
    :cond_1
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    .line 135
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c023e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    .line 136
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c023f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    .line 137
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0240

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    .line 139
    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    sub-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x6

    sub-int v0, v1, v0

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    goto/16 :goto_0

    .line 141
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "You are not defined the dimensions of the cover version : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected b()V
    .locals 10

    .prologue
    const v6, 0x7f050004

    const v0, 0x7f050003

    const v9, 0x7f0200ca

    const v3, 0x7f0200c7

    .line 61
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->A:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/alerts/aj;->b(Landroid/content/Context;)Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->y:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 62
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->y:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-static {v1}, Lcom/android/calendar/alerts/aj;->a(Lcom/samsung/android/sdk/cover/ScoverState;)I

    move-result v1

    .line 65
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 66
    const v6, 0x7f050002

    .line 67
    const v0, 0x7f050001

    move v8, v0

    .line 81
    :goto_0
    new-instance v0, Lcom/android/calendar/alerts/slidingTabView/f;

    const v5, 0x7f0200c8

    sget-object v7, Lcom/android/calendar/alerts/slidingTabView/i;->a:Lcom/android/calendar/alerts/slidingTabView/i;

    move-object v1, p0

    move-object v2, p0

    move v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/alerts/slidingTabView/f;-><init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;Landroid/view/ViewGroup;IIIILcom/android/calendar/alerts/slidingTabView/i;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 89
    new-instance v0, Lcom/android/calendar/alerts/slidingTabView/f;

    const v5, 0x7f0200cb

    sget-object v7, Lcom/android/calendar/alerts/slidingTabView/i;->b:Lcom/android/calendar/alerts/slidingTabView/i;

    move-object v1, p0

    move-object v2, p0

    move v3, v9

    move v4, v9

    move v6, v8

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/alerts/slidingTabView/f;-><init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;Landroid/view/ViewGroup;IIIILcom/android/calendar/alerts/slidingTabView/i;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 96
    return-void

    .line 68
    :cond_0
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    move v8, v0

    .line 70
    goto :goto_0

    .line 71
    :cond_1
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    move v8, v0

    .line 73
    goto :goto_0

    .line 77
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Arrow animation\'s id is not defined. Please, define the arrow animation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setCoverState(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->y:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 55
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->y:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-static {v0}, Lcom/android/calendar/alerts/aj;->a(Lcom/samsung/android/sdk/cover/ScoverState;)I

    move-result v0

    sput v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    .line 56
    return-void
.end method
