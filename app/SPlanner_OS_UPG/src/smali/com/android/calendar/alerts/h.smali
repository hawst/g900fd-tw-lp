.class public Lcom/android/calendar/alerts/h;
.super Landroid/app/DialogFragment;
.source "AlertActivity.java"


# static fields
.field private static a:Lcom/android/calendar/alerts/h;

.field private static b:I


# instance fields
.field private final c:Landroid/content/DialogInterface$OnClickListener;

.field private final d:Landroid/content/DialogInterface$OnClickListener;

.field private final e:Landroid/content/DialogInterface$OnClickListener;

.field private final f:Landroid/content/DialogInterface$OnKeyListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 485
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/alerts/h;->a:Lcom/android/calendar/alerts/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 482
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 522
    new-instance v0, Lcom/android/calendar/alerts/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/i;-><init>(Lcom/android/calendar/alerts/h;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/h;->c:Landroid/content/DialogInterface$OnClickListener;

    .line 530
    new-instance v0, Lcom/android/calendar/alerts/j;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/j;-><init>(Lcom/android/calendar/alerts/h;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/h;->d:Landroid/content/DialogInterface$OnClickListener;

    .line 539
    new-instance v0, Lcom/android/calendar/alerts/k;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/k;-><init>(Lcom/android/calendar/alerts/h;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/h;->e:Landroid/content/DialogInterface$OnClickListener;

    .line 546
    new-instance v0, Lcom/android/calendar/alerts/l;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/l;-><init>(Lcom/android/calendar/alerts/h;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/h;->f:Landroid/content/DialogInterface$OnKeyListener;

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 482
    sput p0, Lcom/android/calendar/alerts/h;->b:I

    return p0
.end method

.method public static a()Lcom/android/calendar/alerts/h;
    .locals 1

    .prologue
    .line 489
    sget-object v0, Lcom/android/calendar/alerts/h;->a:Lcom/android/calendar/alerts/h;

    if-nez v0, :cond_0

    .line 490
    new-instance v0, Lcom/android/calendar/alerts/h;

    invoke-direct {v0}, Lcom/android/calendar/alerts/h;-><init>()V

    sput-object v0, Lcom/android/calendar/alerts/h;->a:Lcom/android/calendar/alerts/h;

    .line 492
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/h;->a:Lcom/android/calendar/alerts/h;

    return-object v0
.end method

.method static synthetic b()I
    .locals 1

    .prologue
    .line 482
    sget v0, Lcom/android/calendar/alerts/h;->b:I

    return v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 503
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/alerts/h;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 504
    invoke-virtual {p0}, Lcom/android/calendar/alerts/h;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 505
    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 507
    if-eqz p1, :cond_0

    .line 508
    const-string v2, "bundle_key_snooze_duration"

    sget v3, Lcom/android/calendar/alerts/h;->b:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/h;->b:I

    .line 512
    :goto_0
    const v2, 0x7f0f03d6

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 513
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 514
    sget v2, Lcom/android/calendar/alerts/h;->b:I

    iget-object v3, p0, Lcom/android/calendar/alerts/h;->c:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 515
    const v1, 0x7f0f02e3

    iget-object v2, p0, Lcom/android/calendar/alerts/h;->d:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 516
    const v1, 0x7f0f00a4

    iget-object v2, p0, Lcom/android/calendar/alerts/h;->e:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 517
    iget-object v1, p0, Lcom/android/calendar/alerts/h;->f:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 519
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 510
    :cond_0
    invoke-static {}, Lcom/android/calendar/alerts/AlertActivity;->e()I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/h;->b:I

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 497
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 498
    const-string v0, "bundle_key_snooze_duration"

    sget v1, Lcom/android/calendar/alerts/h;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 499
    return-void
.end method
