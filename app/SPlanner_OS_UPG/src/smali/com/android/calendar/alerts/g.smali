.class Lcom/android/calendar/alerts/g;
.super Lcom/android/calendar/ag;
.source "AlertActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/AlertActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/AlertActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    .line 151
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 152
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v0, v0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 246
    :goto_0
    return-void

    .line 236
    :cond_0
    new-instance v0, Lcom/android/calendar/g/a/a;

    invoke-direct {v0}, Lcom/android/calendar/g/a/a;-><init>()V

    .line 238
    const/4 v1, -0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 239
    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 240
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 241
    iget-object v1, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v1, v1, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/g/a/a;->b(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v1, v1, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v1, v2, v3}, Lcom/android/calendar/g/a/a;->c(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/android/calendar/g/a/a;->a(JZ)V

    goto :goto_1

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    iput-object v0, v1, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    goto :goto_0
.end method


# virtual methods
.method protected a(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 183
    packed-switch p1, :pswitch_data_0

    .line 193
    :goto_0
    return-void

    .line 185
    :pswitch_0
    new-instance v1, Lcom/android/calendar/alerts/q;

    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/AlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 187
    const/4 v0, 0x0

    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 188
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method

.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 156
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 158
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    .line 179
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 166
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 168
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/alerts/AlertActivity;->a(Lcom/android/calendar/alerts/AlertActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 169
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertActivity;->a(Lcom/android/calendar/alerts/AlertActivity;)Lcom/android/calendar/alerts/m;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/calendar/alerts/m;->changeCursor(Landroid/database/Cursor;)V

    .line 172
    invoke-direct {p0, p3}, Lcom/android/calendar/alerts/g;->a(Landroid/database/Cursor;)V

    .line 173
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->b()V

    .line 174
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->d()V

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method protected a(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 201
    if-nez p3, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 207
    :pswitch_0
    check-cast p2, Ljava/lang/Long;

    .line 208
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    iget-object v1, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;)Lcom/android/calendar/alerts/slidingTabView/e;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;J)V

    .line 217
    iget-object v0, p0, Lcom/android/calendar/alerts/g;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
    .end packed-switch
.end method
