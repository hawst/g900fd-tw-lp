.class Lcom/android/calendar/alerts/av;
.super Ljava/lang/Object;
.source "TaskAlertAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/au;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/au;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/android/calendar/alerts/av;->a:Lcom/android/calendar/alerts/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 148
    invoke-static {}, Lcom/android/calendar/alerts/au;->a()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/TaskAlertActivity;

    .line 149
    const v1, 0x7f120067

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 150
    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 151
    if-eqz v2, :cond_0

    .line 152
    iget-object v3, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    :goto_0
    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->a()V

    .line 158
    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    .line 159
    return-void

    .line 155
    :cond_0
    iget-object v2, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
