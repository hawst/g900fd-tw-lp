.class Lcom/android/calendar/alerts/slidingTabView/g;
.super Lcom/android/calendar/alerts/slidingTabView/c;
.source "AlarmSlidingTabForPopup.java"


# instance fields
.field final synthetic h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

.field private final i:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;)V
    .locals 9

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    .line 88
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/alerts/slidingTabView/c;-><init>(Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;Z)V

    .line 90
    invoke-virtual {p1}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->i:I

    .line 92
    return-void
.end method


# virtual methods
.method protected a(IIIIZ)V
    .locals 21

    .prologue
    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/g;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 139
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 140
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 142
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/g;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 143
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 144
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 146
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 147
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    .line 148
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    .line 150
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/alerts/slidingTabView/g;->c:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 151
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    .line 152
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    .line 156
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    iget v11, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 157
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    iget v12, v12, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 159
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/slidingTabView/g;->d:Landroid/widget/ImageView;

    invoke-virtual {v13}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 160
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v14

    .line 161
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v13

    .line 163
    div-int/lit8 v15, v4, 0x2

    .line 164
    div-int/lit8 v16, v3, 0x2

    .line 165
    div-int/lit8 v6, v6, 0x2

    .line 166
    div-int/lit8 v5, v5, 0x2

    .line 167
    div-int/lit8 v8, v8, 0x2

    .line 168
    div-int/lit8 v7, v7, 0x2

    .line 169
    div-int/lit8 v10, v10, 0x2

    .line 170
    div-int/lit8 v9, v9, 0x2

    .line 171
    div-int/lit8 v11, v11, 0x2

    .line 172
    div-int/lit8 v12, v12, 0x2

    .line 175
    if-eqz p5, :cond_0

    .line 176
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    iget v4, v3, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->b:I

    .line 177
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    iget v3, v3, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->c:I

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->d:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->b:I

    move/from16 v18, v0

    add-int v18, v18, p1

    add-int v18, v18, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->i:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->c:I

    move/from16 v19, v0

    add-int v19, v19, p2

    sub-int v13, v19, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->b:I

    move/from16 v19, v0

    add-int v19, v19, p1

    add-int v19, v19, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->i:I

    move/from16 v20, v0

    add-int v19, v19, v20

    add-int v14, v14, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->c:I

    move/from16 v19, v0

    add-int v19, v19, p2

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v13, v14, v2}, Landroid/widget/ImageView;->layout(IIII)V

    .line 194
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/slidingTabView/g;->a:Landroid/widget/ImageView;

    add-int v14, p1, v4

    sub-int/2addr v14, v15

    add-int v17, p2, v3

    sub-int v17, v17, v16

    add-int v18, p1, v4

    add-int v15, v15, v18

    add-int v18, p2, v3

    add-int v16, v16, v18

    move/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v13, v14, v0, v15, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 200
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/slidingTabView/g;->b:Landroid/widget/ImageView;

    add-int v14, p1, v4

    sub-int/2addr v14, v6

    add-int v15, p2, v3

    sub-int/2addr v15, v5

    add-int v16, p1, v4

    add-int v6, v6, v16

    add-int v16, p2, v3

    add-int v5, v5, v16

    invoke-virtual {v13, v14, v15, v6, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    add-int v6, p1, v4

    sub-int/2addr v6, v8

    add-int v13, p2, v3

    sub-int/2addr v13, v7

    add-int v14, p1, v4

    add-int/2addr v8, v14

    add-int v14, p2, v3

    add-int/2addr v7, v14

    invoke-virtual {v5, v6, v13, v8, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 212
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    add-int v6, p1, v4

    sub-int/2addr v6, v11

    add-int v7, p2, v3

    sub-int/2addr v7, v12

    add-int v8, p1, v4

    add-int/2addr v8, v11

    add-int v11, p2, v3

    add-int/2addr v11, v12

    invoke-virtual {v5, v6, v7, v8, v11}, Landroid/widget/ImageView;->layout(IIII)V

    .line 218
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/g;->c:Landroid/widget/ImageView;

    add-int v6, p1, v4

    sub-int/2addr v6, v10

    add-int v7, p2, v3

    sub-int/2addr v7, v9

    add-int v4, v4, p1

    add-int/2addr v4, v10

    add-int v3, v3, p2

    add-int/2addr v3, v9

    invoke-virtual {v5, v6, v7, v4, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 223
    return-void

    .line 185
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    iget v4, v3, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->d:I

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    iget v3, v3, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->e:I

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->d:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->d:I

    move/from16 v18, v0

    add-int v18, v18, p1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->i:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    sub-int v14, v18, v14

    sub-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->e:I

    move/from16 v18, v0

    add-int v18, v18, p2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->d:I

    move/from16 v19, v0

    add-int v19, v19, p1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->i:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    sub-int v19, v19, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/g;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->e:I

    move/from16 v20, v0

    add-int v20, v20, p2

    add-int v13, v13, v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v14, v1, v2, v13}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-super {p0}, Lcom/android/calendar/alerts/slidingTabView/c;->d()V

    .line 105
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 106
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 107
    return-void
.end method

.method protected d(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 111
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 116
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 118
    if-eqz p1, :cond_2

    .line 119
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->c:Landroid/widget/ImageView;

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 129
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 131
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 132
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 133
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/g;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
