.class Lcom/android/calendar/alerts/b;
.super Ljava/lang/Object;
.source "AlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/AlertActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/AlertActivity;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    .prologue
    .line 277
    iget-object v0, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0, p2}, Lcom/android/calendar/alerts/AlertActivity;->a(Landroid/view/View;)Landroid/database/Cursor;

    move-result-object v0

    .line 278
    if-nez v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    .line 301
    :goto_0
    return-void

    .line 283
    :cond_0
    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    .line 284
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 285
    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 288
    iget-object v1, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static/range {v1 .. v7}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v0

    .line 290
    invoke-static {}, Lcom/android/calendar/hj;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v1}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    .line 292
    const-class v2, Lcom/android/calendar/detail/EventInfoActivity;

    invoke-virtual {v1, v2}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    .line 293
    invoke-virtual {v1, v0}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/TaskStackBuilder;->startActivities()V

    .line 298
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertActivity;->b(Lcom/android/calendar/alerts/AlertActivity;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 299
    iget-object v2, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-static {v2, v0, v1}, Lcom/android/calendar/alerts/AlertActivity;->a(Lcom/android/calendar/alerts/AlertActivity;J)V

    .line 300
    iget-object v0, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    goto :goto_0

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/alerts/b;->a:Lcom/android/calendar/alerts/AlertActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/alerts/AlertActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
