.class Lcom/android/calendar/alerts/z;
.super Landroid/content/BroadcastReceiver;
.source "PopUpActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/PopUpActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 242
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 243
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 244
    const-string v2, "com.samsung.bluetooth.action.STOP_SCHEDULE_ALARM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 246
    if-eqz v1, :cond_0

    const-string v0, "bDismiss"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    const-string v0, "bDismiss"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 248
    iget-object v1, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    iget-object v2, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v2}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v4}, Lcom/android/calendar/alerts/PopUpActivity;->b(Lcom/android/calendar/alerts/PopUpActivity;)Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;JZ)V

    .line 249
    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->c(Lcom/android/calendar/alerts/PopUpActivity;)V

    .line 254
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->a()V

    .line 255
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->finish()V

    .line 271
    :cond_0
    :goto_1
    return-void

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->e(Lcom/android/calendar/alerts/PopUpActivity;)V

    goto :goto_0

    .line 257
    :cond_2
    const-string v1, "com.android.calendar.FINISH_POPUP_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 258
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0, v3}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;Z)Z

    .line 259
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->a()V

    .line 260
    invoke-static {p1}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 261
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->finish()V

    goto :goto_1

    .line 262
    :cond_3
    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "email IN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->h(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/hj;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 264
    new-array v6, v3, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->i(Lcom/android/calendar/alerts/PopUpActivity;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 267
    iget-object v0, p0, Lcom/android/calendar/alerts/z;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->j(Lcom/android/calendar/alerts/PopUpActivity;)Lcom/android/calendar/alerts/ae;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/detail/bp;->k:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/bp;->j:[Ljava/lang/String;

    const-string v7, "formatted_name ASC, email ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/alerts/ae;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
