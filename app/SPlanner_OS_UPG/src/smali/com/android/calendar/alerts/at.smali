.class Lcom/android/calendar/alerts/at;
.super Landroid/content/AsyncQueryHandler;
.source "TaskAlertActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/TaskAlertActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    .line 180
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 181
    return-void
.end method


# virtual methods
.method protected onInsertComplete(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 218
    if-eqz p3, :cond_0

    .line 219
    check-cast p2, Ljava/lang/Long;

    .line 222
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/TaskAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 224
    iget-object v1, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v0, v2, v3}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Landroid/content/Context;Landroid/app/AlarmManager;J)V

    .line 227
    :cond_0
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0, p3}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 189
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/ContentObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/ContentObserver;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c(Lcom/android/calendar/alerts/TaskAlertActivity;)Lcom/android/calendar/alerts/au;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/android/calendar/alerts/au;->changeCursor(Landroid/database/Cursor;)V

    .line 193
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->d(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 194
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    .line 196
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    iget v0, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    if-nez v0, :cond_1

    .line 197
    new-instance v1, Lcom/android/calendar/alerts/q;

    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/TaskAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 199
    const/16 v0, 0x64

    const/16 v2, 0x78

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 202
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->e(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 203
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->f(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 204
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    .line 214
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->a()V

    .line 208
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    goto :goto_0

    .line 212
    :cond_2
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method protected onUpdateComplete(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 231
    packed-switch p1, :pswitch_data_0

    .line 243
    :goto_0
    return-void

    .line 233
    :pswitch_0
    new-instance v1, Lcom/android/calendar/alerts/q;

    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/TaskAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 235
    const/16 v0, 0x64

    const/16 v2, 0x78

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 238
    iget-object v0, p0, Lcom/android/calendar/alerts/at;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;)Z

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method
