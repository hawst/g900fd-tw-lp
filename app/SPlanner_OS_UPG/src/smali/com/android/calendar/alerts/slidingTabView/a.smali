.class public abstract Lcom/android/calendar/alerts/slidingTabView/a;
.super Landroid/view/ViewGroup;
.source "AbstractAlarmSlidingTab.java"


# instance fields
.field protected a:I

.field protected b:I

.field protected c:I

.field protected d:I

.field protected e:I

.field protected f:Lcom/android/calendar/alerts/slidingTabView/h;

.field protected g:I

.field protected h:Z

.field protected i:Landroid/os/Vibrator;

.field protected j:Z

.field protected k:I

.field protected l:I

.field protected m:Lcom/android/calendar/alerts/slidingTabView/c;

.field protected n:Lcom/android/calendar/alerts/slidingTabView/c;

.field protected o:Lcom/android/calendar/alerts/slidingTabView/c;

.field protected p:Z

.field protected final q:F

.field protected r:Lcom/android/calendar/alerts/slidingTabView/c;

.field protected s:Z

.field protected t:Landroid/graphics/Rect;

.field protected u:Z

.field protected v:I

.field protected w:I

.field protected x:Lcom/android/calendar/alerts/slidingTabView/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/alerts/slidingTabView/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/alerts/slidingTabView/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->b:I

    .line 45
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->c:I

    .line 46
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->d:I

    .line 47
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->e:I

    .line 50
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->g:I

    .line 51
    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 53
    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->j:Z

    .line 54
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->k:I

    .line 65
    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->u:Z

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    .line 80
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->b()V

    .line 81
    invoke-static {}, Lcom/android/calendar/dz;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->a()V

    .line 84
    :cond_0
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 86
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f0f0164

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    const v2, 0x7f0f03f4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->q:F

    .line 91
    return-void
.end method

.method private static a(FFFF)D
    .locals 3

    .prologue
    .line 215
    sub-float v0, p0, p2

    sub-float v1, p0, p2

    mul-float/2addr v0, v1

    sub-float v1, p1, p3

    sub-float v2, p1, p3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(FF)V
    .locals 0

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->invalidate()V

    .line 318
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 391
    const-wide/16 v0, 0x28

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(J)V

    .line 392
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->f:Lcom/android/calendar/alerts/slidingTabView/h;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->f:Lcom/android/calendar/alerts/slidingTabView/h;

    invoke-interface {v0, p0, p1}, Lcom/android/calendar/alerts/slidingTabView/h;->b(Landroid/view/View;I)V

    .line 395
    :cond_0
    return-void
.end method

.method private declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 370
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->i:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 371
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->i:Landroid/os/Vibrator;

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->i:Landroid/os/Vibrator;

    invoke-virtual {v0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    monitor-exit p0

    return-void

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(FFLandroid/view/View;)Z
    .locals 2

    .prologue
    const/high16 v1, -0x3db80000    # -50.0f

    .line 300
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    cmpl-float v0, p2, v1

    if-lez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x32

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-ltz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->d()Z

    move-result v0

    if-nez v0, :cond_2

    cmpl-float v0, p1, v1

    if-lez v0, :cond_2

    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x32

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 180
    .line 181
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->j(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    .line 181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->l:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setGrabbedState(I)V
    .locals 2

    .prologue
    .line 402
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->g:I

    if-eq p1, v0, :cond_1

    .line 403
    iput p1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->g:I

    .line 404
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->f:Lcom/android/calendar/alerts/slidingTabView/h;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->f:Lcom/android/calendar/alerts/slidingTabView/h;

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->g:I

    invoke-interface {v0, p0, v1}, Lcom/android/calendar/alerts/slidingTabView/h;->a(Landroid/view/View;I)V

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->j(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    if-eqz v0, :cond_1

    .line 409
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->g:I

    packed-switch v0, :pswitch_data_0

    .line 423
    :cond_1
    :goto_0
    return-void

    .line 411
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/b;->a(Z)V

    goto :goto_0

    .line 414
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/b;->a(Z)V

    goto :goto_0

    .line 417
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    invoke-interface {v0}, Lcom/android/calendar/alerts/slidingTabView/b;->a()V

    goto :goto_0

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 609
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    if-nez v0, :cond_1

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    .line 613
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    goto :goto_0
.end method

.method public abstract a(Landroid/graphics/Point;)V
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 194
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, p1}, Lcom/android/calendar/alerts/slidingTabView/c;->b(Z)V

    .line 195
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, p1}, Lcom/android/calendar/alerts/slidingTabView/c;->b(Z)V

    .line 197
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 198
    if-nez p1, :cond_0

    .line 199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->s:Z

    .line 201
    :cond_0
    return-void
.end method

.method protected abstract b()V
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 433
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 434
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 435
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-eqz v0, :cond_1

    .line 436
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 437
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 438
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 440
    packed-switch v0, :pswitch_data_0

    .line 503
    :cond_1
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-nez v0, :cond_2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v3, v2

    :cond_3
    move v2, v3

    .line 505
    :cond_4
    return v2

    .line 442
    :pswitch_1
    invoke-direct {p0, v5, v6, p0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    invoke-direct {p0, v5, v6}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FF)V

    .line 447
    iget v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->q:F

    .line 448
    const-wide/16 v0, 0x0

    .line 450
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v8, v9, :cond_6

    .line 451
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->b:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->c:I

    int-to-float v1, v1

    invoke-static {v5, v6, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFFF)D

    move-result-wide v0

    .line 456
    :cond_5
    :goto_1
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    double-to-int v9, v0

    mul-int/lit8 v9, v9, 0x2

    invoke-virtual {v8, v9}, Lcom/android/calendar/alerts/slidingTabView/c;->a(I)V

    .line 458
    float-to-double v8, v7

    cmpg-double v0, v8, v0

    if-gez v0, :cond_b

    move v0, v2

    .line 462
    :goto_2
    iget-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    if-nez v1, :cond_9

    if-eqz v0, :cond_9

    .line 463
    iput-boolean v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 464
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 465
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v4}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 466
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v0, v1, :cond_7

    move v0, v2

    .line 467
    :goto_3
    if-eqz v0, :cond_8

    move v0, v2

    :goto_4
    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    .line 469
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->c()V

    .line 470
    invoke-direct {p0, v3}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    goto :goto_0

    .line 452
    :cond_6
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v8, v9, :cond_5

    .line 453
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->d:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->e:I

    int-to-float v1, v1

    invoke-static {v5, v6, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFFF)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    goto :goto_1

    :cond_7
    move v0, v3

    .line 466
    goto :goto_3

    :cond_8
    move v0, v4

    .line 467
    goto :goto_4

    .line 472
    :cond_9
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 473
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v1, v1, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 475
    float-to-int v1, v5

    float-to-int v4, v6

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_a

    .line 476
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 477
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->f()V

    goto/16 :goto_0

    .line 479
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->e()I

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    goto/16 :goto_0

    .line 487
    :pswitch_2
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 488
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 489
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 491
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->b()V

    .line 492
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->d(Z)V

    .line 493
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    .line 494
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 495
    iput-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 496
    iput-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 497
    invoke-direct {p0, v3}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto/16 :goto_2

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x1e

    const/4 v11, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 511
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "accessibility"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 512
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 513
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 514
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 515
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 517
    iget-boolean v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->s:Z

    if-eqz v5, :cond_1

    move v0, v2

    .line 604
    :goto_0
    return v0

    .line 521
    :cond_1
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v5, v5, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    .line 522
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 523
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/lit8 v6, v6, -0x3c

    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/lit8 v7, v7, -0x3c

    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v8, v8, 0x3c

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v9, v9, 0x3c

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 525
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    .line 527
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v6, v6, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    .line 528
    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 529
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v7, v7, -0x3c

    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    add-int/lit8 v8, v8, -0x3c

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v9, 0x3c

    iget-object v10, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, 0x3c

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 531
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {v6, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 533
    iget-boolean v4, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-nez v4, :cond_2

    if-nez v5, :cond_2

    if-nez v3, :cond_2

    move v0, v2

    .line 534
    goto :goto_0

    .line 537
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 601
    :cond_3
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 539
    :pswitch_1
    iput-boolean v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->j:Z

    .line 540
    iput v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->k:I

    .line 541
    iput-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 542
    iput-boolean v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 543
    invoke-direct {p0, v12, v13}, Lcom/android/calendar/alerts/slidingTabView/a;->a(J)V

    .line 544
    if-eqz v5, :cond_5

    .line 545
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 546
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 547
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    .line 548
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 549
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    .line 560
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 561
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->d()V

    .line 563
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->f()V

    .line 564
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a()V

    goto :goto_1

    .line 552
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 553
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 554
    invoke-direct {p0, v11}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    .line 555
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 556
    invoke-direct {p0, v11}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    goto :goto_2

    .line 567
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    if-eqz v0, :cond_3

    .line 568
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    goto :goto_1

    .line 572
    :pswitch_3
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->j:Z

    if-nez v0, :cond_6

    .line 573
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->k:I

    .line 575
    :cond_6
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->j:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->k:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_3

    .line 576
    iput-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->j:Z

    .line 577
    iput-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 578
    iput-boolean v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 579
    invoke-direct {p0, v12, v13}, Lcom/android/calendar/alerts/slidingTabView/a;->a(J)V

    .line 580
    if-eqz v5, :cond_7

    .line 581
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 582
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 583
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    .line 590
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 591
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->d()V

    .line 593
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->f()V

    .line 594
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a()V

    goto/16 :goto_1

    .line 585
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 586
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 587
    invoke-direct {p0, v11}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    goto :goto_3

    :cond_8
    move v0, v1

    .line 604
    goto/16 :goto_0

    .line 537
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 116
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 117
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 119
    iget-boolean v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->s:Z

    if-eqz v5, :cond_1

    .line 176
    :cond_0
    :goto_0
    return v0

    .line 123
    :cond_1
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v5, v5, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    .line 124
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 125
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/lit8 v6, v6, -0x3c

    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/lit8 v7, v7, -0x3c

    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    add-int/lit8 v8, v8, 0x3c

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v9, v9, 0x3c

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 127
    iget-object v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    .line 129
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v6, v6, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    .line 130
    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 131
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v7, v7, -0x3c

    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    add-int/lit8 v8, v8, -0x3c

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    add-int/lit8 v9, v9, 0x3c

    iget-object v10, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v10, v10, 0x3c

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 133
    iget-object v6, p0, Lcom/android/calendar/alerts/slidingTabView/a;->t:Landroid/graphics/Rect;

    float-to-int v3, v3

    float-to-int v4, v4

    invoke-virtual {v6, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    .line 135
    iget-boolean v4, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-nez v4, :cond_2

    if-nez v5, :cond_2

    if-eqz v3, :cond_0

    .line 139
    :cond_2
    packed-switch v2, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 176
    goto :goto_0

    .line 141
    :pswitch_1
    iput-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 142
    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 143
    const-wide/16 v2, 0x1e

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/alerts/slidingTabView/a;->a(J)V

    .line 145
    if-eqz v5, :cond_4

    .line 146
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 147
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 148
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    .line 149
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 150
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    .line 161
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 162
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->d()V

    .line 163
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->d(Z)V

    .line 164
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->f()V

    .line 165
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->a()V

    goto :goto_1

    .line 153
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 154
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 155
    invoke-direct {p0, v11}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    .line 156
    invoke-direct {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 157
    invoke-direct {p0, v11}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    goto :goto_2

    .line 170
    :pswitch_2
    iget-object v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v2, v0}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    goto :goto_1

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 310
    iput-boolean v5, p0, Lcom/android/calendar/alerts/slidingTabView/a;->u:Z

    .line 312
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(IIIIZ)V

    .line 313
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    const/4 v5, 0x0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/alerts/slidingTabView/c;->a(IIIIZ)V

    .line 314
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->u:Z

    if-nez v0, :cond_0

    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 97
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 99
    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->v:I

    .line 100
    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->w:I

    .line 104
    :goto_0
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->v:I

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->w:I

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->setMeasuredDimension(II)V

    .line 105
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->u:Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 221
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 223
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 224
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 226
    packed-switch v0, :pswitch_data_0

    .line 292
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v3, v2

    :cond_2
    return v3

    .line 228
    :pswitch_0
    invoke-direct {p0, v5, v6, p0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-direct {p0, v5, v6}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FF)V

    .line 234
    iget v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->q:F

    .line 235
    const-wide/16 v0, 0x0

    .line 237
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v8, v9, :cond_4

    .line 238
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->b:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->c:I

    int-to-float v1, v1

    invoke-static {v5, v6, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFFF)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    .line 242
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    double-to-int v9, v0

    mul-int/lit8 v9, v9, 0x2

    invoke-virtual {v8, v9}, Lcom/android/calendar/alerts/slidingTabView/c;->a(I)V

    .line 244
    float-to-double v8, v7

    cmpg-double v0, v8, v0

    if-gez v0, :cond_9

    move v0, v2

    .line 248
    :goto_2
    iget-boolean v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    if-nez v1, :cond_7

    if-eqz v0, :cond_7

    .line 249
    iput-boolean v2, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 250
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 251
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v4}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 252
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v0, v1, :cond_5

    move v0, v2

    .line 253
    :goto_3
    if-eqz v0, :cond_6

    move v0, v2

    :goto_4
    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(I)V

    .line 255
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->c()V

    .line 256
    invoke-direct {p0, v3}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    goto :goto_0

    .line 239
    :cond_4
    iget-object v8, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v9, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    if-ne v8, v9, :cond_3

    .line 240
    iget v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->d:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->e:I

    int-to-float v1, v1

    invoke-static {v5, v6, v0, v1}, Lcom/android/calendar/alerts/slidingTabView/a;->a(FFFF)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    goto :goto_1

    :cond_5
    move v0, v3

    .line 252
    goto :goto_3

    :cond_6
    move v0, v4

    .line 253
    goto :goto_4

    .line 258
    :cond_7
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 259
    iget-object v1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v1, v1, Lcom/android/calendar/alerts/slidingTabView/c;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->getHitRect(Landroid/graphics/Rect;)V

    .line 261
    float-to-int v1, v5

    float-to-int v4, v6

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_8

    .line 263
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 264
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->f()V

    goto/16 :goto_0

    .line 266
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->e()I

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    goto/16 :goto_0

    .line 274
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    iget-object v0, v0, Lcom/android/calendar/alerts/slidingTabView/c;->b:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 275
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->p:Z

    .line 276
    iput-boolean v3, p0, Lcom/android/calendar/alerts/slidingTabView/a;->h:Z

    .line 277
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 279
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/slidingTabView/c;->b()V

    .line 280
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->d(Z)V

    .line 281
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/c;->c(Z)V

    .line 282
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v3}, Lcom/android/calendar/alerts/slidingTabView/c;->b(I)V

    .line 283
    iput-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->o:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 284
    iput-object v7, p0, Lcom/android/calendar/alerts/slidingTabView/a;->r:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 285
    invoke-direct {p0, v3}, Lcom/android/calendar/alerts/slidingTabView/a;->setGrabbedState(I)V

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto/16 :goto_2

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setAlarmSlidingListener(Lcom/android/calendar/alerts/slidingTabView/b;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->x:Lcom/android/calendar/alerts/slidingTabView/b;

    .line 297
    return-void
.end method

.method public setLeftHintText(I)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public setOnTriggerListener(Lcom/android/calendar/alerts/slidingTabView/h;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/a;->f:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 383
    return-void
.end method

.method public setRightHintText(I)V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public setSnooze(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 108
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 109
    iget-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/a;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/slidingTabView/c;->a(Z)V

    .line 110
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/a;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/slidingTabView/a;->a(Z)V

    .line 211
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 212
    return-void
.end method
