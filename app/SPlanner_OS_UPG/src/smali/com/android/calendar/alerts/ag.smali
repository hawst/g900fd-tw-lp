.class Lcom/android/calendar/alerts/ag;
.super Ljava/lang/Thread;
.source "QuickResponseActivity.java"


# instance fields
.field a:J

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/android/calendar/alerts/QuickResponseActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/QuickResponseActivity;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 175
    iput-wide p2, p0, Lcom/android/calendar/alerts/ag;->a:J

    .line 176
    iput-object p4, p0, Lcom/android/calendar/alerts/ag;->b:Ljava/lang/String;

    .line 177
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    iget-wide v2, p0, Lcom/android/calendar/alerts/ag;->a:J

    iget-object v1, p0, Lcom/android/calendar/alerts/ag;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/alerts/QuickResponseActivity;->a(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_0

    .line 184
    :try_start_0
    const-string v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 186
    iget-object v1, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    const v2, 0x7f0f01ba

    invoke-virtual {v1, v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 187
    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 188
    iget-object v0, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/QuickResponseActivity;->startActivity(Landroid/content/Intent;)V

    .line 189
    iget-object v0, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->overridePendingTransition(II)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/QuickResponseActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 191
    :catch_0
    move-exception v0

    .line 192
    iget-object v0, p0, Lcom/android/calendar/alerts/ag;->c:Lcom/android/calendar/alerts/QuickResponseActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/QuickResponseActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/alerts/ah;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/ah;-><init>(Lcom/android/calendar/alerts/ag;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
