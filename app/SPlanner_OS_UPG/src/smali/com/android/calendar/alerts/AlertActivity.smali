.class public Lcom/android/calendar/alerts/AlertActivity;
.super Landroid/app/Activity;
.source "AlertActivity.java"


# static fields
.field private static final f:[Ljava/lang/String;

.field private static n:I

.field private static o:[I


# instance fields
.field protected a:Landroid/widget/LinearLayout;

.field protected b:Landroid/widget/CheckBox;

.field protected c:Lcom/android/calendar/g/a/a;

.field protected d:Z

.field protected final e:Landroid/view/View$OnClickListener;

.field private g:Lcom/android/calendar/alerts/m;

.field private h:Lcom/android/calendar/alerts/g;

.field private i:Landroid/database/Cursor;

.field private j:Landroid/widget/ListView;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private final p:Landroid/widget/AdapterView$OnItemClickListener;

.field private final q:Landroid/view/View$OnClickListener;

.field private final r:Landroid/content/BroadcastReceiver;

.field private final s:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "state"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "alarmTime"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/AlertActivity;->f:[Ljava/lang/String;

    .line 147
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/alerts/AlertActivity;->o:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/AlertActivity;->d:Z

    .line 252
    new-instance v0, Lcom/android/calendar/alerts/a;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/a;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->e:Landroid/view/View$OnClickListener;

    .line 273
    new-instance v0, Lcom/android/calendar/alerts/b;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/b;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->p:Landroid/widget/AdapterView$OnItemClickListener;

    .line 431
    new-instance v0, Lcom/android/calendar/alerts/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/d;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->q:Landroid/view/View$OnClickListener;

    .line 445
    new-instance v0, Lcom/android/calendar/alerts/e;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/e;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->r:Landroid/content/BroadcastReceiver;

    .line 561
    new-instance v0, Lcom/android/calendar/alerts/f;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/f;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->s:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 65
    sput p0, Lcom/android/calendar/alerts/AlertActivity;->n:I

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/AlertActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/alerts/AlertActivity;)Lcom/android/calendar/alerts/m;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    return-object v0
.end method

.method private a(J)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 380
    new-instance v5, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 381
    sget-object v0, Lcom/android/calendar/alerts/AlertActivity;->f:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 383
    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->h:Lcom/android/calendar/alerts/g;

    const/16 v2, 0xc8

    sget-object v4, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v8, 0x0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/alerts/g;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 385
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 410
    if-nez p1, :cond_1

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    const-string v1, "bundle_key_snooze_duration"

    sget v2, Lcom/android/calendar/alerts/AlertActivity;->n:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/android/calendar/alerts/AlertActivity;->n:I

    .line 416
    const-string v1, "bundle_key_select_all_status"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 417
    iget-object v2, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 418
    const-string v1, "bundle_key_checked_items_list"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    .line 419
    if-eqz v1, :cond_0

    .line 422
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 423
    iget-object v2, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    aget-wide v4, v1, v0

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v5, v3}, Lcom/android/calendar/g/a/a;->a(JZ)V

    .line 422
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/m;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/AlertActivity;J)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/alerts/AlertActivity;->a(J)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 759
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 760
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->m:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 761
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/alerts/AlertActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/alerts/AlertActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->h()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/alerts/AlertActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->j()V

    return-void
.end method

.method static synthetic e()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/android/calendar/alerts/AlertActivity;->n:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/alerts/AlertActivity;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->i()V

    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 362
    new-instance v5, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 363
    sget-object v0, Lcom/android/calendar/alerts/AlertActivity;->f:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 364
    const-string v6, "state=1"

    .line 365
    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->h:Lcom/android/calendar/alerts/g;

    sget-object v4, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v8, 0x0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/alerts/g;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 369
    new-instance v1, Lcom/android/calendar/alerts/q;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 371
    const/16 v0, 0x14

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 372
    return-void
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 399
    sget-object v3, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI_BY_INSTANCE:Landroid/net/Uri;

    .line 400
    const-string v5, "state=?"

    .line 401
    new-array v6, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 405
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->h:Lcom/android/calendar/alerts/g;

    const/16 v1, 0x64

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/alerts/AlertActivity;->f:[Ljava/lang/String;

    const-string v7, "begin ASC,title ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/alerts/g;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 587
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 588
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 589
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "tag_duration_dialog"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/h;

    .line 592
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/calendar/alerts/h;->isRemoving()Z

    move-result v2

    if-nez v2, :cond_0

    .line 594
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :cond_0
    invoke-static {}, Lcom/android/calendar/alerts/h;->a()Lcom/android/calendar/alerts/h;

    move-result-object v0

    .line 603
    invoke-virtual {v0}, Lcom/android/calendar/alerts/h;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 604
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tag_duration_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/alerts/h;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 606
    :cond_1
    :goto_0
    return-void

    .line 595
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private i()V
    .locals 24

    .prologue
    .line 612
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    if-nez v4, :cond_0

    .line 613
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    .line 649
    :goto_0
    return-void

    .line 616
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 617
    sget-object v4, Lcom/android/calendar/hj;->I:Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 619
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v6, Lcom/android/calendar/alerts/AlertActivity;->o:[I

    sget v7, Lcom/android/calendar/alerts/AlertActivity;->n:I

    aget v6, v6, v7

    int-to-long v6, v6

    const-wide/32 v8, 0xea60

    mul-long/2addr v6, v8

    add-long v10, v4, v6

    .line 620
    const-wide/16 v4, 0x0

    .line 624
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 626
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-wide v14, v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_2

    .line 627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 628
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Lcom/android/calendar/g/a/a;->b(J)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    move/from16 v0, v20

    if-eq v0, v4, :cond_1

    move-wide v4, v14

    .line 626
    :goto_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-wide v14, v4

    goto :goto_1

    .line 631
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    const/4 v5, 0x6

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 632
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 633
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    const/4 v9, 0x5

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 637
    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lcom/android/calendar/alerts/u;->a(JJJJI)Landroid/content/ContentValues;

    move-result-object v17

    .line 640
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-eqz v4, :cond_3

    move-wide v4, v10

    .line 643
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/AlertActivity;->h:Lcom/android/calendar/alerts/g;

    const/16 v14, 0x12c

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    sget-object v16, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v18, 0x0

    invoke-virtual/range {v13 .. v19}, Lcom/android/calendar/alerts/g;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V

    .line 645
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/alerts/AlertActivity;->a(J)V

    .line 646
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Lcom/android/calendar/g/a/a;->a(J)V

    goto :goto_2

    .line 648
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    move-wide v4, v14

    goto :goto_3
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 655
    invoke-static {p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 656
    sget-object v0, Lcom/android/calendar/hj;->I:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 658
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    if-nez v0, :cond_0

    .line 676
    :goto_0
    return-void

    .line 663
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/m;->getCount()I

    move-result v0

    if-gt v0, v1, :cond_3

    .line 664
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->f()V

    .line 674
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->a()V

    .line 675
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->finish()V

    goto :goto_0

    .line 666
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->e()[Ljava/lang/Long;

    move-result-object v1

    .line 668
    if-eqz v1, :cond_2

    array-length v0, v1

    if-eqz v0, :cond_2

    .line 669
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 670
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/alerts/AlertActivity;->a(J)V

    .line 669
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/View;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 686
    if-gez v0, :cond_0

    .line 687
    const/4 v0, 0x0

    .line 689
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 693
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->g()V

    .line 694
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 700
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    .line 716
    :cond_0
    :goto_0
    return-void

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v3, :cond_2

    .line 705
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 711
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->b()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->b()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 712
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 707
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 708
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->a()V

    goto :goto_1

    .line 714
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    .line 722
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 724
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->a()V

    .line 726
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 727
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 728
    iget-object v3, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    invoke-virtual {v3, v0}, Lcom/android/calendar/alerts/m;->a(I)J

    move-result-wide v4

    .line 731
    iget-object v3, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v3, v4, v5, v1}, Lcom/android/calendar/g/a/a;->a(JZ)V

    .line 727
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 733
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 739
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->l:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->k:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->m:Landroid/widget/Button;

    if-nez v0, :cond_1

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->b()I

    move-result v0

    if-lez v0, :cond_4

    .line 746
    :cond_3
    invoke-direct {p0, v1}, Lcom/android/calendar/alerts/AlertActivity;->a(Z)V

    goto :goto_0

    .line 748
    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->a(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 306
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 308
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "usm_setting"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0100

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f045c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/alerts/c;

    invoke-direct {v2, p0}, Lcom/android/calendar/alerts/c;-><init>(Lcom/android/calendar/alerts/AlertActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 322
    :cond_1
    invoke-static {p0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 323
    const v0, 0x103012b

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->setTheme(I)V

    .line 325
    :cond_2
    const v0, 0x7f040016

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->setContentView(I)V

    .line 326
    const v0, 0x7f0f0043

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->setTitle(I)V

    .line 328
    new-instance v0, Lcom/android/calendar/g/a/a;

    invoke-direct {v0}, Lcom/android/calendar/g/a/a;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    .line 329
    new-instance v0, Lcom/android/calendar/alerts/g;

    invoke-direct {v0, p0, p0}, Lcom/android/calendar/alerts/g;-><init>(Lcom/android/calendar/alerts/AlertActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->h:Lcom/android/calendar/alerts/g;

    .line 330
    new-instance v0, Lcom/android/calendar/alerts/m;

    const v1, 0x7f040018

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/m;-><init>(Lcom/android/calendar/alerts/AlertActivity;I)V

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    .line 332
    const v0, 0x7f120055

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    .line 333
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 334
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->g:Lcom/android/calendar/alerts/m;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 335
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->p:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 337
    const v0, 0x7f120056

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->k:Landroid/widget/Button;

    .line 338
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->k:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 339
    const v0, 0x7f120057

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->l:Landroid/widget/Button;

    .line 340
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->l:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    const v0, 0x7f120058

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->m:Landroid/widget/Button;

    .line 342
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->m:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    const v0, 0x7f120054

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    .line 345
    const v0, 0x7f120052

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    .line 346
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->r:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "FinishAlertActivity"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/AlertActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 351
    invoke-static {p0}, Lcom/android/calendar/hj;->o(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/android/calendar/alerts/AlertActivity;->n:I

    .line 352
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/AlertActivity;->o:[I

    .line 355
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/AlertActivity;->a(Landroid/os/Bundle;)V

    .line 356
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->i:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->r:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 459
    sget-object v0, Lcom/android/calendar/hj;->I:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 461
    invoke-virtual {p0}, Lcom/android/calendar/alerts/AlertActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "tag_duration_dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/h;

    .line 463
    if-eqz v0, :cond_1

    .line 464
    invoke-virtual {v0}, Lcom/android/calendar/alerts/h;->dismissAllowingStateLoss()V

    .line 467
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/alerts/AlertActivity;->d:Z

    if-nez v0, :cond_2

    .line 468
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 469
    const-string v1, "com.android.calendar.NEED_UPDATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 473
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.calendar.FINISH_POPUP_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/AlertActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 476
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 477
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 389
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 392
    invoke-direct {p0}, Lcom/android/calendar/alerts/AlertActivity;->g()V

    .line 393
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 766
    const-string v0, "bundle_key_snooze_duration"

    sget v1, Lcom/android/calendar/alerts/AlertActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 768
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 769
    const-string v0, "bundle_key_select_all_status"

    iget-object v1, p0, Lcom/android/calendar/alerts/AlertActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->b()I

    move-result v0

    if-eqz v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/android/calendar/alerts/AlertActivity;->c:Lcom/android/calendar/g/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/g/a/a;->d()[J

    move-result-object v0

    .line 774
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    .line 775
    const-string v1, "bundle_key_checked_items_list"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 778
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 779
    return-void
.end method
