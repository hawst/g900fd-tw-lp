.class public Lcom/android/calendar/alerts/q;
.super Ljava/lang/Object;
.source "AlertService.java"


# instance fields
.field public a:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p1, p0, Lcom/android/calendar/alerts/q;->a:Landroid/app/NotificationManager;

    .line 181
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/calendar/alerts/q;->a:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 185
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/calendar/alerts/q;->a:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 189
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 198
    :goto_0
    if-gt p1, p2, :cond_0

    .line 199
    invoke-virtual {p0, p1}, Lcom/android/calendar/alerts/q;->a(I)V

    .line 198
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 201
    :cond_0
    return-void
.end method

.method public a(ILcom/android/calendar/alerts/s;)V
    .locals 2

    .prologue
    .line 192
    if-eqz p2, :cond_0

    .line 193
    iget-object v0, p0, Lcom/android/calendar/alerts/q;->a:Landroid/app/NotificationManager;

    iget-object v1, p2, Lcom/android/calendar/alerts/s;->a:Landroid/app/Notification;

    invoke-virtual {v0, p1, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 195
    :cond_0
    return-void
.end method
