.class final Lcom/android/calendar/alerts/ax;
.super Landroid/os/Handler;
.source "TaskAlertService.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/TaskAlertService;


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/TaskAlertService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 473
    iput-object p1, p0, Lcom/android/calendar/alerts/ax;->a:Lcom/android/calendar/alerts/TaskAlertService;

    .line 474
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 475
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 479
    invoke-static {}, Lcom/android/calendar/alerts/TaskAlertService;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "----Task AlertService--handleMessage-----when-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getWhen()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v0, p0, Lcom/android/calendar/alerts/ax;->a:Lcom/android/calendar/alerts/TaskAlertService;

    invoke-static {v0, p1}, Lcom/android/calendar/alerts/TaskAlertService;->a(Lcom/android/calendar/alerts/TaskAlertService;Landroid/os/Message;)V

    .line 483
    iget-object v0, p0, Lcom/android/calendar/alerts/ax;->a:Lcom/android/calendar/alerts/TaskAlertService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/TaskAlertReceiver;->a(Landroid/app/Service;I)V

    .line 484
    return-void
.end method
