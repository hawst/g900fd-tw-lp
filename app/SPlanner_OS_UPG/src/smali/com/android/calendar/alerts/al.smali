.class Lcom/android/calendar/alerts/al;
.super Ljava/lang/Object;
.source "TaskAlertActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/TaskAlertActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/android/calendar/alerts/al;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 288
    iget-object v0, p0, Lcom/android/calendar/alerts/al;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    .line 289
    invoke-virtual {v0, p2}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Landroid/view/View;)Landroid/database/Cursor;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_0

    .line 292
    iget-object v2, p0, Lcom/android/calendar/alerts/al;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(Lcom/android/calendar/alerts/TaskAlertActivity;J)V

    .line 293
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    .line 295
    sput-boolean v6, Lcom/android/calendar/alerts/TaskAlertActivity;->g:Z

    .line 296
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 297
    sget-object v4, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 298
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 299
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 300
    iget-object v2, p0, Lcom/android/calendar/alerts/al;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    const-class v3, Lcom/android/calendar/AllInOneActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 302
    iget-object v2, p0, Lcom/android/calendar/alerts/al;->a:Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-static {v2}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v2

    .line 303
    const-class v3, Lcom/android/calendar/detail/TaskInfoActivity;

    invoke-virtual {v2, v3}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    move-result-object v2

    .line 304
    invoke-virtual {v2, v1}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/TaskStackBuilder;->startActivities()V

    .line 306
    :cond_0
    invoke-virtual {v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    .line 307
    return-void
.end method
