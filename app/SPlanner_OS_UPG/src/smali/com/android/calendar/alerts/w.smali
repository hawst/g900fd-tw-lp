.class Lcom/android/calendar/alerts/w;
.super Ljava/lang/Object;
.source "PopUpActivity.java"

# interfaces
.implements Lcom/android/calendar/alerts/slidingTabView/h;


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/PopUpActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public b(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 198
    const/4 v0, 0x1

    if-ne v0, p2, :cond_1

    .line 199
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    iget-object v1, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->b(Lcom/android/calendar/alerts/PopUpActivity;)Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;JZ)V

    .line 200
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->c(Lcom/android/calendar/alerts/PopUpActivity;)V

    .line 201
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->d(Lcom/android/calendar/alerts/PopUpActivity;)V

    .line 207
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->a()V

    .line 209
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->f(Lcom/android/calendar/alerts/PopUpActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/alerts/x;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/x;-><init>(Lcom/android/calendar/alerts/w;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 215
    return-void

    .line 202
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p2, :cond_0

    .line 203
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    iget-object v1, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->b(Lcom/android/calendar/alerts/PopUpActivity;)Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;JZ)V

    .line 204
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->e(Lcom/android/calendar/alerts/PopUpActivity;)V

    .line 205
    iget-object v0, p0, Lcom/android/calendar/alerts/w;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->d(Lcom/android/calendar/alerts/PopUpActivity;)V

    goto :goto_0
.end method
