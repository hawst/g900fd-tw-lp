.class Lcom/android/calendar/alerts/ay;
.super Ljava/lang/Object;
.source "TextToSpeechService.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/TextToSpeechService;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/TextToSpeechService;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/android/calendar/alerts/ay;->a:Lcom/android/calendar/alerts/TextToSpeechService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 5

    .prologue
    .line 136
    invoke-static {}, Lcom/android/calendar/alerts/TextToSpeechService;->a()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    .line 141
    invoke-static {}, Lcom/android/calendar/alerts/TextToSpeechService;->a()Landroid/speech/tts/TextToSpeech;

    move-result-object v3

    new-instance v4, Ljava/util/Locale;

    invoke-direct {v4, v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    move-result v0

    .line 150
    if-gez v0, :cond_1

    .line 151
    invoke-static {}, Lcom/android/calendar/alerts/TextToSpeechService;->a()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 152
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/alerts/TextToSpeechService;->a(Z)Z

    .line 161
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/ay;->a:Lcom/android/calendar/alerts/TextToSpeechService;

    invoke-static {v0}, Lcom/android/calendar/alerts/TextToSpeechService;->a(Lcom/android/calendar/alerts/TextToSpeechService;)V

    .line 162
    return-void

    .line 157
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/calendar/alerts/TextToSpeechService;->a(Z)Z

    goto :goto_0
.end method
