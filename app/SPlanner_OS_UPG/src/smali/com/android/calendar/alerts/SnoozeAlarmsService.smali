.class public Lcom/android/calendar/alerts/SnoozeAlarmsService;
.super Landroid/app/IntentService;
.source "SnoozeAlarmsService.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    const-class v0, Lcom/android/calendar/alerts/SnoozeAlarmsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/SnoozeAlarmsService;->a:Ljava/lang/String;

    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "state"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/SnoozeAlarmsService;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/android/calendar/alerts/SnoozeAlarmsService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 47
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 18

    .prologue
    .line 57
    const-string v2, "eventid"

    const-wide/16 v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 58
    const-string v4, "eventstart"

    const-wide/16 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 59
    const-string v6, "eventend"

    const-wide/16 v8, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 63
    const-string v8, "notificationid"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 66
    const-wide/16 v10, -0x1

    cmp-long v8, v2, v10

    if-eqz v8, :cond_1

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/SnoozeAlarmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    .line 70
    if-eqz v9, :cond_0

    .line 71
    const-string v8, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/calendar/alerts/SnoozeAlarmsService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    .line 72
    invoke-virtual {v8, v9}, Landroid/app/NotificationManager;->cancel(I)V

    .line 74
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 75
    const-string v9, "com.android.calendar.NEED_UPDATE_ACTION"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/calendar/alerts/SnoozeAlarmsService;->sendBroadcast(Landroid/content/Intent;)V

    .line 80
    :cond_0
    sget-object v12, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    .line 81
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "state=1 AND event_id="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 83
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 84
    sget-object v10, Lcom/android/calendar/alerts/SnoozeAlarmsService;->b:[Ljava/lang/String;

    const/4 v13, 0x0

    aget-object v10, v10, v13

    const/4 v13, 0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v9, v10, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 85
    const/4 v10, 0x0

    invoke-virtual {v11, v12, v9, v8, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 86
    sget-object v10, Lcom/android/calendar/alerts/SnoozeAlarmsService;->a:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "onHandleIntent() numOfUpdatedRow : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v13, "info : "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v9}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 90
    const-string v9, "com.android.calendar.FINISH_POPUP_ACTION"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/calendar/alerts/SnoozeAlarmsService;->sendBroadcast(Landroid/content/Intent;)V

    .line 94
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/hj;->n(Landroid/content/Context;)I

    move-result v10

    int-to-long v14, v10

    const-wide/32 v16, 0xea60

    mul-long v14, v14, v16

    add-long/2addr v8, v14

    .line 95
    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lcom/android/calendar/alerts/u;->a(JJJJI)Landroid/content/ContentValues;

    move-result-object v2

    .line 96
    invoke-virtual {v11, v12, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 97
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;)Lcom/android/calendar/alerts/slidingTabView/e;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2, v8, v9}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Lcom/android/calendar/alerts/slidingTabView/e;J)V

    .line 99
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/SnoozeAlarmsService;->stopSelf()V

    .line 101
    return-void
.end method
