.class Lcom/android/calendar/alerts/slidingTabView/f;
.super Lcom/android/calendar/alerts/slidingTabView/c;
.source "AlarmSlidingTabForClearCover.java"


# instance fields
.field final synthetic h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;


# direct methods
.method public constructor <init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;Landroid/view/ViewGroup;IIIILcom/android/calendar/alerts/slidingTabView/i;)V
    .locals 9

    .prologue
    .line 150
    iput-object p1, p0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    .line 151
    const/4 v4, -0x1

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/alerts/slidingTabView/c;-><init>(Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;Z)V

    .line 152
    return-void
.end method


# virtual methods
.method protected a(IIIIZ)V
    .locals 17

    .prologue
    .line 168
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 170
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 172
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/f;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 173
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 174
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 176
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/f;->c:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 177
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 178
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 180
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/slidingTabView/f;->d:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 181
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    .line 182
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    .line 184
    div-int/lit8 v9, v2, 0x2

    .line 185
    div-int/lit8 v10, v1, 0x2

    .line 186
    div-int/lit8 v11, v4, 0x2

    .line 187
    div-int/lit8 v12, v3, 0x2

    .line 188
    div-int/lit8 v13, v6, 0x2

    .line 189
    div-int/lit8 v14, v5, 0x2

    .line 191
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 193
    if-eqz p5, :cond_3

    .line 194
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v6, v5, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    .line 195
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v5, v5, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    .line 198
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 199
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int v4, v1, v2

    .line 200
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    div-int/lit8 v2, v7, 0x2

    sub-int v3, v1, v2

    .line 201
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int/2addr v1, v2

    add-int v2, v1, v8

    .line 202
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    .line 214
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/slidingTabView/f;->d:Landroid/widget/ImageView;

    invoke-virtual {v7, v4, v3, v2, v1}, Landroid/widget/ImageView;->layout(IIII)V

    move v1, v5

    move v2, v6

    .line 239
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/f;->a:Landroid/widget/ImageView;

    sub-int v4, v2, v9

    sub-int v5, v1, v10

    add-int v6, v2, v9

    add-int v7, v1, v10

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 245
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/f;->b:Landroid/widget/ImageView;

    sub-int v4, v2, v11

    sub-int v5, v1, v12

    add-int v6, v2, v11

    add-int v7, v1, v12

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/ImageView;->layout(IIII)V

    .line 251
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/alerts/slidingTabView/f;->c:Landroid/widget/ImageView;

    sub-int v4, v2, v13

    sub-int v5, v1, v14

    add-int/2addr v2, v13

    add-int/2addr v1, v14

    invoke-virtual {v3, v4, v5, v2, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 256
    return-void

    .line 203
    :cond_1
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 204
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int v4, v1, v2

    .line 205
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    sub-int v3, v1, v7

    .line 206
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int/2addr v1, v2

    add-int v2, v1, v8

    .line 207
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    goto :goto_0

    .line 208
    :cond_2
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 209
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int v4, v1, v2

    .line 210
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    div-int/lit8 v2, v7, 0x2

    sub-int v3, v1, v2

    .line 211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->b:I

    add-int/2addr v1, v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    add-int/2addr v1, v2

    add-int v2, v1, v8

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->c:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    goto/16 :goto_0

    .line 217
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v6, v5, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    .line 218
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v5, v5, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    .line 221
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 222
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v8

    sub-int v4, v1, v9

    .line 223
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    div-int/lit8 v2, v7, 0x2

    sub-int v3, v1, v2

    .line 224
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int v2, v1, v9

    .line 225
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    .line 237
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/slidingTabView/f;->d:Landroid/widget/ImageView;

    invoke-virtual {v7, v4, v3, v2, v1}, Landroid/widget/ImageView;->layout(IIII)V

    move v1, v5

    move v2, v6

    goto/16 :goto_1

    .line 226
    :cond_5
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v8

    sub-int v4, v1, v9

    .line 228
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v3, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    .line 229
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int v2, v1, v9

    .line 230
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    add-int/2addr v1, v7

    goto :goto_2

    .line 231
    :cond_6
    sget v15, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->z:I

    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 232
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v8

    sub-int v4, v1, v9

    .line 233
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    div-int/lit8 v2, v7, 0x2

    sub-int v3, v1, v2

    .line 234
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v2, v2, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a:I

    sub-int/2addr v1, v2

    sub-int v2, v1, v9

    .line 235
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/alerts/slidingTabView/f;->h:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget v1, v1, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->e:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    goto :goto_2
.end method

.method protected d(Z)V
    .locals 0

    .prologue
    .line 164
    return-void
.end method
