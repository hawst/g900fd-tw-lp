.class public Lcom/android/calendar/alerts/TaskAlertActivity;
.super Landroid/app/Activity;
.source "TaskAlertActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static g:Z

.field private static final i:Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:[Ljava/lang/String;

.field private static m:J

.field private static n:I

.field private static z:I


# instance fields
.field private final A:Ljava/lang/String;

.field private final B:Ljava/lang/String;

.field private final C:Ljava/lang/String;

.field private final D:Landroid/widget/AdapterView$OnItemClickListener;

.field protected a:Ljava/util/HashMap;

.field protected b:I

.field protected c:Landroid/widget/LinearLayout;

.field protected d:Landroid/widget/CheckBox;

.field protected e:Z

.field protected f:Z

.field h:Landroid/content/BroadcastReceiver;

.field private o:Landroid/content/SharedPreferences;

.field private p:Landroid/database/ContentObserver;

.field private q:Landroid/content/ContentResolver;

.field private r:Lcom/android/calendar/alerts/au;

.field private s:Lcom/android/calendar/alerts/at;

.field private t:Landroid/database/Cursor;

.field private u:Landroid/widget/ListView;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/Button;

.field private y:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    const-class v0, Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->i:Ljava/lang/String;

    .line 79
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "task_id"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "reminder_time"

    aput-object v2, v0, v1

    const-string v1, "state"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountkey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->k:Ljava/lang/String;

    .line 104
    new-array v0, v4, [Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->l:[Ljava/lang/String;

    .line 132
    sput-boolean v3, Lcom/android/calendar/alerts/TaskAlertActivity;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    .line 73
    iput v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    .line 76
    iput-boolean v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    .line 77
    iput-boolean v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->f:Z

    .line 134
    const-string v0, "key_event_id"

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->A:Ljava/lang/String;

    .line 135
    const-string v0, "value_checked"

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->B:Ljava/lang/String;

    .line 136
    const-string v0, "all_items_checked"

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->C:Ljava/lang/String;

    .line 170
    new-instance v0, Lcom/android/calendar/alerts/ak;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/ak;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->h:Landroid/content/BroadcastReceiver;

    .line 284
    new-instance v0, Lcom/android/calendar/alerts/al;

    invoke-direct {v0, p0}, Lcom/android/calendar/alerts/al;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->D:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 68
    sput p0, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    return p0
.end method

.method private static a(JJJJLjava/lang/String;II)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 272
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 273
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 275
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 276
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 277
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 280
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 281
    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/ContentObserver;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->p:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    return-object p1
.end method

.method private a(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 163
    new-instance v4, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 164
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 166
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->s:Lcom/android/calendar/alerts/at;

    const/16 v1, 0x3e8

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/alerts/at;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/app/AlarmManager;J)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 258
    if-nez p1, :cond_0

    .line 259
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 262
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.calendar.ACTION_TASK_ALARM"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    sget-object v2, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 264
    const-string v2, "reminder_time"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 265
    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 266
    invoke-virtual {v0, v3, p2, p3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 267
    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/TaskAlertActivity;J)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(J)V

    return-void
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 68
    sput p0, Lcom/android/calendar/alerts/TaskAlertActivity;->z:I

    return p0
.end method

.method static synthetic b(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/alerts/TaskAlertActivity;)Lcom/android/calendar/alerts/au;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->r:Lcom/android/calendar/alerts/au;

    return-object v0
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 68
    sget v0, Lcom/android/calendar/alerts/TaskAlertActivity;->z:I

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e()I
    .locals 1

    .prologue
    .line 68
    sget v0, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/alerts/TaskAlertActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    return-object v0
.end method

.method private f()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 148
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 149
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    aget-object v0, v0, v8

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    aget-object v5, v5, v8

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 151
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->s:Lcom/android/calendar/alerts/at;

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/alerts/at;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    new-instance v1, Lcom/android/calendar/alerts/q;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 156
    const/16 v0, 0x64

    const/16 v2, 0x78

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 691
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 692
    if-gez v0, :cond_0

    .line 693
    const/4 v0, 0x0

    .line 695
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    goto :goto_0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 311
    iget v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    if-le v0, v3, :cond_0

    .line 312
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 317
    :goto_0
    iget-boolean v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 319
    iput-boolean v2, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    .line 327
    :goto_1
    return-void

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 322
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method

.method public b()V
    .locals 7

    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 333
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 334
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v3

    .line 335
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 336
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->r:Lcom/android/calendar/alerts/au;

    invoke-virtual {v0, v1}, Lcom/android/calendar/alerts/au;->a(I)J

    move-result-wide v4

    .line 337
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_0

    .line 339
    const v6, 0x7f120067

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 340
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 343
    :cond_0
    if-eqz v2, :cond_1

    .line 344
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 347
    :cond_2
    if-nez v2, :cond_3

    .line 348
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 350
    :cond_3
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 353
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    if-gt v0, v1, :cond_1

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 355
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 356
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 362
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 359
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 360
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 21

    .prologue
    .line 622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_5

    .line 623
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 624
    sget-object v4, Lcom/android/calendar/hj;->J:Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 626
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    .line 627
    sget v5, Lcom/android/calendar/alerts/TaskAlertActivity;->z:I

    aget v4, v4, v5

    mul-int/lit8 v4, v4, 0x3c

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    sput-wide v4, Lcom/android/calendar/alerts/TaskAlertActivity;->m:J

    .line 628
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lcom/android/calendar/alerts/TaskAlertActivity;->m:J

    add-long v10, v4, v6

    .line 630
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-eqz v4, :cond_2

    .line 631
    const-wide/16 v4, 0x0

    .line 632
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v15

    .line 633
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v7, -0x1

    invoke-interface {v6, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-wide/from16 v16, v4

    .line 634
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 635
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 636
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    if-ne v15, v4, :cond_0

    .line 640
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 641
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v7, 0x5

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 642
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v9, 0x6

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 643
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/4 v13, 0x4

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 644
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/16 v14, 0x8

    invoke-interface {v13, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 645
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    const/16 v20, 0x7

    move/from16 v0, v20

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 647
    invoke-static/range {v4 .. v14}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(JJJJLjava/lang/String;II)Landroid/content/ContentValues;

    move-result-object v6

    .line 651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-eqz v4, :cond_9

    move-wide v4, v10

    .line 654
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->s:Lcom/android/calendar/alerts/at;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    sget-object v12, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    invoke-virtual {v7, v8, v9, v12, v6}, Lcom/android/calendar/alerts/at;->startInsert(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 655
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(J)V

    .line 656
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-wide/from16 v16, v4

    .line 657
    goto/16 :goto_0

    .line 659
    :cond_2
    sget-object v4, Lcom/android/calendar/alerts/TaskAlertActivity;->i:Ljava/lang/String;

    const-string v5, "Cursor object is null. Ignore the Snooze request."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    .line 683
    :cond_4
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    .line 684
    return-void

    .line 663
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_8

    .line 664
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->e(Landroid/content/Context;)V

    .line 665
    sget-object v4, Lcom/android/calendar/hj;->J:Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 667
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 668
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->f()V

    .line 679
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    goto :goto_2

    .line 670
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/Long;

    .line 671
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Long;

    .line 672
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v6

    .line 673
    const/4 v5, 0x0

    :goto_3
    if-ge v5, v6, :cond_6

    .line 674
    aget-object v7, v4, v5

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/android/calendar/alerts/TaskAlertActivity;->a(J)V

    .line 675
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    aget-object v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 680
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_4

    .line 681
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/calendar/alerts/TaskAlertActivity;->showDialog(I)V

    goto :goto_2

    :cond_9
    move-wide/from16 v4, v16

    goto/16 :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 366
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 368
    invoke-static {p0}, Lcom/android/calendar/dz;->D(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    const v0, 0x103012b

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->setTheme(I)V

    .line 372
    :cond_0
    const v0, 0x7f0400a7

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->setContentView(I)V

    .line 373
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0429

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 374
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 376
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->o:Landroid/content/SharedPreferences;

    .line 378
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->q:Landroid/content/ContentResolver;

    .line 379
    new-instance v0, Lcom/android/calendar/alerts/at;

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->q:Landroid/content/ContentResolver;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/at;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->s:Lcom/android/calendar/alerts/at;

    .line 380
    new-instance v0, Lcom/android/calendar/alerts/au;

    const v1, 0x7f0400a8

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/au;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->r:Lcom/android/calendar/alerts/au;

    .line 382
    const v0, 0x7f120055

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    .line 383
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 384
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->r:Lcom/android/calendar/alerts/au;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 385
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->u:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->D:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 387
    const v0, 0x7f120056

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    .line 388
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 389
    const v0, 0x7f120057

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    .line 390
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 391
    const v0, 0x7f120058

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    .line 392
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 397
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 398
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->x:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 400
    new-instance v0, Lcom/android/calendar/alerts/am;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/am;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->p:Landroid/database/ContentObserver;

    .line 423
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->h:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "FinishTaskAlertActivity"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/alerts/TaskAlertActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 425
    const v0, 0x7f120054

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    .line 426
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    new-instance v1, Lcom/android/calendar/alerts/an;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/an;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 433
    const v0, 0x7f120052

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->c:Landroid/widget/LinearLayout;

    .line 436
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/alerts/ao;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/ao;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 445
    invoke-static {p0}, Lcom/android/calendar/hj;->o(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/android/calendar/alerts/TaskAlertActivity;->z:I

    .line 447
    if-eqz p1, :cond_2

    .line 448
    const-string v0, "all_items_checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    .line 449
    iget-boolean v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    if-eqz v0, :cond_1

    .line 450
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->e:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 451
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->b()V

    .line 452
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    .line 454
    :cond_1
    const-string v0, "preferences_snooze_time"

    sget v1, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    .line 458
    :goto_0
    return-void

    .line 456
    :cond_2
    sget v0, Lcom/android/calendar/alerts/TaskAlertActivity;->z:I

    sput v0, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 564
    packed-switch p1, :pswitch_data_0

    .line 616
    :cond_0
    :goto_0
    return-object v0

    .line 566
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 567
    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 568
    const v3, 0x7f09002f

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    .line 570
    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->y:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->y:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 574
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f03d6

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/calendar/alerts/TaskAlertActivity;->n:I

    new-instance v3, Lcom/android/calendar/alerts/as;

    invoke-direct {v3, p0}, Lcom/android/calendar/alerts/as;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/alerts/ar;

    invoke-direct {v2, p0}, Lcom/android/calendar/alerts/ar;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/alerts/aq;

    invoke-direct {v2, p0}, Lcom/android/calendar/alerts/aq;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/alerts/ap;

    invoke-direct {v1, p0}, Lcom/android/calendar/alerts/ap;-><init>(Lcom/android/calendar/alerts/TaskAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->y:Landroid/app/AlertDialog;

    .line 612
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->y:Landroid/app/AlertDialog;

    goto :goto_0

    .line 564
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->p:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->p:Landroid/database/ContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 505
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 507
    sget-object v0, Lcom/android/calendar/hj;->J:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 509
    iget-boolean v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->f:Z

    if-nez v0, :cond_2

    .line 510
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 511
    const-string v1, "com.android.calendar.NEED_UPDATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 515
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.calendar.FINISH_POPUP_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/TaskAlertActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 518
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 519
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 549
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 551
    const-string v0, "key_event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    .line 552
    const-string v0, "value_checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v2

    .line 554
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 555
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 556
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 557
    iget-object v3, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    aget-wide v4, v1, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aget-boolean v5, v2, v0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 560
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 466
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->s:Lcom/android/calendar/alerts/at;

    const/4 v1, 0x0

    sget-object v3, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/alerts/TaskAlertActivity;->j:[Ljava/lang/String;

    sget-object v5, Lcom/android/calendar/alerts/TaskAlertActivity;->k:Ljava/lang/String;

    sget-object v6, Lcom/android/calendar/alerts/TaskAlertActivity;->l:[Ljava/lang/String;

    const-string v7, "due_date ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/alerts/at;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->c()V

    .line 485
    return-void

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    if-nez v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 472
    iput-object v2, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    .line 479
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->b:I

    .line 481
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->a()V

    goto :goto_0

    .line 473
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 474
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 475
    iput-object v2, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    .line 476
    invoke-virtual {p0}, Lcom/android/calendar/alerts/TaskAlertActivity;->finish()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 523
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 524
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    .line 526
    if-lez v2, :cond_1

    .line 527
    new-array v3, v2, [Ljava/lang/Long;

    .line 528
    new-array v4, v2, [J

    .line 529
    new-array v5, v2, [Z

    .line 531
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 533
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 534
    aget-object v0, v3, v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v4, v1

    .line 535
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    aget-object v6, v3, v1

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v5, v1

    .line 533
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 538
    :cond_0
    const-string v0, "key_event_id"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 539
    const-string v0, "value_checked"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 540
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    const-string v0, "all_items_checked"

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 545
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 490
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 491
    invoke-static {}, Lcom/android/calendar/alerts/bd;->a()V

    .line 492
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertActivity;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 495
    :cond_0
    return-void
.end method
