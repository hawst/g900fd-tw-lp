.class public Lcom/android/calendar/alerts/ai;
.super Landroid/app/Dialog;
.source "SViewCoverDialog.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static k:I


# instance fields
.field private b:Lcom/samsung/android/sdk/cover/ScoverState;

.field private c:Lcom/android/calendar/alerts/PopUpActivity;

.field private d:Lcom/android/calendar/alerts/af;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

.field private j:Lcom/android/calendar/alerts/slidingTabView/h;

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/android/calendar/alerts/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/alerts/PopUpActivity;Lcom/samsung/android/sdk/cover/Scover;Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 55
    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->e:Landroid/widget/TextView;

    .line 56
    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->f:Landroid/widget/TextView;

    .line 57
    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->g:Landroid/widget/TextView;

    .line 58
    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->h:Landroid/widget/TextView;

    .line 60
    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->j:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 62
    iput-boolean v1, p0, Lcom/android/calendar/alerts/ai;->l:Z

    .line 63
    iput-boolean v1, p0, Lcom/android/calendar/alerts/ai;->m:Z

    .line 67
    iput-object p1, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    .line 68
    iput-object p3, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 69
    invoke-static {p1}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/ai;->m:Z

    .line 70
    return-void
.end method

.method private a(JZ)V
    .locals 5

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 273
    iput-wide p1, v0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 274
    if-eqz p3, :cond_0

    .line 275
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    .line 279
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 280
    return-void

    .line 277
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 8

    .prologue
    const v7, 0x7f0b0038

    const/16 v6, 0xef

    const/4 v5, 0x0

    .line 217
    .line 219
    const v0, 0x7f120090

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 222
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sview_color_wallpaper"

    invoke-static {v0, v2, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sview_bg_wallpaper_path"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sview_color_wallpaper"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_0

    .line 226
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "cover_index_wallpaper.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 231
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 233
    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowWidth()I

    move-result v2

    iget-object v3, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowHeight()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 234
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 235
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 236
    const v0, 0x7f120091

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 262
    :goto_1
    return-void

    .line 228
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "cover_wallpaper.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 239
    :cond_1
    sget-object v0, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "There is no wallpaper image in the local storage. Please, check the path of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sview_color_use_all"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sview_color_random"

    invoke-static {v0, v2, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_3

    .line 248
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "s_vew_cover_background_color"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 250
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v6, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 257
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 253
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "sview_bg_display_random"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 255
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v6, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_2

    .line 261
    :cond_4
    const v0, 0x7f0b0028

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1
.end method

.method private b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 119
    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v2}, Lcom/android/calendar/alerts/aj;->b(Landroid/content/Context;)Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v2

    .line 120
    if-eqz v2, :cond_0

    .line 121
    iput-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 124
    :cond_0
    iget-boolean v2, p0, Lcom/android/calendar/alerts/ai;->m:Z

    if-eqz v2, :cond_1

    .line 170
    :goto_0
    return v0

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 147
    :pswitch_0
    sget-object v1, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cover type is not defined. Cover info : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    :pswitch_1
    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-static {v2}, Lcom/android/calendar/alerts/aj;->a(Lcom/samsung/android/sdk/cover/ScoverState;)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/ai;->k:I

    .line 132
    sget v2, Lcom/android/calendar/alerts/ai;->k:I

    if-ne v2, v1, :cond_4

    .line 133
    const v0, 0x7f040022

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->setContentView(I)V

    .line 151
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/alerts/ai;->a(Landroid/content/Context;)V

    .line 153
    const v0, 0x7f120095

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    .line 154
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->j:Lcom/android/calendar/alerts/slidingTabView/h;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->setOnTriggerListener(Lcom/android/calendar/alerts/slidingTabView/h;)V

    .line 156
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0, v2}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->setCoverState(Lcom/samsung/android/sdk/cover/ScoverState;)V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/g/d;->a(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object v0

    .line 159
    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->i:Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;

    invoke-virtual {v2, v0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForClearCover;->a(Landroid/graphics/Point;)V

    .line 162
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/alerts/ai;->l:Z

    if-nez v0, :cond_3

    .line 163
    const v0, 0x7f12005b

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->e:Landroid/widget/TextView;

    .line 164
    const v0, 0x7f120097

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->h:Landroid/widget/TextView;

    .line 165
    const v0, 0x7f120096

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->g:Landroid/widget/TextView;

    .line 168
    :cond_3
    const v0, 0x7f120094

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->f:Landroid/widget/TextView;

    move v0, v1

    .line 170
    goto/16 :goto_0

    .line 134
    :cond_4
    sget v2, Lcom/android/calendar/alerts/ai;->k:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 135
    const v0, 0x7f040023

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->setContentView(I)V

    goto :goto_1

    .line 137
    :cond_5
    sget-object v1, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    const-string v2, "You didn\'t define the cover size. So, you must define the cover size to the SViewCoverUtils class."

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    sget-object v1, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cover info : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 143
    :pswitch_2
    iput-boolean v1, p0, Lcom/android/calendar/alerts/ai;->l:Z

    .line 144
    const v0, 0x7f040021

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->setContentView(I)V

    goto/16 :goto_1

    .line 128
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 174
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 179
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 183
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 185
    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowWidth()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 186
    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 188
    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/g/d;->a(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    .line 189
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 190
    iget-object v3, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowWidth()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 191
    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 192
    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 194
    invoke-virtual {p0, v4}, Lcom/android/calendar/alerts/ai;->setCancelable(Z)V

    .line 195
    return-void
.end method


# virtual methods
.method public a(Lcom/android/calendar/alerts/af;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    .line 74
    return-void
.end method

.method public a(Lcom/android/calendar/alerts/slidingTabView/h;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/android/calendar/alerts/ai;->j:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 78
    return-void
.end method

.method public a()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 81
    iget-boolean v0, p0, Lcom/android/calendar/alerts/ai;->m:Z

    if-eqz v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v9

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    if-nez v0, :cond_3

    .line 86
    :cond_2
    sget-object v0, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    const-string v1, "title view is null or mData is null"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/android/calendar/alerts/ai;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cover info : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->b:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/cover/ScoverState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-object v1, v1, Lcom/android/calendar/alerts/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-boolean v0, p0, Lcom/android/calendar/alerts/ai;->l:Z

    if-nez v0, :cond_4

    .line 96
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v2

    .line 97
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-boolean v3, v0, Lcom/android/calendar/alerts/af;->e:Z

    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-wide v4, v0, Lcom/android/calendar/alerts/af;->b:J

    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-wide v6, v0, Lcom/android/calendar/alerts/af;->c:J

    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-boolean v8, v0, Lcom/android/calendar/alerts/af;->d:Z

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;Ljava/util/Calendar;ZJJZZ)Ljava/lang/String;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-boolean v0, v0, Lcom/android/calendar/alerts/af;->e:Z

    if-eqz v0, :cond_5

    .line 102
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->e:Landroid/widget/TextView;

    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 107
    :goto_1
    const v0, 0x7f12005d

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 108
    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-object v1, v1, Lcom/android/calendar/alerts/af;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 109
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    :cond_4
    :goto_2
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 104
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->e:Landroid/widget/TextView;

    const v1, 0x7f0f01a9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 111
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/alerts/ai;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/calendar/alerts/ai;->d:Lcom/android/calendar/alerts/af;

    iget-object v2, v2, Lcom/android/calendar/alerts/af;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 201
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/g/e;->b(Landroid/content/Context;)V

    .line 202
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/g/h;->a(Landroid/view/Window;)V

    .line 203
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f10005a

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 205
    invoke-direct {p0}, Lcom/android/calendar/alerts/ai;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->dismiss()V

    .line 214
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/alerts/ai;->c()V

    .line 211
    invoke-virtual {p0}, Lcom/android/calendar/alerts/ai;->a()Z

    .line 213
    const-wide/16 v0, 0x1770

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/alerts/ai;->a(JZ)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/calendar/alerts/ai;->c:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/g/e;->a(Landroid/content/Context;)V

    .line 267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/ai;->j:Lcom/android/calendar/alerts/slidingTabView/h;

    .line 268
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 269
    return-void
.end method
