.class Lcom/android/calendar/alerts/y;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "PopUpActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/alerts/PopUpActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/alerts/PopUpActivity;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 4

    .prologue
    .line 222
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 226
    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cover is open"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->g(Lcom/android/calendar/alerts/PopUpActivity;)V

    goto :goto_0

    .line 228
    :cond_2
    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cover is close"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    iget-object v1, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v1}, Lcom/android/calendar/alerts/PopUpActivity;->b(Lcom/android/calendar/alerts/PopUpActivity;)Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/alerts/PopUpActivity;->a(Lcom/android/calendar/alerts/PopUpActivity;JZ)V

    .line 230
    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-static {v0}, Lcom/android/calendar/alerts/PopUpActivity;->e(Lcom/android/calendar/alerts/PopUpActivity;)V

    .line 231
    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->a()V

    .line 232
    iget-object v0, p0, Lcom/android/calendar/alerts/y;->a:Lcom/android/calendar/alerts/PopUpActivity;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/PopUpActivity;->finish()V

    goto :goto_0
.end method
