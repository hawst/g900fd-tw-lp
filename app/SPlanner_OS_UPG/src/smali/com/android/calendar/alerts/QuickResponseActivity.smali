.class public Lcom/android/calendar/alerts/QuickResponseActivity;
.super Landroid/app/ListActivity;
.source "QuickResponseActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static a:J


# instance fields
.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    .line 170
    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 100
    .line 104
    invoke-static {p0, p1, p2}, Lcom/android/calendar/alerts/u;->a(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v3

    .line 106
    if-eqz v3, :cond_b

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 107
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 108
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 109
    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 110
    const/4 v0, 0x3

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 113
    :goto_0
    if-eqz v3, :cond_0

    .line 114
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 118
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f02dd

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 122
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 123
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 124
    invoke-static {p0, p1, p2}, Lcom/android/calendar/alerts/u;->b(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v7

    .line 126
    if-eqz v7, :cond_3

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 128
    :cond_2
    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 129
    const/4 v9, 0x0

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 130
    packed-switch v8, :pswitch_data_0

    .line 135
    invoke-static {v3, v9, v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v8

    if-nez v8, :cond_2

    .line 140
    :cond_3
    if-eqz v7, :cond_4

    .line 141
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 147
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_5

    if-eqz v0, :cond_5

    .line 148
    invoke-static {v3, v0, v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_5
    if-eqz v5, :cond_a

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 153
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/hj;->a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 156
    :goto_2
    if-nez v0, :cond_9

    move-object v0, v6

    .line 160
    :goto_3
    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_7

    .line 114
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 132
    :pswitch_0
    :try_start_2
    invoke-static {v4, v9, v2}, Lcom/android/calendar/alerts/QuickResponseActivity;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 140
    :catchall_1
    move-exception v0

    if-eqz v7, :cond_8

    .line 141
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 159
    :cond_9
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_3

    :cond_a
    move-object v0, v6

    goto :goto_2

    :cond_b
    move-object v0, v6

    move-object v1, v6

    move-object v2, v6

    move-object v5, v6

    goto/16 :goto_0

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 165
    invoke-static {p1, p2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 53
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 56
    if-nez v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->finish()V

    .line 81
    :goto_0
    return-void

    .line 61
    :cond_0
    const-string v1, "eventId"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/calendar/alerts/QuickResponseActivity;->a:J

    .line 62
    sget-wide v0, Lcom/android/calendar/alerts/QuickResponseActivity;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 63
    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->finish()V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 71
    invoke-static {p0}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 74
    array-length v0, v1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    .line 76
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 77
    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    aget-object v3, v1, v0

    aput-object v3, v2, v0

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 79
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/alerts/QuickResponseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0351

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 80
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04007d

    iget-object v2, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/alerts/QuickResponseActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge p3, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/android/calendar/alerts/QuickResponseActivity;->b:[Ljava/lang/String;

    aget-object v0, v0, p3

    .line 92
    :cond_0
    new-instance v1, Lcom/android/calendar/alerts/ag;

    sget-wide v2, Lcom/android/calendar/alerts/QuickResponseActivity;->a:J

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/android/calendar/alerts/ag;-><init>(Lcom/android/calendar/alerts/QuickResponseActivity;JLjava/lang/String;)V

    invoke-virtual {v1}, Lcom/android/calendar/alerts/ag;->start()V

    .line 93
    return-void
.end method
