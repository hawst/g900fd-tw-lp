.class public final enum Lcom/android/calendar/alerts/ad;
.super Ljava/lang/Enum;
.source "PopUpActivity.java"


# static fields
.field public static final enum a:Lcom/android/calendar/alerts/ad;

.field public static final enum b:Lcom/android/calendar/alerts/ad;

.field public static final enum c:Lcom/android/calendar/alerts/ad;

.field public static final enum d:Lcom/android/calendar/alerts/ad;

.field private static final synthetic e:[Lcom/android/calendar/alerts/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 157
    new-instance v0, Lcom/android/calendar/alerts/ad;

    const-string v1, "POPUP_ALERT_START"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/alerts/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/alerts/ad;->a:Lcom/android/calendar/alerts/ad;

    .line 158
    new-instance v0, Lcom/android/calendar/alerts/ad;

    const-string v1, "POPUP_ALERT_DISMISSED"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/alerts/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/alerts/ad;->b:Lcom/android/calendar/alerts/ad;

    .line 159
    new-instance v0, Lcom/android/calendar/alerts/ad;

    const-string v1, "POPUP_ALERT_SNOOZED"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/alerts/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/alerts/ad;->c:Lcom/android/calendar/alerts/ad;

    .line 160
    new-instance v0, Lcom/android/calendar/alerts/ad;

    const-string v1, "NOTI_ALERT_START"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/alerts/ad;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/alerts/ad;->d:Lcom/android/calendar/alerts/ad;

    .line 156
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/calendar/alerts/ad;

    sget-object v1, Lcom/android/calendar/alerts/ad;->a:Lcom/android/calendar/alerts/ad;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/alerts/ad;->b:Lcom/android/calendar/alerts/ad;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/alerts/ad;->c:Lcom/android/calendar/alerts/ad;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/alerts/ad;->d:Lcom/android/calendar/alerts/ad;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/alerts/ad;->e:[Lcom/android/calendar/alerts/ad;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/alerts/ad;
    .locals 1

    .prologue
    .line 156
    const-class v0, Lcom/android/calendar/alerts/ad;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/alerts/ad;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/alerts/ad;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/android/calendar/alerts/ad;->e:[Lcom/android/calendar/alerts/ad;

    invoke-virtual {v0}, [Lcom/android/calendar/alerts/ad;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/alerts/ad;

    return-object v0
.end method
