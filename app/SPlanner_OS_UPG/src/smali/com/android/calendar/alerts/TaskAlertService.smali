.class public Lcom/android/calendar/alerts/TaskAlertService;
.super Landroid/app/Service;
.source "TaskAlertService.java"


# static fields
.field public static final a:Landroid/net/Uri;

.field private static final b:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static g:Z


# instance fields
.field private volatile c:Landroid/os/Looper;

.field private volatile d:Lcom/android/calendar/alerts/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    const-class v0, Lcom/android/calendar/alerts/TaskAlertService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    .line 74
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "task_id"

    aput-object v1, v0, v4

    const-string v1, "reminder_time"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "state"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountkey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertService;->e:[Ljava/lang/String;

    .line 107
    new-array v0, v5, [Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertService;->f:[Ljava/lang/String;

    .line 114
    const-string v0, "content://com.android.calendar/TasksReminders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    .line 117
    sput-boolean v3, Lcom/android/calendar/alerts/TaskAlertService;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 472
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 522
    .line 524
    invoke-static {}, Lcom/android/calendar/dz;->h()Ljava/lang/String;

    move-result-object v3

    .line 525
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "title LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 528
    if-eqz v0, :cond_1

    .line 529
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 530
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v8, :cond_0

    .line 531
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 532
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "preferences_alerts_ringtone"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 534
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 537
    :cond_1
    return-object v4
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 447
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 448
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 454
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 457
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/alerts/TextToSpeechService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 458
    const-string v1, "TTS_TITLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;IZZI)V
    .locals 8

    .prologue
    .line 386
    sget-object v1, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "###### creating new alarm notification, numReminders: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p4, :cond_0

    const-string v0, " QUIET"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 390
    if-nez p3, :cond_1

    .line 391
    new-instance v1, Lcom/android/calendar/alerts/q;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-direct {v1, v0}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 393
    const/16 v0, 0x64

    const/16 v2, 0x78

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 444
    :goto_1
    return-void

    .line 386
    :cond_0
    const-string v0, " loud"

    goto :goto_0

    .line 398
    :cond_1
    invoke-static {p0, p2, p3, p5, p6}, Lcom/android/calendar/alerts/TaskAlertReceiver;->a(Landroid/content/Context;Ljava/lang/String;IZI)Landroid/app/Notification;

    move-result-object v4

    .line 400
    iget v1, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v4, Landroid/app/Notification;->defaults:I

    .line 401
    const-string v1, "event"

    iput-object v1, v4, Landroid/app/Notification;->category:Ljava/lang/String;

    .line 404
    if-nez p4, :cond_6

    .line 406
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 407
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 412
    :goto_2
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 413
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    .line 414
    :goto_3
    const-string v3, "preferences_alerts_vibrate"

    const/4 v5, 0x0

    invoke-interface {p1, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 416
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "dormant_switch_onoff"

    const/4 v7, 0x0

    invoke-static {v3, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_9

    const/4 v3, 0x1

    .line 419
    :goto_4
    if-nez v3, :cond_2

    .line 421
    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    .line 422
    iget v2, v4, Landroid/app/Notification;->haptic:I

    or-int/lit8 v2, v2, 0x12

    iput v2, v4, Landroid/app/Notification;->haptic:I

    .line 427
    :cond_2
    const-string v2, "preferences_alerts_ringtone"

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 428
    if-eqz v2, :cond_3

    const-string v3, "default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 429
    :cond_3
    invoke-static {p1, p0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 434
    :cond_4
    :goto_5
    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_5

    invoke-static {p0}, Lcom/android/calendar/alerts/u;->g(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 436
    :cond_5
    const/4 v1, 0x0

    iput-object v1, v4, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 443
    :cond_6
    :goto_6
    invoke-virtual {v0, p6, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_1

    .line 409
    :cond_7
    iput-object p2, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    goto :goto_2

    .line 413
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 416
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    .line 430
    :cond_a
    invoke-static {v2, p0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 431
    invoke-static {p1, p0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 438
    :cond_b
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x0

    :goto_7
    iput-object v1, v4, Landroid/app/Notification;->sound:Landroid/net/Uri;

    goto :goto_6

    :cond_c
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_7
.end method

.method private a(Landroid/os/Message;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 124
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 126
    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    .line 138
    sget-object v2, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "alarmTime"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 140
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/alerts/TaskAlertService;->b()V

    goto :goto_0

    .line 144
    :cond_3
    const-string v2, "com.android.calendar.ACTION_TASK_ALARM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "android.intent.action.PROVIDER_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "alarmTime"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    .line 148
    :cond_4
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "uri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v2, "alarmTime"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v2, v8

    if-nez v0, :cond_6

    .line 150
    :cond_5
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    const-string v1, "uri or alarmTime is null, so exit"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 153
    :cond_6
    sget-boolean v0, Lcom/android/calendar/alerts/TaskAlertActivity;->g:Z

    if-ne v0, v6, :cond_7

    .line 154
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/alerts/TaskAlertActivity;->g:Z

    .line 157
    :cond_7
    const-string v0, "com.android.calendar.ACTION_TASK_ALARM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 158
    sput-boolean v6, Lcom/android/calendar/alerts/TaskAlertService;->g:Z

    .line 159
    invoke-static {v6}, Lcom/android/calendar/alerts/ba;->a(Z)V

    .line 162
    :cond_8
    invoke-static {p0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;)Z

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/alerts/TaskAlertService;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/os/Message;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 47

    .prologue
    .line 167
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 168
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v38

    .line 170
    sget-object v5, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    sget-object v6, Lcom/android/calendar/alerts/TaskAlertService;->e:[Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(state=? OR state=?) AND reminder_time<="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v38

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/android/calendar/alerts/TaskAlertService;->f:[Ljava/lang/String;

    const-string v9, "reminder_time DESC"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v37

    .line 174
    new-instance v40, Lcom/android/calendar/alerts/q;

    const-string v5, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    move-object/from16 v0, v40

    invoke-direct {v0, v5}, Lcom/android/calendar/alerts/q;-><init>(Landroid/app/NotificationManager;)V

    .line 177
    if-eqz v37, :cond_0

    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_2

    .line 178
    :cond_0
    if-eqz v37, :cond_1

    .line 179
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    .line 181
    :cond_1
    const/16 v4, 0x64

    const/16 v5, 0x78

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 182
    const/4 v4, 0x0

    .line 380
    :goto_0
    return v4

    .line 185
    :cond_2
    sget-object v5, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "alert count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->getCount()I

    move-result v5

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v41, v0

    .line 187
    const/4 v7, 0x0

    .line 188
    const/4 v6, 0x0

    .line 189
    const-wide/16 v28, 0x0

    .line 190
    const-wide/16 v24, 0x0

    .line 191
    const-wide/16 v20, 0x0

    .line 192
    const-wide/16 v16, 0x0

    .line 193
    const-wide/16 v14, 0x0

    .line 194
    const-wide/16 v12, 0x0

    .line 196
    new-instance v42, Ljava/util/HashMap;

    invoke-direct/range {v42 .. v42}, Ljava/util/HashMap;-><init>()V

    .line 197
    const/4 v8, 0x0

    .line 198
    const/4 v5, 0x0

    move/from16 v32, v5

    move-object/from16 v33, v6

    move/from16 v34, v7

    .line 200
    :goto_1
    :try_start_0
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 201
    const/4 v5, 0x0

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 202
    const/4 v5, 0x4

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 203
    const/4 v5, 0x2

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 204
    const/4 v5, 0x6

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 205
    const/4 v5, 0x3

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v36

    .line 206
    const/4 v5, 0x1

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 207
    const/16 v5, 0x8

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 208
    const/4 v5, 0x7

    move-object/from16 v0, v37

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 210
    sget-object v5, Lcom/android/calendar/alerts/TaskAlertService;->a:Landroid/net/Uri;

    move-wide/from16 v0, v18

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v43

    .line 211
    if-nez v33, :cond_23

    .line 212
    if-eqz v9, :cond_4

    move-object v5, v9

    .line 218
    :goto_2
    add-int/lit8 v35, v34, 0x1

    aput-object v9, v41, v34

    .line 219
    new-instance v44, Landroid/content/ContentValues;

    invoke-direct/range {v44 .. v44}, Landroid/content/ContentValues;-><init>()V

    .line 220
    const/16 v33, -0x1

    .line 222
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    move-object/from16 v0, v42

    move-object/from16 v1, v34

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    if-nez v34, :cond_5

    .line 223
    add-int/lit8 v34, v8, 0x1

    .line 224
    if-nez v36, :cond_25

    .line 225
    const/4 v8, 0x1

    .line 226
    add-int/lit8 v32, v32, 0x1

    .line 227
    sget-object v33, Lcom/android/calendar/alerts/TaskAlertService;->e:[Ljava/lang/String;

    const/16 v45, 0x2

    aget-object v33, v33, v45

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    move-object/from16 v0, v44

    move-object/from16 v1, v33

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move/from16 v33, v34

    .line 233
    :goto_3
    const/16 v34, -0x1

    move/from16 v0, v34

    if-eq v8, v0, :cond_24

    .line 234
    sget-object v34, Lcom/android/calendar/alerts/TaskAlertService;->e:[Ljava/lang/String;

    const/16 v36, 0x3

    aget-object v34, v34, v36

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v36

    move-object/from16 v0, v44

    move-object/from16 v1, v34

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    :goto_4
    invoke-virtual/range {v44 .. v44}, Landroid/content/ContentValues;->size()I

    move-result v34

    if-lez v34, :cond_3

    .line 240
    const/16 v34, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, v43

    move-object/from16 v1, v44

    move-object/from16 v2, v34

    move-object/from16 v3, v36

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 243
    :cond_3
    const/16 v34, 0x1

    move/from16 v0, v34

    if-eq v8, v0, :cond_6

    move/from16 v8, v33

    move/from16 v34, v35

    move-object/from16 v33, v5

    .line 244
    goto/16 :goto_1

    .line 215
    :cond_4
    const-string v5, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 230
    :cond_5
    const/16 v33, 0x2

    move/from16 v46, v33

    move/from16 v33, v8

    move/from16 v8, v46

    goto :goto_3

    .line 247
    :cond_6
    if-eqz v5, :cond_7

    cmp-long v8, v28, v30

    if-gtz v8, :cond_22

    :cond_7
    move-wide/from16 v12, v22

    move-wide/from16 v14, v26

    move-wide/from16 v16, v30

    move-object v5, v9

    move-wide v8, v10

    move-wide/from16 v10, v18

    :goto_5
    move-wide/from16 v20, v12

    move-wide/from16 v24, v14

    move-wide/from16 v28, v16

    move/from16 v34, v35

    move-wide v12, v6

    move-wide v14, v8

    move-wide/from16 v16, v10

    move/from16 v8, v33

    move-object/from16 v33, v5

    .line 256
    goto/16 :goto_1

    .line 258
    :cond_8
    if-eqz v37, :cond_9

    .line 259
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    .line 263
    :cond_9
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 264
    const-string v5, "preferences_alerts"

    const/4 v7, 0x1

    invoke-interface {v6, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 265
    const-string v7, "preferences_alerts_popup"

    const/4 v9, 0x0

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 266
    const-string v7, "preferences_alerts_type"

    const-string v10, "1"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 269
    if-nez v5, :cond_b

    .line 270
    sget-object v4, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    const-string v5, "alert preference is OFF"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual/range {v40 .. v40}, Lcom/android/calendar/alerts/q;->a()V

    .line 272
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 258
    :catchall_0
    move-exception v4

    if-eqz v37, :cond_a

    .line 259
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v4

    .line 275
    :cond_b
    if-nez v32, :cond_e

    const/4 v7, 0x1

    .line 276
    :goto_6
    if-lez v32, :cond_f

    if-eqz v9, :cond_f

    const/4 v5, 0x1

    move/from16 v18, v5

    .line 278
    :goto_7
    const-string v5, "power"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    .line 279
    invoke-virtual {v5}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v30

    .line 281
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 282
    const-string v9, "isTaskAlert"

    const/4 v10, 0x1

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 283
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 285
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->f(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 286
    const/4 v5, 0x1

    move/from16 v19, v5

    .line 290
    :goto_8
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "flash_notification"

    const/4 v9, 0x0

    invoke-static {v5, v7, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    const/4 v7, 0x1

    if-ne v5, v7, :cond_10

    const/4 v5, 0x1

    .line 294
    :goto_9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v9, "torch_light"

    const/4 v10, 0x1

    invoke-static {v7, v9, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_11

    const/4 v7, 0x1

    .line 296
    :goto_a
    if-nez v7, :cond_c

    .line 297
    if-eqz v5, :cond_c

    sget-boolean v5, Lcom/android/calendar/alerts/TaskAlertService;->g:Z

    if-eqz v5, :cond_c

    .line 298
    sget-object v5, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    const-string v7, "NotificationFlash start"

    invoke-static {v5, v7}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const/4 v5, 0x0

    sput-boolean v5, Lcom/android/calendar/alerts/TaskAlertService;->g:Z

    .line 300
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/ba;->a(Landroid/content/Context;)V

    .line 305
    :cond_c
    const/16 v11, 0x64

    .line 306
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/aj;->a(Landroid/content/Context;)Z

    move-result v31

    .line 307
    if-eqz v30, :cond_14

    if-nez v31, :cond_14

    .line 308
    const/4 v5, 0x0

    move v12, v5

    :goto_b
    move/from16 v0, v34

    if-ge v12, v0, :cond_1c

    const/16 v5, 0x14

    if-gt v12, v5, :cond_1c

    .line 309
    add-int/lit8 v13, v11, 0x1

    .line 310
    aget-object v7, v41, v12

    if-nez v19, :cond_d

    if-eqz v12, :cond_12

    :cond_d
    const/4 v9, 0x1

    :goto_c
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->l(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_13

    if-eqz v18, :cond_13

    const/4 v10, 0x1

    :goto_d
    move-object/from16 v5, p0

    invoke-static/range {v5 .. v11}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;IZZI)V

    .line 308
    add-int/lit8 v5, v12, 0x1

    move v12, v5

    move v11, v13

    goto :goto_b

    .line 275
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 276
    :cond_f
    const/4 v5, 0x0

    move/from16 v18, v5

    goto/16 :goto_7

    .line 290
    :cond_10
    const/4 v5, 0x0

    goto :goto_9

    .line 294
    :cond_11
    const/4 v7, 0x0

    goto :goto_a

    .line 310
    :cond_12
    const/4 v9, 0x0

    goto :goto_c

    :cond_13
    const/4 v10, 0x0

    goto :goto_d

    .line 313
    :cond_14
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->d(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1c

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/dz;->r(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1c

    .line 314
    const/16 v22, -0x1

    .line 315
    const/4 v5, 0x0

    move/from16 v23, v5

    :goto_e
    move/from16 v0, v23

    move/from16 v1, v34

    if-ge v0, v1, :cond_19

    const/16 v5, 0x14

    move/from16 v0, v23

    if-ge v0, v5, :cond_19

    .line 316
    add-int/lit8 v26, v11, 0x1

    .line 317
    aget-object v5, v41, v23

    if-eqz v5, :cond_15

    aget-object v5, v41, v23

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    move/from16 v22, v11

    .line 320
    :cond_15
    aget-object v7, v41, v23

    if-nez v19, :cond_16

    if-eqz v23, :cond_17

    :cond_16
    const/4 v9, 0x1

    :goto_f
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->l(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_18

    if-eqz v18, :cond_18

    const/4 v10, 0x1

    :goto_10
    move-object/from16 v5, p0

    invoke-static/range {v5 .. v11}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;IZZI)V

    .line 315
    add-int/lit8 v5, v23, 0x1

    move/from16 v23, v5

    move/from16 v11, v26

    goto :goto_e

    .line 320
    :cond_17
    const/4 v9, 0x0

    goto :goto_f

    :cond_18
    const/4 v10, 0x0

    goto :goto_10

    .line 323
    :cond_19
    const-string v5, "ril.cdma.inecmmode"

    const-string v6, "false"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "false"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 326
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    sget v6, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_FULL:I

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->makeMultiWindowIntent(Landroid/content/Intent;ILandroid/graphics/Rect;)Landroid/content/Intent;

    move-result-object v23

    .line 327
    const-class v5, Lcom/android/calendar/alerts/PopUpActivity;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 328
    const/high16 v5, 0x30800000

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 333
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v26

    .line 334
    const v10, 0x8011

    .line 335
    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 336
    const v10, 0x8091

    .line 339
    :cond_1a
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, v26

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 340
    move-wide/from16 v0, v28

    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 341
    iget v5, v5, Landroid/text/format/Time;->isDst:I

    if-eqz v5, :cond_20

    const/4 v5, 0x1

    move/from16 v18, v5

    .line 342
    :goto_11
    new-instance v32, Ljava/lang/StringBuilder;

    move-object/from16 v5, p0

    move-wide/from16 v6, v28

    move-wide/from16 v8, v24

    invoke-static/range {v5 .. v10}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 344
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 345
    const/16 v5, 0x20

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static/range {v26 .. v26}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    move/from16 v0, v18

    invoke-virtual {v6, v0, v7, v8}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    :cond_1b
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 350
    const-string v6, "event_id"

    move-object/from16 v0, v23

    move-wide/from16 v1, v20

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 351
    const-string v6, "alert_id"

    move-object/from16 v0, v23

    move-wide/from16 v1, v16

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 352
    const-string v6, "notification_id"

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 353
    const-string v6, "event_title"

    move-object/from16 v0, v23

    move-object/from16 v1, v33

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    const-string v6, "event_start_date"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v5, "event_start_time"

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 356
    const-string v5, "event_end_time"

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 357
    const-string v5, "reminder_type"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 358
    const-string v5, "accountKey"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 359
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 363
    :cond_1c
    if-nez v19, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->k(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1d

    .line 364
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-static {v0, v4, v1}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 367
    :cond_1d
    sget-object v4, Lcom/android/calendar/hj;->J:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v4

    .line 368
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/alerts/u;->h(Landroid/content/Context;)Z

    move-result v5

    .line 369
    sget-boolean v6, Lcom/android/calendar/alerts/TaskAlertActivity;->g:Z

    if-nez v6, :cond_1e

    if-eqz v30, :cond_1e

    if-nez v31, :cond_1e

    const-string v6, "0"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1e

    if-nez v4, :cond_1e

    if-nez v5, :cond_1e

    .line 371
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/android/calendar/alerts/TaskAlertActivity;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 373
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 375
    :cond_1e
    const/16 v4, 0x78

    if-gt v11, v4, :cond_1f

    .line 376
    const/16 v4, 0x78

    move-object/from16 v0, v40

    invoke-virtual {v0, v11, v4}, Lcom/android/calendar/alerts/q;->a(II)V

    .line 377
    sget-object v4, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Canceling leftover notification IDs "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x14

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_1f
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 341
    :cond_20
    const/4 v5, 0x0

    move/from16 v18, v5

    goto/16 :goto_11

    :cond_21
    move/from16 v19, v7

    goto/16 :goto_8

    :cond_22
    move-wide v6, v12

    move-wide v8, v14

    move-wide/from16 v10, v16

    move-wide/from16 v12, v20

    move-wide/from16 v14, v24

    move-wide/from16 v16, v28

    goto/16 :goto_5

    :cond_23
    move-object/from16 v5, v33

    goto/16 :goto_2

    :cond_24
    move/from16 v8, v36

    goto/16 :goto_4

    :cond_25
    move/from16 v8, v33

    move/from16 v33, v34

    goto/16 :goto_3
.end method

.method private static a(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 541
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    :cond_0
    :goto_0
    return v6

    .line 545
    :cond_1
    const-string v0, "content://settings/system/notification_sound"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 550
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 552
    const-string v0, "external"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 553
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 557
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    const-string v5, "is_notification"

    aput-object v5, v2, v6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 562
    if-nez v1, :cond_3

    move v6, v7

    .line 563
    goto :goto_0

    .line 555
    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 565
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 566
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v6, :cond_4

    .line 567
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v7

    .line 568
    goto :goto_0

    .line 570
    :cond_4
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    .line 571
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v6, v0

    .line 572
    goto :goto_0

    :cond_5
    move v0, v7

    .line 570
    goto :goto_2
.end method

.method private b()V
    .locals 0

    .prologue
    .line 469
    invoke-static {p0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;)Z

    .line 470
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 518
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 489
    sget-object v0, Lcom/android/calendar/alerts/TaskAlertService;->b:Ljava/lang/String;

    const-string v1, "----Task AlertService--created------"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AlertService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 491
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 493
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertService;->c:Landroid/os/Looper;

    .line 494
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertService;->c:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 495
    new-instance v0, Lcom/android/calendar/alerts/ax;

    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertService;->c:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/alerts/ax;-><init>(Lcom/android/calendar/alerts/TaskAlertService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/TaskAlertService;->d:Lcom/android/calendar/alerts/ax;

    .line 497
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertService;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 513
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 514
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 501
    if-eqz p1, :cond_0

    .line 502
    iget-object v0, p0, Lcom/android/calendar/alerts/TaskAlertService;->d:Lcom/android/calendar/alerts/ax;

    invoke-virtual {v0}, Lcom/android/calendar/alerts/ax;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 503
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 504
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 505
    iget-object v1, p0, Lcom/android/calendar/alerts/TaskAlertService;->d:Lcom/android/calendar/alerts/ax;

    invoke-virtual {v1, v0}, Lcom/android/calendar/alerts/ax;->sendMessage(Landroid/os/Message;)Z

    .line 507
    :cond_0
    const/4 v0, 0x3

    return v0
.end method
