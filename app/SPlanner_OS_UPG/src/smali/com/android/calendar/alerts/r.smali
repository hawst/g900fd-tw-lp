.class Lcom/android/calendar/alerts/r;
.super Ljava/lang/Object;
.source "AlertService.java"


# instance fields
.field a:Z

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/SharedPreferences;

.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Z)V
    .locals 1

    .prologue
    .line 941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 936
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/alerts/r;->d:I

    .line 938
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    .line 942
    iput-object p1, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    .line 943
    iput-object p2, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    .line 944
    iput-boolean p3, p0, Lcom/android/calendar/alerts/r;->a:Z

    .line 945
    return-void
.end method

.method private a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 948
    .line 949
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 950
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    .line 953
    invoke-static {}, Lcom/android/calendar/alerts/PopUpActivity;->b()Z

    move-result v3

    .line 954
    iget-object v4, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/calendar/alerts/u;->h(Landroid/content/Context;)Z

    move-result v4

    .line 956
    iget v5, p0, Lcom/android/calendar/alerts/r;->d:I

    if-gez v5, :cond_0

    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    .line 957
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    const-string v3, "preferences_alerts_popup"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v4, :cond_1

    .line 958
    iput v1, p0, Lcom/android/calendar/alerts/r;->d:I

    .line 963
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/calendar/alerts/r;->d:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    return v0

    .line 960
    :cond_1
    iput v2, p0, Lcom/android/calendar/alerts/r;->d:I

    goto :goto_0

    :cond_2
    move v0, v2

    .line 963
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/alerts/r;)Z
    .locals 1

    .prologue
    .line 927
    invoke-direct {p0}, Lcom/android/calendar/alerts/r;->a()Z

    move-result v0

    return v0
.end method

.method private b()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 967
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 969
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 970
    :goto_0
    iget-object v3, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    const-string v4, "preferences_alerts_vibrate"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 973
    if-eqz v3, :cond_1

    if-nez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 969
    goto :goto_0

    :cond_1
    move v1, v2

    .line 973
    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/alerts/r;)Z
    .locals 1

    .prologue
    .line 927
    invoke-direct {p0}, Lcom/android/calendar/alerts/r;->b()Z

    move-result v0

    return v0
.end method

.method private c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 977
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 978
    iget-boolean v0, p0, Lcom/android/calendar/alerts/r;->a:Z

    if-eqz v0, :cond_1

    .line 979
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    .line 989
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    .line 990
    const-string v1, ""

    iput-object v1, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    .line 991
    return-object v0

    .line 981
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    const-string v1, "preferences_alerts_ringtone"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    .line 982
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 983
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    goto :goto_0

    .line 984
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 985
    iget-object v0, p0, Lcom/android/calendar/alerts/r;->c:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/android/calendar/alerts/r;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/SharedPreferences;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/alerts/r;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/alerts/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 927
    invoke-direct {p0}, Lcom/android/calendar/alerts/r;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
