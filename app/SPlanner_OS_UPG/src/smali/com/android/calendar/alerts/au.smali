.class public Lcom/android/calendar/alerts/au;
.super Landroid/widget/ResourceCursorAdapter;
.source "TaskAlertAdapter.java"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z

.field private static c:I

.field private static d:I

.field private static f:Ljava/lang/ref/WeakReference;


# instance fields
.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/android/calendar/alerts/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/alerts/au;->a:Ljava/lang/String;

    .line 39
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/alerts/au;->b:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 54
    new-instance v1, Ljava/lang/ref/WeakReference;

    move-object v0, p1

    check-cast v0, Lcom/android/calendar/alerts/TaskAlertActivity;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/android/calendar/alerts/au;->f:Ljava/lang/ref/WeakReference;

    .line 55
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/alerts/au;->e:Z

    .line 56
    return-void
.end method

.method static synthetic a()Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/android/calendar/alerts/au;->f:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;JJIJI)V
    .locals 8

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 87
    new-instance v4, Lcom/android/calendar/alerts/aw;

    invoke-direct {v4}, Lcom/android/calendar/alerts/aw;-><init>()V

    .line 89
    const v2, 0x7f1202ca

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v4, v2}, Lcom/android/calendar/alerts/aw;->a(Lcom/android/calendar/alerts/aw;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 90
    const v2, 0x7f1202cb

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v4, v2}, Lcom/android/calendar/alerts/aw;->b(Lcom/android/calendar/alerts/aw;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 91
    const v2, 0x7f120067

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    invoke-static {v4, v2}, Lcom/android/calendar/alerts/aw;->a(Lcom/android/calendar/alerts/aw;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 93
    sget-boolean v2, Lcom/android/calendar/alerts/au;->b:Z

    if-eqz v2, :cond_0

    .line 94
    const v2, 0x7f0b001f

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/au;->d:I

    .line 95
    const v2, 0x7f0b001d

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/android/calendar/alerts/au;->c:I

    .line 96
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/calendar/alerts/au;->b:Z

    .line 99
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 100
    :cond_1
    const v2, 0x7f0f02bf

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 102
    :cond_2
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->a(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    const/4 v2, 0x2

    invoke-static {p6, p7, p1, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 107
    const-wide/16 v6, 0x0

    cmp-long v5, p6, v6

    if-eqz v5, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v5, p6, v6

    if-gez v5, :cond_8

    .line 108
    iget-boolean v5, p0, Lcom/android/calendar/alerts/au;->e:Z

    if-eqz v5, :cond_3

    .line 109
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->a(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v5

    sget v6, Lcom/android/calendar/alerts/au;->d:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 110
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->b(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v5

    sget v6, Lcom/android/calendar/alerts/au;->d:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    :cond_3
    :goto_0
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_5

    .line 117
    :cond_4
    const v5, 0x7f0f02bf

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 120
    :cond_5
    const-wide/16 v6, 0x0

    cmp-long v3, p6, v6

    if-nez v3, :cond_9

    .line 121
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->b(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f0f02d1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 127
    :goto_1
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 128
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    const v3, 0x7f120067

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/widget/CheckBox;->setTag(ILjava/lang/Object;)V

    .line 129
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 131
    sget-object v2, Lcom/android/calendar/alerts/au;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/alerts/TaskAlertActivity;

    .line 132
    iget-object v3, v2, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    if-eqz v3, :cond_6

    iget-object v3, v2, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_6

    iget-object v3, v2, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 133
    iget-object v2, v2, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 137
    :cond_6
    const/4 v2, 0x1

    move/from16 v0, p11

    if-le v0, v2, :cond_a

    .line 138
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 145
    :goto_2
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/alerts/av;

    invoke-direct {v3, p0}, Lcom/android/calendar/alerts/av;-><init>(Lcom/android/calendar/alerts/au;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :cond_7
    return-void

    .line 113
    :cond_8
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->b(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v5

    sget v6, Lcom/android/calendar/alerts/au;->c:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 123
    :cond_9
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->b(Lcom/android/calendar/alerts/aw;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 140
    :cond_a
    sget-object v2, Lcom/android/calendar/alerts/au;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/alerts/TaskAlertActivity;

    .line 141
    iget-object v2, v2, Lcom/android/calendar/alerts/TaskAlertActivity;->a:Ljava/util/HashMap;

    invoke-static/range {p9 .. p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    invoke-static {v4}, Lcom/android/calendar/alerts/aw;->c(Lcom/android/calendar/alerts/aw;)Landroid/widget/CheckBox;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public a(I)J
    .locals 4

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/android/calendar/alerts/au;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 167
    if-eqz v2, :cond_0

    .line 168
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 169
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 170
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 171
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 172
    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 176
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 65
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    sget-object v0, Lcom/android/calendar/alerts/au;->a:Ljava/lang/String;

    const-string v1, "Error: query returned no results"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :goto_0
    return-void

    .line 70
    :cond_1
    const v0, 0x7f120063

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 72
    const/4 v1, 0x7

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {p2, v1}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    .line 73
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 74
    const/4 v0, 0x4

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    const/4 v0, 0x5

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 76
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 77
    const/16 v0, 0x8

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 78
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 79
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v11

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    .line 80
    invoke-direct/range {v0 .. v11}, Lcom/android/calendar/alerts/au;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;JJIJI)V

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/ResourceCursorAdapter;->notifyDataSetChanged()V

    .line 61
    return-void
.end method
