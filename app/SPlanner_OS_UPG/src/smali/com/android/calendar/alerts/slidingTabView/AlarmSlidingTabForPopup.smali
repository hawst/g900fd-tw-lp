.class public Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;
.super Lcom/android/calendar/alerts/slidingTabView/a;
.source "AlarmSlidingTabForPopup.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/android/calendar/alerts/slidingTabView/a;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/alerts/slidingTabView/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Point;)V
    .locals 4

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 68
    iget v1, p1, Landroid/graphics/Point;->x:I

    .line 69
    iget v0, p1, Landroid/graphics/Point;->y:I

    .line 74
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0041

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->b:I

    .line 75
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sub-int v2, v0, v2

    iput v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->c:I

    .line 76
    iget v2, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->d:I

    .line 77
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->e:I

    .line 79
    invoke-virtual {p0}, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->invalidate()V

    .line 80
    return-void

    .line 71
    :cond_0
    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 72
    iget v0, p1, Landroid/graphics/Point;->x:I

    goto :goto_0
.end method

.method protected b()V
    .locals 10

    .prologue
    const v9, 0x7f020040

    const v4, 0x7f02003f

    .line 45
    new-instance v0, Lcom/android/calendar/alerts/slidingTabView/g;

    const v3, 0x7f02005b

    const v6, 0x7f02001f

    const v7, 0x7f05000b

    sget-object v8, Lcom/android/calendar/alerts/slidingTabView/i;->a:Lcom/android/calendar/alerts/slidingTabView/i;

    move-object v1, p0

    move-object v2, p0

    move v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/alerts/slidingTabView/g;-><init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->m:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 54
    new-instance v0, Lcom/android/calendar/alerts/slidingTabView/g;

    const v3, 0x7f020060

    const v6, 0x7f020028

    const v7, 0x7f05000a

    sget-object v8, Lcom/android/calendar/alerts/slidingTabView/i;->b:Lcom/android/calendar/alerts/slidingTabView/i;

    move-object v1, p0

    move-object v2, p0

    move v4, v9

    move v5, v9

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/alerts/slidingTabView/g;-><init>(Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;Landroid/view/ViewGroup;IIIIILcom/android/calendar/alerts/slidingTabView/i;)V

    iput-object v0, p0, Lcom/android/calendar/alerts/slidingTabView/AlarmSlidingTabForPopup;->n:Lcom/android/calendar/alerts/slidingTabView/c;

    .line 62
    return-void
.end method
