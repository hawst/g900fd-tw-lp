.class Lcom/android/calendar/dw;
.super Landroid/content/AsyncQueryHandler;
.source "FacebookEventSyncServiceICSUP.java"


# instance fields
.field a:Landroid/content/ContentResolver;

.field final synthetic b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 384
    iput-object p1, p0, Lcom/android/calendar/dw;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    .line 385
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/dw;->a:Landroid/content/ContentResolver;

    .line 382
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/dw;->c:I

    .line 386
    iput-object p2, p0, Lcom/android/calendar/dw;->a:Landroid/content/ContentResolver;

    .line 387
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 390
    iput p1, p0, Lcom/android/calendar/dw;->c:I

    .line 391
    return-void
.end method

.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 395
    const-string v0, "FBEventSyncService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FacebookPartialQueryHandler cursor = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    if-nez p3, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    if-eqz p3, :cond_4

    .line 400
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 402
    :cond_2
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 403
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 405
    const-string v1, "calendar_id"

    iget v2, p0, Lcom/android/calendar/dw;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    const-string v1, "eventTimezone"

    const-string v2, "UTC"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v1, "title"

    const-string v2, "title"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v1, "allDay"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 409
    const-string v1, "dtstart"

    const-string v2, "start_time"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 410
    const-string v1, "dtend"

    const-string v2, "end_time"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 411
    const-string v1, "description"

    const-string v2, "content"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v1, "eventLocation"

    const-string v2, "place"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    const-string v1, "facebook_schedule_id"

    const-string v2, "event_id"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 415
    const-string v1, "facebook_service_provider"

    const-string v2, "service_provider"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 417
    const-string v1, "facebook_owner"

    const-string v2, "owner"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const-string v1, "post_time"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 420
    const-string v1, "facebook_post_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    const-string v1, "photo_url"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 423
    const-string v2, "facebook_photo_url"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    iget-object v2, p0, Lcom/android/calendar/dw;->a:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 426
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 428
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 429
    iget-object v4, p0, Lcom/android/calendar/dw;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v4}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 433
    :cond_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 435
    iget-object v0, p0, Lcom/android/calendar/dw;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 436
    new-instance v0, Lcom/android/calendar/dv;

    iget-object v1, p0, Lcom/android/calendar/dw;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-direct {v0, v1}, Lcom/android/calendar/dv;-><init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)V

    .line 437
    invoke-virtual {v0}, Lcom/android/calendar/dv;->start()V

    .line 441
    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 442
    const-string v0, "FBEventSyncService"

    const-string v1, "FacebookPartialQueryHandler stopSelf"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v0, p0, Lcom/android/calendar/dw;->b:Lcom/android/calendar/FacebookEventSyncServiceICSUP;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->stopSelf()V

    goto/16 :goto_0
.end method
