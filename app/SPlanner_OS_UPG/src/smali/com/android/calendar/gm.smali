.class public Lcom/android/calendar/gm;
.super Ljava/lang/Object;
.source "SecDateUtils.java"


# static fields
.field private static a:Ljava/text/SimpleDateFormat;

.field private static b:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    .line 20
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 117
    sget-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    .line 119
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    .line 121
    :cond_0
    const/16 v1, 0x9

    invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->set(II)V

    .line 122
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    const-string v2, "a"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 123
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 135
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 136
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->set(II)V

    .line 137
    sget-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    .line 139
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    .line 141
    :cond_0
    if-nez p1, :cond_1

    .line 142
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    const-string v2, "EEEEE"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 150
    :goto_0
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 143
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 144
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    const-string v2, "EE"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 146
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    const-string v2, "EEE"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_3
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    const-string v2, "EEEE"

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(IIZ)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x2

    .line 31
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 32
    invoke-virtual {v3, v6, p0}, Ljava/util/Calendar;->set(II)V

    .line 33
    const/4 v0, 0x5

    const/16 v1, 0xf

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 34
    const-string v2, "MMM"

    .line 35
    const-string v1, "MMM"

    .line 36
    const-string v0, "MMMM"

    .line 38
    sget-object v4, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 39
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sput-object v4, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    .line 40
    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-direct {v4}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v4, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    .line 43
    :cond_0
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 44
    const/4 p2, 0x0

    .line 47
    :cond_1
    if-eqz p2, :cond_5

    .line 48
    const-string v2, "LLL"

    .line 49
    const-string v1, "LLL"

    .line 50
    const-string v0, "LLLL"

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 53
    :goto_0
    const/4 v4, 0x1

    if-ne p1, v4, :cond_3

    .line 54
    sget-object v0, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 60
    :goto_1
    sget-object v0, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {}, Lcom/android/calendar/hj;->l()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    sget-object v2, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 63
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    :cond_2
    return-object v0

    .line 55
    :cond_3
    if-ne p1, v6, :cond_4

    .line 56
    sget-object v2, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_1

    .line 58
    :cond_4
    sget-object v0, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 78
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 79
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 80
    const/4 v1, 0x6

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 82
    sget-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/gm;->b:Ljava/util/Locale;

    .line 84
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    .line 86
    :cond_0
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0487

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 87
    sget-object v1, Lcom/android/calendar/gm;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
