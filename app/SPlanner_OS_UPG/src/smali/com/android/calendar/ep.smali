.class Lcom/android/calendar/ep;
.super Lcom/android/calendar/ew;
.source "LinkActionChooserFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/eo;


# direct methods
.method constructor <init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/android/calendar/ep;->a:Lcom/android/calendar/eo;

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/ew;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 108
    :try_start_0
    const-string v0, "tel"

    invoke-virtual {p2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 109
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 110
    const-string v0, "withSpecialChar"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    iget-object v0, p0, Lcom/android/calendar/ep;->a:Lcom/android/calendar/eo;

    invoke-virtual {v0}, Lcom/android/calendar/eo;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f005f

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 114
    invoke-static {}, Lcom/android/calendar/eo;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkActionChooserFragment: Phone activity not found."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
