.class public Lcom/android/calendar/ex;
.super Ljava/lang/Object;
.source "LocationInfo.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ex;->a:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ex;->b:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/ex;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/ex;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 29
    invoke-virtual {p0}, Lcom/android/calendar/ex;->d()V

    .line 34
    iput-object p1, p0, Lcom/android/calendar/ex;->b:Ljava/lang/String;

    .line 37
    const-string v0, " / "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 39
    aget-object v0, v3, v2

    if-eqz v0, :cond_5

    aget-object v0, v3, v2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 40
    aget-object v0, v3, v2

    .line 43
    :goto_0
    array-length v4, v3

    if-le v4, v6, :cond_0

    aget-object v4, v3, v6

    if-eqz v4, :cond_0

    aget-object v4, v3, v6

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 45
    aget-object v1, v3, v6

    .line 49
    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 51
    const-string v3, "] "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 52
    aget-object v3, v0, v2

    if-eqz v3, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 53
    aget-object v3, v0, v2

    const-string v4, "["

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/ex;->a:Ljava/lang/String;

    .line 56
    :cond_1
    array-length v3, v0

    if-le v3, v6, :cond_2

    aget-object v3, v0, v6

    if-eqz v3, :cond_2

    aget-object v3, v0, v6

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 57
    aget-object v0, v0, v6

    iput-object v0, p0, Lcom/android/calendar/ex;->b:Ljava/lang/String;

    .line 62
    :cond_2
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 66
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    move v0, v2

    .line 68
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_4

    .line 69
    aget-object v2, v1, v0

    if-eqz v2, :cond_3

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 70
    iget-object v2, p0, Lcom/android/calendar/ex;->c:Ljava/util/ArrayList;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 75
    :cond_4
    return v6

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/android/calendar/ex;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/calendar/ex;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ex;->a:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ex;->b:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/android/calendar/ex;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/android/calendar/ex;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 96
    :cond_0
    return-void
.end method
