.class Lcom/android/calendar/da;
.super Lcom/android/calendar/ag;
.source "DeleteTaskHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/cz;


# direct methods
.method constructor <init>(Lcom/android/calendar/cz;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/calendar/da;->a:Lcom/android/calendar/cz;

    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 79
    if-nez p3, :cond_0

    .line 92
    :goto_0
    return-void

    .line 82
    :cond_0
    new-instance v0, Lcom/android/calendar/hh;

    iget-object v1, p0, Lcom/android/calendar/da;->a:Lcom/android/calendar/cz;

    invoke-static {v1}, Lcom/android/calendar/cz;->a(Lcom/android/calendar/cz;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/hh;-><init>(Landroid/content/Context;)V

    .line 83
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 84
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/android/calendar/hh;->b:J

    .line 90
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 91
    iget-object v1, p0, Lcom/android/calendar/da;->a:Lcom/android/calendar/cz;

    invoke-virtual {v1, v0}, Lcom/android/calendar/cz;->a(Lcom/android/calendar/hh;)V

    goto :goto_0

    .line 86
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 87
    const-string v1, "_id"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/android/calendar/hh;->b:J

    goto :goto_1
.end method
