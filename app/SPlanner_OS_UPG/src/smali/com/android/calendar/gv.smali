.class Lcom/android/calendar/gv;
.super Landroid/os/AsyncTask;
.source "ShareEventHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/gr;

.field private b:Landroid/app/Activity;

.field private c:Lcom/android/calendar/gu;

.field private d:Landroid/content/Intent;

.field private e:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Lcom/android/calendar/gr;Landroid/app/Activity;Lcom/android/calendar/gu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 204
    iput-object p1, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 205
    iput-object p2, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    .line 206
    iput-object p3, p0, Lcom/android/calendar/gv;->c:Lcom/android/calendar/gu;

    .line 207
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    .line 208
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    .line 209
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 210
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 211
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 212
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/gv;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Long;)Ljava/lang/Integer;
    .locals 14

    .prologue
    .line 216
    if-nez p1, :cond_0

    .line 217
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 308
    :goto_0
    return-object v0

    .line 219
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 220
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 221
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 222
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    array-length v3, p1

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 227
    const/4 v0, 0x0

    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_a

    .line 228
    array-length v3, p1

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 229
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    array-length v5, p1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/android/calendar/gv;->publishProgress([Ljava/lang/Object;)V

    .line 231
    :cond_1
    aget-object v3, p1, v0

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 232
    const-wide/16 v12, 0x0

    cmp-long v3, v4, v12

    if-gez v3, :cond_3

    const/4 v3, 0x1

    move v6, v3

    .line 233
    :goto_2
    if-eqz v6, :cond_4

    .line 234
    const-wide/16 v12, -0x1

    mul-long/2addr v4, v12

    .line 235
    add-int/lit8 v2, v2, 0x1

    .line 240
    :goto_3
    iget-object v3, p0, Lcom/android/calendar/gv;->c:Lcom/android/calendar/gu;

    sget-object v11, Lcom/android/calendar/gu;->e:Lcom/android/calendar/gu;

    if-ne v3, v11, :cond_6

    .line 241
    if-eqz v6, :cond_5

    iget-object v3, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    sget-object v6, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gr;->a(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v3

    .line 243
    :goto_4
    if-eqz v3, :cond_2

    .line 244
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_2
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 232
    :cond_3
    const/4 v3, 0x0

    move v6, v3

    goto :goto_2

    .line 237
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 241
    :cond_5
    iget-object v3, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gr;->b(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 245
    :cond_6
    iget-object v3, p0, Lcom/android/calendar/gv;->c:Lcom/android/calendar/gu;

    sget-object v11, Lcom/android/calendar/gu;->d:Lcom/android/calendar/gu;

    if-ne v3, v11, :cond_8

    .line 246
    if-eqz v6, :cond_7

    iget-object v3, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    sget-object v6, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gr;->a(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v3

    .line 248
    :goto_6
    if-eqz v3, :cond_2

    .line 249
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 246
    :cond_7
    iget-object v3, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/android/calendar/gr;->b(Lcom/android/calendar/gr;Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    .line 251
    :cond_8
    if-eqz v6, :cond_9

    sget-object v3, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    .line 252
    :goto_7
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 253
    iget-object v4, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-static {v4, v3, v6}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/net/Uri;

    move-result-object v4

    .line 255
    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    .line 256
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 251
    :cond_9
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_7

    .line 263
    :cond_a
    array-length v0, p1

    if-ne v1, v0, :cond_c

    const-string v0, "text/x-vCalendar"

    .line 267
    :goto_8
    sget-object v1, Lcom/android/calendar/gt;->a:[I

    iget-object v2, p0, Lcom/android/calendar/gv;->c:Lcom/android/calendar/gu;

    invoke-virtual {v2}, Lcom/android/calendar/gu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 270
    iget-object v1, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    .line 272
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    iget-object v1, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v2, "android.intent.extra.STREAM"

    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 278
    :goto_9
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 308
    :cond_b
    :goto_a
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 263
    :cond_c
    array-length v0, p1

    if-ne v2, v0, :cond_d

    const-string v0, "text/x-vtodo"

    goto :goto_8

    :cond_d
    const-string v0, "text/*"

    goto :goto_8

    .line 275
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_9

    .line 282
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "uristrings"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 284
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "contenturis"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 285
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 286
    iget-object v1, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_a

    .line 290
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "com.samsung.android.sconnect.START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 292
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_a

    .line 296
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_a

    .line 303
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    iget-object v0, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    const-string v1, "result"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_a

    .line 267
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 323
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getMax()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 328
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 333
    :cond_2
    sget-object v0, Lcom/android/calendar/gt;->a:[I

    iget-object v1, p0, Lcom/android/calendar/gv;->c:Lcom/android/calendar/gu;

    invoke-virtual {v1}, Lcom/android/calendar/gu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 337
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHOOSER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 338
    const-string v1, "android.intent.extra.INTENT"

    iget-object v2, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 339
    const-string v1, "android.intent.extra.TITLE"

    iget-object v2, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    const v3, 0x7f0f03d8

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 340
    iget-object v1, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 357
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    invoke-static {v0}, Lcom/android/calendar/gr;->a(Lcom/android/calendar/gr;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    invoke-static {v0}, Lcom/android/calendar/gr;->a(Lcom/android/calendar/gr;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/gv;->a:Lcom/android/calendar/gr;

    invoke-static {v0}, Lcom/android/calendar/gr;->b(Lcom/android/calendar/gr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/gw;

    invoke-direct {v1, p0}, Lcom/android/calendar/gw;-><init>(Lcom/android/calendar/gv;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 345
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 350
    :pswitch_2
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/gv;->d:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 351
    :catch_0
    move-exception v0

    .line 352
    invoke-static {}, Lcom/android/calendar/gr;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unable to find S Connect activity"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 333
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected varargs a([Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 313
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 317
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 198
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/android/calendar/gv;->a([Ljava/lang/Long;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 372
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 373
    iget-object v0, p0, Lcom/android/calendar/gv;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/android/calendar/gv;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 198
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/gv;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 198
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/gv;->a([Ljava/lang/Integer;)V

    return-void
.end method
