.class Lcom/android/calendar/an;
.super Landroid/os/CountDownTimer;
.source "CalendarController.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/aq;

.field final synthetic b:Lcom/android/calendar/al;


# direct methods
.method constructor <init>(Lcom/android/calendar/al;JJLcom/android/calendar/aq;)V
    .locals 0

    .prologue
    .line 889
    iput-object p1, p0, Lcom/android/calendar/an;->b:Lcom/android/calendar/al;

    iput-object p6, p0, Lcom/android/calendar/an;->a:Lcom/android/calendar/aq;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 6

    .prologue
    .line 893
    iget-object v0, p0, Lcom/android/calendar/an;->b:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/an;->a:Lcom/android/calendar/aq;

    iget-object v2, p0, Lcom/android/calendar/an;->a:Lcom/android/calendar/aq;

    iget-object v2, v2, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/an;->a:Lcom/android/calendar/aq;

    iget-object v4, v4, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-nez v4, :cond_0

    const-wide/16 v4, -0x1

    :goto_0
    invoke-static/range {v0 .. v5}, Lcom/android/calendar/al;->a(Lcom/android/calendar/al;Lcom/android/calendar/aq;JJ)V

    .line 894
    return-void

    .line 893
    :cond_0
    iget-object v4, p0, Lcom/android/calendar/an;->a:Lcom/android/calendar/aq;

    iget-object v4, v4, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    goto :goto_0
.end method

.method public onTick(J)V
    .locals 0

    .prologue
    .line 897
    return-void
.end method
