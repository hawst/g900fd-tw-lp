.class Lcom/android/calendar/bl;
.super Landroid/os/Handler;
.source "ContactsAsyncHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/bi;


# direct methods
.method public constructor <init>(Lcom/android/calendar/bi;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/android/calendar/bl;->a:Lcom/android/calendar/bi;

    .line 81
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 82
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 86
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/calendar/bk;

    .line 88
    iget v1, p1, Landroid/os/Message;->arg1:I

    packed-switch v1, :pswitch_data_0

    .line 115
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/bl;->a:Lcom/android/calendar/bi;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/bi;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 116
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 117
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 118
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 119
    return-void

    .line 93
    :pswitch_0
    :try_start_0
    iget-object v1, v0, Lcom/android/calendar/bk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v3, v0, Lcom/android/calendar/bk;->c:Landroid/net/Uri;

    invoke-static {v1, v3}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 99
    :goto_1
    if-eqz v1, :cond_0

    .line 100
    iget-object v2, v0, Lcom/android/calendar/bk;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    goto :goto_0

    .line 95
    :catch_0
    move-exception v1

    .line 96
    const-string v3, "ContactsAsyncHelper"

    const-string v4, "Error opening photo input stream"

    invoke-static {v3, v4, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v2

    goto :goto_1

    .line 105
    :cond_0
    iput-object v2, v0, Lcom/android/calendar/bk;->e:Ljava/lang/Object;

    goto :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
