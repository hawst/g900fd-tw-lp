.class Lcom/android/calendar/ds;
.super Landroid/content/AsyncQueryHandler;
.source "FacebookEventSyncService.java"


# instance fields
.field a:Landroid/content/ContentResolver;

.field final synthetic b:Lcom/android/calendar/FacebookEventSyncService;


# direct methods
.method public constructor <init>(Lcom/android/calendar/FacebookEventSyncService;Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 297
    iput-object p1, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    .line 298
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    .line 299
    iput-object p2, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    .line 300
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 315
    .line 316
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncService;->b()J

    move-result-wide v0

    .line 318
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 319
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 329
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 325
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->a(Lcom/android/calendar/FacebookEventSyncService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    new-instance v0, Lcom/android/calendar/dt;

    iget-object v1, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-direct {v0, v1, v6}, Lcom/android/calendar/dt;-><init>(Lcom/android/calendar/FacebookEventSyncService;Lcom/android/calendar/dq;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/calendar/dt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 328
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncService;->stopSelf()V

    goto :goto_0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 332
    if-eqz p1, :cond_0

    .line 333
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-virtual {v0}, Lcom/android/calendar/FacebookEventSyncService;->b()J

    move-result-wide v0

    long-to-int v9, v0

    .line 337
    const/4 v0, -0x1

    if-ne v9, v0, :cond_2

    .line 402
    :cond_1
    :goto_0
    return-void

    .line 344
    :cond_2
    const-string v3, " calendar_id = ? "

    .line 347
    iget-object v0, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 352
    iget-object v3, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/calendar/hj;->c:Landroid/net/Uri;

    sget-object v5, Lcom/android/calendar/hj;->K:[Ljava/lang/String;

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 355
    if-eqz v10, :cond_3

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, v0, Lcom/android/calendar/FacebookEventSyncService;->a:I

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/android/calendar/dr;->a(I)V

    .line 360
    sget-object v3, Lcom/android/calendar/hj;->c:Landroid/net/Uri;

    .line 362
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    iget v0, v0, Lcom/android/calendar/FacebookEventSyncService;->a:I

    if-lez v0, :cond_a

    if-eqz v8, :cond_a

    .line 363
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    iget v1, v1, Lcom/android/calendar/FacebookEventSyncService;->a:I

    if-le v0, v1, :cond_6

    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " updated_time DESC limit "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    iget v2, v2, Lcom/android/calendar/FacebookEventSyncService;->a:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 366
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/hj;->K:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    :cond_4
    :goto_1
    if-eqz v10, :cond_5

    .line 395
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 397
    :cond_5
    if-eqz v8, :cond_1

    .line 398
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 370
    :cond_6
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    iget v1, v1, Lcom/android/calendar/FacebookEventSyncService;->a:I

    if-ge v0, v1, :cond_9

    .line 371
    iget-object v0, p0, Lcom/android/calendar/ds;->a:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calendar_id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 373
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/hj;->K:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 394
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_7

    .line 395
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 397
    :cond_7
    if-eqz v8, :cond_8

    .line 398
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 378
    :cond_9
    :try_start_2
    const-string v7, " updated_time DESC limit 1 "

    .line 379
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/hj;->K:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 385
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    iget v0, v0, Lcom/android/calendar/FacebookEventSyncService;->a:I

    if-nez v0, :cond_4

    .line 386
    iget-object v0, p0, Lcom/android/calendar/ds;->b:Lcom/android/calendar/FacebookEventSyncService;

    invoke-static {v0}, Lcom/android/calendar/FacebookEventSyncService;->b(Lcom/android/calendar/FacebookEventSyncService;)Lcom/android/calendar/dr;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/hj;->K:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dr;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 304
    if-nez p3, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 308
    invoke-direct {p0, p3}, Lcom/android/calendar/ds;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 309
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 310
    invoke-direct {p0, p3}, Lcom/android/calendar/ds;->b(Landroid/database/Cursor;)V

    goto :goto_0
.end method
