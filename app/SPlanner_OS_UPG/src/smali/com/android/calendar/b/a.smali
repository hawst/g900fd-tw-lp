.class public final Lcom/android/calendar/b/a;
.super Landroid/text/format/Time;
.source "LTime.java"


# instance fields
.field public a:Z

.field public b:Z

.field private c:Lcom/android/calendar/d/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->a:Z

    .line 41
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->b:Z

    .line 51
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/b/a;->c:Lcom/android/calendar/d/g;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 39
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->a:Z

    .line 41
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->b:Z

    .line 71
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/b/a;->c:Lcom/android/calendar/d/g;

    .line 73
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 39
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->a:Z

    .line 41
    iput-boolean v0, p0, Lcom/android/calendar/b/a;->b:Z

    .line 61
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/b/a;->c:Lcom/android/calendar/d/g;

    .line 63
    return-void
.end method


# virtual methods
.method public getActualMaximum(I)I
    .locals 4

    .prologue
    .line 81
    .line 83
    iget-boolean v0, p0, Lcom/android/calendar/b/a;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    .line 85
    :cond_0
    invoke-super {p0, p1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 95
    :goto_0
    return v0

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/b/a;->c:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/b/a;->year:I

    iget v2, p0, Lcom/android/calendar/b/a;->month:I

    iget-boolean v3, p0, Lcom/android/calendar/b/a;->b:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/d/a/a;->a(IIZ)I

    move-result v0

    goto :goto_0
.end method

.method public set(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 109
    instance-of v0, p1, Lcom/android/calendar/b/a;

    if-eqz v0, :cond_0

    .line 111
    check-cast p1, Lcom/android/calendar/b/a;

    .line 113
    iget-boolean v0, p1, Lcom/android/calendar/b/a;->a:Z

    iput-boolean v0, p0, Lcom/android/calendar/b/a;->a:Z

    .line 115
    iget-boolean v0, p1, Lcom/android/calendar/b/a;->b:Z

    iput-boolean v0, p0, Lcom/android/calendar/b/a;->b:Z

    .line 119
    :cond_0
    return-void
.end method
