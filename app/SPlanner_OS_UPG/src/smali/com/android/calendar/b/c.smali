.class public Lcom/android/calendar/b/c;
.super Ljava/lang/Object;
.source "LunarRecurrenceProcessor.java"


# instance fields
.field private a:Lcom/android/a/c;

.field private b:I

.field private c:Lcom/android/calendar/b/a;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    new-instance v0, Lcom/android/calendar/b/a;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/calendar/b/a;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/b/c;->c:Lcom/android/calendar/b/a;

    .line 319
    return-void
.end method

.method private static a(Landroid/text/format/Time;Lcom/android/a/c;)I
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 578
    .line 585
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v6

    .line 588
    iget v7, p1, Lcom/android/a/c;->o:I

    .line 589
    if-lez v7, :cond_7

    .line 591
    iget v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 592
    :goto_0
    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 593
    add-int/lit8 v0, v0, -0x7

    goto :goto_0

    .line 595
    :cond_0
    iget v1, p0, Landroid/text/format/Time;->weekDay:I

    .line 596
    if-lt v1, v0, :cond_2

    .line 597
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    .line 606
    :goto_1
    iget-object v8, p1, Lcom/android/a/c;->m:[I

    .line 607
    iget-object v9, p1, Lcom/android/a/c;->n:[I

    move v5, v2

    move v1, v2

    .line 608
    :goto_2
    if-ge v5, v7, :cond_8

    .line 609
    aget v10, v9, v5

    .line 610
    aget v3, v8, v5

    invoke-static {v3}, Lcom/android/a/c;->c(I)I

    move-result v3

    sub-int/2addr v3, v0

    add-int/lit8 v3, v3, 0x1

    .line 611
    if-gtz v3, :cond_1

    .line 612
    add-int/lit8 v3, v3, 0x7

    .line 614
    :cond_1
    if-nez v10, :cond_3

    .line 616
    :goto_3
    if-gt v3, v6, :cond_4

    .line 619
    shl-int v10, v4, v3

    or-int/2addr v1, v10

    .line 616
    add-int/lit8 v3, v3, 0x7

    goto :goto_3

    .line 599
    :cond_2
    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x8

    goto :goto_1

    .line 622
    :cond_3
    if-lez v10, :cond_5

    .line 625
    add-int/lit8 v10, v10, -0x1

    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v3, v10

    .line 626
    if-gt v3, v6, :cond_4

    .line 630
    shl-int v3, v4, v3

    or-int/2addr v1, v3

    .line 608
    :cond_4
    :goto_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 636
    :cond_5
    :goto_5
    if-gt v3, v6, :cond_6

    add-int/lit8 v3, v3, 0x7

    goto :goto_5

    .line 642
    :cond_6
    mul-int/lit8 v10, v10, 0x7

    add-int/2addr v3, v10

    .line 643
    if-lt v3, v4, :cond_4

    .line 646
    shl-int v3, v4, v3

    or-int/2addr v1, v3

    goto :goto_4

    :cond_7
    move v1, v2

    .line 656
    :cond_8
    iget v0, p1, Lcom/android/a/c;->b:I

    const/4 v3, 0x5

    if-le v0, v3, :cond_f

    .line 657
    iget v5, p1, Lcom/android/a/c;->q:I

    .line 658
    if-eqz v5, :cond_f

    .line 659
    iget-object v7, p1, Lcom/android/a/c;->p:[I

    .line 660
    iget v0, p1, Lcom/android/a/c;->o:I

    if-nez v0, :cond_b

    move v0, v1

    .line 661
    :goto_6
    if-ge v2, v5, :cond_10

    .line 662
    aget v1, v7, v2

    .line 663
    if-ltz v1, :cond_a

    .line 664
    shl-int v1, v4, v1

    or-int/2addr v0, v1

    .line 661
    :cond_9
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 666
    :cond_a
    add-int/2addr v1, v6

    add-int/lit8 v1, v1, 0x1

    .line 667
    if-lt v1, v4, :cond_9

    if-gt v1, v6, :cond_9

    .line 668
    shl-int v1, v4, v1

    or-int/2addr v0, v1

    goto :goto_7

    :cond_b
    move v3, v4

    move v0, v1

    .line 675
    :goto_8
    if-gt v3, v6, :cond_10

    .line 677
    shl-int v1, v4, v3

    and-int/2addr v1, v0

    if-eqz v1, :cond_c

    move v1, v2

    .line 678
    :goto_9
    if-ge v1, v5, :cond_e

    .line 679
    aget v8, v7, v1

    if-ne v8, v3, :cond_d

    .line 675
    :cond_c
    :goto_a
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_8

    .line 678
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 683
    :cond_e
    shl-int v1, v4, v3

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    goto :goto_a

    :cond_f
    move v0, v1

    .line 690
    :cond_10
    return v0
.end method


# virtual methods
.method a(Landroid/text/format/Time;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v0, 0x1

    .line 330
    .line 331
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->x:[I

    aget v1, v1, v3

    if-ne v1, v0, :cond_3

    .line 332
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 333
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 334
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 335
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eqz v2, :cond_1

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v4, :cond_1

    .line 336
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 412
    :cond_0
    :goto_0
    return v0

    .line 338
    :cond_1
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_2

    .line 339
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 340
    :cond_2
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_0

    .line 341
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 344
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->x:[I

    aget v1, v1, v3

    if-ne v1, v6, :cond_8

    .line 345
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 346
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 347
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 348
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v0, :cond_4

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v6, :cond_4

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v5, :cond_5

    .line 350
    :cond_4
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_5
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_6

    .line 353
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 354
    :cond_6
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_7

    .line 355
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 356
    :cond_7
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 357
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 360
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->x:[I

    aget v1, v1, v3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_e

    .line 361
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 362
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 363
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 364
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v0, :cond_9

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v6, :cond_9

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_a

    .line 365
    :cond_9
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 367
    :cond_a
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v5, :cond_b

    .line 368
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_0

    .line 369
    :cond_b
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_c

    .line 370
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_0

    .line 371
    :cond_c
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_d

    .line 372
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_0

    .line 373
    :cond_d
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 374
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x3

    goto/16 :goto_0

    .line 377
    :cond_e
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->x:[I

    aget v1, v1, v3

    if-ne v1, v5, :cond_15

    .line 378
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 379
    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 380
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 381
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v0, :cond_f

    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v6, :cond_10

    .line 382
    :cond_f
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x3

    goto/16 :goto_0

    .line 384
    :cond_10
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_11

    .line 385
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x5

    goto/16 :goto_0

    .line 386
    :cond_11
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v5, :cond_12

    .line 387
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x5

    goto/16 :goto_0

    .line 388
    :cond_12
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_13

    .line 389
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x5

    goto/16 :goto_0

    .line 390
    :cond_13
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v4, :cond_14

    .line 391
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x5

    goto/16 :goto_0

    .line 392
    :cond_14
    iget v2, v1, Landroid/text/format/Time;->weekDay:I

    if-nez v2, :cond_0

    .line 393
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, 0x4

    goto/16 :goto_0

    .line 396
    :cond_15
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v1, v1, Lcom/android/a/c;->x:[I

    aget v1, v1, v3

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 397
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, p1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 399
    invoke-virtual {p1, v5}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    .line 400
    iput v1, v2, Landroid/text/format/Time;->monthDay:I

    .line 401
    invoke-static {v2}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 402
    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eqz v3, :cond_16

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    if-eq v3, v4, :cond_16

    move v0, v1

    .line 403
    goto/16 :goto_0

    .line 405
    :cond_16
    iget v1, v2, Landroid/text/format/Time;->weekDay:I

    if-nez v1, :cond_17

    .line 406
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x2

    goto/16 :goto_0

    .line 407
    :cond_17
    iget v1, v2, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v4, :cond_0

    .line 408
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0
.end method

.method a(Lcom/android/calendar/b/a;)I
    .locals 2

    .prologue
    .line 416
    .line 417
    iget v0, p1, Lcom/android/calendar/b/a;->weekDay:I

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/calendar/b/a;->weekDay:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 418
    :cond_0
    const/4 v0, 0x1

    .line 422
    :goto_0
    return v0

    .line 420
    :cond_1
    iget v0, p1, Lcom/android/calendar/b/a;->weekDay:I

    rsub-int/lit8 v0, v0, 0x7

    goto :goto_0
.end method

.method a(Lcom/android/a/c;)V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/b/c;->d:I

    .line 324
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/b/c;->e:I

    .line 325
    iput-object p1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    .line 326
    return-void
.end method

.method a(Lcom/android/calendar/b/a;I)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 508
    iget v2, p1, Lcom/android/calendar/b/a;->year:I

    .line 509
    iget v1, p1, Lcom/android/calendar/b/a;->month:I

    .line 511
    const/4 v0, 0x0

    .line 519
    if-lt p2, v3, :cond_0

    const/16 v4, 0x1c

    if-le p2, v4, :cond_1

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/b/c;->c:Lcom/android/calendar/b/a;

    .line 522
    invoke-virtual {v0, p2, v1, v2}, Lcom/android/calendar/b/a;->set(III)V

    .line 524
    iget-boolean v1, p1, Lcom/android/calendar/b/a;->a:Z

    iput-boolean v1, v0, Lcom/android/calendar/b/a;->a:Z

    .line 525
    iget-boolean v1, p1, Lcom/android/calendar/b/a;->b:Z

    iput-boolean v1, v0, Lcom/android/calendar/b/a;->b:Z

    .line 526
    invoke-static {v0}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 527
    iget v2, v0, Lcom/android/calendar/b/a;->year:I

    .line 528
    iget v1, v0, Lcom/android/calendar/b/a;->month:I

    .line 529
    iget p2, v0, Lcom/android/calendar/b/a;->monthDay:I

    .line 542
    :cond_1
    iget v4, p0, Lcom/android/calendar/b/c;->d:I

    if-ne v2, v4, :cond_2

    iget v4, p0, Lcom/android/calendar/b/c;->e:I

    if-eq v1, v4, :cond_4

    .line 543
    :cond_2
    if-nez v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/android/calendar/b/c;->c:Lcom/android/calendar/b/a;

    .line 545
    invoke-virtual {v0, p2, v1, v2}, Lcom/android/calendar/b/a;->set(III)V

    .line 547
    iget-boolean v4, p1, Lcom/android/calendar/b/a;->a:Z

    iput-boolean v4, v0, Lcom/android/calendar/b/a;->a:Z

    .line 548
    iget-boolean v4, p1, Lcom/android/calendar/b/a;->b:Z

    iput-boolean v4, v0, Lcom/android/calendar/b/a;->b:Z

    .line 549
    invoke-static {v0}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 557
    :cond_3
    iput v2, p0, Lcom/android/calendar/b/c;->d:I

    .line 558
    iput v1, p0, Lcom/android/calendar/b/c;->e:I

    .line 559
    iget-object v1, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    invoke-static {v0, v1}, Lcom/android/calendar/b/c;->a(Landroid/text/format/Time;Lcom/android/a/c;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/b/c;->b:I

    .line 564
    :cond_4
    iget v0, p0, Lcom/android/calendar/b/c;->b:I

    shl-int v1, v3, p2

    and-int/2addr v0, v1

    if-eqz v0, :cond_5

    move v0, v3

    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Landroid/text/format/Time;)I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 468
    .line 470
    new-instance v1, Lcom/android/calendar/b/a;

    invoke-direct {v1, p1}, Lcom/android/calendar/b/a;-><init>(Landroid/text/format/Time;)V

    .line 471
    iput v0, v1, Lcom/android/calendar/b/a;->monthDay:I

    .line 472
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 474
    iget-object v2, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v2, v2, Lcom/android/a/c;->x:[I

    aget v2, v2, v4

    if-ne v2, v0, :cond_1

    .line 475
    invoke-virtual {p0, v1}, Lcom/android/calendar/b/c;->a(Lcom/android/calendar/b/a;)I

    move-result v0

    .line 497
    :cond_0
    :goto_0
    return v0

    .line 477
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v2, v2, Lcom/android/a/c;->x:[I

    aget v2, v2, v4

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 478
    invoke-virtual {p0, v1}, Lcom/android/calendar/b/c;->b(Lcom/android/calendar/b/a;)I

    move-result v0

    goto :goto_0

    .line 480
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v2, v2, Lcom/android/a/c;->x:[I

    aget v2, v2, v4

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 481
    invoke-virtual {p0, v1}, Lcom/android/calendar/b/c;->c(Lcom/android/calendar/b/a;)I

    move-result v0

    goto :goto_0

    .line 483
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v2, v2, Lcom/android/a/c;->x:[I

    aget v2, v2, v4

    if-ne v2, v5, :cond_4

    .line 484
    invoke-virtual {p0, v1}, Lcom/android/calendar/b/c;->d(Lcom/android/calendar/b/a;)I

    move-result v0

    goto :goto_0

    .line 486
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/b/c;->a:Lcom/android/a/c;

    iget-object v2, v2, Lcom/android/a/c;->x:[I

    aget v2, v2, v4

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 487
    invoke-virtual {p1, v5}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 488
    iput v0, v1, Lcom/android/calendar/b/a;->monthDay:I

    .line 489
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 490
    iget v2, v1, Lcom/android/calendar/b/a;->weekDay:I

    if-eqz v2, :cond_0

    iget v2, v1, Lcom/android/calendar/b/a;->weekDay:I

    const/4 v3, 0x6

    if-eq v2, v3, :cond_0

    .line 493
    iget v1, v1, Lcom/android/calendar/b/a;->weekDay:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method b(Lcom/android/calendar/b/a;)I
    .locals 4

    .prologue
    .line 426
    .line 427
    invoke-virtual {p0, p1}, Lcom/android/calendar/b/c;->a(Lcom/android/calendar/b/a;)I

    move-result v0

    .line 428
    new-instance v1, Lcom/android/calendar/b/a;

    invoke-direct {v1, p1}, Lcom/android/calendar/b/a;-><init>(Landroid/text/format/Time;)V

    .line 429
    iput v0, v1, Lcom/android/calendar/b/a;->monthDay:I

    .line 430
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 431
    iget v2, v1, Lcom/android/calendar/b/a;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 432
    add-int/lit8 v0, v0, 0x1

    .line 436
    :cond_0
    :goto_0
    return v0

    .line 433
    :cond_1
    iget v1, v1, Lcom/android/calendar/b/a;->weekDay:I

    if-nez v1, :cond_0

    .line 434
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method c(Lcom/android/calendar/b/a;)I
    .locals 4

    .prologue
    .line 440
    .line 441
    invoke-virtual {p0, p1}, Lcom/android/calendar/b/c;->b(Lcom/android/calendar/b/a;)I

    move-result v0

    .line 442
    new-instance v1, Lcom/android/calendar/b/a;

    invoke-direct {v1, p1}, Lcom/android/calendar/b/a;-><init>(Landroid/text/format/Time;)V

    .line 443
    iput v0, v1, Lcom/android/calendar/b/a;->monthDay:I

    .line 444
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 445
    iget v2, v1, Lcom/android/calendar/b/a;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 446
    add-int/lit8 v0, v0, 0x1

    .line 450
    :cond_0
    :goto_0
    return v0

    .line 447
    :cond_1
    iget v1, v1, Lcom/android/calendar/b/a;->weekDay:I

    if-nez v1, :cond_0

    .line 448
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method d(Lcom/android/calendar/b/a;)I
    .locals 4

    .prologue
    .line 454
    .line 455
    invoke-virtual {p0, p1}, Lcom/android/calendar/b/c;->c(Lcom/android/calendar/b/a;)I

    move-result v0

    .line 456
    new-instance v1, Lcom/android/calendar/b/a;

    invoke-direct {v1, p1}, Lcom/android/calendar/b/a;-><init>(Landroid/text/format/Time;)V

    .line 457
    iput v0, v1, Lcom/android/calendar/b/a;->monthDay:I

    .line 458
    invoke-static {v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 459
    iget v2, v1, Lcom/android/calendar/b/a;->weekDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    .line 460
    add-int/lit8 v0, v0, 0x1

    .line 464
    :cond_0
    :goto_0
    return v0

    .line 461
    :cond_1
    iget v1, v1, Lcom/android/calendar/b/a;->weekDay:I

    if-nez v1, :cond_0

    .line 462
    add-int/lit8 v0, v0, 0x6

    goto :goto_0
.end method

.method e(Lcom/android/calendar/b/a;)I
    .locals 1

    .prologue
    .line 502
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/android/calendar/b/a;->getActualMaximum(I)I

    move-result v0

    .line 503
    return v0
.end method
