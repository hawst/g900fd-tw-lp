.class public Lcom/android/calendar/b/b;
.super Ljava/lang/Object;
.source "LunarRecurrenceProcessor.java"


# static fields
.field private static n:Lcom/android/calendar/d/g;

.field private static o:Lcom/android/calendar/d/a/b;

.field private static final p:[I

.field private static final q:[I


# instance fields
.field private a:Lcom/android/calendar/b/a;

.field private b:Landroid/text/format/Time;

.field private c:Ljava/lang/StringBuilder;

.field private d:Lcom/android/calendar/b/a;

.field private e:Lcom/android/calendar/b/c;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 1588
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/b/b;->p:[I

    .line 1590
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/calendar/b/b;->q:[I

    return-void

    .line 1588
    nop

    :array_0
    .array-data 4
        0x1f
        0x1c
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
    .end array-data

    .line 1590
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb4
        0xd4
        0xf3
        0x111
        0x130
        0x14e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/android/calendar/b/a;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/calendar/b/a;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    .line 36
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/b/b;->b:Landroid/text/format/Time;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/b/b;->c:Ljava/lang/StringBuilder;

    .line 38
    new-instance v0, Lcom/android/calendar/b/a;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Lcom/android/calendar/b/a;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/b/b;->d:Lcom/android/calendar/b/a;

    .line 39
    new-instance v0, Lcom/android/calendar/b/c;

    invoke-direct {v0, v2}, Lcom/android/calendar/b/c;-><init>(Z)V

    iput-object v0, p0, Lcom/android/calendar/b/b;->e:Lcom/android/calendar/b/c;

    .line 41
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->f:Z

    .line 42
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->g:Z

    .line 43
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->h:Z

    .line 44
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->i:Z

    .line 45
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->j:Z

    .line 46
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->k:Z

    .line 47
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->l:Z

    .line 48
    iput-boolean v2, p0, Lcom/android/calendar/b/b;->m:Z

    .line 54
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/b/b;->n:Lcom/android/calendar/d/g;

    .line 55
    sget-object v0, Lcom/android/calendar/b/b;->n:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->b()Lcom/android/calendar/d/a/b;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    .line 56
    return-void
.end method

.method static a(II)I
    .locals 2

    .prologue
    const/16 v0, 0x1c

    .line 1601
    sget-object v1, Lcom/android/calendar/b/b;->p:[I

    aget v1, v1, p1

    .line 1602
    if-eq v1, v0, :cond_1

    move v0, v1

    .line 1605
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/android/calendar/b/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x1d

    goto :goto_0
.end method

.method static a(III)I
    .locals 2

    .prologue
    .line 1638
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 1639
    add-int/lit8 p1, p1, 0xc

    .line 1640
    add-int/lit8 p0, p0, -0x1

    .line 1642
    :cond_0
    mul-int/lit8 v0, p1, 0xd

    add-int/lit8 v0, v0, -0xe

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, p2

    add-int/2addr v0, p0

    div-int/lit8 v1, p0, 0x4

    add-int/2addr v0, v1

    div-int/lit8 v1, p0, 0x64

    sub-int/2addr v0, v1

    div-int/lit16 v1, p0, 0x190

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    return v0
.end method

.method static a(IIZ)I
    .locals 3

    .prologue
    .line 1616
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v0, p0, -0x759

    .line 1617
    sget-object v1, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v0, v0, 0xe

    .line 1618
    sget-object v1, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    sget-object v2, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v2, v0, 0xd

    invoke-virtual {v1, v2}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v1

    .line 1621
    if-nez p2, :cond_0

    if-ge p1, v1, :cond_0

    .line 1622
    sget-object v1, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    add-int/2addr v0, p1

    invoke-virtual {v1, v0}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v0

    .line 1624
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v0

    goto :goto_0
.end method

.method private static a(Lcom/android/a/c;Landroid/text/format/Time;)I
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 198
    iget v5, p0, Lcom/android/a/c;->b:I

    .line 200
    const/4 v6, 0x6

    if-lt v6, v5, :cond_0

    .line 202
    iget v6, p0, Lcom/android/a/c;->w:I

    if-lez v6, :cond_0

    .line 203
    iget-object v6, p0, Lcom/android/a/c;->v:[I

    iget v7, p0, Lcom/android/a/c;->w:I

    iget v8, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/android/calendar/b/b;->a([III)Z

    move-result v6

    .line 205
    if-nez v6, :cond_0

    .line 286
    :goto_0
    return v0

    .line 210
    :cond_0
    const/4 v6, 0x5

    if-lt v6, v5, :cond_1

    .line 213
    iget v6, p0, Lcom/android/a/c;->u:I

    if-lez v6, :cond_1

    .line 214
    iget-object v6, p0, Lcom/android/a/c;->t:[I

    iget v7, p0, Lcom/android/a/c;->u:I

    invoke-virtual {p1}, Landroid/text/format/Time;->getWeekNumber()I

    move-result v8

    const/16 v9, 0x9

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v6

    .line 217
    if-nez v6, :cond_1

    move v0, v1

    .line 218
    goto :goto_0

    .line 222
    :cond_1
    if-lt v3, v5, :cond_4

    .line 224
    iget v6, p0, Lcom/android/a/c;->s:I

    if-lez v6, :cond_2

    .line 225
    iget-object v6, p0, Lcom/android/a/c;->r:[I

    iget v7, p0, Lcom/android/a/c;->s:I

    iget v8, p1, Landroid/text/format/Time;->yearDay:I

    const/16 v9, 0x8

    invoke-virtual {p1, v9}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v6

    .line 227
    if-nez v6, :cond_2

    move v0, v2

    .line 228
    goto :goto_0

    .line 232
    :cond_2
    iget v6, p0, Lcom/android/a/c;->q:I

    if-lez v6, :cond_3

    .line 233
    iget-object v6, p0, Lcom/android/a/c;->p:[I

    iget v7, p0, Lcom/android/a/c;->q:I

    iget v8, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p1, v3}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v9

    invoke-static {v6, v7, v8, v9}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v6

    .line 236
    if-nez v6, :cond_3

    move v0, v3

    .line 237
    goto :goto_0

    .line 243
    :cond_3
    iget v3, p0, Lcom/android/a/c;->o:I

    if-lez v3, :cond_4

    .line 244
    iget-object v6, p0, Lcom/android/a/c;->m:[I

    .line 245
    iget v7, p0, Lcom/android/a/c;->o:I

    .line 246
    iget v3, p1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v3}, Lcom/android/a/c;->b(I)I

    move-result v8

    move v3, v4

    .line 247
    :goto_1
    if-ge v3, v7, :cond_6

    .line 248
    aget v9, v6, v3

    if-ne v9, v8, :cond_5

    .line 255
    :cond_4
    if-lt v2, v5, :cond_7

    .line 257
    iget-object v3, p0, Lcom/android/a/c;->k:[I

    iget v6, p0, Lcom/android/a/c;->l:I

    iget v7, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v2

    invoke-static {v3, v6, v7, v2}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v2

    .line 260
    if-nez v2, :cond_7

    .line 261
    const/4 v0, 0x6

    goto :goto_0

    .line 247
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 252
    :cond_6
    const/4 v0, 0x5

    goto :goto_0

    .line 264
    :cond_7
    if-lt v1, v5, :cond_8

    .line 266
    iget-object v2, p0, Lcom/android/a/c;->i:[I

    iget v3, p0, Lcom/android/a/c;->j:I

    iget v6, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v1

    invoke-static {v2, v3, v6, v1}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v1

    .line 269
    if-nez v1, :cond_8

    .line 270
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 273
    :cond_8
    if-lt v0, v5, :cond_9

    .line 275
    iget-object v1, p0, Lcom/android/a/c;->g:[I

    iget v2, p0, Lcom/android/a/c;->h:I

    iget v3, p1, Landroid/text/format/Time;->second:I

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Lcom/android/calendar/b/b;->a([IIII)Z

    move-result v0

    .line 278
    if-nez v0, :cond_9

    .line 279
    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_9
    move v0, v4

    .line 286
    goto/16 :goto_0
.end method

.method static a(Landroid/text/format/Time;Z)Landroid/text/format/Time;
    .locals 4

    .prologue
    .line 1429
    sget-object v0, Lcom/android/calendar/b/b;->n:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    .line 1430
    iget v1, p0, Landroid/text/format/Time;->year:I

    iget v2, p0, Landroid/text/format/Time;->month:I

    iget v3, p0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/calendar/d/a/a;->a(IIIZ)V

    .line 1432
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1433
    invoke-virtual {v1, p0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1434
    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->a()I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->year:I

    .line 1435
    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->b()I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 1436
    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->c()I

    move-result v0

    iput v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1438
    return-object v1
.end method

.method static a(Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 1550
    new-instance v0, Lcom/android/calendar/b/a;

    invoke-direct {v0}, Lcom/android/calendar/b/a;-><init>()V

    .line 1551
    invoke-virtual {v0, p0}, Lcom/android/calendar/b/a;->set(Landroid/text/format/Time;)V

    .line 1553
    invoke-static {v0}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;)V

    .line 1554
    return-void
.end method

.method private static final a(Landroid/text/format/Time;J)V
    .locals 3

    .prologue
    .line 1682
    const/16 v0, 0x1a

    shr-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->year:I

    .line 1683
    const/16 v0, 0x16

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Landroid/text/format/Time;->month:I

    .line 1684
    const/16 v0, 0x11

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->monthDay:I

    .line 1685
    const/16 v0, 0xc

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x1f

    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 1686
    const/4 v0, 0x6

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit8 v0, v0, 0x3f

    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 1687
    const-wide/16 v0, 0x3f

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 1688
    return-void
.end method

.method static a(Lcom/android/calendar/b/a;)V
    .locals 1

    .prologue
    .line 1546
    iget-boolean v0, p0, Lcom/android/calendar/b/a;->b:Z

    invoke-static {p0, v0}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;Z)V

    .line 1547
    return-void
.end method

.method static a(Lcom/android/calendar/b/a;Z)V
    .locals 12

    .prologue
    const/16 v10, 0xc

    const/4 v9, 0x1

    .line 1455
    iget-boolean v4, p0, Lcom/android/calendar/b/a;->a:Z

    .line 1456
    iget v1, p0, Lcom/android/calendar/b/a;->second:I

    .line 1457
    iget v5, p0, Lcom/android/calendar/b/a;->minute:I

    .line 1458
    iget v6, p0, Lcom/android/calendar/b/a;->hour:I

    .line 1459
    iget v7, p0, Lcom/android/calendar/b/a;->monthDay:I

    .line 1460
    iget v2, p0, Lcom/android/calendar/b/a;->month:I

    .line 1461
    iget v3, p0, Lcom/android/calendar/b/a;->year:I

    .line 1463
    if-gez v1, :cond_0

    add-int/lit8 v0, v1, -0x3b

    :goto_0
    div-int/lit8 v0, v0, 0x3c

    .line 1464
    mul-int/lit8 v8, v0, 0x3c

    sub-int v8, v1, v8

    .line 1465
    add-int v1, v5, v0

    .line 1466
    if-gez v1, :cond_1

    add-int/lit8 v0, v1, -0x3b

    :goto_1
    div-int/lit8 v0, v0, 0x3c

    .line 1467
    mul-int/lit8 v5, v0, 0x3c

    sub-int v5, v1, v5

    .line 1468
    add-int v1, v6, v0

    .line 1469
    if-gez v1, :cond_2

    add-int/lit8 v0, v1, -0x17

    :goto_2
    div-int/lit8 v0, v0, 0x18

    .line 1470
    mul-int/lit8 v6, v0, 0x18

    sub-int v6, v1, v6

    .line 1471
    add-int/2addr v0, v7

    move v1, v3

    move v3, v0

    .line 1476
    :goto_3
    if-gtz v3, :cond_6

    .line 1485
    if-ne v4, v9, :cond_4

    .line 1487
    if-le v2, v9, :cond_3

    invoke-static {v1}, Lcom/android/calendar/b/b;->c(I)I

    move-result v0

    .line 1491
    :goto_4
    add-int/2addr v3, v0

    .line 1492
    add-int/lit8 v1, v1, -0x1

    .line 1493
    goto :goto_3

    :cond_0
    move v0, v1

    .line 1463
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1466
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1469
    goto :goto_2

    .line 1487
    :cond_3
    add-int/lit8 v0, v1, -0x1

    invoke-static {v0}, Lcom/android/calendar/b/b;->c(I)I

    move-result v0

    goto :goto_4

    .line 1489
    :cond_4
    if-le v2, v9, :cond_5

    invoke-static {v1}, Lcom/android/calendar/b/b;->b(I)I

    move-result v0

    goto :goto_4

    :cond_5
    add-int/lit8 v0, v1, -0x1

    invoke-static {v0}, Lcom/android/calendar/b/b;->b(I)I

    move-result v0

    goto :goto_4

    .line 1495
    :cond_6
    if-gez v2, :cond_8

    .line 1496
    add-int/lit8 v0, v2, 0x1

    div-int/lit8 v0, v0, 0xc

    add-int/lit8 v7, v0, -0x1

    .line 1497
    add-int v0, v1, v7

    .line 1498
    mul-int/lit8 v1, v7, 0xc

    sub-int v1, v2, v1

    move v11, v1

    move v1, v3

    move v3, v11

    .line 1508
    :goto_5
    if-nez v3, :cond_c

    .line 1510
    if-eqz v4, :cond_9

    invoke-static {v0}, Lcom/android/calendar/b/b;->c(I)I

    move-result v2

    .line 1511
    :goto_6
    if-le v1, v2, :cond_c

    .line 1513
    add-int/lit8 v0, v0, 0x1

    .line 1514
    sub-int/2addr v1, v2

    move v2, v1

    .line 1518
    :goto_7
    if-eqz v4, :cond_a

    invoke-static {v0, v3, p1}, Lcom/android/calendar/b/b;->a(IIZ)I

    move-result v1

    .line 1520
    :goto_8
    if-le v2, v1, :cond_b

    .line 1522
    sub-int/2addr v2, v1

    .line 1523
    add-int/lit8 v1, v3, 0x1

    .line 1524
    if-lt v1, v10, :cond_7

    .line 1526
    add-int/lit8 v1, v1, -0xc

    .line 1527
    add-int/lit8 v0, v0, 0x1

    :cond_7
    move v3, v1

    move v1, v2

    .line 1530
    goto :goto_5

    .line 1499
    :cond_8
    if-lt v2, v10, :cond_d

    .line 1500
    div-int/lit8 v7, v2, 0xc

    .line 1501
    add-int v0, v1, v7

    .line 1502
    mul-int/lit8 v1, v7, 0xc

    sub-int v1, v2, v1

    move v11, v1

    move v1, v3

    move v3, v11

    goto :goto_5

    .line 1510
    :cond_9
    invoke-static {v0}, Lcom/android/calendar/b/b;->b(I)I

    move-result v2

    goto :goto_6

    .line 1518
    :cond_a
    invoke-static {v0, v3}, Lcom/android/calendar/b/b;->a(II)I

    move-result v1

    goto :goto_8

    .line 1534
    :cond_b
    iput v8, p0, Lcom/android/calendar/b/a;->second:I

    .line 1535
    iput v5, p0, Lcom/android/calendar/b/a;->minute:I

    .line 1536
    iput v6, p0, Lcom/android/calendar/b/a;->hour:I

    .line 1537
    iput v2, p0, Lcom/android/calendar/b/a;->monthDay:I

    .line 1538
    iput v3, p0, Lcom/android/calendar/b/a;->month:I

    .line 1539
    iput v0, p0, Lcom/android/calendar/b/a;->year:I

    .line 1540
    invoke-static {v0, v3, v2}, Lcom/android/calendar/b/b;->a(III)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/b/a;->weekDay:I

    .line 1541
    invoke-static {v0, v3, v2}, Lcom/android/calendar/b/b;->b(III)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/b/a;->yearDay:I

    .line 1542
    return-void

    :cond_c
    move v2, v1

    goto :goto_7

    :cond_d
    move v0, v1

    move v1, v3

    move v3, v2

    goto :goto_5
.end method

.method static a(I)Z
    .locals 1

    .prologue
    .line 1564
    rem-int/lit8 v0, p0, 0x4

    if-nez v0, :cond_1

    rem-int/lit8 v0, p0, 0x64

    if-nez v0, :cond_0

    rem-int/lit16 v0, p0, 0x190

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IIIZ)Z
    .locals 9

    .prologue
    const/16 v2, 0xd

    const/16 v1, 0xc

    .line 805
    .line 806
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v0, p0, -0x759

    sget-object v3, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v5, v0, 0xe

    .line 807
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    sget-object v3, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v3, v5, 0xd

    invoke-virtual {v0, v3}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v4

    .line 808
    if-ge p1, v4, :cond_0

    move v3, p1

    .line 809
    :goto_0
    if-le v4, v1, :cond_1

    move v0, v1

    :goto_1
    move v7, v0

    move v6, p0

    move v0, v4

    move v4, v5

    move v5, p2

    .line 811
    :goto_2
    if-lez v5, :cond_6

    .line 812
    sub-int/2addr v7, v3

    add-int/lit8 v7, v7, 0x1

    .line 813
    if-le v5, v7, :cond_6

    .line 814
    sub-int p2, v5, v7

    .line 815
    add-int/lit8 v6, v6, 0x1

    .line 817
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v5, v4, 0xe

    .line 818
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    sget-object v4, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v4, v5, 0xd

    invoke-virtual {v0, v4}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v4

    .line 819
    if-le v4, v1, :cond_2

    move v0, v1

    :goto_3
    move v7, v0

    move v0, v4

    move v4, v5

    move v5, p2

    .line 823
    goto :goto_2

    .line 808
    :cond_0
    add-int/lit8 v3, p1, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 809
    goto :goto_1

    :cond_2
    move v0, v2

    .line 819
    goto :goto_3

    .line 825
    :goto_4
    if-gez v5, :cond_4

    .line 827
    neg-int v7, v5

    if-le v7, v0, :cond_4

    .line 828
    add-int/2addr v5, v0

    .line 829
    add-int/lit8 v6, v6, -0x1

    .line 831
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v4, v4, -0xe

    .line 832
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    sget-object v3, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v3, v4, 0xd

    invoke-virtual {v0, v3}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v3

    .line 833
    if-le v3, v1, :cond_3

    move v0, v1

    goto :goto_4

    :cond_3
    move v0, v2

    goto :goto_4

    .line 839
    :cond_4
    add-int/2addr v0, v5

    if-ne v3, v0, :cond_5

    const/4 v0, 0x1

    .line 840
    :goto_5
    return v0

    .line 839
    :cond_5
    const/4 v0, 0x0

    goto :goto_5

    :cond_6
    move v8, v3

    move v3, v0

    move v0, v8

    goto :goto_4
.end method

.method private static a([III)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 153
    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_0

    .line 154
    aget v2, p0, v1

    if-ne v2, p2, :cond_1

    .line 155
    const/4 v0, 0x1

    .line 158
    :cond_0
    return v0

    .line 153
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static a([IIII)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    move v3, v2

    move v0, p3

    :goto_0
    if-ge v3, p1, :cond_2

    .line 172
    aget v4, p0, v3

    .line 173
    if-lez v4, :cond_0

    .line 174
    if-ne v4, p2, :cond_1

    move v0, v1

    .line 184
    :goto_1
    return v0

    .line 178
    :cond_0
    add-int/2addr v0, v4

    .line 179
    if-ne v0, p2, :cond_1

    move v0, v1

    .line 180
    goto :goto_1

    .line 171
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 184
    goto :goto_1
.end method

.method static b(I)I
    .locals 1

    .prologue
    .line 1574
    invoke-static {p0}, Lcom/android/calendar/b/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16e

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x16d

    goto :goto_0
.end method

.method static b(III)I
    .locals 2

    .prologue
    .line 1654
    sget-object v0, Lcom/android/calendar/b/b;->q:[I

    aget v0, v0, p1

    add-int/2addr v0, p2

    add-int/lit8 v0, v0, -0x1

    .line 1655
    const/4 v1, 0x2

    if-lt p1, v1, :cond_0

    invoke-static {p0}, Lcom/android/calendar/b/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1656
    add-int/lit8 v0, v0, 0x1

    .line 1658
    :cond_0
    return v0
.end method

.method private static final b(Landroid/text/format/Time;)J
    .locals 4

    .prologue
    .line 1676
    iget v0, p0, Landroid/text/format/Time;->year:I

    int-to-long v0, v0

    const/16 v2, 0x1a

    shl-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->month:I

    shl-int/lit8 v2, v2, 0x16

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    shl-int/lit8 v2, v2, 0x11

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->hour:I

    shl-int/lit8 v2, v2, 0xc

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->minute:I

    shl-int/lit8 v2, v2, 0x6

    int-to-long v2, v2

    add-long/2addr v0, v2

    iget v2, p0, Landroid/text/format/Time;->second:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method static c(I)I
    .locals 3

    .prologue
    .line 1583
    sget-object v0, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit16 v0, p0, -0x759

    .line 1584
    sget-object v1, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lcom/android/calendar/d/a/b;->b(I)I

    move-result v1

    sget-object v2, Lcom/android/calendar/b/b;->o:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2, v0}, Lcom/android/calendar/d/a/b;->b(I)I

    move-result v0

    sub-int v0, v1, v0

    return v0
.end method

.method private static c(III)Z
    .locals 1

    .prologue
    .line 311
    if-le p0, p1, :cond_0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V
    .locals 45

    .prologue
    .line 872
    const/4 v4, 0x0

    .line 873
    const/4 v3, 0x0

    .line 876
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/android/calendar/b/a;

    if-eqz v2, :cond_4

    move-object/from16 v2, p1

    .line 877
    check-cast v2, Lcom/android/calendar/b/a;

    .line 878
    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->b:I

    const/4 v6, 0x6

    if-eq v5, v6, :cond_0

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->b:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_3

    .line 880
    :cond_0
    iget-boolean v3, v2, Lcom/android/calendar/b/a;->a:Z

    .line 882
    if-eqz v3, :cond_1

    .line 884
    sget-object v4, Lcom/android/calendar/b/b;->n:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v4

    .line 885
    move-object/from16 v0, p1

    iget v5, v0, Landroid/text/format/Time;->year:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/text/format/Time;->month:I

    move-object/from16 v0, p1

    iget v7, v0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v4, v5, v6, v7}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 887
    invoke-virtual {v4}, Lcom/android/calendar/d/a/a;->a()I

    move-result v5

    iput v5, v2, Lcom/android/calendar/b/a;->year:I

    .line 888
    invoke-virtual {v4}, Lcom/android/calendar/d/a/a;->b()I

    move-result v5

    iput v5, v2, Lcom/android/calendar/b/a;->month:I

    .line 889
    invoke-virtual {v4}, Lcom/android/calendar/d/a/a;->c()I

    move-result v5

    iput v5, v2, Lcom/android/calendar/b/a;->monthDay:I

    .line 890
    invoke-virtual {v4}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v4

    iput-boolean v4, v2, Lcom/android/calendar/b/a;->b:Z

    .line 892
    :cond_1
    iget-boolean v2, v2, Lcom/android/calendar/b/a;->b:Z

    move v9, v3

    move v3, v2

    .line 900
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;)V

    .line 902
    if-eqz v9, :cond_5

    .line 904
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;Z)Landroid/text/format/Time;

    move-result-object v2

    .line 905
    invoke-static {v2}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v4

    move-wide v10, v4

    .line 910
    :goto_1
    const/4 v4, 0x0

    .line 922
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->b:I

    const/4 v5, 0x6

    if-ne v2, v5, :cond_b

    .line 923
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_6

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_6

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_6

    .line 927
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->n:[I

    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/a/c;->x:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v2, v5

    .line 977
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    move-object/from16 v32, v0

    .line 978
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/b/b;->b:Landroid/text/format/Time;

    .line 979
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/b/b;->c:Ljava/lang/StringBuilder;

    .line 980
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->d:Lcom/android/calendar/b/a;

    .line 981
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/b/b;->e:Lcom/android/calendar/b/c;

    move-object/from16 v33, v0

    .line 985
    :try_start_0
    move-object/from16 v0, v33

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->a(Lcom/android/a/c;)V

    .line 986
    const-wide v12, 0x7fffffffffffffffL

    cmp-long v5, p5, v12

    if-nez v5, :cond_11

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-nez v5, :cond_11

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->d:I

    if-nez v5, :cond_11

    .line 987
    new-instance v2, Lcom/android/a/a;

    const-string v3, "No range end provided for a recurrence that has no UNTIL or COUNT."

    invoke-direct {v2, v3}, Lcom/android/a/a;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lcom/android/a/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1410
    :catch_0
    move-exception v2

    .line 1411
    const-string v3, "RecurrenceProcessorWithLunar"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DateException with r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeStart="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeEnd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1413
    throw v2

    .line 895
    :cond_3
    const/4 v5, 0x0

    iput-boolean v5, v2, Lcom/android/calendar/b/a;->a:Z

    :cond_4
    move v9, v3

    move v3, v4

    goto/16 :goto_0

    .line 907
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v4

    move-wide v10, v4

    goto/16 :goto_1

    .line 928
    :cond_6
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_9

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_9

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x7

    if-ne v2, v5, :cond_9

    .line 930
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x1

    if-eq v2, v5, :cond_7

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x2

    if-eq v2, v5, :cond_7

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x3

    if-eq v2, v5, :cond_7

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x4

    if-ne v2, v5, :cond_8

    .line 933
    :cond_7
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 934
    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/a/c;->x:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v2, v5

    .line 935
    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/a/c;->p:[I

    .line 936
    const/4 v2, 0x1

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/a/c;->q:I

    goto/16 :goto_2

    .line 937
    :cond_8
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, -0x1

    if-ne v2, v5, :cond_2

    .line 939
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->f:Z

    goto/16 :goto_2

    .line 941
    :cond_9
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_a

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_a

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x2

    if-ne v2, v5, :cond_a

    .line 944
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->g:Z

    goto/16 :goto_2

    .line 946
    :cond_a
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x5

    if-ne v2, v5, :cond_2

    .line 948
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->h:Z

    goto/16 :goto_2

    .line 950
    :cond_b
    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->b:I

    const/4 v5, 0x7

    if-ne v2, v5, :cond_2

    .line 951
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_c

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_c

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_c

    .line 953
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->n:[I

    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/a/c;->x:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v2, v5

    .line 954
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->i:Z

    goto/16 :goto_2

    .line 955
    :cond_c
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_d

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_d

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x5

    if-ne v2, v5, :cond_d

    .line 957
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->j:Z

    goto/16 :goto_2

    .line 958
    :cond_d
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_e

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_e

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x2

    if-ne v2, v5, :cond_e

    .line 960
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->k:Z

    goto/16 :goto_2

    .line 961
    :cond_e
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->y:I

    const/4 v5, 0x1

    if-ne v2, v5, :cond_2

    move-object/from16 v0, p2

    iget v2, v0, Lcom/android/a/c;->o:I

    const/4 v5, 0x7

    if-ne v2, v5, :cond_2

    .line 962
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x1

    if-eq v2, v5, :cond_f

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x2

    if-eq v2, v5, :cond_f

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x3

    if-eq v2, v5, :cond_f

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, 0x4

    if-ne v2, v5, :cond_10

    .line 965
    :cond_f
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 966
    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/a/c;->x:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v2, v5

    .line 967
    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/a/c;->p:[I

    .line 968
    const/4 v2, 0x1

    move-object/from16 v0, p2

    iput v2, v0, Lcom/android/a/c;->q:I

    .line 969
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->l:Z

    goto/16 :goto_2

    .line 970
    :cond_10
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/c;->x:[I

    const/4 v5, 0x0

    aget v2, v2, v5

    const/4 v5, -0x1

    if-ne v2, v5, :cond_2

    .line 972
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->m:Z

    goto/16 :goto_2

    .line 993
    :cond_11
    :try_start_1
    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->e:I

    .line 994
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->b:I

    move/from16 v34, v0

    .line 995
    packed-switch v34, :pswitch_data_0

    .line 1023
    new-instance v2, Lcom/android/a/a;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bad freq="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v34

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/a/a;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Lcom/android/a/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1415
    :catch_1
    move-exception v2

    .line 1416
    const-string v3, "RecurrenceProcessorWithLunar"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RuntimeException with r="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeStart="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rangeEnd="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1418
    throw v2

    .line 998
    :pswitch_0
    const/4 v6, 0x1

    move/from16 v31, v6

    .line 1025
    :goto_3
    if-gtz v5, :cond_3f

    .line 1026
    const/4 v5, 0x1

    move/from16 v30, v5

    .line 1029
    :goto_4
    :try_start_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->w:I

    move/from16 v35, v0

    .line 1030
    const/4 v5, 0x6

    move/from16 v0, v34

    move/from16 v1, v35

    invoke-static {v0, v5, v1}, Lcom/android/calendar/b/b;->c(III)Z

    move-result v36

    .line 1031
    const/4 v5, 0x5

    move/from16 v0, v34

    if-lt v0, v5, :cond_17

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->o:I

    if-gtz v5, :cond_12

    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->q:I

    if-lez v5, :cond_17

    :cond_12
    const/4 v5, 0x1

    move/from16 v27, v5

    .line 1033
    :goto_5
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->l:I

    move/from16 v37, v0

    .line 1034
    const/4 v5, 0x3

    move/from16 v0, v34

    move/from16 v1, v37

    invoke-static {v0, v5, v1}, Lcom/android/calendar/b/b;->c(III)Z

    move-result v38

    .line 1035
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->j:I

    move/from16 v39, v0

    .line 1036
    const/4 v5, 0x2

    move/from16 v0, v34

    move/from16 v1, v39

    invoke-static {v0, v5, v1}, Lcom/android/calendar/b/b;->c(III)Z

    move-result v40

    .line 1037
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/a/c;->h:I

    move/from16 v41, v0

    .line 1038
    const/4 v5, 0x1

    move/from16 v0, v34

    move/from16 v1, v41

    invoke-static {v0, v5, v1}, Lcom/android/calendar/b/b;->c(III)Z

    move-result v42

    .line 1041
    move-object/from16 v0, v32

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/a;->set(Landroid/text/format/Time;)V

    .line 1042
    const/4 v5, 0x5

    move/from16 v0, v31

    if-ne v0, v5, :cond_13

    .line 1043
    if-eqz v27, :cond_13

    .line 1049
    const/4 v5, 0x1

    move-object/from16 v0, v32

    iput v5, v0, Lcom/android/calendar/b/a;->monthDay:I

    .line 1054
    :cond_13
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v5, :cond_18

    .line 1056
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 1060
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v12, 0xf

    if-ne v6, v12, :cond_14

    .line 1061
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x5a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1064
    :cond_14
    invoke-virtual {v7, v5}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 1066
    iget v5, v7, Landroid/text/format/Time;->year:I

    const/16 v6, 0x7f4

    if-le v5, v6, :cond_15

    .line 1067
    const/16 v5, 0x7f4

    iput v5, v7, Landroid/text/format/Time;->year:I

    .line 1068
    const/16 v5, 0xb

    iput v5, v7, Landroid/text/format/Time;->month:I

    .line 1069
    const/16 v5, 0x1f

    iput v5, v7, Landroid/text/format/Time;->monthDay:I

    .line 1075
    :cond_15
    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v7, v5}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 1076
    invoke-static {v7}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    move-wide/from16 v28, v6

    .line 1088
    :goto_6
    const/16 v5, 0xf

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->ensureCapacity(I)V

    .line 1089
    const/16 v5, 0xf

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    move/from16 v17, v3

    move v3, v4

    .line 1100
    :goto_7
    const/4 v4, 0x0

    .line 1102
    move-object/from16 v0, v32

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;Z)V

    .line 1104
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->year:I

    .line 1105
    move-object/from16 v0, v32

    iget v5, v0, Lcom/android/calendar/b/a;->month:I

    add-int/lit8 v25, v5, 0x1

    .line 1106
    move-object/from16 v0, v32

    iget v12, v0, Lcom/android/calendar/b/a;->monthDay:I

    .line 1107
    move-object/from16 v0, v32

    iget v15, v0, Lcom/android/calendar/b/a;->hour:I

    .line 1108
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/calendar/b/a;->minute:I

    move/from16 v16, v0

    .line 1109
    move-object/from16 v0, v32

    iget v0, v0, Lcom/android/calendar/b/a;->second:I

    move/from16 v19, v0

    .line 1112
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Lcom/android/calendar/b/a;->set(Landroid/text/format/Time;)V

    .line 1115
    iget v5, v2, Lcom/android/calendar/b/a;->year:I

    const/16 v6, 0x7f4

    if-le v5, v6, :cond_3e

    .line 1401
    :cond_16
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->f:Z

    .line 1402
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->g:Z

    .line 1403
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->h:Z

    .line 1404
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->i:Z

    .line 1405
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->j:Z

    .line 1406
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->k:Z

    .line 1407
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->m:Z

    .line 1408
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/b/b;->l:Z

    .line 1420
    return-void

    .line 1001
    :pswitch_1
    const/4 v6, 0x2

    move/from16 v31, v6

    .line 1002
    goto/16 :goto_3

    .line 1004
    :pswitch_2
    const/4 v6, 0x3

    move/from16 v31, v6

    .line 1005
    goto/16 :goto_3

    .line 1007
    :pswitch_3
    const/4 v6, 0x4

    move/from16 v31, v6

    .line 1008
    goto/16 :goto_3

    .line 1010
    :pswitch_4
    const/4 v6, 0x4

    .line 1011
    move-object/from16 v0, p2

    iget v5, v0, Lcom/android/a/c;->e:I

    mul-int/lit8 v5, v5, 0x7

    .line 1012
    if-gtz v5, :cond_40

    .line 1013
    const/4 v5, 0x7

    move/from16 v31, v6

    goto/16 :goto_3

    .line 1017
    :pswitch_5
    const/4 v6, 0x5

    move/from16 v31, v6

    .line 1018
    goto/16 :goto_3

    .line 1020
    :pswitch_6
    const/4 v6, 0x6

    move/from16 v31, v6

    .line 1021
    goto/16 :goto_3

    .line 1031
    :cond_17
    const/4 v5, 0x0

    move/from16 v27, v5

    goto/16 :goto_5

    .line 1078
    :cond_18
    new-instance v5, Landroid/text/format/Time;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 1080
    const/16 v6, 0x7f4

    iput v6, v5, Landroid/text/format/Time;->year:I

    .line 1081
    const/16 v6, 0xb

    iput v6, v5, Landroid/text/format/Time;->month:I

    .line 1082
    const/16 v6, 0x1f

    iput v6, v5, Landroid/text/format/Time;->monthDay:I

    .line 1083
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1085
    invoke-static {v5}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    move-wide/from16 v28, v6

    goto/16 :goto_6

    :cond_19
    move/from16 v26, v3

    move v5, v4

    .line 1120
    :goto_8
    if-eqz v36, :cond_24

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->v:[I

    aget v3, v3, v26

    .line 1123
    :goto_9
    add-int/lit8 v7, v3, -0x1

    .line 1126
    const/4 v6, 0x1

    .line 1127
    const/4 v3, 0x0

    .line 1132
    if-eqz v27, :cond_1a

    .line 1136
    const/4 v3, 0x5

    move/from16 v0, v34

    if-ne v0, v3, :cond_25

    .line 1137
    move-object/from16 v0, v32

    iget v3, v0, Lcom/android/calendar/b/a;->weekDay:I

    .line 1138
    move-object/from16 v0, v32

    iget v4, v0, Lcom/android/calendar/b/a;->monthDay:I

    sub-int v6, v4, v3

    .line 1139
    add-int/lit8 v3, v6, 0x6

    .line 1151
    :cond_1a
    :goto_a
    if-eqz v27, :cond_30

    .line 1152
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->f:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_26

    .line 1154
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->e(Lcom/android/calendar/b/a;)I

    move-result v6

    move v13, v3

    move v14, v6

    .line 1203
    :goto_b
    const/4 v3, 0x0

    move/from16 v24, v3

    move/from16 v18, v5

    .line 1205
    :goto_c
    if-eqz v38, :cond_31

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->k:[I

    aget v5, v3, v24

    .line 1211
    :goto_d
    const/4 v3, 0x0

    move/from16 v23, v3

    .line 1213
    :goto_e
    if-eqz v40, :cond_32

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->i:[I

    aget v4, v3, v23

    .line 1219
    :goto_f
    const/4 v3, 0x0

    move/from16 v22, v3

    .line 1221
    :goto_10
    if-eqz v42, :cond_33

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/c;->g:[I

    aget v3, v3, v22

    .line 1229
    :goto_11
    invoke-virtual/range {v2 .. v8}, Lcom/android/calendar/b/a;->set(IIIIII)V

    .line 1230
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;Z)V

    .line 1234
    if-eqz v9, :cond_34

    .line 1236
    move/from16 v0, v17

    invoke-static {v2, v0}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;Z)Landroid/text/format/Time;

    move-result-object v3

    .line 1237
    invoke-static {v3}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v20

    .line 1246
    :goto_12
    cmp-long v3, v20, v10

    if-ltz v3, :cond_1d

    .line 1248
    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/calendar/b/b;->a(Lcom/android/a/c;Landroid/text/format/Time;)I

    move-result v3

    .line 1249
    if-nez v3, :cond_1d

    .line 1268
    if-eqz p7, :cond_35

    cmp-long v3, v10, p3

    if-ltz v3, :cond_35

    cmp-long v3, v10, p5

    if-gez v3, :cond_35

    .line 1272
    add-int/lit8 v18, v18, 0x1

    .line 1284
    :cond_1b
    :goto_13
    cmp-long v3, v20, v28

    if-gtz v3, :cond_16

    .line 1294
    cmp-long v3, v20, p5

    if-gez v3, :cond_16

    .line 1303
    cmp-long v3, v20, p3

    if-ltz v3, :cond_1c

    .line 1307
    if-eqz p7, :cond_36

    .line 1308
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1314
    :cond_1c
    :goto_14
    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/a/c;->d:I

    if-lez v3, :cond_1d

    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/a/c;->d:I

    move/from16 v0, v18

    if-eq v3, v0, :cond_16

    .line 1320
    :cond_1d
    add-int/lit8 v3, v22, 0x1

    .line 1321
    if-eqz v42, :cond_1e

    move/from16 v0, v41

    if-lt v3, v0, :cond_3d

    .line 1322
    :cond_1e
    add-int/lit8 v3, v23, 0x1

    .line 1323
    if-eqz v40, :cond_1f

    move/from16 v0, v39

    if-lt v3, v0, :cond_3c

    .line 1324
    :cond_1f
    add-int/lit8 v3, v24, 0x1

    .line 1325
    if-eqz v38, :cond_20

    move/from16 v0, v37

    if-lt v3, v0, :cond_3b

    .line 1326
    :cond_20
    add-int/lit8 v3, v14, 0x1

    move v5, v3

    move/from16 v4, v18

    move v3, v13

    .line 1327
    :goto_15
    if-eqz v27, :cond_21

    if-le v5, v3, :cond_3a

    .line 1328
    :cond_21
    add-int/lit8 v3, v26, 0x1

    .line 1329
    if-eqz v36, :cond_22

    move/from16 v0, v35

    if-lt v3, v0, :cond_19

    .line 1335
    :cond_22
    move-object/from16 v0, v32

    iget v6, v0, Lcom/android/calendar/b/a;->monthDay:I

    .line 1336
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Lcom/android/calendar/b/a;->set(Landroid/text/format/Time;)V

    .line 1337
    const/4 v3, 0x1

    move v5, v3

    move/from16 v3, v17

    .line 1339
    :goto_16
    mul-int v7, v30, v5

    .line 1342
    if-eqz v9, :cond_38

    const/4 v8, 0x5

    move/from16 v0, v31

    if-ne v0, v8, :cond_38

    .line 1343
    if-nez v3, :cond_37

    .line 1344
    move-object/from16 v0, v32

    iget v3, v0, Lcom/android/calendar/b/a;->year:I

    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->month:I

    const/4 v12, 0x0

    invoke-static {v3, v8, v7, v12}, Lcom/android/calendar/b/b;->a(IIIZ)Z

    move-result v3

    .line 1349
    if-eqz v3, :cond_38

    .line 1350
    const/4 v8, 0x1

    move-object/from16 v0, v32

    invoke-static {v0, v8}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;Z)V

    .line 1351
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->monthDay:I

    if-ne v8, v6, :cond_38

    :cond_23
    move/from16 v17, v3

    move v3, v4

    .line 1399
    goto/16 :goto_7

    :cond_24
    move/from16 v3, v25

    .line 1120
    goto/16 :goto_9

    .line 1141
    :cond_25
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/android/calendar/b/a;->getActualMaximum(I)I

    move-result v3

    goto/16 :goto_a

    .line 1156
    :cond_26
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->g:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_27

    .line 1158
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->b(Landroid/text/format/Time;)I

    move-result v6

    .line 1159
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->e(Lcom/android/calendar/b/a;)I

    move-result v4

    move v13, v3

    move v14, v4

    goto/16 :goto_b

    .line 1160
    :cond_27
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->h:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_28

    .line 1162
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->a(Landroid/text/format/Time;)I

    move-result v6

    .line 1163
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->e(Lcom/android/calendar/b/a;)I

    move-result v4

    move v13, v3

    move v14, v4

    goto/16 :goto_b

    .line 1166
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->i:Z

    const/4 v13, 0x1

    if-eq v4, v13, :cond_29

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->j:Z

    const/4 v13, 0x1

    if-eq v4, v13, :cond_29

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->k:Z

    const/4 v13, 0x1

    if-eq v4, v13, :cond_29

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->l:Z

    const/4 v13, 0x1

    if-eq v4, v13, :cond_29

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->m:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_2a

    .line 1170
    :cond_29
    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->month:I

    .line 1172
    :cond_2a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->j:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_2b

    .line 1173
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->a(Landroid/text/format/Time;)I

    move-result v6

    .line 1174
    const/16 v4, 0x1f

    move v13, v3

    move v14, v4

    goto/16 :goto_b

    .line 1175
    :cond_2b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->k:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_2c

    .line 1176
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->b(Landroid/text/format/Time;)I

    move-result v6

    .line 1177
    const/16 v4, 0x1f

    move v13, v3

    move v14, v4

    goto/16 :goto_b

    .line 1178
    :cond_2c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/b/b;->m:Z

    const/4 v13, 0x1

    if-ne v4, v13, :cond_2d

    .line 1179
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/calendar/b/c;->e(Lcom/android/calendar/b/a;)I

    move-result v6

    .line 1180
    const/16 v4, 0x1f

    move v13, v3

    move v14, v4

    goto/16 :goto_b

    .line 1183
    :cond_2d
    move-object/from16 v0, p2

    iget v4, v0, Lcom/android/a/c;->b:I

    const/4 v13, 0x7

    if-ne v4, v13, :cond_2e

    .line 1184
    const/4 v3, 0x4

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, Lcom/android/calendar/b/a;->getActualMaximum(I)I

    move-result v3

    .line 1186
    :cond_2e
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v6}, Lcom/android/calendar/b/c;->a(Lcom/android/calendar/b/a;I)Z

    move-result v4

    if-nez v4, :cond_2f

    .line 1187
    add-int/lit8 v4, v6, 0x1

    move/from16 v43, v4

    move v4, v5

    move/from16 v5, v43

    .line 1188
    goto/16 :goto_15

    :cond_2f
    move v13, v3

    move v14, v6

    .line 1190
    goto/16 :goto_b

    :cond_30
    move v13, v3

    move v14, v6

    move v6, v12

    .line 1196
    goto/16 :goto_b

    :cond_31
    move v5, v15

    .line 1205
    goto/16 :goto_d

    :cond_32
    move/from16 v4, v16

    .line 1213
    goto/16 :goto_f

    :cond_33
    move/from16 v3, v19

    .line 1221
    goto/16 :goto_11

    .line 1239
    :cond_34
    invoke-static {v2}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v20

    goto/16 :goto_12

    .line 1279
    :cond_35
    move-object/from16 v0, p2

    iget v3, v0, Lcom/android/a/c;->d:I

    if-lez v3, :cond_1b

    .line 1280
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_13

    .line 1310
    :cond_36
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_14

    .line 1356
    :cond_37
    const/4 v3, 0x0

    .line 1360
    :cond_38
    packed-switch v31, :pswitch_data_1

    .line 1386
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bad field="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v31

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1362
    :pswitch_7
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->second:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->second:I

    .line 1389
    :goto_17
    const/4 v7, 0x0

    move-object/from16 v0, v32

    invoke-static {v0, v7}, Lcom/android/calendar/b/b;->a(Lcom/android/calendar/b/a;Z)V

    .line 1390
    const/4 v7, 0x6

    move/from16 v0, v31

    if-eq v0, v7, :cond_39

    const/4 v7, 0x5

    move/from16 v0, v31

    if-ne v0, v7, :cond_23

    .line 1393
    :cond_39
    move-object/from16 v0, v32

    iget v7, v0, Lcom/android/calendar/b/a;->monthDay:I

    if-eq v7, v6, :cond_23

    .line 1396
    add-int/lit8 v5, v5, 0x1

    .line 1397
    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Lcom/android/calendar/b/a;->set(Landroid/text/format/Time;)V

    goto/16 :goto_16

    .line 1365
    :pswitch_8
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->minute:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->minute:I

    goto :goto_17

    .line 1368
    :pswitch_9
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->hour:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->hour:I

    goto :goto_17

    .line 1371
    :pswitch_a
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->monthDay:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->monthDay:I

    goto :goto_17

    .line 1374
    :pswitch_b
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->month:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->month:I

    goto :goto_17

    .line 1377
    :pswitch_c
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->year:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->year:I

    goto :goto_17

    .line 1380
    :pswitch_d
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->monthDay:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->monthDay:I

    goto :goto_17

    .line 1383
    :pswitch_e
    move-object/from16 v0, v32

    iget v8, v0, Lcom/android/calendar/b/a;->monthDay:I

    add-int/2addr v7, v8

    move-object/from16 v0, v32

    iput v7, v0, Lcom/android/calendar/b/a;->monthDay:I
    :try_end_2
    .catch Lcom/android/a/a; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_17

    :cond_3a
    move v6, v5

    move v5, v4

    goto/16 :goto_a

    :cond_3b
    move/from16 v24, v3

    goto/16 :goto_c

    :cond_3c
    move/from16 v23, v3

    goto/16 :goto_e

    :cond_3d
    move/from16 v22, v3

    goto/16 :goto_10

    :cond_3e
    move/from16 v26, v4

    move v5, v3

    goto/16 :goto_8

    :cond_3f
    move/from16 v30, v5

    goto/16 :goto_4

    :cond_40
    move/from16 v31, v6

    goto/16 :goto_3

    .line 995
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 1360
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J
    .locals 17

    .prologue
    .line 721
    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 722
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-virtual {v2, v12}, Lcom/android/calendar/b/a;->clear(Ljava/lang/String;)V

    .line 723
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->d:Lcom/android/calendar/b/a;

    invoke-virtual {v2, v12}, Lcom/android/calendar/b/a;->clear(Ljava/lang/String;)V

    .line 729
    const-wide v2, -0x1f3be2e8340L

    cmp-long v2, p3, v2

    if-gez v2, :cond_0

    .line 733
    const-wide p3, -0x1f3be2e8340L

    .line 736
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Lcom/android/calendar/b/a;->set(J)V

    .line 737
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-static {v2}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    .line 740
    const-wide/16 v2, -0x1

    cmp-long v2, p5, v2

    if-eqz v2, :cond_1

    .line 741
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Lcom/android/calendar/b/a;->set(J)V

    .line 742
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-static {v2}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v8

    .line 747
    :goto_0
    new-instance v11, Ljava/util/TreeSet;

    invoke-direct {v11}, Ljava/util/TreeSet;-><init>()V

    .line 749
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    if-eqz v2, :cond_2

    .line 750
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/a/ad;->a:[Lcom/android/a/c;

    array-length v14, v13

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v14, :cond_2

    aget-object v5, v13, v2

    .line 751
    const/4 v10, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V

    .line 750
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 744
    :cond_1
    const-wide v8, 0x7fffffffffffffffL

    goto :goto_0

    .line 755
    :cond_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->b:[J

    if-eqz v2, :cond_3

    .line 756
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/ad;->b:[J

    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_3

    aget-wide v14, v3, v2

    .line 759
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-virtual {v5, v14, v15}, Lcom/android/calendar/b/a;->set(J)V

    .line 760
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-static {v5}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v14

    .line 761
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 756
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 764
    :cond_3
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->c:[Lcom/android/a/c;

    if-eqz v2, :cond_4

    .line 765
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/a/ad;->c:[Lcom/android/a/c;

    array-length v14, v13

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v14, :cond_4

    aget-object v5, v13, v2

    .line 766
    const/4 v10, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;Lcom/android/a/c;JJZLjava/util/TreeSet;)V

    .line 765
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 770
    :cond_4
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/a/ad;->d:[J

    if-eqz v2, :cond_5

    .line 771
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/a/ad;->d:[J

    array-length v4, v3

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v4, :cond_5

    aget-wide v6, v3, v2

    .line 774
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-virtual {v5, v6, v7}, Lcom/android/calendar/b/a;->set(J)V

    .line 775
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/b/b;->a:Lcom/android/calendar/b/a;

    invoke-static {v5}, Lcom/android/calendar/b/b;->b(Landroid/text/format/Time;)J

    move-result-wide v6

    .line 776
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 771
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 779
    :cond_5
    invoke-virtual {v11}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 782
    const/4 v2, 0x0

    new-array v2, v2, [J

    .line 799
    :goto_5
    return-object v2

    .line 789
    :cond_6
    invoke-virtual {v11}, Ljava/util/TreeSet;->size()I

    move-result v2

    .line 790
    new-array v4, v2, [J

    .line 791
    const/4 v2, 0x0

    .line 793
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 794
    invoke-virtual {v5, v12}, Landroid/text/format/Time;->clear(Ljava/lang/String;)V

    .line 795
    invoke-virtual {v11}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 796
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v5, v8, v9}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;J)V

    .line 797
    add-int/lit8 v2, v3, 0x1

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    aput-wide v8, v4, v3

    move v3, v2

    .line 798
    goto :goto_6

    :cond_7
    move-object v2, v4

    .line 799
    goto :goto_5
.end method
