.class Lcom/android/calendar/gn;
.super Ljava/lang/Object;
.source "SelectAccountActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/SelectAccountActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/SelectAccountActivity;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->a(Lcom/android/calendar/SelectAccountActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->b(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 185
    iget-object v1, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->b(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/android/calendar/SelectAccountActivity;->a(Lcom/android/calendar/SelectAccountActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->c(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/SelectAccountActivity;->a(Lcom/android/calendar/SelectAccountActivity;I)I

    .line 193
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->e(Lcom/android/calendar/SelectAccountActivity;)V

    .line 194
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 195
    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-virtual {v0}, Lcom/android/calendar/SelectAccountActivity;->finish()V

    .line 196
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->d(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 190
    iget-object v1, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    iget-object v0, p0, Lcom/android/calendar/gn;->a:Lcom/android/calendar/SelectAccountActivity;

    invoke-static {v0}, Lcom/android/calendar/SelectAccountActivity;->d(Lcom/android/calendar/SelectAccountActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/SelectAccountActivity;->b(Lcom/android/calendar/SelectAccountActivity;I)I

    goto :goto_0
.end method
