.class public Lcom/android/calendar/preference/CalendarSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "CalendarSettingsActivity.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:[Landroid/accounts/Account;

.field private c:Landroid/os/Handler;

.field private d:Z

.field private e:Landroid/app/Activity;

.field private f:Z

.field private g:Lcom/android/calendar/preference/e;

.field private h:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/CalendarSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->c:Landroid/os/Handler;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    .line 186
    new-instance v0, Lcom/android/calendar/preference/d;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/d;-><init>(Lcom/android/calendar/preference/CalendarSettingsActivity;)V

    iput-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->h:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/calendar/hj;->w(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v0}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 119
    const v1, 0x7f0f032f

    invoke-virtual {p0, v1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 120
    const-string v1, "com.android.calendar.preference.SyncPreferences"

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 121
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/CalendarSettingsActivity;)[Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->b:[Landroid/accounts/Account;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/android/calendar/preference/e;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->g:Lcom/android/calendar/preference/e;

    .line 61
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 169
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 170
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 171
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 172
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    :cond_0
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 198
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 200
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    const/16 v0, 0x132

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_0

    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "timezone"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 206
    const-string v0, ""

    .line 208
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 209
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 210
    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 213
    :cond_2
    sget-object v2, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 215
    new-instance v1, Lcom/android/calendar/timezone/v;

    iget-object v2, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->e:Landroid/app/Activity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 216
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->e:Landroid/app/Activity;

    sget-object v2, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Lcom/android/calendar/timezone/v;Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->g:Lcom/android/calendar/preference/e;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->g:Lcom/android/calendar/preference/e;

    invoke-interface {v0}, Lcom/android/calendar/preference/e;->a()V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 111
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    .line 112
    const/high16 v0, 0x7f070000

    invoke-virtual {p0, v0, p1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->a(Ljava/util/List;)V

    .line 114
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 79
    const v1, 0x7f0a000a

    invoke-static {p0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    .line 80
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    if-nez v2, :cond_0

    .line 82
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 83
    const-string v3, "com.sec.android.intent.calendar.setting"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->f:Z

    .line 85
    const-string v2, ":android:show_fragment"

    const-class v3, Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    iput-object p0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->e:Landroid/app/Activity;

    .line 91
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 92
    if-eqz v1, :cond_1

    .line 93
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 94
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 95
    invoke-virtual {v1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_2

    .line 100
    iget-boolean v2, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->f:Z

    if-nez v2, :cond_3

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 102
    :cond_2
    return-void

    .line 100
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onIsMultiPane()Z
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 65
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    .line 66
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->d:Z

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v1, "com.sec.android.intent.calendar.setting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->f:Z

    .line 70
    const-string v0, ":android:show_fragment"

    const-class v1, Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->setIntent(Landroid/content/Intent;)V

    .line 74
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 129
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    const/4 v0, 0x1

    .line 135
    :goto_1
    return v0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    sget-object v0, Lcom/android/calendar/preference/CalendarSettingsActivity;->a:Ljava/lang/String;

    const-string v1, "Unknown IllegalStateException"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 135
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 155
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 140
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 141
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/calendar/preference/CalendarSettingsActivity;->h:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 144
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->finish()V

    .line 147
    :cond_1
    return-void
.end method
