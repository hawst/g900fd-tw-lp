.class final Lcom/android/calendar/preference/ar;
.super Lcom/android/calendar/common/a/a;
.source "QuickResponseSettings.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method public constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    .line 1002
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 1003
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->f(Lcom/android/calendar/preference/QuickResponseSettings;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 1064
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1065
    const v1, 0x7f11000a

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1066
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/preference/ax;->a(Z)V

    .line 1013
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/preference/ax;->a(Z)V

    .line 1018
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    .line 1040
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 1022
    invoke-virtual {p0}, Lcom/android/calendar/preference/ar;->a()I

    move-result v0

    .line 1023
    iget-object v1, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1044
    invoke-virtual {p0}, Lcom/android/calendar/preference/ar;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->c:Landroid/view/Menu;

    const v1, 0x7f120324

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1029
    iget-object v1, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->f(Lcom/android/calendar/preference/QuickResponseSettings;)I

    move-result v1

    .line 1030
    if-lez v1, :cond_0

    .line 1031
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1035
    :goto_0
    return-void

    .line 1033
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1049
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1059
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1051
    :sswitch_0
    iget-object v1, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->m(Lcom/android/calendar/preference/QuickResponseSettings;)V

    goto :goto_0

    .line 1054
    :sswitch_1
    iget-object v1, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->l(Lcom/android/calendar/preference/QuickResponseSettings;)V

    goto :goto_0

    .line 1049
    :sswitch_data_0
    .sparse-switch
        0x7f1200d4 -> :sswitch_1
        0x7f120324 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1070
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 1071
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0, v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;Z)V

    .line 1072
    return v1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1082
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    move v0, v1

    .line 1083
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1084
    iget-object v2, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    aput-boolean v1, v2, v0

    .line 1083
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1086
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1087
    if-eqz v0, :cond_1

    .line 1088
    iget-object v2, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v2

    invoke-static {v0}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v3}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/android/calendar/preference/ax;->a([Ljava/lang/String;[Z)V

    .line 1090
    :cond_1
    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Z)Z

    .line 1091
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/preference/ax;->a([Z)V

    .line 1092
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    iget-object v2, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/preference/PreferenceScreen;)V

    .line 1093
    iget-object v0, p0, Lcom/android/calendar/preference/ar;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0, v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;Z)V

    .line 1094
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1077
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
