.class Lcom/android/calendar/preference/am;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 700
    iput-object p1, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 704
    iget-object v0, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 705
    iget-object v0, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->k(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v2

    .line 706
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 707
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 708
    iget-object v4, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v4}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v4}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v4

    aget-boolean v4, v4, v0

    if-nez v4, :cond_0

    .line 709
    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->h(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/content/Context;

    move-result-object v2

    const-string v4, "preferences_quick_responses"

    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v2, v4, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 716
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/am;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->l(Lcom/android/calendar/preference/QuickResponseSettings;)V

    .line 717
    return-void
.end method
