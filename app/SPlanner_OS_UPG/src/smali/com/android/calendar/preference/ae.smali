.class Lcom/android/calendar/preference/ae;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 197
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Z)Z

    .line 202
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 203
    :goto_1
    if-ge v3, v4, :cond_3

    .line 204
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 205
    const/4 v0, -0x1

    if-eq v5, v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v6

    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v0

    aget-boolean v0, v0, v5

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    aput-boolean v0, v6, v5

    .line 203
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 206
    goto :goto_2

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 210
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 211
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 212
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    iget-object v1, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->f(Lcom/android/calendar/preference/QuickResponseSettings;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;I)V

    .line 218
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/preference/ax;->a([Z)V

    .line 219
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 214
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->g(Lcom/android/calendar/preference/QuickResponseSettings;)V

    .line 215
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0, v4}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;I)V

    goto :goto_3
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/ae;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
