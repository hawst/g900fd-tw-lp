.class Lcom/android/calendar/preference/aa;
.super Ljava/lang/Object;
.source "GeneralPreferences.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/android/calendar/preference/GeneralPreferences;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/GeneralPreferences;Landroid/widget/CheckBox;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/android/calendar/preference/aa;->d:Lcom/android/calendar/preference/GeneralPreferences;

    iput-object p2, p0, Lcom/android/calendar/preference/aa;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/android/calendar/preference/aa;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/calendar/preference/aa;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/android/calendar/preference/aa;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/android/calendar/preference/aa;->d:Lcom/android/calendar/preference/GeneralPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_background_data"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/aa;->d:Lcom/android/calendar/preference/GeneralPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/aa;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/preference/aa;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    if-eqz p1, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 548
    :cond_1
    return-void
.end method
