.class public Lcom/android/calendar/preference/CalendarSoundSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "CalendarSoundSettingsActivity.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Ljava/util/List;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->b:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const-class v1, Lcom/android/calendar/preference/EventNotificationPreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 76
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->b:Z

    .line 77
    iget-boolean v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->b:Z

    if-nez v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, ":android:show_fragment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->d:Ljava/lang/String;

    .line 81
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "key_sound_mode"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 82
    const v0, 0x7f070001

    invoke-virtual {p0, v0, p1}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 83
    iput-object p1, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->c:Ljava/util/List;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 42
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 43
    const-string v0, ":android:show_fragment"

    const-class v2, Lcom/android/calendar/preference/EventNotificationPreference;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v0, ":android:show_fragment_args"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 45
    if-nez v0, :cond_0

    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    :cond_0
    const-string v2, "key_sound_mode"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 50
    const-string v2, ":android:show_fragment_args"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 51
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    invoke-static {}, Lcom/android/calendar/dz;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 56
    const v1, 0x7f0f005d

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 60
    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 61
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 65
    iget-object v0, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 66
    iget-object v2, p0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->d:Ljava/lang/String;

    iget-object v3, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 67
    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->switchToHeader(Landroid/preference/PreferenceActivity$Header;)V

    .line 72
    :cond_3
    return-void

    .line 58
    :cond_4
    const v1, 0x7f0f03f8

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 101
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    const/4 v0, 0x1

    .line 107
    :goto_1
    return v0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    sget-object v0, Lcom/android/calendar/preference/CalendarSoundSettingsActivity;->a:Ljava/lang/String;

    const-string v1, "Unknown IllegalStateException"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 107
    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method
