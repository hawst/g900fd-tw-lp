.class Lcom/android/calendar/preference/ax;
.super Landroid/widget/BaseAdapter;
.source "QuickResponseSettings.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;

.field private b:[Ljava/lang/String;

.field private c:[Z

.field private d:Landroid/view/LayoutInflater;

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 820
    iput-object p1, p0, Lcom/android/calendar/preference/ax;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 821
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/ax;->d:Landroid/view/LayoutInflater;

    .line 822
    iput-object p2, p0, Lcom/android/calendar/preference/ax;->e:Landroid/content/Context;

    .line 823
    invoke-static {p3}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Z)Z

    .line 824
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/android/calendar/preference/ax;->getCount()I

    move-result v1

    .line 838
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 839
    iget-object v2, p0, Lcom/android/calendar/preference/ax;->c:[Z

    aput-boolean p1, v2, v0

    .line 838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 841
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    .line 842
    return-void
.end method

.method public a([Ljava/lang/String;[Z)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/android/calendar/preference/ax;->b:[Ljava/lang/String;

    .line 828
    invoke-virtual {p0, p2}, Lcom/android/calendar/preference/ax;->a([Z)V

    .line 829
    return-void
.end method

.method public a([Z)V
    .locals 0

    .prologue
    .line 845
    iput-object p1, p0, Lcom/android/calendar/preference/ax;->c:[Z

    .line 846
    invoke-virtual {p0}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    .line 847
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/android/calendar/preference/ax;->b:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 852
    const/4 v0, 0x0

    .line 854
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/ax;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 859
    iget-object v0, p0, Lcom/android/calendar/preference/ax;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 864
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 870
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "position is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    if-nez p2, :cond_0

    .line 875
    iget-object v0, p0, Lcom/android/calendar/preference/ax;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04007b

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 880
    :cond_0
    const v0, 0x7f120261

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 881
    iget-object v1, p0, Lcom/android/calendar/preference/ax;->c:[Z

    aget-boolean v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 882
    const v1, 0x7f120262

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 883
    iget-object v2, p0, Lcom/android/calendar/preference/ax;->b:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 884
    iget-object v2, p0, Lcom/android/calendar/preference/ax;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c02ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 885
    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 887
    iget-object v1, p0, Lcom/android/calendar/preference/ax;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 888
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 892
    :goto_0
    return-object p2

    .line 890
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method
