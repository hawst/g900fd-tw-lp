.class Lcom/android/calendar/preference/au;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/at;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/at;)V
    .locals 0

    .prologue
    .line 619
    iput-object p1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 622
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 623
    sget-object v0, Lcom/android/calendar/hj;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-virtual {v1}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 626
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v0, v0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v2, v2, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 627
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->c()[Landroid/preference/EditTextPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v1, v1, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v2, v2, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 628
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->c()[Landroid/preference/EditTextPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v1, v1, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v2, v2, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 629
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_quick_responses"

    iget-object v2, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v2, v2, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v2, v2, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    iget-object v3, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v3, v3, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v3, v3, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v3}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 631
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->dismiss()V

    .line 632
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v0, v0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v1, v1, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v1, v1, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v2, v2, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v2, v2, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/preference/ax;->a([Ljava/lang/String;[Z)V

    .line 633
    iget-object v0, p0, Lcom/android/calendar/preference/au;->a:Lcom/android/calendar/preference/at;

    iget-object v0, v0, Lcom/android/calendar/preference/at;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v0, v0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    .line 634
    return-void
.end method
