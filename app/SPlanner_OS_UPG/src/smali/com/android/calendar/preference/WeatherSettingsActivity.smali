.class public Lcom/android/calendar/preference/WeatherSettingsActivity;
.super Landroid/app/Activity;
.source "WeatherSettingsActivity.java"

# interfaces
.implements Lcom/android/calendar/h/e/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static i:Lcom/android/calendar/preference/ay;


# instance fields
.field private b:Landroid/widget/Switch;

.field private c:Landroid/app/Activity;

.field private d:Z

.field private e:Z

.field private f:Landroid/app/AlertDialog;

.field private g:Landroid/app/AlertDialog;

.field private h:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/WeatherSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 60
    iput-boolean v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->d:Z

    .line 61
    iput-boolean v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->e:Z

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 104
    iput-object p0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    .line 106
    invoke-virtual {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 109
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 110
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 114
    if-eqz v0, :cond_1

    .line 115
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 118
    :cond_1
    new-instance v0, Landroid/widget/Switch;

    iget-object v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    .line 120
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 122
    iget-object v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 123
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 125
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const v3, 0x800015

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 131
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/bb;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/bb;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 140
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/bj;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/bj;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/bk;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/bk;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 173
    invoke-virtual {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 174
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 175
    new-instance v1, Lcom/android/calendar/preference/ay;

    invoke-direct {v1}, Lcom/android/calendar/preference/ay;-><init>()V

    sput-object v1, Lcom/android/calendar/preference/WeatherSettingsActivity;->i:Lcom/android/calendar/preference/ay;

    .line 176
    const v1, 0x1020002

    sget-object v2, Lcom/android/calendar/preference/WeatherSettingsActivity;->i:Lcom/android/calendar/preference/ay;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 177
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 178
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/android/calendar/preference/WeatherSettingsActivity;->b(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/WeatherSettingsActivity;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/preference/WeatherSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->e:Z

    return p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->f:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 184
    const v1, 0x7f0400c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 185
    const v0, 0x7f12007b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 186
    if-eqz v0, :cond_1

    .line 187
    const v2, 0x7f0f0333

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 189
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 190
    const v2, 0x7f0f033a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f046e

    new-instance v3, Lcom/android/calendar/preference/bn;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bn;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f046f

    new-instance v3, Lcom/android/calendar/preference/bm;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bm;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/preference/bl;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/bl;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 229
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->f:Landroid/app/AlertDialog;

    .line 230
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 382
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 383
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 385
    if-ne p1, v0, :cond_1

    .line 386
    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 387
    const-string v3, "SHOW_USE_LOCATION_POPUP"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 388
    sget-object v0, Lcom/android/calendar/hj;->O:Landroid/net/Uri;

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 390
    :cond_0
    sget-object v0, Lcom/android/calendar/preference/WeatherSettingsActivity;->i:Lcom/android/calendar/preference/ay;

    invoke-virtual {v0}, Lcom/android/calendar/preference/ay;->onResume()V

    .line 391
    return-void

    .line 385
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/preference/WeatherSettingsActivity;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/preference/WeatherSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->g:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 238
    const v1, 0x7f0400c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 239
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 240
    const v2, 0x7f0f0470

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0330

    new-instance v3, Lcom/android/calendar/preference/bq;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bq;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/preference/bp;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bp;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/preference/bo;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/bo;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->g:Landroid/app/AlertDialog;

    .line 277
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    :goto_0
    return-void

    .line 284
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 285
    const v1, 0x7f040063

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 286
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 287
    const v2, 0x7f0f046c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0330

    new-instance v3, Lcom/android/calendar/preference/be;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/be;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0472

    new-instance v3, Lcom/android/calendar/preference/bd;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bd;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/preference/bc;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/bc;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 322
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->h:Landroid/app/AlertDialog;

    .line 323
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 327
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 328
    const v1, 0x7f04001a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 329
    const v0, 0x7f12007d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 330
    const v1, 0x7f12007e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 331
    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 333
    new-instance v3, Lcom/android/calendar/preference/bf;

    invoke-direct {v3, p0, v1}, Lcom/android/calendar/preference/bf;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 342
    const v3, 0x7f0f0446

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f02e3

    new-instance v4, Lcom/android/calendar/preference/bi;

    invoke-direct {v4, p0, v1}, Lcom/android/calendar/preference/bi;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/preference/bh;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/bh;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/preference/bg;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/bg;-><init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 378
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 379
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->b()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c()V

    return-void
.end method

.method static synthetic g(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->d()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    return-object v0
.end method


# virtual methods
.method public a(ZZ)V
    .locals 2

    .prologue
    .line 76
    iget-object v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 77
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-direct {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->a()V

    .line 72
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 97
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->finish()V

    .line 99
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    new-instance v0, Lcom/android/calendar/h/e/a;

    invoke-virtual {p0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Lcom/android/calendar/h/e/a;-><init>(Landroid/content/Context;Lcom/android/calendar/h/e/b;)V

    .line 82
    new-array v2, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/android/calendar/h/e/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    iget-object v2, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->b:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->c:Landroid/app/Activity;

    const-string v3, "preferences_weather"

    invoke-static {v0, v3, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 85
    iput-boolean v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->d:Z

    .line 86
    iput-boolean v1, p0, Lcom/android/calendar/preference/WeatherSettingsActivity;->e:Z

    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_0
    move v0, v1

    .line 83
    goto :goto_0
.end method
