.class public Lcom/android/calendar/preference/CustomPreference;
.super Landroid/preference/Preference;
.source "CustomPreference.java"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const v0, 0x7f04002e

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/CustomPreference;->setWidgetLayoutResource(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/android/calendar/preference/CustomPreference;->c:Ljava/lang/String;

    .line 55
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 44
    const v0, 0x7f1200a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/preference/CustomPreference;->a:Landroid/widget/TextView;

    .line 45
    iget-object v0, p0, Lcom/android/calendar/preference/CustomPreference;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/preference/CustomPreference;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const v0, 0x7f1200a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/preference/CustomPreference;->b:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/android/calendar/preference/CustomPreference;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    :cond_0
    return-void
.end method
