.class Lcom/android/calendar/preference/ap;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 906
    iput-object p1, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 909
    iget-object v0, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 910
    iget-object v0, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    .line 911
    if-nez v3, :cond_1

    .line 941
    :cond_0
    return v2

    .line 913
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;Z)Z

    move v1, v2

    .line 915
    :goto_0
    if-ge v1, v3, :cond_0

    .line 916
    iget-object v0, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f120261

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 917
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    .line 919
    invoke-virtual {v0, v6}, Landroid/view/View;->setTranslationX(F)V

    .line 920
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->d()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 927
    iget-object v0, p0, Lcom/android/calendar/preference/ap;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v5, 0x7f120036

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 928
    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 929
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->d()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/calendar/preference/aq;

    invoke-direct {v5, p0, v0}, Lcom/android/calendar/preference/aq;-><init>(Lcom/android/calendar/preference/ap;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 915
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
