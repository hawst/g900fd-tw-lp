.class public Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;
.super Landroid/app/DialogFragment;
.source "QuickResponseSettings.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/Button;

.field private d:I

.field private e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;ILandroid/content/Context;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 590
    iput p2, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->d:I

    .line 591
    iput-object p3, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->e:Landroid/content/Context;

    .line 592
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)I
    .locals 1

    .prologue
    .line 580
    iget v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->d:I

    return v0
.end method

.method private a()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 642
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 643
    const-string v0, "layout_inflater"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 645
    const v2, 0x7f04007c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 646
    const v0, 0x7f120263

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    .line 647
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    new-array v3, v6, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    new-instance v5, Lcom/android/calendar/en;

    invoke-direct {v5, v1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 648
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 649
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 650
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b()V

    .line 651
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/preference/av;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/av;-><init>(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 668
    return-object v2
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->c:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 672
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/preference/aw;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/aw;-><init>(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 684
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->c:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->e:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a()Landroid/view/View;

    move-result-object v0

    .line 597
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 598
    const v2, 0x7f0f035c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 599
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 600
    const v0, 0x7f0f0397

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 601
    const v0, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/preference/as;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/as;-><init>(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 607
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->d:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 611
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->b:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVerticalScrollBarEnabled(Z)V

    .line 612
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 613
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 614
    new-instance v1, Lcom/android/calendar/preference/at;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/at;-><init>(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 638
    return-object v0
.end method
