.class Lcom/android/calendar/preference/ad;
.super Ljava/lang/Object;
.source "QuickResponseAddFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/ab;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/ab;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/android/calendar/preference/ad;->a:Lcom/android/calendar/preference/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 175
    iget-object v0, p0, Lcom/android/calendar/preference/ad;->a:Lcom/android/calendar/preference/ab;

    invoke-static {v0}, Lcom/android/calendar/preference/ab;->a(Lcom/android/calendar/preference/ab;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    .line 184
    :goto_1
    iget-object v2, p0, Lcom/android/calendar/preference/ad;->a:Lcom/android/calendar/preference/ab;

    invoke-static {v2}, Lcom/android/calendar/preference/ab;->a(Lcom/android/calendar/preference/ab;)Landroid/view/View;

    move-result-object v2

    if-nez v0, :cond_2

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_1

    .line 184
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method
