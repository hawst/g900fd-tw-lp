.class Lcom/android/calendar/preference/af;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 954
    iput-object p1, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x1f4

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 957
    iget-object v0, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 958
    iget-object v0, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    .line 959
    if-nez v3, :cond_1

    .line 994
    :cond_0
    return v2

    .line 962
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;Z)Z

    move v1, v2

    .line 964
    :goto_0
    if-ge v1, v3, :cond_0

    .line 965
    iget-object v0, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v4, 0x7f120261

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 966
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 967
    iget-object v0, p0, Lcom/android/calendar/preference/af;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v6, 0x7f120036

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 969
    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->d()Landroid/view/animation/Interpolator;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v7, Lcom/android/calendar/preference/ag;

    invoke-direct {v7, p0, v4}, Lcom/android/calendar/preference/ag;-><init>(Lcom/android/calendar/preference/af;Landroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 981
    int-to-float v4, v5

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setTranslationX(F)V

    .line 982
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->d()Landroid/view/animation/Interpolator;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, Lcom/android/calendar/preference/ah;

    invoke-direct {v5, p0, v0}, Lcom/android/calendar/preference/ah;-><init>(Lcom/android/calendar/preference/af;Landroid/widget/LinearLayout;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 964
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
