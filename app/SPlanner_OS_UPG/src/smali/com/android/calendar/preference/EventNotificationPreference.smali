.class public Lcom/android/calendar/preference/EventNotificationPreference;
.super Landroid/preference/PreferenceFragment;
.source "EventNotificationPreference.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final f:Ljava/util/regex/Pattern;


# instance fields
.field private a:Landroid/preference/ListPreference;

.field private b:Landroid/preference/ListPreference;

.field private c:Landroid/preference/CheckBoxPreference;

.field private d:Landroid/preference/RingtonePreference;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-string v0, "(\\(.*\\))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/EventNotificationPreference;->f:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->e:Z

    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->e:Z

    if-eqz v0, :cond_0

    .line 125
    const v0, 0x7f070004

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->addPreferencesFromResource(I)V

    .line 130
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 131
    const-string v0, "preferences_notification_snooze_duration"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    .line 132
    const-string v0, "preferences_alerts_type"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    .line 133
    const-string v0, "preferences_alerts_ringtone"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/RingtonePreference;

    iput-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->d:Landroid/preference/RingtonePreference;

    .line 135
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;Landroid/preference/PreferenceScreen;)V

    .line 136
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    invoke-static {p1, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/app/Activity;Landroid/preference/ListPreference;)V

    .line 137
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    invoke-static {p1, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;Landroid/preference/ListPreference;)V

    .line 138
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->d:Landroid/preference/RingtonePreference;

    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    .line 139
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->d:Landroid/preference/RingtonePreference;

    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    .line 140
    return-void

    .line 127
    :cond_0
    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->addPreferencesFromResource(I)V

    goto :goto_0
.end method

.method static a(Landroid/app/Activity;Landroid/preference/ListPreference;)V
    .locals 3

    .prologue
    .line 357
    const-string v0, "preferences_alerts"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 358
    const-string v1, "preferences_alerts_popup"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 359
    if-eqz v0, :cond_1

    .line 360
    if-eqz v1, :cond_0

    .line 361
    const-string v0, "0"

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 368
    :goto_0
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 369
    return-void

    .line 363
    :cond_0
    const-string v0, "1"

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 366
    :cond_1
    const-string v0, "2"

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 143
    const-string v0, "preferences_alerts_vibrate"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->c:Landroid/preference/CheckBoxPreference;

    .line 144
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 145
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    :cond_0
    const-string v0, "preferences_alerts_category"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 148
    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 154
    :cond_1
    :goto_0
    return-void

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->c:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 324
    const-string v0, "preferences_notification_snooze"

    invoke-static {p0, v0, p1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 326
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 327
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 328
    const-string v2, "preferences_notification_snooze_duration"

    aget-object v0, v1, v0

    invoke-static {p0, v2, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    return-void

    .line 326
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->d:Landroid/preference/RingtonePreference;

    invoke-virtual {v0, p1}, Landroid/preference/RingtonePreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 268
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 269
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 270
    return-void
.end method

.method static a(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 158
    if-nez p1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "preferences_alerts_ringtone"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_2

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 164
    :cond_2
    invoke-static {p0, p1}, Lcom/android/calendar/preference/EventNotificationPreference;->c(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    goto :goto_0

    .line 166
    :cond_3
    invoke-static {v0, p1}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Ljava/lang/String;Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    invoke-static {p0, p1}, Lcom/android/calendar/preference/EventNotificationPreference;->c(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/app/Activity;)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 190
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v7, v6

    .line 226
    :cond_0
    :goto_0
    return v7

    .line 194
    :cond_1
    const-string v0, "content://settings/system/notification_sound"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v7, v6

    .line 196
    goto :goto_0

    .line 198
    :cond_2
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    .line 201
    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    const-string v0, "external"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 207
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 211
    :goto_1
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v7

    const-string v5, "is_notification"

    aput-object v5, v2, v6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 216
    if-eqz v1, :cond_0

    .line 219
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 220
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v6, :cond_4

    .line 221
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 209
    :cond_3
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 224
    :cond_4
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    .line 225
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v0

    .line 226
    goto :goto_0

    :cond_5
    move v0, v7

    .line 224
    goto :goto_2

    .line 202
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static b(Landroid/app/Activity;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 397
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090030

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 398
    const-string v0, "preferences_notification_snooze_duration"

    aget-object v3, v2, v1

    invoke-static {p0, v0, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 401
    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 402
    aget-object v4, v2, v0

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 407
    :goto_1
    return v0

    .line 401
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected static b(Landroid/app/Activity;Landroid/preference/ListPreference;)V
    .locals 5

    .prologue
    .line 379
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 380
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 382
    invoke-static {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/app/Activity;)I

    move-result v2

    .line 383
    const-string v3, "preferences_notification_snooze"

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-static {p0, v3, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 385
    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 386
    return-void
.end method

.method static b(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 338
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v0

    .line 348
    :cond_0
    :goto_0
    const-string v2, "preferences_alerts"

    invoke-static {p0, v2, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 349
    const-string v1, "preferences_alerts_popup"

    invoke-static {p0, v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 350
    return-void

    .line 341
    :cond_1
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 346
    goto :goto_0
.end method

.method static b(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 231
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "preferences_alerts_ringtone"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 233
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 234
    invoke-static {p1, v1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v2

    .line 236
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 237
    :cond_0
    const-string v0, "ringtone_silent"

    invoke-static {v0}, Lcom/android/calendar/common/b/a;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 259
    :goto_0
    invoke-virtual {p0, v0}, Landroid/preference/RingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 260
    return-void

    .line 238
    :cond_1
    const-string v1, "content://settings/system/notification_sound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 239
    invoke-virtual {v2, p1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 240
    sget-object v0, Lcom/android/calendar/preference/EventNotificationPreference;->f:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 241
    const-string v0, ""

    .line 242
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 243
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 245
    :cond_2
    const v2, 0x7f0f0142

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 246
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 247
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v0, v1

    .line 248
    goto :goto_0

    .line 250
    :cond_3
    if-lez v3, :cond_4

    .line 251
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 253
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 257
    :cond_5
    invoke-virtual {v2, p1}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 173
    const-string v0, "content://media/internal/audio/media/30"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 174
    invoke-static {}, Lcom/android/calendar/dz;->h()Ljava/lang/String;

    move-result-object v3

    .line 175
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v9, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "title LIKE \'%"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 178
    if-eqz v1, :cond_1

    .line 179
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 180
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 183
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 185
    :goto_1
    invoke-virtual {p0}, Landroid/preference/RingtonePreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 186
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "preferences_alerts_ringtone"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 187
    return-void

    :cond_0
    move-object v0, v6

    goto :goto_0

    :cond_1
    move-object v0, v6

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    .line 98
    const-string v2, "com.android.calendar_preferences"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_0

    const-string v2, "key_sound_mode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    const-string v2, "key_sound_mode"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->e:Z

    .line 103
    :cond_0
    const-string v1, "key_sound_mode"

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->e:Z

    .line 106
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 275
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 276
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 277
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 305
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 306
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 308
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 319
    :goto_0
    const/4 v0, 0x0

    :cond_0
    :goto_1
    return v0

    .line 309
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 312
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/EventNotificationPreference;->d:Landroid/preference/RingtonePreference;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 111
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;)V

    .line 117
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 118
    invoke-direct {p0, p0}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 119
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/android/calendar/preference/EventNotificationPreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 283
    if-nez v0, :cond_0

    .line 298
    :goto_0
    return-void

    .line 287
    :cond_0
    const-string v1, "preferences_alerts_type"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 289
    const-class v2, Lcom/android/calendar/alerts/AlertReceiver;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 290
    iget-object v2, p0, Lcom/android/calendar/preference/EventNotificationPreference;->b:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 291
    const-string v2, "removeOldReminders"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 297
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 293
    :cond_2
    const-string v2, "android.intent.action.EVENT_REMINDER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
