.class public Lcom/android/calendar/preference/ab;
.super Landroid/app/Fragment;
.source "QuickResponseAddFragment.java"

# interfaces
.implements Lcom/android/calendar/preference/e;


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Z


# instance fields
.field private c:Landroid/widget/EditText;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/android/calendar/preference/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/ab;->a:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/preference/ab;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/preference/ab;->e:Z

    .line 125
    new-instance v0, Lcom/android/calendar/preference/ac;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/ac;-><init>(Lcom/android/calendar/preference/ab;)V

    iput-object v0, p0, Lcom/android/calendar/preference/ab;->f:Landroid/view/View$OnClickListener;

    .line 172
    new-instance v0, Lcom/android/calendar/preference/ad;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/ad;-><init>(Lcom/android/calendar/preference/ab;)V

    iput-object v0, p0, Lcom/android/calendar/preference/ab;->g:Landroid/text/TextWatcher;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/ab;)Landroid/view/View;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 74
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 75
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 76
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 77
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 78
    const v1, 0x7f04007e

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 80
    const v0, 0x7f1200d5

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    .line 81
    const v0, 0x7f1200d4

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/preference/ab;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 89
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    :cond_0
    return-void

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->d:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/preference/ab;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/android/calendar/preference/ab;->e:Z

    return p1
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "preferences_quick_responses"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 147
    if-eqz v2, :cond_1

    .line 148
    sget-object v0, Lcom/android/calendar/hj;->N:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 149
    array-length v0, v2

    add-int/lit8 v0, v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    move v0, v1

    .line 152
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    .line 153
    aget-object v4, v2, v0

    aput-object v4, v3, v0

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    array-length v4, v2

    if-lez v4, :cond_2

    .line 158
    sget-object v4, Lcom/android/calendar/hj;->M:Ljava/util/regex/Pattern;

    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    aget-object v2, v2, v5

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 159
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 160
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 164
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 165
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ",y,"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    .line 166
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "preferences_quick_responses"

    invoke-static {v0, v2, v3}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 167
    const v0, 0x7f0f01d9

    .line 168
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 170
    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/preference/ab;->e:Z

    if-eqz v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcom/android/calendar/preference/ab;->b()V

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/preference/CalendarSettingsActivity;

    .line 203
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->a(Lcom/android/calendar/preference/e;)V

    .line 204
    invoke-virtual {v0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->onBackPressed()V

    .line 205
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-virtual {v0, p0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->a(Lcom/android/calendar/preference/e;)V

    .line 67
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/ab;->a(Landroid/app/Activity;)V

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 96
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 97
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0a000a

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/preference/ab;->b:Z

    .line 99
    const v0, 0x7f04007a

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 100
    const v0, 0x7f120260

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    .line 101
    iget-object v0, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/android/calendar/preference/ab;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 102
    iget-object v0, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    new-array v3, v5, [Landroid/text/InputFilter;

    new-instance v4, Lcom/android/calendar/en;

    invoke-direct {v4, v1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v6

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    iget-object v0, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 106
    const/4 v0, 0x5

    .line 107
    sget-boolean v3, Lcom/android/calendar/preference/ab;->b:Z

    if-eqz v3, :cond_1

    .line 108
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_0

    .line 109
    const/16 v0, 0x15

    .line 110
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 121
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 122
    return-object v2

    .line 112
    :cond_0
    const/16 v0, 0x35

    .line 113
    iget-object v1, p0, Lcom/android/calendar/preference/ab;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setSingleLine(Z)V

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 118
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x100

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 119
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 60
    invoke-virtual {p0}, Lcom/android/calendar/preference/ab;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/ab;->a(Landroid/app/Activity;)V

    .line 61
    return-void
.end method
