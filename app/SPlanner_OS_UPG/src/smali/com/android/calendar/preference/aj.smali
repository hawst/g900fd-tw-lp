.class Lcom/android/calendar/preference/aj;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 245
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/preference/QuickResponseSettings;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 247
    if-nez v0, :cond_0

    .line 248
    new-instance v0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    iget-object v1, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    iget-object v2, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->h(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, p3, v2}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;ILandroid/content/Context;)V

    .line 250
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/app/Fragment;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 275
    :cond_1
    return-void

    .line 251
    :catch_0
    move-exception v0

    .line 252
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Fail to launch AddCalendarFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->i(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 257
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 259
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v1, p3, v0

    .line 261
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v0

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v0

    aget-boolean v0, v0, v1

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    aput-boolean v0, v2, v1

    .line 263
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    aget-boolean v2, v2, v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 264
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/preference/ax;->a([Z)V

    .line 265
    iget-object v0, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->f(Lcom/android/calendar/preference/QuickResponseSettings;)I

    move-result v0

    .line 266
    if-ltz v0, :cond_0

    .line 267
    iget-object v1, p0, Lcom/android/calendar/preference/aj;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;I)V

    goto/16 :goto_0

    .line 262
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
