.class public Lcom/android/calendar/preference/ay;
.super Landroid/preference/PreferenceFragment;
.source "WeatherPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private a:Lcom/android/calendar/preference/CustomPreference;

.field private b:Landroid/preference/ListPreference;

.field private c:Landroid/preference/ListPreference;

.field private d:Landroid/preference/Preference;

.field private final e:[Ljava/lang/String;

.field private f:I

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "AUTO_REFRESH_TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SHOW_USE_LOCATION_POPUP"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->e:[Ljava/lang/String;

    .line 199
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/ay;)Lcom/android/calendar/preference/CustomPreference;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->a:Lcom/android/calendar/preference/CustomPreference;

    return-object v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 84
    const v0, 0x7f070012

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/ay;->addPreferencesFromResource(I)V

    .line 86
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 87
    const-string v0, "preferences_weather_settings_message"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/preference/CustomPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->a:Lcom/android/calendar/preference/CustomPreference;

    .line 88
    const-string v0, "preferences_weather_unit"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    .line 89
    const-string v0, "preferences_weather_auto_refresh"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    .line 90
    const-string v0, "preferences_use_current_location"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->d:Landroid/preference/Preference;

    .line 91
    new-array v0, v8, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f031a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 93
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0318

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 95
    const/4 v1, 0x2

    const/4 v0, 0x3

    :goto_0
    if-ge v1, v8, :cond_0

    .line 96
    iget-object v2, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0319

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 95
    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/preference/Preference;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 216
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 217
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 220
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 221
    iget-object v3, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    const-string v3, "TEMP_SCALE"

    invoke-virtual {v1, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 225
    const-string v3, "AUTO_REFRESH_TIME"

    invoke-virtual {v1, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 227
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/preference/ay;->d:Landroid/preference/Preference;

    invoke-virtual {p2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 228
    const-string v3, "SHOW_USE_LOCATION_POPUP"

    invoke-virtual {v1, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    if-nez p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 107
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "\u2109"

    .line 108
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 111
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 112
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/calendar/preference/ay;->g:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 115
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/preference/ay;->f:I

    .line 116
    iget v0, p0, Lcom/android/calendar/preference/ay;->f:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0312

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->d:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 107
    :cond_1
    const-string v0, "\u2103"

    goto :goto_1

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0313

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 189
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 191
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/ay;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/ay;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 174
    new-instance v0, Lcom/android/calendar/preference/az;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/az;-><init>(Lcom/android/calendar/preference/ay;)V

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/android/calendar/g/a;->a(Lcom/android/calendar/g/c;Landroid/content/Context;Z)V

    .line 185
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    .line 80
    const-string v1, "com.android.calendar_preferences"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/ay;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 196
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 197
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/ay;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, v0, p1, p2}, Lcom/android/calendar/preference/ay;->a(Landroid/content/Context;Landroid/preference/Preference;Ljava/lang/String;)V

    .line 132
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v1, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/ay;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, v0, p1, p2}, Lcom/android/calendar/preference/ay;->a(Landroid/content/Context;Landroid/preference/Preference;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 138
    const-string v1, "preferences_use_current_location"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 142
    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/ay;->startActivity(Landroid/content/Intent;)V

    .line 144
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 149
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 151
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 152
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 156
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/preference/ay;->a()V

    .line 158
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_weather"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 159
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v3, "preferences_weather"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v3, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f046a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 167
    new-instance v0, Lcom/android/calendar/preference/ba;

    invoke-virtual {p0}, Lcom/android/calendar/preference/ay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/preference/ba;-><init>(Lcom/android/calendar/preference/ay;Landroid/content/ContentResolver;)V

    .line 168
    const/4 v1, -0x1

    sget-object v3, Lcom/android/calendar/hj;->O:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/preference/ay;->e:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/preference/ba;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0, p0}, Lcom/android/calendar/preference/ay;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 170
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/ay;->a(Z)V

    .line 171
    return-void
.end method
