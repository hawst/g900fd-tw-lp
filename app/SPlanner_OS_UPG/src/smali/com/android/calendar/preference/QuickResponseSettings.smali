.class public Lcom/android/calendar/preference/QuickResponseSettings;
.super Landroid/preference/PreferenceFragment;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static c:[Landroid/preference/EditTextPreference;

.field private static k:Z

.field private static v:Landroid/view/animation/Interpolator;

.field private static x:Z


# instance fields
.field private d:Landroid/view/View;

.field private e:Landroid/widget/ListView;

.field private f:[Ljava/lang/String;

.field private g:[Ljava/lang/String;

.field private h:Z

.field private i:[Z

.field private j:Landroid/os/Handler;

.field private l:Landroid/view/accessibility/AccessibilityManager;

.field private m:Lcom/android/calendar/preference/ar;

.field private n:Landroid/content/Context;

.field private o:Landroid/view/ActionMode;

.field private p:Lcom/android/calendar/preference/ax;

.field private q:Landroid/app/AlertDialog;

.field private r:Landroid/view/LayoutInflater;

.field private s:Ljava/util/ArrayList;

.field private t:Z

.field private u:Z

.field private w:Z

.field private y:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->b:Ljava/lang/String;

    .line 94
    const-class v0, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->a:Ljava/lang/String;

    .line 103
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/preference/QuickResponseSettings;->k:Z

    .line 116
    new-instance v0, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    sput-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->v:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 100
    iput-boolean v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->h:Z

    .line 102
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->j:Landroid/os/Handler;

    .line 104
    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->l:Landroid/view/accessibility/AccessibilityManager;

    .line 110
    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->q:Landroid/app/AlertDialog;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->s:Ljava/util/ArrayList;

    .line 113
    iput-boolean v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->t:Z

    .line 115
    iput-boolean v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->u:Z

    .line 118
    iput-boolean v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->w:Z

    .line 120
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->y:Ljava/lang/Boolean;

    .line 999
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->y:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 796
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 812
    :goto_0
    return-void

    .line 799
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f120324

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 800
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->y:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 801
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 802
    const v1, 0x7f0f0398

    invoke-virtual {p0, v1}, Lcom/android/calendar/preference/QuickResponseSettings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 804
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    invoke-virtual {v1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f1200d4

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 805
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 806
    const v2, 0x7f0f00a4

    invoke-virtual {p0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 807
    if-lez p1, :cond_2

    .line 808
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 810
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private a(Landroid/app/Fragment;)V
    .locals 2

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 335
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 336
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 337
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 144
    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 146
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    .line 148
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    if-nez v2, :cond_0

    .line 149
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->e()V

    .line 152
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v2, v2

    iget-object v3, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    array-length v3, v3

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    if-eqz v2, :cond_1

    .line 153
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    .line 154
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v2, v2

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    .line 155
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    iget-object v3, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    invoke-virtual {v2, v3, v4}, Lcom/android/calendar/preference/ax;->a([Ljava/lang/String;[Z)V

    .line 156
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    invoke-virtual {v2}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    .line 159
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ne v2, v5, :cond_5

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 161
    const v3, 0x7f04007f

    invoke-virtual {v2, v3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    .line 169
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 170
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 171
    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 172
    iget-object v3, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    const v3, 0x7f120264

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 174
    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    .line 175
    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    .line 178
    :cond_3
    iget-boolean v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->t:Z

    if-eqz v2, :cond_4

    .line 179
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 180
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    new-instance v3, Lcom/android/calendar/preference/ae;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/ae;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 224
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    new-instance v3, Lcom/android/calendar/preference/ai;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/ai;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 242
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    new-instance v3, Lcom/android/calendar/preference/aj;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/aj;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 278
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    new-instance v3, Lcom/android/calendar/preference/ak;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/ak;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 298
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 299
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 300
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 301
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 304
    :cond_5
    new-array v4, v6, [Landroid/text/InputFilter;

    new-instance v0, Lcom/android/calendar/en;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    aput-object v0, v4, v1

    .line 305
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 306
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Landroid/preference/EditTextPreference;

    sput-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    .line 308
    iget-object v5, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v6, v5

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v6, :cond_7

    aget-object v3, v5, v0

    .line 309
    new-instance v7, Landroid/preference/EditTextPreference;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/preference/EditTextPreference;-><init>(Landroid/content/Context;)V

    .line 310
    invoke-virtual {v7}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v8

    .line 311
    invoke-virtual {v8, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 312
    invoke-virtual {v8}, Landroid/widget/EditText;->getInputType()I

    move-result v9

    .line 313
    or-int/lit16 v9, v9, 0x4000

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setInputType(I)V

    .line 314
    sget-boolean v9, Lcom/android/calendar/preference/QuickResponseSettings;->k:Z

    if-nez v9, :cond_6

    .line 315
    const-string v9, "inputType=PredictionOn"

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 317
    :cond_6
    const v8, 0x7f0f035d

    invoke-virtual {v7, v8}, Landroid/preference/EditTextPreference;->setDialogTitle(I)V

    .line 318
    invoke-virtual {v7, v3}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 319
    invoke-virtual {v7, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 320
    invoke-virtual {v7, p0}, Landroid/preference/EditTextPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 321
    sget-object v8, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    add-int/lit8 v3, v2, 0x1

    aput-object v7, v8, v2

    .line 322
    invoke-virtual {p1, v7}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 308
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_0

    .line 324
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 330
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    .line 331
    return-void

    .line 327
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;I)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/app/Fragment;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Landroid/app/Fragment;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Landroid/preference/PreferenceScreen;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 90
    sget-boolean v0, Lcom/android/calendar/preference/QuickResponseSettings;->x:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->h:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 572
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->g:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 573
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 574
    const/4 v0, 0x1

    .line 577
    :cond_0
    return v0

    .line 572
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 90
    sput-boolean p0, Lcom/android/calendar/preference/QuickResponseSettings;->x:Z

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/preference/QuickResponseSettings;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/QuickResponseSettings;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 560
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 561
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 562
    aget-object v2, p1, v0

    invoke-direct {p0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 563
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",n,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 561
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 565
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",y,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 568
    :cond_1
    return-object v1
.end method

.method static synthetic b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/preference/PreferenceScreen;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 340
    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 341
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 342
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 343
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 344
    const v3, 0x7f04007f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    .line 345
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 346
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 347
    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 348
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    if-nez v0, :cond_2

    .line 352
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->e()V

    .line 355
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    .line 356
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    sget-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 357
    sget-object v2, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 358
    invoke-virtual {p1, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 357
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 366
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    .line 367
    return-void

    .line 363
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_1
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 897
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->u:Z

    if-nez v0, :cond_0

    .line 898
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->m()V

    .line 900
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->u:Z

    if-eqz v0, :cond_1

    .line 901
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->n()V

    .line 903
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/preference/QuickResponseSettings;Z)Z
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->u:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/preference/QuickResponseSettings;Z)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Z)V

    return-void
.end method

.method static synthetic c()[Landroid/preference/EditTextPreference;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    return-object v0
.end method

.method static synthetic d()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/calendar/preference/QuickResponseSettings;->v:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 370
    new-instance v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    .line 371
    new-instance v0, Lcom/android/calendar/preference/ax;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/preference/ax;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    .line 372
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/preference/ax;->a([Ljava/lang/String;[Z)V

    .line 373
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 374
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/preference/QuickResponseSettings;)I
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->l()I

    move-result v0

    return v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->q:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 398
    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 498
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->m:Lcom/android/calendar/preference/ar;

    if-nez v0, :cond_0

    .line 499
    new-instance v0, Lcom/android/calendar/preference/ar;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/preference/ar;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->m:Lcom/android/calendar/preference/ar;

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->m:Lcom/android/calendar/preference/ar;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    .line 502
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->g()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    return-object v0
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 505
    iget-boolean v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->h:Z

    if-eqz v1, :cond_0

    .line 515
    :goto_0
    return v0

    .line 508
    :cond_0
    iput-boolean v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->h:Z

    .line 509
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->j:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/preference/al;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/al;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 515
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/android/calendar/preference/QuickResponseSettings;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->l:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 520
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    sget-object v0, Lcom/android/calendar/hj;->k:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 524
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/preference/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 536
    :goto_0
    return-void

    .line 528
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v3, "preferences_quick_responses"

    move-object v0, v2

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v3, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 530
    if-nez v0, :cond_2

    .line 531
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_quick_responses"

    iget-object v3, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/android/calendar/preference/QuickResponseSettings;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 534
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/android/calendar/preference/ab;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0f0030

    const-string v4, ""

    const/4 v6, 0x0

    move-object v5, v2

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    const v3, 0x7f0f0143

    .line 688
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 689
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 690
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->l()I

    move-result v0

    .line 692
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 693
    const v0, 0x7f0f0357

    .line 699
    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 700
    new-instance v0, Lcom/android/calendar/preference/am;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/am;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 720
    const/high16 v0, 0x1040000

    new-instance v2, Lcom/android/calendar/preference/an;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/an;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 727
    new-instance v0, Lcom/android/calendar/preference/ao;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/ao;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 735
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->q:Landroid/app/AlertDialog;

    .line 736
    return-void

    .line 694
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    array-length v2, v2

    if-ne v0, v2, :cond_1

    .line 695
    const v0, 0x7f0f0356

    goto :goto_0

    .line 697
    :cond_1
    const v0, 0x7f0f0358

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    return-object v0
.end method

.method private k()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 747
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "preferences_quick_responses"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 749
    if-nez v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->g:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 752
    :cond_0
    sget-object v1, Lcom/android/calendar/hj;->N:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 753
    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/preference/QuickResponseSettings;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->k()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 757
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    if-nez v1, :cond_1

    .line 766
    :cond_0
    return v0

    .line 761
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-boolean v4, v2, v1

    .line 762
    if-eqz v4, :cond_2

    .line 763
    add-int/lit8 v0, v0, 0x1

    .line 761
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic l(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->f()V

    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 906
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/preference/ap;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/ap;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 944
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    move v0, v1

    .line 945
    :goto_0
    if-ge v0, v2, :cond_0

    .line 946
    iget-object v3, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f120261

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 947
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 945
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 949
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    .line 950
    return-void
.end method

.method static synthetic m(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->j()V

    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 954
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/preference/af;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/af;-><init>(Lcom/android/calendar/preference/QuickResponseSettings;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 997
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 428
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 429
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    .line 430
    sget-boolean v0, Lcom/android/calendar/preference/QuickResponseSettings;->k:Z

    if-eqz v0, :cond_2

    .line 431
    sget-object v3, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 432
    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v5

    .line 434
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 435
    invoke-virtual {v0}, Landroid/preference/EditTextPreference;->getEditText()Landroid/widget/EditText;

    move-result-object v6

    .line 436
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_1

    .line 437
    const/16 v0, 0x15

    .line 438
    invoke-virtual {v6, v2}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 439
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    .line 445
    :goto_1
    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 431
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 441
    :cond_1
    const/16 v0, 0x35

    .line 442
    invoke-virtual {v6, v8}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 443
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1

    .line 449
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 450
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 451
    const v0, 0x7f04007f

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    .line 459
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 460
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 461
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 462
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->d:Landroid/view/View;

    const v1, 0x7f120264

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 464
    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    .line 465
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 468
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Landroid/preference/PreferenceScreen;)V

    .line 469
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 124
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->setHasOptionsMenu(Z)V

    .line 125
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    .line 126
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->t:Z

    .line 127
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->x(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->w:Z

    .line 129
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    .line 130
    array-length v0, v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    .line 132
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/preference/QuickResponseSettings;->k:Z

    .line 133
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 134
    const v1, 0x7f0f035f

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 136
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->g:[Ljava/lang/String;

    .line 137
    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 138
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->l:Landroid/view/accessibility/AccessibilityManager;

    .line 140
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->r:Landroid/view/LayoutInflater;

    .line 141
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 415
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 416
    const v0, 0x7f11000b

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 417
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->o:Landroid/view/ActionMode;

    .line 772
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 773
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 473
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 474
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 475
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 494
    :goto_0
    return v0

    .line 478
    :cond_0
    sparse-switch v1, :sswitch_data_0

    .line 494
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 480
    :sswitch_0
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->g()V

    .line 481
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->y:Ljava/lang/Boolean;

    .line 482
    invoke-direct {p0, v3}, Lcom/android/calendar/preference/QuickResponseSettings;->a(I)V

    .line 483
    sput-boolean v0, Lcom/android/calendar/preference/QuickResponseSettings;->x:Z

    .line 484
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->i:[Z

    invoke-virtual {v1, v2}, Lcom/android/calendar/preference/ax;->a([Z)V

    .line 485
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->p:Lcom/android/calendar/preference/ax;

    invoke-virtual {v1}, Lcom/android/calendar/preference/ax;->notifyDataSetChanged()V

    goto :goto_0

    .line 488
    :sswitch_1
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 489
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->i()V

    goto :goto_0

    .line 478
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f120324 -> :sswitch_0
        0x7f12033c -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 403
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 404
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 541
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    sget-object v0, Lcom/android/calendar/hj;->k:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    :cond_0
    move v0, v1

    .line 544
    :goto_0
    sget-object v2, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 545
    sget-object v2, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/preference/EditTextPreference;->compareTo(Landroid/preference/Preference;)I

    move-result v2

    if-nez v2, :cond_3

    .line 546
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 547
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 548
    sget-object v1, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/preference/EditTextPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 549
    sget-object v1, Lcom/android/calendar/preference/QuickResponseSettings;->c:[Landroid/preference/EditTextPreference;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 550
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_quick_responses"

    iget-object v2, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 553
    :cond_1
    const/4 v1, 0x1

    .line 556
    :cond_2
    return v1

    .line 544
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 421
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 422
    const v0, 0x7f120324

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 423
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 424
    return-void

    .line 423
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 378
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 379
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 380
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->q(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    .line 381
    iget-object v1, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->f()V

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/preference/QuickResponseSettings;->f:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 385
    invoke-direct {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->f()V

    .line 387
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Landroid/preference/PreferenceScreen;)V

    .line 388
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 408
    const-string v0, "preferences_quick_responses"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Landroid/preference/PreferenceScreen;)V

    .line 411
    :cond_0
    return-void
.end method
