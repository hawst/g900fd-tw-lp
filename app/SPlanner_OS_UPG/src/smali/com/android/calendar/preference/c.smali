.class Lcom/android/calendar/preference/c;
.super Ljava/lang/Object;
.source "AllCalendarPreferences.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/AllCalendarPreferences;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/AllCalendarPreferences;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const v4, 0x7f0f0393

    const/4 v3, 0x0

    .line 469
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 474
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 475
    if-eqz v0, :cond_1

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v0, :cond_1

    .line 476
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 477
    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 478
    const-string v1, "directcall"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 479
    const-string v1, "CallerType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 480
    const-string v1, "GUID"

    const-string v2, "com.sec.android.providers.chinaholiday"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 481
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 482
    iget-object v1, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v1, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->startActivity(Landroid/content/Intent;)V

    .line 490
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0393

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 486
    :catch_0
    move-exception v0

    .line 487
    iget-object v1, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/preference/c;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v2}, Lcom/android/calendar/preference/AllCalendarPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 488
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
