.class Lcom/android/calendar/preference/ak;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 281
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 282
    invoke-static {}, Lcom/android/calendar/preference/QuickResponseSettings;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 294
    :goto_0
    return v0

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v1

    if-nez v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    iget-object v2, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-virtual {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;Landroid/preference/PreferenceScreen;)V

    .line 288
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v1

    aput-boolean v0, v1, p3

    .line 289
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->g(Lcom/android/calendar/preference/QuickResponseSettings;)V

    .line 290
    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Z)Z

    .line 291
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1, v0}, Lcom/android/calendar/preference/QuickResponseSettings;->a(Lcom/android/calendar/preference/QuickResponseSettings;I)V

    .line 293
    iget-object v1, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v1}, Lcom/android/calendar/preference/QuickResponseSettings;->b(Lcom/android/calendar/preference/QuickResponseSettings;)Lcom/android/calendar/preference/ax;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/preference/ak;->a:Lcom/android/calendar/preference/QuickResponseSettings;

    invoke-static {v2}, Lcom/android/calendar/preference/QuickResponseSettings;->c(Lcom/android/calendar/preference/QuickResponseSettings;)[Z

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/preference/ax;->a([Z)V

    goto :goto_0
.end method
