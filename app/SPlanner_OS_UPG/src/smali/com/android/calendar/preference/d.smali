.class Lcom/android/calendar/preference/d;
.super Ljava/lang/Object;
.source "CalendarSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/CalendarSettingsActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/CalendarSettingsActivity;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/android/calendar/preference/d;->a:Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/calendar/preference/d;->a:Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/preference/d;->a:Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-static {v1}, Lcom/android/calendar/preference/CalendarSettingsActivity;->a(Lcom/android/calendar/preference/CalendarSettingsActivity;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/android/calendar/preference/d;->a:Lcom/android/calendar/preference/CalendarSettingsActivity;

    invoke-virtual {v0}, Lcom/android/calendar/preference/CalendarSettingsActivity;->invalidateHeaders()V

    .line 193
    :cond_0
    return-void
.end method
