.class Lcom/android/calendar/preference/bk;
.super Ljava/lang/Object;
.source "WeatherSettingsActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/WeatherSettingsActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->a(Lcom/android/calendar/preference/WeatherSettingsActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->b(Lcom/android/calendar/preference/WeatherSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 152
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_background_data"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_weather"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->d(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    .line 169
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-virtual {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "preferences_weather"

    invoke-static {v0, v1, p2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 170
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->e(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->f(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->s(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->t(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/android/calendar/preference/bk;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->g(Lcom/android/calendar/preference/WeatherSettingsActivity;)V

    goto :goto_0
.end method
