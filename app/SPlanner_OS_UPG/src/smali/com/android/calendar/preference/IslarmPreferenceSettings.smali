.class public Lcom/android/calendar/preference/IslarmPreferenceSettings;
.super Landroid/preference/PreferenceFragment;
.source "IslarmPreferenceSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static d:[Ljava/lang/String;


# instance fields
.field private a:Landroid/preference/ListPreference;

.field private b:Landroid/preference/ListPreference;

.field private c:Landroid/preference/ListPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "-5"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "-4"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "-3"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "-2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "-1"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "2"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "3"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "4"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "5"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 83
    const/4 v0, 0x2

    sget-object v1, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v1, v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/CharSequence;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/CharSequence;

    move v1, v2

    .line 85
    :goto_0
    sget-object v3, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 86
    aget-object v3, v0, v2

    sget-object v4, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    aget-object v4, v4, v1

    aput-object v4, v3, v1

    .line 87
    sget-object v3, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 89
    const-string v3, ""

    .line 90
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v5, :cond_6

    move v4, v2

    .line 93
    :goto_1
    if-ltz v6, :cond_0

    .line 94
    const-string v3, "+"

    .line 96
    :cond_0
    aget-object v7, v0, v5

    if-eqz v4, :cond_1

    const v4, 0x7f0f02c6

    invoke-virtual {p0, v4}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    new-array v8, v5, [Ljava/lang/Object;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, "%d"

    new-array v10, v5, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v2

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v1

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_1
    const v4, 0x7f0f02c7

    invoke-virtual {p0, v4}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 102
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    aget-object v3, v0, v5

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 105
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    aget-object v3, v0, v2

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 106
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    aget-object v3, v0, v5

    invoke-virtual {v1, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 108
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 109
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    aget-object v2, v0, v5

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    .line 112
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    sget-object v2, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 115
    aget-object v1, v0, v5

    sget-object v2, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x2

    aget-object v1, v1, v2

    .line 117
    :cond_3
    iget-object v2, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    .line 120
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    sget-object v2, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 122
    aget-object v1, v0, v5

    sget-object v2, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x2

    aget-object v1, v1, v2

    .line 124
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    .line 127
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 128
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    sget-object v2, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 129
    aget-object v0, v0, v5

    sget-object v1, Lcom/android/calendar/preference/IslarmPreferenceSettings;->d:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    aget-object v0, v0, v1

    .line 131
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 132
    return-void

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move v4, v5

    goto/16 :goto_1
.end method

.method private a(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/android/calendar/dz;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 77
    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 78
    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->addPreferencesFromResource(I)V

    .line 51
    invoke-virtual {p0}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 52
    const-string v0, "preferences_islarm_correction"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    .line 54
    const-string v0, "preferences_ramadan_start_day"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    .line 56
    const-string v0, "preferences_ramadan_end_day"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    .line 58
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 64
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->b:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/IslarmPreferenceSettings;->c:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p1

    .line 139
    check-cast v0, Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    move-object v0, p1

    .line 140
    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 142
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 68
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 70
    invoke-direct {p0}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a()V

    .line 71
    invoke-direct {p0, p0}, Lcom/android/calendar/preference/IslarmPreferenceSettings;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 72
    return-void
.end method
