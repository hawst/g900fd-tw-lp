.class Lcom/android/calendar/preference/av;
.super Ljava/lang/Object;
.source "QuickResponseSettings.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)V
    .locals 0

    .prologue
    .line 651
    iput-object p1, p0, Lcom/android/calendar/preference/av;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 666
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 661
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/android/calendar/preference/av;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->c(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/android/calendar/preference/av;->a:Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;

    invoke-static {v0}, Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;->c(Lcom/android/calendar/preference/QuickResponseSettings$EditDialogFragment;)Landroid/widget/Button;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 657
    :cond_0
    return-void

    .line 655
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
