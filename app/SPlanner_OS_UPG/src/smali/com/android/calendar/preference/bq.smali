.class Lcom/android/calendar/preference/bq;
.super Ljava/lang/Object;
.source "WeatherSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/WeatherSettingsActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/WeatherSettingsActivity;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/android/calendar/preference/bq;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 245
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/preference/bq;->a:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :goto_0
    if-eqz p1, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 256
    :cond_0
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
