.class Lcom/android/calendar/preference/a;
.super Ljava/lang/Object;
.source "AllCalendarPreferences.java"

# interfaces
.implements Lcom/android/calendar/preference/h;


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/AllCalendarPreferences;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/AllCalendarPreferences;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 442
    if-eqz p1, :cond_1

    .line 443
    iget-object v0, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-static {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Lcom/android/calendar/preference/AllCalendarPreferences;)V

    .line 454
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-static {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->b(Lcom/android/calendar/preference/AllCalendarPreferences;)Lcom/android/calendar/preference/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/preference/f;->a()V

    .line 455
    return-void

    .line 447
    :cond_1
    invoke-static {}, Lcom/android/calendar/preference/f;->b()Ljava/lang/String;

    move-result-object v0

    .line 448
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 449
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0054

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 451
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/a;->a:Lcom/android/calendar/preference/AllCalendarPreferences;

    invoke-virtual {v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f03d5

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
