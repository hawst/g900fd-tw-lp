.class public Lcom/android/calendar/preference/GeneralPreferences;
.super Landroid/preference/PreferenceFragment;
.source "GeneralPreferences.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static a:Z

.field private static j:Landroid/app/AlertDialog;

.field private static final k:I


# instance fields
.field private b:Landroid/preference/ListPreference;

.field private c:Landroid/preference/ListPreference;

.field private d:Landroid/preference/CheckBoxPreference;

.field private e:Landroid/preference/CheckBoxPreference;

.field private f:Landroid/preference/ListPreference;

.field private g:[[Ljava/lang/CharSequence;

.field private h:Landroid/preference/CheckBoxPreference;

.field private i:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 199
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/preference/GeneralPreferences;->a:Z

    .line 213
    const-string v0, "TMO"

    invoke-static {}, Lcom/android/calendar/dz;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    const v0, 0x7f07000b

    sput v0, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    .line 224
    :goto_0
    return-void

    .line 215
    :cond_0
    const-string v0, "VZW"

    invoke-static {}, Lcom/android/calendar/dz;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    const v0, 0x7f07000c

    sput v0, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    goto :goto_0

    .line 217
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    const v0, 0x7f07000a

    sput v0, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    goto :goto_0

    .line 219
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    const v0, 0x7f070009

    sput v0, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    goto :goto_0

    .line 222
    :cond_3
    const v0, 0x7f070008

    sput v0, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 228
    const-string v0, "com.android.calendar_preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/preference/GeneralPreferences;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)Landroid/text/SpannableStringBuilder;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 396
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 397
    invoke-static {}, Lcom/android/calendar/h/c;->a()Lcom/android/calendar/h/c;

    move-result-object v2

    .line 398
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-object v0

    .line 401
    :cond_1
    invoke-virtual {v2}, Lcom/android/calendar/h/c;->c()I

    move-result v2

    .line 402
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 406
    new-instance v0, Landroid/text/SpannableStringBuilder;

    const-string v3, " "

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 407
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 408
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 409
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x1

    const/16 v3, 0x21

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 414
    if-nez p0, :cond_0

    .line 422
    :goto_0
    return-void

    .line 417
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 418
    const-string v1, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    const-string v1, "key"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 420
    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Landroid/app/Activity;Z)V
    .locals 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 275
    if-eqz p2, :cond_3

    .line 276
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 277
    :cond_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 278
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 286
    :cond_1
    :goto_0
    new-instance v1, Lcom/android/calendar/timezone/v;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    invoke-virtual {v1}, Lcom/android/calendar/timezone/v;->b()[[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->g:[[Ljava/lang/CharSequence;

    .line 287
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->g:[[Ljava/lang/CharSequence;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 288
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->g:[[Ljava/lang/CharSequence;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 289
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    .line 290
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 291
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 293
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 294
    return-void

    .line 281
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_1

    .line 282
    :cond_4
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 436
    invoke-static {p0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    .line 437
    if-eqz v0, :cond_0

    .line 438
    const-string v0, "preferences_show_week_num"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    const-string v0, "preferences_week_number_dialog"

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 440
    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/preference/GeneralPreferences;->a:Z

    if-nez v0, :cond_0

    .line 441
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->c(Landroid/content/Context;)V

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 443
    :cond_1
    const-string v0, "preferences_week_start_day"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    const-string v0, "preferences_start_day_dialog"

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 445
    if-nez v0, :cond_0

    sget-object v0, Lcom/android/calendar/preference/GeneralPreferences;->j:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/preference/GeneralPreferences;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 446
    :cond_2
    invoke-static {p0}, Lcom/android/calendar/preference/GeneralPreferences;->d(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 311
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 312
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 313
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 314
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 315
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 316
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 319
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 519
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 520
    const v1, 0x7f04001a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 521
    const v0, 0x7f12007d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 522
    const v1, 0x7f12007e

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 523
    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 525
    new-instance v3, Lcom/android/calendar/preference/y;

    invoke-direct {v3, p0, v1}, Lcom/android/calendar/preference/y;-><init>(Lcom/android/calendar/preference/GeneralPreferences;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 534
    const v3, 0x7f0f0446

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f02e3

    new-instance v4, Lcom/android/calendar/preference/aa;

    invoke-direct {v4, p0, v1, p1, p2}, Lcom/android/calendar/preference/aa;-><init>(Lcom/android/calendar/preference/GeneralPreferences;Landroid/widget/CheckBox;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/preference/z;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/z;-><init>(Lcom/android/calendar/preference/GeneralPreferences;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 559
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 560
    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 59
    sput-boolean p0, Lcom/android/calendar/preference/GeneralPreferences;->a:Z

    return p0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233
    const-string v0, "com.android.calendar_preferences"

    sget v1, Lcom/android/calendar/preference/GeneralPreferences;->k:I

    invoke-static {p0, v0, v2, v1, v2}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 235
    return-void
.end method

.method static c(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 453
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/preference/GeneralPreferences;->a:Z

    .line 454
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 455
    const v1, 0x7f040062

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 456
    const v0, 0x7f1201e5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 457
    const v1, 0x7f12007b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 458
    const v2, 0x7f12007e

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 459
    new-instance v4, Lcom/android/calendar/preference/t;

    invoke-direct {v4, v2}, Lcom/android/calendar/preference/t;-><init>(Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 466
    const v0, 0x7f0f0229

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 467
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 468
    const v1, 0x7f0f022c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 469
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 470
    const v1, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/preference/u;

    invoke-direct {v3, v2, p0}, Lcom/android/calendar/preference/u;-><init>(Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 479
    new-instance v1, Lcom/android/calendar/preference/v;

    invoke-direct {v1}, Lcom/android/calendar/preference/v;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 485
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 486
    return-void
.end method

.method static d(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 489
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 490
    const v1, 0x7f040062

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 491
    const v0, 0x7f1201e5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 492
    const v1, 0x7f12007b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 493
    const v2, 0x7f12007e

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 494
    new-instance v4, Lcom/android/calendar/preference/w;

    invoke-direct {v4, v2}, Lcom/android/calendar/preference/w;-><init>(Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 501
    const v0, 0x7f0f0228

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 503
    const v1, 0x7f0f022c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 504
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 505
    const v1, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/preference/x;

    invoke-direct {v3, v2, p0}, Lcom/android/calendar/preference/x;-><init>(Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 514
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/GeneralPreferences;->j:Landroid/app/AlertDialog;

    .line 515
    sget-object v0, Lcom/android/calendar/preference/GeneralPreferences;->j:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 516
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 239
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 241
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 244
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    .line 245
    const-string v2, "com.android.calendar_preferences"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 248
    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/GeneralPreferences;->addPreferencesFromResource(I)V

    .line 249
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/GeneralPreferences;->addPreferencesFromResource(I)V

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 254
    const-string v0, "preferences_month_views"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    .line 255
    const-string v0, "preferences_week_start_day"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    .line 256
    const-string v0, "preferences_hide_declined"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->d:Landroid/preference/CheckBoxPreference;

    .line 257
    const-string v0, "preferences_hide_completed_task"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->h:Landroid/preference/CheckBoxPreference;

    .line 258
    const-string v0, "preferences_home_tz_enabled"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->e:Landroid/preference/CheckBoxPreference;

    .line 259
    const-string v0, "preferences_home_tz"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    .line 260
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 263
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string v0, "preferences_weather"

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    .line 265
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 270
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Z)V

    .line 271
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 325
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 327
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 343
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 344
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    move v0, v2

    .line 391
    :goto_1
    return v0

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 349
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 350
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v3, v0, p2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 351
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->d:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 353
    new-instance v2, Landroid/content/Intent;

    invoke-static {v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 354
    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "vnd.android.data/update"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    invoke-virtual {v3, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 356
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v2, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 357
    goto :goto_1

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 359
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 360
    new-instance v2, Landroid/content/Intent;

    invoke-static {v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 361
    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "vnd.android.data/update"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    invoke-virtual {v3, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 363
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v2, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 364
    goto/16 :goto_1

    .line 365
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 366
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 367
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 371
    :goto_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Z)V

    .line 372
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    .line 373
    goto/16 :goto_1

    .line 369
    :cond_4
    const-string v0, "auto"

    goto :goto_2

    .line 374
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 375
    check-cast p2, Ljava/lang/String;

    .line 377
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/GeneralPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 379
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 380
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/preference/GeneralPreferences;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 381
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v4, "preferences_confirm_background_data"

    invoke-static {v0, v4, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    .line 383
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    move v0, v1

    .line 387
    goto/16 :goto_1

    .line 385
    :cond_7
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    move v0, v1

    .line 389
    goto/16 :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 426
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 427
    const-string v1, "preferences_show_week_num"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 428
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 432
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0

    .line 429
    :cond_1
    const-string v1, "preferences_week_start_day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 430
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 298
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 299
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 301
    invoke-direct {p0, p0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 302
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Z)V

    .line 303
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/android/calendar/preference/GeneralPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 332
    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V

    .line 335
    :cond_0
    return-void
.end method
