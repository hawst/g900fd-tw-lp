.class Lcom/android/calendar/preference/bi;
.super Ljava/lang/Object;
.source "WeatherSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Lcom/android/calendar/preference/WeatherSettingsActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/preference/WeatherSettingsActivity;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/android/calendar/preference/bi;->b:Lcom/android/calendar/preference/WeatherSettingsActivity;

    iput-object p2, p0, Lcom/android/calendar/preference/bi;->a:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 347
    iget-object v0, p0, Lcom/android/calendar/preference/bi;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/android/calendar/preference/bi;->b:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_background_data"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/bi;->b:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/preference/WeatherSettingsActivity;->b(Lcom/android/calendar/preference/WeatherSettingsActivity;Z)Z

    .line 352
    iget-object v0, p0, Lcom/android/calendar/preference/bi;->b:Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-static {v0}, Lcom/android/calendar/preference/WeatherSettingsActivity;->c(Lcom/android/calendar/preference/WeatherSettingsActivity;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/calendar/preference/WeatherSettingsActivity;->a(Landroid/content/Context;Z)V

    .line 353
    if-eqz p1, :cond_1

    move-object v0, p1

    check-cast v0, Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 356
    :cond_1
    return-void
.end method
