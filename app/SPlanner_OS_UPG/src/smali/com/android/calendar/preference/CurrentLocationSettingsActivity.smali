.class public Lcom/android/calendar/preference/CurrentLocationSettingsActivity;
.super Landroid/app/Activity;
.source "CurrentLocationSettingsActivity.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Switch;

.field private c:Landroid/app/Activity;

.field private d:Z

.field private e:Z

.field private f:Landroid/app/AlertDialog;

.field private g:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->d:Z

    .line 52
    iput-boolean v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->e:Z

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 97
    iput-object p0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    .line 99
    invoke-virtual {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 102
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 103
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_1

    .line 108
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 111
    :cond_1
    new-instance v0, Landroid/widget/Switch;

    iget-object v1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    .line 113
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 116
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 118
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const v3, 0x800015

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 123
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/k;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/k;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 132
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/l;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/l;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 139
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    new-instance v1, Lcom/android/calendar/preference/m;

    invoke-direct {v1, p0}, Lcom/android/calendar/preference/m;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 155
    const v0, 0x7f04002a

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->setContentView(I)V

    .line 156
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b(Landroid/content/Context;Z)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->e:Z

    return p1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->f:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 177
    const v1, 0x7f0400c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 178
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 179
    const v2, 0x7f0f0470

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0330

    new-instance v3, Lcom/android/calendar/preference/p;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/p;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/preference/o;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/o;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/preference/n;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/n;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->f:Landroid/app/AlertDialog;

    .line 218
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 158
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 161
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 162
    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 163
    const-string v3, "SHOW_USE_LOCATION_POPUP"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 164
    sget-object v0, Lcom/android/calendar/hj;->O:Landroid/net/Uri;

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 165
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_0

    .line 166
    invoke-static {p0}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Z)V

    .line 169
    :cond_0
    return-void

    .line 161
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->d:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->g:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 226
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 227
    const v1, 0x7f040063

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 228
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 229
    const v2, 0x7f0f046c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0330

    new-instance v3, Lcom/android/calendar/preference/s;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/s;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0472

    new-instance v3, Lcom/android/calendar/preference/r;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/r;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/preference/q;

    invoke-direct {v2, p0}, Lcom/android/calendar/preference/q;-><init>(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->g:Landroid/app/AlertDialog;

    .line 265
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/preference/CurrentLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 93
    invoke-virtual {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->finish()V

    .line 94
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-direct {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->a()V

    .line 61
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 84
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->onBackPressed()V

    .line 86
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->b:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->c:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/hj;->H(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 72
    iput-boolean v2, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->d:Z

    .line 73
    iput-boolean v2, p0, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->e:Z

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 66
    invoke-virtual {p0}, Lcom/android/calendar/preference/CurrentLocationSettingsActivity;->invalidateOptionsMenu()V

    .line 67
    return-void
.end method
