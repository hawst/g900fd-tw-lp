.class Lcom/android/calendar/preference/i;
.super Landroid/os/AsyncTask;
.source "ChinaHolidayUpdation.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/preference/f;


# direct methods
.method private constructor <init>(Lcom/android/calendar/preference/f;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/preference/f;Lcom/android/calendar/preference/g;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/android/calendar/preference/i;-><init>(Lcom/android/calendar/preference/f;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    const-string v0, "ChinaHolidayUpdation"

    const-string v1, "CheckTask : start update check"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string v0, ""

    invoke-static {v0}, Lcom/android/calendar/preference/f;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->a(Lcom/android/calendar/preference/f;)Z

    move-result v0

    .line 115
    if-nez v0, :cond_0

    .line 116
    const-string v0, "ChinaHolidayUpdation"

    const-string v1, "CheckTask : fail to get base server url for china model"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    .line 120
    :cond_0
    invoke-static {}, Lcom/android/calendar/preference/f;->c()Ljava/lang/String;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    :cond_1
    const-string v0, "ChinaHolidayUpdation"

    const-string v1, "CheckTask : server_url is null or empty"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v1, v0}, Lcom/android/calendar/preference/f;->a(Lcom/android/calendar/preference/f;Ljava/lang/String;)Z

    move-result v0

    .line 128
    if-eqz v0, :cond_3

    .line 129
    const-string v0, "ChinaHolidayUpdation"

    const-string v1, "CheckTask : Update needed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0, v3}, Lcom/android/calendar/preference/f;->a(Lcom/android/calendar/preference/f;Z)Z

    .line 131
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_3
    const-string v0, "ChinaHolidayUpdation"

    const-string v1, "CheckTask : No update"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->b(Lcom/android/calendar/preference/f;)Lcom/android/calendar/preference/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->b(Lcom/android/calendar/preference/f;)Lcom/android/calendar/preference/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/preference/i;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->c(Lcom/android/calendar/preference/f;)Lcom/android/calendar/preference/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->c(Lcom/android/calendar/preference/f;)Lcom/android/calendar/preference/h;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v1}, Lcom/android/calendar/preference/f;->d(Lcom/android/calendar/preference/f;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/calendar/preference/h;->a(Z)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    invoke-static {v0}, Lcom/android/calendar/preference/f;->b(Lcom/android/calendar/preference/f;)Lcom/android/calendar/preference/i;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/android/calendar/preference/i;->a:Lcom/android/calendar/preference/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/preference/f;->a(Lcom/android/calendar/preference/f;Lcom/android/calendar/preference/i;)Lcom/android/calendar/preference/i;

    .line 146
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/android/calendar/preference/i;->a([Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 107
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/calendar/preference/i;->a(Ljava/lang/Integer;)V

    return-void
.end method
