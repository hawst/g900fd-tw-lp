.class public Lcom/android/calendar/preference/AllCalendarPreferences;
.super Landroid/preference/PreferenceFragment;
.source "AllCalendarPreferences.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/calendar/h/e/b;


# static fields
.field public static a:Landroid/preference/CheckBoxPreference;

.field public static b:Landroid/preference/ListPreference;

.field public static c:Landroid/preference/ListPreference;

.field public static d:Landroid/preference/Preference;


# instance fields
.field private e:Landroid/preference/ListPreference;

.field private f:Landroid/preference/ListPreference;

.field private g:Landroid/preference/CheckBoxPreference;

.field private h:Landroid/preference/CheckBoxPreference;

.field private i:Landroid/preference/CheckBoxPreference;

.field private j:[[Ljava/lang/CharSequence;

.field private k:Landroid/preference/ListPreference;

.field private l:Landroid/preference/ListPreference;

.field private m:Landroid/preference/CheckBoxPreference;

.field private n:Landroid/preference/RingtonePreference;

.field private o:Landroid/preference/CheckBoxPreference;

.field private p:Z

.field private q:Lcom/android/calendar/preference/f;

.field private final r:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 82
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->r:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 262
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;Landroid/preference/ListPreference;)V

    .line 263
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/app/Activity;Landroid/preference/ListPreference;)V

    .line 264
    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 96
    const v0, 0x7f070005

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 98
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_4

    .line 100
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 106
    :cond_0
    :goto_0
    const v0, 0x7f07000f

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 108
    const v0, 0x7f070003

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 109
    const v0, 0x7f07000e

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 111
    iget-boolean v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->r:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->G(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    const v0, 0x7f070002

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 115
    const-string v0, "preferences_month_views"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    .line 116
    const-string v0, "preferences_week_start_day"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    .line 117
    const-string v0, "preferences_hide_declined"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->g:Landroid/preference/CheckBoxPreference;

    .line 118
    const-string v0, "preferences_hide_completed_task"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->i:Landroid/preference/CheckBoxPreference;

    .line 119
    const-string v0, "preferences_home_tz_enabled"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    sput-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    .line 120
    const-string v0, "preferences_home_tz"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    sput-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    .line 121
    const-string v0, "preferences_today_tz"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    sput-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    .line 122
    const-string v0, "preferences_notification_snooze_duration"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    .line 123
    const-string v0, "preferences_alerts_type"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    .line 124
    const-string v0, "preferences_alerts_ringtone"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/RingtonePreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->n:Landroid/preference/RingtonePreference;

    .line 125
    const-string v0, "preference_gal_search"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->o:Landroid/preference/CheckBoxPreference;

    .line 126
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_5

    .line 127
    const-string v0, "preferences_weather"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->d:Landroid/preference/Preference;

    .line 133
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Z)V

    .line 134
    invoke-direct {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a()V

    .line 135
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Landroid/preference/PreferenceScreen;)V

    .line 137
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->o:Landroid/preference/CheckBoxPreference;

    invoke-static {v0, v2}, Lcom/android/calendar/preference/SyncPreferences;->a(Landroid/content/Context;Landroid/preference/CheckBoxPreference;)V

    .line 138
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->w(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 139
    const-string v0, "preferences_sync_title"

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 140
    iget-object v2, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->o:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 141
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 144
    :cond_2
    const-string v0, "preferences_general_title"

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 146
    invoke-static {}, Lcom/android/calendar/dz;->t()Z

    move-result v1

    if-nez v1, :cond_3

    .line 147
    const-string v1, "preferences_islamic_settings"

    invoke-virtual {p0, v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    .line 148
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 150
    :cond_3
    return-void

    .line 102
    :cond_4
    const v0, 0x7f070011

    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->addPreferencesFromResource(I)V

    goto/16 :goto_0

    .line 129
    :cond_5
    const-string v0, "preferences_weather"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->h:Landroid/preference/CheckBoxPreference;

    .line 130
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->h:Landroid/preference/CheckBoxPreference;

    invoke-static {p1}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private a(Landroid/app/Activity;Landroid/preference/PreferenceScreen;)V
    .locals 2

    .prologue
    .line 153
    const-string v0, "preferences_alerts_vibrate"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->m:Landroid/preference/CheckBoxPreference;

    .line 154
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 155
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    :cond_0
    const-string v0, "preferences_alerts_category"

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 158
    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->m:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 164
    :cond_1
    :goto_0
    return-void

    .line 161
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->m:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;Lcom/android/calendar/timezone/v;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 196
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 198
    if-eqz p2, :cond_0

    .line 199
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 203
    :goto_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v2

    .line 204
    aget-object v3, v1, v5

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/android/calendar/timezone/v;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    .line 205
    aget-object v0, v1, v6

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v2}, Lcom/android/calendar/timezone/v;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    .line 206
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 209
    const-string v0, "0"

    sget-object v2, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    aget-object v0, v1, v5

    .line 214
    :goto_1
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 215
    return-void

    .line 201
    :cond_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 212
    :cond_1
    aget-object v0, v1, v6

    goto :goto_1
.end method

.method private a(Landroid/app/Activity;Z)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 167
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 169
    if-eqz p2, :cond_4

    .line 170
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    .line 171
    :cond_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 172
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 174
    :cond_1
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    .line 182
    :goto_0
    new-instance v4, Lcom/android/calendar/timezone/v;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v4, p1, v0, v6, v7}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 183
    invoke-virtual {v4}, Lcom/android/calendar/timezone/v;->b()[[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->j:[[Ljava/lang/CharSequence;

    .line 184
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    iget-object v5, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->j:[[Ljava/lang/CharSequence;

    aget-object v3, v5, v3

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 185
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->j:[[Ljava/lang/CharSequence;

    aget-object v2, v3, v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 186
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 188
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 190
    :cond_2
    sget-object v2, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 191
    invoke-static {p1, v4, v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Lcom/android/calendar/timezone/v;Z)V

    .line 192
    return-void

    :cond_3
    move v1, v3

    .line 174
    goto :goto_0

    .line 176
    :cond_4
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    if-nez v0, :cond_6

    .line 177
    :cond_5
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 178
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 180
    :cond_6
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    goto :goto_0
.end method

.method private a(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 275
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 276
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 277
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 278
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 279
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 280
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 281
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    invoke-virtual {v0, p1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 283
    invoke-static {}, Lcom/android/calendar/dz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->d:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    .line 285
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->h:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->h:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/preference/AllCalendarPreferences;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->c()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/preference/AllCalendarPreferences;)Lcom/android/calendar/preference/f;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/preference/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f02c8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 458
    :goto_0
    return-void

    .line 438
    :cond_0
    new-instance v0, Lcom/android/calendar/preference/f;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/preference/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    .line 439
    new-instance v0, Lcom/android/calendar/preference/a;

    invoke-direct {v0, p0}, Lcom/android/calendar/preference/a;-><init>(Lcom/android/calendar/preference/AllCalendarPreferences;)V

    .line 457
    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    invoke-virtual {v1, v0}, Lcom/android/calendar/preference/f;->a(Lcom/android/calendar/preference/h;)V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 461
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 462
    const v1, 0x7f0f045b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f045a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f02e3

    new-instance v3, Lcom/android/calendar/preference/c;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/c;-><init>(Lcom/android/calendar/preference/AllCalendarPreferences;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/preference/b;

    invoke-direct {v3, p0}, Lcom/android/calendar/preference/b;-><init>(Lcom/android/calendar/preference/AllCalendarPreferences;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 500
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 501
    return-void
.end method


# virtual methods
.method public a(ZZ)V
    .locals 2

    .prologue
    .line 243
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->d:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    const-string v0, ""

    .line 248
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 249
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0313

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 253
    :goto_1
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->d:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 251
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0312

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    .line 91
    const-string v1, "com.android.calendar_preferences"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 297
    iget-boolean v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    invoke-virtual {v0}, Lcom/android/calendar/preference/f;->g()V

    .line 300
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 301
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 326
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 327
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->e:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 380
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->f:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 333
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v2, v0, p2}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 335
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 336
    new-instance v3, Landroid/content/Intent;

    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 337
    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "vnd.android.data/update"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    invoke-virtual {v2, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 339
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 340
    goto :goto_1

    .line 341
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->i:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 342
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 343
    new-instance v3, Landroid/content/Intent;

    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 344
    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "vnd.android.data/update"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    invoke-virtual {v2, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 346
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 347
    goto/16 :goto_1

    .line 348
    :cond_3
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 349
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 350
    sget-object v0, Lcom/android/calendar/hj;->m:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 353
    :cond_4
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 354
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 358
    :goto_2
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 359
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Z)V

    move v0, v1

    .line 360
    goto/16 :goto_1

    .line 356
    :cond_5
    const-string v0, "auto"

    goto :goto_2

    .line 361
    :cond_6
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, p2

    .line 362
    check-cast v0, Ljava/lang/String;

    .line 363
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    sget-object v1, Lcom/android/calendar/preference/AllCalendarPreferences;->c:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 365
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 366
    new-instance v1, Lcom/android/calendar/timezone/v;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v2, v0, v4, v5}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 367
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-static {v2, v1, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;Lcom/android/calendar/timezone/v;Z)V

    .line 368
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 369
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 370
    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->k:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 372
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 373
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 374
    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 375
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 376
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    .line 378
    goto/16 :goto_1
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x20000000

    const/4 v1, 0x1

    .line 385
    iget-boolean v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->q:Lcom/android/calendar/preference/f;

    invoke-virtual {v0}, Lcom/android/calendar/preference/f;->g()V

    .line 389
    :cond_0
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 390
    const-string v2, "perferences_sync"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 391
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/preference/SyncPreferences;->a(Landroid/app/Activity;)V

    move v0, v1

    .line 430
    :goto_0
    return v0

    .line 393
    :cond_1
    const-string v2, "preferences_show_week_num"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 394
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 430
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0

    .line 395
    :cond_3
    const-string v2, "preferences_week_start_day"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 396
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 397
    :cond_4
    const-string v2, "preferences_alerts_ringtone"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 398
    iput-boolean v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->p:Z

    goto :goto_1

    .line 399
    :cond_5
    const-string v2, "preferences_home_tz"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 400
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 401
    sget-object v0, Lcom/android/calendar/preference/AllCalendarPreferences;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 404
    :cond_6
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 405
    const-string v0, "query"

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-class v3, Lcom/android/calendar/timezone/CalendarTimezoneActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 409
    const/16 v0, 0x94

    .line 410
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    .line 411
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 412
    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 413
    aget-object v0, v0, v1

    .line 414
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 416
    :cond_7
    const-string v3, "mVar"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 418
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/16 v3, 0x132

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 419
    goto/16 :goto_0

    .line 420
    :cond_8
    iget-boolean v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->r:Z

    if-eqz v1, :cond_9

    const-string v1, "preferences_china_holiday_update_settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 421
    invoke-direct {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->b()V

    goto/16 :goto_1

    .line 422
    :cond_9
    const-string v1, "preferences_weather"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 423
    sget-boolean v0, Lcom/android/calendar/dz;->a:Z

    if-eqz v0, :cond_2

    .line 424
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/android/calendar/preference/WeatherSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 427
    invoke-virtual {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 219
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 220
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 221
    iget-boolean v1, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->p:Z

    if-nez v1, :cond_1

    .line 222
    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 225
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;)V

    .line 227
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 228
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/app/Activity;)V

    .line 230
    :cond_2
    iput-boolean v2, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->p:Z

    .line 231
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 233
    invoke-direct {p0, p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->a(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 234
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->n:Landroid/preference/RingtonePreference;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->a(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    .line 235
    iget-object v0, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->n:Landroid/preference/RingtonePreference;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/preference/EventNotificationPreference;->b(Landroid/preference/RingtonePreference;Landroid/app/Activity;)V

    .line 237
    new-instance v0, Lcom/android/calendar/h/e/a;

    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/android/calendar/h/e/a;-><init>(Landroid/content/Context;Lcom/android/calendar/h/e/b;)V

    .line 238
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/calendar/h/e/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 239
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/android/calendar/preference/AllCalendarPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 306
    if-nez v0, :cond_0

    .line 321
    :goto_0
    return-void

    .line 310
    :cond_0
    const-string v1, "preferences_alerts_type"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 311
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 312
    const-class v2, Lcom/android/calendar/alerts/AlertReceiver;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 313
    iget-object v2, p0, Lcom/android/calendar/preference/AllCalendarPreferences;->l:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    const-string v2, "removeOldReminders"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 320
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :cond_2
    const-string v2, "android.intent.action.EVENT_REMINDER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method
