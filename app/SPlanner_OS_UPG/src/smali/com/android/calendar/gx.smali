.class public Lcom/android/calendar/gx;
.super Ljava/lang/Object;
.source "StickerUtils.java"


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:[Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    const-class v0, Lcom/android/calendar/gx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gx;->d:Ljava/lang/String;

    .line 46
    const-string v0, "content://com.android.calendar/stickers"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sticker_name"

    aput-object v1, v0, v4

    const-string v1, "filepath"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "sticker_group"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "recently"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "packageId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/gx;->b:[Ljava/lang/String;

    .line 57
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "filepath"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/gx;->e:[Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.Splanner_sticker/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/gx;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 166
    .line 167
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    if-nez p0, :cond_0

    .line 169
    const/4 v0, 0x1

    .line 178
    :goto_0
    return v0

    .line 170
    :cond_0
    const/16 v0, 0xf

    if-ne p0, v0, :cond_1

    .line 171
    const/4 v0, 0x2

    goto :goto_0

    .line 173
    :cond_1
    add-int/lit8 v0, p0, 0x2

    goto :goto_0

    .line 176
    :cond_2
    add-int/lit8 v0, p0, 0x1

    goto :goto_0
.end method

.method public static a(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3

    .prologue
    .line 209
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 210
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 211
    const/4 v0, 0x1

    .line 213
    if-gt v1, p2, :cond_0

    if-le v2, p1, :cond_1

    .line 214
    :cond_0
    if-le v2, v1, :cond_2

    .line 215
    int-to-float v0, v1

    int-to-float v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 220
    :cond_1
    :goto_0
    return v0

    .line 217
    :cond_2
    int-to-float v0, v2

    int-to-float v1, p1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 198
    invoke-static {p0, v0, v0}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 199
    if-nez v1, :cond_0

    .line 200
    const/4 v0, -0x1

    .line 204
    :goto_0
    return v0

    .line 202
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 203
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 96
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-object v3

    .line 100
    :cond_1
    if-lez p2, :cond_2

    if-gtz p1, :cond_6

    :cond_2
    move v0, v2

    .line 103
    :goto_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 107
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 108
    if-eqz v0, :cond_5

    .line 109
    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 110
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 112
    invoke-static {v4, p1, p1}, Lcom/android/calendar/gx;->a(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 118
    :goto_2
    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 119
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move-object v2, v3

    .line 123
    :cond_3
    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 125
    :try_start_0
    invoke-static {p0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 130
    add-int/lit8 v1, v1, 0x1

    .line 133
    :goto_3
    if-nez v2, :cond_4

    const/4 v3, 0x4

    if-lt v1, v3, :cond_3

    :cond_4
    move-object v3, v2

    .line 135
    goto :goto_0

    .line 114
    :cond_5
    iput v1, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_2

    .line 126
    :catch_0
    move-exception v3

    .line 127
    mul-int/lit8 v0, v0, 0x2

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :catchall_0
    move-exception v0

    add-int/lit8 v1, v1, 0x1

    throw v0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public static a()Landroid/net/Uri;
    .locals 6

    .prologue
    .line 144
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/user_created"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 148
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "samsung_created_user_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 149
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/FragmentManager;)Landroid/util/LruCache;
    .locals 4

    .prologue
    .line 224
    invoke-static {p0}, Lcom/android/calendar/gz;->a(Landroid/app/FragmentManager;)Lcom/android/calendar/gz;

    move-result-object v1

    .line 225
    if-nez v1, :cond_1

    .line 226
    const/4 v0, 0x0

    .line 243
    :cond_0
    :goto_0
    return-object v0

    .line 229
    :cond_1
    invoke-virtual {v1}, Lcom/android/calendar/gz;->a()Landroid/util/LruCache;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    long-to-int v0, v2

    .line 233
    div-int/lit8 v2, v0, 0x10

    .line 234
    new-instance v0, Lcom/android/calendar/gy;

    invoke-direct {v0, v2}, Lcom/android/calendar/gy;-><init>(I)V

    .line 241
    invoke-virtual {v1, v0}, Lcom/android/calendar/gz;->a(Landroid/util/LruCache;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 75
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 76
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 79
    sget-object v1, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/gx;->e:[Ljava/lang/String;

    const-string v3, "_id=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 80
    const-string v0, ""

    .line 82
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 83
    const-string v0, "filepath"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 84
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 85
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 88
    :cond_0
    if-eqz v1, :cond_1

    .line 89
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 92
    :cond_1
    return-object v0

    .line 88
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 89
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static a(Landroid/content/ContentResolver;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 269
    const-string v0, "recently ASC"

    .line 270
    const-string v0, "recently!=0"

    .line 272
    new-instance v0, Lcom/android/calendar/ha;

    invoke-direct {v0, p0}, Lcom/android/calendar/ha;-><init>(Landroid/content/ContentResolver;)V

    .line 273
    const/4 v1, 0x0

    sget-object v3, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    const-string v5, "recently!=0"

    const-string v7, "recently ASC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ha;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 153
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 154
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 155
    const-string v1, "file_path"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v1, "packageId"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 158
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 162
    :cond_0
    return-void
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 182
    .line 183
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 194
    :goto_0
    return v0

    .line 186
    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    .line 187
    const/16 v0, 0xf

    goto :goto_0

    .line 189
    :cond_1
    add-int/lit8 v0, p0, -0x2

    goto :goto_0

    .line 192
    :cond_2
    add-int/lit8 v0, p0, -0x1

    goto :goto_0
.end method

.method public static b(Landroid/app/FragmentManager;)Landroid/util/SparseIntArray;
    .locals 3

    .prologue
    .line 247
    invoke-static {p0}, Lcom/android/calendar/gz;->a(Landroid/app/FragmentManager;)Lcom/android/calendar/gz;

    move-result-object v1

    .line 248
    if-nez v1, :cond_1

    .line 249
    const/4 v0, 0x0

    .line 256
    :cond_0
    :goto_0
    return-object v0

    .line 251
    :cond_1
    invoke-virtual {v1}, Lcom/android/calendar/gz;->c()Landroid/util/SparseIntArray;

    move-result-object v0

    .line 252
    if-nez v0, :cond_0

    .line 253
    new-instance v0, Landroid/util/SparseIntArray;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 254
    invoke-virtual {v1, v0}, Lcom/android/calendar/gz;->a(Landroid/util/SparseIntArray;)V

    goto :goto_0
.end method
