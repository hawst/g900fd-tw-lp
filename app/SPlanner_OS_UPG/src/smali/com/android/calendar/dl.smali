.class Lcom/android/calendar/dl;
.super Ljava/lang/Object;
.source "EventLoader.java"

# interfaces
.implements Lcom/android/calendar/dm;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/util/ArrayList;

.field public e:Ljava/lang/Runnable;

.field public f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(IIILjava/util/ArrayList;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput p1, p0, Lcom/android/calendar/dl;->a:I

    .line 135
    iput p2, p0, Lcom/android/calendar/dl;->b:I

    .line 136
    iput p3, p0, Lcom/android/calendar/dl;->c:I

    .line 137
    iput-object p4, p0, Lcom/android/calendar/dl;->d:Ljava/util/ArrayList;

    .line 138
    iput-object p5, p0, Lcom/android/calendar/dl;->e:Ljava/lang/Runnable;

    .line 139
    iput-object p6, p0, Lcom/android/calendar/dl;->f:Ljava/lang/Runnable;

    .line 140
    return-void
.end method


# virtual methods
.method public a(Lcom/android/calendar/dj;)V
    .locals 6

    .prologue
    .line 143
    invoke-static {p1}, Lcom/android/calendar/dj;->b(Lcom/android/calendar/dj;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/dl;->d:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/dl;->b:I

    iget v3, p0, Lcom/android/calendar/dl;->c:I

    iget v4, p0, Lcom/android/calendar/dl;->a:I

    invoke-static {p1}, Lcom/android/calendar/dj;->c(Lcom/android/calendar/dj;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Landroid/content/Context;Ljava/util/ArrayList;IIILjava/util/concurrent/atomic/AtomicInteger;)V

    .line 147
    iget v0, p0, Lcom/android/calendar/dl;->a:I

    invoke-static {p1}, Lcom/android/calendar/dj;->c(Lcom/android/calendar/dj;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 148
    invoke-static {p1}, Lcom/android/calendar/dj;->a(Lcom/android/calendar/dj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/dl;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-static {p1}, Lcom/android/calendar/dj;->a(Lcom/android/calendar/dj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/dl;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public b(Lcom/android/calendar/dj;)V
    .locals 2

    .prologue
    .line 155
    invoke-static {p1}, Lcom/android/calendar/dj;->a(Lcom/android/calendar/dj;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/dl;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return-void
.end method
