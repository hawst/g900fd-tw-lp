.class public Lcom/android/calendar/a/a/c;
.super Lcom/android/calendar/a/a/a;
.source "DownloadStickerReq.java"


# direct methods
.method public constructor <init>(Lcom/android/calendar/a/a/b;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/android/calendar/a/a/a;-><init>(Lcom/android/calendar/a/a/b;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 31
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 33
    const-string v2, "stickeritem"

    invoke-static {v1, v2}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 34
    if-eqz v1, :cond_0

    .line 35
    const-string v2, "stickerurl"

    invoke-static {v1, v2}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/a/a/c;->a(Ljava/lang/Object;)V

    .line 42
    const/4 v0, 0x0

    return v0

    .line 37
    :catch_0
    move-exception v1

    .line 38
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/a/a/b;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x96

    .line 16
    sget-object v0, Lcom/android/calendar/a/a/f;->a:Lcom/android/calendar/a/a/f;

    invoke-static {v0}, Lcom/android/calendar/a/a/d;->a(Lcom/android/calendar/a/a/f;)Ljava/lang/String;

    move-result-object v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    const-string v0, ""

    .line 22
    :goto_0
    return-object v0

    .line 20
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/snote/sticker/download?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/android/calendar/a/a/b;)I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public c(Lcom/android/calendar/a/a/b;)I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method
