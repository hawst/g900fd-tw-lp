.class Lcom/android/calendar/a/b;
.super Ljava/lang/Object;
.source "UiDownloadListHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Lcom/android/calendar/a/c;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/android/calendar/a/c;)V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/android/calendar/a/b;->a:Landroid/graphics/Bitmap;

    .line 134
    iput-object p2, p0, Lcom/android/calendar/a/b;->b:Lcom/android/calendar/a/c;

    .line 135
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/android/calendar/a/b;->b:Lcom/android/calendar/a/c;

    invoke-static {v0}, Lcom/android/calendar/a/a;->a(Lcom/android/calendar/a/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/a/b;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/a/b;->b:Lcom/android/calendar/a/c;

    iget-object v0, v0, Lcom/android/calendar/a/c;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/calendar/a/b;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 142
    iget-object v0, p0, Lcom/android/calendar/a/b;->b:Lcom/android/calendar/a/c;

    iget-object v0, v0, Lcom/android/calendar/a/c;->c:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/android/calendar/a/b;->b:Lcom/android/calendar/a/c;

    iget-object v0, v0, Lcom/android/calendar/a/c;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
