.class public Lcom/android/calendar/a/a/a/e;
.super Ljava/lang/Thread;
.source "DownloadManager.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/a/a/a/b;

.field private b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/android/calendar/a/a/a/b;)V
    .locals 1

    .prologue
    .line 188
    iput-object p1, p0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/e;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/a/a/a/e;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/calendar/a/a/a/e;->b:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/android/calendar/a/a/a/e;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V
    .locals 3

    .prologue
    .line 253
    if-eqz p1, :cond_0

    .line 255
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/b;->a()Landroid/os/Handler;

    move-result-object v0

    .line 256
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 257
    iput-object p3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 258
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 259
    iput p2, v1, Landroid/os/Message;->arg1:I

    .line 260
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 262
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/a/a/a/g;Lcom/android/calendar/a/a/a/a;)V
    .locals 3

    .prologue
    .line 241
    if-eqz p1, :cond_0

    .line 243
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/b;->a()Landroid/os/Handler;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 245
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 246
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    .line 247
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 249
    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 195
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "DownloadThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 197
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 198
    new-instance v0, Lcom/android/calendar/a/a/a/f;

    invoke-direct {v0, p0}, Lcom/android/calendar/a/a/a/f;-><init>(Lcom/android/calendar/a/a/a/e;)V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/e;->b:Landroid/os/Handler;

    .line 230
    iget-object v0, p0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/b;->d(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->a()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 231
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 232
    return-void
.end method
