.class public Lcom/android/calendar/a/a/a/a;
.super Ljava/lang/Object;
.source "BaseReq.java"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field protected c:Lcom/android/calendar/a/a/a/j;

.field protected d:Ljava/lang/Object;

.field protected e:Z

.field private f:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->c:Lcom/android/calendar/a/a/a/j;

    .line 12
    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->d:Ljava/lang/Object;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->a:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->b:Ljava/lang/String;

    .line 17
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/android/calendar/a/a/a/a;->f:I

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a/a;->e:Z

    .line 25
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->c:Lcom/android/calendar/a/a/a/j;

    .line 12
    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->d:Ljava/lang/Object;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->a:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->b:Ljava/lang/String;

    .line 17
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/android/calendar/a/a/a/a;->f:I

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a/a;->e:Z

    .line 30
    if-nez p1, :cond_0

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->a:Ljava/lang/String;

    .line 35
    :goto_0
    return-void

    .line 34
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/a;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/android/calendar/a/a/a/j;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/android/calendar/a/a/a/a;->c:Lcom/android/calendar/a/a/a/j;

    .line 59
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/a/a/a/a;->d:Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/calendar/a/a/a/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/android/calendar/a/a/a/a;->f:I

    return v0
.end method

.method public i()Lcom/android/calendar/a/a/a/j;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/calendar/a/a/a/a;->c:Lcom/android/calendar/a/a/a/j;

    return-object v0
.end method

.method public j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/calendar/a/a/a/a;->d:Ljava/lang/Object;

    return-object v0
.end method
