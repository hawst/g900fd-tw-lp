.class Lcom/android/calendar/a/a/a/f;
.super Landroid/os/Handler;
.source "DownloadManager.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/a/a/a/e;


# direct methods
.method constructor <init>(Lcom/android/calendar/a/a/a/e;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 200
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v0, v0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/b;->b(Lcom/android/calendar/a/a/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v0, v0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/b;->b(Lcom/android/calendar/a/a/a/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a;

    .line 204
    iget-object v1, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v1, v1, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v1, v0}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a/b;Lcom/android/calendar/a/a/a;)Lcom/android/calendar/a/a/a;

    .line 205
    iget-object v1, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v1, v1, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v1}, Lcom/android/calendar/a/a/a/b;->b(Lcom/android/calendar/a/a/a/b;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 207
    if-eqz v0, :cond_1

    .line 208
    iget-object v1, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0, v1}, Lcom/android/calendar/a/a/a;->a(Lcom/android/calendar/a/a/a/e;)I

    move-result v0

    .line 210
    if-eqz v0, :cond_1

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/e;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 222
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v0, v0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/a/a/a/b;->a(Lcom/android/calendar/a/a/a/b;Lcom/android/calendar/a/a/a;)Lcom/android/calendar/a/a/a;

    .line 223
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v0, v0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/b;->c(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/android/calendar/a/a/a/f;->a:Lcom/android/calendar/a/a/a/e;

    iget-object v0, v0, Lcom/android/calendar/a/a/a/e;->a:Lcom/android/calendar/a/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/a/b;->c(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/calendar/a/a/a/d;->a()V

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
