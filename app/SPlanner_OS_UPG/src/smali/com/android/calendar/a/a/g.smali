.class public Lcom/android/calendar/a/a/g;
.super Lcom/android/calendar/a/a/a/a;
.source "ListStickerReq.java"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 29
    sget-object v0, Lcom/android/calendar/a/a/f;->a:Lcom/android/calendar/a/a/f;

    invoke-static {v0}, Lcom/android/calendar/a/a/d;->a(Lcom/android/calendar/a/a/f;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/snote/sticker/list?version=0&width=150&height=150&cid=1"

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/a/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    iput v2, p0, Lcom/android/calendar/a/a/g;->a:I

    .line 33
    iput v2, p0, Lcom/android/calendar/a/a/g;->a:I

    .line 34
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/android/calendar/a/a/f;->a:Lcom/android/calendar/a/a/f;

    invoke-static {v0}, Lcom/android/calendar/a/a/d;->a(Lcom/android/calendar/a/a/f;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/snote/sticker/list?version=0&width=150&height=150&cid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/a/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/a/a/g;->a:I

    .line 41
    iput p1, p0, Lcom/android/calendar/a/a/g;->a:I

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 47
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 53
    const-string v2, "stickeritem"

    invoke-static {v0, v2}, Lcom/android/calendar/a/a/d;->d(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 54
    if-eqz v4, :cond_2

    .line 55
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    move v2, v1

    .line 56
    :goto_0
    if-ge v2, v5, :cond_2

    .line 57
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 59
    new-instance v6, Lcom/android/calendar/a/a/h;

    invoke-direct {v6}, Lcom/android/calendar/a/a/h;-><init>()V

    .line 61
    const-string v7, "id"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/calendar/a/a/h;->a:Ljava/lang/String;

    .line 62
    const-string v7, "thumbnailurl"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/calendar/a/a/h;->b:Ljava/lang/String;

    .line 63
    const-string v7, "filesize"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->c(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v6, Lcom/android/calendar/a/a/h;->d:J

    .line 64
    const-string v7, "special"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/calendar/a/a/h;->e:Ljava/lang/String;

    .line 66
    const-string v7, "titlelist"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    const-string v7, "title"

    invoke-static {v0, v7}, Lcom/android/calendar/a/a/d;->d(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 70
    if-nez v7, :cond_1

    .line 72
    iget-object v0, v6, Lcom/android/calendar/a/a/h;->f:Ljava/util/HashMap;

    const-string v7, "en"

    const-string v8, "no title"

    invoke-virtual {v0, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_0
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    move v0, v1

    .line 76
    :goto_1
    if-ge v0, v8, :cond_0

    .line 77
    invoke-virtual {v7, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 79
    const-string v10, "lang"

    invoke-static {v9, v10}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 80
    const-string v11, "value"

    invoke-static {v9, v11}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 82
    iget-object v11, v6, Lcom/android/calendar/a/a/h;->f:Ljava/util/HashMap;

    invoke-virtual {v11, v10, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 97
    :cond_2
    :goto_2
    invoke-virtual {p0, v3}, Lcom/android/calendar/a/a/g;->a(Ljava/lang/Object;)V

    .line 98
    return v1

    .line 93
    :catch_1
    move-exception v0

    .line 94
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2
.end method
