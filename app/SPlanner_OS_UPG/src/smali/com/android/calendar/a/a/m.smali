.class public Lcom/android/calendar/a/a/m;
.super Lcom/android/calendar/a/a/a/a;
.source "WeatherReq.java"


# instance fields
.field private a:Lcom/android/calendar/a/a/q;

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLcom/android/calendar/a/a/o;)V
    .locals 4

    .prologue
    .line 106
    sget-object v0, Lcom/android/calendar/a/a/f;->e:Lcom/android/calendar/a/a/f;

    invoke-static {v0}, Lcom/android/calendar/a/a/d;->a(Lcom/android/calendar/a/a/f;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/forecasts/v1/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".json?apikey="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "0460650BB2524F84BAECAA9381D79EFC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "&language="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&metric="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/a/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/a/a/m;->b:Z

    .line 110
    iget-object v0, p4, Lcom/android/calendar/a/a/o;->l:Lcom/android/calendar/a/a/p;

    sget-object v1, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/android/calendar/a/a/q;->b:Lcom/android/calendar/a/a/q;

    :goto_1
    iput-object v0, p0, Lcom/android/calendar/a/a/m;->a:Lcom/android/calendar/a/a/q;

    .line 111
    iput-boolean p3, p0, Lcom/android/calendar/a/a/m;->b:Z

    .line 112
    return-void

    .line 106
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 110
    :cond_1
    sget-object v0, Lcom/android/calendar/a/a/q;->c:Lcom/android/calendar/a/a/q;

    goto :goto_1
.end method

.method public static a(Landroid/text/format/Time;Z)Lcom/android/calendar/a/a/o;
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 231
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 232
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 233
    invoke-static {}, Lcom/android/calendar/hj;->d()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 234
    invoke-virtual {p0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 237
    invoke-virtual {p0, v1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, v2, Landroid/text/format/Time;->year:I

    const/16 v1, 0x7b2

    if-gt v0, v1, :cond_0

    iget v0, v2, Landroid/text/format/Time;->month:I

    if-gtz v0, :cond_0

    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x19

    if-lt v0, v1, :cond_1

    .line 238
    :cond_0
    const/4 v0, 0x0

    .line 266
    :goto_0
    return-object v0

    .line 241
    :cond_1
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 242
    sget-object v0, Lcom/android/calendar/a/a/o;->k:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 245
    :cond_2
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 246
    sget-object v0, Lcom/android/calendar/a/a/o;->j:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 249
    :cond_3
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x6

    if-lt v0, v1, :cond_5

    .line 250
    if-eqz p1, :cond_4

    sget-object v0, Lcom/android/calendar/a/a/o;->f:Lcom/android/calendar/a/a/o;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/calendar/a/a/o;->i:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 253
    :cond_5
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x3

    if-lt v0, v1, :cond_7

    .line 254
    if-eqz p1, :cond_6

    sget-object v0, Lcom/android/calendar/a/a/o;->e:Lcom/android/calendar/a/a/o;

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/android/calendar/a/a/o;->h:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 257
    :cond_7
    iget v0, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    if-lt v0, v3, :cond_9

    .line 258
    if-eqz p1, :cond_8

    sget-object v0, Lcom/android/calendar/a/a/o;->d:Lcom/android/calendar/a/a/o;

    goto :goto_0

    :cond_8
    sget-object v0, Lcom/android/calendar/a/a/o;->h:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 261
    :cond_9
    if-eqz p1, :cond_c

    .line 262
    iget v0, v2, Landroid/text/format/Time;->hour:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_a

    sget-object v0, Lcom/android/calendar/a/a/o;->c:Lcom/android/calendar/a/a/o;

    goto :goto_0

    :cond_a
    iget v0, v2, Landroid/text/format/Time;->hour:I

    if-lt v0, v3, :cond_b

    sget-object v0, Lcom/android/calendar/a/a/o;->b:Lcom/android/calendar/a/a/o;

    goto :goto_0

    :cond_b
    sget-object v0, Lcom/android/calendar/a/a/o;->a:Lcom/android/calendar/a/a/o;

    goto :goto_0

    .line 266
    :cond_c
    sget-object v0, Lcom/android/calendar/a/a/o;->g:Lcom/android/calendar/a/a/o;

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;J)Lcom/android/calendar/a/a/r;
    .locals 9

    .prologue
    .line 277
    const-wide v4, 0x7fffffffffffffffL

    .line 278
    const/4 v1, 0x0

    .line 280
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/r;

    .line 281
    iget-wide v2, v0, Lcom/android/calendar/a/a/r;->a:J

    sub-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 283
    cmp-long v7, v2, v4

    if-gez v7, :cond_1

    :goto_1
    move-object v1, v0

    move-wide v4, v2

    .line 287
    goto :goto_0

    .line 289
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/android/calendar/a/a/m;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "C"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "F"

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 116
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 119
    new-instance v5, Lcom/android/calendar/a/a/r;

    invoke-direct {v5}, Lcom/android/calendar/a/a/r;-><init>()V

    .line 122
    :try_start_0
    sget-object v0, Lcom/android/calendar/a/a/n;->a:[I

    iget-object v2, p0, Lcom/android/calendar/a/a/m;->a:Lcom/android/calendar/a/a/q;

    invoke-virtual {v2}, Lcom/android/calendar/a/a/q;->ordinal()I

    move-result v2

    aget v0, v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    packed-switch v0, :pswitch_data_0

    .line 218
    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/calendar/a/a/m;->a(Ljava/lang/Object;)V

    .line 219
    return v1

    .line 125
    :pswitch_0
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 126
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 127
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 129
    const-string v6, "EpochTime"

    invoke-static {v2, v6}, Lcom/android/calendar/a/a/d;->c(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/calendar/a/a/r;->a:J

    .line 130
    const-string v6, "WeatherText"

    invoke-static {v2, v6}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    .line 131
    const-string v6, "WeatherIcon"

    invoke-static {v2, v6}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/android/calendar/a/a/r;->c:I

    .line 133
    const-string v6, "Temperature"

    invoke-static {v2, v6}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 135
    if-eqz v6, :cond_1

    .line 136
    iget-boolean v2, p0, Lcom/android/calendar/a/a/m;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "Metric"

    :goto_2
    invoke-static {v6, v2}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 138
    if-eqz v2, :cond_1

    .line 139
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Value"

    invoke-static {v2, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\u00b0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Unit"

    invoke-static {v2, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lcom/android/calendar/a/a/r;->d:Ljava/lang/String;

    .line 144
    :cond_1
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 136
    :cond_2
    const-string v2, "Imperial"

    goto :goto_2

    .line 149
    :pswitch_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 150
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 151
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 152
    const-string v6, "EpochDateTime"

    invoke-static {v3, v6}, Lcom/android/calendar/a/a/d;->c(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/android/calendar/a/a/r;->a:J

    .line 153
    const-string v6, "IconPhrase"

    invoke-static {v3, v6}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    .line 154
    const-string v6, "WeatherIcon"

    invoke-static {v3, v6}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Lcom/android/calendar/a/a/r;->c:I

    .line 156
    const-string v6, "Temperature"

    invoke-static {v3, v6}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 157
    if-eqz v3, :cond_3

    .line 158
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Value"

    invoke-static {v3, v7}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\u00b0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Unit"

    invoke-static {v3, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lcom/android/calendar/a/a/r;->d:Ljava/lang/String;

    .line 162
    :cond_3
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 167
    :pswitch_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "DailyForecasts"

    invoke-static {v0, v2}, Lcom/android/calendar/a/a/d;->d(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 168
    if-eqz v6, :cond_0

    move v0, v1

    .line 173
    :goto_4
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 174
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 175
    if-nez v2, :cond_4

    .line 173
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 179
    :cond_4
    const-string v3, "EpochDate"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->c(Lorg/json/JSONObject;Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, v5, Lcom/android/calendar/a/a/r;->a:J

    .line 181
    const-string v3, "Day"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 182
    if-eqz v3, :cond_5

    .line 183
    const-string v7, "IconPhrase"

    invoke-static {v3, v7}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v5, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    .line 185
    :cond_5
    const-string v3, "Day"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_6

    .line 187
    const-string v7, "Icon"

    invoke-static {v3, v7}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v3

    iput v3, v5, Lcom/android/calendar/a/a/r;->c:I

    .line 190
    :cond_6
    const-string v3, "Temperature"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 191
    if-eqz v2, :cond_b

    .line 192
    const-string v3, "Minimum"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 193
    const-string v7, "Maximum"

    invoke-static {v2, v7}, Lcom/android/calendar/a/a/d;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 194
    if-eqz v3, :cond_b

    if-eqz v7, :cond_b

    .line 195
    const-string v8, "Value"

    invoke-static {v3, v8}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v8

    .line 196
    const-string v3, "Unit"

    invoke-static {v2, v3}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 197
    const-string v9, "Value"

    invoke-static {v7, v9}, Lcom/android/calendar/a/a/d;->b(Lorg/json/JSONObject;Ljava/lang/String;)I

    move-result v7

    .line 198
    const-string v9, "Unit"

    invoke-static {v2, v9}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_8

    :cond_7
    invoke-direct {p0}, Lcom/android/calendar/a/a/m;->a()Ljava/lang/String;

    move-result-object v3

    .line 201
    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_a

    :cond_9
    invoke-direct {p0}, Lcom/android/calendar/a/a/m;->a()Ljava/lang/String;

    move-result-object v2

    .line 203
    :cond_a
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    const-string v11, "%d"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\u00b0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "  \u2014  "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    const-string v9, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\u00b0"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lcom/android/calendar/a/a/r;->d:Ljava/lang/String;

    .line 208
    :cond_b
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_5

    .line 212
    :catch_0
    move-exception v0

    .line 213
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 214
    :catch_1
    move-exception v0

    .line 215
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
