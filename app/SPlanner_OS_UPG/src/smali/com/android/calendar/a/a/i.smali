.class public Lcom/android/calendar/a/a/i;
.super Lcom/android/calendar/a/a/a/a;
.source "LocationReq.java"


# instance fields
.field private a:Lcom/android/calendar/a/a/l;


# direct methods
.method public constructor <init>(DD)V
    .locals 3

    .prologue
    .line 34
    sget-object v0, Lcom/android/calendar/a/a/f;->d:Lcom/android/calendar/a/a/f;

    invoke-static {v0}, Lcom/android/calendar/a/a/d;->a(Lcom/android/calendar/a/a/f;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/locations/v1/cities/geoposition/search.json?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&apikey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "0460650BB2524F84BAECAA9381D79EFC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/a/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/android/calendar/a/a/l;->a:Lcom/android/calendar/a/a/l;

    iput-object v0, p0, Lcom/android/calendar/a/a/i;->a:Lcom/android/calendar/a/a/l;

    .line 37
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lcom/android/calendar/a/a/k;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/android/calendar/a/a/k;

    invoke-direct {v0}, Lcom/android/calendar/a/a/k;-><init>()V

    .line 80
    const-string v1, "Key"

    invoke-static {p1, v1}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/a/a/k;->a:Ljava/lang/String;

    .line 81
    const-string v1, "LocalizedName"

    invoke-static {p1, v1}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/a/a/k;->b:Ljava/lang/String;

    .line 82
    const-string v1, "EnglishName"

    invoke-static {p1, v1}, Lcom/android/calendar/a/a/d;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/a/a/k;->c:Ljava/lang/String;

    .line 84
    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 52
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 55
    :try_start_0
    sget-object v0, Lcom/android/calendar/a/a/j;->a:[I

    iget-object v3, p0, Lcom/android/calendar/a/a/i;->a:Lcom/android/calendar/a/a/l;

    invoke-virtual {v3}, Lcom/android/calendar/a/a/l;->ordinal()I

    move-result v3

    aget v0, v0, v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    packed-switch v0, :pswitch_data_0

    .line 74
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/calendar/a/a/i;->a(Ljava/lang/Object;)V

    .line 75
    return v1

    .line 57
    :pswitch_0
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/calendar/a/a/i;->a(Lorg/json/JSONObject;)Lcom/android/calendar/a/a/k;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 61
    :pswitch_1
    :try_start_2
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 62
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 63
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 64
    invoke-direct {p0, v4}, Lcom/android/calendar/a/a/i;->a(Lorg/json/JSONObject;)Lcom/android/calendar/a/a/k;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :catch_1
    move-exception v0

    .line 71
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
