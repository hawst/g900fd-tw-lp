.class public Lcom/android/calendar/a/a/a/h;
.super Ljava/lang/Object;
.source "HttpManager.java"


# static fields
.field private static a:Lcom/android/calendar/a/a/a/h;


# instance fields
.field private b:Lcom/android/calendar/a/a/a/k;

.field private c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/a/a/a/h;->a:Lcom/android/calendar/a/a/a/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    .line 35
    new-instance v0, Lcom/android/calendar/a/a/a/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/a/a/a/i;-><init>(Lcom/android/calendar/a/a/a/h;)V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/h;->c:Landroid/os/Handler;

    .line 59
    return-void
.end method

.method public static declared-synchronized b()Lcom/android/calendar/a/a/a/h;
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/android/calendar/a/a/a/h;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/a/a/a/h;->a:Lcom/android/calendar/a/a/a/h;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/android/calendar/a/a/a/h;

    invoke-direct {v0}, Lcom/android/calendar/a/a/a/h;-><init>()V

    sput-object v0, Lcom/android/calendar/a/a/a/h;->a:Lcom/android/calendar/a/a/a/h;

    .line 66
    :cond_0
    sget-object v0, Lcom/android/calendar/a/a/a/h;->a:Lcom/android/calendar/a/a/a/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/a/a/a/h;->c:Landroid/os/Handler;

    return-object v0
.end method

.method public a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p1, p2}, Lcom/android/calendar/a/a/a/a;->a(Lcom/android/calendar/a/a/a/j;)V

    .line 73
    new-instance v0, Lcom/android/calendar/a/a/a/k;

    invoke-direct {v0, p1}, Lcom/android/calendar/a/a/a/k;-><init>(Lcom/android/calendar/a/a/a/a;)V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    .line 74
    iget-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/k;->start()V

    .line 75
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/k;->a()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/h;->b:Lcom/android/calendar/a/a/a/k;

    .line 83
    :cond_0
    return-void
.end method
