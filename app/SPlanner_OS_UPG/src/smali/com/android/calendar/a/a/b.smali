.class public Lcom/android/calendar/a/a/b;
.super Ljava/lang/Object;
.source "DownloadReq.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Object;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide v2, p0, Lcom/android/calendar/a/a/b;->a:J

    .line 32
    iput-object v0, p0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    .line 33
    iput v1, p0, Lcom/android/calendar/a/a/b;->c:I

    .line 34
    iput-wide v2, p0, Lcom/android/calendar/a/a/b;->d:J

    .line 35
    iput-wide v2, p0, Lcom/android/calendar/a/a/b;->e:J

    .line 36
    iput-object v0, p0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/android/calendar/a/a/b;->g:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/android/calendar/a/a/b;->h:Ljava/lang/Object;

    .line 39
    iput v1, p0, Lcom/android/calendar/a/a/b;->i:I

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/a/a/b;I)I
    .locals 0

    .prologue
    .line 30
    iput p1, p0, Lcom/android/calendar/a/a/b;->i:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/a/a/b;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/android/calendar/a/a/b;->i:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/android/calendar/a/a/b;->i:I

    return v0
.end method

.method public a(Lcom/android/calendar/a/a/b;)I
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lcom/android/calendar/a/a/b;->i:I

    iget v1, p1, Lcom/android/calendar/a/a/b;->i:I

    if-ge v0, v1, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    .line 58
    :cond_0
    iget v0, p0, Lcom/android/calendar/a/a/b;->i:I

    iget v1, p1, Lcom/android/calendar/a/a/b;->i:I

    if-le v0, v1, :cond_1

    .line 59
    const/4 v0, -0x1

    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/android/calendar/a/a/b;->i:I

    .line 47
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/android/calendar/a/a/b;

    invoke-virtual {p0, p1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;)I

    move-result v0

    return v0
.end method
