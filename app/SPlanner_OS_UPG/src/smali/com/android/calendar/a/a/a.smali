.class public abstract Lcom/android/calendar/a/a/a;
.super Lcom/android/calendar/a/a/a/a;
.source "DownloadReq.java"


# instance fields
.field public a:J

.field public b:J

.field private f:Lcom/android/calendar/a/a/b;

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/a/a/b;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 80
    invoke-direct {p0}, Lcom/android/calendar/a/a/a/a;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    .line 75
    iput-wide v2, p0, Lcom/android/calendar/a/a/a;->a:J

    .line 76
    iput-wide v2, p0, Lcom/android/calendar/a/a/a;->b:J

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a;->g:Z

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a;->e:Z

    .line 82
    iput-object p1, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    .line 83
    return-void
.end method

.method private a(Lcom/android/calendar/a/a/a/e;Lcom/android/calendar/a/a/b;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 184
    .line 188
    :try_start_0
    new-instance v0, Lcom/android/calendar/a/a/a/l;

    invoke-direct {v0}, Lcom/android/calendar/a/a/a/l;-><init>()V

    .line 189
    invoke-virtual {p0, p2}, Lcom/android/calendar/a/a/a;->a(Lcom/android/calendar/a/a/b;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "Application/JSON"

    invoke-virtual {p0}, Lcom/android/calendar/a/a/a;->h()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/android/calendar/a/a/a/l;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/net/HttpURLConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 191
    :try_start_1
    iget-boolean v0, p0, Lcom/android/calendar/a/a/a;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    .line 220
    if-eqz v2, :cond_0

    .line 221
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    move-object v0, v1

    .line 222
    :cond_1
    :goto_0
    return-object v0

    .line 195
    :cond_2
    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    .line 196
    const/16 v3, 0xc8

    if-eq v0, v3, :cond_4

    .line 220
    if-eqz v2, :cond_3

    .line 221
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    move-object v0, v1

    .line 222
    goto :goto_0

    .line 200
    :cond_4
    :try_start_3
    iget-boolean v0, p0, Lcom/android/calendar/a/a/a;->g:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_6

    .line 220
    if-eqz v2, :cond_5

    .line 221
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    move-object v0, v1

    .line 222
    goto :goto_0

    .line 204
    :cond_6
    :try_start_4
    invoke-static {v2}, Lcom/android/calendar/a/a/a/l;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    .line 207
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 208
    iget-boolean v2, p0, Lcom/android/calendar/a/a/a;->g:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_8

    .line 220
    if-eqz v1, :cond_7

    .line 221
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_7
    move-object v0, v1

    .line 222
    goto :goto_0

    .line 212
    :cond_8
    :try_start_6
    invoke-virtual {p0, v0}, Lcom/android/calendar/a/a/a;->a(Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    .line 213
    if-eqz v0, :cond_a

    .line 220
    if-eqz v1, :cond_9

    .line 221
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    move-object v0, v1

    .line 222
    goto :goto_0

    .line 217
    :cond_a
    :try_start_7
    invoke-virtual {p0}, Lcom/android/calendar/a/a/a;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 220
    if-eqz v1, :cond_1

    .line 221
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_b

    .line 221
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 222
    :cond_b
    throw v0

    .line 220
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/android/calendar/a/a/a/e;)I
    .locals 10

    .prologue
    const/4 v4, -0x5

    const/16 v1, -0x9

    const/4 v0, -0x1

    const/4 v2, -0x8

    const/4 v3, 0x0

    .line 112
    iget-object v5, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    if-nez v5, :cond_0

    .line 180
    :goto_0
    return v0

    .line 116
    :cond_0
    invoke-static {}, Lcom/android/calendar/a/a/a/b;->b()Lcom/android/calendar/a/a/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/calendar/a/a/a/b;->c()Lcom/android/calendar/a/a/a/g;

    move-result-object v5

    .line 117
    iget-object v6, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    .line 118
    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 119
    const-wide/16 v8, 0x0

    iput-wide v8, v6, Lcom/android/calendar/a/a/b;->e:J

    .line 120
    invoke-virtual {p1, v5, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;Lcom/android/calendar/a/a/a/a;)V

    .line 122
    const-wide/32 v8, 0xa00000

    invoke-static {v8, v9}, Lcom/android/calendar/a/a/d;->a(J)Z

    move-result v7

    if-nez v7, :cond_1

    .line 123
    invoke-static {v6, v3}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 124
    invoke-virtual {p1, v5, v1, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    move v0, v1

    .line 125
    goto :goto_0

    .line 129
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v7, "/data/data/com.android.calendar/download/temp/"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 131
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 134
    :cond_2
    iget-boolean v1, p0, Lcom/android/calendar/a/a/a;->g:Z

    if-eqz v1, :cond_3

    .line 135
    const/4 v0, 0x0

    invoke-static {v6, v0}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 136
    const/4 v0, -0x8

    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    move v0, v2

    .line 137
    goto :goto_0

    .line 140
    :cond_3
    invoke-direct {p0, p1, v6}, Lcom/android/calendar/a/a/a;->a(Lcom/android/calendar/a/a/a/e;Lcom/android/calendar/a/a/b;)Ljava/lang/String;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_6

    .line 142
    :cond_4
    iget-boolean v1, p0, Lcom/android/calendar/a/a/a;->g:Z

    if-eqz v1, :cond_5

    .line 143
    const/4 v0, -0x8

    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    move v0, v2

    .line 144
    goto :goto_0

    .line 146
    :cond_5
    const/4 v1, 0x0

    invoke-static {v6, v1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 147
    const/4 v1, -0x1

    invoke-virtual {p1, v5, v1, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 178
    invoke-static {v6, v3}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 179
    invoke-virtual {p1, v5, v4, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    move v0, v4

    .line 180
    goto :goto_0

    .line 151
    :cond_6
    :try_start_1
    invoke-virtual {p0, v5, v1, v6, p1}, Lcom/android/calendar/a/a/a;->a(Lcom/android/calendar/a/a/a/g;Ljava/lang/String;Lcom/android/calendar/a/a/b;Lcom/android/calendar/a/a/a/e;)I

    move-result v0

    .line 152
    if-gez v0, :cond_7

    .line 153
    const/4 v1, 0x0

    invoke-static {v6, v1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 154
    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    goto :goto_0

    .line 158
    :cond_7
    invoke-virtual {p0, v6}, Lcom/android/calendar/a/a/a;->b(Lcom/android/calendar/a/a/b;)I

    move-result v0

    .line 159
    if-gez v0, :cond_8

    .line 160
    const/4 v1, 0x0

    invoke-static {v6, v1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 161
    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    goto/16 :goto_0

    .line 165
    :cond_8
    invoke-virtual {p0, v6}, Lcom/android/calendar/a/a/a;->c(Lcom/android/calendar/a/a/b;)I

    move-result v0

    .line 166
    if-gez v0, :cond_9

    .line 167
    const/4 v1, 0x0

    invoke-static {v6, v1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 168
    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V

    goto/16 :goto_0

    .line 172
    :cond_9
    const/4 v0, 0x2

    invoke-static {v6, v0}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 173
    const/4 v0, 0x0

    invoke-virtual {p1, v5, v0, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;ILcom/android/calendar/a/a/a/a;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v3

    .line 174
    goto/16 :goto_0
.end method

.method public a(Lcom/android/calendar/a/a/a/g;Ljava/lang/String;Lcom/android/calendar/a/a/b;Lcom/android/calendar/a/a/a/e;)I
    .locals 15

    .prologue
    .line 298
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    if-nez p2, :cond_1

    .line 299
    :cond_0
    const/4 v2, -0x1

    .line 393
    :goto_0
    return v2

    .line 302
    :cond_1
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/android/calendar/gx;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 304
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 305
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_2

    .line 306
    const/4 v2, -0x1

    goto :goto_0

    .line 310
    :cond_2
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/calendar/a/a/b;->f:Ljava/lang/String;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 311
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 312
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 316
    :cond_3
    const/4 v3, 0x0

    .line 317
    const/4 v4, 0x0

    .line 318
    const/4 v5, 0x0

    .line 321
    const-wide/16 v8, 0x0

    :try_start_0
    move-object/from16 v0, p3

    iput-wide v8, v0, Lcom/android/calendar/a/a/b;->e:J

    .line 322
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;Lcom/android/calendar/a/a/a/a;)V

    .line 324
    new-instance v2, Ljava/net/URL;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 326
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    const/16 v3, 0x3a98

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 328
    const/16 v3, 0x3a98

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 329
    const-string v3, "GET"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 331
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 332
    const/16 v6, 0xc8

    if-ne v3, v6, :cond_d

    .line 333
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 335
    :try_start_2
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 337
    const/16 v3, 0x800

    :try_start_3
    new-array v8, v3, [B

    .line 339
    const/4 v5, 0x0

    .line 341
    :goto_1
    const/4 v3, 0x0

    const/16 v9, 0x800

    invoke-virtual {v6, v8, v3, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v3

    if-lez v3, :cond_4

    .line 342
    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9, v3}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 344
    iget-wide v10, p0, Lcom/android/calendar/a/a/a;->a:J

    int-to-long v12, v3

    add-long/2addr v10, v12

    iput-wide v10, p0, Lcom/android/calendar/a/a/a;->a:J

    .line 345
    move-object/from16 v0, p3

    iget-wide v10, v0, Lcom/android/calendar/a/a/b;->d:J

    iput-wide v10, p0, Lcom/android/calendar/a/a/a;->b:J

    .line 347
    iget-wide v10, p0, Lcom/android/calendar/a/a/a;->a:J

    move-object/from16 v0, p3

    iput-wide v10, v0, Lcom/android/calendar/a/a/b;->e:J

    .line 348
    iget-wide v10, p0, Lcom/android/calendar/a/a/a;->a:J

    const-wide/16 v12, 0x64

    mul-long/2addr v10, v12

    iget-wide v12, p0, Lcom/android/calendar/a/a/a;->b:J

    div-long/2addr v10, v12

    long-to-int v3, v10

    .line 351
    add-int/lit8 v9, v5, 0x3

    if-le v3, v9, :cond_1c

    .line 352
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;Lcom/android/calendar/a/a/a/a;)V

    .line 356
    :goto_2
    iget-boolean v5, p0, Lcom/android/calendar/a/a/a;->g:Z

    if-eqz v5, :cond_1b

    .line 361
    :cond_4
    iget-boolean v3, p0, Lcom/android/calendar/a/a/a;->g:Z

    if-eqz v3, :cond_9

    .line 362
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 363
    invoke-virtual {v7}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 365
    :cond_5
    const/4 v3, -0x8

    .line 383
    if-eqz v2, :cond_6

    .line 384
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_6
    if-eqz v4, :cond_7

    .line 387
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_7
    if-eqz v6, :cond_8

    .line 390
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    :cond_8
    move v2, v3

    goto/16 :goto_0

    .line 368
    :cond_9
    :try_start_4
    move-object/from16 v0, p3

    iget-wide v8, v0, Lcom/android/calendar/a/a/b;->d:J

    iput-wide v8, p0, Lcom/android/calendar/a/a/a;->a:J

    .line 369
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/a/a/a/e;->a(Lcom/android/calendar/a/a/a/g;Lcom/android/calendar/a/a/a/a;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 371
    const/4 v3, 0x0

    .line 383
    if-eqz v2, :cond_a

    .line 384
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_a
    if-eqz v4, :cond_b

    .line 387
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_b
    if-eqz v6, :cond_c

    .line 390
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    :cond_c
    move v2, v3

    goto/16 :goto_0

    .line 373
    :cond_d
    const/16 v6, 0x194

    if-ne v3, v6, :cond_11

    .line 374
    const/4 v3, -0x6

    .line 383
    if-eqz v2, :cond_e

    .line 384
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_e
    if-eqz v5, :cond_f

    .line 387
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_f
    if-eqz v4, :cond_10

    .line 390
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    :cond_10
    move v2, v3

    goto/16 :goto_0

    .line 376
    :cond_11
    const/4 v3, -0x6

    .line 383
    if-eqz v2, :cond_12

    .line 384
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_12
    if-eqz v5, :cond_13

    .line 387
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_13
    if-eqz v4, :cond_14

    .line 390
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    :cond_14
    move v2, v3

    goto/16 :goto_0

    .line 380
    :catch_0
    move-exception v2

    move-object v14, v5

    move-object v5, v3

    move-object v3, v14

    .line 381
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 383
    if-eqz v5, :cond_15

    .line 384
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_15
    if-eqz v3, :cond_16

    .line 387
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_16
    if-eqz v4, :cond_17

    .line 390
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 393
    :cond_17
    const/4 v2, -0x1

    goto/16 :goto_0

    .line 383
    :catchall_0
    move-exception v2

    move-object v6, v4

    move-object v4, v5

    :goto_4
    if-eqz v3, :cond_18

    .line 384
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 386
    :cond_18
    if-eqz v4, :cond_19

    .line 387
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    .line 389
    :cond_19
    if-eqz v6, :cond_1a

    .line 390
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    :cond_1a
    throw v2

    .line 383
    :catchall_1
    move-exception v3

    move-object v6, v4

    move-object v4, v5

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    goto :goto_4

    :catchall_2
    move-exception v3

    move-object v4, v5

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    goto :goto_4

    :catchall_3
    move-exception v3

    move-object v14, v3

    move-object v3, v2

    move-object v2, v14

    goto :goto_4

    :catchall_4
    move-exception v2

    move-object v6, v4

    move-object v4, v3

    move-object v3, v5

    goto :goto_4

    .line 380
    :catch_1
    move-exception v3

    move-object v14, v3

    move-object v3, v5

    move-object v5, v2

    move-object v2, v14

    goto :goto_3

    :catch_2
    move-exception v3

    move-object v4, v6

    move-object v14, v5

    move-object v5, v2

    move-object v2, v3

    move-object v3, v14

    goto :goto_3

    :catch_3
    move-exception v3

    move-object v5, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v6

    goto :goto_3

    :cond_1b
    move v5, v3

    goto/16 :goto_1

    :cond_1c
    move v3, v5

    goto/16 :goto_2
.end method

.method public abstract a(Lcom/android/calendar/a/a/b;)Ljava/lang/String;
.end method

.method public a()V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/android/calendar/a/a/b;->e:J

    .line 89
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    invoke-static {v0, p1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 101
    return-void
.end method

.method public abstract b(Lcom/android/calendar/a/a/b;)I
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    iget-object v0, v0, Lcom/android/calendar/a/a/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public abstract c(Lcom/android/calendar/a/a/b;)I
.end method

.method public c()J
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    iget-wide v0, v0, Lcom/android/calendar/a/a/b;->e:J

    return-wide v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    invoke-static {v0}, Lcom/android/calendar/a/a/b;->b(Lcom/android/calendar/a/a/b;)I

    move-result v0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    iget-wide v0, v0, Lcom/android/calendar/a/a/b;->d:J

    return-wide v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a;->g:Z

    .line 398
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/android/calendar/a/a/a;->f:Lcom/android/calendar/a/a/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/a/a/b;->a(Lcom/android/calendar/a/a/b;I)I

    .line 401
    :cond_0
    return-void
.end method
