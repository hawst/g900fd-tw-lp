.class public final enum Lcom/android/calendar/a/a/p;
.super Ljava/lang/Enum;
.source "WeatherReq.java"


# static fields
.field public static final enum a:Lcom/android/calendar/a/a/p;

.field public static final enum b:Lcom/android/calendar/a/a/p;

.field private static final synthetic e:[Lcom/android/calendar/a/a/p;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 32
    new-instance v0, Lcom/android/calendar/a/a/p;

    const-string v1, "HOURLY"

    const-string v2, "hourly"

    const-string v3, "hour"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/android/calendar/a/a/p;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    .line 33
    new-instance v0, Lcom/android/calendar/a/a/p;

    const-string v1, "DAILY"

    const-string v2, "daily"

    const-string v3, "day"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/calendar/a/a/p;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/android/calendar/a/a/p;

    sget-object v1, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/a/a/p;->e:[Lcom/android/calendar/a/a/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/android/calendar/a/a/p;->c:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/android/calendar/a/a/p;->d:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/a/a/p;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/android/calendar/a/a/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/p;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/a/a/p;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/android/calendar/a/a/p;->e:[Lcom/android/calendar/a/a/p;

    invoke-virtual {v0}, [Lcom/android/calendar/a/a/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/a/a/p;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/android/calendar/a/a/p;->c:Ljava/lang/String;

    return-object v0
.end method
