.class final enum Lcom/android/calendar/a/a/q;
.super Ljava/lang/Enum;
.source "WeatherReq.java"


# static fields
.field public static final enum a:Lcom/android/calendar/a/a/q;

.field public static final enum b:Lcom/android/calendar/a/a/q;

.field public static final enum c:Lcom/android/calendar/a/a/q;

.field private static final synthetic d:[Lcom/android/calendar/a/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Lcom/android/calendar/a/a/q;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/a/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/q;->a:Lcom/android/calendar/a/a/q;

    new-instance v0, Lcom/android/calendar/a/a/q;

    const-string v1, "HOURLY"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/a/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/q;->b:Lcom/android/calendar/a/a/q;

    new-instance v0, Lcom/android/calendar/a/a/q;

    const-string v1, "DAILY"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/a/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/q;->c:Lcom/android/calendar/a/a/q;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/calendar/a/a/q;

    sget-object v1, Lcom/android/calendar/a/a/q;->a:Lcom/android/calendar/a/a/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/a/a/q;->b:Lcom/android/calendar/a/a/q;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/a/a/q;->c:Lcom/android/calendar/a/a/q;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/a/a/q;->d:[Lcom/android/calendar/a/a/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/a/a/q;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/android/calendar/a/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/q;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/a/a/q;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/android/calendar/a/a/q;->d:[Lcom/android/calendar/a/a/q;

    invoke-virtual {v0}, [Lcom/android/calendar/a/a/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/a/a/q;

    return-object v0
.end method
