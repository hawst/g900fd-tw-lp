.class public Lcom/android/calendar/a/a/a/b;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# static fields
.field private static c:Lcom/android/calendar/a/a/a/b;


# instance fields
.field private a:Lcom/android/calendar/a/a/a/d;

.field private b:Lcom/android/calendar/a/a/a/e;

.field private d:Ljava/util/ArrayList;

.field private e:Lcom/android/calendar/a/a/a;

.field private f:Lcom/android/calendar/a/a/a/g;

.field private g:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/a/a/a/b;->c:Lcom/android/calendar/a/a/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    .line 35
    iput-object v1, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    .line 36
    iput-object v1, p0, Lcom/android/calendar/a/a/a/b;->f:Lcom/android/calendar/a/a/a/g;

    .line 42
    new-instance v0, Lcom/android/calendar/a/a/a/c;

    invoke-direct {v0, p0}, Lcom/android/calendar/a/a/a/c;-><init>(Lcom/android/calendar/a/a/a/b;)V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/b;->g:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/g;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->f:Lcom/android/calendar/a/a/a/g;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/a/a/a/b;Lcom/android/calendar/a/a/a;)Lcom/android/calendar/a/a/a;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    return-object p1
.end method

.method public static declared-synchronized b()Lcom/android/calendar/a/a/a/b;
    .locals 2

    .prologue
    .line 75
    const-class v1, Lcom/android/calendar/a/a/a/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/calendar/a/a/a/b;->c:Lcom/android/calendar/a/a/a/b;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/android/calendar/a/a/a/b;

    invoke-direct {v0}, Lcom/android/calendar/a/a/a/b;-><init>()V

    sput-object v0, Lcom/android/calendar/a/a/a/b;->c:Lcom/android/calendar/a/a/a/b;

    .line 78
    :cond_0
    sget-object v0, Lcom/android/calendar/a/a/a/b;->c:Lcom/android/calendar/a/a/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/android/calendar/a/a/a/b;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/d;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->a:Lcom/android/calendar/a/a/a/d;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/a/a/a/b;)Lcom/android/calendar/a/a/a/e;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->g:Landroid/os/Handler;

    return-object v0
.end method

.method public a(Lcom/android/calendar/a/a/a/d;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/android/calendar/a/a/a/b;->a:Lcom/android/calendar/a/a/a/d;

    .line 88
    return-void
.end method

.method public a(Lcom/android/calendar/a/a/a/g;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/android/calendar/a/a/a/b;->f:Lcom/android/calendar/a/a/a/g;

    .line 84
    return-void
.end method

.method public a(Lcom/android/calendar/a/a/a;)V
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    monitor-exit v1

    .line 129
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p1}, Lcom/android/calendar/a/a/a;->a()V

    .line 103
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    if-nez v0, :cond_2

    .line 104
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/calendar/a/a/a;->a(I)V

    .line 109
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    if-nez v0, :cond_3

    .line 113
    new-instance v0, Lcom/android/calendar/a/a/a/e;

    invoke-direct {v0, p0}, Lcom/android/calendar/a/a/a/e;-><init>(Lcom/android/calendar/a/a/a/b;)V

    iput-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->start()V

    .line 128
    :cond_1
    :goto_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 107
    :cond_2
    const/4 v0, 0x3

    :try_start_3
    invoke-virtual {p1, v0}, Lcom/android/calendar/a/a/a;->a(I)V

    goto :goto_1

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->a()Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_4

    .line 122
    monitor-exit v1

    goto :goto_0

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->a()Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->b:Lcom/android/calendar/a/a/a/e;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/e;->a()Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 116
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 169
    iget-object v2, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    monitor-enter v2

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->f()V

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    .line 174
    monitor-exit v2

    .line 186
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 178
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 179
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->f()V

    .line 181
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 185
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 178
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public c()Lcom/android/calendar/a/a/a/g;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->f:Lcom/android/calendar/a/a/a/g;

    return-object v0
.end method

.method public d()V
    .locals 4

    .prologue
    .line 154
    iget-object v2, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    monitor-enter v2

    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->f()V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 159
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 160
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a;->f()V

    .line 159
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/a/a/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/a/a/a/b;->e:Lcom/android/calendar/a/a/a;

    .line 164
    monitor-exit v2

    .line 165
    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
