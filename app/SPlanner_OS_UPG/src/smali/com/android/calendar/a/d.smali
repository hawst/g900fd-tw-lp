.class Lcom/android/calendar/a/d;
.super Ljava/lang/Object;
.source "UiDownloadListHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Lcom/android/calendar/a/c;


# direct methods
.method constructor <init>(Lcom/android/calendar/a/c;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    .line 72
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    invoke-static {v0}, Lcom/android/calendar/a/a;->a(Lcom/android/calendar/a/c;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-static {}, Lcom/android/calendar/a/a;->a()Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    iget-object v1, v1, Lcom/android/calendar/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 79
    if-nez v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    iget-object v0, v0, Lcom/android/calendar/a/c;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/a/a;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 81
    invoke-static {}, Lcom/android/calendar/a/a;->a()Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    iget-object v2, v2, Lcom/android/calendar/a/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    invoke-static {v1}, Lcom/android/calendar/a/a;->a(Lcom/android/calendar/a/c;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    new-instance v1, Lcom/android/calendar/a/b;

    iget-object v2, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    invoke-direct {v1, v0, v2}, Lcom/android/calendar/a/b;-><init>(Landroid/graphics/Bitmap;Lcom/android/calendar/a/c;)V

    .line 86
    iget-object v0, p0, Lcom/android/calendar/a/d;->a:Lcom/android/calendar/a/c;

    iget-object v0, v0, Lcom/android/calendar/a/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 87
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
