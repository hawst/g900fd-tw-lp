.class public final enum Lcom/android/calendar/a/a/f;
.super Ljava/lang/Enum;
.source "DownloadUtil.java"


# static fields
.field public static final enum a:Lcom/android/calendar/a/a/f;

.field public static final enum b:Lcom/android/calendar/a/a/f;

.field public static final enum c:Lcom/android/calendar/a/a/f;

.field public static final enum d:Lcom/android/calendar/a/a/f;

.field public static final enum e:Lcom/android/calendar/a/a/f;

.field private static final synthetic f:[Lcom/android/calendar/a/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/android/calendar/a/a/f;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/f;->a:Lcom/android/calendar/a/a/f;

    .line 25
    new-instance v0, Lcom/android/calendar/a/a/f;

    const-string v1, "TEMPLATE"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/f;->b:Lcom/android/calendar/a/a/f;

    .line 26
    new-instance v0, Lcom/android/calendar/a/a/f;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/f;->c:Lcom/android/calendar/a/a/f;

    .line 27
    new-instance v0, Lcom/android/calendar/a/a/f;

    const-string v1, "LOCATION"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/f;->d:Lcom/android/calendar/a/a/f;

    .line 28
    new-instance v0, Lcom/android/calendar/a/a/f;

    const-string v1, "WEATHER"

    invoke-direct {v0, v1, v6}, Lcom/android/calendar/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/a/a/f;->e:Lcom/android/calendar/a/a/f;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/calendar/a/a/f;

    sget-object v1, Lcom/android/calendar/a/a/f;->a:Lcom/android/calendar/a/a/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/a/a/f;->b:Lcom/android/calendar/a/a/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/a/a/f;->c:Lcom/android/calendar/a/a/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/a/a/f;->d:Lcom/android/calendar/a/a/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/calendar/a/a/f;->e:Lcom/android/calendar/a/a/f;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/calendar/a/a/f;->f:[Lcom/android/calendar/a/a/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/a/a/f;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/android/calendar/a/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/f;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/a/a/f;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/android/calendar/a/a/f;->f:[Lcom/android/calendar/a/a/f;

    invoke-virtual {v0}, [Lcom/android/calendar/a/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/a/a/f;

    return-object v0
.end method
