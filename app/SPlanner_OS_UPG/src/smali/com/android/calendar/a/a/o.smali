.class public final enum Lcom/android/calendar/a/a/o;
.super Ljava/lang/Enum;
.source "WeatherReq.java"


# static fields
.field public static final enum a:Lcom/android/calendar/a/a/o;

.field public static final enum b:Lcom/android/calendar/a/a/o;

.field public static final enum c:Lcom/android/calendar/a/a/o;

.field public static final enum d:Lcom/android/calendar/a/a/o;

.field public static final enum e:Lcom/android/calendar/a/a/o;

.field public static final enum f:Lcom/android/calendar/a/a/o;

.field public static final enum g:Lcom/android/calendar/a/a/o;

.field public static final enum h:Lcom/android/calendar/a/a/o;

.field public static final enum i:Lcom/android/calendar/a/a/o;

.field public static final enum j:Lcom/android/calendar/a/a/o;

.field public static final enum k:Lcom/android/calendar/a/a/o;

.field private static final synthetic n:[Lcom/android/calendar/a/a/o;


# instance fields
.field public final l:Lcom/android/calendar/a/a/p;

.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v7, 0xa

    const/4 v6, 0x5

    const/4 v5, 0x1

    .line 50
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_HOUR"

    sget-object v2, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    invoke-direct {v0, v1, v8, v2, v5}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->a:Lcom/android/calendar/a/a/o;

    .line 51
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_12_HOURS"

    sget-object v2, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    const/16 v3, 0xc

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->b:Lcom/android/calendar/a/a/o;

    .line 52
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_24_HOURS"

    sget-object v2, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    const/16 v3, 0x18

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->c:Lcom/android/calendar/a/a/o;

    .line 53
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_72_HOURS"

    const/4 v2, 0x3

    sget-object v3, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    const/16 v4, 0x48

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->d:Lcom/android/calendar/a/a/o;

    .line 54
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_120_HOURS"

    const/4 v2, 0x4

    sget-object v3, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    const/16 v4, 0x78

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->e:Lcom/android/calendar/a/a/o;

    .line 55
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_240_HOURS"

    sget-object v2, Lcom/android/calendar/a/a/p;->a:Lcom/android/calendar/a/a/p;

    const/16 v3, 0xf0

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->f:Lcom/android/calendar/a/a/o;

    .line 56
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_DAY"

    const/4 v2, 0x6

    sget-object v3, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->g:Lcom/android/calendar/a/a/o;

    .line 57
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_5_DAYS"

    const/4 v2, 0x7

    sget-object v3, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->h:Lcom/android/calendar/a/a/o;

    .line 58
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_10_DAYS"

    const/16 v2, 0x8

    sget-object v3, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->i:Lcom/android/calendar/a/a/o;

    .line 59
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_15_DAYS"

    const/16 v2, 0x9

    sget-object v3, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->j:Lcom/android/calendar/a/a/o;

    .line 60
    new-instance v0, Lcom/android/calendar/a/a/o;

    const-string v1, "NEXT_25_DAYS"

    sget-object v2, Lcom/android/calendar/a/a/p;->b:Lcom/android/calendar/a/a/p;

    const/16 v3, 0x19

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/android/calendar/a/a/o;-><init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V

    sput-object v0, Lcom/android/calendar/a/a/o;->k:Lcom/android/calendar/a/a/o;

    .line 49
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/android/calendar/a/a/o;

    sget-object v1, Lcom/android/calendar/a/a/o;->a:Lcom/android/calendar/a/a/o;

    aput-object v1, v0, v8

    sget-object v1, Lcom/android/calendar/a/a/o;->b:Lcom/android/calendar/a/a/o;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/calendar/a/a/o;->c:Lcom/android/calendar/a/a/o;

    aput-object v1, v0, v9

    const/4 v1, 0x3

    sget-object v2, Lcom/android/calendar/a/a/o;->d:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/android/calendar/a/a/o;->e:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    sget-object v1, Lcom/android/calendar/a/a/o;->f:Lcom/android/calendar/a/a/o;

    aput-object v1, v0, v6

    const/4 v1, 0x6

    sget-object v2, Lcom/android/calendar/a/a/o;->g:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/calendar/a/a/o;->h:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/calendar/a/a/o;->i:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/calendar/a/a/o;->j:Lcom/android/calendar/a/a/o;

    aput-object v2, v0, v1

    sget-object v1, Lcom/android/calendar/a/a/o;->k:Lcom/android/calendar/a/a/o;

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/calendar/a/a/o;->n:[Lcom/android/calendar/a/a/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/android/calendar/a/a/p;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 66
    iput-object p3, p0, Lcom/android/calendar/a/a/o;->l:Lcom/android/calendar/a/a/p;

    .line 67
    iput p4, p0, Lcom/android/calendar/a/a/o;->m:I

    .line 68
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/a/a/o;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/android/calendar/a/a/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/o;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/a/a/o;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/android/calendar/a/a/o;->n:[Lcom/android/calendar/a/a/o;

    invoke-virtual {v0}, [Lcom/android/calendar/a/a/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/a/a/o;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/a/a/o;->l:Lcom/android/calendar/a/a/p;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/a/a/o;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/a/a/o;->l:Lcom/android/calendar/a/a/p;

    iget-object v1, v1, Lcom/android/calendar/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
