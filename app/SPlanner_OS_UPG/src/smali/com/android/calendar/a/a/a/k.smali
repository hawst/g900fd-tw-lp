.class public Lcom/android/calendar/a/a/a/k;
.super Ljava/lang/Thread;
.source "HttpThread.java"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/android/calendar/a/a/a/a;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/android/calendar/a/a/a/a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/android/calendar/a/a/a/k;->a:Landroid/os/Handler;

    .line 42
    iput-object v0, p0, Lcom/android/calendar/a/a/a/k;->b:Lcom/android/calendar/a/a/a/a;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a/k;->c:Z

    .line 46
    iput-object p1, p0, Lcom/android/calendar/a/a/a/k;->b:Lcom/android/calendar/a/a/a/a;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lcom/android/calendar/a/a/a/a;)I
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x6

    const/4 v1, -0x7

    const/4 v0, -0x8

    .line 84
    invoke-virtual {p1}, Lcom/android/calendar/a/a/a/a;->i()Lcom/android/calendar/a/a/a/j;

    move-result-object v6

    .line 88
    :try_start_0
    new-instance v4, Lcom/android/calendar/a/a/a/l;

    invoke-direct {v4}, Lcom/android/calendar/a/a/a/l;-><init>()V

    .line 89
    invoke-virtual {p1}, Lcom/android/calendar/a/a/a/a;->g()Ljava/lang/String;

    move-result-object v4

    const-string v7, "Application/JSON"

    invoke-virtual {p1}, Lcom/android/calendar/a/a/a/a;->h()I

    move-result v8

    invoke-static {v4, v7, v8}, Lcom/android/calendar/a/a/a/l;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 91
    :try_start_1
    invoke-virtual {p0}, Lcom/android/calendar/a/a/a/k;->b()Z
    :try_end_1
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    if-eqz v7, :cond_1

    .line 151
    if-eqz v4, :cond_0

    .line 152
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    :try_start_2
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    .line 96
    const/16 v8, 0xc8

    if-eq v7, v8, :cond_5

    .line 99
    const/16 v0, 0xcc

    if-ne v7, v0, :cond_3

    .line 100
    const/4 v0, -0x7

    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_2
    .catch Ljava/net/ConnectException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 151
    if-eqz v4, :cond_2

    .line 152
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    move v0, v1

    .line 153
    goto :goto_0

    .line 104
    :cond_3
    const/4 v0, -0x6

    :try_start_3
    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_3
    .catch Ljava/net/ConnectException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 151
    if-eqz v4, :cond_4

    .line 152
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move v0, v2

    .line 153
    goto :goto_0

    .line 108
    :cond_5
    :try_start_4
    invoke-virtual {p0}, Lcom/android/calendar/a/a/a/k;->b()Z
    :try_end_4
    .catch Ljava/net/ConnectException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v1

    if-eqz v1, :cond_6

    .line 151
    if-eqz v4, :cond_0

    .line 152
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 112
    :cond_6
    :try_start_5
    invoke-static {v4}, Lcom/android/calendar/a/a/a/l;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/net/ConnectException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v1

    .line 115
    :try_start_6
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-virtual {p0}, Lcom/android/calendar/a/a/a/k;->b()Z
    :try_end_6
    .catch Ljava/net/ConnectException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v2

    if-eqz v2, :cond_7

    .line 151
    if-eqz v5, :cond_0

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 121
    :cond_7
    :try_start_7
    invoke-virtual {p1, v1}, Lcom/android/calendar/a/a/a/a;->a(Ljava/lang/String;)I

    move-result v0

    .line 122
    if-eqz v0, :cond_8

    .line 123
    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_7
    .catch Ljava/net/ConnectException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 151
    if-eqz v5, :cond_0

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 127
    :cond_8
    const/4 v0, 0x0

    :try_start_8
    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_8
    .catch Ljava/net/ConnectException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 151
    if-eqz v5, :cond_9

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    :goto_1
    move v0, v3

    .line 156
    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    move-object v1, v5

    .line 132
    :goto_2
    :try_start_9
    invoke-virtual {v0}, Ljava/net/ConnectException;->printStackTrace()V

    .line 133
    const/4 v0, -0x2

    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 151
    if-eqz v1, :cond_9

    .line 152
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 135
    :catch_1
    move-exception v0

    .line 137
    :goto_3
    :try_start_a
    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->printStackTrace()V

    .line 138
    const/4 v0, -0x4

    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 151
    if-eqz v5, :cond_9

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 140
    :catch_2
    move-exception v0

    .line 142
    :goto_4
    :try_start_b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 143
    const/4 v0, -0x5

    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 151
    if-eqz v5, :cond_9

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 145
    :catch_3
    move-exception v0

    .line 147
    :goto_5
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 148
    const/4 v0, -0x1

    invoke-virtual {p0, v6, v0, p1}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 151
    if-eqz v5, :cond_9

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    .line 151
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v5, :cond_a

    .line 152
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 153
    :cond_a
    throw v0

    .line 151
    :catchall_1
    move-exception v0

    move-object v5, v4

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v5, v1

    goto :goto_6

    .line 145
    :catch_4
    move-exception v0

    move-object v5, v4

    goto :goto_5

    .line 140
    :catch_5
    move-exception v0

    move-object v5, v4

    goto :goto_4

    .line 135
    :catch_6
    move-exception v0

    move-object v5, v4

    goto :goto_3

    .line 130
    :catch_7
    move-exception v0

    move-object v1, v4

    goto :goto_2
.end method

.method public a()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/a/a/a/k;->c:Z

    .line 54
    return-void
.end method

.method public a(Lcom/android/calendar/a/a/a/j;ILcom/android/calendar/a/a/a/a;)V
    .locals 3

    .prologue
    .line 164
    if-eqz p1, :cond_0

    .line 166
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/a/a/a/h;->a()Landroid/os/Handler;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 169
    iput-object p3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 170
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 171
    iput p2, v1, Landroid/os/Message;->arg1:I

    .line 172
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 174
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/android/calendar/a/a/a/k;->c:Z

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 69
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "HttpThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/android/calendar/a/a/a/k;->b:Lcom/android/calendar/a/a/a/a;

    invoke-virtual {p0, v0}, Lcom/android/calendar/a/a/a/k;->a(Lcom/android/calendar/a/a/a/a;)I

    .line 79
    return-void
.end method
