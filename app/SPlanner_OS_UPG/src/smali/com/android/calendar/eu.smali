.class Lcom/android/calendar/eu;
.super Lcom/android/calendar/ew;
.source "LinkActionChooserFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/eo;


# direct methods
.method constructor <init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/android/calendar/eu;->a:Lcom/android/calendar/eo;

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/ew;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 199
    :try_start_0
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/eu;->a:Lcom/android/calendar/eo;

    invoke-static {v0}, Lcom/android/calendar/eo;->c(Lcom/android/calendar/eo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 201
    const-string v1, "vnd.android.cursor.dir/bookmark"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    const-string v1, "com.sec.android.app.sbrowser"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v1, "url"

    iget-object v2, p0, Lcom/android/calendar/eu;->a:Lcom/android/calendar/eo;

    invoke-static {v2}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 214
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    sget-object v2, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 207
    const-string v1, "url"

    iget-object v2, p0, Lcom/android/calendar/eu;->a:Lcom/android/calendar/eo;

    invoke-static {v2}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v1, "vnd.android.cursor.dir/bookmark"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    :catch_0
    move-exception v0

    .line 212
    invoke-static {}, Lcom/android/calendar/eo;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkActionChooserFragment: Browser activity not found."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
