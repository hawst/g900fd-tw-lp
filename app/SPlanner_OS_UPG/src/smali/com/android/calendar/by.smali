.class public Lcom/android/calendar/by;
.super Landroid/widget/BaseAdapter;
.source "DayEventAdapter.java"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/android/calendar/al;

.field protected c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/android/calendar/by;->a:Landroid/content/Context;

    .line 41
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/by;->b:Lcom/android/calendar/al;

    .line 42
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/by;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/by;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 65
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 70
    .line 73
    iget-object v0, p0, Lcom/android/calendar/by;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 77
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 78
    instance-of v3, v1, Lcom/android/calendar/bz;

    if-eqz v3, :cond_4

    .line 80
    check-cast v1, Lcom/android/calendar/bz;

    move-object v2, p2

    .line 84
    :goto_0
    if-nez v1, :cond_3

    .line 86
    new-instance v1, Lcom/android/calendar/bz;

    invoke-direct {v1}, Lcom/android/calendar/bz;-><init>()V

    .line 87
    const v2, 0x7f040032

    invoke-virtual {v0, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 88
    const v0, 0x7f1200b7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/bz;->a:Landroid/view/View;

    .line 89
    const v0, 0x7f1200b3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/bz;->e:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f1200b4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/bz;->f:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f1200b5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/bz;->g:Landroid/view/View;

    .line 92
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    move-object v1, v2

    .line 95
    :goto_1
    iget-object v2, v0, Lcom/android/calendar/bz;->a:Landroid/view/View;

    .line 96
    iget-object v3, v0, Lcom/android/calendar/bz;->e:Landroid/widget/TextView;

    .line 97
    iget-object v4, v0, Lcom/android/calendar/bz;->f:Landroid/widget/TextView;

    .line 98
    iget-object v5, v0, Lcom/android/calendar/bz;->g:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lcom/android/calendar/by;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 102
    iget v6, v0, Lcom/android/calendar/dh;->c:I

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 103
    iget-object v2, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v2, p0, Lcom/android/calendar/by;->a:Landroid/content/Context;

    const v3, 0x7f0f02d7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    iget-object v3, p0, Lcom/android/calendar/by;->b:Lcom/android/calendar/al;

    invoke-virtual {v3}, Lcom/android/calendar/al;->g()I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_0

    .line 106
    iget-object v3, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 107
    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_0
    iget-boolean v0, v0, Lcom/android/calendar/dh;->o:Z

    if-eqz v0, :cond_2

    .line 111
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 115
    :goto_3
    return-object v1

    .line 106
    :cond_1
    iget-object v2, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 113
    :cond_2
    const/4 v0, 0x4

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_3
    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto/16 :goto_0
.end method
