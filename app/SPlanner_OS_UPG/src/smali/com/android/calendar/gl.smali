.class public Lcom/android/calendar/gl;
.super Landroid/widget/ArrayAdapter;
.source "SPinnerArrayAdapter.java"


# instance fields
.field private a:I

.field private b:Landroid/content/res/ColorStateList;

.field private c:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/gl;->a:I

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/gl;->b:Landroid/content/res/ColorStateList;

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b014b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/gl;->c:Landroid/content/res/ColorStateList;

    .line 38
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 41
    iput p1, p0, Lcom/android/calendar/gl;->a:I

    .line 42
    return-void
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    iget v1, p0, Lcom/android/calendar/gl;->a:I

    if-ne p1, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/android/calendar/gl;->c:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 52
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/gl;->b:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method
