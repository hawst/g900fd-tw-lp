.class public Lcom/android/calendar/ej;
.super Ljava/lang/Object;
.source "HolidayManager.java"


# static fields
.field private static a:Lcom/android/calendar/ej;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/ContentResolver;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private final w:[[Ljava/lang/String;

.field private final x:[I

.field private final y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/ej;->a:Lcom/android/calendar/ej;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/16 v0, 0xc

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v5

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v6

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v4, [Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    .line 76
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/ej;->x:[I

    .line 80
    const/16 v0, 0x16

    iput v0, p0, Lcom/android/calendar/ej;->y:I

    .line 131
    iput-object p1, p0, Lcom/android/calendar/ej;->b:Landroid/content/Context;

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ej;->c:Landroid/content/ContentResolver;

    .line 133
    iget-object v0, p0, Lcom/android/calendar/ej;->b:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/calendar/ej;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/android/calendar/ej;->a()Z

    .line 136
    :cond_0
    return-void

    .line 76
    :array_0
    .array-data 4
        0x7d9
        0x7df
        0x7ea
    .end array-data
.end method

.method private a(I)I
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/16 v1, 0x15

    const/16 v0, 0x14

    .line 344
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 345
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 346
    const/4 v4, 0x4

    invoke-virtual {v2, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 348
    rem-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_0

    .line 349
    packed-switch v2, :pswitch_data_0

    .line 389
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 363
    goto :goto_0

    .line 369
    :cond_0
    packed-switch v2, :pswitch_data_1

    move v0, v1

    .line 385
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 377
    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 369
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(IIII)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 332
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 333
    invoke-virtual {v0, v1, p2, p1}, Landroid/text/format/Time;->set(III)V

    .line 334
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 335
    iget v1, v0, Landroid/text/format/Time;->weekDay:I

    .line 336
    const/4 v0, 0x0

    .line 337
    if-ge p4, v1, :cond_0

    .line 338
    const/4 v0, 0x7

    .line 340
    :cond_0
    add-int/lit8 v2, p3, -0x1

    mul-int/lit8 v2, v2, 0x7

    add-int/2addr v0, p4

    sub-int/2addr v0, v1

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static a(Landroid/content/Context;)Lcom/android/calendar/ej;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/android/calendar/ej;->a:Lcom/android/calendar/ej;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/android/calendar/ej;

    invoke-direct {v0, p0}, Lcom/android/calendar/ej;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/calendar/ej;->a:Lcom/android/calendar/ej;

    .line 142
    :cond_0
    sget-object v0, Lcom/android/calendar/ej;->a:Lcom/android/calendar/ej;

    return-object v0
.end method

.method private a()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 283
    .line 285
    iget-object v0, p0, Lcom/android/calendar/ej;->c:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "account_name=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/calendar/ej;->f:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "account_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'LOCAL\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_sync_id"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "1"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'legalHoliday\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "calendar_color"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0xd4

    const/16 v8, 0x3f

    const/16 v9, 0x16

    invoke-static {v5, v8, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 310
    if-eqz v1, :cond_2

    .line 312
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 313
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ej;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v6

    .line 323
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 328
    :goto_1
    return v0

    .line 315
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/ej;->d:Ljava/lang/String;

    move v0, v7

    .line 317
    goto :goto_0

    .line 319
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ej;->d:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v6

    goto :goto_0

    .line 323
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 326
    :cond_2
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/ej;->d:Ljava/lang/String;

    move v0, v6

    goto :goto_1
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 393
    rem-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_0

    rem-int/lit8 v0, p1, 0x64

    if-nez v0, :cond_1

    :cond_0
    rem-int/lit16 v0, p1, 0x190

    if-nez v0, :cond_2

    .line 394
    :cond_1
    const/16 v0, 0x16

    .line 396
    :goto_0
    return v0

    :cond_2
    const/16 v0, 0x17

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    .line 84
    if-eqz p1, :cond_0

    .line 86
    const v2, 0x7f0f0251

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->e:Ljava/lang/String;

    .line 87
    const v2, 0x7f0f0252

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->f:Ljava/lang/String;

    .line 88
    const v2, 0x7f0f0259

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->g:Ljava/lang/String;

    .line 89
    const v2, 0x7f0f0258

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->h:Ljava/lang/String;

    .line 90
    const v2, 0x7f0f025a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->i:Ljava/lang/String;

    .line 91
    const v2, 0x7f0f025b

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->j:Ljava/lang/String;

    .line 92
    const v2, 0x7f0f025c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->k:Ljava/lang/String;

    .line 93
    const v2, 0x7f0f025e

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->l:Ljava/lang/String;

    .line 94
    const v2, 0x7f0f025f

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->m:Ljava/lang/String;

    .line 95
    const v2, 0x7f0f025d

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->n:Ljava/lang/String;

    .line 96
    const v2, 0x7f0f0260

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->o:Ljava/lang/String;

    .line 97
    const v2, 0x7f0f0262

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->p:Ljava/lang/String;

    .line 98
    const v2, 0x7f0f0261

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->q:Ljava/lang/String;

    .line 99
    const v2, 0x7f0f0253

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->r:Ljava/lang/String;

    .line 100
    const v2, 0x7f0f0254

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->s:Ljava/lang/String;

    .line 101
    const v2, 0x7f0f0255

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->t:Ljava/lang/String;

    .line 102
    const v2, 0x7f0f0256

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->u:Ljava/lang/String;

    .line 103
    const v2, 0x7f0f0257

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/ej;->v:Ljava/lang/String;

    .line 104
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->g:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 105
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/ej;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 106
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->i:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 107
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->j:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 108
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->k:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 109
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->l:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 110
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/ej;->m:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 111
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/calendar/ej;->n:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 112
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->o:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 113
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->p:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 114
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/ej;->q:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 115
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->r:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 116
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->s:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 117
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/android/calendar/ej;->t:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 118
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 119
    iget-object v2, p0, Lcom/android/calendar/ej;->w:[[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/ej;->v:Ljava/lang/String;

    aput-object v4, v2, v3
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return v0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    const-string v2, "HolidayManager"

    invoke-virtual {v0}, Landroid/content/res/Resources$NotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 123
    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/text/format/Time;)Z
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/4 v7, 0x2

    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v1, 0x1

    .line 146
    const/4 v0, 0x0

    .line 147
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-nez v2, :cond_0

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 150
    :cond_0
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v1, :cond_1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_1

    move v0, v1

    .line 153
    :cond_1
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v5, :cond_2

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 156
    :cond_2
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v4, :cond_3

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v5, :cond_3

    move v0, v1

    .line 159
    :cond_3
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v4, :cond_4

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v4, :cond_4

    move v0, v1

    .line 162
    :cond_4
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v4, :cond_5

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    move v0, v1

    .line 166
    :cond_5
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_6

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_6

    iget v2, p1, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7df

    if-le v2, v3, :cond_6

    move v0, v1

    .line 169
    :cond_6
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_7

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v5, :cond_7

    move v0, v1

    .line 172
    :cond_7
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_8

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x17

    if-ne v2, v3, :cond_8

    move v0, v1

    .line 175
    :cond_8
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_9

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x17

    if-ne v2, v3, :cond_9

    move v0, v1

    .line 180
    :cond_9
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v7, :cond_a

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2}, Lcom/android/calendar/ej;->a(I)I

    move-result v2

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_a

    move v0, v1

    .line 185
    :cond_a
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v6, :cond_b

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2}, Lcom/android/calendar/ej;->b(I)I

    move-result v2

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_b

    move v0, v1

    .line 190
    :cond_b
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-nez v2, :cond_c

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4, v7, v1}, Lcom/android/calendar/ej;->a(IIII)I

    move-result v3

    if-ne v2, v3, :cond_c

    move v0, v1

    .line 195
    :cond_c
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_d

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    const/16 v4, 0x9

    invoke-direct {p0, v3, v4, v7, v1}, Lcom/android/calendar/ej;->a(IIII)I

    move-result v3

    if-ne v2, v3, :cond_d

    move v0, v1

    .line 200
    :cond_d
    iget v2, p1, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7cc

    if-lt v2, v3, :cond_e

    iget v2, p1, Landroid/text/format/Time;->month:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_e

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    const/4 v4, 0x6

    invoke-direct {p0, v3, v4, v5, v1}, Lcom/android/calendar/ej;->a(IIII)I

    move-result v3

    if-ne v2, v3, :cond_e

    move v0, v1

    .line 205
    :cond_e
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v6, :cond_f

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    iget v3, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v3, v6, v5, v1}, Lcom/android/calendar/ej;->a(IIII)I

    move-result v3

    if-ne v2, v3, :cond_f

    move v0, v1

    .line 210
    :cond_f
    iget v2, p1, Landroid/text/format/Time;->year:I

    iget-object v3, p0, Lcom/android/calendar/ej;->x:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eq v2, v3, :cond_10

    iget v2, p1, Landroid/text/format/Time;->year:I

    iget-object v3, p0, Lcom/android/calendar/ej;->x:[I

    aget v3, v3, v1

    if-eq v2, v3, :cond_10

    iget v2, p1, Landroid/text/format/Time;->year:I

    iget-object v3, p0, Lcom/android/calendar/ej;->x:[I

    aget v3, v3, v7

    if-ne v2, v3, :cond_11

    :cond_10
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v6, :cond_11

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x16

    if-ne v2, v3, :cond_11

    move v0, v1

    .line 215
    :cond_11
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v6, :cond_12

    .line 216
    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2}, Lcom/android/calendar/ej;->b(I)I

    move-result v2

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, 0x1

    if-ne v2, v3, :cond_12

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v3, v6, v5, v1}, Lcom/android/calendar/ej;->a(IIII)I

    move-result v3

    if-ne v2, v3, :cond_12

    move v0, v1

    .line 221
    :cond_12
    return v0
.end method

.method public b(Landroid/text/format/Time;)Z
    .locals 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 231
    const/4 v0, 0x0

    .line 232
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-nez v2, :cond_0

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v4, :cond_0

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 236
    :cond_0
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v1, :cond_1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_1

    move v0, v1

    .line 240
    :cond_1
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v5, :cond_2

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x1e

    if-ne v2, v3, :cond_2

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_2

    move v0, v1

    .line 244
    :cond_2
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v6, :cond_4

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_4

    .line 245
    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v1, :cond_3

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-eq v2, v4, :cond_3

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v5, :cond_4

    :cond_3
    move v0, v1

    .line 251
    :cond_4
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v7, :cond_5

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v6, :cond_5

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_5

    move v0, v1

    .line 255
    :cond_5
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v7, :cond_6

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x18

    if-ne v2, v3, :cond_6

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_6

    move v0, v1

    .line 259
    :cond_6
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_7

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0x18

    if-ne v2, v3, :cond_7

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_7

    move v0, v1

    .line 264
    :cond_7
    iget v2, p1, Landroid/text/format/Time;->month:I

    if-ne v2, v4, :cond_8

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2}, Lcom/android/calendar/ej;->a(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_8

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_8

    move v0, v1

    .line 270
    :cond_8
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_9

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2}, Lcom/android/calendar/ej;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_9

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_9

    move v0, v1

    .line 276
    :cond_9
    iget v2, p1, Landroid/text/format/Time;->month:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_a

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_a

    iget v2, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v2, v1, :cond_a

    iget v2, p1, Landroid/text/format/Time;->year:I

    const/16 v3, 0x7e0

    if-lt v2, v3, :cond_a

    move v0, v1

    .line 279
    :cond_a
    return v0
.end method
