.class public Lcom/android/calendar/au;
.super Ljava/lang/Object;
.source "CalendarEventModel.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput p1, p0, Lcom/android/calendar/au;->a:I

    .line 153
    iput p2, p0, Lcom/android/calendar/au;->b:I

    .line 154
    return-void
.end method

.method public static a(I)Lcom/android/calendar/au;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Lcom/android/calendar/au;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/android/calendar/au;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/au;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/android/calendar/au;->a:I

    return v0
.end method

.method public a(Lcom/android/calendar/au;)I
    .locals 2

    .prologue
    .line 194
    iget v0, p1, Lcom/android/calendar/au;->a:I

    iget v1, p0, Lcom/android/calendar/au;->a:I

    if-eq v0, v1, :cond_0

    .line 195
    iget v0, p1, Lcom/android/calendar/au;->a:I

    iget v1, p0, Lcom/android/calendar/au;->a:I

    sub-int/2addr v0, v1

    .line 200
    :goto_0
    return v0

    .line 197
    :cond_0
    iget v0, p1, Lcom/android/calendar/au;->b:I

    iget v1, p0, Lcom/android/calendar/au;->b:I

    if-eq v0, v1, :cond_1

    .line 198
    iget v0, p0, Lcom/android/calendar/au;->b:I

    iget v1, p1, Lcom/android/calendar/au;->b:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 200
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/android/calendar/au;->b:I

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 120
    check-cast p1, Lcom/android/calendar/au;

    invoke-virtual {p0, p1}, Lcom/android/calendar/au;->a(Lcom/android/calendar/au;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 163
    if-ne p0, p1, :cond_1

    move v0, v1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    instance-of v2, p1, Lcom/android/calendar/au;

    if-eqz v2, :cond_0

    .line 170
    check-cast p1, Lcom/android/calendar/au;

    .line 172
    iget v2, p1, Lcom/android/calendar/au;->a:I

    iget v3, p0, Lcom/android/calendar/au;->a:I

    if-ne v2, v3, :cond_0

    .line 179
    iget v2, p1, Lcom/android/calendar/au;->b:I

    iget v3, p0, Lcom/android/calendar/au;->b:I

    if-eq v2, v3, :cond_3

    iget v2, p1, Lcom/android/calendar/au;->b:I

    if-nez v2, :cond_2

    iget v2, p0, Lcom/android/calendar/au;->b:I

    if-eq v2, v1, :cond_3

    :cond_2
    iget v2, p1, Lcom/android/calendar/au;->b:I

    if-ne v2, v1, :cond_0

    iget v2, p0, Lcom/android/calendar/au;->b:I

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/android/calendar/au;->a:I

    mul-int/lit8 v0, v0, 0xa

    iget v1, p0, Lcom/android/calendar/au;->b:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReminderEntry min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/au;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " meth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/au;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
