.class public Lcom/android/calendar/el;
.super Ljava/lang/Object;
.source "LimitContract.java"


# direct methods
.method public static a(Landroid/text/format/Time;)I
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const v8, 0x253d8b    # 3.419991E-39f

    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 64
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/text/format/Time;->toMillis(Z)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 71
    :goto_0
    iget-wide v6, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v5

    .line 72
    const v6, 0x24dc87

    if-ge v5, v6, :cond_0

    move v0, v2

    .line 87
    :goto_1
    return v0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    iput-boolean v4, p0, Landroid/text/format/Time;->allDay:Z

    .line 67
    invoke-virtual {p0, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 68
    invoke-virtual {p0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    goto :goto_0

    .line 76
    :cond_0
    iget v6, p0, Landroid/text/format/Time;->year:I

    const/16 v7, 0x76e

    if-gt v6, v7, :cond_2

    if-eq v5, v8, :cond_1

    cmp-long v6, v0, v10

    if-nez v6, :cond_2

    :cond_1
    move v0, v2

    .line 77
    goto :goto_1

    .line 80
    :cond_2
    const v2, 0x259d23

    if-le v5, v2, :cond_3

    move v0, v3

    .line 81
    goto :goto_1

    .line 84
    :cond_3
    iget v2, p0, Landroid/text/format/Time;->year:I

    const/16 v6, 0x7f4

    if-lt v2, v6, :cond_5

    if-eq v5, v8, :cond_4

    cmp-long v0, v0, v10

    if-nez v0, :cond_5

    :cond_4
    move v0, v3

    .line 85
    goto :goto_1

    :cond_5
    move v0, v4

    .line 87
    goto :goto_1
.end method
