.class public abstract Lcom/android/calendar/b;
.super Landroid/app/Activity;
.source "AbstractCalendarActivity.java"


# instance fields
.field protected a:Lcom/android/calendar/ag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()Lcom/android/calendar/ag;
    .locals 1

    .prologue
    .line 23
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/b;->a:Lcom/android/calendar/ag;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/android/calendar/ag;

    invoke-direct {v0, p0}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/b;->a:Lcom/android/calendar/ag;

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/b;->a:Lcom/android/calendar/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 13
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 14
    invoke-virtual {p0}, Lcom/android/calendar/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 17
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 18
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 20
    :cond_0
    return-void
.end method
