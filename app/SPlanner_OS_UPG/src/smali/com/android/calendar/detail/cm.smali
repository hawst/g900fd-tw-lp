.class public Lcom/android/calendar/detail/cm;
.super Landroid/app/DialogFragment;
.source "SelectResponseDialog.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Z

.field private final f:Z

.field private final g:Lcom/android/calendar/detail/cp;

.field private h:Lcom/android/calendar/detail/w;

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Landroid/app/DialogFragment;

.field private m:Landroid/content/DialogInterface$OnClickListener;

.field private final n:Lcom/android/calendar/detail/z;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/android/calendar/detail/cm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/cm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/detail/cp;JLjava/lang/String;JZILjava/lang/String;ZLcom/android/calendar/detail/w;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 105
    new-instance v0, Lcom/android/calendar/detail/cn;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cn;-><init>(Lcom/android/calendar/detail/cm;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cm;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 160
    new-instance v0, Lcom/android/calendar/detail/co;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/co;-><init>(Lcom/android/calendar/detail/cm;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cm;->n:Lcom/android/calendar/detail/z;

    .line 76
    iput-object p1, p0, Lcom/android/calendar/detail/cm;->g:Lcom/android/calendar/detail/cp;

    .line 77
    iput-wide p2, p0, Lcom/android/calendar/detail/cm;->b:J

    .line 78
    iput-object p4, p0, Lcom/android/calendar/detail/cm;->c:Ljava/lang/String;

    .line 79
    iput-wide p5, p0, Lcom/android/calendar/detail/cm;->d:J

    .line 80
    iput-boolean p7, p0, Lcom/android/calendar/detail/cm;->e:Z

    .line 81
    iput p8, p0, Lcom/android/calendar/detail/cm;->i:I

    .line 82
    iput-object p9, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    .line 83
    iput-boolean p10, p0, Lcom/android/calendar/detail/cm;->f:Z

    .line 84
    iput-object p11, p0, Lcom/android/calendar/detail/cm;->h:Lcom/android/calendar/detail/w;

    .line 85
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->h:Lcom/android/calendar/detail/w;

    iget-object v1, p0, Lcom/android/calendar/detail/cm;->n:Lcom/android/calendar/detail/z;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/w;->a(Lcom/android/calendar/detail/z;)V

    .line 86
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/cm;->setRetainInstance(Z)V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cm;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/android/calendar/detail/cm;->j:I

    return p1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 146
    iget v0, p0, Lcom/android/calendar/detail/cm;->j:I

    if-eqz v0, :cond_1

    .line 147
    iget v0, p0, Lcom/android/calendar/detail/cm;->j:I

    .line 152
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/detail/cm;->g:Lcom/android/calendar/detail/cp;

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/android/calendar/detail/cm;->g:Lcom/android/calendar/detail/cp;

    invoke-interface {v1, v0}, Lcom/android/calendar/detail/cp;->a(I)V

    .line 155
    :cond_0
    return-void

    .line 149
    :cond_1
    iget v0, p0, Lcom/android/calendar/detail/cm;->i:I

    goto :goto_0
.end method

.method private a(JJI)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 235
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 236
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    const-string v0, "attendeeEmail"

    iget-object v1, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :cond_0
    const-string v0, "attendeeStatus"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 240
    const-string v0, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 241
    sget-object v0, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 243
    new-instance v0, Lcom/android/calendar/detail/cq;

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/calendar/detail/cq;-><init>(Landroid/content/ContentResolver;)V

    const/4 v1, 0x0

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/android/calendar/detail/cq;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cm;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->f()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/android/calendar/detail/cm;->f:Z

    if-nez v0, :cond_0

    .line 170
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->a()V

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->h:Lcom/android/calendar/detail/w;

    iget-object v1, p0, Lcom/android/calendar/detail/cm;->h:Lcom/android/calendar/detail/w;

    invoke-virtual {v1}, Lcom/android/calendar/detail/w;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/w;->b(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/detail/cm;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/android/calendar/detail/cm;->e:Z

    return v0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 180
    new-instance v1, Lcom/android/calendar/detail/h;

    iget-wide v2, p0, Lcom/android/calendar/detail/cm;->b:J

    iget v4, p0, Lcom/android/calendar/detail/cm;->j:I

    iget-object v5, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/cm;->n:Lcom/android/calendar/detail/z;

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/detail/h;-><init>(JILjava/lang/String;Lcom/android/calendar/detail/z;)V

    iput-object v1, p0, Lcom/android/calendar/detail/cm;->l:Landroid/app/DialogFragment;

    .line 182
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->l:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/detail/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/detail/cm;)Z
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/cm;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/android/calendar/detail/cm;->j:I

    return v0
.end method

.method private d()V
    .locals 9

    .prologue
    .line 186
    new-instance v1, Lcom/android/calendar/detail/f;

    iget-wide v2, p0, Lcom/android/calendar/detail/cm;->b:J

    iget-object v4, p0, Lcom/android/calendar/detail/cm;->c:Ljava/lang/String;

    iget v5, p0, Lcom/android/calendar/detail/cm;->j:I

    iget-object v6, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    iget-object v7, p0, Lcom/android/calendar/detail/cm;->n:Lcom/android/calendar/detail/z;

    iget-object v8, p0, Lcom/android/calendar/detail/cm;->h:Lcom/android/calendar/detail/w;

    invoke-direct/range {v1 .. v8}, Lcom/android/calendar/detail/f;-><init>(JLjava/lang/String;ILjava/lang/String;Lcom/android/calendar/detail/z;Lcom/android/calendar/detail/w;)V

    iput-object v1, p0, Lcom/android/calendar/detail/cm;->l:Landroid/app/DialogFragment;

    .line 188
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->l:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/detail/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/detail/cm;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/android/calendar/detail/cm;->i:I

    return v0
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 193
    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->isRemoving()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v0

    .line 197
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 198
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.email.intent.action.CALENDAR_MEETING_RESPONSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 199
    if-eqz v1, :cond_0

    .line 200
    const/high16 v3, 0x10000

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 201
    if-eqz v1, :cond_0

    .line 202
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 216
    iget v0, p0, Lcom/android/calendar/detail/cm;->i:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 217
    iget-wide v2, p0, Lcom/android/calendar/detail/cm;->b:J

    iget-wide v4, p0, Lcom/android/calendar/detail/cm;->d:J

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/detail/cm;->a(JJI)V

    .line 218
    iput v6, p0, Lcom/android/calendar/detail/cm;->i:I

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cm;->g:Lcom/android/calendar/detail/cp;

    iget v1, p0, Lcom/android/calendar/detail/cm;->j:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/android/calendar/detail/cp;->a(IZ)V

    .line 222
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.email.intent.action.CALENDAR_PROPOSE_NEW_TIME"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 223
    const-string v1, "event_id"

    iget-wide v2, p0, Lcom/android/calendar/detail/cm;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 224
    const-string v1, "account_email"

    iget-object v2, p0, Lcom/android/calendar/detail/cm;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/cm;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    sget-object v0, Lcom/android/calendar/detail/cm;->a:Ljava/lang/String;

    const-string v1, "Error: Could not find IEventInfo.PROPOSE_NEW_TIME_ACTION activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/detail/cm;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/android/calendar/detail/cm;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/detail/cm;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->c()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/detail/cm;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->d()V

    return-void
.end method

.method static synthetic i(Lcom/android/calendar/detail/cm;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->b()V

    return-void
.end method

.method static synthetic j(Lcom/android/calendar/detail/cm;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/calendar/detail/cm;->a()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    .line 95
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/detail/cm;->e:Z

    if-eqz v0, :cond_2

    const v0, 0x7f09002c

    .line 96
    :goto_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/detail/cm;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    const v2, 0x7f0f0388

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 98
    iget-object v2, p0, Lcom/android/calendar/detail/cm;->m:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 100
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 101
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0

    .line 95
    :cond_2
    const v0, 0x7f09002d

    goto :goto_1
.end method
