.class Lcom/android/calendar/detail/af;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/android/calendar/detail/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1842
    iput-object p1, p0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    iput-object p2, p0, Lcom/android/calendar/detail/af;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1844
    iget-object v0, p0, Lcom/android/calendar/detail/af;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1845
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.DIAL"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1846
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tel:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/detail/af;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1847
    iget-object v0, p0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 1915
    :goto_0
    return-void

    .line 1849
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f04009b

    iget-object v3, p0, Lcom/android/calendar/detail/af;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1853
    iget-object v1, p0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;I)I

    .line 1855
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f009f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/detail/ai;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/ai;-><init>(Lcom/android/calendar/detail/af;)V

    invoke-virtual {v1, v0, v4, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/android/calendar/detail/ah;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/ah;-><init>(Lcom/android/calendar/detail/af;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/android/calendar/detail/ag;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/ag;-><init>(Lcom/android/calendar/detail/af;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1899
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 1900
    new-instance v2, Lcom/android/calendar/detail/aj;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/detail/aj;-><init>(Lcom/android/calendar/detail/af;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1913
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
