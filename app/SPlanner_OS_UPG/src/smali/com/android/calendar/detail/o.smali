.class public Lcom/android/calendar/detail/o;
.super Landroid/widget/BaseAdapter;
.source "EasyModeListAdapter.java"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/content/res/Resources;

.field private c:Ljava/util/ArrayList;

.field private d:Ljava/util/HashMap;

.field private e:Z

.field private f:I

.field private g:Lcom/android/calendar/d/g;

.field private h:Landroid/view/LayoutInflater;

.field private i:I

.field private j:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/android/calendar/detail/o;->b:Landroid/content/res/Resources;

    .line 35
    iput-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    .line 36
    iput-object v0, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    .line 37
    iput-boolean v1, p0, Lcom/android/calendar/detail/o;->e:Z

    .line 38
    iput v1, p0, Lcom/android/calendar/detail/o;->f:I

    .line 42
    iput-object v0, p0, Lcom/android/calendar/detail/o;->h:Landroid/view/LayoutInflater;

    .line 108
    new-instance v0, Lcom/android/calendar/detail/p;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/p;-><init>(Lcom/android/calendar/detail/o;)V

    iput-object v0, p0, Lcom/android/calendar/detail/o;->j:Ljava/util/Comparator;

    .line 60
    iput-object p1, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    .line 61
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/o;->g:Lcom/android/calendar/d/g;

    .line 62
    invoke-direct {p0}, Lcom/android/calendar/detail/o;->g()V

    .line 63
    return-void
.end method

.method private a(ILandroid/view/View;Lcom/android/calendar/detail/q;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 233
    .line 234
    invoke-virtual {p0, p1}, Lcom/android/calendar/detail/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 235
    if-nez v0, :cond_0

    .line 298
    :goto_0
    return-object p2

    .line 240
    :cond_0
    iget v1, v0, Lcom/android/calendar/dh;->c:I

    .line 241
    if-nez v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/android/calendar/detail/o;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b0073

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 244
    :cond_1
    iget-object v4, p3, Lcom/android/calendar/detail/q;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 247
    iget-object v1, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 248
    iget-object v1, p3, Lcom/android/calendar/detail/q;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/android/calendar/dh;->d:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    :cond_2
    invoke-direct {p0, v0, p3}, Lcom/android/calendar/detail/o;->a(Lcom/android/calendar/dh;Lcom/android/calendar/detail/q;)V

    .line 255
    iget-object v1, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 257
    iget-object v1, p3, Lcom/android/calendar/detail/q;->c:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/android/calendar/dh;->e:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v1, p3, Lcom/android/calendar/detail/q;->c:Landroid/widget/TextView;

    invoke-static {v1, v3}, Lcom/android/calendar/detail/o;->a(Landroid/view/View;I)V

    .line 263
    :goto_1
    invoke-static {v0}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;)Z

    move-result v4

    .line 264
    const v1, 0x7f1200d6

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 266
    iget-wide v6, v0, Lcom/android/calendar/dh;->b:J

    iput-wide v6, p3, Lcom/android/calendar/detail/q;->g:J

    .line 267
    iget-wide v6, v0, Lcom/android/calendar/dh;->m:J

    iput-wide v6, p3, Lcom/android/calendar/detail/q;->h:J

    .line 268
    iget-wide v6, v0, Lcom/android/calendar/dh;->n:J

    iput-wide v6, p3, Lcom/android/calendar/detail/q;->i:J

    .line 269
    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 271
    if-nez v4, :cond_4

    .line 272
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 277
    :goto_2
    iget-boolean v1, p0, Lcom/android/calendar/detail/o;->e:Z

    if-eqz v1, :cond_5

    if-eqz v4, :cond_5

    .line 278
    iget-object v1, p3, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 283
    :goto_3
    iget-object v4, p3, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/android/calendar/detail/o;->e:Z

    if-eqz v1, :cond_6

    iget-wide v6, v0, Lcom/android/calendar/dh;->b:J

    invoke-virtual {p0, v6, v7}, Lcom/android/calendar/detail/o;->a(J)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 284
    iget-object v1, p3, Lcom/android/calendar/detail/q;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 285
    iget-boolean v2, p0, Lcom/android/calendar/detail/o;->e:Z

    if-eqz v2, :cond_7

    .line 286
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 290
    :goto_5
    iget-object v2, p3, Lcom/android/calendar/detail/q;->f:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 292
    iget v0, v0, Lcom/android/calendar/dh;->B:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 293
    const v0, 0x3ecccccd    # 0.4f

    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_0

    .line 260
    :cond_3
    iget-object v1, p3, Lcom/android/calendar/detail/q;->c:Landroid/widget/TextView;

    invoke-static {v1, v5}, Lcom/android/calendar/detail/o;->a(Landroid/view/View;I)V

    goto :goto_1

    .line 274
    :cond_4
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_2

    .line 280
    :cond_5
    iget-object v1, p3, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v1, v3

    .line 283
    goto :goto_4

    .line 288
    :cond_7
    iget v2, p0, Lcom/android/calendar/detail/o;->i:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_5

    .line 295
    :cond_8
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_0
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 317
    if-eqz p0, :cond_0

    .line 318
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    :cond_0
    return-void
.end method

.method private a(Lcom/android/calendar/dh;Lcom/android/calendar/detail/q;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 302
    iget-object v1, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    iget-wide v2, p1, Lcom/android/calendar/dh;->m:J

    iget-wide v4, p1, Lcom/android/calendar/dh;->n:J

    iget-boolean v6, p1, Lcom/android/calendar/dh;->f:Z

    iget-boolean v7, p1, Lcom/android/calendar/dh;->f:Z

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 306
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/detail/o;->g:Lcom/android/calendar/d/g;

    invoke-virtual {v1}, Lcom/android/calendar/d/g;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 308
    iget-object v1, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    iget-wide v2, p1, Lcom/android/calendar/dh;->m:J

    iget-wide v4, p1, Lcom/android/calendar/dh;->n:J

    iget-boolean v6, p1, Lcom/android/calendar/dh;->f:Z

    iget-boolean v7, p1, Lcom/android/calendar/dh;->f:Z

    iget-boolean v9, p1, Lcom/android/calendar/dh;->M:Z

    invoke-static/range {v1 .. v9}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 313
    :cond_1
    iget-object v1, p2, Lcom/android/calendar/detail/q;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/detail/o;->j:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 120
    iput v0, p0, Lcom/android/calendar/detail/o;->f:I

    .line 122
    iget-object v1, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 123
    :goto_0
    if-ge v1, v2, :cond_1

    .line 124
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    invoke-static {v0}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;)Z

    move-result v0

    .line 125
    if-eqz v0, :cond_0

    .line 126
    iget v0, p0, Lcom/android/calendar/detail/o;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/detail/o;->f:I

    .line 123
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 129
    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/detail/o;->h:Landroid/view/LayoutInflater;

    .line 162
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/o;->b:Landroid/content/res/Resources;

    .line 163
    iget-object v0, p0, Lcom/android/calendar/detail/o;->b:Landroid/content/res/Resources;

    const v1, 0x7f0c0100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/o;->i:I

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    .line 165
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(I)V
    .locals 6

    .prologue
    .line 337
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 338
    if-eqz v0, :cond_0

    .line 339
    iget-object v1, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    iget-wide v2, v0, Lcom/android/calendar/dh;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, v0, Lcom/android/calendar/dh;->b:J

    invoke-virtual {p0, v4, v5}, Lcom/android/calendar/detail/o;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    :cond_0
    return-void

    .line 339
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JZ)V
    .locals 3

    .prologue
    .line 344
    iget-object v0, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    return-void
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 4

    .prologue
    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 76
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 77
    iget v3, v0, Lcom/android/calendar/dh;->i:I

    if-gt v3, p2, :cond_0

    iget v3, v0, Lcom/android/calendar/dh;->j:I

    if-lt v3, p2, :cond_0

    .line 81
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_1
    iput-object v1, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    .line 86
    invoke-direct {p0}, Lcom/android/calendar/detail/o;->e()V

    .line 87
    invoke-direct {p0}, Lcom/android/calendar/detail/o;->f()V

    .line 88
    invoke-virtual {p0}, Lcom/android/calendar/detail/o;->notifyDataSetChanged()V

    .line 89
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 91
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/o;->a(Z)V

    .line 92
    return-void

    .line 91
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected a(Z)V
    .locals 9

    .prologue
    const v8, 0x7f020132

    const/16 v4, 0x8

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 132
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    const v1, 0x7f1200dc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 137
    if-eqz v0, :cond_3

    .line 138
    const v1, 0x7f1200dd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 139
    const v1, 0x7f1200de

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 141
    if-eqz p1, :cond_4

    move v2, v3

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 143
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 144
    iget v6, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v7, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v2, v6, v3, v7, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 145
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 148
    const v2, 0x7f0f02d2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 150
    iget-object v1, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v5, :cond_5

    move v1, v5

    .line 151
    :goto_2
    if-eqz v1, :cond_2

    :cond_2
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    const v1, 0x7f1200db

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 155
    if-eqz v0, :cond_0

    .line 156
    if-eqz p1, :cond_6

    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 141
    goto :goto_1

    :cond_5
    move v1, v3

    .line 150
    goto :goto_2

    :cond_6
    move v4, v3

    .line 156
    goto :goto_3
.end method

.method public a(J)Z
    .locals 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 329
    if-nez v0, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 332
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/android/calendar/detail/o;->f:I

    return v0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 348
    iput-boolean p1, p0, Lcom/android/calendar/detail/o;->e:Z

    .line 349
    invoke-virtual {p0}, Lcom/android/calendar/detail/o;->notifyDataSetChanged()V

    .line 350
    return-void
.end method

.method public c()I
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/android/calendar/detail/o;->d()Ljava/util/HashMap;

    move-result-object v1

    .line 169
    const/4 v0, 0x0

    .line 170
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 171
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 174
    goto :goto_0

    .line 175
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method protected c(Z)V
    .locals 6

    .prologue
    .line 353
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 354
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 355
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 356
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    invoke-static {v0}, Lcom/android/calendar/dh;->a(Lcom/android/calendar/dh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v3, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    iget-wide v4, v0, Lcom/android/calendar/dh;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 362
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/o;->notifyDataSetChanged()V

    .line 363
    iget-object v0, p0, Lcom/android/calendar/detail/o;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 364
    return-void
.end method

.method public d()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/android/calendar/detail/o;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    iget-object v1, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 184
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 189
    invoke-virtual {p0}, Lcom/android/calendar/detail/o;->getCount()I

    move-result v1

    .line 190
    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 193
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 194
    iget-object v0, p0, Lcom/android/calendar/detail/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 202
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 207
    .line 210
    if-nez p2, :cond_0

    .line 211
    iget-object v0, p0, Lcom/android/calendar/detail/o;->h:Landroid/view/LayoutInflater;

    const v1, 0x7f040039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 213
    new-instance v1, Lcom/android/calendar/detail/q;

    invoke-direct {v1}, Lcom/android/calendar/detail/q;-><init>()V

    .line 214
    const v0, 0x7f12002d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/detail/q;->a:Landroid/widget/TextView;

    .line 215
    const v0, 0x7f12002a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/detail/q;->b:Landroid/widget/TextView;

    .line 216
    const v0, 0x7f12003c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/detail/q;->c:Landroid/widget/TextView;

    .line 217
    const v0, 0x7f1200d9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/android/calendar/detail/q;->d:Landroid/widget/ImageView;

    .line 218
    iget-object v0, v1, Lcom/android/calendar/detail/q;->a:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 219
    const v0, 0x7f1200d8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    .line 220
    const v0, 0x7f1200d7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/detail/q;->f:Landroid/view/View;

    .line 221
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 227
    :goto_0
    iget-object v1, v0, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setId(I)V

    .line 229
    invoke-direct {p0, p1, p2, v0}, Lcom/android/calendar/detail/o;->a(ILandroid/view/View;Lcom/android/calendar/detail/q;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 224
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/q;

    goto :goto_0
.end method
