.class public Lcom/android/calendar/detail/r;
.super Landroid/app/Fragment;
.source "EasyModeListFragment.java"


# static fields
.field private static h:J


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ListView;

.field private d:Z

.field private e:Lcom/android/calendar/detail/o;

.field private f:J

.field private g:J

.field private i:Lcom/android/calendar/detail/v;

.field private j:Ljava/util/ArrayList;

.field private k:Landroid/widget/AdapterView$OnItemClickListener;

.field private l:Landroid/widget/AdapterView$OnItemLongClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/calendar/detail/r;->h:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    .line 43
    iput-object v1, p0, Lcom/android/calendar/detail/r;->b:Landroid/view/View;

    .line 44
    iput-object v1, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/r;->d:Z

    .line 47
    iput-object v1, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    .line 48
    iput-wide v2, p0, Lcom/android/calendar/detail/r;->f:J

    .line 49
    iput-wide v2, p0, Lcom/android/calendar/detail/r;->g:J

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->j:Ljava/util/ArrayList;

    .line 209
    new-instance v0, Lcom/android/calendar/detail/t;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/t;-><init>(Lcom/android/calendar/detail/r;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->k:Landroid/widget/AdapterView$OnItemClickListener;

    .line 228
    new-instance v0, Lcom/android/calendar/detail/u;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/u;-><init>(Lcom/android/calendar/detail/r;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->l:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 83
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    .line 43
    iput-object v1, p0, Lcom/android/calendar/detail/r;->b:Landroid/view/View;

    .line 44
    iput-object v1, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/r;->d:Z

    .line 47
    iput-object v1, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    .line 48
    iput-wide v2, p0, Lcom/android/calendar/detail/r;->f:J

    .line 49
    iput-wide v2, p0, Lcom/android/calendar/detail/r;->g:J

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->j:Ljava/util/ArrayList;

    .line 209
    new-instance v0, Lcom/android/calendar/detail/t;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/t;-><init>(Lcom/android/calendar/detail/r;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->k:Landroid/widget/AdapterView$OnItemClickListener;

    .line 228
    new-instance v0, Lcom/android/calendar/detail/u;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/u;-><init>(Lcom/android/calendar/detail/r;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->l:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 87
    iput-wide p1, p0, Lcom/android/calendar/detail/r;->f:J

    .line 88
    iput-wide p3, p0, Lcom/android/calendar/detail/r;->g:J

    .line 89
    return-void
.end method

.method static synthetic a(J)J
    .locals 0

    .prologue
    .line 34
    sput-wide p0, Lcom/android/calendar/detail/r;->h:J

    return-wide p0
.end method

.method static synthetic a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/r;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/android/calendar/detail/r;->f:J

    return-wide v0
.end method

.method static synthetic c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    return-object v0
.end method

.method public static d()J
    .locals 2

    .prologue
    .line 206
    sget-wide v0, Lcom/android/calendar/detail/r;->h:J

    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/calendar/detail/r;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/detail/r;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/android/calendar/detail/r;->d:Z

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->d()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 163
    return-void
.end method

.method protected a(Z)V
    .locals 1

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/android/calendar/detail/r;->d:Z

    .line 167
    iget-object v0, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    invoke-virtual {v0, p1}, Lcom/android/calendar/detail/o;->b(Z)V

    .line 168
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 189
    const-string v5, "visible=1"

    .line 190
    iget-object v0, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 194
    :cond_0
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 195
    iget-wide v6, p0, Lcom/android/calendar/detail/r;->f:J

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 196
    iget-wide v6, p0, Lcom/android/calendar/detail/r;->g:J

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 197
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 198
    iget-object v0, p0, Lcom/android/calendar/detail/r;->i:Lcom/android/calendar/detail/v;

    const/4 v1, 0x0

    invoke-static {}, Lcom/android/calendar/dh;->j()[Ljava/lang/String;

    move-result-object v4

    const-string v7, "startDay,startMinute,title"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/v;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public c()Lcom/android/calendar/detail/o;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 182
    if-eqz p1, :cond_0

    .line 183
    const-string v0, "startMillis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/r;->f:J

    .line 184
    const-string v0, "endMillis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/r;->g:J

    .line 186
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 94
    iput-object p1, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    .line 95
    new-instance v0, Lcom/android/calendar/detail/v;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/detail/v;-><init>(Lcom/android/calendar/detail/r;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->i:Lcom/android/calendar/detail/v;

    .line 96
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 100
    const v0, 0x7f04003a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/r;->b:Landroid/view/View;

    .line 101
    iget-object v0, p0, Lcom/android/calendar/detail/r;->b:Landroid/view/View;

    const v1, 0x7f1200db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    .line 102
    new-instance v0, Lcom/android/calendar/detail/o;

    iget-object v1, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/detail/o;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    .line 103
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/detail/r;->e:Lcom/android/calendar/detail/o;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 104
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 105
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 106
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 108
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/detail/r;->k:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/detail/r;->l:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 110
    iget-object v0, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 112
    iget-object v0, p0, Lcom/android/calendar/detail/r;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/detail/s;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/s;-><init>(Lcom/android/calendar/detail/r;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/r;->b:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 155
    invoke-virtual {p0}, Lcom/android/calendar/detail/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/android/calendar/detail/r;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 157
    invoke-virtual {p0}, Lcom/android/calendar/detail/r;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 159
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 174
    const-string v0, "startMillis"

    iget-wide v2, p0, Lcom/android/calendar/detail/r;->f:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 175
    const-string v0, "endMillis"

    iget-wide v2, p0, Lcom/android/calendar/detail/r;->g:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 176
    return-void
.end method
