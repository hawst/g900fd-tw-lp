.class Lcom/android/calendar/detail/al;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 2164
    iput-object p1, p0, Lcom/android/calendar/detail/al;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2167
    iget-object v0, p0, Lcom/android/calendar/detail/al;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/calendar/detail/al;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->u(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$RawContacts;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 2169
    if-nez v0, :cond_0

    .line 2170
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Photo uri is null Contact id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/detail/al;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->u(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2175
    :goto_0
    return-void

    .line 2174
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/detail/al;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v1}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, p1, v0, v2, v3}, Landroid/provider/ContactsContract$QuickContact;->showQuickContact(Landroid/content/Context;Landroid/view/View;Landroid/net/Uri;I[Ljava/lang/String;)V

    goto :goto_0
.end method
