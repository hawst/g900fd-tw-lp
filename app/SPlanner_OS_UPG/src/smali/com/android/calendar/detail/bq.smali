.class public Lcom/android/calendar/detail/bq;
.super Lcom/android/calendar/detail/a;
.source "MeetingInvitationFragment.java"


# static fields
.field private static X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

.field private static final Y:Landroid/util/SparseArray;

.field private static Z:I

.field public static final d:Ljava/lang/String;


# instance fields
.field protected A:I

.field protected B:Lcom/android/calendar/detail/w;

.field protected C:I

.field protected D:Ljava/util/ArrayList;

.field protected E:I

.field private F:[Ljava/lang/String;

.field private final G:I

.field private final H:I

.field private final I:I

.field private final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private P:Landroid/app/DialogFragment;

.field private Q:Landroid/view/View;

.field private R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

.field private S:Landroid/app/Activity;

.field private final T:Ljava/util/ArrayList;

.field private U:Landroid/os/Handler;

.field private V:Lcom/android/calendar/detail/cc;

.field private W:Lcom/android/calendar/detail/cb;

.field private aa:Z

.field private ab:Z

.field private final ac:Lcom/android/calendar/detail/cp;

.field private final ad:Landroid/view/View$OnClickListener;

.field private ae:Landroid/view/View$OnClickListener;

.field private final af:Landroid/view/View$OnClickListener;

.field private ag:Landroid/app/ProgressDialog;

.field private final ah:Landroid/os/Handler;

.field private final ai:Landroid/content/BroadcastReceiver;

.field private aj:Landroid/content/ServiceConnection;

.field private ak:Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;

.field protected e:Z

.field protected f:J

.field protected g:J

.field protected h:J

.field protected s:Ljava/lang/String;

.field protected t:Ljava/lang/String;

.field protected u:Ljava/lang/String;

.field protected v:Z

.field protected w:Z

.field protected x:Z

.field protected y:Ljava/lang/String;

.field protected z:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    .line 122
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    .line 123
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/calendar/detail/bq;->Y:Landroid/util/SparseArray;

    .line 124
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/detail/bq;->Z:I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 141
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 70
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "account_type"

    aput-object v1, v0, v6

    const-string v1, "organizer"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "availability"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "availabilityStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "rdate"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->F:[Ljava/lang/String;

    .line 83
    iput v5, p0, Lcom/android/calendar/detail/bq;->G:I

    .line 84
    iput v6, p0, Lcom/android/calendar/detail/bq;->H:I

    .line 85
    iput v7, p0, Lcom/android/calendar/detail/bq;->I:I

    .line 86
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/detail/bq;->J:I

    .line 87
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/calendar/detail/bq;->K:I

    .line 88
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/calendar/detail/bq;->L:I

    .line 89
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/detail/bq;->M:I

    .line 90
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/calendar/detail/bq;->N:I

    .line 91
    const/16 v0, 0x9

    iput v0, p0, Lcom/android/calendar/detail/bq;->O:I

    .line 104
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->z:J

    .line 112
    iput-object v3, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    .line 113
    iput-object v3, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    .line 114
    iput-object v3, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->U:Landroid/os/Handler;

    .line 119
    iput-object v3, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    .line 138
    iput-boolean v4, p0, Lcom/android/calendar/detail/bq;->ab:Z

    .line 207
    new-instance v0, Lcom/android/calendar/detail/br;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/br;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ac:Lcom/android/calendar/detail/cp;

    .line 227
    new-instance v0, Lcom/android/calendar/detail/bs;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bs;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ad:Landroid/view/View$OnClickListener;

    .line 238
    new-instance v0, Lcom/android/calendar/detail/bt;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bt;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ae:Landroid/view/View$OnClickListener;

    .line 343
    new-instance v0, Lcom/android/calendar/detail/bu;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bu;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->af:Landroid/view/View$OnClickListener;

    .line 781
    iput-object v3, p0, Lcom/android/calendar/detail/bq;->ag:Landroid/app/ProgressDialog;

    .line 785
    new-instance v0, Lcom/android/calendar/detail/bw;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bw;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ah:Landroid/os/Handler;

    .line 812
    new-instance v0, Lcom/android/calendar/detail/bx;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bx;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ai:Landroid/content/BroadcastReceiver;

    .line 856
    new-instance v0, Lcom/android/calendar/detail/by;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/by;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->aj:Landroid/content/ServiceConnection;

    .line 872
    new-instance v0, Lcom/android/calendar/detail/bz;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bz;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ak:Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JJJZ)V
    .locals 4

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 70
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "availability"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "availabilityStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "rdate"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->F:[Ljava/lang/String;

    .line 83
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/detail/bq;->G:I

    .line 84
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/detail/bq;->H:I

    .line 85
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/detail/bq;->I:I

    .line 86
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/detail/bq;->J:I

    .line 87
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/calendar/detail/bq;->K:I

    .line 88
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/calendar/detail/bq;->L:I

    .line 89
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/detail/bq;->M:I

    .line 90
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/calendar/detail/bq;->N:I

    .line 91
    const/16 v0, 0x9

    iput v0, p0, Lcom/android/calendar/detail/bq;->O:I

    .line 104
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->z:J

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->U:Landroid/os/Handler;

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/bq;->ab:Z

    .line 207
    new-instance v0, Lcom/android/calendar/detail/br;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/br;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ac:Lcom/android/calendar/detail/cp;

    .line 227
    new-instance v0, Lcom/android/calendar/detail/bs;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bs;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ad:Landroid/view/View$OnClickListener;

    .line 238
    new-instance v0, Lcom/android/calendar/detail/bt;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bt;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ae:Landroid/view/View$OnClickListener;

    .line 343
    new-instance v0, Lcom/android/calendar/detail/bu;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bu;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->af:Landroid/view/View$OnClickListener;

    .line 781
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ag:Landroid/app/ProgressDialog;

    .line 785
    new-instance v0, Lcom/android/calendar/detail/bw;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bw;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ah:Landroid/os/Handler;

    .line 812
    new-instance v0, Lcom/android/calendar/detail/bx;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bx;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ai:Landroid/content/BroadcastReceiver;

    .line 856
    new-instance v0, Lcom/android/calendar/detail/by;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/by;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->aj:Landroid/content/ServiceConnection;

    .line 872
    new-instance v0, Lcom/android/calendar/detail/bz;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bz;-><init>(Lcom/android/calendar/detail/bq;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->ak:Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;

    .line 146
    iput-wide p2, p0, Lcom/android/calendar/detail/bq;->f:J

    .line 147
    iput-wide p4, p0, Lcom/android/calendar/detail/bq;->g:J

    .line 148
    iput-wide p6, p0, Lcom/android/calendar/detail/bq;->h:J

    .line 149
    iput-boolean p8, p0, Lcom/android/calendar/detail/bq;->e:Z

    .line 150
    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 66
    sput p0, Lcom/android/calendar/detail/bq;->Z:I

    return p0
.end method

.method static synthetic a(Lcom/android/calendar/detail/bq;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/calendar/detail/bq;->ag:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)Lcom/android/calendar/at;
    .locals 1

    .prologue
    .line 529
    new-instance v0, Lcom/android/calendar/at;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method static synthetic a()Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;)Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;
    .locals 0

    .prologue
    .line 66
    sput-object p0, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    return-object p0
.end method

.method public static a(JJIZLandroid/app/Activity;Lcom/android/calendar/detail/cb;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 706
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 707
    const-string v1, "originalInstanceTime"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 708
    const-string v1, "selfAttendeeStatus"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 710
    if-nez p5, :cond_0

    .line 711
    const-string v1, "eventStatus"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 714
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 715
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_EXCEPTION_URI:Landroid/net/Uri;

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 717
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    if-nez p7, :cond_1

    .line 721
    new-instance v1, Lcom/android/calendar/ag;

    invoke-direct {v1, p6}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 722
    const-string v4, "com.android.calendar"

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 727
    :goto_0
    return-void

    .line 725
    :cond_1
    const-string v4, "com.android.calendar"

    move-object v1, p7

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/detail/cb;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;JJILjava/lang/String;Lcom/android/calendar/detail/cb;)V
    .locals 13

    .prologue
    .line 733
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 735
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 736
    const-string v2, "attendeeEmail"

    move-object/from16 v0, p6

    invoke-virtual {v7, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    :cond_0
    const-string v2, "attendeeStatus"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 739
    const-string v2, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 741
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p3

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 743
    if-nez p7, :cond_1

    .line 745
    new-instance v3, Lcom/android/calendar/ag;

    invoke-direct {v3, p0}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 746
    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 753
    :goto_0
    return-void

    .line 750
    :cond_1
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v3, p7

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/detail/cb;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/widget/QuickContactBadge;Lcom/android/calendar/at;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 688
    sget-object v0, Lcom/android/calendar/g/f;->a:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 689
    invoke-virtual {p1, v2}, Landroid/widget/QuickContactBadge;->setClipToOutline(Z)V

    .line 690
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 691
    iget-object v0, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Landroid/widget/QuickContactBadge;->assignContactFromPhone(Ljava/lang/String;Z)V

    .line 701
    :goto_0
    return-void

    .line 693
    :cond_0
    const-string v0, "My calendar"

    iget-object v1, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 694
    const v0, 0x7f0f02ba

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_0

    .line 696
    :cond_1
    iget-object v0, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_0

    .line 699
    :cond_2
    iget-object v0, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 9

    .prologue
    .line 459
    new-instance v1, Lcom/android/calendar/detail/ck;

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->g:J

    iget-wide v6, p0, Lcom/android/calendar/detail/bq;->h:J

    iget-boolean v8, p0, Lcom/android/calendar/detail/bq;->v:Z

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/android/calendar/detail/ck;-><init>(Landroid/content/Intent;Landroid/net/Uri;JJZ)V

    iput-object v1, p0, Lcom/android/calendar/detail/bq;->P:Landroid/app/DialogFragment;

    .line 460
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->P:Landroid/app/DialogFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/detail/ck;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 461
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 480
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 483
    const-string v0, ""

    .line 484
    const-string v0, ""

    .line 486
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 487
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 488
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 489
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 490
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 492
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 496
    iget-object v2, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-static {v2, v1}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 498
    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->z:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 500
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v4, v3

    iput-wide v4, p0, Lcom/android/calendar/detail/bq;->z:J

    .line 501
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/android/calendar/detail/bq;->A:I

    .line 503
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-static {v2, v1, v0}, Lcom/android/calendar/detail/bq;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/android/calendar/at;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 505
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/bq;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/bq;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/bq;Z[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/detail/bq;->a(Z[Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/android/calendar/event/aw;)V
    .locals 4

    .prologue
    .line 841
    iget-object v0, p0, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    .line 842
    sget-object v1, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    if-eqz v1, :cond_1

    .line 844
    :try_start_0
    sget-object v1, Lcom/android/calendar/detail/bq;->Y:Landroid/util/SparseArray;

    monitor-enter v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 845
    :try_start_1
    sget-object v2, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    sget v3, Lcom/android/calendar/detail/bq;->Z:I

    iget-object v0, v0, Lcom/android/calendar/at;->f:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;->getImage(ILjava/lang/String;)I

    move-result v0

    .line 846
    if-ltz v0, :cond_0

    .line 847
    sget-object v2, Lcom/android/calendar/detail/bq;->Y:Landroid/util/SparseArray;

    invoke-virtual {v2, v0, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 849
    :cond_0
    monitor-exit v1

    .line 854
    :cond_1
    :goto_0
    return-void

    .line 849
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 850
    :catch_0
    move-exception v0

    .line 851
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Z[Ljava/lang/String;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 408
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v0, :cond_1

    .line 409
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 410
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.email.intent.action.CALENDAR_MEETING_FORWARD"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 411
    const-string v2, "account_email"

    iget-object v3, p0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    const-string v2, "event_id"

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 414
    iget-boolean v2, p0, Lcom/android/calendar/detail/bq;->x:Z

    if-eqz v2, :cond_0

    .line 415
    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/bq;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 456
    :goto_0
    return-void

    .line 419
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/bq;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    sget-object v0, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    const-string v1, "Error: Could not find com.android.email.intent.action.CALENDAR_MEETING_FORWARD activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 424
    :cond_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 425
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->s:Ljava/lang/String;

    .line 426
    if-eqz p1, :cond_2

    const v0, 0x7f0f0381

    .line 427
    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/bq;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 428
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 429
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 433
    :goto_2
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 434
    if-eqz p1, :cond_4

    .line 435
    const-string v0, "android.intent.action.SENDTO"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v0, "plain/text"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mailto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 449
    :goto_3
    const-string v0, "theme"

    const/4 v1, 0x2

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 451
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHOOSER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 452
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 453
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 454
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-virtual {v0, v3, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    .line 426
    :cond_2
    const v0, 0x7f0f01fd

    goto :goto_1

    .line 431
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/bq;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 439
    :cond_4
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v6, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 441
    iget-boolean v0, p0, Lcom/android/calendar/detail/bq;->x:Z

    if-eqz v0, :cond_5

    .line 442
    invoke-direct {p0, v8, v2}, Lcom/android/calendar/detail/bq;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 445
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-wide v6, v4

    invoke-static/range {v1 .. v7}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;ZJJ)Landroid/net/Uri;

    move-result-object v0

    .line 446
    const-string v1, "text/x-vCalendar"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_3
.end method

.method static synthetic a(Lcom/android/calendar/detail/bq;)Z
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/detail/bq;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/calendar/detail/bq;->aa:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 584
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/android/calendar/detail/bq;->Y:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/bq;)Lcom/android/calendar/detail/cp;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ac:Lcom/android/calendar/detail/cp;

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 508
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 526
    :cond_0
    return-void

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 513
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 514
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 515
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 516
    if-eqz v2, :cond_3

    iget-object v3, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 517
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/at;->a:Ljava/lang/String;

    .line 518
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/at;->d:Ljava/lang/String;

    .line 519
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/at;->e:Ljava/lang/String;

    .line 520
    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/at;->f:Ljava/lang/String;

    .line 521
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/calendar/at;->g:Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/detail/bq;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/bq;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/detail/bq;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/calendar/detail/bq;->ab:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/detail/bq;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    return-object v0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 200
    const/4 v0, 0x0

    .line 201
    iget-boolean v1, p0, Lcom/android/calendar/detail/bq;->v:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const/4 v0, 0x1

    .line 204
    :cond_0
    return v0
.end method

.method private d()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 256
    iget v6, p0, Lcom/android/calendar/detail/bq;->C:I

    .line 258
    if-nez v6, :cond_1

    .line 298
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 263
    :cond_1
    iget v1, p0, Lcom/android/calendar/detail/bq;->A:I

    if-ne v6, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/calendar/detail/bq;->aa:Z

    if-eqz v1, :cond_0

    .line 268
    :cond_2
    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->z:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 272
    iget v1, p0, Lcom/android/calendar/detail/bq;->C:I

    iput v1, p0, Lcom/android/calendar/detail/bq;->A:I

    .line 274
    iget-boolean v1, p0, Lcom/android/calendar/detail/bq;->x:Z

    if-nez v1, :cond_3

    .line 276
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->z:J

    iget-object v7, p0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/calendar/detail/bq;->W:Lcom/android/calendar/detail/cb;

    invoke-static/range {v1 .. v8}, Lcom/android/calendar/detail/bq;->a(Landroid/app/Activity;JJILjava/lang/String;Lcom/android/calendar/detail/cb;)V

    move v0, v10

    .line 277
    goto :goto_0

    .line 281
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->B:Lcom/android/calendar/detail/w;

    invoke-virtual {v1}, Lcom/android/calendar/detail/w;->a()I

    move-result v1

    .line 282
    packed-switch v1, :pswitch_data_0

    .line 292
    sget-object v1, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    const-string v2, "Unexpected choice for updating invitation response"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iput-boolean v0, p0, Lcom/android/calendar/detail/bq;->aa:Z

    goto :goto_0

    .line 286
    :pswitch_1
    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->g:J

    iget-boolean v7, p0, Lcom/android/calendar/detail/bq;->v:Z

    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v8

    iget-object v9, p0, Lcom/android/calendar/detail/bq;->W:Lcom/android/calendar/detail/cb;

    invoke-static/range {v2 .. v9}, Lcom/android/calendar/detail/bq;->a(JJIZLandroid/app/Activity;Lcom/android/calendar/detail/cb;)V

    move v0, v10

    .line 287
    goto :goto_0

    .line 289
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->z:J

    iget-object v7, p0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    iget-object v8, p0, Lcom/android/calendar/detail/bq;->W:Lcom/android/calendar/detail/cb;

    invoke-static/range {v1 .. v8}, Lcom/android/calendar/detail/bq;->a(Landroid/app/Activity;JJILjava/lang/String;Lcom/android/calendar/detail/cb;)V

    move v0, v10

    .line 290
    goto :goto_0

    .line 282
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic d(Lcom/android/calendar/detail/bq;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/calendar/detail/bq;->ab:Z

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/detail/bq;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->U:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    iput v2, p0, Lcom/android/calendar/detail/bq;->A:I

    .line 303
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->z:J

    .line 304
    iput v2, p0, Lcom/android/calendar/detail/bq;->C:I

    .line 305
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 379
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 381
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/bq;->a(Z[Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->f()V

    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 385
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 387
    const-string v0, ""

    .line 388
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 389
    iget-object v3, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 390
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 393
    invoke-static {v3}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 396
    iget-boolean v4, p0, Lcom/android/calendar/detail/bq;->w:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 399
    :cond_1
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 402
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 403
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/bq;->a(Z[Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->g()V

    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    .line 464
    const-string v0, "event_id=?"

    .line 465
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 466
    const-string v7, "attendeeName ASC, attendeeEmail ASC"

    .line 468
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/bq;->i:[Ljava/lang/String;

    const-string v5, "event_id=?"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/cc;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    return-void
.end method

.method static synthetic h(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->h()V

    return-void
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 473
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ah:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 475
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 476
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/android/calendar/detail/bq;->F:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/cc;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/detail/bq;)Z
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->l()Z

    move-result v0

    return v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f01d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 538
    iget-boolean v1, p0, Lcom/android/calendar/detail/bq;->e:Z

    if-eqz v1, :cond_1

    .line 539
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 544
    if-eqz v1, :cond_0

    .line 545
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->k()V

    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 550
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 551
    iget-boolean v2, p0, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/android/calendar/detail/bq;->w:Z

    if-eqz v2, :cond_2

    if-ne v1, v3, :cond_2

    .line 557
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a(Ljava/util/ArrayList;)V

    .line 560
    :cond_1
    return-void

    .line 553
    :cond_2
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/android/calendar/detail/bq;->w:Z

    if-eqz v2, :cond_3

    if-eq v1, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->j()V

    return-void
.end method

.method static synthetic l(Lcom/android/calendar/detail/bq;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ah:Landroid/os/Handler;

    return-object v0
.end method

.method private l()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 563
    const-string v0, "com.sec.android.app.sns3.linkedin"

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/bq;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 579
    :goto_0
    return v0

    .line 567
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 568
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 569
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 572
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 573
    goto :goto_0

    .line 576
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 577
    const-string v1, "emailList"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 578
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    const-string v2, "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 579
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic m(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->i()V

    return-void
.end method

.method private m()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 830
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 832
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 833
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 834
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    aput-object v0, v2, v1

    .line 833
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 837
    :cond_0
    return-object v2
.end method

.method static synthetic n(Lcom/android/calendar/detail/bq;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ag:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/detail/bq;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->T:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic p(Lcom/android/calendar/detail/bq;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->m()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/android/calendar/detail/bq;)Lcom/android/calendar/detail/cc;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    return-object v0
.end method

.method static synthetic r(Lcom/android/calendar/detail/bq;)Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ak:Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;

    return-object v0
.end method

.method static synthetic s(Lcom/android/calendar/detail/bq;)Lcom/android/calendar/detail/MeetingInvitationAttendeesView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 309
    iput-object p1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    .line 310
    new-instance v0, Lcom/android/calendar/detail/cc;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/cc;-><init>(Lcom/android/calendar/detail/bq;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->V:Lcom/android/calendar/detail/cc;

    .line 311
    new-instance v0, Lcom/android/calendar/detail/cb;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/cb;-><init>(Lcom/android/calendar/detail/bq;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->W:Lcom/android/calendar/detail/cb;

    .line 312
    new-instance v0, Lcom/android/calendar/detail/w;

    invoke-direct {v0, p1}, Lcom/android/calendar/detail/w;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->B:Lcom/android/calendar/detail/w;

    .line 313
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->B:Lcom/android/calendar/detail/w;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/w;->a(I)V

    .line 315
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onAttach(Landroid/app/Activity;)V

    .line 316
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onCreate(Landroid/os/Bundle;)V

    .line 156
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->e()V

    .line 157
    if-eqz p1, :cond_0

    .line 158
    const-string v0, "key_event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->f:J

    .line 159
    const-string v0, "key_start_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->g:J

    .line 160
    const-string v0, "key_end_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/bq;->h:J

    .line 161
    const-string v0, "key_fragment_is_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/bq;->e:Z

    .line 163
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.snsimagecache"

    const-string v3, "com.sec.android.app.snsimagecache.SnsImageCacheService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 165
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/detail/bq;->aj:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 166
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 321
    const v0, 0x7f04006e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    .line 322
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    const v1, 0x7f1200fb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    iput-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    .line 323
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a(Lcom/android/calendar/detail/bq;Landroid/app/Activity;)V

    .line 324
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->ad:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->setResponseClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->R:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->ae:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->setStatusClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    const v1, 0x7f1201c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 328
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    const v2, 0x7f1201c4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 329
    iget-object v2, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    const v3, 0x7f1201c6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 330
    iget-object v3, p0, Lcom/android/calendar/detail/bq;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 335
    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/detail/bq;->ai:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 338
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->i()V

    .line 340
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->Q:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 589
    sget-object v0, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    if-eqz v0, :cond_0

    .line 591
    :try_start_0
    sget-object v0, Lcom/android/calendar/detail/bq;->X:Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->ak:Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;

    invoke-interface {v0, v1}, Lcom/sec/android/app/snsimagecache/ISnsImageCacheService;->unregisterCallback(Lcom/sec/android/app/snsimagecache/ISnsImageCacheCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    :goto_0
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/detail/bq;->Z:I

    .line 597
    :cond_0
    sget-object v0, Lcom/android/calendar/detail/bq;->Y:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 598
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->aj:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 599
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroy()V

    .line 600
    return-void

    .line 592
    :catch_0
    move-exception v0

    .line 593
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/android/calendar/detail/bq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/android/calendar/detail/bq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f039b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->S:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/detail/bq;->ai:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 194
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ah:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 196
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroyView()V

    .line 197
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onResume()V

    .line 181
    iget-boolean v0, p0, Lcom/android/calendar/detail/bq;->aa:Z

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/android/calendar/detail/bq;->ac:Lcom/android/calendar/detail/cp;

    iget v1, p0, Lcom/android/calendar/detail/bq;->C:I

    invoke-interface {v0, v1}, Lcom/android/calendar/detail/cp;->a(I)V

    .line 184
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 170
    const-string v0, "key_event_id"

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->f:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 171
    const-string v0, "key_start_millis"

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->g:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 172
    const-string v0, "key_end_millis"

    iget-wide v2, p0, Lcom/android/calendar/detail/bq;->h:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 173
    const-string v0, "key_fragment_is_dialog"

    iget-boolean v1, p0, Lcom/android/calendar/detail/bq;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 175
    return-void
.end method
