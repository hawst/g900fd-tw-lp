.class public Lcom/android/calendar/detail/f;
.super Landroid/app/DialogFragment;
.source "EASRepeatEventDialog.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:J

.field private c:Z

.field private d:I

.field private e:I

.field private final f:Ljava/lang/String;

.field private g:Lcom/android/calendar/detail/z;

.field private h:Lcom/android/calendar/detail/w;

.field private i:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/android/calendar/detail/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;ILjava/lang/String;Lcom/android/calendar/detail/z;Lcom/android/calendar/detail/w;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 31
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 71
    new-instance v0, Lcom/android/calendar/detail/g;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/g;-><init>(Lcom/android/calendar/detail/f;)V

    iput-object v0, p0, Lcom/android/calendar/detail/f;->i:Landroid/content/DialogInterface$OnClickListener;

    .line 32
    iput-wide p1, p0, Lcom/android/calendar/detail/f;->b:J

    .line 33
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/detail/f;->c:Z

    .line 34
    iput p4, p0, Lcom/android/calendar/detail/f;->d:I

    .line 35
    iput-object p5, p0, Lcom/android/calendar/detail/f;->f:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Lcom/android/calendar/detail/f;->g:Lcom/android/calendar/detail/z;

    .line 37
    iput-object p7, p0, Lcom/android/calendar/detail/f;->h:Lcom/android/calendar/detail/w;

    .line 39
    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/f;->setRetainInstance(Z)V

    .line 40
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/detail/f;)Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/android/calendar/detail/f;->c:Z

    return v0
.end method

.method private a()[Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 57
    .line 58
    const/4 v0, 0x0

    .line 60
    iget-boolean v2, p0, Lcom/android/calendar/detail/f;->c:Z

    if-nez v2, :cond_0

    .line 61
    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0f0048

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v0

    .line 68
    return-object v1

    .line 63
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    .line 64
    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0f02e6

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v0

    move v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/detail/f;)Lcom/android/calendar/detail/w;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/android/calendar/detail/f;->h:Lcom/android/calendar/detail/w;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    .line 103
    new-instance v1, Lcom/android/calendar/detail/h;

    iget-wide v2, p0, Lcom/android/calendar/detail/f;->b:J

    iget v4, p0, Lcom/android/calendar/detail/f;->d:I

    iget-object v5, p0, Lcom/android/calendar/detail/f;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/f;->g:Lcom/android/calendar/detail/z;

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/detail/h;-><init>(JILjava/lang/String;Lcom/android/calendar/detail/z;)V

    .line 105
    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/detail/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 109
    .line 110
    iget v0, p0, Lcom/android/calendar/detail/f;->d:I

    if-eqz v0, :cond_0

    .line 111
    iget v0, p0, Lcom/android/calendar/detail/f;->d:I

    .line 117
    :goto_0
    iget v1, p0, Lcom/android/calendar/detail/f;->e:I

    if-ne v0, v1, :cond_1

    .line 118
    const/4 v0, 0x0

    .line 123
    :goto_1
    return v0

    .line 113
    :cond_0
    iget v0, p0, Lcom/android/calendar/detail/f;->e:I

    goto :goto_0

    .line 121
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/detail/f;->b()V

    .line 123
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/detail/f;)Z
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/android/calendar/detail/f;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/f;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/android/calendar/detail/f;->b()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    :cond_0
    const/4 v0, 0x0

    .line 53
    :goto_0
    return-object v0

    .line 47
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/detail/f;->a()[Ljava/lang/CharSequence;

    move-result-object v0

    .line 49
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/detail/f;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 50
    const v2, 0x7f0f00af

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 51
    iget-object v2, p0, Lcom/android/calendar/detail/f;->i:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method
