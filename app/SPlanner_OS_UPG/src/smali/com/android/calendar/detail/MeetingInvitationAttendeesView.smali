.class public Lcom/android/calendar/detail/MeetingInvitationAttendeesView;
.super Landroid/widget/LinearLayout;
.source "MeetingInvitationAttendeesView.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Lcom/android/calendar/detail/cd;

.field private e:Lcom/android/calendar/detail/cd;

.field private f:Lcom/android/calendar/detail/cd;

.field private g:Lcom/android/calendar/detail/cd;

.field private h:Lcom/android/calendar/detail/bq;

.field private i:Lcom/android/calendar/detail/ch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Lcom/android/calendar/detail/ch;

    invoke-direct {v0, p1}, Lcom/android/calendar/detail/ch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->i:Lcom/android/calendar/detail/ch;

    .line 48
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a()V

    .line 49
    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    const v1, 0x7f0200b0

    const v0, 0x7f0200af

    .line 121
    const/4 v2, 0x0

    .line 124
    iget-object v3, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-boolean v3, v3, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v3, :cond_0

    .line 125
    packed-switch p1, :pswitch_data_0

    move v0, v2

    .line 154
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :pswitch_2
    const v0, 0x7f0200b1

    .line 134
    goto :goto_0

    .line 136
    :pswitch_3
    const v0, 0x7f0200b2

    goto :goto_0

    .line 144
    :cond_0
    packed-switch p1, :pswitch_data_1

    move v1, v2

    :goto_1
    :pswitch_4
    move v0, v1

    .line 154
    goto :goto_0

    :pswitch_5
    move v1, v0

    .line 150
    goto :goto_1

    .line 125
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 144
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private static b(I)I
    .locals 1

    .prologue
    const v0, 0x7f020086

    .line 229
    .line 231
    packed-switch p0, :pswitch_data_0

    .line 245
    :goto_0
    :pswitch_0
    return v0

    .line 233
    :pswitch_1
    const v0, 0x7f02007e

    .line 234
    goto :goto_0

    .line 236
    :pswitch_2
    const v0, 0x7f020080

    .line 237
    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 158
    .line 159
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 160
    iget-object v3, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-object v4, v4, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v8, v0

    .line 168
    :goto_0
    if-nez v8, :cond_1

    .line 169
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 171
    sget-object v0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a:Ljava/lang/String;

    const-string v1, "Can\'t find host information from the attendee list."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :goto_1
    return-void

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b:Landroid/view/View;

    const v2, 0x7f120222

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0295

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 178
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 179
    iget-object v3, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-boolean v3, v3, Lcom/android/calendar/detail/bq;->w:Z

    if-eqz v3, :cond_4

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f045f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 184
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 185
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    const v2, 0x7f020081

    invoke-virtual {v0, v2, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 189
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v2, 0x7f120225

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 190
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget v3, v2, Lcom/android/calendar/detail/bq;->E:I

    .line 192
    :try_start_0
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-object v2, v2, Lcom/android/calendar/detail/bq;->D:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v2, 0x7f120224

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 199
    invoke-direct {p0, v3}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a(I)I

    move-result v2

    .line 200
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 203
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v2, 0x7f120226

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 204
    iget v2, v8, Lcom/android/calendar/at;->c:I

    invoke-static {v2}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b(I)I

    move-result v2

    .line 205
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget v4, v8, Lcom/android/calendar/at;->c:I

    invoke-static {v3, v4}, Lcom/android/calendar/detail/cd;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 206
    invoke-virtual {v0, v2, v9, v9, v9}, Landroid/widget/Button;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 207
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-boolean v2, v2, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-boolean v2, v2, Lcom/android/calendar/detail/bq;->w:Z

    if-nez v2, :cond_3

    .line 208
    :cond_2
    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 212
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v2, 0x7f120219

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 213
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v8}, Lcom/android/calendar/detail/bq;->a(Landroid/content/Context;Landroid/widget/QuickContactBadge;Lcom/android/calendar/at;)V

    .line 214
    invoke-virtual {v0, v9}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 217
    invoke-static {v8}, Lcom/android/calendar/detail/ch;->a(Lcom/android/calendar/at;)Lcom/android/calendar/event/aw;

    move-result-object v2

    .line 218
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    iput-object v0, v2, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    .line 219
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->i:Lcom/android/calendar/detail/ch;

    invoke-virtual {v0}, Lcom/android/calendar/detail/ch;->a()Lcom/android/calendar/detail/ci;

    move-result-object v0

    sget-object v3, Lcom/android/calendar/detail/ch;->b:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/ch;->a:[Ljava/lang/String;

    const-string v5, "data1 IN (?)"

    new-array v6, v1, [Ljava/lang/String;

    iget-object v8, v8, Lcom/android/calendar/at;->b:Ljava/lang/String;

    aput-object v8, v6, v9

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/ci;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 182
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f01d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 193
    :catch_0
    move-exception v0

    .line 194
    sget-object v0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "index out of bounds : availability : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    iget-object v4, v4, Lcom/android/calendar/detail/bq;->D:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_5
    move-object v8, v7

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 57
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04006f

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b:Landroid/view/View;

    .line 58
    const v1, 0x7f040070

    invoke-static {v0, v1, v4}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    .line 60
    new-instance v1, Lcom/android/calendar/detail/cd;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v2}, Lcom/android/calendar/detail/cd;-><init>(Lcom/android/calendar/detail/MeetingInvitationAttendeesView;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    .line 61
    new-instance v1, Lcom/android/calendar/detail/cd;

    const/4 v2, 0x4

    invoke-direct {v1, p0, v0, v2}, Lcom/android/calendar/detail/cd;-><init>(Lcom/android/calendar/detail/MeetingInvitationAttendeesView;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    .line 62
    new-instance v1, Lcom/android/calendar/detail/cd;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v0, v2}, Lcom/android/calendar/detail/cd;-><init>(Lcom/android/calendar/detail/MeetingInvitationAttendeesView;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    .line 63
    new-instance v1, Lcom/android/calendar/detail/cd;

    invoke-direct {v1, p0, v0, v3}, Lcom/android/calendar/detail/cd;-><init>(Lcom/android/calendar/detail/MeetingInvitationAttendeesView;Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    .line 66
    invoke-virtual {p0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->removeAllViews()V

    .line 68
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 69
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->b()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->addView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0, v3}, Lcom/android/calendar/detail/cd;->a(I)V

    .line 81
    return-void
.end method

.method public a(Lcom/android/calendar/detail/bq;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    .line 42
    return-void
.end method

.method public a(Lcom/android/calendar/event/aw;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->i:Lcom/android/calendar/detail/ch;

    invoke-static {p1}, Lcom/android/calendar/detail/ch;->a(Lcom/android/calendar/event/aw;)V

    .line 118
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->d()V

    .line 85
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->d()V

    .line 86
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->d()V

    .line 87
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->d()V

    .line 90
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 92
    iget v2, v0, Lcom/android/calendar/at;->c:I

    packed-switch v2, :pswitch_data_0

    .line 103
    :pswitch_0
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v2, v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    invoke-virtual {v2, v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 97
    :pswitch_2
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    invoke-virtual {v2, v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 100
    :pswitch_3
    iget-object v2, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    invoke-virtual {v2, v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/at;)V

    goto :goto_0

    .line 109
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->b(Ljava/util/ArrayList;)V

    .line 110
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->d:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->c()V

    .line 111
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->f:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->c()V

    .line 112
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->e:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->c()V

    .line 113
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->g:Lcom/android/calendar/detail/cd;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cd;->c()V

    .line 114
    return-void

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected getFragment()Lcom/android/calendar/detail/bq;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->h:Lcom/android/calendar/detail/bq;

    return-object v0
.end method

.method protected setResponseClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v1, 0x7f120226

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 250
    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    :cond_0
    return-void
.end method

.method protected setStatusClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->c:Landroid/view/View;

    const v1, 0x7f120223

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    :cond_0
    return-void
.end method
