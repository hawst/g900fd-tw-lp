.class Lcom/android/calendar/detail/ah;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/af;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/af;)V
    .locals 0

    .prologue
    .line 1874
    iput-object p1, p0, Lcom/android/calendar/detail/ah;->a:Lcom/android/calendar/detail/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/android/calendar/detail/ah;->a:Lcom/android/calendar/detail/af;

    iget-object v0, v0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->s(Lcom/android/calendar/detail/EventInfoFragment;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1879
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.DIAL"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1881
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tel:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/detail/ah;->a:Lcom/android/calendar/detail/af;

    iget-object v0, v0, Lcom/android/calendar/detail/af;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/detail/ah;->a:Lcom/android/calendar/detail/af;

    iget-object v3, v3, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v3}, Lcom/android/calendar/detail/EventInfoFragment;->s(Lcom/android/calendar/detail/EventInfoFragment;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1884
    iget-object v0, p0, Lcom/android/calendar/detail/ah;->a:Lcom/android/calendar/detail/af;

    iget-object v0, v0, Lcom/android/calendar/detail/af;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->startActivity(Landroid/content/Intent;)V

    .line 1886
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1888
    :cond_0
    return-void
.end method
