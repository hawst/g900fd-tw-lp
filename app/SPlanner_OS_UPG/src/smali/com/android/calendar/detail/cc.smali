.class Lcom/android/calendar/detail/cc;
.super Landroid/content/AsyncQueryHandler;
.source "MeetingInvitationFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/bq;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/bq;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 604
    iput-object p1, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    .line 605
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 606
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 643
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    .line 644
    sget-object v0, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    const-string v1, "error to move cursor first"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    :goto_0
    return-void

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/detail/bq;->s:Ljava/lang/String;

    .line 649
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 650
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "com.android.exchange"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iput-boolean v2, v0, Lcom/android/calendar/detail/bq;->v:Z

    .line 654
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    const-string v3, ""

    iput-object v3, v0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    .line 655
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 656
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 657
    iget-object v3, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iput-object v0, v3, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    .line 660
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/detail/bq;->t:Ljava/lang/String;

    .line 661
    iget-object v3, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v0, v0, Lcom/android/calendar/detail/bq;->u:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v4, v4, Lcom/android/calendar/detail/bq;->t:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, ""

    iget-object v4, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v4, v4, Lcom/android/calendar/detail/bq;->t:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    move v0, v2

    :goto_1
    iput-boolean v0, v3, Lcom/android/calendar/detail/bq;->w:Z

    .line 664
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 665
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 666
    iget-object v4, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    iput-boolean v1, v4, Lcom/android/calendar/detail/bq;->x:Z

    .line 668
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-boolean v0, v0, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v0, :cond_7

    .line 669
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/detail/bq;->E:I

    .line 675
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-boolean v0, v0, Lcom/android/calendar/detail/bq;->v:Z

    if-eqz v0, :cond_8

    .line 676
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v1, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v1}, Lcom/android/calendar/detail/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-static {v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/detail/bq;->D:Ljava/util/ArrayList;

    .line 683
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    const/16 v1, 0x8

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/detail/bq;->y:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 661
    goto :goto_1

    .line 671
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/detail/bq;->E:I

    goto :goto_2

    .line 677
    :cond_8
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 678
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v1, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v1}, Lcom/android/calendar/detail/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003c

    invoke-static {v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/detail/bq;->D:Ljava/util/ArrayList;

    goto :goto_3

    .line 680
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    iget-object v1, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v1}, Lcom/android/calendar/detail/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-static {v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/detail/bq;->D:Ljava/util/ArrayList;

    goto :goto_3
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 611
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 636
    :goto_1
    :pswitch_0
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 637
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 618
    :pswitch_1
    :try_start_0
    invoke-direct {p0, p3}, Lcom/android/calendar/detail/cc;->a(Landroid/database/Cursor;)V

    .line 619
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->h(Lcom/android/calendar/detail/bq;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 636
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 637
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 622
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0, p3}, Lcom/android/calendar/detail/bq;->a(Lcom/android/calendar/detail/bq;Landroid/database/Cursor;)V

    .line 623
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->i(Lcom/android/calendar/detail/bq;)Z

    .line 624
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->j(Lcom/android/calendar/detail/bq;)V

    .line 625
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->k(Lcom/android/calendar/detail/bq;)V

    .line 626
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->l(Lcom/android/calendar/detail/bq;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 629
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0, p3}, Lcom/android/calendar/detail/bq;->b(Lcom/android/calendar/detail/bq;Landroid/database/Cursor;)V

    .line 630
    iget-object v0, p0, Lcom/android/calendar/detail/cc;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->j(Lcom/android/calendar/detail/bq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 616
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
