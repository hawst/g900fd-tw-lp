.class Lcom/android/calendar/detail/v;
.super Lcom/android/calendar/ag;
.source "EasyModeListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/r;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/r;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    .line 57
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 58
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 63
    if-nez p3, :cond_0

    .line 78
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 69
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->b(Lcom/android/calendar/detail/r;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 71
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 72
    iget-object v2, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->b(Lcom/android/calendar/detail/r;)J

    move-result-wide v2

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 73
    iget-object v1, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v1}, Lcom/android/calendar/detail/r;->b(Lcom/android/calendar/detail/r;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/android/calendar/detail/r;->a(J)J

    .line 75
    iget-object v1, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v1}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v2

    const/4 v5, 0x0

    move-object v1, p3

    move v4, v3

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/dh;->a(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;IIZ)V

    .line 76
    iget-object v1, p0, Lcom/android/calendar/detail/v;->a:Lcom/android/calendar/detail/r;

    invoke-static {v1}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/android/calendar/detail/o;->a(Ljava/util/ArrayList;I)V

    .line 77
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
