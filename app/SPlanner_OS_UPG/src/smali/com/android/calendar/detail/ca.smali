.class Lcom/android/calendar/detail/ca;
.super Ljava/lang/Object;
.source "MeetingInvitationFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/aw;

.field final synthetic b:Landroid/net/Uri;

.field final synthetic c:Lcom/android/calendar/detail/bz;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/bz;Lcom/android/calendar/event/aw;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 882
    iput-object p1, p0, Lcom/android/calendar/detail/ca;->c:Lcom/android/calendar/detail/bz;

    iput-object p2, p0, Lcom/android/calendar/detail/ca;->a:Lcom/android/calendar/event/aw;

    iput-object p3, p0, Lcom/android/calendar/detail/ca;->b:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 885
    iget-object v0, p0, Lcom/android/calendar/detail/ca;->a:Lcom/android/calendar/event/aw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/ca;->a:Lcom/android/calendar/event/aw;

    iget-object v0, v0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 886
    iget-object v0, p0, Lcom/android/calendar/detail/ca;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 888
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/ca;->c:Lcom/android/calendar/detail/bz;

    iget-object v0, v0, Lcom/android/calendar/detail/bz;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->c(Lcom/android/calendar/detail/bq;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/ca;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 889
    iget-object v1, p0, Lcom/android/calendar/detail/ca;->a:Lcom/android/calendar/event/aw;

    iget-object v2, p0, Lcom/android/calendar/detail/ca;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v1, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    .line 890
    iget-object v0, p0, Lcom/android/calendar/detail/ca;->c:Lcom/android/calendar/detail/bz;

    iget-object v0, v0, Lcom/android/calendar/detail/bz;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->s(Lcom/android/calendar/detail/bq;)Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/ca;->a:Lcom/android/calendar/event/aw;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->a(Lcom/android/calendar/event/aw;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 896
    :cond_0
    :goto_0
    return-void

    .line 891
    :catch_0
    move-exception v0

    .line 892
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
