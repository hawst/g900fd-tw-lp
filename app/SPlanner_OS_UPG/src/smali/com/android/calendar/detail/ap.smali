.class Lcom/android/calendar/detail/ap;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 2346
    iput-object p1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2349
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2429
    :cond_0
    :goto_0
    return-void

    .line 2353
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v2}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;

    .line 2354
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->w(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->x(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->y(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->z(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 2356
    iget-object v1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f110008

    iget-object v3, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v3}, Lcom/android/calendar/detail/EventInfoFragment;->A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2357
    iget-object v1, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    .line 2359
    const/4 v2, 0x1

    if-gt v0, v2, :cond_2

    .line 2360
    const v0, 0x7f12033e

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 2361
    const v0, 0x7f12033f

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 2363
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->B(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2364
    const v0, 0x7f12033d

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 2368
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->C(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2369
    const v0, 0x7f120324

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 2371
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->D(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2372
    const v0, 0x7f120323

    invoke-interface {v1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 2374
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/detail/aq;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/aq;-><init>(Lcom/android/calendar/detail/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 2428
    iget-object v0, p0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_0
.end method
