.class Lcom/android/calendar/detail/cz;
.super Ljava/lang/Object;
.source "TaskInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/cu;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 779
    iput-object p1, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15

    .prologue
    .line 782
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 784
    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->h(Lcom/android/calendar/detail/cu;)Lcom/android/calendar/al;

    move-result-object v0

    const-wide/16 v2, 0x8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v7}, Lcom/android/calendar/detail/cu;->g(Lcom/android/calendar/detail/cu;)J

    move-result-wide v7

    const/4 v9, 0x0

    int-to-long v10, v1

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 787
    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->i(Lcom/android/calendar/detail/cu;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 788
    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 790
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 791
    :catch_0
    move-exception v0

    .line 792
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 796
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cz;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->j(Lcom/android/calendar/detail/cu;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
