.class final Lcom/android/calendar/detail/d;
.super Ljava/lang/Object;
.source "BaseEventInfoFragment.java"

# interfaces
.implements Landroid/text/util/Linkify$MatchFilter;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final acceptMatch(Ljava/lang/CharSequence;II)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    .line 75
    invoke-static {}, Lcom/android/calendar/dz;->B()I

    move-result v2

    move v0, v1

    .line 77
    :goto_0
    if-ge p2, p3, :cond_0

    .line 78
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 79
    sget-object v4, Lcom/android/calendar/detail/bp;->q:Ljava/util/regex/Pattern;

    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 80
    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    add-int/lit8 v0, v0, 0x1

    .line 82
    if-lt v0, v2, :cond_2

    .line 83
    const/4 v1, 0x1

    .line 91
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 77
    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method
