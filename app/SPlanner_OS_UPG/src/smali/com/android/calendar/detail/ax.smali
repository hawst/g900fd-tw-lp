.class Lcom/android/calendar/detail/ax;
.super Landroid/content/BroadcastReceiver;
.source "EventInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 3586
    iput-object p1, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 3589
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_UPDATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3590
    iget-object v0, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v2, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->n(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 3592
    iget-object v0, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->ab(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->ab(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->ac(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3593
    iget-object v0, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->ac(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->c(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)V

    .line 3600
    :cond_0
    :goto_0
    return-void

    .line 3595
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3596
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "email IN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->ad(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/hj;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3597
    iget-object v0, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/detail/bp;->k:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/bp;->j:[Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/ax;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v6}, Lcom/android/calendar/detail/EventInfoFragment;->ae(Lcom/android/calendar/detail/EventInfoFragment;)[Ljava/lang/String;

    move-result-object v6

    const-string v7, "formatted_name ASC, email ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
