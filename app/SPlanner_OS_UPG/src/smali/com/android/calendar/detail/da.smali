.class Lcom/android/calendar/detail/da;
.super Ljava/lang/Object;
.source "TaskInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/cu;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 1038
    iput-object p1, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    :cond_0
    :goto_0
    return-void

    .line 1046
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v2}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-static {v0, v1}, Lcom/android/calendar/detail/cu;->a(Lcom/android/calendar/detail/cu;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;

    .line 1047
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110008

    iget-object v2, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v2}, Lcom/android/calendar/detail/cu;->k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1048
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 1049
    const v1, 0x7f120340

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 1050
    const v1, 0x7f12033e

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 1051
    const v1, 0x7f12033f

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 1052
    const v1, 0x7f12033d

    invoke-interface {v0, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 1054
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/detail/db;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/db;-><init>(Lcom/android/calendar/detail/da;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1088
    iget-object v0, p0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0
.end method
