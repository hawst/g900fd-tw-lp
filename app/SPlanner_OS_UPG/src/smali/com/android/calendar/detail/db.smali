.class Lcom/android/calendar/detail/db;
.super Ljava/lang/Object;
.source "TaskInfoFragment.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/da;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/da;)V
    .locals 0

    .prologue
    .line 1054
    iput-object p1, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1057
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1085
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1059
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->l(Lcom/android/calendar/detail/cu;)V

    .line 1060
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1062
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1063
    :catch_0
    move-exception v0

    .line 1064
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1069
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->m(Lcom/android/calendar/detail/cu;)V

    .line 1070
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1072
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1073
    :catch_1
    move-exception v0

    .line 1074
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1079
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->n(Lcom/android/calendar/detail/cu;)V

    goto :goto_0

    .line 1082
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/db;->a:Lcom/android/calendar/detail/da;

    iget-object v0, v0, Lcom/android/calendar/detail/da;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->o(Lcom/android/calendar/detail/cu;)V

    goto :goto_0

    .line 1057
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f120323 -> :sswitch_0
        0x7f120324 -> :sswitch_2
        0x7f120325 -> :sswitch_1
        0x7f120341 -> :sswitch_3
    .end sparse-switch
.end method
