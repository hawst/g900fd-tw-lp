.class public Lcom/android/calendar/detail/EventInfoActivity;
.super Landroid/app/Activity;
.source "EventInfoActivity.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/detail/a;

.field private c:Z

.field private d:J

.field private e:J

.field private f:J

.field private g:I

.field private h:Z

.field private i:Lcom/android/calendar/al;

.field private j:Z

.field private k:Landroid/content/BroadcastReceiver;

.field private l:Lcom/android/calendar/detail/ac;

.field private final m:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/android/calendar/detail/EventInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/EventInfoActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 57
    iput v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->g:I

    .line 58
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->h:Z

    .line 64
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->j:Z

    .line 272
    new-instance v0, Lcom/android/calendar/detail/aa;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/aa;-><init>(Lcom/android/calendar/detail/EventInfoActivity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->k:Landroid/content/BroadcastReceiver;

    .line 314
    new-instance v0, Lcom/android/calendar/detail/ab;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/ab;-><init>(Lcom/android/calendar/detail/EventInfoActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->m:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoActivity;)Lcom/android/calendar/detail/a;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    return-object v0
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    instance-of v0, v0, Lcom/android/calendar/detail/EventInfoFragment;

    if-eqz v0, :cond_1

    .line 209
    if-eqz p1, :cond_0

    .line 210
    invoke-static {p0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 212
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->finish()V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoActivity;->a(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 12

    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    instance-of v0, v0, Lcom/android/calendar/detail/EventInfoFragment;

    if-eqz v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EventInfoFragment;

    .line 153
    if-nez v0, :cond_1

    .line 154
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->c:Z

    if-eqz v0, :cond_2

    const/4 v10, 0x1

    .line 156
    :goto_1
    new-instance v0, Lcom/android/calendar/detail/EventInfoFragment;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoActivity;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoActivity;->d:J

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoActivity;->e:J

    iget v8, p0, Lcom/android/calendar/detail/EventInfoActivity;->g:I

    iget-boolean v9, p0, Lcom/android/calendar/detail/EventInfoActivity;->c:Z

    iget-boolean v11, p0, Lcom/android/calendar/detail/EventInfoActivity;->h:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v11}, Lcom/android/calendar/detail/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZIZ)V

    .line 159
    :cond_1
    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    .line 160
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 161
    const v2, 0x7f12006b

    sget-object v3, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 162
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 163
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 154
    :cond_2
    const/4 v10, 0x0

    goto :goto_1
.end method

.method private c()V
    .locals 9

    .prologue
    .line 167
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    instance-of v0, v0, Lcom/android/calendar/detail/bq;

    if-eqz v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/bq;

    .line 172
    if-nez v0, :cond_1

    .line 173
    new-instance v0, Lcom/android/calendar/detail/bq;

    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoActivity;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoActivity;->d:J

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoActivity;->e:J

    iget-boolean v8, p0, Lcom/android/calendar/detail/EventInfoActivity;->c:Z

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/detail/bq;-><init>(Landroid/content/Context;JJJZ)V

    .line 176
    :cond_1
    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->b:Lcom/android/calendar/detail/a;

    .line 177
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 178
    const v2, 0x7f12006b

    sget-object v3, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 179
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 180
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 241
    .line 242
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    .line 243
    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->j:Z

    if-eq v1, v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->finish()V

    .line 246
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 309
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->i:Lcom/android/calendar/al;

    const-wide/16 v2, 0x80

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v8}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JI)V

    .line 310
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 135
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 137
    :pswitch_0
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoActivity;->b()V

    goto :goto_0

    .line 140
    :pswitch_1
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoActivity;->c()V

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 300
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iput-boolean v5, p0, Lcom/android/calendar/detail/EventInfoActivity;->j:Z

    .line 72
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->i:Lcom/android/calendar/al;

    .line 74
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 77
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 78
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    :cond_1
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoActivity;->setContentView(I)V

    .line 84
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 85
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoActivity;->f:J

    .line 86
    iput-boolean v4, p0, Lcom/android/calendar/detail/EventInfoActivity;->c:Z

    .line 88
    if-eqz p1, :cond_4

    .line 89
    const-string v0, "key_event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->f:J

    .line 90
    const-string v0, "key_start_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->d:J

    .line 91
    const-string v0, "key_end_millis"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->e:J

    .line 92
    const-string v0, "key_attendee_response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->g:I

    .line 93
    const-string v0, "key_fragment_is_dialog"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->c:Z

    .line 110
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_3

    .line 112
    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->h:Z

    if-eqz v1, :cond_5

    .line 113
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 114
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 120
    :cond_3
    :goto_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 121
    const-string v1, "clock.date_format_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/detail/EventInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hj;->e:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoActivity;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v5, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 124
    new-instance v0, Lcom/android/calendar/detail/ac;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ac;-><init>(Lcom/android/calendar/detail/EventInfoActivity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->l:Lcom/android/calendar/detail/ac;

    .line 125
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->i:Lcom/android/calendar/al;

    invoke-virtual {v0, v4, p0}, Lcom/android/calendar/al;->b(ILcom/android/calendar/ap;)V

    .line 127
    invoke-virtual {p0, v5}, Lcom/android/calendar/detail/EventInfoActivity;->a(I)V

    .line 128
    return-void

    .line 94
    :cond_4
    if-eqz v0, :cond_2

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    const-string v1, "beginTime"

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/EventInfoActivity;->d:J

    .line 96
    const-string v1, "endTime"

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/EventInfoActivity;->e:J

    .line 97
    const-string v1, "attendeeStatus"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->g:I

    .line 98
    const-string v1, "ignore_time"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->h:Z

    .line 100
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_2

    .line 103
    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->f:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    sget-object v0, Lcom/android/calendar/detail/EventInfoActivity;->a:Ljava/lang/String;

    const-string v1, "No event id"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 116
    :cond_5
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->i:Lcom/android/calendar/al;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Integer;)V

    .line 260
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->i:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->a()V

    .line 261
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->m:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->m:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->k:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 268
    :cond_1
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 269
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 270
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 197
    packed-switch p1, :pswitch_data_0

    .line 204
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 199
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoActivity;->a(Z)V

    .line 200
    const/4 v0, 0x1

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/android/calendar/detail/EventInfoActivity;->setIntent(Landroid/content/Intent;)V

    .line 221
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 185
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 192
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 187
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoActivity;->a(Z)V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoActivity;->l:Lcom/android/calendar/detail/ac;

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoActivity;->l:Lcom/android/calendar/detail/ac;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 254
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 255
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 230
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 231
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hj;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoActivity;->l:Lcom/android/calendar/detail/ac;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 233
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoActivity;->d()V

    .line 234
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 225
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 226
    return-void
.end method
