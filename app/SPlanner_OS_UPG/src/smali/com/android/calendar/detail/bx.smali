.class Lcom/android/calendar/detail/bx;
.super Landroid/content/BroadcastReceiver;
.source "MeetingInvitationFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/bq;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 812
    iput-object p1, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 816
    iget-object v0, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 820
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "email IN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v1}, Lcom/android/calendar/detail/bq;->o(Lcom/android/calendar/detail/bq;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/android/calendar/hj;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 822
    iget-object v0, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->q(Lcom/android/calendar/detail/bq;)Lcom/android/calendar/detail/cc;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/detail/bp;->k:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/bp;->j:[Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/bx;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v6}, Lcom/android/calendar/detail/bq;->p(Lcom/android/calendar/detail/bq;)[Ljava/lang/String;

    move-result-object v6

    const-string v7, "formatted_name ASC, email ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/cc;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
