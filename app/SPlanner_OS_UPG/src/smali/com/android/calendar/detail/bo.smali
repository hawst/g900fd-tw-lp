.class Lcom/android/calendar/detail/bo;
.super Lcom/android/calendar/ag;
.source "EventInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;

.field private b:Lcom/android/calendar/detail/bm;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 3069
    iput-object p1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    .line 3070
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 3067
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/bo;->c:I

    .line 3071
    return-void
.end method

.method private c()V
    .locals 12

    .prologue
    const v11, 0x7f120158

    const/4 v10, 0x1

    const v9, 0x7f1201a0

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 3222
    .line 3225
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->M(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3279
    :cond_0
    :goto_0
    return-void

    .line 3230
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->V(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 3232
    new-array v6, v10, [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->h(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    .line 3235
    sget-object v3, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    .line 3236
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;

    move-result-object v0

    const/16 v1, 0x10

    sget-object v4, Lcom/android/calendar/detail/bp;->l:[Ljava/lang/String;

    const-string v5, "event_id=?"

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 3238
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f120184

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 3239
    if-eqz v0, :cond_2

    .line 3240
    invoke-virtual {v0, v8}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 3242
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3243
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->W(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3244
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1201b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3247
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3248
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->X(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3249
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v3}, Lcom/android/calendar/detail/EventInfoFragment;->X(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v9, v3}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 3258
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;

    move-result-object v0

    const/16 v1, 0x20

    sget-object v3, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/EditEventActivity;->e:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "event_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v6}, Lcom/android/calendar/detail/EventInfoFragment;->h(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 3261
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->Y(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3262
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f120184

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 3263
    if-eqz v0, :cond_5

    .line 3264
    invoke-virtual {v0, v8}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 3266
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3269
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->Z(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->aa(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->Y(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3270
    new-array v6, v10, [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->h(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v8

    .line 3275
    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    .line 3276
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;

    move-result-object v0

    const/4 v1, 0x2

    sget-object v4, Lcom/android/calendar/detail/bp;->i:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const-string v7, "attendeeName ASC, attendeeEmail ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3252
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3253
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->X(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3254
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v3}, Lcom/android/calendar/detail/EventInfoFragment;->X(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v9, v3}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected a(ILjava/lang/Object;I)V
    .locals 2

    .prologue
    .line 3210
    packed-switch p1, :pswitch_data_0

    .line 3219
    :goto_0
    return-void

    .line 3212
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 3213
    invoke-virtual {v0}, Landroid/app/NotificationManager;->cancelAll()V

    .line 3214
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/AlertService;->a(Landroid/content/Context;)Z

    goto :goto_0

    .line 3210
    nop

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_0
    .end packed-switch
.end method

.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3092
    monitor-enter p0

    .line 3093
    :try_start_0
    iget v0, p0, Lcom/android/calendar/detail/bo;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/bo;->c:I

    .line 3094
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3097
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 3098
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3099
    :cond_0
    if-eqz p3, :cond_1

    .line 3100
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 3206
    :cond_1
    :goto_0
    return-void

    .line 3094
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 3105
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 3192
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onQueryComplete no token id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3196
    :cond_3
    :goto_1
    if-eqz p3, :cond_4

    .line 3197
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 3200
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->b:Lcom/android/calendar/detail/bm;

    if-eqz v0, :cond_1

    .line 3201
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->b:Lcom/android/calendar/detail/bm;

    invoke-interface {v0, p1}, Lcom/android/calendar/detail/bm;->a(I)V

    .line 3202
    invoke-virtual {p0}, Lcom/android/calendar/detail/bo;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 3203
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->b:Lcom/android/calendar/detail/bm;

    invoke-interface {v0}, Lcom/android/calendar/detail/bm;->a()V

    goto :goto_0

    .line 3107
    :sswitch_0
    if-nez p3, :cond_5

    .line 3108
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 3111
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 3113
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->J(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 3114
    if-eqz p3, :cond_6

    .line 3115
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 3117
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0f0455

    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 3118
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 3121
    :cond_7
    invoke-direct {p0}, Lcom/android/calendar/detail/bo;->c()V

    .line 3122
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;)V

    .line 3123
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->K(Lcom/android/calendar/detail/EventInfoFragment;)V

    .line 3124
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->L(Lcom/android/calendar/detail/EventInfoFragment;)V

    .line 3127
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->M(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3128
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->N(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto :goto_1

    .line 3132
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 3133
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;)V

    .line 3134
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->O(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3135
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0, v6}, Lcom/android/calendar/detail/EventInfoFragment;->e(Lcom/android/calendar/detail/EventInfoFragment;Z)Z

    .line 3137
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->P(Lcom/android/calendar/detail/EventInfoFragment;)Z

    goto/16 :goto_1

    .line 3140
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->c(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 3141
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;)V

    goto/16 :goto_1

    .line 3144
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->d(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 3145
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->Q(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 3150
    :sswitch_4
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v6, :cond_3

    .line 3151
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->S(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->R(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3152
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 3153
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->S(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2}, Lcom/android/calendar/detail/EventInfoFragment;->R(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 3155
    iget-object v1, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v1}, Lcom/android/calendar/detail/EventInfoFragment;->T(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 3160
    :sswitch_5
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 3161
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0, p3}, Lcom/android/calendar/detail/EventInfoFragment;->e(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)V

    goto/16 :goto_1

    .line 3166
    :sswitch_6
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 3171
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 3173
    :cond_9
    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    .line 3174
    const/16 v2, 0x20

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    .line 3176
    iget-object v4, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v4}, Lcom/android/calendar/detail/EventInfoFragment;->h(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_a

    .line 3177
    iget-object v2, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v2, v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Lcom/android/calendar/detail/EventInfoFragment;J)J

    .line 3178
    new-array v6, v6, [Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->h(Lcom/android/calendar/detail/EventInfoFragment;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    .line 3183
    iget-object v0, p0, Lcom/android/calendar/detail/bo;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/bp;->i:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const-string v7, "attendeeName ASC, attendeeEmail ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3188
    :cond_a
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_9

    goto/16 :goto_1

    .line 3105
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_4
        0x10 -> :sswitch_3
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3084
    monitor-enter p0

    .line 3085
    :try_start_0
    iget v0, p0, Lcom/android/calendar/detail/bo;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/detail/bo;->c:I

    .line 3086
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3087
    invoke-super/range {p0 .. p7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 3088
    return-void

    .line 3086
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lcom/android/calendar/detail/bm;)V
    .locals 0

    .prologue
    .line 3074
    iput-object p1, p0, Lcom/android/calendar/detail/bo;->b:Lcom/android/calendar/detail/bm;

    .line 3075
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 3078
    iget v0, p0, Lcom/android/calendar/detail/bo;->c:I

    return v0
.end method
