.class public Lcom/android/calendar/detail/EventInfoFragment;
.super Lcom/android/calendar/detail/a;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/calendar/a/a/a/j;
.implements Lcom/android/calendar/ap;
.implements Lcom/android/calendar/cq;


# static fields
.field private static bh:Landroid/app/AlertDialog;

.field public static final d:Ljava/lang/String;

.field public static e:[Ljava/lang/String;

.field private static s:I

.field private static final t:[Ljava/lang/String;

.field private static final u:[Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/widget/LinearLayout;

.field private H:Landroid/widget/LinearLayout;

.field private I:Landroid/widget/Button;

.field private J:Landroid/widget/Button;

.field private K:Lcom/android/calendar/ex;

.field private L:Landroid/widget/ScrollView;

.field private M:Landroid/widget/ImageButton;

.field private N:Landroid/view/Menu;

.field private O:Landroid/app/Activity;

.field private P:Landroid/widget/TextView;

.field private Q:Landroid/net/Uri;

.field private R:J

.field private S:Landroid/database/Cursor;

.field private T:Landroid/database/Cursor;

.field private U:Landroid/database/Cursor;

.field private V:Landroid/database/Cursor;

.field private W:J

.field private X:J

.field private Y:Ljava/lang/String;

.field private Z:Z

.field private aA:Z

.field private aB:Ljava/lang/String;

.field private aC:I

.field private aD:I

.field private aE:J

.field private aF:I

.field private aG:Z

.field private final aH:Ljava/util/ArrayList;

.field private final aI:Ljava/util/ArrayList;

.field private final aJ:Ljava/util/ArrayList;

.field private final aK:Ljava/util/ArrayList;

.field private final aL:Ljava/util/ArrayList;

.field private aM:Landroid/text/SpannableStringBuilder;

.field private aN:Landroid/widget/LinearLayout;

.field private aO:I

.field private aP:Ljava/util/ArrayList;

.field private aQ:Ljava/util/ArrayList;

.field private aR:Ljava/util/ArrayList;

.field private aS:Ljava/util/ArrayList;

.field private aT:Ljava/util/ArrayList;

.field private aU:Ljava/lang/String;

.field private aV:Ljava/lang/String;

.field private aW:Z

.field private aX:Z

.field private aY:Z

.field private aZ:Z

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Ljava/lang/String;

.field private ag:Ljava/lang/String;

.field private ah:Ljava/lang/String;

.field private ai:J

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:Z

.field private ar:I

.field private as:I

.field private at:Z

.field private au:Z

.field private av:I

.field private aw:Ljava/lang/String;

.field private ax:Z

.field private ay:Z

.field private az:Z

.field private bA:I

.field private bB:I

.field private bC:J

.field private bD:J

.field private bE:J

.field private bF:Z

.field private bG:Lcom/android/calendar/c/a;

.field private bH:Z

.field private bI:Z

.field private bJ:Landroid/os/Handler;

.field private bK:Lcom/android/calendar/d/a/a;

.field private bL:Landroid/app/DialogFragment;

.field private bM:Ljava/lang/Runnable;

.field private bN:Ljava/lang/Runnable;

.field private bO:Landroid/view/View$OnClickListener;

.field private final bP:Landroid/view/View$OnClickListener;

.field private final bQ:Landroid/content/BroadcastReceiver;

.field private final bR:Landroid/view/View$OnClickListener;

.field private ba:I

.field private bb:I

.field private bc:I

.field private bd:I

.field private be:I

.field private bf:I

.field private bg:I

.field private bi:Lcom/android/calendar/al;

.field private bj:Lcom/android/calendar/d/g;

.field private bk:Lcom/android/calendar/detail/w;

.field private bl:Lcom/android/calendar/cj;

.field private bm:Lcom/android/calendar/detail/bo;

.field private bn:Z

.field private bo:Z

.field private bp:Z

.field private bq:Z

.field private br:Z

.field private bs:Z

.field private bt:Z

.field private bu:Z

.field private bv:Ljava/util/ArrayList;

.field private bw:Ljava/util/ArrayList;

.field private bx:Ljava/util/ArrayList;

.field private by:I

.field private bz:I

.field protected final f:Ljava/util/ArrayList;

.field public g:Ljava/util/ArrayList;

.field public h:Ljava/util/ArrayList;

.field private v:I

.field private w:Landroid/view/View;

.field private x:Landroid/widget/PopupMenu;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 163
    const-class v0, Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    .line 165
    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "rrule"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "maxReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "allowedReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "availability"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "accessLevel"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "canOrganizerRespond"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "availabilityStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "facebook_schedule_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "facebook_service_provider"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "facebook_owner"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "facebook_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "deleted"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "rdate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->e:[Ljava/lang/String;

    .line 241
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/detail/EventInfoFragment;->s:I

    .line 243
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "begin"

    aput-object v1, v0, v3

    const-string v1, "end"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->t:[Ljava/lang/String;

    .line 271
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "friend_current_status"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->u:[Ljava/lang/String;

    .line 406
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 461
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    .line 277
    iput v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    .line 294
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    .line 297
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    .line 321
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    .line 332
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    .line 339
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ax:Z

    .line 340
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ay:Z

    .line 342
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->az:Z

    .line 343
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aA:Z

    .line 347
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    .line 348
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    .line 349
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    .line 351
    iput v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aF:I

    .line 359
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aG:Z

    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    .line 365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    .line 367
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    .line 370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    .line 372
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->h:Ljava/util/ArrayList;

    .line 383
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    .line 384
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aV:Ljava/lang/String;

    .line 386
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    .line 387
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    .line 388
    iput-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->aY:Z

    .line 389
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aZ:Z

    .line 391
    iput v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    .line 392
    iput v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    .line 393
    iput v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    .line 394
    iput v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    .line 395
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->be:I

    .line 396
    const/16 v0, 0x258

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    .line 397
    const/16 v0, 0x258

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bg:I

    .line 413
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bn:Z

    .line 414
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bo:Z

    .line 416
    iput-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->bp:Z

    .line 417
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bq:Z

    .line 418
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->br:Z

    .line 419
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bs:Z

    .line 420
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    .line 421
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bu:Z

    .line 426
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    .line 427
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    .line 428
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    .line 429
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    .line 430
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bC:J

    .line 431
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bD:J

    .line 432
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bE:J

    .line 434
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bF:Z

    .line 435
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    .line 436
    iput-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->bH:Z

    .line 440
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bI:Z

    .line 441
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bJ:Landroid/os/Handler;

    .line 443
    iput-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    .line 454
    new-instance v0, Lcom/android/calendar/detail/ad;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ad;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bM:Ljava/lang/Runnable;

    .line 891
    new-instance v0, Lcom/android/calendar/detail/be;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/be;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bN:Ljava/lang/Runnable;

    .line 911
    new-instance v0, Lcom/android/calendar/detail/bf;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bf;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bO:Landroid/view/View$OnClickListener;

    .line 2282
    new-instance v0, Lcom/android/calendar/detail/ao;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ao;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bP:Landroid/view/View$OnClickListener;

    .line 3586
    new-instance v0, Lcom/android/calendar/detail/ax;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ax;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bQ:Landroid/content/BroadcastReceiver;

    .line 3699
    new-instance v0, Lcom/android/calendar/detail/ay;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ay;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bR:Landroid/view/View$OnClickListener;

    .line 462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JJJIZI)V
    .locals 12

    .prologue
    .line 476
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/android/calendar/detail/EventInfoFragment;-><init>(Landroid/content/Context;JJJIZIZ)V

    .line 477
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JJJIZIZ)V
    .locals 12

    .prologue
    .line 481
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v1 .. v10}, Lcom/android/calendar/detail/EventInfoFragment;-><init>(Landroid/content/Context;Landroid/net/Uri;JJIZI)V

    .line 483
    iput-wide p2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    .line 484
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    .line 485
    if-nez p11, :cond_0

    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 486
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bF:Z

    .line 488
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;JJIZI)V
    .locals 3

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    .line 277
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    .line 321
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    .line 339
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ax:Z

    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ay:Z

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->az:Z

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aA:Z

    .line 347
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    .line 348
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    .line 349
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    .line 351
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aF:I

    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aG:Z

    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    .line 365
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    .line 370
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    .line 372
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->h:Ljava/util/ArrayList;

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aV:Ljava/lang/String;

    .line 386
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aY:Z

    .line 389
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aZ:Z

    .line 391
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    .line 392
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    .line 393
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    .line 394
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    .line 395
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->be:I

    .line 396
    const/16 v0, 0x258

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    .line 397
    const/16 v0, 0x258

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bg:I

    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bn:Z

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bo:Z

    .line 416
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bp:Z

    .line 417
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bq:Z

    .line 418
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->br:Z

    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bs:Z

    .line 420
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    .line 421
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bu:Z

    .line 426
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    .line 427
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    .line 428
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    .line 429
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    .line 430
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bC:J

    .line 431
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bD:J

    .line 432
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bE:J

    .line 434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bF:Z

    .line 435
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bH:Z

    .line 440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bI:Z

    .line 441
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bJ:Landroid/os/Handler;

    .line 443
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    .line 454
    new-instance v0, Lcom/android/calendar/detail/ad;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ad;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bM:Ljava/lang/Runnable;

    .line 891
    new-instance v0, Lcom/android/calendar/detail/be;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/be;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bN:Ljava/lang/Runnable;

    .line 911
    new-instance v0, Lcom/android/calendar/detail/bf;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/bf;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bO:Landroid/view/View$OnClickListener;

    .line 2282
    new-instance v0, Lcom/android/calendar/detail/ao;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ao;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bP:Landroid/view/View$OnClickListener;

    .line 3586
    new-instance v0, Lcom/android/calendar/detail/ax;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ax;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bQ:Landroid/content/BroadcastReceiver;

    .line 3699
    new-instance v0, Lcom/android/calendar/detail/ay;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ay;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bR:Landroid/view/View$OnClickListener;

    .line 466
    iput-boolean p8, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    .line 467
    iput-object p2, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    .line 468
    iput-wide p3, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    .line 469
    iput-wide p5, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    .line 470
    iput p7, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    .line 471
    iput p9, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    .line 472
    return-void
.end method

.method static synthetic A(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/PopupMenu;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->x:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method private declared-synchronized A()V
    .locals 3

    .prologue
    .line 2835
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aQ:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aR:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aS:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aT:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aw:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2860
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2845
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2846
    const v1, 0x7f090028

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aQ:Ljava/util/ArrayList;

    .line 2847
    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Landroid/content/res/Resources;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aR:Ljava/util/ArrayList;

    .line 2848
    const v1, 0x7f090026

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aS:Ljava/util/ArrayList;

    .line 2849
    const v1, 0x7f090025

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aT:Ljava/util/ArrayList;

    .line 2853
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aw:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2854
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aS:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aT:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aw:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 2857
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2858
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2835
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private B()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2933
    const/4 v1, 0x0

    .line 2935
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v2, :cond_2

    .line 2936
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2937
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v3, 0x18

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2938
    if-nez v2, :cond_1

    .line 2945
    :cond_0
    :goto_0
    return v0

    .line 2941
    :cond_1
    const-string v3, "local"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic B(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v0

    return v0
.end method

.method private C()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2949
    .line 2951
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 2952
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2953
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v2, 0x18

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2954
    if-nez v1, :cond_1

    .line 2961
    :cond_0
    :goto_0
    return v0

    .line 2957
    :cond_1
    const-string v2, "com.android.sharepoint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2958
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic C(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    return v0
.end method

.method private D()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2965
    .line 2967
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 2968
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2969
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v2, 0x18

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2970
    if-nez v1, :cond_1

    .line 2977
    :cond_0
    :goto_0
    return v0

    .line 2973
    :cond_1
    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2974
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic D(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ao:Z

    return v0
.end method

.method static synthetic E(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->j()V

    return-void
.end method

.method private E()Z
    .locals 1

    .prologue
    .line 2981
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ad:Z

    return v0
.end method

.method static synthetic F(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->n()V

    return-void
.end method

.method private F()Z
    .locals 1

    .prologue
    .line 2985
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ae:Z

    return v0
.end method

.method static synthetic G(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->k()V

    return-void
.end method

.method private G()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 3019
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 3020
    const-string v2, "fb://profile"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3021
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 3022
    const/high16 v3, 0x10000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 3023
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic H(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->m()V

    return-void
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 3027
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    return v0
.end method

.method private I()V
    .locals 2

    .prologue
    .line 3040
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->v()V

    .line 3042
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 3043
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 3044
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 3045
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3047
    :cond_0
    return-void
.end method

.method static synthetic I(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->l()V

    return-void
.end method

.method private J()Z
    .locals 1

    .prologue
    .line 3428
    const/4 v0, 0x0

    .line 3436
    return v0
.end method

.method static synthetic J(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->q()Z

    move-result v0

    return v0
.end method

.method private K()V
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x0

    const-wide/16 v0, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 3441
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 3493
    :cond_0
    :goto_0
    return-void

    .line 3447
    :cond_1
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_6

    .line 3448
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->L()J

    move-result-wide v0

    .line 3454
    :cond_2
    :goto_1
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "data"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "requesting_package"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 3457
    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v8

    const-string v0, "data1"

    aput-object v0, v2, v6

    const-string v0, "data2"

    aput-object v0, v2, v7

    .line 3463
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 3464
    const-string v0, "mimetype =?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3465
    const-string v0, " AND "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3466
    const-string v0, "data2 IN (?,?)"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3468
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "vnd.android.cursor.item/phone_v2"

    aput-object v5, v4, v8

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3470
    if-eqz v1, :cond_5

    .line 3472
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3473
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3478
    :cond_3
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3479
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 3480
    new-instance v3, Lcom/android/calendar/detail/bn;

    invoke-direct {v3, v0, v2}, Lcom/android/calendar/detail/bn;-><init>(Ljava/lang/String;I)V

    .line 3481
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3482
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 3485
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3489
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 3490
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120185

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 3491
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->f:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    .line 3449
    :cond_6
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 3450
    const-string v0, "contact_id"

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_1

    .line 3485
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic K(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->p()V

    return-void
.end method

.method private L()J
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 3496
    const-wide/16 v6, 0x0

    .line 3497
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 3499
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "contact_id"

    aput-object v0, v2, v8

    .line 3500
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 3502
    if-eqz v2, :cond_1

    .line 3504
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3505
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 3508
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 3511
    :goto_1
    return-wide v0

    .line 3508
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-wide v0, v6

    goto :goto_0

    :cond_1
    move-wide v0, v6

    goto :goto_1
.end method

.method static synthetic L(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->w()V

    return-void
.end method

.method private M()V
    .locals 3

    .prologue
    .line 3581
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_REQUESTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3582
    const-string v1, "id"

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3583
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 3584
    return-void
.end method

.method static synthetic M(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    return v0
.end method

.method static synthetic N(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->A()V

    return-void
.end method

.method private N()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 3637
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 3639
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 3640
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 3641
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    aput-object v0, v2, v1

    .line 3640
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3644
    :cond_0
    return-object v2
.end method

.method private O()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3653
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 3654
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 3655
    iput v2, v0, Landroid/text/format/Time;->month:I

    .line 3656
    iput v5, v0, Landroid/text/format/Time;->monthDay:I

    .line 3657
    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 3658
    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 3659
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 3660
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 3662
    const/16 v1, 0x76e

    iput v1, v0, Landroid/text/format/Time;->year:I

    .line 3663
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 3665
    const/16 v1, 0x7f4

    iput v1, v0, Landroid/text/format/Time;->year:I

    .line 3666
    const/16 v1, 0xb

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 3667
    const/16 v1, 0x1f

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 3668
    const/16 v1, 0x18

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 3669
    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 3670
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 3671
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    .line 3672
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3673
    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 3674
    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 3675
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 3677
    const-string v3, "( begin >= ? AND end <= ? )  AND event_id == ? "

    .line 3680
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 3684
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/detail/EventInfoFragment;->t:[Ljava/lang/String;

    const-string v5, "startDay,startMinute"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3686
    if-eqz v1, :cond_1

    .line 3688
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3689
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    .line 3690
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3693
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3696
    :cond_1
    return-void

    .line 3693
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic O(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic P(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->t()Z

    move-result v0

    return v0
.end method

.method static synthetic Q(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->V:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic R(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic S(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->al:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic T(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->P:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic U(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/detail/bo;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    return-object v0
.end method

.method static synthetic V(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->au:Z

    return v0
.end method

.method static synthetic W(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    return v0
.end method

.method static synthetic X(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic Y(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bq:Z

    return v0
.end method

.method static synthetic Z(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ap:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;I)I
    .locals 0

    .prologue
    .line 160
    iput p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aF:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;J)J
    .locals 1

    .prologue
    .line 160
    iput-wide p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bE:J

    return-wide p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 3515
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-wide v0, v8

    .line 3532
    :goto_0
    return-wide v0

    .line 3519
    :cond_1
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    .line 3520
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/String;

    aput-object p1, v2, v6

    const-string v3, "sourceid = ?  "

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v2

    .line 3523
    if-eqz v2, :cond_3

    .line 3525
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3526
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 3529
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move-wide v0, v8

    goto :goto_1

    :cond_3
    move-wide v0, v8

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->x:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->A:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aV:Ljava/lang/String;

    return-object p1
.end method

.method public static a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 2928
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 2929
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private a(J)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2758
    new-instance v5, Landroid/content/ContentValues;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 2759
    const-string v0, "state"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2760
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2761
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    const/16 v2, 0x100

    sget-object v4, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const-wide/16 v8, 0x0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 2763
    return-void
.end method

.method private a(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 9

    .prologue
    .line 1174
    new-instance v1, Lcom/android/calendar/detail/ck;

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    iget-boolean v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/android/calendar/detail/ck;-><init>(Landroid/content/Intent;Landroid/net/Uri;JJZ)V

    iput-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    .line 1178
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Recurring"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1182
    :goto_0
    return-void

    .line 1179
    :catch_0
    move-exception v0

    .line 1180
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v1, "Failure saving state: active SelectRecurrenceDialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const v7, 0x7f12018e

    const v6, 0x7f120118

    const/4 v1, 0x0

    .line 2185
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2246
    :goto_0
    return-void

    .line 2189
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2190
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 2191
    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2192
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-nez v0, :cond_5

    .line 2193
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c01ce

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2195
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c01c9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2197
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v0, v5

    div-int/lit8 v4, v0, 0x2

    .line 2198
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 2199
    if-gez v4, :cond_1

    .line 2201
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    move v5, v4

    move v4, v1

    .line 2203
    :cond_1
    if-gez v0, :cond_2

    .line 2205
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    move v2, v0

    move v0, v1

    .line 2207
    :cond_2
    invoke-static {v3, v4, v0, v5, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 2210
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2211
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2212
    new-instance v2, Lcom/android/calendar/detail/am;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/am;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2223
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f028f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2225
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2226
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2227
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-static {v2, v7, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2228
    new-instance v2, Lcom/android/calendar/detail/an;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/an;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2241
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-static {v0, v6, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2242
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-nez v0, :cond_4

    .line 2243
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v2, 0x7f12003c

    invoke-static {v0, v2, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2245
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v2, 0x7f12005d

    invoke-static {v0, v2, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto/16 :goto_0

    :cond_5
    move-object v2, v3

    goto :goto_1
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 906
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_0

    .line 907
    const v0, 0x7f120186

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bO:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 909
    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 160
    invoke-static {p0, p1, p2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2552
    if-nez p1, :cond_1

    .line 2576
    :cond_0
    :goto_0
    return-void

    .line 2556
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2557
    if-eqz v0, :cond_0

    .line 2562
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_voice_capable"

    invoke-static {v2}, Lcom/android/calendar/common/b/a;->d(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2563
    invoke-virtual {v0}, Landroid/widget/TextView;->getAutoLinkMask()I

    move-result v1

    and-int/lit8 v1, v1, -0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 2566
    :cond_2
    const v1, 0x7f120197

    if-eq p2, v1, :cond_3

    .line 2567
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 2570
    :cond_3
    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bs:Z

    if-eqz v1, :cond_5

    .line 2571
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 2567
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 2573
    :cond_5
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->c(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 3536
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3538
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 3539
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/detail/bn;

    .line 3540
    const v2, 0x7f040051

    invoke-virtual {v0, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 3542
    const v3, 0x7f1201bc

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 3543
    const v4, 0x7f1201bd

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 3545
    iget v6, v1, Lcom/android/calendar/detail/bn;->b:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 3546
    const v6, 0x7f0f0238

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 3551
    :goto_1
    iget-object v3, v1, Lcom/android/calendar/detail/bn;->a:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3552
    const v3, 0x7f1201be

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 3553
    const v4, 0x7f1201c0

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 3554
    const-string v6, "tel"

    iget-object v7, v1, Lcom/android/calendar/detail/bn;->a:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 3555
    const-string v7, "sms"

    iget-object v1, v1, Lcom/android/calendar/detail/bn;->a:Ljava/lang/String;

    invoke-static {v7, v1, v8}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3556
    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 3557
    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 3558
    new-instance v1, Lcom/android/calendar/detail/av;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/av;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3566
    new-instance v1, Lcom/android/calendar/detail/aw;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/aw;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3574
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 3548
    :cond_0
    const v6, 0x7f0f02a4

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 3577
    :cond_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3578
    return-void

    .line 3577
    :cond_2
    const/16 v0, 0x8

    goto :goto_2
.end method

.method private a(Z[Ljava/lang/String;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1107
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    if-eqz v0, :cond_1

    .line 1108
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1109
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.email.intent.action.CALENDAR_MEETING_FORWARD"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1110
    const-string v2, "account_email"

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1111
    const-string v2, "event_id"

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1113
    iget-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v2, :cond_0

    .line 1114
    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 1155
    :goto_0
    return-void

    .line 1118
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v1, "Error: Could not find com.android.email.intent.action.CALENDAR_MEETING_FORWARD activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1123
    :cond_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1124
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    .line 1125
    if-eqz p1, :cond_2

    const v0, 0x7f0f0381

    .line 1126
    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1127
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1132
    :goto_2
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1133
    if-eqz p1, :cond_4

    .line 1134
    const-string v0, "android.intent.action.SENDTO"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1135
    const-string v0, "plain/text"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mailto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1148
    :goto_3
    const-string v0, "theme"

    const/4 v1, 0x2

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1150
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHOOSER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1151
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1152
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1153
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0, v3, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    .line 1125
    :cond_2
    const v0, 0x7f0f01fd

    goto :goto_1

    .line 1130
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0f02dd

    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1138
    :cond_4
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1139
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1140
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v0, :cond_5

    .line 1141
    invoke-direct {p0, v8, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1144
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-wide v6, v4

    invoke-static/range {v1 .. v7}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;ZJJ)Landroid/net/Uri;

    move-result-object v0

    .line 1145
    const-string v1, "text/x-vCalendar"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1146
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2249
    if-nez p0, :cond_0

    .line 2256
    :goto_0
    return v1

    .line 2252
    :cond_0
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2255
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2256
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EventInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aA:Z

    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1587
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3415
    array-length v0, p1

    .line 3416
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 3417
    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3418
    const-string v2, "setLunar"

    aput-object v2, v1, v0

    .line 3419
    sput v0, Lcom/android/calendar/detail/EventInfoFragment;->s:I

    .line 3420
    return-object v1
.end method

.method static synthetic aa(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->br:Z

    return v0
.end method

.method static synthetic ab(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ac(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic ad(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic ae(Lcom/android/calendar/detail/EventInfoFragment;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->N()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic af(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bI:Z

    return v0
.end method

.method static synthetic ag(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bJ:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;J)J
    .locals 1

    .prologue
    .line 160
    iput-wide p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    return-wide p1
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/res/Resources;I)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 2913
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    .line 2914
    array-length v2, v1

    .line 2915
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2917
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2918
    aget v4, v1, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2917
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2921
    :cond_0
    return-object v3
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1310
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    .line 1311
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ar:I

    .line 1313
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 1321
    :goto_0
    return-void

    .line 1317
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ar:I

    .line 1319
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->r()V

    .line 1320
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->d(Landroid/view/View;)V

    goto :goto_0
.end method

.method private static b(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 2614
    if-nez p0, :cond_1

    .line 2622
    :cond_0
    :goto_0
    return-void

    .line 2618
    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2619
    if-eqz v0, :cond_0

    .line 2620
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 7

    .prologue
    const v6, 0x7f12016d

    const v5, 0x7f120088

    const v4, 0x7f12002d

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2579
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2581
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2583
    iget-boolean v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v3, :cond_2

    .line 2584
    if-eq p2, v4, :cond_0

    if-eq p2, v5, :cond_0

    const v3, 0x7f12018a

    if-eq p2, v3, :cond_0

    const v3, 0x7f120181

    if-eq p2, v3, :cond_0

    const v3, 0x7f120108

    if-eq p2, v3, :cond_0

    const v3, 0x7f1201a0

    if-eq p2, v3, :cond_0

    const v3, 0x7f120146

    if-eq p2, v3, :cond_0

    const v3, 0x7f120197

    if-eq p2, v3, :cond_0

    const v3, 0x7f120102

    if-eq p2, v3, :cond_0

    const v3, 0x7f120105

    if-eq p2, v3, :cond_0

    if-ne p2, v6, :cond_3

    .line 2607
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 2608
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c01de

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2611
    :cond_1
    return-void

    .line 2598
    :cond_2
    if-eq p2, v4, :cond_0

    if-eq p2, v5, :cond_0

    const v3, 0x7f12018a

    if-eq p2, v3, :cond_0

    const v3, 0x7f120197

    if-eq p2, v3, :cond_0

    if-eq p2, v6, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->I()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2989
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ad:Z

    .line 2990
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ae:Z

    .line 2991
    if-eqz p1, :cond_3

    .line 2992
    const-string v0, "com.sec.android.app.sns3.facebook"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2994
    :cond_0
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ad:Z

    .line 2995
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x1d

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ah:Ljava/lang/String;

    .line 2996
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ah:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ah:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2997
    iput-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ae:Z

    .line 2999
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x1e

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    .line 3000
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x1f

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->af:Ljava/lang/String;

    .line 3002
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->M()V

    .line 3004
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3005
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    .line 3008
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3009
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->e(Ljava/lang/String;)V

    .line 3012
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->x()V

    .line 3016
    :cond_3
    return-void
.end method

.method private b(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 991
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1099
    :goto_0
    return-void

    .line 994
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 996
    if-eqz p1, :cond_18

    .line 997
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 998
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 999
    :goto_1
    if-ge v1, v4, :cond_2

    .line 1000
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1001
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 1002
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1003
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1008
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1009
    :goto_2
    if-ge v1, v4, :cond_4

    .line 1010
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1011
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 1012
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1013
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1018
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1019
    :goto_3
    if-ge v1, v4, :cond_6

    .line 1020
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1021
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 1022
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1023
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1028
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1029
    :goto_4
    if-ge v1, v4, :cond_19

    .line 1030
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1031
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_7

    .line 1032
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1033
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1029
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1041
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1042
    :goto_5
    if-ge v1, v4, :cond_c

    .line 1043
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1044
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1042
    :cond_9
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1047
    :cond_a
    iget-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v5, :cond_b

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 1050
    :cond_b
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1053
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1054
    :goto_7
    if-ge v1, v4, :cond_10

    .line 1055
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1056
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1054
    :cond_d
    :goto_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1059
    :cond_e
    iget-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v5, :cond_f

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 1062
    :cond_f
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1065
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1066
    :goto_9
    if-ge v1, v4, :cond_14

    .line 1067
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1068
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1066
    :cond_11
    :goto_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1071
    :cond_12
    iget-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v5, :cond_13

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 1074
    :cond_13
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1077
    :cond_14
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 1078
    :goto_b
    if-ge v1, v4, :cond_19

    .line 1079
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1080
    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1078
    :cond_15
    :goto_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1083
    :cond_16
    iget-boolean v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v5, :cond_17

    iget-object v5, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 1086
    :cond_17
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 1090
    :cond_18
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v0, :cond_19

    .line 1091
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1094
    :cond_19
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 1095
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 1096
    array-length v3, v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1098
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Z[Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3734
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3738
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "visible"

    aput-object v3, v2, v6

    const-string v3, "calendar_access_level>=500 AND deleted!=1 AND account_type!=\'LOCAL\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3740
    if-eqz v1, :cond_1

    .line 3742
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    move v0, v6

    .line 3746
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3750
    :goto_1
    return v0

    .line 3746
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v7

    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/detail/EventInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aZ:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    return-object p1
.end method

.method private c(Landroid/view/View;)V
    .locals 24

    .prologue
    .line 1605
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v4, :cond_0

    if-nez p1, :cond_1

    .line 2130
    :cond_0
    :goto_0
    return-void

    .line 1609
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    .line 1610
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 1611
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0f02dd

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    .line 1614
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_4

    const/4 v10, 0x1

    .line 1615
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x9

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 1616
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x8

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1617
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v5, 0x2

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1618
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x25

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 1619
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1620
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0xd

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 1621
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0xb

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 1622
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x23

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 1624
    const v4, 0x7f120088

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->P:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 1628
    const v4, 0x7f12002d

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1629
    const v5, 0x7f12002d

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->aj:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1630
    invoke-static {}, Lcom/android/calendar/dz;->D()Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->a:Z

    if-eqz v6, :cond_6

    const/4 v6, 0x2

    :goto_3
    or-int/lit8 v6, v6, 0x8

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/widget/TextView;ZI)V

    .line 1631
    invoke-static {v4}, Lcom/android/calendar/detail/EventInfoFragment;->c(Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v7

    .line 1632
    const/4 v5, 0x0

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v6

    const-class v8, Landroid/text/style/URLSpan;

    invoke-interface {v7, v5, v6, v8}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/URLSpan;

    .line 1633
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1634
    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 1635
    array-length v9, v5

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v9, :cond_7

    aget-object v11, v5, v6

    .line 1636
    new-instance v12, Lcom/android/calendar/detail/EventInfoFragment$SoundSpan;

    invoke-virtual {v11}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lcom/android/calendar/detail/EventInfoFragment$SoundSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v7, v11}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v13

    invoke-interface {v7, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    const/16 v14, 0x21

    invoke-virtual {v8, v12, v13, v11, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1635
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1614
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 1630
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    :cond_6
    const/4 v6, 0x0

    goto :goto_3

    .line 1639
    :cond_7
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1642
    :cond_8
    const v4, 0x7f120303

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1645
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x1b

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    .line 1647
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_9

    .line 1648
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->u()V

    .line 1649
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v4, :cond_9

    .line 1650
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->K()V

    .line 1651
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120179

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1652
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120088

    const/16 v6, 0x8

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1657
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x22

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1658
    if-eqz v4, :cond_a

    .line 1659
    new-instance v5, Lcom/android/calendar/detail/bk;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Lcom/android/calendar/detail/bk;-><init>(Lcom/android/calendar/detail/EventInfoFragment;Lcom/android/calendar/detail/ad;)V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-virtual {v5, v6}, Lcom/android/calendar/detail/bk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1664
    :cond_a
    const/4 v12, 0x0

    .line 1665
    if-eqz v10, :cond_b

    .line 1666
    const/4 v12, 0x1

    .line 1669
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bF:Z

    if-eqz v4, :cond_c

    .line 1670
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->O()V

    .line 1673
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    const/4 v11, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->c()Z

    move-result v4

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aq:Z

    if-eqz v4, :cond_1d

    const/4 v14, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const/4 v15, 0x0

    invoke-static {v4, v15}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v15

    invoke-static/range {v5 .. v15}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZZLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->am:Ljava/lang/String;

    .line 1675
    const v4, 0x7f12017d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->am:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1678
    if-eqz v16, :cond_1e

    .line 1679
    invoke-static/range {v16 .. v16}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v15

    .line 1684
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-static {v4, v15, v6, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/util/TimeZone;J)J

    move-result-wide v4

    .line 1686
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_10

    if-nez v10, :cond_10

    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->E()Z

    move-result v4

    if-nez v4, :cond_10

    .line 1688
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    const/4 v11, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->c()Z

    move-result v4

    if-eqz v4, :cond_1f

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aq:Z

    if-eqz v4, :cond_1f

    const/4 v14, 0x1

    :goto_7
    invoke-static/range {v5 .. v15}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJZZZZZLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v5

    .line 1692
    new-instance v4, Lcom/android/calendar/timezone/v;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object/from16 v0, v16

    invoke-direct {v4, v6, v0, v8, v9}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 1693
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;)I

    move-result v6

    .line 1695
    if-gez v6, :cond_20

    move-object/from16 v4, v16

    .line 1705
    :cond_d
    :goto_8
    const v6, 0x7f12017e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1706
    const v4, 0x7f12017f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1707
    const v4, 0x7f12017e

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1708
    const v4, 0x7f12017f

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1710
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v4, :cond_e

    .line 1711
    const v4, 0x7f1201b1

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1714
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    .line 1716
    new-instance v5, Lcom/android/calendar/timezone/v;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v5, v6, v4, v8, v9}, Lcom/android/calendar/timezone/v;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    .line 1717
    invoke-virtual {v5, v4}, Lcom/android/calendar/timezone/v;->a(Ljava/lang/String;)I

    move-result v6

    .line 1719
    if-gez v6, :cond_21

    .line 1728
    :cond_f
    :goto_9
    const v5, 0x7f12017c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1729
    const v4, 0x7f12017c

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1733
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-nez v4, :cond_12

    .line 1734
    const v4, 0x7f120183

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 1735
    const/4 v5, 0x0

    .line 1736
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v7, 0x21

    invoke-interface {v6, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_11

    .line 1737
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v7, 0x21

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1738
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-eqz v8, :cond_11

    .line 1739
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v5, v6, v7}, Lcom/android/calendar/gx;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 1740
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/android/calendar/gx;->a(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1743
    :cond_11
    if-eqz v5, :cond_22

    .line 1744
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1745
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1752
    :cond_12
    :goto_a
    if-nez v18, :cond_23

    .line 1753
    if-nez v17, :cond_4a

    .line 1754
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0073

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 1760
    :goto_b
    const v5, 0x7f120087

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v4}, Lcom/android/calendar/hj;->b(I)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1763
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f12018a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->z:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1764
    if-eqz v20, :cond_13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_24

    .line 1765
    :cond_13
    const v4, 0x7f12003c

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1962
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v4, :cond_32

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-nez v4, :cond_32

    .line 1963
    new-instance v4, Lcom/android/a/c;

    invoke-direct {v4}, Lcom/android/a/c;-><init>()V

    .line 1964
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_30

    .line 1965
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 1969
    :cond_14
    :goto_d
    new-instance v5, Landroid/text/format/Time;

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/detail/EventInfoFragment;->bM:Ljava/lang/Runnable;

    invoke-static {v6, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1970
    if-eqz v10, :cond_15

    .line 1971
    const-string v6, "UTC"

    iput-object v6, v5, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1973
    :cond_15
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1974
    invoke-virtual {v4, v5}, Lcom/android/a/c;->a(Landroid/text/format/Time;)V

    .line 1975
    const/4 v5, 0x1

    .line 1976
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v6

    if-nez v6, :cond_16

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_49

    .line 1977
    :cond_16
    const/4 v5, 0x0

    move v6, v5

    .line 1979
    :goto_e
    if-eqz v10, :cond_31

    const-string v5, "UTC"

    .line 1980
    :goto_f
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v7, v8, v4, v6, v5}, Lcom/android/calendar/dp;->a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1982
    const v5, 0x7f120182

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1983
    const v4, 0x7f120180

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1984
    const v4, 0x7f1201b3

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1986
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120158

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_17

    .line 1987
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f1201b5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1991
    :cond_17
    const v4, 0x7f120181

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->D:Landroid/widget/TextView;

    .line 1992
    const v4, 0x7f120181

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->D:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1998
    :cond_18
    :goto_10
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bp:Z

    if-nez v4, :cond_19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bq:Z

    if-nez v4, :cond_19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->br:Z

    if-nez v4, :cond_19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-eqz v4, :cond_34

    .line 1999
    :cond_19
    const v4, 0x7f1201a2

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2000
    const v4, 0x7f1201a4

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2001
    const v4, 0x7f120196

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2003
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bp:Z

    if-eqz v4, :cond_1a

    .line 2004
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_1a

    .line 2005
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 2065
    :cond_1a
    :goto_11
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v4

    if-eqz v4, :cond_48

    .line 2066
    const/4 v4, 0x0

    .line 2068
    :goto_12
    if-eqz v4, :cond_1b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x2000

    if-le v5, v6, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->aA:Z

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-nez v5, :cond_1b

    .line 2070
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aB:Ljava/lang/String;

    .line 2071
    const/4 v5, 0x0

    const/16 v6, 0x2000

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 2072
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->F:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2073
    new-instance v5, Landroid/text/SpannableString;

    const-string v6, "..."

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2074
    new-instance v6, Landroid/text/style/UnderlineSpan;

    invoke-direct {v6}, Landroid/text/style/UnderlineSpan;-><init>()V

    const/4 v7, 0x0

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2075
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->F:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1b
    move-object v5, v4

    .line 2078
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    if-eqz v4, :cond_43

    if-eqz v5, :cond_43

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_43

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-nez v4, :cond_43

    .line 2079
    const v6, 0x7f12016d

    const v4, 0x7f12016d

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2080
    const v4, 0x7f12010d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2082
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/calendar/dz;->D()Z

    move-result v4

    if-nez v4, :cond_3d

    const/4 v4, 0x1

    :goto_13
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->a:Z

    if-eqz v5, :cond_3e

    const/4 v5, 0x2

    :goto_14
    or-int/lit8 v5, v5, 0x8

    invoke-static {v6, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/widget/TextView;ZI)V

    .line 2083
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_1c

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_40

    .line 2084
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/android/calendar/detail/EventInfoFragment;->c(Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v6

    .line 2085
    const/4 v4, 0x0

    invoke-interface {v6}, Landroid/text/Spannable;->length()I

    move-result v5

    const-class v7, Landroid/text/style/URLSpan;

    invoke-interface {v6, v4, v5, v7}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/text/style/URLSpan;

    .line 2086
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2087
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 2088
    array-length v8, v4

    const/4 v5, 0x0

    :goto_15
    if-ge v5, v8, :cond_3f

    aget-object v9, v4, v5

    .line 2089
    new-instance v10, Lcom/android/calendar/detail/EventInfoFragment$SoundSpan;

    invoke-virtual {v9}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/android/calendar/detail/EventInfoFragment$SoundSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v9}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    invoke-interface {v6, v9}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    const/16 v12, 0x21

    invoke-virtual {v7, v10, v11, v9, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2088
    add-int/lit8 v5, v5, 0x1

    goto :goto_15

    .line 1673
    :cond_1d
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 1681
    :cond_1e
    const-string v4, "GMT"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v15

    goto/16 :goto_6

    .line 1688
    :cond_1f
    const/4 v14, 0x0

    goto/16 :goto_7

    .line 1698
    :cond_20
    invoke-virtual {v4, v6}, Lcom/android/calendar/timezone/v;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/timezone/w;

    invoke-virtual {v4}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1699
    const/16 v6, 0x29

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1700
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-le v6, v7, :cond_d

    .line 1701
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 1722
    :cond_21
    invoke-virtual {v5, v6}, Lcom/android/calendar/timezone/v;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/timezone/w;

    invoke-virtual {v4}, Lcom/android/calendar/timezone/w;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1723
    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1724
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_f

    .line 1725
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    .line 1747
    :cond_22
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    :cond_23
    move/from16 v4, v18

    .line 1757
    goto/16 :goto_b

    .line 1767
    :cond_24
    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v4

    if-nez v4, :cond_25

    .line 1768
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    if-eqz v4, :cond_25

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    if-eqz v4, :cond_25

    .line 1770
    new-instance v4, Lcom/android/calendar/a/a/i;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    int-to-double v6, v5

    const-wide v8, 0x412e848000000000L    # 1000000.0

    div-double/2addr v6, v8

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    int-to-double v8, v5

    const-wide v12, 0x412e848000000000L    # 1000000.0

    div-double/2addr v8, v12

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/android/calendar/a/a/i;-><init>(DD)V

    .line 1771
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v4, v0}, Lcom/android/calendar/a/a/a/h;->a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V

    .line 1775
    :cond_25
    invoke-static {}, Lcom/android/calendar/dz;->i()Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 1776
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1778
    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 1779
    const v4, 0x7f12019b

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1781
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    if-nez v4, :cond_26

    .line 1782
    new-instance v4, Lcom/android/calendar/ex;

    invoke-direct {v4}, Lcom/android/calendar/ex;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    .line 1785
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/android/calendar/ex;->a(Ljava/lang/String;)Z

    .line 1787
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    invoke-virtual {v4}, Lcom/android/calendar/ex;->b()Ljava/lang/String;

    move-result-object v5

    .line 1788
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    invoke-virtual {v4}, Lcom/android/calendar/ex;->c()Ljava/util/ArrayList;

    move-result-object v6

    .line 1790
    const/4 v4, 0x1

    .line 1792
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.baidu.BaiduMap.samsung"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1799
    :goto_16
    if-eqz v5, :cond_28

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_28

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    if-eqz v7, :cond_28

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    if-eqz v7, :cond_28

    if-eqz v4, :cond_28

    .line 1801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const v7, 0x7f12018c

    invoke-virtual {v4, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 1802
    if-eqz v4, :cond_27

    .line 1803
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1805
    :cond_27
    const v4, 0x7f12019b

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v7}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1806
    const v4, 0x7f12019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->I:Landroid/widget/Button;

    .line 1807
    const v4, 0x7f1200ea

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->J:Landroid/widget/Button;

    .line 1808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->J:Landroid/widget/Button;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1809
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->J:Landroid/widget/Button;

    new-instance v7, Lcom/android/calendar/detail/ae;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v5}, Lcom/android/calendar/detail/ae;-><init>(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833
    :cond_28
    if-eqz v6, :cond_2a

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2a

    .line 1834
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const v5, 0x7f12018c

    invoke-virtual {v4, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 1835
    if-eqz v4, :cond_29

    .line 1836
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1838
    :cond_29
    const v4, 0x7f12019b

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1839
    const v4, 0x7f1200ea

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->J:Landroid/widget/Button;

    .line 1840
    const v4, 0x7f12019c

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->I:Landroid/widget/Button;

    .line 1841
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->I:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 1842
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->I:Landroid/widget/Button;

    new-instance v5, Lcom/android/calendar/detail/af;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6}, Lcom/android/calendar/detail/af;-><init>(Lcom/android/calendar/detail/EventInfoFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1941
    :cond_2a
    :goto_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    new-instance v5, Lcom/android/calendar/detail/ak;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/calendar/detail/ak;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1957
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f12003c

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 1958
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f12005d

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto/16 :goto_c

    .line 1794
    :catch_0
    move-exception v4

    .line 1795
    sget-object v4, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v7, "isSupportBaiduMap false"

    invoke-static {v4, v7}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1796
    const/4 v4, 0x0

    goto/16 :goto_16

    .line 1919
    :cond_2b
    const v4, 0x7f12019b

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto :goto_17

    .line 1922
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1923
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 1925
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1926
    if-eqz v4, :cond_2a

    .line 1927
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    const-string v7, "geo:0,0"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1928
    const/high16 v6, 0x10000

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 1929
    if-eqz v4, :cond_2f

    .line 1930
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bu:Z

    if-eqz v4, :cond_2e

    .line 1931
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/calendar/dz;->D()Z

    move-result v4

    if-nez v4, :cond_2d

    const/4 v4, 0x1

    :goto_18
    const/16 v6, 0xc

    invoke-static {v5, v4, v6}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/widget/TextView;ZI)V

    goto/16 :goto_17

    :cond_2d
    const/4 v4, 0x0

    goto :goto_18

    .line 1933
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    sget-object v5, Lcom/android/calendar/detail/EventInfoFragment;->n:Ljava/util/regex/Pattern;

    const-string v6, "geo:0,0?q="

    invoke-static {v4, v5, v6}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 1936
    :cond_2f
    sget-object v4, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v5, "Linkify supported Activity is not exist"

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_17

    .line 1966
    :cond_30
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 1967
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->f(Ljava/lang/String;)Lcom/android/a/c;

    move-result-object v4

    goto/16 :goto_d

    .line 1979
    :cond_31
    const/4 v5, 0x0

    goto/16 :goto_f

    .line 1994
    :cond_32
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v4, :cond_33

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-eqz v4, :cond_18

    .line 1995
    :cond_33
    const v4, 0x7f120180

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto/16 :goto_10

    .line 2009
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120184

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewStub;

    .line 2010
    if-eqz v4, :cond_35

    .line 2011
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2013
    :cond_35
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->H()Z

    move-result v4

    if-eqz v4, :cond_39

    .line 2014
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x1c

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    .line 2015
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090031

    invoke-static {v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/content/res/Resources;I)Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bv:Ljava/util/ArrayList;

    .line 2016
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->bv:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_36

    .line 2017
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bv:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    .line 2019
    :cond_36
    const v5, 0x7f1201a7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bv:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->by:I

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2020
    const v4, 0x7f1201a6

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2021
    const v4, 0x7f1201a2

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2040
    :goto_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x14

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    .line 2041
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    if-lez v4, :cond_37

    .line 2043
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    .line 2046
    :cond_37
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090039

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 2047
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->bx:Ljava/util/ArrayList;

    .line 2048
    const v5, 0x7f1201a5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bx:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->bA:I

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2050
    const v4, 0x7f1201a4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2051
    const v4, 0x7f120105

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->C:Landroid/widget/TextView;

    .line 2052
    const v4, 0x7f120105

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->C:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2056
    if-eqz v23, :cond_38

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_38

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 2057
    :cond_38
    const v4, 0x7f1201a8

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto/16 :goto_11

    .line 2023
    :cond_39
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v5, 0x13

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    .line 2025
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 2026
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09003c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 2030
    :goto_1a
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->bw:Ljava/util/ArrayList;

    .line 2031
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->bw:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_3a

    .line 2032
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bw:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    .line 2034
    :cond_3a
    const v5, 0x7f1201a3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bw:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->bz:I

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v4}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2035
    const v4, 0x7f1201a2

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2036
    const v4, 0x7f120102

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->B:Landroid/widget/TextView;

    .line 2037
    const v4, 0x7f120102

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->B:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_19

    .line 2028
    :cond_3b
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    goto :goto_1a

    .line 2059
    :cond_3c
    const v4, 0x7f1201a8

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2060
    const v4, 0x7f1201aa

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v4, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_11

    .line 2082
    :cond_3d
    const/4 v4, 0x0

    goto/16 :goto_13

    :cond_3e
    const/4 v5, 0x0

    goto/16 :goto_14

    .line 2092
    :cond_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094
    :cond_40
    const v4, 0x7f120191

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2099
    :goto_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->al:Ljava/lang/String;

    .line 2100
    if-eqz v4, :cond_42

    .line 2101
    const-string v5, "My calendar"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_44

    .line 2102
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f02ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2115
    :cond_41
    :goto_1c
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v5

    if-eqz v5, :cond_42

    .line 2116
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->af:Ljava/lang/String;

    if-eqz v5, :cond_42

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->af:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_42

    .line 2117
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->af:Ljava/lang/String;

    .line 2121
    :cond_42
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->P:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2124
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v4, :cond_0

    .line 2125
    invoke-direct/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->K()V

    .line 2126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120088

    const/16 v6, 0x8

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120179

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2128
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f120189

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto/16 :goto_0

    .line 2096
    :cond_43
    const v4, 0x7f120191

    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto :goto_1b

    .line 2103
    :cond_44
    const-string v5, "My calendars (personal)"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_45

    .line 2104
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f02bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1c

    .line 2105
    :cond_45
    const-string v5, "My calendars (KNOX)"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_47

    .line 2106
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v4}, Lcom/android/calendar/hj;->I(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2107
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_46

    .line 2108
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f02bc

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1c

    .line 2110
    :cond_46
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f02bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "KNOX"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1c

    .line 2112
    :cond_47
    const-string v5, "My calendars (KNOX II)"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_41

    .line 2113
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f02bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "KNOX II"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1c

    :cond_48
    move-object/from16 v4, v19

    goto/16 :goto_12

    :cond_49
    move v6, v5

    goto/16 :goto_e

    :cond_4a
    move/from16 v4, v17

    goto/16 :goto_b
.end method

.method static synthetic c(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->f()V

    return-void
.end method

.method static synthetic c(Lcom/android/calendar/detail/EventInfoFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->e(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3031
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    .line 3032
    if-eqz p1, :cond_0

    .line 3033
    const-string v0, "com.android.exchange"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3034
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    .line 3037
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/detail/EventInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bH:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->V:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic d(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aB:Ljava/lang/String;

    return-object v0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 3604
    const-string v7, ""

    .line 3606
    const-string v0, "content://com.sec.android.app.sns3.sp.facebook/friends"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 3607
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/calendar/detail/EventInfoFragment;->u:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "friend_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v1

    .line 3610
    if-eqz v1, :cond_1

    .line 3612
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3613
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3616
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3619
    :goto_1
    return-object v0

    .line 3616
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-object v0, v7

    goto :goto_0

    :cond_1
    move-object v0, v7

    goto :goto_1
.end method

.method private d(Landroid/view/View;)V
    .locals 10

    .prologue
    const v4, 0x7f12019a

    const v3, 0x7f120196

    const/4 v9, 0x1

    const/4 v2, 0x0

    const/16 v8, 0x8

    .line 2447
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 2449
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->H()Z

    move-result v1

    if-eqz v1, :cond_1

    if-ne v0, v9, :cond_1

    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-eqz v1, :cond_1

    move v1, v2

    .line 2455
    :goto_0
    if-lez v1, :cond_2

    .line 2456
    invoke-static {p1, v4, v2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2457
    invoke-static {p1, v3, v2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2459
    const v0, 0x7f120199

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2461
    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v4, 0x7f120197

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f01d5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 2464
    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    iget-object v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2472
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->x()V

    .line 2474
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2475
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v3, 0x7f120178

    invoke-static {v0, v3, v2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2476
    const v0, 0x7f1201c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bR:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2477
    const v0, 0x7f1201c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bR:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2478
    const v0, 0x7f1201c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bR:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2480
    if-ge v1, v9, :cond_0

    .line 2481
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201c2

    invoke-static {v0, v1, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2482
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201c3

    invoke-static {v0, v1, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2483
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201c4

    invoke-static {v0, v1, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2484
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201c5

    invoke-static {v0, v1, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2487
    :cond_0
    return-void

    .line 2451
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v1

    if-eqz v1, :cond_3

    if-ne v0, v9, :cond_3

    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-eqz v1, :cond_3

    move v1, v2

    .line 2452
    goto/16 :goto_0

    .line 2468
    :cond_2
    invoke-static {p1, v4, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2469
    invoke-static {p1, v3, v8}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto :goto_1

    :cond_3
    move v1, v0

    goto/16 :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/detail/EventInfoFragment;Z)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Z)V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 28

    .prologue
    .line 653
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v12

    .line 654
    if-nez v12, :cond_0

    .line 866
    :goto_0
    return-void

    .line 658
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 660
    invoke-virtual {v12}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v13

    .line 661
    const/4 v2, 0x2

    invoke-virtual {v13, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 663
    invoke-virtual {v13}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    .line 664
    const v2, 0x3ecccccd    # 0.4f

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 665
    const/16 v2, 0x33

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 667
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/AllInOneActivity;

    .line 668
    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 670
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/detail/EventInfoFragment;->be:I

    iput v3, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 671
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    iput v3, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 673
    const/4 v11, 0x0

    .line 674
    sget-object v10, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    .line 675
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v4, 0x7f1201ac

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 676
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v5, 0x7f1201ad

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 677
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v6, 0x7f1201ae

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 679
    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    .line 680
    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    .line 682
    const/4 v7, 0x0

    .line 683
    new-instance v6, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v6, v8, v9, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 684
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-eqz v8, :cond_1

    invoke-static {v2}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 685
    invoke-static {v2}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v7

    .line 686
    invoke-static {v2}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v6

    .line 689
    :cond_1
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    const/4 v8, 0x1

    .line 690
    :goto_1
    const v9, 0x7f0a0003

    invoke-virtual {v15, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    .line 691
    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->i()Z

    move-result v18

    .line 693
    if-nez v18, :cond_2

    if-eqz v8, :cond_a

    :cond_2
    const/4 v2, 0x0

    .line 694
    :goto_2
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v19

    const v20, 0x7f0c0215

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    mul-int/lit8 v20, v20, 0x2

    sub-int v19, v19, v20

    sub-int v19, v19, v2

    .line 696
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v20

    .line 698
    const v2, 0x7f0c01d4

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 699
    const v2, 0x7f0c01d3

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 700
    const v2, 0x7f0c01d2

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 701
    if-eqz v7, :cond_b

    const/4 v2, 0x0

    .line 702
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3

    if-eqz v9, :cond_c

    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const-string v24, "status_bar_height"

    invoke-static/range {v24 .. v24}, Lcom/android/calendar/common/b/a;->b(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 704
    :goto_4
    const v24, 0x7f0c01d5

    move/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 706
    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    add-int v24, v24, v9

    move/from16 v0, v24

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 708
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    move/from16 v24, v0

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    .line 709
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    move/from16 v25, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    .line 719
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    move/from16 v26, v0

    if-nez v26, :cond_5

    .line 720
    :cond_4
    const/16 v26, -0x2

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    .line 723
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    move/from16 v26, v0

    if-nez v26, :cond_7

    .line 724
    :cond_6
    const/16 v26, -0x2

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    .line 727
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    move/from16 v26, v0

    const/16 v27, -0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    move/from16 v26, v0

    const/16 v27, -0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_d

    .line 728
    :cond_8
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v2, v19, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 729
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v2, v20, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 730
    sget-object v7, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    move v6, v11

    .line 832
    :goto_5
    invoke-virtual {v13, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 834
    sget-object v2, Lcom/android/calendar/detail/ba;->a:[I

    invoke-virtual {v7}, Lcom/android/calendar/detail/bi;->ordinal()I

    move-result v7

    aget v2, v2, v7

    packed-switch v2, :pswitch_data_0

    .line 865
    :goto_6
    invoke-virtual {v12}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 689
    :cond_9
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 693
    :cond_a
    const v2, 0x7f0c0317

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    goto/16 :goto_2

    .line 701
    :cond_b
    const v2, 0x7f0d000c

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto/16 :goto_3

    .line 702
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 732
    :cond_d
    if-nez v7, :cond_10

    if-nez v8, :cond_10

    if-nez v18, :cond_10

    move/from16 v0, v24

    move/from16 v1, v19

    if-le v0, v1, :cond_10

    .line 733
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v2, v24, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 734
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v25, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 735
    sget-object v7, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    .line 736
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v2, v25, v2

    sub-int v2, v2, v23

    .line 738
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v8, v10

    if-ge v8, v9, :cond_f

    .line 739
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v10, v6, Landroid/graphics/Rect;->top:I

    sub-int v10, v9, v10

    sub-int/2addr v8, v10

    add-int/2addr v2, v8

    .line 740
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v9, v6

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 746
    :cond_e
    :goto_7
    add-int v6, v2, v22

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v8, v15

    if-le v6, v8, :cond_1c

    .line 747
    sget-object v7, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    move v6, v2

    goto :goto_5

    .line 741
    :cond_f
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v8, v9

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v8, v9

    move/from16 v0, v17

    if-le v8, v0, :cond_e

    .line 742
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v17, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v10

    sub-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 743
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v17, v6

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v6, v8

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_7

    .line 749
    :cond_10
    if-eqz v7, :cond_14

    iget v9, v6, Landroid/graphics/Rect;->top:I

    add-int v9, v9, v25

    iget v11, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v11

    if-lez v9, :cond_14

    iget v9, v6, Landroid/graphics/Rect;->top:I

    add-int v9, v9, v25

    iget v11, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v11, v11, 0x3

    add-int/2addr v9, v11

    move/from16 v0, v17

    if-le v9, v0, :cond_14

    .line 751
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v24, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 752
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v2, v25, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 753
    sget-object v7, Lcom/android/calendar/detail/bi;->d:Lcom/android/calendar/detail/bi;

    .line 754
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v2, v23

    .line 756
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    if-gez v8, :cond_13

    .line 757
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 758
    iget v8, v6, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 764
    :cond_11
    :goto_8
    move/from16 v0, v21

    if-lt v2, v0, :cond_12

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v6, v8

    add-int/2addr v6, v2

    add-int v6, v6, v22

    sub-int v8, v16, v21

    if-le v6, v8, :cond_1c

    .line 766
    :cond_12
    sget-object v7, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    move v6, v2

    goto/16 :goto_5

    .line 759
    :cond_13
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v8, v9

    move/from16 v0, v16

    if-le v8, v0, :cond_11

    .line 760
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v9, v6, Landroid/graphics/Rect;->left:I

    sub-int v9, v16, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v9, v10

    sub-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 761
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int v8, v16, v8

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v8, v9

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_8

    .line 768
    :cond_14
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/detail/EventInfoFragment;->bi:Lcom/android/calendar/al;

    invoke-virtual {v9}, Lcom/android/calendar/al;->g()I

    move-result v9

    packed-switch v9, :pswitch_data_1

    move-object v7, v10

    .line 816
    :goto_9
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v25, v8

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 817
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v8, v25, v8

    sub-int v8, v8, v23

    .line 818
    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v9, v10

    iget v10, v6, Landroid/graphics/Rect;->top:I

    sub-int v10, v17, v10

    if-le v9, v10, :cond_1b

    .line 819
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v17, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v10

    sub-int/2addr v2, v9

    add-int/2addr v2, v8

    .line 820
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v17, v6

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v6, v8

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 826
    :goto_a
    add-int v6, v2, v22

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v8, v15

    if-le v6, v8, :cond_1c

    .line 827
    sget-object v7, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    move v6, v2

    goto/16 :goto_5

    .line 770
    :pswitch_0
    div-int/lit8 v7, v16, 0x2

    iget v8, v6, Landroid/graphics/Rect;->left:I

    add-int v8, v8, v24

    if-ge v7, v8, :cond_15

    .line 771
    sget-object v7, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    .line 772
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v8, v24, v8

    add-int v8, v8, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 774
    :cond_15
    sget-object v7, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    .line 775
    sub-int v8, v24, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 780
    :pswitch_1
    if-eqz v7, :cond_17

    .line 781
    div-int/lit8 v7, v16, 0x2

    iget v8, v6, Landroid/graphics/Rect;->left:I

    add-int v8, v8, v24

    if-ge v7, v8, :cond_16

    .line 782
    sget-object v7, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    .line 783
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v8, v24, v8

    add-int v8, v8, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 785
    :cond_16
    sget-object v7, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    .line 786
    sub-int v8, v24, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 789
    :cond_17
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v24, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 790
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v7, :cond_18

    .line 791
    move/from16 v0, v24

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 793
    :cond_18
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v0, v24

    if-le v0, v7, :cond_19

    sget-object v7, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    goto/16 :goto_9

    :cond_19
    sget-object v7, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    goto/16 :goto_9

    .line 798
    :pswitch_2
    if-eqz v8, :cond_1a

    .line 799
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v19, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 800
    sget-object v7, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    goto/16 :goto_9

    .line 802
    :cond_1a
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v24, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 803
    sget-object v7, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    goto/16 :goto_9

    .line 808
    :pswitch_3
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v19, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 809
    sget-object v7, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    goto/16 :goto_9

    .line 821
    :cond_1b
    iget v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    if-ge v6, v2, :cond_1d

    .line 822
    iget v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v6, v2

    add-int/2addr v6, v8

    .line 823
    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    move v2, v6

    goto/16 :goto_a

    .line 836
    :pswitch_4
    const/4 v2, 0x4

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 837
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 838
    const/4 v2, 0x4

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 842
    :pswitch_5
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 843
    const/16 v4, 0x30

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 844
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 845
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 849
    :pswitch_6
    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 850
    const/16 v3, 0x30

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 851
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 852
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 856
    :pswitch_7
    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 857
    const/4 v3, 0x3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 858
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v3, v4, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 859
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_1c
    move v6, v2

    goto/16 :goto_5

    :cond_1d
    move v2, v8

    goto/16 :goto_a

    .line 834
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 768
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic e(Lcom/android/calendar/detail/EventInfoFragment;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    .prologue
    const v3, 0x7f12019e

    const/4 v2, 0x0

    .line 3623
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3624
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120184

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 3625
    if-eqz v0, :cond_0

    .line 3626
    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 3628
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-static {v0, v3, v2}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 3629
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f12019f

    invoke-direct {p0, v0, v1, p1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 3630
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aU:Ljava/lang/String;

    .line 3634
    :goto_0
    return-void

    .line 3632
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-static {v0, v3, v1}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/detail/EventInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aG:Z

    return p1
.end method

.method static synthetic f(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->F:Landroid/widget/TextView;

    return-object v0
.end method

.method private f(Ljava/lang/String;)Lcom/android/a/c;
    .locals 13

    .prologue
    const/16 v12, 0x7f4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3754
    new-instance v3, Lcom/android/a/c;

    invoke-direct {v3}, Lcom/android/a/c;-><init>()V

    .line 3755
    const-string v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 3756
    aget-object v4, v0, v1

    .line 3757
    aget-object v0, v0, v2

    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 3759
    const/4 v0, 0x2

    new-array v6, v0, [Landroid/text/format/Time;

    .line 3760
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 3761
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v5, v0

    invoke-virtual {v7, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    move v0, v1

    .line 3764
    :goto_0
    array-length v8, v6

    if-ge v0, v8, :cond_1

    .line 3765
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    aput-object v8, v6, v0

    .line 3766
    aget-object v8, v6, v0

    aget-object v9, v5, v0

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 3767
    iget-object v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    if-eqz v8, :cond_0

    .line 3768
    iget-object v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    aget-object v9, v6, v0

    iget v9, v9, Landroid/text/format/Time;->year:I

    aget-object v10, v6, v0

    iget v10, v10, Landroid/text/format/Time;->month:I

    aget-object v11, v6, v0

    iget v11, v11, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v8, v9, v10, v11}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 3769
    aget-object v8, v6, v0

    iget-object v9, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    invoke-virtual {v9}, Lcom/android/calendar/d/a/a;->a()I

    move-result v9

    iput v9, v8, Landroid/text/format/Time;->year:I

    .line 3770
    aget-object v8, v6, v0

    iget-object v9, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    invoke-virtual {v9}, Lcom/android/calendar/d/a/a;->b()I

    move-result v9

    iput v9, v8, Landroid/text/format/Time;->month:I

    .line 3771
    aget-object v8, v6, v0

    iget-object v9, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    invoke-virtual {v9}, Lcom/android/calendar/d/a/a;->c()I

    move-result v9

    iput v9, v8, Landroid/text/format/Time;->monthDay:I

    .line 3764
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3774
    :cond_1
    aget-object v0, v6, v1

    iget v0, v0, Landroid/text/format/Time;->year:I

    aget-object v4, v6, v2

    iget v4, v4, Landroid/text/format/Time;->year:I

    if-eq v0, v4, :cond_4

    aget-object v0, v6, v1

    iget v0, v0, Landroid/text/format/Time;->month:I

    aget-object v4, v6, v2

    iget v4, v4, Landroid/text/format/Time;->month:I

    if-ne v0, v4, :cond_4

    .line 3775
    const/4 v0, 0x7

    iput v0, v3, Lcom/android/a/c;->b:I

    .line 3776
    aget-object v0, v6, v2

    iget v0, v0, Landroid/text/format/Time;->year:I

    aget-object v4, v6, v1

    iget v4, v4, Landroid/text/format/Time;->year:I

    sub-int/2addr v0, v4

    iput v0, v3, Lcom/android/a/c;->e:I

    .line 3777
    iget v0, v7, Landroid/text/format/Time;->year:I

    iget v4, v3, Lcom/android/a/c;->e:I

    add-int/2addr v0, v4

    if-le v0, v12, :cond_2

    move v1, v2

    .line 3797
    :cond_2
    :goto_1
    if-nez v1, :cond_3

    .line 3798
    invoke-virtual {v7}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 3801
    :cond_3
    return-object v3

    .line 3781
    :cond_4
    const/4 v0, 0x6

    iput v0, v3, Lcom/android/a/c;->b:I

    .line 3783
    aget-object v0, v6, v2

    iget v0, v0, Landroid/text/format/Time;->month:I

    aget-object v4, v6, v1

    iget v4, v4, Landroid/text/format/Time;->month:I

    sub-int/2addr v0, v4

    .line 3784
    aget-object v4, v6, v2

    iget v4, v4, Landroid/text/format/Time;->year:I

    aget-object v5, v6, v1

    iget v5, v5, Landroid/text/format/Time;->year:I

    sub-int/2addr v4, v5

    .line 3785
    mul-int/lit8 v4, v4, 0xc

    add-int/2addr v0, v4

    iput v0, v3, Lcom/android/a/c;->e:I

    .line 3787
    iget v0, v3, Lcom/android/a/c;->e:I

    div-int/lit8 v0, v0, 0xc

    .line 3788
    iget v4, v3, Lcom/android/a/c;->e:I

    rem-int/lit8 v4, v4, 0xc

    .line 3789
    iget v5, v7, Landroid/text/format/Time;->month:I

    add-int/2addr v4, v5

    const/16 v5, 0xb

    if-le v4, v5, :cond_5

    .line 3790
    add-int/lit8 v0, v0, 0x1

    .line 3792
    :cond_5
    iget v4, v7, Landroid/text/format/Time;->year:I

    add-int/2addr v0, v4

    if-le v0, v12, :cond_2

    move v1, v2

    .line 3793
    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 869
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_0

    .line 881
    :goto_0
    return-void

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->L:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->L:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 875
    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    .line 876
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bg:I

    if-le v0, v1, :cond_1

    .line 877
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bg:I

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    .line 880
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->e()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/detail/EventInfoFragment;Z)Z
    .locals 0

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bI:Z

    return p1
.end method

.method static synthetic g(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/detail/EventInfoFragment;)J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    return-wide v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 933
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 934
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 936
    array-length v0, v0

    if-nez v0, :cond_0

    .line 937
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 938
    const v1, 0x7f0f020e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f020d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/detail/bh;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/bh;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/detail/bg;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/bg;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 966
    :goto_0
    return-void

    .line 958
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 959
    const-string v1, "market://details?id=com.facebook.katana"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 961
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 962
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic i(Lcom/android/calendar/detail/EventInfoFragment;)J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    return-wide v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 969
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 970
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->h()V

    .line 988
    :goto_0
    return-void

    .line 974
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 975
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 977
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 978
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fb://profile/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 984
    :goto_1
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 985
    :catch_0
    move-exception v0

    .line 986
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v1, "Error: Could not find Facebook activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 980
    :cond_1
    const-string v1, "fb://profile"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1
.end method

.method static synthetic j(Lcom/android/calendar/detail/EventInfoFragment;)J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    return-wide v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1102
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->a(Z[Ljava/lang/String;)V

    .line 1103
    return-void
.end method

.method private k()V
    .locals 18

    .prologue
    .line 1159
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1160
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/calendar/detail/EventInfoFragment;->bD:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x258

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 1171
    :cond_0
    :goto_0
    return-void

    .line 1163
    :cond_1
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->bD:J

    .line 1165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->bi:Lcom/android/calendar/al;

    const-wide/16 v4, 0x8

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 1168
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v2, :cond_0

    .line 1169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aY:Z

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/detail/EventInfoFragment;)J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bE:J

    return-wide v0
.end method

.method private l()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 1185
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    if-nez v0, :cond_1

    .line 1201
    :cond_0
    :goto_0
    return-void

    .line 1189
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1190
    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->bC:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x258

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    .line 1193
    iput-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bC:J

    .line 1195
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 1197
    :goto_1
    new-instance v2, Lcom/android/calendar/cj;

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {v2, v3, v0}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    iput-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    .line 1198
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    invoke-virtual {v0, v1}, Lcom/android/calendar/cj;->a(Z)V

    .line 1199
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    invoke-virtual {v0, p0}, Lcom/android/calendar/cj;->a(Lcom/android/calendar/cq;)V

    .line 1200
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    iget-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    const/4 v8, -0x1

    iget-object v9, p0, Lcom/android/calendar/detail/EventInfoFragment;->bN:Ljava/lang/Runnable;

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/cj;->a(JJJILjava/lang/Runnable;)V

    goto :goto_0

    .line 1195
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private m()V
    .locals 18

    .prologue
    .line 1204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->bi:Lcom/android/calendar/al;

    const-wide/32 v4, 0x200000

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 1206
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v2, :cond_0

    .line 1207
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 1209
    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->i()V

    return-void
.end method

.method static synthetic n(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ag:Ljava/lang/String;

    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1212
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    .line 1213
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "enterprise_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1214
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1220
    :goto_0
    return-void

    .line 1217
    :cond_0
    new-instance v0, Lcom/android/calendar/gr;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    .line 1218
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->c(J)V

    .line 1219
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0
.end method

.method static synthetic o(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    return-object v0
.end method

.method private o()V
    .locals 4

    .prologue
    .line 1223
    new-instance v0, Lcom/android/calendar/bm;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/bm;-><init>(Landroid/app/Activity;)V

    .line 1224
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/bm;->a(J)V

    .line 1225
    return-void
.end method

.method static synthetic p(Lcom/android/calendar/detail/EventInfoFragment;)I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    return v0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 1228
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1229
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 1231
    const-string v2, ""

    .line 1232
    iget-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v2, :cond_0

    .line 1233
    const v2, 0x7f0f01d7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1237
    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1238
    return-void

    .line 1235
    :cond_0
    const v2, 0x7f0f015d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic q(Lcom/android/calendar/detail/EventInfoFragment;)I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    return v0
.end method

.method private declared-synchronized q()Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v8

    .line 1306
    :goto_0
    monitor-exit p0

    return v0

    .line 1250
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1251
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1252
    if-eqz v0, :cond_2

    move v0, v8

    .line 1253
    goto :goto_0

    .line 1256
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    .line 1257
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1258
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v2, 0x25

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1259
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_3
    move v0, v9

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    .line 1260
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->B()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bp:Z

    .line 1261
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->C()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bq:Z

    .line 1262
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->D()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->br:Z

    .line 1263
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xf

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_9

    move v0, v9

    :goto_2
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->au:Z

    .line 1264
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->av:I

    .line 1265
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x11

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aw:Ljava/lang/String;

    .line 1266
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aC:I

    .line 1267
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x1a

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aD:I

    .line 1268
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Y:Ljava/lang/String;

    .line 1270
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x15

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1271
    if-eqz v0, :cond_a

    .line 1272
    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    .line 1278
    :goto_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aq:Z

    .line 1279
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    .line 1280
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/android/calendar/detail/EventInfoFragment;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    sget v1, Lcom/android/calendar/detail/EventInfoFragment;->s:I

    if-le v0, v1, :cond_4

    .line 1282
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    sget v1, Lcom/android/calendar/detail/EventInfoFragment;->s:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b

    move v0, v9

    :goto_4
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aq:Z

    .line 1286
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1287
    if-eqz v0, :cond_c

    .line 1288
    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->al:Ljava/lang/String;

    .line 1294
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "calendar_displayName=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v10, p0, Lcom/android/calendar/detail/EventInfoFragment;->al:Ljava/lang/String;

    aput-object v10, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x1b

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    move v0, v9

    :goto_6
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aa:Z

    .line 1298
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, ""

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_5
    move v0, v9

    :goto_7
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    .line 1299
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_f

    move v0, v9

    :goto_8
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ab:Z

    .line 1300
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x1f4

    if-lt v0, v1, :cond_10

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aa:Z

    if-nez v0, :cond_10

    move v0, v9

    :goto_9
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    .line 1301
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    if-eqz v0, :cond_11

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Z:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ab:Z

    if-eqz v0, :cond_11

    :cond_6
    move v0, v9

    :goto_a
    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ao:Z

    .line 1302
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_7

    move v8, v9

    :cond_7
    iput-boolean v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->ap:Z

    .line 1303
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v1, 0x18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1304
    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->c(Ljava/lang/String;)V

    .line 1305
    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->b(Ljava/lang/String;)V

    move v0, v9

    .line 1306
    goto/16 :goto_0

    :cond_8
    move v0, v8

    .line 1259
    goto/16 :goto_1

    :cond_9
    move v0, v8

    .line 1263
    goto/16 :goto_2

    .line 1274
    :cond_a
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    .line 1246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_b
    move v0, v8

    .line 1282
    goto/16 :goto_4

    .line 1290
    :cond_c
    :try_start_2
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->al:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_5

    :cond_d
    move v0, v8

    .line 1297
    goto/16 :goto_6

    :cond_e
    move v0, v8

    .line 1298
    goto :goto_7

    :cond_f
    move v0, v8

    .line 1299
    goto :goto_8

    :cond_10
    move v0, v8

    .line 1300
    goto :goto_9

    :cond_11
    move v0, v8

    .line 1301
    goto :goto_a
.end method

.method static synthetic r(Lcom/android/calendar/detail/EventInfoFragment;)Lcom/android/calendar/ex;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    return-object v0
.end method

.method private r()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1324
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1399
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1329
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1330
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1331
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1332
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1333
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    .line 1336
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1337
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 1338
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1339
    const-string v4, ""

    .line 1340
    const-string v5, ""

    .line 1341
    const-string v6, ""

    .line 1343
    if-nez v2, :cond_4

    .line 1344
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ar:I

    if-le v0, v13, :cond_3

    .line 1345
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ar:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ar:I

    .line 1398
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 1350
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1352
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1354
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    const/4 v7, 0x5

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1355
    if-eqz v0, :cond_a

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1356
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    const/4 v4, 0x6

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1357
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1358
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    const/4 v6, 0x7

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1360
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1361
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1370
    :cond_7
    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v2

    .line 1371
    :goto_3
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "My calendar"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1372
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v7, 0x7f0f02ba

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1375
    :cond_8
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v7, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1376
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aM:Landroid/text/SpannableStringBuilder;

    const-string v7, ", "

    invoke-virtual {v0, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1378
    iget-wide v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    const-wide/16 v10, -0x1

    cmp-long v0, v8, v10

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1380
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v8, v0

    iput-wide v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    .line 1383
    :cond_9
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/at;

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1385
    packed-switch v3, :pswitch_data_0

    .line 1396
    :pswitch_0
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/at;

    move v3, v12

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1366
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_2

    :cond_b
    move-object v0, v1

    .line 1370
    goto :goto_3

    .line 1387
    :pswitch_1
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/at;

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1390
    :pswitch_2
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/at;

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1393
    :pswitch_3
    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    new-instance v0, Lcom/android/calendar/at;

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1385
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic s(Lcom/android/calendar/detail/EventInfoFragment;)I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aF:I

    return v0
.end method

.method private s()V
    .locals 8

    .prologue
    .line 1559
    .line 1561
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 1564
    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    .line 1565
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/detail/EventInfoFragment;->i:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const-string v7, "attendeeName ASC, attendeeEmail ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    return-void
.end method

.method private t()Z
    .locals 3

    .prologue
    .line 1570
    const-string v0, "com.sec.android.app.sns3.linkedin"

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1572
    const/4 v0, 0x0

    .line 1583
    :goto_0
    return v0

    .line 1575
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1576
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aH:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/at;

    .line 1577
    iget-object v0, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1580
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_REQUESTED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1581
    const-string v2, "emailList"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1582
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const-string v2, "com.sec.android.app.sns3.permission.REQUEST_PEOPLE_LOOKUP"

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1583
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic t(Lcom/android/calendar/detail/EventInfoFragment;)Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bH:Z

    return v0
.end method

.method static synthetic u(Lcom/android/calendar/detail/EventInfoFragment;)J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    return-wide v0
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2133
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/ContactsContract$RawContacts;->getContactLookupUri(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 2135
    if-nez v0, :cond_1

    .line 2182
    :cond_0
    :goto_0
    return-void

    .line 2138
    :cond_1
    const-string v1, "photo"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2139
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "data15"

    aput-object v0, v2, v6

    .line 2140
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2141
    if-eqz v1, :cond_0

    .line 2147
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2148
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 2153
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v2, 0x7f12017a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 2154
    sget-object v2, Lcom/android/calendar/g/f;->a:Landroid/view/ViewOutlineProvider;

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 2155
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setClipToOutline(Z)V

    .line 2156
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 2157
    if-eqz v3, :cond_3

    .line 2158
    const/4 v2, 0x0

    array-length v4, v3

    invoke-static {v3, v2, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2159
    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2164
    :goto_1
    new-instance v2, Lcom/android/calendar/detail/al;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/al;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2178
    if-eqz v1, :cond_0

    .line 2179
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2161
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020101

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2178
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 2179
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method static synthetic v(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    return-object v0
.end method

.method private v()V
    .locals 3

    .prologue
    .line 2293
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 2295
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2304
    :goto_0
    return-void

    .line 2299
    :cond_0
    const/16 v1, 0x8

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 2300
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2301
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 2303
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method static synthetic w(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aI:Ljava/util/ArrayList;

    return-object v0
.end method

.method private w()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2308
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-nez v0, :cond_4

    .line 2309
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    if-nez v0, :cond_0

    .line 2342
    :goto_0
    return-void

    .line 2312
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    const v3, 0x7f120343

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2313
    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    const v4, 0x7f120342

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2314
    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    const v5, 0x7f120346

    invoke-interface {v4, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 2315
    iget-object v5, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    const v6, 0x7f120344

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 2317
    if-eqz v0, :cond_1

    .line 2318
    iget-boolean v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2319
    iget-boolean v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->an:Z

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2321
    :cond_1
    if-eqz v3, :cond_2

    .line 2322
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ao:Z

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2323
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ao:Z

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2326
    :cond_2
    if-eqz v5, :cond_3

    .line 2327
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2330
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/content/Context;)Z

    move-result v0

    .line 2331
    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aq:Z

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    .line 2332
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2334
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2335
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 2336
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2341
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->x()V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 2327
    goto :goto_1
.end method

.method static synthetic x(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aJ:Ljava/util/ArrayList;

    return-object v0
.end method

.method private x()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2345
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v0, :cond_1

    .line 2346
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->M:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/detail/ap;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/ap;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2431
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->M:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2444
    :cond_0
    :goto_0
    return-void

    .line 2433
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2434
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120188

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2435
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0481

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    const/16 v4, 0x1f

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2436
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2438
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120186

    invoke-static {v0, v1, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2439
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201a2

    invoke-static {v0, v1, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2440
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201a4

    invoke-static {v0, v1, v6}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    .line 2441
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120179

    invoke-static {v0, v1, v5}, Lcom/android/calendar/detail/EventInfoFragment;->b(Landroid/view/View;II)V

    goto :goto_0
.end method

.method static synthetic y(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aK:Ljava/util/ArrayList;

    return-object v0
.end method

.method private y()Z
    .locals 10

    .prologue
    .line 2696
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aG:Z

    if-eqz v0, :cond_0

    .line 2697
    const/4 v0, 0x0

    .line 2732
    :goto_0
    return v0

    .line 2700
    :cond_0
    iget v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    .line 2701
    if-nez v6, :cond_1

    .line 2702
    const/4 v0, 0x0

    goto :goto_0

    .line 2706
    :cond_1
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 2707
    const/4 v0, 0x0

    goto :goto_0

    .line 2710
    :cond_2
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-nez v0, :cond_3

    .line 2712
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/android/calendar/detail/bq;->a(Landroid/app/Activity;JJILjava/lang/String;Lcom/android/calendar/detail/cb;)V

    .line 2713
    const/4 v0, 0x1

    goto :goto_0

    .line 2717
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bk:Lcom/android/calendar/detail/w;

    invoke-virtual {v0}, Lcom/android/calendar/detail/w;->a()I

    move-result v0

    .line 2718
    packed-switch v0, :pswitch_data_0

    .line 2729
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->d:Ljava/lang/String;

    const-string v1, "Unexpected choice for updating invitation response"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2732
    const/4 v0, 0x0

    goto :goto_0

    .line 2720
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2722
    :pswitch_1
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    iget-boolean v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->ac:Z

    iget-object v8, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/android/calendar/detail/bq;->a(JJIZLandroid/app/Activity;Lcom/android/calendar/detail/cb;)V

    .line 2723
    const/4 v0, 0x1

    goto :goto_0

    .line 2726
    :pswitch_2
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->ai:J

    iget-object v7, p0, Lcom/android/calendar/detail/EventInfoFragment;->ak:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/android/calendar/detail/bq;->a(Landroid/app/Activity;JJILjava/lang/String;Lcom/android/calendar/detail/cb;)V

    .line 2727
    const/4 v0, 0x1

    goto :goto_0

    .line 2718
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic z(Lcom/android/calendar/detail/EventInfoFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aL:Ljava/util/ArrayList;

    return-object v0
.end method

.method private z()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 2738
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    .line 2739
    const-string v3, "event_id=?"

    .line 2740
    new-array v4, v1, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 2741
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$CalendarAlerts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2745
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2746
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2747
    invoke-direct {p0, v2, v3}, Lcom/android/calendar/detail/EventInfoFragment;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2750
    :cond_0
    if-eqz v1, :cond_1

    .line 2751
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2754
    :cond_1
    return-void

    .line 2750
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2751
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 3410
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ay:Z

    .line 3411
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 884
    iput p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    iput p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    .line 885
    iput p2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    iput p2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    .line 886
    return-void
.end method

.method public a(ILcom/android/calendar/a/a/a/a;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3285
    packed-switch p1, :pswitch_data_0

    .line 3391
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/detail/au;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/au;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3406
    :cond_0
    :goto_0
    return-void

    .line 3287
    :pswitch_0
    instance-of v0, p2, Lcom/android/calendar/a/a/i;

    if-eqz v0, :cond_2

    .line 3288
    invoke-virtual {p2}, Lcom/android/calendar/a/a/a/a;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3289
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 3290
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/a/a/k;

    .line 3291
    if-eqz v0, :cond_0

    .line 3292
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 3293
    iget-wide v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 3294
    invoke-static {v2, v1}, Lcom/android/calendar/a/a/m;->a(Landroid/text/format/Time;Z)Lcom/android/calendar/a/a/o;

    move-result-object v2

    .line 3295
    if-eqz v2, :cond_0

    .line 3296
    new-instance v3, Lcom/android/calendar/a/a/m;

    iget-object v4, v0, Lcom/android/calendar/a/a/k;->a:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bu:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v3, v4, v5, v0, v2}, Lcom/android/calendar/a/a/m;-><init>(Ljava/lang/String;Ljava/lang/String;ZLcom/android/calendar/a/a/o;)V

    .line 3297
    invoke-static {}, Lcom/android/calendar/a/a/a/h;->b()Lcom/android/calendar/a/a/a/h;

    move-result-object v0

    invoke-virtual {v0, v3, p0}, Lcom/android/calendar/a/a/a/h;->a(Lcom/android/calendar/a/a/a/a;Lcom/android/calendar/a/a/a/j;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 3296
    goto :goto_1

    .line 3301
    :cond_2
    instance-of v0, p2, Lcom/android/calendar/a/a/m;

    if-eqz v0, :cond_0

    .line 3302
    invoke-virtual {p2}, Lcom/android/calendar/a/a/a/a;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 3303
    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-static {v0, v2, v3}, Lcom/android/calendar/a/a/m;->a(Ljava/util/ArrayList;J)Lcom/android/calendar/a/a/r;

    move-result-object v0

    .line 3304
    if-eqz v0, :cond_0

    .line 3305
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 3306
    iget-wide v2, v0, Lcom/android/calendar/a/a/r;->a:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 3307
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    new-instance v2, Lcom/android/calendar/detail/as;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/detail/as;-><init>(Lcom/android/calendar/detail/EventInfoFragment;Lcom/android/calendar/a/a/r;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 3285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 2490
    if-nez p2, :cond_1

    .line 2549
    :cond_0
    :goto_0
    return-void

    .line 2494
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2495
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2496
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2497
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2498
    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2500
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aS:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2503
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->h:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2505
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2509
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/calendar/detail/ar;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/ar;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2517
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 2518
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2521
    :cond_4
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->au:Z

    if-eqz v0, :cond_0

    .line 2522
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120184

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2523
    if-eqz v0, :cond_5

    .line 2524
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2526
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    .line 2528
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 2529
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->aQ:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v0

    invoke-static {v2, v3, v4, v0}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    goto :goto_2

    .line 2534
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2535
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2536
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 2537
    const-string v1, ""

    .line 2538
    const-string v1, ""

    .line 2539
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aQ:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v4

    invoke-static {v1, v4}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;I)I

    move-result v1

    .line 2540
    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aR:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2541
    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/calendar/au;->b()I

    move-result v0

    invoke-static {v4, v0}, Lcom/android/calendar/event/hm;->b(Ljava/util/ArrayList;I)I

    move-result v0

    .line 2542
    iget-object v4, p0, Lcom/android/calendar/detail/EventInfoFragment;->aT:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2543
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " / "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2545
    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 2546
    const v0, 0x7f1201a1

    invoke-direct {p0, p1, v0, v2}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 2787
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    if-eqz v0, :cond_0

    .line 2790
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->c()V

    .line 2792
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 2766
    iput-boolean p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->ax:Z

    .line 2767
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2770
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/EventInfoFragment;->e:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/bo;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2771
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 2775
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2776
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->b()V

    .line 2778
    :cond_0
    return-void
.end method

.method public d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3424
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 2782
    const-wide/32 v0, 0x10000080

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 492
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onAttach(Landroid/app/Activity;)V

    .line 493
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    .line 494
    new-instance v0, Lcom/android/calendar/c/a;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/c/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    .line 495
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    new-instance v1, Lcom/android/calendar/c/c;

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    invoke-direct {v1, v2, v4}, Lcom/android/calendar/c/c;-><init>(Landroid/net/Uri;Z)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/c/a;->a(Lcom/android/calendar/c/c;)V

    .line 497
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    .line 498
    invoke-static {}, Lcom/android/calendar/dz;->E()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bu:Z

    .line 499
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    .line 501
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v0, :cond_1

    .line 502
    const/4 v0, 0x2

    const v1, 0x103013b

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->setStyle(II)V

    .line 508
    :goto_0
    new-instance v0, Lcom/android/calendar/detail/w;

    invoke-direct {v0, p1}, Lcom/android/calendar/detail/w;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bk:Lcom/android/calendar/detail/w;

    .line 509
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bk:Lcom/android/calendar/detail/w;

    invoke-virtual {v0, v3}, Lcom/android/calendar/detail/w;->a(I)V

    .line 511
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    .line 512
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->e:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->e:[Ljava/lang/String;

    .line 514
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bj:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bK:Lcom/android/calendar/d/a/a;

    .line 517
    :cond_0
    new-instance v0, Lcom/android/calendar/detail/bo;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/detail/bo;-><init>(Lcom/android/calendar/detail/EventInfoFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    .line 518
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    new-instance v1, Lcom/android/calendar/detail/at;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/at;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/bo;->a(Lcom/android/calendar/detail/bm;)V

    .line 528
    return-void

    .line 504
    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/calendar/detail/EventInfoFragment;->setHasOptionsMenu(Z)V

    .line 505
    invoke-virtual {p0, v3, v4}, Lcom/android/calendar/detail/EventInfoFragment;->setStyle(II)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2797
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2798
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2799
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2800
    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2802
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2803
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2805
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 1462
    invoke-super {p0, p1, p2}, Lcom/android/calendar/detail/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1464
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-eqz v0, :cond_1

    .line 1465
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->setHasOptionsMenu(Z)V

    .line 1474
    :cond_0
    :goto_0
    return-void

    .line 1470
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    if-nez v0, :cond_0

    .line 1471
    :cond_2
    const v0, 0x7f110009

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1472
    iput-object p1, p0, Lcom/android/calendar/detail/EventInfoFragment;->N:Landroid/view/Menu;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 532
    if-eqz p3, :cond_0

    .line 533
    const-string v0, "key_fragment_is_dialog"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    .line 534
    const-string v0, "key_window_style"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    .line 535
    const-string v0, "bundle_popup_text"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->b(Ljava/lang/String;)V

    .line 536
    const-string v0, "bundle_alarminput_text"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(I)V

    .line 537
    const-string v0, "bundle_popup_button_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Z)V

    .line 540
    :cond_0
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    if-ne v0, v1, :cond_6

    .line 541
    const v0, 0x7f04004f

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    .line 542
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 543
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->M:Landroid/widget/ImageButton;

    .line 548
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120177

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->L:Landroid/widget/ScrollView;

    .line 549
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->L:Landroid/widget/ScrollView;

    new-instance v1, Lcom/android/calendar/detail/bb;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/bb;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 557
    invoke-static {}, Lcom/android/calendar/dz;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    new-instance v0, Lcom/android/calendar/ex;

    invoke-direct {v0}, Lcom/android/calendar/ex;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->K:Lcom/android/calendar/ex;

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f12003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->y:Landroid/widget/TextView;

    .line 562
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f12018a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->z:Landroid/widget/TextView;

    .line 563
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f12010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->E:Landroid/widget/TextView;

    .line 564
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120088

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->P:Landroid/widget/TextView;

    .line 566
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120194

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->G:Landroid/widget/LinearLayout;

    .line 567
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f12018f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->H:Landroid/widget/LinearLayout;

    .line 569
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->F:Landroid/widget/TextView;

    .line 570
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->F:Landroid/widget/TextView;

    new-instance v1, Lcom/android/calendar/detail/bc;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/bc;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 584
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f120196

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    .line 585
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aN:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/detail/bd;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/bd;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    if-nez v0, :cond_3

    .line 599
    if-eqz p3, :cond_2

    .line 600
    const-string v0, "configuration_launch_orientation"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    .line 601
    const-string v0, "key_event_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    .line 602
    const-string v0, "key_start_millis"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    .line 603
    const-string v0, "key_end_millis"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    .line 604
    const-string v0, "x_coordinate"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    .line 605
    const-string v0, "y_coordinate"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    .line 606
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_7

    .line 607
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    .line 608
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    .line 614
    :cond_2
    :goto_1
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->Q:Landroid/net/Uri;

    .line 617
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0, p0, v4}, Lcom/android/calendar/fd;->a(Landroid/app/Activity;Landroid/app/Fragment;Z)V

    .line 619
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bi:Lcom/android/calendar/al;

    .line 622
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->b()V

    .line 623
    const/16 v0, 0xf

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aO:I

    .line 624
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->A()V

    .line 626
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bC:J

    .line 627
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bD:J

    .line 628
    iput-wide v6, p0, Lcom/android/calendar/detail/EventInfoFragment;->bE:J

    .line 630
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->at:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-eqz v0, :cond_4

    .line 631
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    const v1, 0x7f1201b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f120180

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 634
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Landroid/view/View;)V

    .line 636
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-eqz v0, :cond_5

    .line 637
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 638
    const v1, 0x7f0c044e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->be:I

    .line 639
    const v1, 0x7f0c044c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bf:I

    .line 640
    const v1, 0x7f0c044b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bg:I

    .line 641
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->e()V

    .line 644
    :cond_5
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 645
    const-string v1, "com.sec.android.app.sns.profile.ACTION_FACEBOOK_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 646
    const-string v1, "com.sec.android.app.sns.profile.ACTION_LINKEDIN_PEOPLE_LOOKUP_UPDATED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 647
    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bQ:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 649
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    return-object v0

    .line 545
    :cond_6
    const v0, 0x7f04004a

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    goto/16 :goto_0

    .line 610
    :cond_7
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->ba:I

    .line 611
    iput v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->bb:I

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->c()V

    .line 1543
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 1544
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->S:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1546
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 1547
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->T:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1549
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 1550
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->U:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1552
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->V:Landroid/database/Cursor;

    if-eqz v0, :cond_3

    .line 1553
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->V:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1555
    :cond_3
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroy()V

    .line 1556
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1513
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ay:Z

    if-nez v0, :cond_0

    .line 1514
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->az:Z

    if-nez v0, :cond_0

    .line 1515
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f039b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1516
    iput-boolean v3, p0, Lcom/android/calendar/detail/EventInfoFragment;->az:Z

    .line 1520
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1521
    const-string v0, "15"

    invoke-static {v0}, Lcom/android/calendar/event/hm;->b(Ljava/lang/String;)V

    .line 1522
    invoke-static {v3}, Lcom/android/calendar/event/hm;->a(Z)V

    .line 1523
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bt:Z

    if-eqz v0, :cond_1

    .line 1524
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(I)V

    .line 1528
    :cond_1
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 1529
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1532
    :cond_2
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    .line 1533
    invoke-static {}, Lcom/android/calendar/event/hm;->a()V

    .line 1535
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bQ:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1537
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroyView()V

    .line 1538
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1485
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aX:Z

    if-eqz v0, :cond_1

    .line 1486
    :cond_0
    const/4 v0, 0x0

    .line 1508
    :goto_0
    return v0

    .line 1489
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1508
    :goto_1
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 1491
    :pswitch_0
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->k()V

    goto :goto_1

    .line 1494
    :pswitch_1
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->l()V

    goto :goto_1

    .line 1497
    :pswitch_2
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->m()V

    goto :goto_1

    .line 1500
    :pswitch_3
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->n()V

    goto :goto_1

    .line 1503
    :pswitch_4
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->o()V

    goto :goto_1

    .line 1489
    :pswitch_data_0
    .packed-switch 0x7f120342
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 2626
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aY:Z

    .line 2627
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->x:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 2628
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->x:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 2631
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/bo;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2633
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2634
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2635
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    if-eqz v0, :cond_1

    .line 2636
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 2641
    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onPause()V

    .line 2642
    return-void

    .line 2637
    :cond_2
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2638
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1478
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1479
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->w()V

    .line 1480
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 2646
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onResume()V

    .line 2647
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bG:Lcom/android/calendar/c/a;

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->b()V

    .line 2648
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bi:Lcom/android/calendar/al;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->w:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 2649
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 2650
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2651
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    instance-of v0, v0, Lcom/android/calendar/detail/ck;

    if-eqz v0, :cond_0

    .line 2652
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bL:Landroid/app/DialogFragment;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Recurring"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2658
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aY:Z

    .line 2659
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bH:Z

    .line 2660
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aZ:Z

    if-eqz v0, :cond_1

    .line 2661
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bm:Lcom/android/calendar/detail/bo;

    iget-object v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bN:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/bo;->post(Ljava/lang/Runnable;)Z

    .line 2664
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->ax:Z

    if-eqz v0, :cond_6

    .line 2665
    invoke-virtual {p0}, Lcom/android/calendar/detail/EventInfoFragment;->b()V

    .line 2670
    :goto_1
    iget-wide v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->aE:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2671
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->u()V

    .line 2673
    :cond_2
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    if-eqz v0, :cond_3

    .line 2674
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->s()V

    .line 2675
    iget v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 2676
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->z()V

    .line 2681
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    invoke-virtual {v0}, Lcom/android/calendar/cj;->b()Lcom/android/calendar/ca;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    invoke-virtual {v0}, Lcom/android/calendar/cj;->b()Lcom/android/calendar/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/ca;->c()Lcom/android/calendar/detail/bj;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/detail/bj;->a:Lcom/android/calendar/detail/bj;

    if-eq v0, v1, :cond_4

    .line 2683
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bl:Lcom/android/calendar/cj;

    invoke-virtual {v0}, Lcom/android/calendar/cj;->b()Lcom/android/calendar/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/ca;->b()V

    .line 2686
    :cond_4
    invoke-direct {p0}, Lcom/android/calendar/detail/EventInfoFragment;->p()V

    .line 2687
    return-void

    .line 2655
    :cond_5
    sget-object v0, Lcom/android/calendar/detail/EventInfoFragment;->bh:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 2667
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->O:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/EventInfoFragment;->bs:Z

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1445
    const-string v0, "key_event_id"

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->R:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1446
    const-string v0, "key_start_millis"

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->W:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1447
    const-string v0, "key_end_millis"

    iget-wide v2, p0, Lcom/android/calendar/detail/EventInfoFragment;->X:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1448
    const-string v0, "key_fragment_is_dialog"

    iget-boolean v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->aW:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1449
    const-string v0, "key_window_style"

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->v:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1450
    const-string v0, "key_attendee_response"

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->as:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1451
    const-string v0, "bundle_popup_text"

    invoke-static {}, Lcom/android/calendar/event/hm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    const-string v0, "bundle_alarminput_text"

    invoke-static {}, Lcom/android/calendar/event/hm;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1453
    const-string v0, "bundle_popup_button_enabled"

    invoke-static {}, Lcom/android/calendar/event/hm;->d()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1454
    const-string v0, "x_coordinate"

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bc:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1455
    const-string v0, "y_coordinate"

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bd:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1456
    const-string v0, "configuration_launch_orientation"

    iget v1, p0, Lcom/android/calendar/detail/EventInfoFragment;->bB:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1457
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1458
    return-void
.end method
