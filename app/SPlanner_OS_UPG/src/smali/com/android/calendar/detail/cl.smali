.class Lcom/android/calendar/detail/cl;
.super Ljava/lang/Object;
.source "SelectRecurrenceDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/ck;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/ck;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 59
    packed-switch p2, :pswitch_data_0

    .line 82
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "theme"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHOOSER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v1, "android.intent.extra.INTENT"

    iget-object v2, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v2}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 85
    iget-object v1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-virtual {v1}, Lcom/android/calendar/detail/ck;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 86
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-virtual {v0}, Lcom/android/calendar/detail/ck;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v8, v8}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 87
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 88
    return-void

    .line 61
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->a(Lcom/android/calendar/detail/ck;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "onlyEvent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 63
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "dtstart"

    iget-object v2, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v2}, Lcom/android/calendar/detail/ck;->c(Lcom/android/calendar/detail/ck;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-virtual {v0}, Lcom/android/calendar/detail/ck;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->d(Lcom/android/calendar/detail/ck;)Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->c(Lcom/android/calendar/detail/ck;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->e(Lcom/android/calendar/detail/ck;)J

    move-result-wide v6

    invoke-static/range {v1 .. v7}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;ZJJ)Landroid/net/Uri;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v1}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "text/x-vCalendar"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    iget-object v1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v1}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 72
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->a(Lcom/android/calendar/detail/ck;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-virtual {v0}, Lcom/android/calendar/detail/ck;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v0}, Lcom/android/calendar/detail/ck;->d(Lcom/android/calendar/detail/ck;)Landroid/net/Uri;

    move-result-object v2

    move v3, v8

    move-wide v6, v4

    invoke-static/range {v1 .. v7}, Lcom/android/calendar/vcal/y;->a(Landroid/content/Context;Landroid/net/Uri;ZJJ)Landroid/net/Uri;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v1}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "text/x-vCalendar"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    iget-object v1, p0, Lcom/android/calendar/detail/cl;->a:Lcom/android/calendar/detail/ck;

    invoke-static {v1}, Lcom/android/calendar/detail/ck;->b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
