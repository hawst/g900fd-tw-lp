.class final enum Lcom/android/calendar/detail/bi;
.super Ljava/lang/Enum;
.source "EventInfoFragment.java"


# static fields
.field public static final enum a:Lcom/android/calendar/detail/bi;

.field public static final enum b:Lcom/android/calendar/detail/bi;

.field public static final enum c:Lcom/android/calendar/detail/bi;

.field public static final enum d:Lcom/android/calendar/detail/bi;

.field private static final synthetic e:[Lcom/android/calendar/detail/bi;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 400
    new-instance v0, Lcom/android/calendar/detail/bi;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/detail/bi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    .line 401
    new-instance v0, Lcom/android/calendar/detail/bi;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/detail/bi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    .line 402
    new-instance v0, Lcom/android/calendar/detail/bi;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/detail/bi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    .line 403
    new-instance v0, Lcom/android/calendar/detail/bi;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/detail/bi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bi;->d:Lcom/android/calendar/detail/bi;

    .line 399
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/calendar/detail/bi;

    sget-object v1, Lcom/android/calendar/detail/bi;->a:Lcom/android/calendar/detail/bi;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/detail/bi;->b:Lcom/android/calendar/detail/bi;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/detail/bi;->c:Lcom/android/calendar/detail/bi;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/detail/bi;->d:Lcom/android/calendar/detail/bi;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/detail/bi;->e:[Lcom/android/calendar/detail/bi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/detail/bi;
    .locals 1

    .prologue
    .line 399
    const-class v0, Lcom/android/calendar/detail/bi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/bi;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/detail/bi;
    .locals 1

    .prologue
    .line 399
    sget-object v0, Lcom/android/calendar/detail/bi;->e:[Lcom/android/calendar/detail/bi;

    invoke-virtual {v0}, [Lcom/android/calendar/detail/bi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/detail/bi;

    return-object v0
.end method
