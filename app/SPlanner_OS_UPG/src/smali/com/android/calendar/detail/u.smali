.class Lcom/android/calendar/detail/u;
.super Ljava/lang/Object;
.source "EasyModeListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/r;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/r;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 232
    iget-object v0, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-virtual {v0, v2}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Z)V

    .line 233
    iget-object v0, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 234
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/q;

    .line 235
    iget-wide v4, v0, Lcom/android/calendar/detail/q;->g:J

    .line 236
    iget-object v3, v0, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    iget-object v6, v0, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-nez v6, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 237
    iget-object v1, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v1}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v1

    iget-object v0, v0, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v4, v5, v0}, Lcom/android/calendar/detail/o;->a(JZ)V

    .line 238
    iget-object v0, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 240
    iget-object v0, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->c()I

    .line 241
    iget-object v0, p0, Lcom/android/calendar/detail/u;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 244
    :goto_0
    return v2

    :cond_1
    move v2, v1

    goto :goto_0
.end method
