.class final Lcom/android/calendar/detail/l;
.super Lcom/android/calendar/common/a/a;
.source "EasyModeListActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EasyModeListActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/EasyModeListActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    .line 368
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 369
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->c()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 447
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 448
    const v1, 0x7f110004

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 449
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Z)V

    .line 379
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Z)V

    .line 384
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->notifyDataSetChanged()V

    .line 409
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/android/calendar/detail/l;->a()I

    move-result v0

    .line 389
    iget-object v1, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v1, v1, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v1}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/detail/o;->b()I

    move-result v1

    .line 390
    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/android/calendar/detail/l;->f()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 6

    .prologue
    const v4, 0x7f120324

    const v5, 0x7f120323

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 395
    invoke-virtual {p0}, Lcom/android/calendar/detail/l;->a()I

    move-result v3

    .line 396
    iget-object v0, p0, Lcom/android/calendar/detail/l;->c:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/android/calendar/detail/l;->c:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-lez v3, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/l;->c:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/android/calendar/detail/l;->c:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    if-ne v3, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->b(Lcom/android/calendar/detail/EasyModeListActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->c(Lcom/android/calendar/detail/EasyModeListActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 401
    iget-object v0, p0, Lcom/android/calendar/detail/l;->c:Landroid/view/Menu;

    invoke-interface {v0, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-ne v3, v1, :cond_4

    iget-object v3, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v3}, Lcom/android/calendar/detail/EasyModeListActivity;->b(Lcom/android/calendar/detail/EasyModeListActivity;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v3}, Lcom/android/calendar/detail/EasyModeListActivity;->c(Lcom/android/calendar/detail/EasyModeListActivity;)Z

    move-result v3

    if-nez v3, :cond_4

    :goto_2
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 404
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 397
    goto :goto_0

    :cond_3
    move v0, v2

    .line 400
    goto :goto_1

    :cond_4
    move v1, v2

    .line 401
    goto :goto_2
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 418
    new-instance v0, Lcom/android/calendar/detail/m;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/m;-><init>(Lcom/android/calendar/detail/l;)V

    .line 426
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 442
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 428
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->d(Lcom/android/calendar/detail/EasyModeListActivity;)V

    .line 429
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->e(Lcom/android/calendar/detail/EasyModeListActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/detail/n;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/n;-><init>(Lcom/android/calendar/detail/l;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 437
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v1, v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Ljava/lang/Runnable;)V

    .line 438
    const/4 v0, 0x1

    goto :goto_1

    .line 426
    nop

    :pswitch_data_0
    .packed-switch 0x7f120323
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 4

    .prologue
    .line 453
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 454
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    new-instance v1, Lcom/android/calendar/c/a;

    iget-object v2, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v3, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v3}, Lcom/android/calendar/detail/EasyModeListActivity;->f(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/d;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/calendar/c/a;-><init>(Landroid/app/Activity;Lcom/android/calendar/c/d;)V

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;

    .line 455
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->b()V

    .line 456
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 466
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->c()V

    .line 468
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iput-object v2, v0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    .line 472
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;Lcom/android/calendar/detail/l;)Lcom/android/calendar/detail/l;

    .line 473
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->g(Lcom/android/calendar/detail/EasyModeListActivity;)V

    .line 474
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/r;->a(Z)V

    .line 475
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->b()V

    .line 476
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->b(Lcom/android/calendar/detail/EasyModeListActivity;Z)Z

    .line 477
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->c(Lcom/android/calendar/detail/EasyModeListActivity;Z)Z

    .line 479
    iget-object v0, p0, Lcom/android/calendar/detail/l;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->b()V

    .line 480
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 481
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 461
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
