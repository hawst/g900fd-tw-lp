.class Lcom/android/calendar/detail/k;
.super Ljava/lang/Object;
.source "EasyModeListActivity.java"

# interfaces
.implements Lcom/android/calendar/c/d;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EasyModeListActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EasyModeListActivity;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/android/calendar/detail/k;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 347
    iget-object v0, p0, Lcom/android/calendar/detail/k;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v0, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->d()Ljava/util/HashMap;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 351
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 353
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 355
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 356
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 358
    new-instance v3, Lcom/android/calendar/c/c;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/android/calendar/c/c;-><init>(Landroid/net/Uri;Z)V

    .line 359
    iget-object v0, p0, Lcom/android/calendar/detail/k;->a:Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-static {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/c/a;->a(Lcom/android/calendar/c/c;)V

    goto :goto_0

    .line 362
    :cond_1
    return-void
.end method
