.class public final enum Lcom/android/calendar/detail/bj;
.super Ljava/lang/Enum;
.source "EventInfoFragment.java"


# static fields
.field public static final enum a:Lcom/android/calendar/detail/bj;

.field public static final enum b:Lcom/android/calendar/detail/bj;

.field public static final enum c:Lcom/android/calendar/detail/bj;

.field public static final enum d:Lcom/android/calendar/detail/bj;

.field private static final synthetic e:[Lcom/android/calendar/detail/bj;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 446
    new-instance v0, Lcom/android/calendar/detail/bj;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/detail/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bj;->a:Lcom/android/calendar/detail/bj;

    .line 447
    new-instance v0, Lcom/android/calendar/detail/bj;

    const-string v1, "SENDING_EMAIL_BY_NOT_RECURRING_EVENTS"

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/detail/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bj;->b:Lcom/android/calendar/detail/bj;

    .line 448
    new-instance v0, Lcom/android/calendar/detail/bj;

    const-string v1, "SENDING_EMAIL_BY_RECURRING_EVENTS"

    invoke-direct {v0, v1, v4}, Lcom/android/calendar/detail/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bj;->c:Lcom/android/calendar/detail/bj;

    .line 449
    new-instance v0, Lcom/android/calendar/detail/bj;

    const-string v1, "SENDING_EMAIL_BY_EXCEPTIONAL_EVENTS"

    invoke-direct {v0, v1, v5}, Lcom/android/calendar/detail/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/calendar/detail/bj;->d:Lcom/android/calendar/detail/bj;

    .line 445
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/calendar/detail/bj;

    sget-object v1, Lcom/android/calendar/detail/bj;->a:Lcom/android/calendar/detail/bj;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/calendar/detail/bj;->b:Lcom/android/calendar/detail/bj;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/calendar/detail/bj;->c:Lcom/android/calendar/detail/bj;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/calendar/detail/bj;->d:Lcom/android/calendar/detail/bj;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/detail/bj;->e:[Lcom/android/calendar/detail/bj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/calendar/detail/bj;
    .locals 1

    .prologue
    .line 445
    const-class v0, Lcom/android/calendar/detail/bj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/bj;

    return-object v0
.end method

.method public static values()[Lcom/android/calendar/detail/bj;
    .locals 1

    .prologue
    .line 445
    sget-object v0, Lcom/android/calendar/detail/bj;->e:[Lcom/android/calendar/detail/bj;

    invoke-virtual {v0}, [Lcom/android/calendar/detail/bj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/calendar/detail/bj;

    return-object v0
.end method
