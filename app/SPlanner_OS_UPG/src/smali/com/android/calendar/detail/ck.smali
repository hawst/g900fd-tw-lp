.class public Lcom/android/calendar/detail/ck;
.super Landroid/app/DialogFragment;
.source "SelectRecurrenceDialog.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:J

.field private final c:J

.field private final d:Z

.field private e:Landroid/content/Intent;

.field private final f:Landroid/net/Uri;

.field private g:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/android/calendar/detail/ck;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/ck;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;Landroid/net/Uri;JJZ)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 53
    new-instance v0, Lcom/android/calendar/detail/cl;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cl;-><init>(Lcom/android/calendar/detail/ck;)V

    iput-object v0, p0, Lcom/android/calendar/detail/ck;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 31
    iput-object p1, p0, Lcom/android/calendar/detail/ck;->e:Landroid/content/Intent;

    .line 32
    iput-object p2, p0, Lcom/android/calendar/detail/ck;->f:Landroid/net/Uri;

    .line 33
    iput-wide p3, p0, Lcom/android/calendar/detail/ck;->b:J

    .line 34
    iput-wide p5, p0, Lcom/android/calendar/detail/ck;->c:J

    .line 35
    iput-boolean p7, p0, Lcom/android/calendar/detail/ck;->d:Z

    .line 37
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/ck;->setRetainInstance(Z)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/ck;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/android/calendar/detail/ck;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/ck;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/calendar/detail/ck;->e:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/detail/ck;)J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/android/calendar/detail/ck;->b:J

    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/ck;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/android/calendar/detail/ck;->f:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/detail/ck;)J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/android/calendar/detail/ck;->c:J

    return-wide v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/android/calendar/detail/ck;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/detail/ck;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    .line 47
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/detail/ck;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    const v1, 0x7f0f01fc

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 49
    const v1, 0x7f09002b

    iget-object v2, p0, Lcom/android/calendar/detail/ck;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 94
    return-void
.end method
