.class Lcom/android/calendar/detail/bk;
.super Landroid/os/AsyncTask;
.source "EventInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 1401
    iput-object p1, p0, Lcom/android/calendar/detail/bk;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;Lcom/android/calendar/detail/ad;)V
    .locals 0

    .prologue
    .line 1401
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/bk;-><init>(Lcom/android/calendar/detail/EventInfoFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 1406
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/android/calendar/detail/bk;->b:Ljava/lang/String;

    .line 1410
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/android/calendar/detail/bk;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1411
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1418
    :goto_0
    return-object v0

    .line 1413
    :catch_0
    move-exception v0

    .line 1414
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 1418
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1415
    :catch_1
    move-exception v0

    .line 1416
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1423
    if-eqz p1, :cond_0

    .line 1424
    iget-object v0, p0, Lcom/android/calendar/detail/bk;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->a(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f12017a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 1425
    invoke-virtual {v0, p1}, Landroid/widget/QuickContactBadge;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1426
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 1428
    new-instance v1, Lcom/android/calendar/detail/bl;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/bl;-><init>(Lcom/android/calendar/detail/bk;)V

    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1440
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1401
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/calendar/detail/bk;->a([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1401
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/android/calendar/detail/bk;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
