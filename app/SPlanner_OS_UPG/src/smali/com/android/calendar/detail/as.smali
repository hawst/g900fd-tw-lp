.class Lcom/android/calendar/detail/as;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/a/a/r;

.field final synthetic b:Lcom/android/calendar/detail/EventInfoFragment;

.field private final c:[I


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;Lcom/android/calendar/a/a/r;)V
    .locals 1

    .prologue
    .line 3307
    iput-object p1, p0, Lcom/android/calendar/detail/as;->b:Lcom/android/calendar/detail/EventInfoFragment;

    iput-object p2, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3337
    const/16 v0, 0x2d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/detail/as;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x7f02026c
        0x7f02026c
        0x7f02026e
        0x7f02026e
        0x7f02026e
        0x7f020270
        0x7f020270
        0x7f020270
        0x0
        0x0
        0x7f020272
        0x7f020274
        0x7f020274
        0x7f020276
        0x7f020278
        0x7f02027a
        0x7f02027a
        0x7f02027c
        0x7f02027e
        0x7f020280
        0x7f020280
        0x7f020282
        0x7f020282
        0x7f020284
        0x7f020284
        0x7f020284
        0x0
        0x0
        0x7f020286
        0x7f020288
        0x7f02028a
        0x7f02028c
        0x7f02028e
        0x7f020290
        0x7f020290
        0x7f020290
        0x7f020290
        0x7f020292
        0x7f020294
        0x7f020296
        0x7f020298
        0x7f020278
        0x7f02027e
        0x7f020282
    .end array-data
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3310
    iget-object v0, p0, Lcom/android/calendar/detail/as;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 3311
    if-nez v0, :cond_1

    .line 3335
    :cond_0
    :goto_0
    return-void

    .line 3315
    :cond_1
    const v1, 0x7f12018d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3316
    iget-object v1, p0, Lcom/android/calendar/detail/as;->c:[I

    iget-object v2, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    iget v2, v2, Lcom/android/calendar/a/a/r;->c:I

    aget v1, v1, v2

    .line 3317
    if-eqz v0, :cond_0

    .line 3318
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 3319
    iget-object v3, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    iget-object v3, v3, Lcom/android/calendar/a/a/r;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3320
    iget-object v3, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    iget-object v3, v3, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    iget-object v3, v3, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3322
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/detail/as;->a:Lcom/android/calendar/a/a/r;

    iget-object v4, v4, Lcom/android/calendar/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3324
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3325
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3326
    if-eqz v1, :cond_3

    .line 3327
    iget-object v2, p0, Lcom/android/calendar/detail/as;->b:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v2}, Lcom/android/calendar/detail/EventInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3329
    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 3333
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
