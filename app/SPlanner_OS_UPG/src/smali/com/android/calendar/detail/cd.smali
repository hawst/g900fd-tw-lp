.class public Lcom/android/calendar/detail/cd;
.super Ljava/lang/Object;
.source "MeetingInvitationStatusView.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/ListView;

.field private d:I

.field private e:Ljava/util/ArrayList;

.field private f:Lcom/android/calendar/detail/cg;

.field private g:Lcom/android/calendar/detail/ch;

.field private h:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

.field private i:Landroid/view/animation/AlphaAnimation;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/MeetingInvitationAttendeesView;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/cd;->d:I

    .line 47
    iput-object p1, p0, Lcom/android/calendar/detail/cd;->h:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    .line 48
    iput-object p2, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    .line 49
    iput p3, p0, Lcom/android/calendar/detail/cd;->d:I

    .line 50
    invoke-direct {p0}, Lcom/android/calendar/detail/cd;->f()V

    .line 51
    return-void
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    .prologue
    const v1, 0x7f0f0387

    .line 112
    const-string v0, ""

    .line 113
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 115
    packed-switch p1, :pswitch_data_0

    .line 129
    :pswitch_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 131
    :goto_0
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const-string v1, "[\\\uff08\\\uff09]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_1
    return-object v0

    .line 117
    :pswitch_1
    const v1, 0x7f0f0383

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    :pswitch_2
    const v1, 0x7f0f0384

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 123
    :pswitch_3
    const v1, 0x7f0f038a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 126
    :pswitch_4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 134
    :cond_0
    const-string v1, "[\\(\\)]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/detail/cd;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static b(I)I
    .locals 2

    .prologue
    .line 180
    int-to-float v0, p0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/cd;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/detail/cd;)Lcom/android/calendar/detail/MeetingInvitationAttendeesView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->h:Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/cd;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/android/calendar/detail/cd;->d:I

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/detail/cd;)Landroid/view/animation/AlphaAnimation;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->i:Landroid/view/animation/AlphaAnimation;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const v0, 0x7f020084

    const/4 v4, 0x0

    .line 66
    .line 67
    iget v1, p0, Lcom/android/calendar/detail/cd;->d:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v0

    .line 84
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->b:Landroid/view/View;

    const v2, 0x7f120222

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    iget-object v2, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    iget v3, p0, Lcom/android/calendar/detail/cd;->d:I

    invoke-static {v2, v3}, Lcom/android/calendar/detail/cd;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 86
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 87
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 90
    iget-object v1, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c022a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 91
    iget-object v2, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 92
    invoke-virtual {v0, v4, v1, v4, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 97
    :goto_1
    iget v0, p0, Lcom/android/calendar/detail/cd;->d:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 98
    invoke-virtual {p0, v4}, Lcom/android/calendar/detail/cd;->a(I)V

    .line 102
    :goto_2
    return-void

    .line 69
    :pswitch_1
    const v0, 0x7f02007d

    move v1, v0

    .line 70
    goto :goto_0

    .line 72
    :pswitch_2
    const v0, 0x7f02007f

    move v1, v0

    .line 73
    goto :goto_0

    .line 75
    :pswitch_3
    const v0, 0x7f020085

    move v1, v0

    .line 76
    goto :goto_0

    :pswitch_4
    move v1, v0

    .line 79
    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 100
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/cd;->a(I)V

    goto :goto_2

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic f(Lcom/android/calendar/detail/cd;)Lcom/android/calendar/detail/ch;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->g:Lcom/android/calendar/detail/ch;

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    new-instance v0, Lcom/android/calendar/detail/ch;

    iget-object v1, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/calendar/detail/ch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->g:Lcom/android/calendar/detail/ch;

    .line 140
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->i:Landroid/view/animation/AlphaAnimation;

    .line 141
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->i:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    .line 144
    new-instance v0, Lcom/android/calendar/detail/cg;

    invoke-direct {v0, p0, v4}, Lcom/android/calendar/detail/cg;-><init>(Lcom/android/calendar/detail/cd;Lcom/android/calendar/detail/ce;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->f:Lcom/android/calendar/detail/cg;

    .line 145
    invoke-direct {p0}, Lcom/android/calendar/detail/cd;->g()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->c:Landroid/widget/ListView;

    .line 146
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/detail/cd;->f:Lcom/android/calendar/detail/cg;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    const v1, 0x7f04006f

    invoke-static {v0, v1, v4}, Lcom/android/calendar/g/f;->a(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cd;->b:Landroid/view/View;

    .line 149
    return-void
.end method

.method private g()Landroid/widget/ListView;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 152
    new-instance v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 153
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 155
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 157
    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->b:Landroid/view/View;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->b:Landroid/view/View;

    const v1, 0x7f12021e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/at;)V
    .locals 2

    .prologue
    .line 184
    invoke-static {p1}, Lcom/android/calendar/detail/ch;->a(Lcom/android/calendar/at;)Lcom/android/calendar/event/aw;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    return-void
.end method

.method public b()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c021f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 164
    iget-object v1, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0221

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 166
    iget-object v2, p0, Lcom/android/calendar/detail/cd;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0220

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 168
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/android/calendar/detail/cd;->b(I)I

    move-result v3

    .line 169
    add-int/2addr v0, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    iget-object v1, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/2addr v1, v0

    .line 172
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 173
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 174
    iget-object v1, p0, Lcom/android/calendar/detail/cd;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->f:Lcom/android/calendar/detail/cg;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cg;->notifyDataSetChanged()V

    .line 176
    invoke-direct {p0}, Lcom/android/calendar/detail/cd;->e()V

    .line 177
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/calendar/detail/cd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 190
    return-void
.end method
