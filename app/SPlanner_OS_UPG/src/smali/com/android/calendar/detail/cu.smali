.class public Lcom/android/calendar/detail/cu;
.super Lcom/android/calendar/detail/a;
.source "TaskInfoFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field public static final d:Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:I

.field private K:Landroid/app/Activity;

.field private L:Landroid/content/Context;

.field private M:Lcom/android/calendar/d/g;

.field private N:Lcom/android/calendar/al;

.field private O:Landroid/view/View;

.field private P:Landroid/view/View;

.field private Q:Landroid/widget/CheckBox;

.field private R:Landroid/widget/TextView;

.field private S:Landroid/widget/TextView;

.field private T:Landroid/widget/TextView;

.field private U:I

.field private final V:[I

.field private W:[Ljava/lang/CharSequence;

.field private X:J

.field private Y:J

.field private Z:Landroid/widget/TextView;

.field private aa:Ljava/lang/String;

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Landroid/view/accessibility/AccessibilityManager;

.field private af:I

.field private ag:Lcom/android/calendar/c/a;

.field private ah:Ljava/lang/Runnable;

.field g:Landroid/view/View$OnClickListener;

.field private h:Landroid/net/Uri;

.field private s:J

.field private t:Landroid/database/Cursor;

.field private u:Z

.field private v:Landroid/view/Menu;

.field private w:Landroid/widget/ScrollView;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/PopupMenu;

.field private z:Lcom/android/calendar/detail/dh;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    const-class v0, Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/cu;->d:Ljava/lang/String;

    .line 132
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "body_size"

    aput-object v1, v0, v6

    const-string v1, "due_date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "reminder_time"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "utc_start_date"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "deleted"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/detail/cu;->e:[Ljava/lang/String;

    .line 153
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_sync_account"

    aput-object v1, v0, v4

    const-string v1, "_sync_account_key"

    aput-object v1, v0, v5

    const-string v1, "_sync_account_type"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/calendar/detail/cu;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v1, 0x258

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 248
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 199
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->u:Z

    .line 201
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    .line 207
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->A:Z

    .line 208
    iput-boolean v4, p0, Lcom/android/calendar/detail/cu;->B:Z

    .line 209
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->C:Z

    .line 211
    iput v0, p0, Lcom/android/calendar/detail/cu;->D:I

    .line 212
    iput v0, p0, Lcom/android/calendar/detail/cu;->E:I

    .line 213
    iput v0, p0, Lcom/android/calendar/detail/cu;->F:I

    .line 214
    iput v0, p0, Lcom/android/calendar/detail/cu;->G:I

    .line 215
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/detail/cu;->H:I

    .line 216
    iput v1, p0, Lcom/android/calendar/detail/cu;->I:I

    .line 217
    iput v1, p0, Lcom/android/calendar/detail/cu;->J:I

    .line 221
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->M:Lcom/android/calendar/d/g;

    .line 223
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->O:Landroid/view/View;

    .line 225
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    .line 226
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    .line 227
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    .line 228
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->T:Landroid/widget/TextView;

    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->V:[I

    .line 231
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->W:[Ljava/lang/CharSequence;

    .line 232
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->X:J

    .line 233
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->Y:J

    .line 240
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->ab:Z

    .line 241
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->ac:Z

    .line 242
    iput-boolean v4, p0, Lcom/android/calendar/detail/cu;->ad:Z

    .line 243
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->ae:Landroid/view/accessibility/AccessibilityManager;

    .line 245
    iput v3, p0, Lcom/android/calendar/detail/cu;->af:I

    .line 246
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    .line 657
    new-instance v0, Lcom/android/calendar/detail/cy;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cy;-><init>(Lcom/android/calendar/detail/cu;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->ah:Ljava/lang/Runnable;

    .line 779
    new-instance v0, Lcom/android/calendar/detail/cz;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cz;-><init>(Lcom/android/calendar/detail/cu;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->g:Landroid/view/View$OnClickListener;

    .line 249
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JZ)V
    .locals 8

    .prologue
    .line 262
    sget-object v0, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/detail/cu;-><init>(Landroid/content/Context;Landroid/net/Uri;JZ)V

    .line 263
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/calendar/detail/cu;->af:I

    .line 264
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;JZ)V
    .locals 5

    .prologue
    const/16 v1, 0x258

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 251
    invoke-direct {p0}, Lcom/android/calendar/detail/a;-><init>()V

    .line 199
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->u:Z

    .line 201
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    .line 207
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->A:Z

    .line 208
    iput-boolean v4, p0, Lcom/android/calendar/detail/cu;->B:Z

    .line 209
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->C:Z

    .line 211
    iput v0, p0, Lcom/android/calendar/detail/cu;->D:I

    .line 212
    iput v0, p0, Lcom/android/calendar/detail/cu;->E:I

    .line 213
    iput v0, p0, Lcom/android/calendar/detail/cu;->F:I

    .line 214
    iput v0, p0, Lcom/android/calendar/detail/cu;->G:I

    .line 215
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/calendar/detail/cu;->H:I

    .line 216
    iput v1, p0, Lcom/android/calendar/detail/cu;->I:I

    .line 217
    iput v1, p0, Lcom/android/calendar/detail/cu;->J:I

    .line 221
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->M:Lcom/android/calendar/d/g;

    .line 223
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->O:Landroid/view/View;

    .line 225
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    .line 226
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    .line 227
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    .line 228
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->T:Landroid/widget/TextView;

    .line 230
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->V:[I

    .line 231
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->W:[Ljava/lang/CharSequence;

    .line 232
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->X:J

    .line 233
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->Y:J

    .line 240
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->ab:Z

    .line 241
    iput-boolean v3, p0, Lcom/android/calendar/detail/cu;->ac:Z

    .line 242
    iput-boolean v4, p0, Lcom/android/calendar/detail/cu;->ad:Z

    .line 243
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->ae:Landroid/view/accessibility/AccessibilityManager;

    .line 245
    iput v3, p0, Lcom/android/calendar/detail/cu;->af:I

    .line 246
    iput-object v2, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    .line 657
    new-instance v0, Lcom/android/calendar/detail/cy;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cy;-><init>(Lcom/android/calendar/detail/cu;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->ah:Ljava/lang/Runnable;

    .line 779
    new-instance v0, Lcom/android/calendar/detail/cz;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cz;-><init>(Lcom/android/calendar/detail/cu;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->g:Landroid/view/View$OnClickListener;

    .line 252
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    .line 253
    iput-object p2, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    .line 254
    iput-wide p3, p0, Lcom/android/calendar/detail/cu;->s:J

    .line 255
    iput-boolean p5, p0, Lcom/android/calendar/detail/cu;->A:Z

    .line 256
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const-string v0, "GATE"

    const-string v1, "<GATE-M>TASK_OPENED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->y:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method private a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1153
    if-nez p1, :cond_1

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1157
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1158
    if-eqz v0, :cond_0

    .line 1159
    invoke-virtual {v0, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;J)V
    .locals 8

    .prologue
    const v6, 0x7f1202d8

    .line 802
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 803
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 804
    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 805
    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 806
    invoke-virtual {v0, p2, p3}, Landroid/text/format/Time;->set(J)V

    .line 807
    iget-wide v2, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {p2, p3, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    .line 809
    if-lt v0, v1, :cond_0

    .line 810
    sub-int/2addr v0, v1

    .line 811
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0120

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 812
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 813
    invoke-direct {p0, p1, v6, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 814
    const/4 v0, 0x0

    invoke-direct {p0, p1, v6, v0}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 818
    :goto_0
    return-void

    .line 816
    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, p1, v6, v0}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    goto :goto_0
.end method

.method private a(Landroid/widget/CompoundButton;Z)V
    .locals 6

    .prologue
    const v5, 0x7f0b007b

    const v4, 0x7f0b007a

    .line 1202
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1234
    :cond_0
    :goto_0
    return-void

    .line 1206
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1207
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 1208
    invoke-static {p1}, Lcom/android/calendar/dz;->a(Landroid/widget/CompoundButton;)Z

    move-result v1

    .line 1210
    if-eqz p2, :cond_3

    .line 1211
    const v2, 0x7f0b00fe

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1212
    iget-object v3, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1214
    iget-boolean v2, p0, Lcom/android/calendar/detail/cu;->ac:Z

    if-eqz v2, :cond_2

    .line 1215
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1216
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->T:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1218
    :cond_2
    if-nez v1, :cond_0

    .line 1219
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0

    .line 1223
    :cond_3
    const v2, 0x7f0b0085

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 1224
    iget-object v3, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1225
    iget-boolean v2, p0, Lcom/android/calendar/detail/cu;->ac:Z

    if-eqz v2, :cond_4

    .line 1226
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1227
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->T:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1229
    :cond_4
    if-nez v1, :cond_0

    .line 1230
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    and-int/lit8 v1, v1, -0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->n()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/detail/cu;->a(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;ZJ)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/detail/cu;->a(ZJ)V

    return-void
.end method

.method private a(ZJ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 1238
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    .line 1239
    const-string v2, "complete"

    if-eqz p1, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1240
    const-string v0, "date_completed"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1242
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1243
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1244
    return-void

    .line 1239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)Z
    .locals 13

    .prologue
    const/4 v10, 0x2

    const v6, 0x7f1202d9

    const/16 v12, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 822
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1033
    :cond_0
    :goto_0
    return v8

    .line 826
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 827
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0x11

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 828
    if-nez v0, :cond_0

    .line 832
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->m()V

    .line 837
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 838
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 845
    :cond_2
    :goto_1
    const v1, 0x7f1202d3

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 847
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0x10

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 848
    const v0, 0x7f1202d6

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 861
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 862
    const v0, 0x7f1202d7

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 863
    const v0, 0x7f1202d8

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 878
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 879
    const v0, 0x7f1202d4

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 892
    :goto_4
    const/4 v1, 0x0

    .line 893
    const v0, 0x7f1202d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 894
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v2, v12}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 895
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v2, v8}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 901
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v2, 0x9

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 902
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0288

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v6, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    move-object v7, v1

    .line 925
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_sync_account_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 926
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/detail/cu;->f:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 928
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4

    .line 929
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 931
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 932
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "task_personal"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 933
    iput-boolean v8, p0, Lcom/android/calendar/detail/cu;->ad:Z

    .line 934
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->m()V

    .line 938
    :cond_4
    if-eqz v0, :cond_5

    .line 939
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 944
    :cond_5
    const-wide/16 v2, 0x0

    .line 945
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_26

    .line 946
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0xc

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 947
    if-ltz v0, :cond_6

    const/4 v1, 0x3

    if-le v0, v1, :cond_7

    :cond_6
    move v0, v8

    .line 952
    :cond_7
    :goto_7
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v4, 0xe

    invoke-interface {v1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_19

    move v0, v8

    .line 958
    :goto_8
    if-nez v0, :cond_1a

    .line 959
    const v1, 0x7f1202d5

    iget-object v2, p0, Lcom/android/calendar/detail/cu;->W:[Ljava/lang/CharSequence;

    aget-object v0, v2, v0

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 983
    :goto_9
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_9

    if-eqz v7, :cond_8

    const-string v0, "My task"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->M:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 986
    :cond_9
    const v0, 0x7f1202e6

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 997
    :goto_a
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 998
    const v0, 0x7f1202da

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 1029
    :goto_b
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1030
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_a

    move v8, v9

    .line 1032
    :cond_a
    invoke-direct {p0, v8}, Lcom/android/calendar/detail/cu;->b(Z)V

    move v8, v9

    .line 1033
    goto/16 :goto_0

    .line 840
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 841
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 842
    :cond_c
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 850
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0400

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 851
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v2, 0x10

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 852
    const v1, 0x7f1202d6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    const/4 v5, 0x7

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 858
    const v0, 0x7f1202d6

    invoke-direct {p0, p1, v0, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    goto/16 :goto_2

    .line 865
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f017f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 866
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 867
    const v1, 0x7f1202d7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    const/4 v5, 0x7

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 872
    const v0, 0x7f1202d7

    invoke-direct {p0, p1, v0, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 873
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_3

    .line 874
    invoke-direct {p0, p1, v2, v3}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;J)V

    goto/16 :goto_3

    .line 881
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/cu;->U:I

    .line 882
    iget v0, p0, Lcom/android/calendar/detail/cu;->U:I

    if-eqz v0, :cond_10

    iget v0, p0, Lcom/android/calendar/detail/cu;->U:I

    if-ne v0, v10, :cond_11

    .line 883
    :cond_10
    const v0, 0x7f1202d4

    invoke-direct {p0, p1, v0, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 884
    const v0, 0x7f1202d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 885
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->V:[I

    iget v2, p0, Lcom/android/calendar/detail/cu;->U:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 887
    :cond_11
    const v0, 0x7f1202d4

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    goto/16 :goto_4

    .line 897
    :cond_12
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v2, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 898
    iget-object v3, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto/16 :goto_5

    .line 905
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 906
    const-string v1, "My task"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 907
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0288

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    move-object v7, v0

    goto/16 :goto_6

    .line 908
    :cond_14
    const-string v1, "My task (personal)"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 909
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    move-object v7, v0

    goto/16 :goto_6

    .line 910
    :cond_15
    const-string v1, "My task (KNOX)"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 911
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/hj;->I(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 912
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 913
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v1, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    :goto_c
    move-object v7, v0

    .line 917
    goto/16 :goto_6

    .line 915
    :cond_16
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "KNOX"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto :goto_c

    .line 917
    :cond_17
    const-string v1, "My task (KNOX II)"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 918
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v9, [Ljava/lang/Object;

    const-string v3, "KNOX II"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    move-object v7, v0

    goto/16 :goto_6

    .line 920
    :cond_18
    invoke-direct {p0, p1, v6, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    move-object v7, v0

    goto/16 :goto_6

    .line 955
    :cond_19
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v2, 0xe

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    goto/16 :goto_8

    .line 961
    :cond_1a
    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->ac:Z

    if-eqz v1, :cond_1b

    .line 962
    const v1, 0x7f1202e3

    invoke-direct {p0, p1, v1, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 964
    :cond_1b
    const v1, 0x7f1202d5

    invoke-direct {p0, p1, v1, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 966
    new-instance v11, Landroid/text/SpannableStringBuilder;

    invoke-direct {v11}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 967
    if-ne v0, v10, :cond_1c

    .line 968
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->W:[Ljava/lang/CharSequence;

    aget-object v0, v1, v0

    invoke-virtual {v11, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 969
    const-string v0, " : "

    invoke-virtual {v11, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 971
    :cond_1c
    const/16 v6, 0x301

    .line 973
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 974
    const/16 v6, 0x381

    .line 976
    :cond_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    const/4 v4, -0x1

    invoke-static {v2, v3, v1, v4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 978
    invoke-virtual {v11, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 979
    const v0, 0x7f1202d5

    invoke-virtual {v11}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 988
    :cond_1e
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    const/16 v1, 0xb

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 989
    const-string v1, "Default"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 990
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 992
    :cond_1f
    const v1, 0x7f1201e4

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 993
    const v0, 0x7f1202e6

    invoke-direct {p0, p1, v0, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    goto/16 :goto_a

    .line 1000
    :cond_20
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->t:Landroid/database/Cursor;

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1002
    if-eqz v0, :cond_21

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1003
    :cond_21
    const v0, 0x7f1202da

    invoke-direct {p0, p1, v0, v12}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    goto/16 :goto_b

    .line 1005
    :cond_22
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x2000

    if-le v1, v2, :cond_23

    .line 1006
    iput-object v0, p0, Lcom/android/calendar/detail/cu;->aa:Ljava/lang/String;

    .line 1007
    const/16 v1, 0x2000

    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1010
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->Z:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1011
    new-instance v1, Landroid/text/SpannableString;

    const-string v2, "..."

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1012
    new-instance v2, Landroid/text/style/UnderlineSpan;

    invoke-direct {v2}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, v8, v3, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1013
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->Z:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1016
    :cond_23
    const v1, 0x7f1202dc

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1017
    const v1, 0x7f1202db

    const v0, 0x7f1202db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 1019
    const v0, 0x7f1202da

    invoke-direct {p0, p1, v0, v8}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;II)V

    .line 1021
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1022
    invoke-static {}, Lcom/android/calendar/dz;->D()Z

    move-result v1

    if-nez v1, :cond_24

    move v1, v9

    :goto_d
    iget-boolean v2, p0, Lcom/android/calendar/detail/cu;->a:Z

    if-eqz v2, :cond_25

    move v2, v10

    :goto_e
    or-int/lit8 v2, v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/android/calendar/detail/cu;->a(Landroid/widget/TextView;ZI)V

    goto/16 :goto_b

    :cond_24
    move v1, v8

    goto :goto_d

    :cond_25
    move v2, v8

    goto :goto_e

    :cond_26
    move v0, v8

    goto/16 :goto_7
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/detail/cu;Z)Z
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/android/calendar/detail/cu;->C:Z

    return p1
.end method

.method private b(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 1117
    if-nez p1, :cond_1

    .line 1138
    :cond_0
    :goto_0
    return-void

    .line 1121
    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1122
    if-eqz v0, :cond_0

    .line 1127
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_voice_capable"

    invoke-static {v2}, Lcom/android/calendar/common/b/a;->d(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1128
    invoke-virtual {v0}, Landroid/widget/TextView;->getAutoLinkMask()I

    move-result v1

    and-int/lit8 v1, v1, -0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 1131
    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    .line 1133
    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->ab:Z

    if-eqz v1, :cond_4

    .line 1134
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/calendar/detail/cu;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 1131
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1136
    :cond_4
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->d()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1165
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1166
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1168
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    invoke-direct {p0, v0, p1}, Lcom/android/calendar/detail/cu;->a(Landroid/widget/CompoundButton;Z)V

    .line 1170
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->ad:Z

    if-nez v0, :cond_0

    .line 1171
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 1199
    :goto_0
    return-void

    .line 1173
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    new-instance v1, Lcom/android/calendar/detail/dc;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/dc;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1188
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->O:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/detail/dd;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/dd;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/detail/cu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->aa:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 28

    .prologue
    .line 393
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/cu;->getDialog()Landroid/app/Dialog;

    move-result-object v12

    .line 394
    if-nez v12, :cond_0

    .line 603
    :goto_0
    return-void

    .line 398
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v12, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 400
    invoke-virtual {v12}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v13

    .line 401
    const/4 v2, 0x2

    invoke-virtual {v13, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 403
    invoke-virtual {v13}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    .line 404
    const v2, 0x3ecccccd    # 0.4f

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 405
    const/16 v2, 0x33

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 407
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/AllInOneActivity;

    .line 408
    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 410
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/detail/cu;->H:I

    iput v3, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 411
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/detail/cu;->I:I

    iput v3, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 413
    const/4 v11, 0x0

    .line 414
    sget-object v10, Lcom/android/calendar/detail/df;->a:Lcom/android/calendar/detail/df;

    .line 415
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v4, 0x7f1201ac

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v5, 0x7f1201ad

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 417
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v6, 0x7f1201ae

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 419
    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v0, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v16, v0

    .line 420
    invoke-virtual {v15}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v0, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v17, v0

    .line 422
    const/4 v7, 0x0

    .line 423
    new-instance v6, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v6, v8, v9, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 424
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/calendar/detail/cu;->ac:Z

    if-eqz v8, :cond_1

    invoke-static {v2}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 425
    invoke-static {v2}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v7

    .line 426
    invoke-static {v2}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v6

    .line 429
    :cond_1
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_9

    const/4 v8, 0x1

    .line 430
    :goto_1
    const v9, 0x7f0a0003

    invoke-virtual {v15, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    .line 431
    invoke-virtual {v2}, Lcom/android/calendar/AllInOneActivity;->i()Z

    move-result v18

    .line 433
    if-nez v18, :cond_2

    if-eqz v8, :cond_a

    :cond_2
    const/4 v2, 0x0

    .line 434
    :goto_2
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v19

    const v20, 0x7f0c0215

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    mul-int/lit8 v20, v20, 0x2

    sub-int v19, v19, v20

    sub-int v19, v19, v2

    .line 436
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v20

    .line 438
    const v2, 0x7f0c01d4

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 439
    const v2, 0x7f0c01d3

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 440
    const v2, 0x7f0c01d2

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 441
    if-eqz v7, :cond_b

    const/4 v2, 0x0

    .line 442
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/calendar/detail/cu;->ac:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3

    if-eqz v9, :cond_c

    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const-string v24, "status_bar_height"

    invoke-static/range {v24 .. v24}, Lcom/android/calendar/common/b/a;->b(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 444
    :goto_4
    const v24, 0x7f0c01d5

    move/from16 v0, v24

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 446
    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    add-int v24, v24, v9

    move/from16 v0, v24

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 448
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->D:I

    move/from16 v24, v0

    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    .line 449
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->E:I

    move/from16 v25, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    .line 459
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->D:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->D:I

    move/from16 v26, v0

    if-nez v26, :cond_5

    .line 460
    :cond_4
    const/16 v26, -0x2

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/detail/cu;->D:I

    .line 463
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->E:I

    move/from16 v26, v0

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->E:I

    move/from16 v26, v0

    if-nez v26, :cond_7

    .line 464
    :cond_6
    const/16 v26, -0x2

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/detail/cu;->E:I

    .line 467
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->D:I

    move/from16 v26, v0

    const/16 v27, -0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/detail/cu;->E:I

    move/from16 v26, v0

    const/16 v27, -0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_d

    .line 468
    :cond_8
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v2, v19, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 469
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v2, v20, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 470
    sget-object v7, Lcom/android/calendar/detail/df;->a:Lcom/android/calendar/detail/df;

    move v6, v11

    .line 571
    :goto_5
    invoke-virtual {v13, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 573
    sget-object v2, Lcom/android/calendar/detail/de;->a:[I

    invoke-virtual {v7}, Lcom/android/calendar/detail/df;->ordinal()I

    move-result v7

    aget v2, v2, v7

    packed-switch v2, :pswitch_data_0

    .line 602
    :goto_6
    invoke-virtual {v12}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 429
    :cond_9
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 433
    :cond_a
    const v2, 0x7f0c0317

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    goto/16 :goto_2

    .line 441
    :cond_b
    const v2, 0x7f0d000c

    invoke-virtual {v15, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto/16 :goto_3

    .line 442
    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 472
    :cond_d
    if-nez v7, :cond_10

    if-nez v8, :cond_10

    if-nez v18, :cond_10

    move/from16 v0, v24

    move/from16 v1, v19

    if-le v0, v1, :cond_10

    .line 473
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v2, v24, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 474
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v25, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 475
    sget-object v7, Lcom/android/calendar/detail/df;->c:Lcom/android/calendar/detail/df;

    .line 476
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v2, v25, v2

    sub-int v2, v2, v23

    .line 478
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v8, v10

    if-ge v8, v9, :cond_f

    .line 479
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v10, v6, Landroid/graphics/Rect;->top:I

    sub-int v10, v9, v10

    sub-int/2addr v8, v10

    add-int/2addr v2, v8

    .line 480
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v9, v6

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 486
    :cond_e
    :goto_7
    add-int v6, v2, v22

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v8, v15

    if-le v6, v8, :cond_1c

    .line 487
    sget-object v7, Lcom/android/calendar/detail/df;->a:Lcom/android/calendar/detail/df;

    move v6, v2

    goto :goto_5

    .line 481
    :cond_f
    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/2addr v8, v9

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v8, v9

    move/from16 v0, v17

    if-le v8, v0, :cond_e

    .line 482
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v17, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v10

    sub-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 483
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v17, v6

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v6, v8

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_7

    .line 489
    :cond_10
    if-eqz v7, :cond_14

    iget v9, v6, Landroid/graphics/Rect;->top:I

    add-int v9, v9, v25

    iget v11, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v11

    if-lez v9, :cond_14

    iget v9, v6, Landroid/graphics/Rect;->top:I

    add-int v9, v9, v25

    iget v11, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v11, v11, 0x3

    add-int/2addr v9, v11

    move/from16 v0, v17

    if-le v9, v0, :cond_14

    .line 491
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v24, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 492
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int v2, v25, v2

    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 493
    sget-object v7, Lcom/android/calendar/detail/df;->d:Lcom/android/calendar/detail/df;

    .line 494
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v2, v23

    .line 496
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    if-gez v8, :cond_13

    .line 497
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 498
    iget v8, v6, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 504
    :cond_11
    :goto_8
    move/from16 v0, v21

    if-lt v2, v0, :cond_12

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v6, v8

    add-int/2addr v6, v2

    add-int v6, v6, v22

    sub-int v8, v16, v21

    if-le v6, v8, :cond_1c

    .line 506
    :cond_12
    sget-object v7, Lcom/android/calendar/detail/df;->a:Lcom/android/calendar/detail/df;

    move v6, v2

    goto/16 :goto_5

    .line 499
    :cond_13
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/2addr v8, v9

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v8, v9

    move/from16 v0, v16

    if-le v8, v0, :cond_11

    .line 500
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v9, v6, Landroid/graphics/Rect;->left:I

    sub-int v9, v16, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v9, v10

    sub-int/2addr v8, v9

    add-int/2addr v2, v8

    .line 501
    iget v8, v6, Landroid/graphics/Rect;->left:I

    sub-int v8, v16, v8

    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v8, v9

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_8

    .line 508
    :cond_14
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    invoke-virtual {v9}, Lcom/android/calendar/al;->g()I

    move-result v9

    packed-switch v9, :pswitch_data_1

    move-object v7, v10

    .line 555
    :goto_9
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    div-int/lit8 v8, v8, 0x2

    sub-int v8, v25, v8

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 556
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v8, v25, v8

    sub-int v8, v8, v23

    .line 557
    iget v9, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v9, v10

    iget v10, v6, Landroid/graphics/Rect;->top:I

    sub-int v10, v17, v10

    if-le v9, v10, :cond_1b

    .line 558
    iget v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    sub-int v9, v17, v9

    iget v10, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v9, v10

    sub-int/2addr v2, v9

    add-int/2addr v2, v8

    .line 559
    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int v6, v17, v6

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v6, v8

    iput v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 565
    :goto_a
    add-int v6, v2, v22

    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    sub-int/2addr v8, v15

    if-le v6, v8, :cond_1c

    .line 566
    sget-object v7, Lcom/android/calendar/detail/df;->a:Lcom/android/calendar/detail/df;

    move v6, v2

    goto/16 :goto_5

    .line 510
    :pswitch_0
    div-int/lit8 v7, v16, 0x2

    iget v8, v6, Landroid/graphics/Rect;->left:I

    add-int v8, v8, v24

    if-ge v7, v8, :cond_15

    .line 511
    sget-object v7, Lcom/android/calendar/detail/df;->c:Lcom/android/calendar/detail/df;

    .line 512
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v8, v24, v8

    add-int v8, v8, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 514
    :cond_15
    sget-object v7, Lcom/android/calendar/detail/df;->b:Lcom/android/calendar/detail/df;

    .line 515
    sub-int v8, v24, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 520
    :pswitch_1
    if-eqz v7, :cond_17

    .line 521
    div-int/lit8 v7, v16, 0x2

    iget v8, v6, Landroid/graphics/Rect;->left:I

    add-int v8, v8, v24

    if-ge v7, v8, :cond_16

    .line 522
    sget-object v7, Lcom/android/calendar/detail/df;->c:Lcom/android/calendar/detail/df;

    .line 523
    iget v8, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v8, v24, v8

    add-int v8, v8, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 525
    :cond_16
    sget-object v7, Lcom/android/calendar/detail/df;->b:Lcom/android/calendar/detail/df;

    .line 526
    sub-int v8, v24, v21

    iput v8, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_9

    .line 529
    :cond_17
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v24, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 530
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v7, :cond_18

    .line 531
    move/from16 v0, v24

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 533
    :cond_18
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v0, v24

    if-le v0, v7, :cond_19

    sget-object v7, Lcom/android/calendar/detail/df;->c:Lcom/android/calendar/detail/df;

    goto/16 :goto_9

    :cond_19
    sget-object v7, Lcom/android/calendar/detail/df;->b:Lcom/android/calendar/detail/df;

    goto/16 :goto_9

    .line 538
    :pswitch_2
    if-eqz v8, :cond_1a

    .line 539
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v19, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 540
    sget-object v7, Lcom/android/calendar/detail/df;->b:Lcom/android/calendar/detail/df;

    goto/16 :goto_9

    .line 542
    :cond_1a
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v24, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 543
    sget-object v7, Lcom/android/calendar/detail/df;->c:Lcom/android/calendar/detail/df;

    goto/16 :goto_9

    .line 548
    :pswitch_3
    iget v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int v7, v19, v7

    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 549
    sget-object v7, Lcom/android/calendar/detail/df;->b:Lcom/android/calendar/detail/df;

    goto/16 :goto_9

    .line 560
    :cond_1b
    iget v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    if-ge v6, v2, :cond_1d

    .line 561
    iget v6, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v6, v2

    add-int/2addr v6, v8

    .line 562
    iput v2, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    move v2, v6

    goto/16 :goto_a

    .line 575
    :pswitch_4
    const/4 v2, 0x4

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 576
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 577
    const/4 v2, 0x4

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 581
    :pswitch_5
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 582
    const/16 v4, 0x30

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 583
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 584
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 588
    :pswitch_6
    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 589
    const/16 v3, 0x30

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 590
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v6, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 591
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    .line 595
    :pswitch_7
    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 596
    const/4 v3, 0x3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 597
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v3, v4, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 598
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_1c
    move v6, v2

    goto/16 :goto_5

    :cond_1d
    move v2, v8

    goto/16 :goto_a

    .line 573
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 508
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic d(Lcom/android/calendar/detail/cu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_0

    .line 617
    :goto_0
    return-void

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->w:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->w:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 612
    iget v1, p0, Lcom/android/calendar/detail/cu;->I:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/detail/cu;->I:I

    .line 613
    iget v0, p0, Lcom/android/calendar/detail/cu;->I:I

    iget v1, p0, Lcom/android/calendar/detail/cu;->J:I

    if-le v0, v1, :cond_1

    .line 614
    iget v0, p0, Lcom/android/calendar/detail/cu;->J:I

    iput v0, p0, Lcom/android/calendar/detail/cu;->I:I

    .line 616
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->c()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/detail/cu;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Z:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f015d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 655
    return-void
.end method

.method private f()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    .line 671
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 672
    iget-wide v2, p0, Lcom/android/calendar/detail/cu;->Y:J

    sub-long v2, v0, v2

    const-wide/16 v6, 0x258

    cmp-long v2, v2, v6

    if-gez v2, :cond_1

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 675
    :cond_1
    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->Y:J

    .line 677
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    const-wide/16 v2, 0x8

    iget-wide v7, p0, Lcom/android/calendar/detail/cu;->s:J

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x1

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    move-object v13, v4

    move-object v14, v4

    invoke-virtual/range {v0 .. v14}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;Landroid/text/format/Time;JIJZLjava/lang/String;Landroid/content/ComponentName;)V

    .line 679
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/detail/cu;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->B:Z

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/detail/cu;)J
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/android/calendar/detail/cu;->s:J

    return-wide v0
.end method

.method static synthetic h(Lcom/android/calendar/detail/cu;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    return-object v0
.end method

.method private h()V
    .locals 6

    .prologue
    .line 685
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 686
    iget-wide v2, p0, Lcom/android/calendar/detail/cu;->X:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x258

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 695
    :goto_0
    return-void

    .line 689
    :cond_0
    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->X:J

    .line 691
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 693
    :goto_1
    new-instance v1, Lcom/android/calendar/cz;

    iget-object v2, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/android/calendar/cz;-><init>(Landroid/app/Activity;Z)V

    .line 694
    iget-wide v2, p0, Lcom/android/calendar/detail/cu;->s:J

    iget-object v0, p0, Lcom/android/calendar/detail/cu;->ah:Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v3, v0}, Lcom/android/calendar/cz;->a(JLjava/lang/Runnable;)V

    goto :goto_0

    .line 691
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private i()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 698
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    const-wide/32 v2, 0x200000

    iget-wide v6, p0, Lcom/android/calendar/detail/cu;->s:J

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v1, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v9}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JLandroid/text/format/Time;Landroid/text/format/Time;JIZ)V

    .line 700
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 703
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/detail/cu;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    return v0
.end method

.method static synthetic j(Lcom/android/calendar/detail/cu;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    return-object v0
.end method

.method private j()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 706
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    .line 707
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "enterprise_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 708
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    :goto_0
    return-void

    .line 711
    :cond_0
    new-instance v0, Lcom/android/calendar/gr;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-direct {v0, v1, v3}, Lcom/android/calendar/gr;-><init>(Landroid/app/Activity;Z)V

    const-wide/16 v2, -0x1

    iget-wide v4, p0, Lcom/android/calendar/detail/cu;->s:J

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/gr;->a(J)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/detail/cu;)Landroid/widget/PopupMenu;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->y:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 769
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->V:[I

    const/4 v1, 0x0

    const v2, 0x7f02013c

    aput v2, v0, v1

    .line 770
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->V:[I

    const/4 v1, 0x2

    const v2, 0x7f02013b

    aput v2, v0, v1

    .line 772
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->W:[Ljava/lang/CharSequence;

    .line 773
    return-void
.end method

.method private l()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 776
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/cu;->e:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/dh;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    return-void
.end method

.method static synthetic l(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->f()V

    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1037
    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-eqz v1, :cond_1

    .line 1038
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->x:Landroid/widget/ImageButton;

    new-instance v2, Lcom/android/calendar/detail/da;

    invoke-direct {v2, p0}, Lcom/android/calendar/detail/da;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1091
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->x:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1114
    :cond_0
    :goto_0
    return-void

    .line 1094
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 1095
    iget-object v1, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    const v2, 0x7f120342

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1096
    iget-object v2, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    const v3, 0x7f120343

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1097
    iget-object v3, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    const v4, 0x7f120344

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1099
    if-eqz v1, :cond_2

    .line 1100
    iget-boolean v4, p0, Lcom/android/calendar/detail/cu;->ad:Z

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1101
    iget-boolean v4, p0, Lcom/android/calendar/detail/cu;->ad:Z

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1104
    :cond_2
    if-eqz v2, :cond_3

    .line 1105
    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->ad:Z

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1106
    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->ad:Z

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1109
    :cond_3
    if-eqz v3, :cond_0

    .line 1110
    invoke-static {}, Lcom/android/calendar/dz;->F()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method static synthetic m(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->i()V

    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1290
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 1291
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1292
    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 1293
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1295
    :cond_0
    return-void
.end method

.method static synthetic n(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->h()V

    return-void
.end method

.method static synthetic o(Lcom/android/calendar/detail/cu;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->j()V

    return-void
.end method

.method static synthetic p(Lcom/android/calendar/detail/cu;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->ae:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic q(Lcom/android/calendar/detail/cu;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 634
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->l()V

    .line 637
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 620
    iput p1, p0, Lcom/android/calendar/detail/cu;->D:I

    iput p1, p0, Lcom/android/calendar/detail/cu;->F:I

    .line 621
    iput p2, p0, Lcom/android/calendar/detail/cu;->E:I

    iput p2, p0, Lcom/android/calendar/detail/cu;->G:I

    .line 622
    return-void
.end method

.method public a(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 1141
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1143
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1144
    const v1, 0x7f1202d3

    if-eq p2, v1, :cond_0

    const v1, 0x7f1202d9

    if-eq p2, v1, :cond_0

    const v1, 0x7f1202db

    if-ne p2, v1, :cond_1

    .line 1147
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c01de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1150
    :cond_1
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 646
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->a()V

    .line 651
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1282
    iput-boolean p1, p0, Lcom/android/calendar/detail/cu;->u:Z

    .line 1283
    return-void
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    return-object v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 641
    const-wide/32 v0, 0x10000080

    return-wide v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 268
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onAttach(Landroid/app/Activity;)V

    .line 269
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    .line 270
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 271
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    .line 273
    :cond_0
    new-instance v0, Lcom/android/calendar/c/a;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/c/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    .line 274
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    new-instance v1, Lcom/android/calendar/c/c;

    iget-object v2, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Lcom/android/calendar/c/c;-><init>(Landroid/net/Uri;Z)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/c/a;->a(Lcom/android/calendar/c/c;)V

    .line 275
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->ac:Z

    .line 277
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 279
    const v1, 0x7f0c044e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/detail/cu;->H:I

    .line 280
    const v1, 0x7f0c044c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/detail/cu;->I:I

    .line 281
    const v1, 0x7f0c044b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/cu;->J:I

    .line 283
    const/4 v0, 0x2

    const v1, 0x103013b

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/detail/cu;->setStyle(II)V

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->ae:Landroid/view/accessibility/AccessibilityManager;

    .line 291
    new-instance v0, Lcom/android/calendar/detail/dh;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/detail/dh;-><init>(Lcom/android/calendar/detail/cu;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    .line 292
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    new-instance v1, Lcom/android/calendar/detail/cv;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/cv;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/dh;->a(Lcom/android/calendar/detail/dg;)V

    .line 302
    return-void

    .line 285
    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/calendar/detail/cu;->setHasOptionsMenu(Z)V

    .line 286
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/android/calendar/detail/cu;->setStyle(II)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 726
    invoke-super {p0, p1, p2}, Lcom/android/calendar/detail/a;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 727
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-nez v0, :cond_0

    .line 728
    const v0, 0x7f110009

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 729
    iput-object p1, p0, Lcom/android/calendar/detail/cu;->v:Landroid/view/Menu;

    .line 731
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f1202d6

    const v6, 0x7f1202d3

    const v5, 0x7f1202d0

    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 306
    if-eqz p3, :cond_0

    .line 307
    const-string v0, "key_fragment_is_dialog"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    if-nez v0, :cond_2

    .line 311
    if-eqz p3, :cond_1

    .line 312
    const-string v0, "configuration_launch_orientation"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/cu;->af:I

    .line 313
    const-string v0, "key_task_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->s:J

    .line 314
    const-string v0, "x_coordinate"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/cu;->F:I

    .line 315
    const-string v0, "y_coordinate"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/detail/cu;->G:I

    .line 316
    iget v0, p0, Lcom/android/calendar/detail/cu;->af:I

    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_5

    .line 317
    iget v0, p0, Lcom/android/calendar/detail/cu;->F:I

    iput v0, p0, Lcom/android/calendar/detail/cu;->D:I

    .line 318
    iget v0, p0, Lcom/android/calendar/detail/cu;->G:I

    iput v0, p0, Lcom/android/calendar/detail/cu;->E:I

    .line 324
    :cond_1
    :goto_0
    sget-object v0, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/calendar/detail/cu;->s:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->h:Landroid/net/Uri;

    .line 327
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/android/calendar/fd;->a(Landroid/app/Activity;Landroid/app/Fragment;Z)V

    .line 329
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-eqz v0, :cond_6

    .line 330
    const v0, 0x7f0400b0

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    .line 331
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->x:Landroid/widget/ImageButton;

    .line 337
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    .line 338
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    invoke-virtual {v0, v5, p0}, Lcom/android/calendar/al;->a(ILcom/android/calendar/ap;)V

    .line 340
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->w:Landroid/widget/ScrollView;

    .line 341
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->w:Landroid/widget/ScrollView;

    new-instance v1, Lcom/android/calendar/detail/cw;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/cw;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 350
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->Z:Landroid/widget/TextView;

    .line 351
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->Z:Landroid/widget/TextView;

    new-instance v1, Lcom/android/calendar/detail/cx;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/cx;-><init>(Lcom/android/calendar/detail/cu;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->O:Landroid/view/View;

    .line 364
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202b5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->Q:Landroid/widget/CheckBox;

    .line 365
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    .line 366
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    .line 367
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    const v1, 0x7f1202d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->T:Landroid/widget/TextView;

    .line 370
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->R:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v6, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 371
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->S:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v7, v1}, Lcom/android/calendar/detail/cu;->b(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 372
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->k()V

    .line 375
    if-eqz p3, :cond_3

    .line 376
    const-string v0, "key_fragment_is_dialog"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    .line 379
    :cond_3
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->e()V

    .line 380
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->l()V

    .line 382
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->X:J

    .line 383
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/detail/cu;->Y:J

    .line 385
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-eqz v0, :cond_4

    .line 386
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->c()V

    .line 389
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    return-object v0

    .line 320
    :cond_5
    iput v2, p0, Lcom/android/calendar/detail/cu;->D:I

    .line 321
    iput v2, p0, Lcom/android/calendar/detail/cu;->E:I

    goto/16 :goto_0

    .line 334
    :cond_6
    const v0, 0x7f0400ae

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/cu;->P:Landroid/view/View;

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->c()V

    .line 1277
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->N:Lcom/android/calendar/al;

    const v1, 0x7f1202d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/al;->a(Ljava/lang/Integer;)V

    .line 1278
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroy()V

    .line 1279
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 629
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onDestroyView()V

    .line 630
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 741
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->A:Z

    if-eqz v0, :cond_0

    .line 742
    const/4 v0, 0x0

    .line 765
    :goto_0
    return v0

    .line 745
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 765
    :goto_1
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 747
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 748
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->K:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 749
    const/4 v0, 0x1

    goto :goto_0

    .line 751
    :sswitch_1
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->f()V

    goto :goto_1

    .line 754
    :sswitch_2
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->h()V

    goto :goto_1

    .line 757
    :sswitch_3
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->i()V

    goto :goto_1

    .line 760
    :sswitch_4
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->j()V

    goto :goto_1

    .line 745
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f120342 -> :sswitch_1
        0x7f120343 -> :sswitch_2
        0x7f120344 -> :sswitch_3
        0x7f120345 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->B:Z

    .line 1249
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->ah:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/dh;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1250
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onPause()V

    .line 1251
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 735
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->m()V

    .line 736
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 737
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1255
    invoke-super {p0}, Lcom/android/calendar/detail/a;->onResume()V

    .line 1256
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1257
    invoke-virtual {p0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1272
    :goto_0
    return-void

    .line 1260
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->B:Z

    .line 1262
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->C:Z

    if-eqz v0, :cond_1

    .line 1263
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->z:Lcom/android/calendar/detail/dh;

    iget-object v1, p0, Lcom/android/calendar/detail/cu;->ah:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/dh;->post(Ljava/lang/Runnable;)Z

    .line 1266
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/detail/cu;->u:Z

    if-eqz v0, :cond_2

    .line 1267
    invoke-direct {p0}, Lcom/android/calendar/detail/cu;->l()V

    .line 1271
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->ag:Lcom/android/calendar/c/a;

    invoke-virtual {v0}, Lcom/android/calendar/c/a;->b()V

    goto :goto_0

    .line 1269
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/cu;->L:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/hj;->t(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/cu;->ab:Z

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 716
    invoke-super {p0, p1}, Lcom/android/calendar/detail/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 717
    const-string v0, "key_task_id"

    iget-wide v2, p0, Lcom/android/calendar/detail/cu;->s:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 718
    const-string v0, "key_fragment_is_dialog"

    iget-boolean v1, p0, Lcom/android/calendar/detail/cu;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 719
    const-string v0, "x_coordinate"

    iget v1, p0, Lcom/android/calendar/detail/cu;->F:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 720
    const-string v0, "y_coordinate"

    iget v1, p0, Lcom/android/calendar/detail/cu;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 721
    const-string v0, "configuration_launch_orientation"

    iget v1, p0, Lcom/android/calendar/detail/cu;->af:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 722
    return-void
.end method
