.class public Lcom/android/calendar/detail/TaskInfoActivity;
.super Landroid/app/Activity;
.source "TaskInfoActivity.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/detail/cu;

.field private c:J

.field private d:Lcom/android/calendar/al;

.field private e:Landroid/database/ContentObserver;

.field private f:Landroid/content/BroadcastReceiver;

.field private final g:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/android/calendar/detail/TaskInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/TaskInfoActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 166
    new-instance v0, Lcom/android/calendar/detail/cr;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/cr;-><init>(Lcom/android/calendar/detail/TaskInfoActivity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->f:Landroid/content/BroadcastReceiver;

    .line 191
    new-instance v0, Lcom/android/calendar/detail/cs;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/cs;-><init>(Lcom/android/calendar/detail/TaskInfoActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->g:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/TaskInfoActivity;)Lcom/android/calendar/detail/cu;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->b:Lcom/android/calendar/detail/cu;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/TaskInfoActivity;)Lcom/android/calendar/al;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->d:Lcom/android/calendar/al;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v6, 0x7f12006b

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->d:Lcom/android/calendar/al;

    .line 56
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 59
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 60
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 63
    :cond_0
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/TaskInfoActivity;->setContentView(I)V

    .line 66
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/cu;

    iput-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->b:Lcom/android/calendar/detail/cu;

    .line 70
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 71
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/calendar/detail/TaskInfoActivity;->c:J

    .line 72
    const/4 v1, 0x0

    .line 74
    if-eqz v0, :cond_1

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_1

    .line 78
    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/TaskInfoActivity;->c:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_2

    .line 87
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 90
    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 91
    const-string v2, "clock.date_format_changed"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    iget-object v2, p0, Lcom/android/calendar/detail/TaskInfoActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/detail/TaskInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 93
    new-instance v0, Lcom/android/calendar/detail/ct;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/ct;-><init>(Lcom/android/calendar/detail/TaskInfoActivity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->e:Landroid/database/ContentObserver;

    .line 96
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->b:Lcom/android/calendar/detail/cu;

    if-nez v0, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 99
    new-instance v2, Lcom/android/calendar/detail/cu;

    iget-wide v4, p0, Lcom/android/calendar/detail/TaskInfoActivity;->c:J

    invoke-direct {v2, p0, v4, v5, v1}, Lcom/android/calendar/detail/cu;-><init>(Landroid/content/Context;JZ)V

    iput-object v2, p0, Lcom/android/calendar/detail/TaskInfoActivity;->b:Lcom/android/calendar/detail/cu;

    .line 100
    iget-object v1, p0, Lcom/android/calendar/detail/TaskInfoActivity;->b:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0, v6, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 101
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 103
    :cond_3
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    sget-object v0, Lcom/android/calendar/detail/TaskInfoActivity;->a:Ljava/lang/String;

    const-string v2, "No task id"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 159
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/TaskInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 163
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 164
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lcom/android/calendar/detail/TaskInfoActivity;->setIntent(Landroid/content/Intent;)V

    .line 129
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 108
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 117
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 110
    :pswitch_0
    invoke-static {p0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 111
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->finish()V

    .line 112
    const/4 v0, 0x1

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->g:Landroid/database/ContentObserver;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/TaskInfoActivity;->g:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/TaskInfoActivity;->e:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/detail/TaskInfoActivity;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 151
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 138
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 139
    invoke-virtual {p0}, Lcom/android/calendar/detail/TaskInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/hj;->a:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/detail/TaskInfoActivity;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 140
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 134
    return-void
.end method
