.class Lcom/android/calendar/detail/aq;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/ap;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/ap;)V
    .locals 0

    .prologue
    .line 2374
    iput-object p1, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2377
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2425
    :cond_0
    :goto_0
    return v2

    .line 2379
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->m(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto :goto_0

    .line 2383
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->d(Lcom/android/calendar/detail/EventInfoFragment;Z)V

    goto :goto_0

    .line 2387
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0, v2}, Lcom/android/calendar/detail/EventInfoFragment;->d(Lcom/android/calendar/detail/EventInfoFragment;Z)V

    goto :goto_0

    .line 2391
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->E(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto :goto_0

    .line 2395
    :sswitch_4
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->F(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto :goto_0

    .line 2398
    :sswitch_5
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->G(Lcom/android/calendar/detail/EventInfoFragment;)V

    .line 2399
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2402
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2403
    :catch_0
    move-exception v0

    .line 2404
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2409
    :sswitch_6
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->H(Lcom/android/calendar/detail/EventInfoFragment;)V

    .line 2410
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2413
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2414
    :catch_1
    move-exception v0

    .line 2415
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2420
    :sswitch_7
    iget-object v0, p0, Lcom/android/calendar/detail/aq;->a:Lcom/android/calendar/detail/ap;

    iget-object v0, v0, Lcom/android/calendar/detail/ap;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->I(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto/16 :goto_0

    .line 2377
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f120323 -> :sswitch_5
        0x7f120324 -> :sswitch_7
        0x7f120325 -> :sswitch_6
        0x7f12033d -> :sswitch_0
        0x7f12033e -> :sswitch_1
        0x7f12033f -> :sswitch_2
        0x7f120340 -> :sswitch_3
        0x7f120341 -> :sswitch_4
    .end sparse-switch
.end method
