.class public Lcom/android/calendar/detail/w;
.super Ljava/lang/Object;
.source "EditResponseHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:I

.field private c:Landroid/app/AlertDialog;

.field private d:Lcom/android/calendar/detail/z;

.field private final e:Landroid/content/DialogInterface$OnClickListener;

.field private f:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/w;->b:I

    .line 57
    new-instance v0, Lcom/android/calendar/detail/x;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/x;-><init>(Lcom/android/calendar/detail/w;)V

    iput-object v0, p0, Lcom/android/calendar/detail/w;->e:Landroid/content/DialogInterface$OnClickListener;

    .line 91
    new-instance v0, Lcom/android/calendar/detail/y;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/y;-><init>(Lcom/android/calendar/detail/w;)V

    iput-object v0, p0, Lcom/android/calendar/detail/w;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 69
    iput-object p1, p0, Lcom/android/calendar/detail/w;->a:Landroid/app/Activity;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/w;I)I
    .locals 0

    .prologue
    .line 30
    iput p1, p0, Lcom/android/calendar/detail/w;->b:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/w;)Lcom/android/calendar/detail/z;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/calendar/detail/w;->d:Lcom/android/calendar/detail/z;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/w;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/calendar/detail/w;->c:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/android/calendar/detail/w;->b:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/android/calendar/detail/w;->b:I

    .line 78
    return-void
.end method

.method public a(Lcom/android/calendar/detail/z;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/android/calendar/detail/w;->d:Lcom/android/calendar/detail/z;

    .line 82
    return-void
.end method

.method public b(I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 104
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/detail/w;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f00af

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090009

    iget-object v2, p0, Lcom/android/calendar/detail/w;->f:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    iget-object v2, p0, Lcom/android/calendar/detail/w;->e:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    .line 109
    iput-object v0, p0, Lcom/android/calendar/detail/w;->c:Landroid/app/AlertDialog;

    .line 110
    iget-object v1, p0, Lcom/android/calendar/detail/w;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 111
    if-ne p1, v3, :cond_0

    .line 112
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 113
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 115
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method
