.class public Lcom/android/calendar/detail/h;
.super Landroid/app/DialogFragment;
.source "EASResponseDialog.java"

# interfaces
.implements Lcom/android/calendar/detail/bp;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private f:Lcom/android/calendar/detail/z;

.field private g:Landroid/content/DialogInterface$OnClickListener;

.field private h:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/android/calendar/detail/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/h;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JILjava/lang/String;Lcom/android/calendar/detail/z;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 64
    new-instance v0, Lcom/android/calendar/detail/i;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/i;-><init>(Lcom/android/calendar/detail/h;)V

    iput-object v0, p0, Lcom/android/calendar/detail/h;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 102
    new-instance v0, Lcom/android/calendar/detail/j;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/j;-><init>(Lcom/android/calendar/detail/h;)V

    iput-object v0, p0, Lcom/android/calendar/detail/h;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/h;->setRetainInstance(Z)V

    .line 40
    iput-wide p1, p0, Lcom/android/calendar/detail/h;->b:J

    .line 41
    iput p3, p0, Lcom/android/calendar/detail/h;->d:I

    .line 42
    iput-object p4, p0, Lcom/android/calendar/detail/h;->c:Ljava/lang/String;

    .line 43
    iget v0, p0, Lcom/android/calendar/detail/h;->d:I

    invoke-static {v0}, Lcom/android/calendar/detail/h;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/h;->e:I

    .line 44
    iput-object p5, p0, Lcom/android/calendar/detail/h;->f:Lcom/android/calendar/detail/z;

    .line 45
    return-void
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 111
    packed-switch p0, :pswitch_data_0

    .line 119
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 113
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 115
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 117
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/calendar/detail/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/calendar/detail/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/h;)J
    .locals 2

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/android/calendar/detail/h;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/android/calendar/detail/h;)I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lcom/android/calendar/detail/h;->e:I

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/detail/h;)Lcom/android/calendar/detail/z;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/android/calendar/detail/h;->f:Lcom/android/calendar/detail/z;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/android/calendar/detail/h;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/detail/h;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    .line 53
    :cond_1
    const v0, 0x7f09000c

    .line 54
    invoke-virtual {p0}, Lcom/android/calendar/detail/h;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 56
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/detail/h;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    iget v3, p0, Lcom/android/calendar/detail/h;->e:I

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 58
    iget-object v1, p0, Lcom/android/calendar/detail/h;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    const v0, 0x7f0f0163

    iget-object v1, p0, Lcom/android/calendar/detail/h;->h:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 61
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method
