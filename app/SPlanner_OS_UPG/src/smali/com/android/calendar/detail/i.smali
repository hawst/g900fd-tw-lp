.class Lcom/android/calendar/detail/i;
.super Ljava/lang/Object;
.source "EASResponseDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/h;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/h;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-virtual {v0}, Lcom/android/calendar/detail/h;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-virtual {v0}, Lcom/android/calendar/detail/h;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.email.intent.action.CALENDAR_MEETING_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    const-string v1, "account_email"

    iget-object v2, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v2}, Lcom/android/calendar/detail/h;->a(Lcom/android/calendar/detail/h;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v1, "event_id"

    iget-object v2, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v2}, Lcom/android/calendar/detail/h;->b(Lcom/android/calendar/detail/h;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 76
    packed-switch p2, :pswitch_data_0

    .line 91
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v1}, Lcom/android/calendar/detail/h;->d(Lcom/android/calendar/detail/h;)Lcom/android/calendar/detail/z;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/calendar/detail/z;->a()V

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-virtual {v1, v0}, Lcom/android/calendar/detail/h;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    sget-object v0, Lcom/android/calendar/detail/h;->a:Ljava/lang/String;

    const-string v1, "Error: Could not find MEETING_RESPONSE_ACTION activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 78
    :pswitch_0
    const-string v1, "meeting_response"

    iget-object v2, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v2}, Lcom/android/calendar/detail/h;->c(Lcom/android/calendar/detail/h;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 81
    :pswitch_1
    const-string v1, "meeting_response"

    iget-object v2, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v2}, Lcom/android/calendar/detail/h;->c(Lcom/android/calendar/detail/h;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 84
    :pswitch_2
    const-string v1, "meeting_response"

    iget-object v2, p0, Lcom/android/calendar/detail/i;->a:Lcom/android/calendar/detail/h;

    invoke-static {v2}, Lcom/android/calendar/detail/h;->c(Lcom/android/calendar/detail/h;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
