.class public interface abstract Lcom/android/calendar/detail/bp;
.super Ljava/lang/Object;
.source "IEventInfo.java"


# static fields
.field public static final i:[Ljava/lang/String;

.field public static final j:[Ljava/lang/String;

.field public static final k:Landroid/net/Uri;

.field public static final l:[Ljava/lang/String;

.field public static final m:[Ljava/lang/String;

.field public static final n:Ljava/util/regex/Pattern;

.field public static final o:Ljava/util/regex/Pattern;

.field public static final p:Ljava/util/regex/Pattern;

.field public static final q:Ljava/util/regex/Pattern;

.field public static final r:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "attendeeName"

    aput-object v1, v0, v4

    const-string v1, "attendeeEmail"

    aput-object v1, v0, v5

    const-string v1, "attendeeStatus"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/calendar/detail/bp;->i:[Ljava/lang/String;

    .line 86
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "member_id"

    aput-object v1, v0, v3

    const-string v1, "first_name"

    aput-object v1, v0, v4

    const-string v1, "last_name"

    aput-object v1, v0, v5

    const-string v1, "formatted_name"

    aput-object v1, v0, v6

    const-string v1, "headline"

    aput-object v1, v0, v2

    const/4 v1, 0x5

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "phone_number"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "picture_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "profile_url"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "position_title"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "position_company_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "last_updated_timestamp"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/detail/bp;->j:[Ljava/lang/String;

    .line 114
    const-string v0, "content://com.sec.android.app.sns3.sp.linkedin/people_lookup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->k:Landroid/net/Uri;

    .line 118
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "minutes"

    aput-object v1, v0, v4

    const-string v1, "method"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/detail/bp;->l:[Ljava/lang/String;

    .line 131
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "latitude"

    aput-object v1, v0, v3

    const-string v1, "longitude"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/calendar/detail/bp;->m:[Ljava/lang/String;

    .line 143
    const-string v0, "^.*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->n:Ljava/util/regex/Pattern;

    .line 150
    const-string v0, "\\s*[\\,;]?\\s*(?i:[\\,;#(]+|x|ext|extension|\\bp|pwd|password|code|cr|conf|conference|(?:conf\\.?)?(?:conference)?\\s*(?:bridge|code|id)|(?:pass)\\s*(?:code)?|(?:pin)\\s*(?:code)?|(?:participant)\'?s?\\s*(?:code)?|(?:attendee)\'?s?\\s*(?:code)?)\\.?\\s*[#\\:\\-]*\\s*([0-9]+)\\)*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->o:Ljava/util/regex/Pattern;

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "((\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.]+[0-9]))(?:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/detail/bp;->o:Ljava/util/regex/Pattern;

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->p:Ljava/util/regex/Pattern;

    .line 185
    const-string v0, "[0-9\\+\\-\\.\\(\\) ]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->q:Ljava/util/regex/Pattern;

    .line 190
    const-string v0, "[,;#(]|[xp]\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/detail/bp;->r:Ljava/util/regex/Pattern;

    return-void
.end method
