.class Lcom/android/calendar/detail/cb;
.super Lcom/android/calendar/ag;
.source "MeetingInvitationFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/bq;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/bq;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 757
    iput-object p1, p0, Lcom/android/calendar/detail/cb;->a:Lcom/android/calendar/detail/bq;

    .line 758
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 759
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;I)V
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/android/calendar/detail/cb;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->m(Lcom/android/calendar/detail/bq;)V

    .line 764
    return-void
.end method

.method protected a(ILjava/lang/Object;[Landroid/content/ContentProviderResult;)V
    .locals 4

    .prologue
    .line 772
    if-eqz p3, :cond_0

    array-length v0, p3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 773
    :cond_0
    sget-object v0, Lcom/android/calendar/detail/bq;->d:Ljava/lang/String;

    const-string v1, "onBatchComplete() - results is null"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :goto_0
    return-void

    .line 776
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/cb;->a:Lcom/android/calendar/detail/bq;

    const/4 v1, 0x0

    aget-object v1, p3, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/detail/bq;->f:J

    .line 777
    iget-object v0, p0, Lcom/android/calendar/detail/cb;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->m(Lcom/android/calendar/detail/bq;)V

    goto :goto_0
.end method
