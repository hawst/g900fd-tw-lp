.class public Lcom/android/calendar/detail/ci;
.super Landroid/content/AsyncQueryHandler;
.source "PresenceQueryHelper.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/ch;


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/ch;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/android/calendar/detail/ci;->a:Lcom/android/calendar/detail/ch;

    .line 115
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 116
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 120
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    check-cast p2, Lcom/android/calendar/event/aw;

    .line 129
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v2, v1

    move v0, v1

    .line 130
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 132
    if-lez v0, :cond_7

    .line 134
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 136
    :goto_2
    const/4 v2, 0x1

    move v4, v0

    move v0, v2

    move v2, v1

    move v1, v4

    goto :goto_1

    .line 139
    :cond_2
    if-eqz v0, :cond_3

    if-gtz v1, :cond_5

    .line 140
    :cond_3
    iget-object v0, p2, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    .line 141
    iget-boolean v1, v0, Lcom/android/calendar/at;->g:Z

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/android/calendar/at;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 142
    invoke-static {p2}, Lcom/android/calendar/detail/bq;->a(Lcom/android/calendar/event/aw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_4
    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 147
    :cond_5
    :try_start_1
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/android/calendar/detail/ci;->a:Lcom/android/calendar/detail/ch;

    invoke-static {v1}, Lcom/android/calendar/detail/ch;->a(Lcom/android/calendar/detail/ch;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/detail/cj;

    invoke-direct {v2, p0, p2}, Lcom/android/calendar/detail/cj;-><init>(Lcom/android/calendar/detail/ci;Lcom/android/calendar/event/aw;)V

    invoke-static {v1, p2, v2, v0}, Lcom/android/calendar/bi;->a(Landroid/content/Context;Lcom/android/calendar/event/aw;Ljava/lang/Runnable;Landroid/net/Uri;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_6

    .line 160
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    :cond_7
    move v0, v1

    move v1, v2

    goto :goto_2
.end method
