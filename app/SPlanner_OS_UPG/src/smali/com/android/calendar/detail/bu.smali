.class Lcom/android/calendar/detail/bu;
.super Ljava/lang/Object;
.source "MeetingInvitationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/bq;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/bq;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 348
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->d(Lcom/android/calendar/detail/bq;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-virtual {v0}, Lcom/android/calendar/detail/bq;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/bq;->b(Lcom/android/calendar/detail/bq;Z)Z

    .line 353
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->e(Lcom/android/calendar/detail/bq;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/detail/bv;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/bv;-><init>(Lcom/android/calendar/detail/bu;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 361
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 363
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->f(Lcom/android/calendar/detail/bq;)V

    goto :goto_0

    .line 366
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    invoke-static {v0}, Lcom/android/calendar/detail/bq;->g(Lcom/android/calendar/detail/bq;)V

    goto :goto_0

    .line 369
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/bu;->a:Lcom/android/calendar/detail/bq;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/detail/bq;->a(Lcom/android/calendar/detail/bq;Z[Ljava/lang/String;)V

    goto :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x7f1201c2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
