.class Lcom/android/calendar/detail/ay;
.super Ljava/lang/Object;
.source "EventInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/EventInfoFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/EventInfoFragment;)V
    .locals 0

    .prologue
    .line 3699
    iput-object p1, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3703
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->af(Lcom/android/calendar/detail/EventInfoFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EventInfoFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3729
    :cond_0
    :goto_0
    return-void

    .line 3707
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0, v4}, Lcom/android/calendar/detail/EventInfoFragment;->f(Lcom/android/calendar/detail/EventInfoFragment;Z)Z

    .line 3708
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->ag(Lcom/android/calendar/detail/EventInfoFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/detail/az;

    invoke-direct {v1, p0}, Lcom/android/calendar/detail/az;-><init>(Lcom/android/calendar/detail/ay;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3716
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 3718
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/detail/EventInfoFragment;->d(Lcom/android/calendar/detail/EventInfoFragment;Z)V

    goto :goto_0

    .line 3721
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0, v4}, Lcom/android/calendar/detail/EventInfoFragment;->d(Lcom/android/calendar/detail/EventInfoFragment;Z)V

    goto :goto_0

    .line 3724
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/detail/ay;->a:Lcom/android/calendar/detail/EventInfoFragment;

    invoke-static {v0}, Lcom/android/calendar/detail/EventInfoFragment;->E(Lcom/android/calendar/detail/EventInfoFragment;)V

    goto :goto_0

    .line 3716
    :pswitch_data_0
    .packed-switch 0x7f1201c2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
