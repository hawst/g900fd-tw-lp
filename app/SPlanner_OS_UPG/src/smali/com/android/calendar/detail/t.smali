.class Lcom/android/calendar/detail/t;
.super Ljava/lang/Object;
.source "EasyModeListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/r;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/r;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 18

    .prologue
    .line 213
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/android/calendar/detail/q;

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->e(Lcom/android/calendar/detail/r;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 215
    iget-wide v4, v10, Lcom/android/calendar/detail/q;->g:J

    .line 216
    iget-object v3, v10, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    iget-object v2, v10, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v2

    iget-object v3, v10, Lcom/android/calendar/detail/q;->e:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v4, v5, v3}, Lcom/android/calendar/detail/o;->a(JZ)V

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/detail/EasyModeListActivity;

    iget-object v2, v2, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    invoke-virtual {v2}, Landroid/view/ActionMode;->invalidate()V

    .line 225
    :goto_1
    return-void

    .line 216
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 221
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v2}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 222
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/detail/t;->a:Lcom/android/calendar/detail/r;

    invoke-static {v3}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v3

    const-wide/16 v4, 0x8

    iget-wide v6, v10, Lcom/android/calendar/detail/q;->g:J

    iget-wide v8, v10, Lcom/android/calendar/detail/q;->h:J

    iget-wide v10, v10, Lcom/android/calendar/detail/q;->i:J

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    goto :goto_1
.end method
