.class public Lcom/android/calendar/detail/EasyModeListActivity;
.super Landroid/app/Activity;
.source "EasyModeListActivity.java"


# instance fields
.field protected a:Lcom/android/calendar/detail/r;

.field protected b:Landroid/view/ActionMode;

.field private c:Lcom/android/calendar/c/a;

.field private d:Landroid/app/ActionBar;

.field private e:Landroid/view/Menu;

.field private f:J

.field private g:J

.field private h:Z

.field private i:Z

.field private j:Landroid/os/Handler;

.field private k:Lcom/android/calendar/cj;

.field private l:Ljava/util/ArrayList;

.field private m:Lcom/android/calendar/detail/l;

.field private n:Landroid/content/Context;

.field private final o:Lcom/android/calendar/c/d;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    .line 46
    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    .line 47
    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    .line 48
    iput-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    .line 49
    iput-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->g:J

    .line 52
    iput-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    .line 53
    iput-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->j:Landroid/os/Handler;

    .line 58
    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    .line 63
    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->n:Landroid/content/Context;

    .line 339
    new-instance v0, Lcom/android/calendar/detail/k;

    invoke-direct {v0, p0}, Lcom/android/calendar/detail/k;-><init>(Lcom/android/calendar/detail/EasyModeListActivity;)V

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->o:Lcom/android/calendar/c/d;

    .line 365
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/a;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->c:Lcom/android/calendar/c/a;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/detail/EasyModeListActivity;Lcom/android/calendar/c/a;)Lcom/android/calendar/c/a;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->c:Lcom/android/calendar/c/a;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EasyModeListActivity;Lcom/android/calendar/detail/l;)Lcom/android/calendar/detail/l;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->m:Lcom/android/calendar/detail/l;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/detail/EasyModeListActivity;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/EasyModeListActivity;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/EasyModeListActivity;->b(Z)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 290
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->d()Ljava/util/HashMap;

    move-result-object v4

    .line 293
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 294
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 295
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 296
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 298
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 299
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 305
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 306
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v3

    .line 307
    :goto_1
    if-ge v1, v4, :cond_2

    .line 308
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    iget-wide v6, v0, Lcom/android/calendar/dh;->b:J

    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-nez v0, :cond_3

    .line 309
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/dh;

    .line 310
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->k:Lcom/android/calendar/cj;

    iget-wide v2, v0, Lcom/android/calendar/dh;->m:J

    iget-wide v4, v0, Lcom/android/calendar/dh;->n:J

    iget-wide v6, v0, Lcom/android/calendar/dh;->b:J

    const/4 v8, -0x1

    move-object v9, p1

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/cj;->a(JJJILjava/lang/Runnable;)V

    .line 323
    :cond_2
    :goto_2
    return-void

    .line 307
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 315
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/Long;

    .line 317
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 318
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v4, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v4}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/calendar/detail/o;->b()I

    move-result v4

    if-ne v0, v4, :cond_5

    move v0, v2

    .line 320
    :goto_3
    iget-object v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->k:Lcom/android/calendar/cj;

    invoke-virtual {v2, v1, v0, p1}, Lcom/android/calendar/cj;->a([Ljava/lang/Long;ZLjava/lang/Runnable;)V

    goto :goto_2

    :cond_5
    move v0, v3

    .line 318
    goto :goto_3
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/calendar/detail/o;->c(Z)V

    .line 337
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/detail/EasyModeListActivity;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/detail/EasyModeListActivity;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    return p1
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 146
    :cond_0
    iput-boolean v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    .line 147
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/r;->a(Z)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/detail/EasyModeListActivity;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/detail/EasyModeListActivity;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    return p1
.end method

.method private d()V
    .locals 6

    .prologue
    const v5, 0x7f120339

    const v4, 0x7f120338

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 225
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->b()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 227
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 228
    iget-object v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    invoke-interface {v2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 229
    iget-object v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 230
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    invoke-interface {v0, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 231
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->e()V

    .line 232
    return-void

    .line 225
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/detail/EasyModeListActivity;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->f()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/detail/EasyModeListActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->j:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    const/16 v2, 0x9

    invoke-static {v0, v1, p0, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v2, 0x140

    if-ne v1, v2, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0377

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 245
    return-void
.end method

.method static synthetic f(Lcom/android/calendar/detail/EasyModeListActivity;)Lcom/android/calendar/c/d;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->o:Lcom/android/calendar/c/d;

    return-object v0
.end method

.method private f()V
    .locals 18

    .prologue
    .line 262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v2}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v2

    .line 263
    invoke-virtual {v2}, Lcom/android/calendar/detail/o;->d()Ljava/util/HashMap;

    move-result-object v4

    .line 265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 266
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 267
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 268
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 269
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 270
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 271
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 276
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v2}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/detail/o;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 277
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 278
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_2

    .line 279
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/dh;

    iget-wide v6, v2, Lcom/android/calendar/dh;->b:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v2, v6, v8

    if-nez v2, :cond_3

    .line 280
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/android/calendar/dh;

    .line 281
    invoke-static/range {p0 .. p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v2

    .line 282
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/detail/EasyModeListActivity;->n:Landroid/content/Context;

    const-wide/16 v4, 0x8

    iget-wide v6, v10, Lcom/android/calendar/dh;->b:J

    iget-wide v8, v10, Lcom/android/calendar/dh;->m:J

    iget-wide v10, v10, Lcom/android/calendar/dh;->n:J

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, -0x1

    invoke-virtual/range {v2 .. v17}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJJ)V

    .line 287
    :cond_2
    return-void

    .line 278
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1
.end method

.method static synthetic g(Lcom/android/calendar/detail/EasyModeListActivity;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->d()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 258
    iput-boolean p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    .line 259
    return-void
.end method

.method protected a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 129
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->m:Lcom/android/calendar/detail/l;

    if-nez v1, :cond_0

    .line 130
    new-instance v1, Lcom/android/calendar/detail/l;

    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/detail/l;-><init>(Lcom/android/calendar/detail/EasyModeListActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->m:Lcom/android/calendar/detail/l;

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    if-nez v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->m:Lcom/android/calendar/detail/l;

    invoke-virtual {p0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    .line 135
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v1, v0}, Lcom/android/calendar/detail/r;->a(Z)V

    .line 138
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->a()V

    .line 222
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 119
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/android/calendar/detail/o;->a(Z)V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->b:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 126
    :cond_1
    return-void

    .line 120
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const v6, 0x7f12006b

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    iput-object p0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->n:Landroid/content/Context;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->l:Ljava/util/ArrayList;

    .line 71
    new-instance v0, Lcom/android/calendar/cj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/cj;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->k:Lcom/android/calendar/cj;

    .line 73
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 76
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 77
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 80
    :cond_0
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/detail/EasyModeListActivity;->setContentView(I)V

    .line 82
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/r;

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    .line 85
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    .line 87
    const-string v1, "KEY_START_MILLIS"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    .line 88
    const-string v1, "KEY_END_MILLIS"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->g:J

    .line 91
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    .line 92
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 94
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->e()V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    if-nez v0, :cond_3

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/android/calendar/detail/r;

    iget-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    iget-wide v4, p0, Lcom/android/calendar/detail/EasyModeListActivity;->g:J

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/calendar/detail/r;-><init>(JJ)V

    iput-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    .line 101
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0, v6, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 102
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 104
    :cond_3
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 153
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 155
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 156
    iput-object p1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    .line 158
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->d()V

    .line 159
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->d:Landroid/app/ActionBar;

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 161
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 249
    invoke-static {p0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 250
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 251
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x1

    const/4 v10, 0x0

    .line 178
    iget-boolean v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    if-eqz v1, :cond_0

    .line 179
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 217
    :goto_0
    return v0

    .line 182
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 217
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 184
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->finish()V

    goto :goto_0

    .line 187
    :sswitch_1
    iput-boolean v10, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    .line 188
    iput-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    .line 189
    iget-object v1, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v1, v0}, Lcom/android/calendar/detail/r;->a(Z)V

    .line 190
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->a()Z

    .line 191
    invoke-virtual {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->b()V

    goto :goto_1

    .line 194
    :sswitch_2
    new-instance v1, Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 195
    iget-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 196
    iget-wide v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->f:J

    iget-wide v6, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 197
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v3

    .line 198
    if-ne v2, v3, :cond_1

    .line 199
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 200
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 201
    iget v2, v2, Landroid/text/format/Time;->hour:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 205
    :goto_2
    iput v10, v1, Landroid/text/format/Time;->minute:I

    .line 206
    iput v10, v1, Landroid/text/format/Time;->second:I

    .line 207
    iput-boolean v10, v1, Landroid/text/format/Time;->allDay:Z

    .line 208
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 209
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 210
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    .line 211
    const-wide/16 v2, 0x1

    const-wide/16 v8, 0x0

    move-object v1, p0

    move v11, v10

    move-wide v12, v4

    invoke-virtual/range {v0 .. v13}, Lcom/android/calendar/al;->a(Ljava/lang/Object;JJJJIIJ)V

    goto :goto_1

    .line 203
    :cond_1
    const/16 v2, 0x8

    iput v2, v1, Landroid/text/format/Time;->hour:I

    goto :goto_2

    .line 182
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f120338 -> :sswitch_2
        0x7f120339 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 167
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->c()Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->b()I

    move-result v0

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    .line 170
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    const v3, 0x7f120339

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->e:Landroid/view/Menu;

    const v2, 0x7f120338

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 173
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 168
    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 109
    iget-object v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->a:Lcom/android/calendar/detail/r;

    invoke-virtual {v0}, Lcom/android/calendar/detail/r;->b()V

    .line 110
    iget-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/detail/EasyModeListActivity;->h:Z

    if-nez v0, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/android/calendar/detail/EasyModeListActivity;->c()V

    .line 113
    :cond_0
    return-void
.end method
