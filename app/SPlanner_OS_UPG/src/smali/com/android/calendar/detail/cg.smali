.class Lcom/android/calendar/detail/cg;
.super Landroid/widget/BaseAdapter;
.source "MeetingInvitationStatusView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/cd;


# direct methods
.method private constructor <init>(Lcom/android/calendar/detail/cd;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/detail/cd;Lcom/android/calendar/detail/ce;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lcom/android/calendar/detail/cg;-><init>(Lcom/android/calendar/detail/cd;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/detail/cd;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 218
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/16 v9, 0x8

    const/4 v10, 0x0

    .line 223
    .line 224
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/detail/cd;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/event/aw;

    .line 226
    if-eqz v2, :cond_9

    .line 227
    iget-object v0, v2, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    move-object v4, v0

    .line 230
    :goto_0
    if-eqz p2, :cond_8

    .line 231
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/cf;

    .line 234
    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v2, :cond_7

    iget-object v3, v2, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    if-nez v3, :cond_7

    .line 235
    :cond_0
    new-instance v3, Lcom/android/calendar/detail/cf;

    invoke-direct {v3}, Lcom/android/calendar/detail/cf;-><init>()V

    .line 236
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->b(Lcom/android/calendar/detail/cd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f04006d

    invoke-virtual {v0, v5, p3, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 238
    iput-object p2, v2, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    .line 239
    const v0, 0x7f12021b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->d:Landroid/widget/LinearLayout;

    .line 240
    const v0, 0x7f12021a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->e:Landroid/widget/ImageView;

    .line 241
    const v0, 0x7f12021c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->f:Landroid/widget/ImageView;

    .line 242
    const v0, 0x7f12021d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->g:Landroid/widget/TextView;

    .line 243
    const v0, 0x7f120219

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->a:Landroid/widget/QuickContactBadge;

    .line 244
    const v0, 0x7f120060

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->b:Landroid/widget/TextView;

    .line 245
    const v0, 0x7f12021e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v3, Lcom/android/calendar/detail/cf;->c:Landroid/view/View;

    .line 246
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v8, p2

    .line 249
    :goto_2
    if-nez v4, :cond_1

    move-object v0, v8

    .line 300
    :goto_3
    return-object v0

    .line 254
    :cond_1
    iget-object v5, v3, Lcom/android/calendar/detail/cf;->a:Landroid/widget/QuickContactBadge;

    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->a(Lcom/android/calendar/detail/cd;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/aw;

    iget-object v0, v0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v0}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 255
    iget-object v0, v4, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 258
    :goto_4
    iget-object v5, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v5}, Lcom/android/calendar/detail/cd;->c(Lcom/android/calendar/detail/cd;)Lcom/android/calendar/detail/MeetingInvitationAttendeesView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/calendar/detail/MeetingInvitationAttendeesView;->getFragment()Lcom/android/calendar/detail/bq;

    move-result-object v5

    iget-object v5, v5, Lcom/android/calendar/detail/bq;->t:Ljava/lang/String;

    .line 259
    iget-object v6, v4, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, v4, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 260
    iget-object v5, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v5}, Lcom/android/calendar/detail/cd;->b(Lcom/android/calendar/detail/cd;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f045f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 261
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " ("

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ")"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 264
    :cond_2
    iget-object v5, v3, Lcom/android/calendar/detail/cf;->b:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->d(Lcom/android/calendar/detail/cd;)I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/calendar/detail/cg;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 268
    iget-object v0, v3, Lcom/android/calendar/detail/cf;->c:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_3
    iget-boolean v0, v4, Lcom/android/calendar/at;->g:Z

    if-eqz v0, :cond_6

    .line 275
    iget-object v0, v4, Lcom/android/calendar/at;->e:Ljava/lang/String;

    .line 276
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 277
    iget-object v0, v3, Lcom/android/calendar/detail/cf;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 278
    iget-object v0, v3, Lcom/android/calendar/detail/cf;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 289
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->b(Lcom/android/calendar/detail/cd;)Landroid/content/Context;

    move-result-object v0

    iget-object v3, v3, Lcom/android/calendar/detail/cf;->a:Landroid/widget/QuickContactBadge;

    invoke-static {v0, v3, v4}, Lcom/android/calendar/detail/bq;->a(Landroid/content/Context;Landroid/widget/QuickContactBadge;Lcom/android/calendar/at;)V

    .line 291
    iget-object v0, v2, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    iget-object v9, v0, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 292
    iget-object v0, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v0}, Lcom/android/calendar/detail/cd;->f(Lcom/android/calendar/detail/cd;)Lcom/android/calendar/detail/ch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/ch;->a()Lcom/android/calendar/detail/ci;

    move-result-object v0

    sget-object v3, Lcom/android/calendar/detail/ch;->b:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/detail/ch;->a:[Ljava/lang/String;

    const-string v5, "data1 IN (?)"

    new-array v6, v1, [Ljava/lang/String;

    aput-object v9, v6, v10

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/detail/ci;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 300
    goto/16 :goto_3

    .line 255
    :cond_4
    iget-object v0, v4, Lcom/android/calendar/at;->a:Ljava/lang/String;

    goto/16 :goto_4

    .line 280
    :cond_5
    iget-object v5, v3, Lcom/android/calendar/detail/cf;->e:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 281
    iget-object v5, v3, Lcom/android/calendar/detail/cf;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 282
    iget-object v5, v3, Lcom/android/calendar/detail/cf;->g:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v0, v3, Lcom/android/calendar/detail/cf;->d:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/android/calendar/detail/cg;->a:Lcom/android/calendar/detail/cd;

    invoke-static {v5}, Lcom/android/calendar/detail/cd;->e(Lcom/android/calendar/detail/cd;)Landroid/view/animation/AlphaAnimation;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_5

    .line 286
    :cond_6
    iget-object v0, v3, Lcom/android/calendar/detail/cf;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5

    :cond_7
    move-object v3, v0

    move-object v8, p2

    goto/16 :goto_2

    :cond_8
    move-object v0, v7

    goto/16 :goto_1

    :cond_9
    move-object v4, v7

    goto/16 :goto_0
.end method
