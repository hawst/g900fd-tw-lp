.class Lcom/android/calendar/detail/dh;
.super Lcom/android/calendar/ag;
.source "TaskInfoFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/cu;

.field private b:Lcom/android/calendar/detail/dg;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/detail/cu;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1317
    iput-object p1, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    .line 1318
    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 1315
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/detail/dh;->c:I

    .line 1319
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 1340
    monitor-enter p0

    .line 1341
    :try_start_0
    iget v0, p0, Lcom/android/calendar/detail/dh;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/detail/dh;->c:I

    .line 1342
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1344
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1345
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1346
    :cond_0
    if-eqz p3, :cond_1

    .line 1347
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1377
    :cond_1
    :goto_0
    return-void

    .line 1342
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1352
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 1367
    :cond_3
    if-eqz p3, :cond_4

    .line 1368
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1371
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->b:Lcom/android/calendar/detail/dg;

    if-eqz v0, :cond_1

    .line 1372
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->b:Lcom/android/calendar/detail/dg;

    invoke-interface {v0, p1}, Lcom/android/calendar/detail/dg;->a(I)V

    .line 1373
    invoke-virtual {p0}, Lcom/android/calendar/detail/dh;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 1374
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->b:Lcom/android/calendar/detail/dg;

    invoke-interface {v0}, Lcom/android/calendar/detail/dg;->a()V

    goto :goto_0

    .line 1354
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/cu;->a(Lcom/android/calendar/detail/cu;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1355
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    iget-object v1, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v1}, Lcom/android/calendar/detail/cu;->d(Lcom/android/calendar/detail/cu;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/detail/cu;->a(Lcom/android/calendar/detail/cu;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1356
    if-eqz p3, :cond_5

    .line 1357
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 1359
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    invoke-static {v0}, Lcom/android/calendar/detail/cu;->j(Lcom/android/calendar/detail/cu;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0455

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1360
    iget-object v0, p0, Lcom/android/calendar/detail/dh;->a:Lcom/android/calendar/detail/cu;

    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 1352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1332
    monitor-enter p0

    .line 1333
    :try_start_0
    iget v0, p0, Lcom/android/calendar/detail/dh;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/detail/dh;->c:I

    .line 1334
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1335
    invoke-super/range {p0 .. p7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    return-void

    .line 1334
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(Lcom/android/calendar/detail/dg;)V
    .locals 0

    .prologue
    .line 1322
    iput-object p1, p0, Lcom/android/calendar/detail/dh;->b:Lcom/android/calendar/detail/dg;

    .line 1323
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1326
    iget v0, p0, Lcom/android/calendar/detail/dh;->c:I

    return v0
.end method
