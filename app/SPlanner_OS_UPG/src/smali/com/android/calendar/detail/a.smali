.class public Lcom/android/calendar/detail/a;
.super Landroid/app/DialogFragment;
.source "BaseEventInfoFragment.java"

# interfaces
.implements Lcom/android/calendar/detail/bp;


# static fields
.field public static final b:Landroid/text/util/Linkify$MatchFilter;

.field public static final c:Landroid/text/util/Linkify$MatchFilter;

.field private static final d:Landroid/text/util/Linkify$MatchFilter;

.field private static final e:Lcom/android/calendar/detail/e;


# instance fields
.field protected a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/android/calendar/detail/b;

    invoke-direct {v0}, Lcom/android/calendar/detail/b;-><init>()V

    sput-object v0, Lcom/android/calendar/detail/a;->b:Landroid/text/util/Linkify$MatchFilter;

    .line 56
    new-instance v0, Lcom/android/calendar/detail/c;

    invoke-direct {v0}, Lcom/android/calendar/detail/c;-><init>()V

    sput-object v0, Lcom/android/calendar/detail/a;->c:Landroid/text/util/Linkify$MatchFilter;

    .line 72
    new-instance v0, Lcom/android/calendar/detail/d;

    invoke-direct {v0}, Lcom/android/calendar/detail/d;-><init>()V

    sput-object v0, Lcom/android/calendar/detail/a;->d:Landroid/text/util/Linkify$MatchFilter;

    .line 100
    new-instance v0, Lcom/android/calendar/detail/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/detail/e;-><init>(Lcom/android/calendar/detail/b;)V

    sput-object v0, Lcom/android/calendar/detail/a;->e:Lcom/android/calendar/detail/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/detail/a;->a:Z

    .line 129
    return-void
.end method

.method protected static a(Landroid/widget/TextView;)I
    .locals 4

    .prologue
    .line 138
    invoke-static {p0}, Lcom/android/calendar/detail/a;->c(Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v0

    .line 139
    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v0, v0

    return v0
.end method

.method protected static a(Landroid/widget/TextView;ZI)V
    .locals 13

    .prologue
    .line 173
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v2, v0

    .line 174
    :goto_0
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    move v5, v0

    .line 175
    :goto_1
    and-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 177
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 178
    invoke-static {p0}, Lcom/android/calendar/detail/a;->b(Landroid/widget/TextView;)V

    .line 179
    const/4 v1, 0x0

    .line 181
    if-eqz v0, :cond_18

    .line 182
    if-eqz p1, :cond_6

    .line 183
    sget-object v0, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    const-string v1, "url-action:"

    sget-object v3, Lcom/android/calendar/detail/a;->b:Landroid/text/util/Linkify$MatchFilter;

    const/4 v4, 0x0

    invoke-static {p0, v0, v1, v3, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    .line 187
    :goto_3
    sget-object v1, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    if-eqz p1, :cond_7

    const-string v0, "mailto-action:"

    :goto_4
    sget-object v3, Lcom/android/calendar/detail/a;->c:Landroid/text/util/Linkify$MatchFilter;

    const/4 v4, 0x0

    invoke-static {p0, v1, v0, v3, v4}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    .line 188
    invoke-static {p0}, Lcom/android/calendar/detail/a;->a(Landroid/widget/TextView;)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_5
    move v4, v0

    .line 190
    :goto_6
    if-nez v2, :cond_0

    if-eqz v5, :cond_2

    .line 191
    :cond_0
    const/4 v1, 0x0

    .line 192
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 196
    const-string v2, "\\s+"

    const-string v3, " "

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_1
    sget-object v2, Lcom/android/calendar/detail/a;->o:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 201
    sget-object v2, Lcom/android/calendar/detail/a;->r:Ljava/util/regex/Pattern;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 202
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-nez v2, :cond_17

    .line 203
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_7
    sget-object v1, Lcom/android/calendar/detail/a;->e:Lcom/android/calendar/detail/e;

    invoke-virtual {v1, v0}, Lcom/android/calendar/detail/e;->a(Ljava/lang/String;)V

    .line 207
    sget-object v1, Lcom/android/calendar/detail/a;->p:Ljava/util/regex/Pattern;

    if-eqz p1, :cond_9

    const-string v0, "tel-action:"

    :goto_8
    sget-object v2, Lcom/android/calendar/detail/a;->d:Landroid/text/util/Linkify$MatchFilter;

    sget-object v3, Lcom/android/calendar/detail/a;->e:Lcom/android/calendar/detail/e;

    invoke-static {p0, v1, v0, v2, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;Landroid/text/util/Linkify$MatchFilter;Landroid/text/util/Linkify$TransformFilter;)V

    .line 211
    :cond_2
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 212
    invoke-static {p0}, Lcom/android/calendar/detail/a;->c(Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v7

    .line 213
    const/4 v0, 0x0

    invoke-interface {v7}, Landroid/text/Spannable;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-interface {v7, v0, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 215
    const/4 v2, 0x0

    .line 216
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 217
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 219
    array-length v10, v0

    const/4 v1, 0x0

    move v3, v2

    :goto_9
    if-ge v1, v10, :cond_a

    aget-object v2, v0, v1

    .line 220
    invoke-interface {v7, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    .line 221
    invoke-interface {v7, v2}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 223
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    add-int/lit8 v2, v3, 0x1

    .line 219
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_9

    .line 173
    :cond_3
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    .line 174
    :cond_4
    const/4 v0, 0x0

    move v5, v0

    goto/16 :goto_1

    .line 175
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 185
    :cond_6
    const/16 v0, 0x21

    invoke-static {p0, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    goto/16 :goto_3

    .line 187
    :cond_7
    const-string v0, "mailto:"

    goto/16 :goto_4

    .line 188
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 207
    :cond_9
    const-string v0, "tel-action-no-chooser:"

    goto :goto_8

    .line 229
    :cond_a
    if-eqz v5, :cond_16

    if-lez v3, :cond_16

    .line 234
    const/4 v0, 0x0

    move v2, v0

    :goto_a
    if-ge v2, v3, :cond_12

    .line 235
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 236
    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 237
    if-nez v2, :cond_d

    const/4 v0, 0x1

    if-le v4, v0, :cond_d

    .line 238
    const/4 v0, 0x0

    .line 239
    add-int/lit8 v4, v4, -0x1

    .line 241
    :goto_b
    if-ge v0, v4, :cond_c

    invoke-interface {v6, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    const/16 v11, 0x20

    if-eq v10, v11, :cond_b

    invoke-interface {v6, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    const/16 v11, 0x2c

    if-eq v10, v11, :cond_b

    invoke-interface {v6, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    const/16 v11, 0x3b

    if-eq v10, v11, :cond_b

    invoke-interface {v6, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    const/16 v11, 0x2e

    if-ne v10, v11, :cond_c

    .line 245
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 247
    :cond_c
    if-eqz v5, :cond_d

    if-ge v0, v4, :cond_d

    .line 248
    const/4 v0, 0x0

    invoke-interface {v6, v0, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 249
    new-instance v10, Landroid/text/style/URLSpan;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "geo:0,0?q="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 250
    const/4 v0, 0x0

    const/16 v11, 0x21

    invoke-interface {v7, v10, v0, v4, v11}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 255
    :cond_d
    add-int/lit8 v0, v3, -0x1

    if-ne v2, v0, :cond_f

    .line 256
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 261
    :goto_c
    if-ge v1, v0, :cond_10

    invoke-interface {v6, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v10, 0x20

    if-eq v4, v10, :cond_e

    invoke-interface {v6, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v10, 0x2c

    if-eq v4, v10, :cond_e

    invoke-interface {v6, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v10, 0x3b

    if-eq v4, v10, :cond_e

    invoke-interface {v6, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    const/16 v10, 0x2e

    if-ne v4, v10, :cond_10

    .line 265
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 258
    :cond_f
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_c

    .line 268
    :cond_10
    if-eqz v5, :cond_11

    if-ge v1, v0, :cond_11

    .line 269
    invoke-interface {v6, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    .line 270
    new-instance v10, Landroid/text/style/URLSpan;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "geo:0,0?q="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 271
    const/16 v4, 0x21

    invoke-interface {v7, v10, v1, v0, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 234
    :cond_11
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_a

    .line 275
    :cond_12
    if-eq v7, v6, :cond_13

    .line 276
    invoke-virtual {p0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    :cond_13
    invoke-virtual {p0}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_14

    instance-of v0, v0, Landroid/text/method/LinkMovementMethod;

    if-nez v0, :cond_15

    .line 281
    :cond_14
    invoke-virtual {p0}, Landroid/widget/TextView;->getLinksClickable()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 282
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 291
    :cond_15
    :goto_d
    return-void

    .line 287
    :cond_16
    if-nez v4, :cond_15

    if-eqz v5, :cond_15

    .line 288
    sget-object v0, Lcom/android/calendar/detail/a;->n:Ljava/util/regex/Pattern;

    const-string v1, "geo:0,0?q="

    invoke-static {p0, v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    goto :goto_d

    :cond_17
    move-object v0, v1

    goto/16 :goto_7

    :cond_18
    move v4, v1

    goto/16 :goto_6
.end method

.method protected static b(Landroid/widget/TextView;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-static {p0}, Lcom/android/calendar/detail/a;->c(Landroid/widget/TextView;)Landroid/text/Spannable;

    move-result-object v2

    .line 144
    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 146
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 147
    invoke-interface {v2, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    :cond_0
    return-void
.end method

.method protected static c(Landroid/widget/TextView;)Landroid/text/Spannable;
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 155
    instance-of v1, v0, Landroid/text/SpannableString;

    if-eqz v1, :cond_0

    .line 156
    check-cast v0, Landroid/text/SpannableString;

    .line 161
    :goto_0
    return-object v0

    .line 158
    :cond_0
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 134
    invoke-virtual {p0}, Lcom/android/calendar/detail/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "config_voice_capable"

    invoke-static {v1}, Lcom/android/calendar/common/b/a;->d(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/detail/a;->a:Z

    .line 135
    return-void
.end method
