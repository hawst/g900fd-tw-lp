.class Lcom/android/calendar/detail/s;
.super Ljava/lang/Object;
.source "EasyModeListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/detail/r;


# direct methods
.method constructor <init>(Lcom/android/calendar/detail/r;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 4

    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v0

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 133
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 134
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 135
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 136
    iget-object v3, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v3}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/calendar/detail/o;->a(I)V

    .line 133
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EasyModeListActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/detail/EasyModeListActivity;->a(Z)V

    .line 140
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/detail/EasyModeListActivity;

    invoke-virtual {v0}, Lcom/android/calendar/detail/EasyModeListActivity;->a()Z

    .line 141
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->a(Lcom/android/calendar/detail/r;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 142
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 143
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->c(Lcom/android/calendar/detail/r;)Lcom/android/calendar/detail/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/detail/o;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 120
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/s;->a:Lcom/android/calendar/detail/r;

    invoke-static {v0}, Lcom/android/calendar/detail/r;->d(Lcom/android/calendar/detail/r;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
