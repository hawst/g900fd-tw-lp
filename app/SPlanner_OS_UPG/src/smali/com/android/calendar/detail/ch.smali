.class public Lcom/android/calendar/detail/ch;
.super Ljava/lang/Object;
.source "PresenceQueryHelper.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:Landroid/net/Uri;

.field public static c:Landroid/graphics/drawable/Drawable;

.field private static e:I

.field private static f:I

.field private static g:Landroid/graphics/ColorMatrixColorFilter;


# instance fields
.field private d:Landroid/content/Context;

.field private h:Lcom/android/calendar/detail/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/detail/ch;->a:[Ljava/lang/String;

    .line 45
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/calendar/detail/ch;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    .line 58
    iget-object v0, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/detail/ch;->c:Landroid/graphics/drawable/Drawable;

    .line 61
    const v1, 0x7f0d001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/calendar/detail/ch;->e:I

    .line 62
    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/android/calendar/detail/ch;->f:I

    .line 65
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 66
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 67
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    sput-object v1, Lcom/android/calendar/detail/ch;->g:Landroid/graphics/ColorMatrixColorFilter;

    .line 69
    new-instance v0, Lcom/android/calendar/detail/ci;

    iget-object v1, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/ci;-><init>(Lcom/android/calendar/detail/ch;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/detail/ch;->h:Lcom/android/calendar/detail/ci;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/detail/ch;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Lcom/android/calendar/at;)Lcom/android/calendar/event/aw;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/android/calendar/event/aw;

    sget-object v1, Lcom/android/calendar/detail/ch;->c:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/aw;-><init>(Lcom/android/calendar/at;Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public static a(Lcom/android/calendar/event/aw;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 84
    iget-object v1, p0, Lcom/android/calendar/event/aw;->e:Landroid/view/View;

    .line 85
    iget-object v3, p0, Lcom/android/calendar/event/aw;->b:Lcom/android/calendar/at;

    .line 86
    const v0, 0x7f120219

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/QuickContactBadge;

    .line 87
    const v2, 0x7f120060

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 89
    if-eqz v1, :cond_0

    .line 90
    iget-object v2, v3, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v3, Lcom/android/calendar/at;->b:Ljava/lang/String;

    .line 91
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_0
    iget v1, v3, Lcom/android/calendar/at;->c:I

    if-ne v1, v4, :cond_2

    .line 95
    iget-object v1, p0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    sget v2, Lcom/android/calendar/detail/ch;->e:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 99
    :goto_1
    iget v1, v3, Lcom/android/calendar/at;->c:I

    if-ne v1, v4, :cond_3

    .line 100
    iget-object v1, p0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    sget-object v2, Lcom/android/calendar/detail/ch;->g:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 105
    :goto_2
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 106
    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 107
    iget-object v2, p0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/QuickContactBadge;->setVisibility(I)V

    .line 109
    invoke-virtual {v0, v1}, Landroid/widget/QuickContactBadge;->startAnimation(Landroid/view/animation/Animation;)V

    .line 110
    return-void

    .line 90
    :cond_1
    iget-object v2, v3, Lcom/android/calendar/at;->a:Ljava/lang/String;

    goto :goto_0

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    sget v2, Lcom/android/calendar/detail/ch;->f:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1

    .line 102
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/aw;->c:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_2
.end method


# virtual methods
.method public a()Lcom/android/calendar/detail/ci;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/calendar/detail/ch;->h:Lcom/android/calendar/detail/ci;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/android/calendar/detail/ci;

    iget-object v1, p0, Lcom/android/calendar/detail/ch;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/detail/ci;-><init>(Lcom/android/calendar/detail/ch;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/detail/ch;->h:Lcom/android/calendar/detail/ci;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/detail/ch;->h:Lcom/android/calendar/detail/ci;

    return-object v0
.end method
