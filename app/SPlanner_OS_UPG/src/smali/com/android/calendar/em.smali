.class public Lcom/android/calendar/em;
.super Ljava/lang/Object;
.source "LimitContract.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/android/calendar/em;->a:Landroid/content/Context;

    .line 142
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/android/calendar/em;->b:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/android/calendar/em;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/android/calendar/em;->a(Landroid/content/Context;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/em;->b:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/android/calendar/em;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 151
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 155
    const v1, 0x7f0f0293

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/16 v2, 0x2000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/em;->b:Landroid/widget/Toast;

    .line 157
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 161
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v0

    sub-int v1, p6, p5

    sub-int/2addr v0, v1

    rsub-int v0, v0, 0x2000

    .line 163
    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/android/calendar/em;->a()V

    .line 166
    :cond_0
    if-gtz v0, :cond_1

    .line 167
    const-string v0, ""

    .line 174
    :goto_0
    return-object v0

    .line 168
    :cond_1
    sub-int v1, p3, p2

    if-lt v0, v1, :cond_2

    .line 169
    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 172
    invoke-direct {p0}, Lcom/android/calendar/em;->a()V

    .line 174
    :cond_3
    add-int/2addr v0, p2

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/e/an;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
