.class public Lcom/android/calendar/colorpicker/ColorPickerPalette;
.super Landroid/widget/TableLayout;
.source "ColorPickerPalette.java"


# instance fields
.field public a:Lcom/android/calendar/colorpicker/j;

.field public b:[I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    .line 43
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->b:[I

    .line 58
    return-void

    .line 43
    nop

    :array_0
    .array-data 4
        0x7f0f01c5
        0x7f0f01c4
        0x7f0f01cc
        0x7f0f01ca
        0x7f0f01c2
        0x7f0f01cb
        0x7f0f01cd
        0x7f0f01c7
        0x7f0f01c6
        0x7f0f01c3
        0x7f0f01c9
        0x7f0f01c8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->b:[I

    .line 54
    return-void

    .line 43
    nop

    :array_0
    .array-data 4
        0x7f0f01c5
        0x7f0f01c4
        0x7f0f01cc
        0x7f0f01ca
        0x7f0f01c2
        0x7f0f01cb
        0x7f0f01cd
        0x7f0f01c7
        0x7f0f01c6
        0x7f0f01c3
        0x7f0f01c9
        0x7f0f01c8
    .end array-data
.end method

.method private a()Landroid/widget/TableRow;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 75
    new-instance v0, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 76
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 78
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    return-object v0
.end method

.method private a(IIZ)Lcom/android/calendar/colorpicker/g;
    .locals 6

    .prologue
    .line 148
    new-instance v0, Lcom/android/calendar/colorpicker/g;

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getContext()Landroid/content/Context;

    move-result-object v1

    if-ne p1, p2, :cond_0

    const/4 v3, 0x1

    :goto_0
    iget-object v5, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a:Lcom/android/calendar/colorpicker/j;

    move v2, p1

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/colorpicker/g;-><init>(Landroid/content/Context;IZZLcom/android/calendar/colorpicker/j;)V

    .line 150
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    iget v2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->c:I

    iget v3, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->c:I

    invoke-direct {v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 151
    iget v2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v3, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v4, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v5, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 152
    invoke-virtual {v0, v1}, Lcom/android/calendar/colorpicker/g;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    return-object v0

    .line 148
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private b()Landroid/widget/ImageView;
    .locals 6

    .prologue
    .line 137
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 138
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    iget v2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->c:I

    iget v3, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->c:I

    invoke-direct {v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 139
    iget v2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v3, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v4, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    iget v5, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 140
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    return-object v0
.end method


# virtual methods
.method public a(IILcom/android/calendar/colorpicker/j;)V
    .locals 2

    .prologue
    .line 65
    iput p2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->e:I

    .line 66
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 68
    const v1, 0x7f0c009a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->c:I

    .line 69
    const v1, 0x7f0c009b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->d:I

    .line 71
    iput-object p3, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a:Lcom/android/calendar/colorpicker/j;

    .line 72
    return-void
.end method

.method public a([III)V
    .locals 12

    .prologue
    .line 86
    if-nez p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->removeAllViews()V

    .line 93
    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a()Landroid/widget/TableRow;

    move-result-object v1

    .line 98
    const/4 v0, 0x1

    invoke-direct {p0, p3, p2, v0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a(IIZ)Lcom/android/calendar/colorpicker/g;

    move-result-object v0

    .line 99
    const/4 v4, 0x1

    .line 100
    const v3, 0x7f12009a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->b:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 104
    const/4 v3, 0x1

    .line 106
    array-length v5, p1

    const/4 v0, 0x0

    move v11, v0

    move-object v0, v1

    move v1, v2

    move v2, v3

    move v3, v11

    :goto_1
    if-ge v3, v5, :cond_3

    aget v6, p1, v3

    .line 107
    add-int/lit8 v4, v4, 0x1

    .line 109
    const/4 v7, 0x0

    invoke-direct {p0, v6, p2, v7}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a(IIZ)Lcom/android/calendar/colorpicker/g;

    move-result-object v6

    .line 110
    const v7, 0x7f12009a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v9, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->b:[I

    add-int/lit8 v10, v4, -0x1

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {v0, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 114
    add-int/lit8 v2, v2, 0x1

    .line 115
    iget v6, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->e:I

    if-ne v2, v6, :cond_2

    .line 116
    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->addView(Landroid/view/View;)V

    .line 117
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a()Landroid/widget/TableRow;

    move-result-object v0

    .line 118
    const/4 v2, 0x0

    .line 119
    add-int/lit8 v1, v1, 0x1

    .line 106
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 124
    :cond_3
    if-lez v2, :cond_0

    move v1, v2

    .line 125
    :goto_2
    iget v2, p0, Lcom/android/calendar/colorpicker/ColorPickerPalette;->e:I

    if-eq v1, v2, :cond_4

    .line 126
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->b()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 129
    :cond_4
    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
