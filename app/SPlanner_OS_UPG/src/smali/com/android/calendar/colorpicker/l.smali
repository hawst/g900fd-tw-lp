.class public Lcom/android/calendar/colorpicker/l;
.super Ljava/lang/Object;
.source "HsvColorComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;Ljava/lang/Integer;)I
    .locals 10

    .prologue
    const/4 v6, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 30
    new-array v3, v6, [F

    .line 31
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4, v3}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 32
    aget v4, v3, v2

    .line 33
    aget v5, v3, v0

    .line 34
    aget v3, v3, v9

    .line 36
    new-array v6, v6, [F

    .line 37
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7, v6}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 38
    aget v7, v6, v2

    .line 39
    aget v8, v6, v0

    .line 40
    aget v6, v6, v9

    .line 42
    cmpg-float v9, v4, v7

    if-gez v9, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    cmpl-float v4, v4, v7

    if-lez v4, :cond_2

    move v0, v1

    .line 45
    goto :goto_0

    .line 47
    :cond_2
    cmpg-float v4, v5, v8

    if-ltz v4, :cond_0

    .line 49
    cmpl-float v4, v5, v8

    if-lez v4, :cond_3

    move v0, v1

    .line 50
    goto :goto_0

    .line 52
    :cond_3
    cmpg-float v4, v3, v6

    if-ltz v4, :cond_0

    .line 54
    cmpl-float v0, v3, v6

    if-lez v0, :cond_4

    move v0, v1

    .line 55
    goto :goto_0

    :cond_4
    move v0, v2

    .line 59
    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/colorpicker/l;->a(Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
