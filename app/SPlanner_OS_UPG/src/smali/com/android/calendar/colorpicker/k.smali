.class public Lcom/android/calendar/colorpicker/k;
.super Landroid/graphics/drawable/LayerDrawable;
.source "ColorStateDrawable.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 35
    iput p2, p0, Lcom/android/calendar/colorpicker/k;->a:I

    .line 36
    return-void
.end method

.method private static a(I)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 62
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 63
    aget v1, v0, v3

    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 64
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method


# virtual methods
.method public isStateful()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method protected onStateChange([I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 40
    .line 41
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, p1, v1

    .line 42
    const v4, 0x10100a7

    if-eq v3, v4, :cond_0

    const v4, 0x101009c

    if-ne v3, v4, :cond_2

    .line 43
    :cond_0
    const/4 v0, 0x1

    .line 48
    :cond_1
    if-eqz v0, :cond_3

    .line 49
    iget v0, p0, Lcom/android/calendar/colorpicker/k;->a:I

    invoke-static {v0}, Lcom/android/calendar/colorpicker/k;->a(I)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-super {p0, v0, v1}, Landroid/graphics/drawable/LayerDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 54
    :goto_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/LayerDrawable;->onStateChange([I)Z

    move-result v0

    return v0

    .line 41
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_3
    iget v0, p0, Lcom/android/calendar/colorpicker/k;->a:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-super {p0, v0, v1}, Landroid/graphics/drawable/LayerDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1
.end method
