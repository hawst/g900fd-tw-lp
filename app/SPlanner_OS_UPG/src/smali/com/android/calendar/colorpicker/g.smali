.class public Lcom/android/calendar/colorpicker/g;
.super Landroid/widget/FrameLayout;
.source "ColorPickerSwatch.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;

.field private d:Lcom/android/calendar/colorpicker/j;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZZLcom/android/calendar/colorpicker/j;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 55
    iput p2, p0, Lcom/android/calendar/colorpicker/g;->a:I

    .line 56
    iput-object p5, p0, Lcom/android/calendar/colorpicker/g;->d:Lcom/android/calendar/colorpicker/j;

    .line 57
    iput-boolean p4, p0, Lcom/android/calendar/colorpicker/g;->e:Z

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040027

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 60
    const v0, 0x7f12009a

    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/colorpicker/g;->b:Landroid/widget/ImageView;

    .line 61
    const v0, 0x7f12009b

    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/colorpicker/g;->c:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->b:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/calendar/colorpicker/h;

    invoke-direct {v1, p0}, Lcom/android/calendar/colorpicker/h;-><init>(Lcom/android/calendar/colorpicker/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 70
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->b:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/calendar/colorpicker/i;

    invoke-direct {v1, p0}, Lcom/android/calendar/colorpicker/i;-><init>(Lcom/android/calendar/colorpicker/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 83
    invoke-virtual {p0, p2}, Lcom/android/calendar/colorpicker/g;->setColor(I)V

    .line 84
    invoke-direct {p0, p3}, Lcom/android/calendar/colorpicker/g;->setChecked(Z)V

    .line 85
    invoke-virtual {p0, p0}, Lcom/android/calendar/colorpicker/g;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/colorpicker/g;)Lcom/android/calendar/colorpicker/j;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->d:Lcom/android/calendar/colorpicker/j;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/colorpicker/g;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/android/calendar/colorpicker/g;->setFocusStroke(Z)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/colorpicker/g;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/android/calendar/colorpicker/g;->a:I

    return v0
.end method

.method private setChecked(Z)V
    .locals 2

    .prologue
    .line 109
    if-eqz p1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setFocusStroke(Z)V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 91
    if-eqz p1, :cond_1

    .line 92
    const/4 v1, 0x5

    const v2, -0xffff01

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 97
    :cond_0
    iget v1, p0, Lcom/android/calendar/colorpicker/g;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 98
    iget-object v1, p0, Lcom/android/calendar/colorpicker/g;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/colorpicker/g;->e:Z

    if-eqz v1, :cond_0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->d:Lcom/android/calendar/colorpicker/j;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/android/calendar/colorpicker/g;->d:Lcom/android/calendar/colorpicker/j;

    iget v1, p0, Lcom/android/calendar/colorpicker/g;->a:I

    invoke-interface {v0, v1}, Lcom/android/calendar/colorpicker/j;->a(I)V

    .line 121
    :cond_0
    return-void
.end method

.method protected setColor(I)V
    .locals 4

    .prologue
    .line 102
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/g;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    .line 105
    iget-object v1, p0, Lcom/android/calendar/colorpicker/g;->b:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/calendar/colorpicker/k;

    invoke-direct {v2, v0, p1}, Lcom/android/calendar/colorpicker/k;-><init>([Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 106
    return-void
.end method
