.class public Lcom/android/calendar/colorpicker/f;
.super Landroid/app/DialogFragment;
.source "ColorPickerDialog.java"

# interfaces
.implements Lcom/android/calendar/colorpicker/j;


# instance fields
.field protected a:Landroid/app/AlertDialog;

.field protected b:I

.field protected c:[I

.field protected d:I

.field protected e:I

.field protected f:I

.field protected g:I

.field protected h:Lcom/android/calendar/colorpicker/j;

.field private i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

.field private j:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 47
    const v0, 0x7f0f00d5

    iput v0, p0, Lcom/android/calendar/colorpicker/f;->b:I

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    .line 61
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    iget-object v1, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    iget v2, p0, Lcom/android/calendar/colorpicker/f;->d:I

    iget v3, p0, Lcom/android/calendar/colorpicker/f;->e:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a([III)V

    .line 187
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->h:Lcom/android/calendar/colorpicker/j;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->h:Lcom/android/calendar/colorpicker/j;

    invoke-interface {v0, p1}, Lcom/android/calendar/colorpicker/j;->a(I)V

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/android/calendar/colorpicker/j;

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/colorpicker/j;

    .line 133
    invoke-interface {v0, p1}, Lcom/android/calendar/colorpicker/j;->a(I)V

    .line 136
    :cond_1
    iget v0, p0, Lcom/android/calendar/colorpicker/f;->d:I

    if-eq p1, v0, :cond_2

    .line 137
    iput p1, p0, Lcom/android/calendar/colorpicker/f;->d:I

    .line 139
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    iget-object v1, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    iget v2, p0, Lcom/android/calendar/colorpicker/f;->d:I

    iget v3, p0, Lcom/android/calendar/colorpicker/f;->e:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a([III)V

    .line 142
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->dismiss()V

    .line 143
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 77
    const-string v1, "title_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    const-string v1, "columns"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/f;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-void
.end method

.method public a(I[IIII)V
    .locals 0

    .prologue
    .line 71
    invoke-virtual {p0, p1, p5}, Lcom/android/calendar/colorpicker/f;->a(II)V

    .line 72
    invoke-virtual {p0, p2, p3, p4}, Lcom/android/calendar/colorpicker/f;->a([III)V

    .line 73
    return-void
.end method

.method public a(Lcom/android/calendar/colorpicker/j;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/android/calendar/colorpicker/f;->h:Lcom/android/calendar/colorpicker/j;

    .line 84
    return-void
.end method

.method public a([III)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/android/calendar/colorpicker/f;->d:I

    if-eq v0, p2, :cond_1

    .line 162
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    .line 163
    iput p2, p0, Lcom/android/calendar/colorpicker/f;->d:I

    .line 164
    iput p3, p0, Lcom/android/calendar/colorpicker/f;->e:I

    .line 165
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/f;->a()V

    .line 167
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->j:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->j:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 148
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/f;->a()V

    .line 149
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->setVisibility(I)V

    .line 151
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/colorpicker/f;->b:I

    .line 92
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "columns"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/colorpicker/f;->f:I

    .line 93
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "size"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/colorpicker/f;->g:I

    .line 96
    :cond_0
    if-eqz p1, :cond_1

    .line 97
    const-string v0, "colors"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    .line 98
    const-string v0, "selected_color"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/colorpicker/f;->d:I

    .line 100
    :cond_1
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 104
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 106
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040025

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 107
    const v0, 0x102000d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/calendar/colorpicker/f;->j:Landroid/widget/ProgressBar;

    .line 108
    const v0, 0x7f120028

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/colorpicker/ColorPickerPalette;

    iput-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    .line 109
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->i:Lcom/android/calendar/colorpicker/ColorPickerPalette;

    iget v3, p0, Lcom/android/calendar/colorpicker/f;->g:I

    iget v4, p0, Lcom/android/calendar/colorpicker/f;->f:I

    invoke-virtual {v0, v3, v4, p0}, Lcom/android/calendar/colorpicker/ColorPickerPalette;->a(IILcom/android/calendar/colorpicker/j;)V

    .line 111
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/android/calendar/colorpicker/f;->b()V

    .line 115
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/android/calendar/colorpicker/f;->b:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/colorpicker/f;->a:Landroid/app/AlertDialog;

    .line 121
    iget-object v0, p0, Lcom/android/calendar/colorpicker/f;->a:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 200
    const-string v0, "colors"

    iget-object v1, p0, Lcom/android/calendar/colorpicker/f;->c:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 201
    const-string v0, "selected_color"

    iget v1, p0, Lcom/android/calendar/colorpicker/f;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 202
    return-void
.end method
