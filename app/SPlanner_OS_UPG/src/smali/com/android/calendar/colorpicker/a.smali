.class public Lcom/android/calendar/colorpicker/a;
.super Lcom/android/calendar/colorpicker/f;
.source "CalendarColorPickerDialog.java"


# static fields
.field private static i:Z

.field private static l:Lcom/android/calendar/colorpicker/d;


# instance fields
.field private j:Landroid/util/SparseIntArray;

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/colorpicker/a;->i:Z

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/colorpicker/a;->l:Lcom/android/calendar/colorpicker/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/colorpicker/f;-><init>()V

    .line 34
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/colorpicker/a;->j:Landroid/util/SparseIntArray;

    .line 56
    return-void
.end method

.method public static a(JLcom/android/calendar/colorpicker/d;Z[III)Lcom/android/calendar/colorpicker/a;
    .locals 6

    .prologue
    .line 60
    sput-object p2, Lcom/android/calendar/colorpicker/a;->l:Lcom/android/calendar/colorpicker/d;

    .line 61
    sput-boolean p3, Lcom/android/calendar/colorpicker/a;->i:Z

    .line 63
    new-instance v0, Lcom/android/calendar/colorpicker/a;

    invoke-direct {v0}, Lcom/android/calendar/colorpicker/a;-><init>()V

    .line 64
    sget-boolean v1, Lcom/android/calendar/colorpicker/a;->i:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0f00d5

    :goto_0
    const/4 v5, 0x4

    move-object v2, p4

    move v3, p5

    move v4, p6

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/colorpicker/a;->a(I[IIII)V

    .line 67
    invoke-virtual {v0, p0, p1}, Lcom/android/calendar/colorpicker/a;->a(J)V

    .line 68
    return-object v0

    .line 64
    :cond_0
    const v1, 0x7f0f009a

    goto :goto_0
.end method

.method static synthetic a()Lcom/android/calendar/colorpicker/d;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/android/calendar/colorpicker/a;->l:Lcom/android/calendar/colorpicker/d;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    array-length v0, v0

    new-array v1, v0, [I

    .line 84
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 85
    iget-object v2, p0, Lcom/android/calendar/colorpicker/a;->j:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    aput v2, v1, v0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 87
    :cond_1
    const-string v0, "color_keys"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 101
    const-string v0, "color_keys"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    .line 102
    iget-object v0, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 103
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/android/calendar/colorpicker/a;->j:Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/android/calendar/colorpicker/a;->c:[I

    aget v3, v3, v0

    aget v4, v1, v0

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_0
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 3

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/android/calendar/colorpicker/a;->k:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 111
    iput-wide p1, p0, Lcom/android/calendar/colorpicker/a;->k:J

    .line 113
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/android/calendar/colorpicker/f;->onCreate(Landroid/os/Bundle;)V

    .line 93
    if-eqz p1, :cond_0

    .line 94
    const-string v0, "calendar_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/colorpicker/a;->k:J

    .line 95
    invoke-direct {p0, p1}, Lcom/android/calendar/colorpicker/a;->b(Landroid/os/Bundle;)V

    .line 97
    :cond_0
    new-instance v0, Lcom/android/calendar/colorpicker/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/colorpicker/c;-><init>(Lcom/android/calendar/colorpicker/a;Lcom/android/calendar/colorpicker/b;)V

    invoke-virtual {p0, v0}, Lcom/android/calendar/colorpicker/a;->a(Lcom/android/calendar/colorpicker/j;)V

    .line 98
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/android/calendar/colorpicker/f;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 74
    const-string v0, "calendar_id"

    iget-wide v2, p0, Lcom/android/calendar/colorpicker/a;->k:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 75
    invoke-direct {p0, p1}, Lcom/android/calendar/colorpicker/a;->a(Landroid/os/Bundle;)V

    .line 76
    return-void
.end method
