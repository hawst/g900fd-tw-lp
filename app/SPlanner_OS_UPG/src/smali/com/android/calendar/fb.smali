.class Lcom/android/calendar/fb;
.super Ljava/lang/Object;
.source "MiniCalendarFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/android/calendar/ez;


# direct methods
.method constructor <init>(Lcom/android/calendar/ez;I)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/android/calendar/fb;->b:Lcom/android/calendar/ez;

    iput p2, p0, Lcom/android/calendar/fb;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/format/Time;

    .line 161
    iget v1, p0, Lcom/android/calendar/fb;->a:I

    const v2, 0x7f120229

    if-ne v1, v2, :cond_1

    .line 162
    iget v1, v0, Landroid/text/format/Time;->year:I

    const/16 v2, 0x76e

    if-le v1, v2, :cond_0

    .line 163
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    iget v2, v0, Landroid/text/format/Time;->month:I

    iget v3, v0, Landroid/text/format/Time;->year:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v0, Landroid/text/format/Time;->year:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    .line 175
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/fb;->b:Lcom/android/calendar/ez;

    invoke-virtual {v1, v0}, Lcom/android/calendar/ez;->a(Landroid/text/format/Time;)V

    .line 176
    return-void

    .line 166
    :cond_1
    iget v1, v0, Landroid/text/format/Time;->year:I

    const/16 v2, 0x7f4

    if-ge v1, v2, :cond_0

    .line 167
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    iget v2, v0, Landroid/text/format/Time;->month:I

    iget v3, v0, Landroid/text/format/Time;->year:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/text/format/Time;->year:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    goto :goto_0
.end method
