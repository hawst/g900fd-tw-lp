.class Lcom/android/calendar/cx;
.super Ljava/lang/Object;
.source "DeleteTaskDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/cv;


# direct methods
.method constructor <init>(Lcom/android/calendar/cv;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v0}, Lcom/android/calendar/cv;->a(Lcom/android/calendar/cv;)J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 107
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v1}, Lcom/android/calendar/cv;->a(Lcom/android/calendar/cv;)J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 108
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v0}, Lcom/android/calendar/cv;->b(Lcom/android/calendar/cv;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    const/4 v1, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "GATE"

    const-string v1, "<GATE-M>TASK_DELETED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v0}, Lcom/android/calendar/cv;->c(Lcom/android/calendar/cv;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v0}, Lcom/android/calendar/cv;->c(Lcom/android/calendar/cv;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-virtual {v0}, Lcom/android/calendar/cv;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 118
    const-string v1, "TaskInfoFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 119
    check-cast v0, Lcom/android/calendar/detail/cu;

    .line 120
    if-eqz v0, :cond_2

    .line 121
    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    invoke-virtual {v0}, Lcom/android/calendar/detail/cu;->dismiss()V

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-static {v0}, Lcom/android/calendar/cv;->d(Lcom/android/calendar/cv;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-virtual {v0}, Lcom/android/calendar/cv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/cx;->a:Lcom/android/calendar/cv;

    invoke-virtual {v0}, Lcom/android/calendar/cv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/alerts/TaskAlertService;->a(Landroid/content/Context;)Z

    .line 130
    return-void
.end method
