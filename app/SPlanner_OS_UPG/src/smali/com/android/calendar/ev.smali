.class Lcom/android/calendar/ev;
.super Lcom/android/calendar/ew;
.source "LinkActionChooserFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/eo;


# direct methods
.method constructor <init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/android/calendar/ev;->a:Lcom/android/calendar/eo;

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/ew;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 221
    :try_start_0
    const-string v0, "mailto"

    iget-object v1, p0, Lcom/android/calendar/ev;->a:Lcom/android/calendar/eo;

    invoke-static {v1}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 222
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 223
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_0
    return-void

    .line 224
    :catch_0
    move-exception v0

    .line 225
    invoke-static {}, Lcom/android/calendar/eo;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkActionChooserFragment: Email activity not found."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
