.class Lcom/android/calendar/co;
.super Landroid/app/DialogFragment;
.source "DeleteEventHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 327
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/android/calendar/co;
    .locals 1

    .prologue
    .line 329
    new-instance v0, Lcom/android/calendar/co;

    invoke-direct {v0}, Lcom/android/calendar/co;-><init>()V

    .line 330
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 335
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/co;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 336
    const v1, 0x7f0f02d2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 337
    const v1, 0x7f0f01c1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 338
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/cp;

    invoke-direct {v2, p0}, Lcom/android/calendar/cp;-><init>(Lcom/android/calendar/co;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 343
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
