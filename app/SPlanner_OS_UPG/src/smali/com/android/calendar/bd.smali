.class Lcom/android/calendar/bd;
.super Ljava/lang/Object;
.source "CalendarSearchView.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/CalendarSearchView;


# direct methods
.method constructor <init>(Lcom/android/calendar/CalendarSearchView;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/android/calendar/bd;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/android/calendar/bd;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->d(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/android/calendar/bd;->a:Lcom/android/calendar/CalendarSearchView;

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->b(Lcom/android/calendar/CalendarSearchView;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 379
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/android/calendar/CalendarSearchView;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/android/calendar/bd;->a:Lcom/android/calendar/CalendarSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/CalendarSearchView;->setImeVisibility(Z)V

    .line 383
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
