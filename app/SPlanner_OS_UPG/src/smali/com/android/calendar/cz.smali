.class public Lcom/android/calendar/cz;
.super Ljava/lang/Object;
.source "DeleteTaskHelper.java"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Lcom/android/calendar/hh;

.field private c:Z

.field private d:Ljava/lang/Runnable;

.field private e:Ljava/lang/Runnable;

.field private f:Lcom/android/calendar/ag;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    if-nez p1, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/cz;->a:Landroid/app/Activity;

    .line 74
    iput-boolean p2, p0, Lcom/android/calendar/cz;->c:Z

    .line 76
    new-instance v0, Lcom/android/calendar/da;

    iget-object v1, p0, Lcom/android/calendar/cz;->a:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/da;-><init>(Lcom/android/calendar/cz;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/cz;->f:Lcom/android/calendar/ag;

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/cz;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/calendar/cz;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 115
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 116
    iget-object v0, p0, Lcom/android/calendar/cz;->f:Lcom/android/calendar/ag;

    iget-object v1, p0, Lcom/android/calendar/cz;->f:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-void
.end method

.method public a(JLjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 121
    iput-object p3, p0, Lcom/android/calendar/cz;->d:Ljava/lang/Runnable;

    .line 122
    invoke-virtual {p0, p1, p2}, Lcom/android/calendar/cz;->a(J)V

    .line 123
    return-void
.end method

.method public a(Lcom/android/calendar/hh;)V
    .locals 7

    .prologue
    .line 165
    iput-object p1, p0, Lcom/android/calendar/cz;->b:Lcom/android/calendar/hh;

    .line 166
    new-instance v1, Lcom/android/calendar/cv;

    iget-object v0, p0, Lcom/android/calendar/cz;->b:Lcom/android/calendar/hh;

    iget-wide v2, v0, Lcom/android/calendar/hh;->b:J

    iget-object v4, p0, Lcom/android/calendar/cz;->d:Ljava/lang/Runnable;

    iget-object v5, p0, Lcom/android/calendar/cz;->e:Ljava/lang/Runnable;

    iget-boolean v6, p0, Lcom/android/calendar/cz;->c:Z

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/cv;-><init>(JLjava/lang/Runnable;Ljava/lang/Runnable;Z)V

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/cz;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "DeleteTask"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v0

    goto :goto_0
.end method
