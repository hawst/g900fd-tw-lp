.class public Lcom/android/calendar/dp;
.super Ljava/lang/Object;
.source "EventRecurrenceFormatter.java"


# direct methods
.method private static a(I)I
    .locals 3

    .prologue
    .line 187
    sparse-switch p0, :sswitch_data_0

    .line 195
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad day argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :sswitch_0
    const/4 v0, 0x1

    .line 194
    :goto_0
    return v0

    .line 189
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 190
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 191
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 192
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 193
    :sswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 194
    :sswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 187
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x80000 -> :sswitch_3
        0x100000 -> :sswitch_4
        0x200000 -> :sswitch_5
        0x400000 -> :sswitch_6
    .end sparse-switch
.end method

.method private static a(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    invoke-static {p0}, Lcom/android/calendar/dp;->a(I)I

    move-result v0

    invoke-static {v0, p1}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/android/calendar/dp;->a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/android/a/c;ZLjava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 39
    const-string v0, ""

    .line 40
    const-string v1, ""

    .line 41
    if-eqz p3, :cond_2

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    iget-object v0, p2, Lcom/android/a/c;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 45
    :try_start_0
    new-instance v2, Landroid/text/format/Time;

    if-nez p4, :cond_3

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 46
    iget-object v0, p2, Lcom/android/a/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 47
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const/4 v0, 0x2

    invoke-static {v2, v3, p0, v0, p4}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v2, "; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    const v2, 0x7f0f01bd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_1
    iget v0, p2, Lcom/android/a/c;->d:I

    if-lez v0, :cond_1

    .line 55
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    iget v0, p2, Lcom/android/a/c;->d:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 57
    const v0, 0x7f0f037d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    :cond_1
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    :cond_2
    iget v1, p2, Lcom/android/a/c;->e:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_5

    const/4 v1, 0x1

    .line 67
    :goto_3
    iget v2, p2, Lcom/android/a/c;->b:I

    packed-switch v2, :pswitch_data_0

    .line 169
    const/4 v0, 0x0

    :goto_4
    return-object v0

    :cond_3
    move-object v0, p4

    .line 45
    goto :goto_0

    .line 59
    :cond_4
    const v0, 0x7f0f037c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p2, Lcom/android/a/c;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 66
    :cond_5
    iget v1, p2, Lcom/android/a/c;->e:I

    goto :goto_3

    .line 69
    :pswitch_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0f0122

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 72
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0f036c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 77
    :pswitch_1
    invoke-virtual {p2}, Lcom/android/a/c;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0f01dd

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 82
    :cond_7
    const/16 v2, 0x14

    .line 83
    iget v3, p2, Lcom/android/a/c;->o:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    .line 84
    const/16 v2, 0xa

    .line 86
    :cond_8
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 87
    const/4 v2, 0x1

    .line 90
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget v3, p2, Lcom/android/a/c;->o:I

    if-lez v3, :cond_b

    .line 96
    iget v3, p2, Lcom/android/a/c;->o:I

    add-int/lit8 v5, v3, -0x1

    .line 97
    const/4 v3, 0x0

    :goto_5
    if-ge v3, v5, :cond_a

    .line 98
    iget-object v6, p2, Lcom/android/a/c;->m:[I

    aget v6, v6, v3

    invoke-static {v6, v2}, Lcom/android/calendar/dp;->a(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    const-string v6, ", "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 101
    :cond_a
    iget-object v3, p2, Lcom/android/a/c;->m:[I

    aget v3, v3, v5

    invoke-static {v3, v2}, Lcom/android/calendar/dp;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 117
    :goto_6
    const/4 v3, 0x1

    if-ne v1, v3, :cond_d

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0f0478

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 109
    :cond_b
    iget-object v2, p2, Lcom/android/a/c;->a:Landroid/text/format/Time;

    if-nez v2, :cond_c

    .line 110
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 113
    :cond_c
    iget-object v2, p2, Lcom/android/a/c;->a:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->weekDay:I

    invoke-static {v2}, Lcom/android/a/c;->b(I)I

    move-result v2

    .line 114
    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/android/calendar/dp;->a(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 120
    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0f01dc

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v2, v5, v1

    invoke-virtual {p1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 126
    :pswitch_2
    const/4 v2, 0x1

    if-ne v1, v2, :cond_e

    .line 127
    const v1, 0x7f0f02a9

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    :goto_7
    iget v2, p2, Lcom/android/a/c;->o:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    .line 133
    iget-object v2, p2, Lcom/android/a/c;->a:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->weekDay:I

    .line 135
    iget-object v2, p2, Lcom/android/a/c;->a:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v4, v2, 0x7

    .line 136
    const v2, 0x7f09002a

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 137
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    .line 138
    :goto_8
    const/4 v6, 0x7

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/4 v8, 0x3

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const/4 v8, 0x4

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const/4 v8, 0x5

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const/4 v8, 0x6

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const/4 v8, 0x7

    invoke-static {v8, v2}, Lcom/android/calendar/gm;->a(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string v1, " ("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    aget-object v1, v5, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aget-object v3, v6, v3

    aput-object v3, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    const/16 v1, 0x29

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 129
    :cond_e
    const v2, 0x7f0f036e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_7

    .line 137
    :cond_f
    const/4 v2, 0x3

    goto :goto_8

    .line 156
    :cond_10
    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 161
    :pswitch_3
    const/4 v2, 0x1

    if-ne v1, v2, :cond_11

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0f048b

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 164
    :cond_11
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0f0372

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 50
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 67
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
