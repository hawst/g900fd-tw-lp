.class public Lcom/android/calendar/cv;
.super Landroid/app/DialogFragment;
.source "DeleteTaskDialogFragment.java"


# static fields
.field private static a:Landroid/app/AlertDialog;


# instance fields
.field private b:Landroid/content/AsyncQueryHandler;

.field private c:J

.field private d:Ljava/lang/Runnable;

.field private e:Ljava/lang/Runnable;

.field private f:Z

.field private g:Landroid/content/DialogInterface$OnClickListener;

.field private h:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 103
    new-instance v0, Lcom/android/calendar/cx;

    invoke-direct {v0, p0}, Lcom/android/calendar/cx;-><init>(Lcom/android/calendar/cv;)V

    iput-object v0, p0, Lcom/android/calendar/cv;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 133
    new-instance v0, Lcom/android/calendar/cy;

    invoke-direct {v0, p0}, Lcom/android/calendar/cy;-><init>(Lcom/android/calendar/cv;)V

    iput-object v0, p0, Lcom/android/calendar/cv;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/cv;->setRetainInstance(Z)V

    .line 50
    return-void
.end method

.method public constructor <init>(JLjava/lang/Runnable;Ljava/lang/Runnable;Z)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 103
    new-instance v0, Lcom/android/calendar/cx;

    invoke-direct {v0, p0}, Lcom/android/calendar/cx;-><init>(Lcom/android/calendar/cv;)V

    iput-object v0, p0, Lcom/android/calendar/cv;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 133
    new-instance v0, Lcom/android/calendar/cy;

    invoke-direct {v0, p0}, Lcom/android/calendar/cy;-><init>(Lcom/android/calendar/cv;)V

    iput-object v0, p0, Lcom/android/calendar/cv;->h:Landroid/content/DialogInterface$OnClickListener;

    .line 53
    iput-wide p1, p0, Lcom/android/calendar/cv;->c:J

    .line 54
    iput-object p3, p0, Lcom/android/calendar/cv;->d:Ljava/lang/Runnable;

    .line 55
    iput-object p4, p0, Lcom/android/calendar/cv;->e:Ljava/lang/Runnable;

    .line 56
    iput-boolean p5, p0, Lcom/android/calendar/cv;->f:Z

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/cv;->setRetainInstance(Z)V

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/cv;)J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/android/calendar/cv;->c:J

    return-wide v0
.end method

.method static synthetic b(Lcom/android/calendar/cv;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/cv;->b:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/cv;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/cv;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/cv;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/android/calendar/cv;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/cv;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/calendar/cv;->e:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 63
    new-instance v0, Lcom/android/calendar/cw;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/cw;-><init>(Lcom/android/calendar/cv;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/cv;->b:Landroid/content/AsyncQueryHandler;

    .line 65
    sget-object v0, Lcom/android/calendar/cv;->a:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/android/calendar/cv;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/android/calendar/cv;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    .line 75
    :cond_0
    if-eqz p1, :cond_2

    .line 76
    const-string v0, "task_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const-string v0, "task_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/cv;->c:J

    .line 79
    :cond_1
    const-string v0, "exit_when_done"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const-string v0, "exit_when_done"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/cv;->f:Z

    .line 83
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/cv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 85
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0f0159

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0158

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    iget-object v2, p0, Lcom/android/calendar/cv;->h:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0149

    iget-object v2, p0, Lcom/android/calendar/cv;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/cv;->a:Landroid/app/AlertDialog;

    .line 92
    sget-object v0, Lcom/android/calendar/cv;->a:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/android/calendar/cv;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/cv;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/android/calendar/cv;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 160
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 161
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/android/calendar/cv;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 152
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 153
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 145
    const-string v0, "task_id"

    iget-wide v2, p0, Lcom/android/calendar/cv;->c:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 146
    const-string v0, "exit_when_done"

    iget-boolean v1, p0, Lcom/android/calendar/cv;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    return-void
.end method
