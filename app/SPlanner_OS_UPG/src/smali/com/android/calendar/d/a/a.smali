.class public Lcom/android/calendar/d/a/a;
.super Ljava/lang/Object;
.source "SolarLunarConverter.java"


# static fields
.field private static j:I


# instance fields
.field private a:Lcom/android/calendar/d/a/b;

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:[I

.field private g:[I

.field private final h:I

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/d/a/a;->j:I

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/d/a/b;)V
    .locals 2

    .prologue
    const/16 v1, 0xd

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/d/a/a;->f:[I

    .line 35
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/calendar/d/a/a;->g:[I

    .line 92
    const v0, 0xa7a5e

    iput v0, p0, Lcom/android/calendar/d/a/a;->h:I

    .line 93
    const/16 v0, 0xdd

    iput v0, p0, Lcom/android/calendar/d/a/a;->i:I

    .line 40
    iput-object p1, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    .line 41
    return-void

    .line 31
    nop

    :array_0
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb5
        0xd4
        0xf3
        0x111
        0x130
        0x14e
        0x16d
    .end array-data

    .line 35
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3c
        0x5b
        0x79
        0x98
        0xb6
        0xd5
        0xf4
        0x112
        0x131
        0x14f
        0x16e
    .end array-data
.end method

.method private b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 79
    rem-int/lit8 v1, p1, 0x4

    if-gtz v1, :cond_0

    rem-int/lit8 v1, p1, 0x64

    if-ge v1, v0, :cond_1

    rem-int/lit16 v1, p1, 0x190

    if-lez v1, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 82
    :cond_1
    return v0
.end method

.method private c(I)[I
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/android/calendar/d/a/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->g:[I

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->f:[I

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/android/calendar/d/a/a;->b:I

    return v0
.end method

.method public a(I)I
    .locals 3

    .prologue
    .line 60
    add-int/lit8 v0, p1, -0x1

    .line 61
    mul-int/lit16 v1, v0, 0x16d

    div-int/lit8 v2, v0, 0x4

    add-int/2addr v1, v2

    div-int/lit8 v2, v0, 0x64

    sub-int/2addr v1, v2

    div-int/lit16 v0, v0, 0x190

    add-int/2addr v0, v1

    return v0
.end method

.method public a(IIZ)I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/d/a/b;->a(IIZ)I

    move-result v0

    return v0
.end method

.method public a(III)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 96
    iput-boolean v3, p0, Lcom/android/calendar/d/a/a;->e:Z

    .line 97
    const/16 v0, 0x759

    if-lt p1, v0, :cond_0

    const/16 v0, 0x834

    if-gt p1, v0, :cond_0

    if-ltz p2, :cond_0

    const/16 v0, 0xb

    if-gt p2, v0, :cond_0

    if-lt p3, v1, :cond_0

    const/16 v0, 0x1f

    if-le p3, v0, :cond_1

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The date "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/d/a/a;->a(I)I

    move-result v0

    add-int/2addr v0, p3

    const v2, 0xa7a5e

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 104
    iget v0, p0, Lcom/android/calendar/d/a/a;->d:I

    invoke-direct {p0, p1}, Lcom/android/calendar/d/a/a;->c(I)[I

    move-result-object v2

    aget v2, v2, p2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 109
    sget v0, Lcom/android/calendar/d/a/a;->j:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v0, v0, Lcom/android/calendar/d/a/b;->f:[I

    sget v2, Lcom/android/calendar/d/a/a;->j:I

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    iget v2, p0, Lcom/android/calendar/d/a/a;->d:I

    if-ge v0, v2, :cond_3

    iget v0, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v2, v2, Lcom/android/calendar/d/a/b;->f:[I

    sget v4, Lcom/android/calendar/d/a/a;->j:I

    aget v2, v2, v4

    if-gt v0, v2, :cond_3

    .line 112
    sget v0, Lcom/android/calendar/d/a/a;->j:I

    .line 128
    :goto_0
    add-int/lit8 v0, v0, -0x1

    .line 130
    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v4, v0, 0xe

    .line 131
    add-int/lit16 v2, v0, 0x759

    iput v2, p0, Lcom/android/calendar/d/a/a;->b:I

    .line 134
    iget v2, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-object v5, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v5, v5, Lcom/android/calendar/d/a/b;->f:[I

    aget v0, v5, v0

    sub-int v0, v2, v0

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 137
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v0, v0, Lcom/android/calendar/d/a/b;->e:[B

    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v2, v4, 0xd

    aget-byte v5, v0, v2

    .line 139
    const/16 v0, 0x7f

    if-ne v5, v0, :cond_7

    const/16 v0, 0xc

    .line 141
    :goto_1
    const/4 v2, -0x1

    iput v2, p0, Lcom/android/calendar/d/a/a;->c:I

    move v2, v3

    .line 143
    :goto_2
    if-ge v2, v0, :cond_2

    .line 144
    iget-object v6, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v6, v6, Lcom/android/calendar/d/a/b;->e:[B

    add-int v7, v4, v2

    aget-byte v6, v6, v7

    .line 146
    if-ne v5, v2, :cond_8

    .line 147
    iput-boolean v1, p0, Lcom/android/calendar/d/a/a;->e:Z

    .line 153
    :goto_3
    iget v7, p0, Lcom/android/calendar/d/a/a;->d:I

    if-gt v7, v6, :cond_9

    .line 159
    :cond_2
    return-void

    .line 116
    :cond_3
    iget v0, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v2, v2, Lcom/android/calendar/d/a/b;->f:[I

    const/16 v4, 0x6e

    aget v2, v2, v4

    if-gt v0, v2, :cond_5

    move v0, v1

    .line 118
    :goto_4
    const/16 v2, 0xdd

    if-ge v0, v2, :cond_4

    .line 121
    iget v2, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-object v4, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v4, v4, Lcom/android/calendar/d/a/b;->f:[I

    aget v4, v4, v0

    if-gt v2, v4, :cond_6

    .line 125
    :cond_4
    sput v0, Lcom/android/calendar/d/a/a;->j:I

    goto :goto_0

    .line 116
    :cond_5
    const/16 v0, 0x6f

    goto :goto_4

    .line 118
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 139
    :cond_7
    const/16 v0, 0xd

    goto :goto_1

    .line 149
    :cond_8
    iget v7, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/calendar/d/a/a;->c:I

    .line 150
    iput-boolean v3, p0, Lcom/android/calendar/d/a/a;->e:Z

    goto :goto_3

    .line 157
    :cond_9
    iget v7, p0, Lcom/android/calendar/d/a/a;->d:I

    sub-int v6, v7, v6

    iput v6, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 143
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public a(IIIZ)V
    .locals 11

    .prologue
    const/16 v1, 0x16d

    const/16 v10, 0x1e

    const/16 v4, 0xc

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 162
    const/16 v0, 0x759

    if-lt p1, v0, :cond_0

    const/16 v0, 0x834

    if-gt p1, v0, :cond_0

    if-ltz p2, :cond_0

    const/16 v0, 0xb

    if-gt p2, v0, :cond_0

    if-lt p3, v9, :cond_0

    if-le p3, v10, :cond_1

    .line 163
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The date "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/d/a/a;->c(I)[I

    move-result-object v5

    .line 169
    add-int/lit16 v0, p1, -0x759

    .line 170
    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v6, v0, 0xe

    .line 173
    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v2, v0}, Lcom/android/calendar/d/a/b;->b(I)I

    move-result v0

    .line 177
    iget-object v2, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget-object v7, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v7, v6, 0xd

    invoke-virtual {v2, v7}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v7

    .line 179
    const/16 v2, 0x7f

    if-ne v7, v2, :cond_2

    move v2, v4

    .line 181
    :goto_0
    if-ne v2, v4, :cond_3

    move v2, v3

    .line 182
    :goto_1
    if-ge v2, p2, :cond_6

    .line 183
    iget-object v4, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    add-int v7, v6, v2

    invoke-virtual {v4, v7}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v4

    add-int/2addr v4, v0

    .line 182
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_1

    .line 179
    :cond_2
    const/16 v2, 0xd

    goto :goto_0

    .line 186
    :cond_3
    if-eqz p4, :cond_4

    add-int/lit8 v2, p2, 0x1

    if-ne v2, v7, :cond_4

    move v2, v3

    .line 187
    :goto_2
    if-ge v2, v7, :cond_6

    .line 188
    iget-object v4, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    add-int v8, v6, v2

    invoke-virtual {v4, v8}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v4

    add-int/2addr v4, v0

    .line 187
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_2

    .line 192
    :cond_4
    add-int/lit8 v2, p2, 0x1

    if-le v2, v7, :cond_5

    .line 193
    add-int/lit8 p2, p2, 0x1

    :cond_5
    move v2, v3

    .line 198
    :goto_3
    if-ge v2, p2, :cond_6

    .line 199
    iget-object v4, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    add-int v7, v6, v2

    invoke-virtual {v4, v7}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v4

    add-int/2addr v4, v0

    .line 198
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_3

    .line 205
    :cond_6
    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    .line 208
    const/16 v2, 0x759

    iput v2, p0, Lcom/android/calendar/d/a/a;->b:I

    .line 209
    iput v3, p0, Lcom/android/calendar/d/a/a;->c:I

    .line 210
    iput v10, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 212
    const/16 v2, 0x14f

    if-le v0, v2, :cond_a

    .line 214
    const/16 v2, 0x75a

    iput v2, p0, Lcom/android/calendar/d/a/a;->b:I

    .line 215
    iput v3, p0, Lcom/android/calendar/d/a/a;->c:I

    .line 216
    iput v9, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 217
    add-int/lit16 v0, v0, -0x150

    move v2, v0

    move v0, v1

    .line 221
    :goto_4
    if-lt v2, v0, :cond_8

    .line 222
    sub-int/2addr v2, v0

    .line 224
    iget v0, p0, Lcom/android/calendar/d/a/a;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/d/a/a;->b:I

    .line 225
    iget v0, p0, Lcom/android/calendar/d/a/a;->b:I

    invoke-direct {p0, v0}, Lcom/android/calendar/d/a/a;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 226
    const/16 v0, 0x16e

    goto :goto_4

    :cond_7
    move v0, v1

    .line 228
    goto :goto_4

    .line 232
    :cond_8
    :goto_5
    iget v0, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    aget v0, v5, v0

    if-lt v2, v0, :cond_9

    .line 233
    iget v0, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/d/a/a;->c:I

    goto :goto_5

    .line 235
    :cond_9
    iget v0, p0, Lcom/android/calendar/d/a/a;->c:I

    aget v0, v5, v0

    sub-int v0, v2, v0

    .line 238
    iget v1, p0, Lcom/android/calendar/d/a/a;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 256
    :goto_6
    return-void

    .line 240
    :cond_a
    if-le v0, v9, :cond_c

    .line 241
    iput v9, p0, Lcom/android/calendar/d/a/a;->c:I

    .line 242
    iput v9, p0, Lcom/android/calendar/d/a/a;->d:I

    .line 243
    add-int/lit8 v0, v0, -0x2

    .line 245
    :goto_7
    iget v1, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    if-lt v0, v1, :cond_b

    .line 246
    iget v1, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/calendar/d/a/a;->c:I

    goto :goto_7

    .line 248
    :cond_b
    iget v1, p0, Lcom/android/calendar/d/a/a;->c:I

    aget v1, v5, v1

    sub-int/2addr v0, v1

    .line 251
    iget v1, p0, Lcom/android/calendar/d/a/a;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    goto :goto_6

    .line 253
    :cond_c
    iget v1, p0, Lcom/android/calendar/d/a/a;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/d/a/a;->d:I

    goto :goto_6
.end method

.method public a(Landroid/text/format/Time;)Z
    .locals 5

    .prologue
    .line 272
    iget v0, p1, Landroid/text/format/Time;->year:I

    iget v1, p1, Landroid/text/format/Time;->month:I

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 273
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/d/a/b;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget v1, p0, Lcom/android/calendar/d/a/a;->b:I

    iget v2, p0, Lcom/android/calendar/d/a/a;->c:I

    iget v3, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-boolean v4, p0, Lcom/android/calendar/d/a/a;->e:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/calendar/d/a/b;->a(IIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/android/calendar/d/a/a;->c:I

    return v0
.end method

.method public b(Landroid/text/format/Time;)Z
    .locals 5

    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/d/a/b;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    iget v1, p0, Lcom/android/calendar/d/a/a;->b:I

    iget v2, p0, Lcom/android/calendar/d/a/a;->c:I

    iget v3, p0, Lcom/android/calendar/d/a/a;->d:I

    iget-boolean v4, p0, Lcom/android/calendar/d/a/a;->e:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/calendar/d/a/b;->a(IIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/android/calendar/d/a/a;->d:I

    return v0
.end method

.method public c(Landroid/text/format/Time;)Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    invoke-virtual {v0, p1}, Lcom/android/calendar/d/a/b;->b(Landroid/text/format/Time;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/android/calendar/d/a/a;->e:Z

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/d/a/a;->a:Lcom/android/calendar/d/a/b;

    instance-of v0, v0, Lcom/android/calendar/d/a/g;

    if-eqz v0, :cond_0

    .line 73
    iget v0, p0, Lcom/android/calendar/d/a/a;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/calendar/d/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/d/a/a;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
