.class public Lcom/android/calendar/d/a/d;
.super Lcom/android/calendar/d/a/b;
.source "SolarLunarTablesHK.java"


# instance fields
.field g:Lcom/android/calendar/d/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/android/calendar/d/a/b;-><init>()V

    .line 23
    new-instance v0, Lcom/android/calendar/d/a/c;

    invoke-direct {v0}, Lcom/android/calendar/d/a/c;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/d/a/d;->g:Lcom/android/calendar/d/a/c;

    .line 27
    iget-object v0, p0, Lcom/android/calendar/d/a/d;->g:Lcom/android/calendar/d/a/c;

    iget-object v0, v0, Lcom/android/calendar/d/a/c;->g:[B

    iput-object v0, p0, Lcom/android/calendar/d/a/d;->e:[B

    .line 28
    iget-object v0, p0, Lcom/android/calendar/d/a/d;->g:Lcom/android/calendar/d/a/c;

    iget-object v0, v0, Lcom/android/calendar/d/a/c;->h:[I

    iput-object v0, p0, Lcom/android/calendar/d/a/d;->f:[I

    .line 29
    return-void
.end method

.method private c(Landroid/text/format/Time;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 82
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 83
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 85
    iget v3, p1, Landroid/text/format/Time;->year:I

    iput v3, v1, Landroid/text/format/Time;->year:I

    .line 89
    iget v3, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v3, v3, 0x64

    .line 90
    iget v4, v1, Landroid/text/format/Time;->year:I

    iget v5, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v5, v5, 0x13

    mul-int/lit8 v5, v5, 0x13

    sub-int/2addr v4, v5

    .line 91
    add-int/lit8 v5, v3, -0x11

    div-int/lit8 v5, v5, 0x19

    .line 92
    div-int/lit8 v6, v3, 0x4

    sub-int v5, v3, v5

    div-int/lit8 v5, v5, 0x3

    add-int/2addr v5, v6

    sub-int v5, v3, v5

    mul-int/lit8 v6, v4, 0x13

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0xf

    .line 93
    div-int/lit8 v6, v5, 0x1e

    mul-int/lit8 v6, v6, 0x1e

    sub-int/2addr v5, v6

    .line 94
    div-int/lit8 v6, v5, 0x1c

    div-int/lit8 v7, v5, 0x1c

    rsub-int/lit8 v7, v7, 0x1

    mul-int/2addr v6, v7

    const/16 v7, 0x1d

    add-int/lit8 v8, v5, 0x1

    div-int/2addr v7, v8

    mul-int/2addr v6, v7

    rsub-int/lit8 v4, v4, 0x15

    div-int/lit8 v4, v4, 0xb

    mul-int/2addr v4, v6

    sub-int v4, v5, v4

    .line 95
    iget v6, v1, Landroid/text/format/Time;->year:I

    iget v7, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v7, v7, 0x4

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x2

    sub-int/2addr v5, v3

    div-int/lit8 v3, v3, 0x4

    add-int/2addr v3, v5

    .line 96
    div-int/lit8 v5, v3, 0x7

    mul-int/lit8 v5, v5, 0x7

    sub-int/2addr v3, v5

    .line 97
    sub-int v3, v4, v3

    .line 99
    add-int/lit8 v4, v3, 0x28

    div-int/lit8 v4, v4, 0x2c

    add-int/lit8 v4, v4, 0x3

    iput v4, v1, Landroid/text/format/Time;->month:I

    .line 100
    add-int/lit8 v3, v3, 0x1a

    iget v4, v1, Landroid/text/format/Time;->month:I

    div-int/lit8 v4, v4, 0x4

    mul-int/lit8 v4, v4, 0x1f

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/text/format/Time;->monthDay:I

    .line 101
    iget v3, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Landroid/text/format/Time;->month:I

    .line 103
    invoke-virtual {v2, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 104
    iget v3, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 106
    iget v3, v1, Landroid/text/format/Time;->monthDay:I

    if-gtz v3, :cond_0

    .line 107
    iget v3, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v1, Landroid/text/format/Time;->month:I

    .line 108
    iget v3, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, 0x1f

    iput v3, v1, Landroid/text/format/Time;->monthDay:I

    .line 111
    :cond_0
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 112
    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 114
    iget v3, p1, Landroid/text/format/Time;->month:I

    iget v4, v1, Landroid/text/format/Time;->month:I

    if-ne v3, v4, :cond_2

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v3, v1, :cond_2

    .line 120
    :cond_1
    :goto_0
    return v0

    .line 116
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    iget v3, v2, Landroid/text/format/Time;->month:I

    if-ne v1, v3, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v2, :cond_1

    .line 120
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(IIIZ)Z
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 32
    if-eqz p4, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    if-nez p2, :cond_3

    if-eq p3, v1, :cond_2

    const/4 v2, 0x2

    if-eq p3, v2, :cond_2

    if-ne p3, v3, :cond_3

    :cond_2
    move v0, v1

    .line 37
    goto :goto_0

    .line 39
    :cond_3
    if-ne p2, v3, :cond_4

    if-ne p3, v4, :cond_4

    move v0, v1

    .line 40
    goto :goto_0

    .line 42
    :cond_4
    const/4 v2, 0x4

    if-ne p2, v2, :cond_5

    const/4 v2, 0x5

    if-ne p3, v2, :cond_5

    move v0, v1

    .line 43
    goto :goto_0

    .line 45
    :cond_5
    const/4 v2, 0x7

    if-ne p2, v2, :cond_6

    const/16 v2, 0xf

    if-ne p3, v2, :cond_6

    move v0, v1

    .line 46
    goto :goto_0

    .line 48
    :cond_6
    if-ne p2, v4, :cond_0

    const/16 v2, 0x9

    if-ne p3, v2, :cond_0

    move v0, v1

    .line 49
    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 56
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 62
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 65
    :cond_3
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_4

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 68
    :cond_4
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_5

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x19

    if-eq v1, v2, :cond_0

    .line 71
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/d/a/d;->g:Lcom/android/calendar/d/a/c;

    invoke-virtual {v1, p1}, Lcom/android/calendar/d/a/c;->c(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/d/a/d;->c(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    const/4 v0, 0x0

    goto :goto_0
.end method
