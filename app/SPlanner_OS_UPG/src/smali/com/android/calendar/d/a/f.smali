.class public Lcom/android/calendar/d/a/f;
.super Lcom/android/calendar/d/a/b;
.source "SolarLunarTablesTW.java"


# instance fields
.field g:Lcom/android/calendar/d/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/android/calendar/d/a/b;-><init>()V

    .line 23
    new-instance v0, Lcom/android/calendar/d/a/c;

    invoke-direct {v0}, Lcom/android/calendar/d/a/c;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/d/a/f;->g:Lcom/android/calendar/d/a/c;

    .line 27
    iget-object v0, p0, Lcom/android/calendar/d/a/f;->g:Lcom/android/calendar/d/a/c;

    iget-object v0, v0, Lcom/android/calendar/d/a/c;->g:[B

    iput-object v0, p0, Lcom/android/calendar/d/a/f;->e:[B

    .line 28
    iget-object v0, p0, Lcom/android/calendar/d/a/f;->g:Lcom/android/calendar/d/a/c;

    iget-object v0, v0, Lcom/android/calendar/d/a/c;->h:[I

    iput-object v0, p0, Lcom/android/calendar/d/a/f;->f:[I

    .line 29
    return-void
.end method


# virtual methods
.method public a(IIIZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 32
    if-eqz p4, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    if-nez p2, :cond_2

    if-ne p3, v1, :cond_2

    move v0, v1

    .line 37
    goto :goto_0

    .line 39
    :cond_2
    const/4 v2, 0x4

    if-ne p2, v2, :cond_3

    const/4 v2, 0x5

    if-ne p3, v2, :cond_3

    move v0, v1

    .line 40
    goto :goto_0

    .line 42
    :cond_3
    const/4 v2, 0x7

    if-ne p2, v2, :cond_0

    const/16 v2, 0xf

    if-ne p3, v2, :cond_0

    move v0, v1

    .line 43
    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 50
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v0, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_0

    .line 56
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    .line 59
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/d/a/f;->g:Lcom/android/calendar/d/a/c;

    invoke-virtual {v1, p1}, Lcom/android/calendar/d/a/c;->c(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    const/4 v0, 0x0

    goto :goto_0
.end method
