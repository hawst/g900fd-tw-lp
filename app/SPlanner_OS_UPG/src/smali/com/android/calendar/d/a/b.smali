.class public abstract Lcom/android/calendar/d/a/b;
.super Ljava/lang/Object;
.source "SolarLunarTables.java"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field protected e:[B

.field protected f:[I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/16 v0, 0x759

    iput v0, p0, Lcom/android/calendar/d/a/b;->a:I

    .line 23
    const/16 v0, 0x834

    iput v0, p0, Lcom/android/calendar/d/a/b;->b:I

    .line 24
    const/16 v0, 0xd

    iput v0, p0, Lcom/android/calendar/d/a/b;->c:I

    .line 25
    const/16 v0, 0xe

    iput v0, p0, Lcom/android/calendar/d/a/b;->d:I

    .line 32
    return-void
.end method


# virtual methods
.method public a(I)B
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/calendar/d/a/b;->e:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public a(IIZ)I
    .locals 3

    .prologue
    .line 35
    const/16 v0, 0x759

    if-lt p1, v0, :cond_0

    const/16 v0, 0x834

    if-gt p1, v0, :cond_0

    if-ltz p2, :cond_0

    const/16 v0, 0xb

    if-le p2, v0, :cond_1

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The month "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_1
    add-int/lit16 v0, p1, -0x759

    mul-int/lit8 v0, v0, 0xe

    .line 40
    add-int/lit8 v1, v0, 0xd

    invoke-virtual {p0, v1}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v1

    .line 41
    if-nez p3, :cond_2

    if-ge p2, v1, :cond_2

    .line 42
    add-int/2addr v0, p2

    invoke-virtual {p0, v0}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v0

    .line 44
    :goto_0
    return v0

    :cond_2
    add-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/d/a/b;->a(I)B

    move-result v0

    goto :goto_0
.end method

.method public a(IIIZ)Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/text/format/Time;)Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/d/a/b;->f:[I

    aget v0, v0, p1

    return v0
.end method

.method public b(Landroid/text/format/Time;)Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method
