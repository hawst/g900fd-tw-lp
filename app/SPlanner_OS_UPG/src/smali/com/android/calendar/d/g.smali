.class public Lcom/android/calendar/d/g;
.super Ljava/lang/Object;
.source "SECCalendarFeatures.java"


# static fields
.field private static a:Lcom/android/calendar/d/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static h()Lcom/android/calendar/d/g;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v1, "KOREA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    new-instance v0, Lcom/android/calendar/d/f;

    invoke-direct {v0}, Lcom/android/calendar/d/f;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    .line 66
    :cond_0
    :goto_0
    sget-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    return-object v0

    .line 50
    :cond_1
    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    new-instance v0, Lcom/android/calendar/d/a;

    invoke-direct {v0}, Lcom/android/calendar/d/a;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 52
    :cond_2
    const-string v1, "HKTW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 53
    new-instance v0, Lcom/android/calendar/d/c;

    invoke-direct {v0}, Lcom/android/calendar/d/c;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 54
    :cond_3
    const-string v1, "HONGKONG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 55
    new-instance v0, Lcom/android/calendar/d/d;

    invoke-direct {v0}, Lcom/android/calendar/d/d;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 56
    :cond_4
    const-string v1, "TAIWAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 57
    new-instance v0, Lcom/android/calendar/d/h;

    invoke-direct {v0}, Lcom/android/calendar/d/h;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 58
    :cond_5
    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 59
    new-instance v0, Lcom/android/calendar/d/e;

    invoke-direct {v0}, Lcom/android/calendar/d/e;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 60
    :cond_6
    const-string v1, "VI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 61
    new-instance v0, Lcom/android/calendar/d/i;

    invoke-direct {v0}, Lcom/android/calendar/d/i;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0

    .line 63
    :cond_7
    new-instance v0, Lcom/android/calendar/d/g;

    invoke-direct {v0}, Lcom/android/calendar/d/g;-><init>()V

    sput-object v0, Lcom/android/calendar/d/g;->a:Lcom/android/calendar/d/g;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/android/calendar/d/a/a;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()Lcom/android/calendar/d/a/b;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method
