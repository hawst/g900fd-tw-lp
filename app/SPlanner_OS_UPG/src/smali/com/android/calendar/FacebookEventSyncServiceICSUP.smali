.class public Lcom/android/calendar/FacebookEventSyncServiceICSUP;
.super Landroid/app/Service;
.source "FacebookEventSyncServiceICSUP.java"


# instance fields
.field private volatile a:Landroid/os/Looper;

.field private volatile b:Lcom/android/calendar/dy;

.field private c:Lcom/android/calendar/dx;

.field private d:Lcom/android/calendar/dw;

.field private e:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c:Lcom/android/calendar/dx;

    .line 70
    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->d:Lcom/android/calendar/dw;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->e:Ljava/util/HashMap;

    .line 379
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/FacebookEventSyncServiceICSUP;)Lcom/android/calendar/dw;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->d:Lcom/android/calendar/dw;

    return-object v0
.end method

.method private c()V
    .locals 9

    .prologue
    .line 227
    const-string v0, "FBEventSyncService"

    const-string v1, "downloadPhoto start"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 231
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 232
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 234
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 235
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 237
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 239
    new-instance v6, Ljava/net/URL;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v6, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 242
    invoke-virtual {v2}, Ljava/net/URLConnection;->getContentLength()I

    move-result v6

    .line 243
    if-lez v6, :cond_0

    .line 247
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 249
    invoke-virtual {v2}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 251
    if-eqz v2, :cond_0

    .line 252
    new-array v7, v6, [B

    .line 253
    invoke-virtual {v2, v7}, Ljava/io/InputStream;->read([B)I

    move-result v8

    .line 254
    if-ne v8, v6, :cond_0

    .line 258
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 259
    const-string v6, "event_id"

    invoke-virtual {v5, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 260
    const-string v1, "photo"

    invoke-virtual {v5, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 263
    :try_start_0
    sget-object v1, Lcom/android/calendar/hj;->d:Landroid/net/Uri;

    invoke-virtual {v3, v1, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :goto_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 264
    :catch_0
    move-exception v1

    .line 265
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteFullException;->printStackTrace()V

    goto :goto_1

    .line 271
    :cond_1
    const-string v0, "FBEventSyncService"

    const-string v1, "downloadPhoto end"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 135
    const-string v0, "FBEventSyncService"

    const-string v1, "syncEventsPartial"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    sget-object v3, Lcom/android/calendar/hj;->f:Landroid/net/Uri;

    .line 138
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c:Lcom/android/calendar/dx;

    const/4 v1, 0x3

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/dx;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public b()J
    .locals 12

    .prologue
    const/4 v3, 0x0

    const-wide/16 v8, -0x1

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 143
    const-string v0, "FBEventSyncService"

    const-string v1, "createFacebookCalendar"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 150
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 151
    array-length v5, v2

    move v1, v3

    :goto_0
    if-ge v1, v5, :cond_8

    aget-object v6, v2, v1

    .line 152
    iget-object v7, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    if-eqz v7, :cond_1

    iget-object v7, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v10, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 153
    iget-object v2, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 154
    iget-object v1, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object v6, v1

    move-object v7, v2

    .line 159
    :goto_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 210
    :cond_0
    :goto_2
    return-wide v8

    .line 151
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_2
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "account_name="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "account_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v6}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " (name=\'Facebook\' OR name=\'facebook\') "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 174
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_6

    .line 175
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 177
    const-string v3, "account_name"

    invoke-virtual {v1, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v3, "account_type"

    invoke-virtual {v1, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v3, "name"

    const-string v5, "Facebook"

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v3, "calendar_displayName"

    const-string v5, "Facebook"

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v3, "calendar_access_level"

    const/16 v5, 0xc8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    const-string v3, "visible"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 184
    const-string v3, "sync_events"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 185
    const-string v3, "calendar_color"

    sget v5, Lcom/android/calendar/hj;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 186
    const-string v3, "ownerAccount"

    invoke-virtual {v1, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :try_start_0
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 195
    :goto_3
    if-eqz v4, :cond_7

    .line 196
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 203
    :goto_4
    if-eqz v2, :cond_4

    .line 204
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 207
    :cond_4
    cmp-long v2, v0, v8

    if-nez v2, :cond_5

    :cond_5
    move-wide v8, v0

    .line 210
    goto/16 :goto_2

    .line 191
    :catch_0
    move-exception v0

    .line 192
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteFullException;->printStackTrace()V

    goto :goto_3

    .line 199
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 200
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    goto :goto_4

    :cond_7
    move-wide v0, v8

    goto :goto_4

    :cond_8
    move-object v6, v4

    move-object v7, v4

    goto/16 :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 90
    const-string v0, "FBEventSyncService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FacebookEventSyncService"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 93
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 94
    new-instance v1, Lcom/android/calendar/dx;

    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/dx;-><init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->c:Lcom/android/calendar/dx;

    .line 95
    new-instance v1, Lcom/android/calendar/dw;

    invoke-virtual {p0}, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/dw;-><init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->d:Lcom/android/calendar/dw;

    .line 97
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->a:Landroid/os/Looper;

    .line 98
    new-instance v0, Lcom/android/calendar/dy;

    iget-object v1, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->a:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/dy;-><init>(Lcom/android/calendar/FacebookEventSyncServiceICSUP;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b:Lcom/android/calendar/dy;

    .line 99
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 121
    const-string v0, "FBEventSyncService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 123
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 103
    const-string v0, "FBEventSyncService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    if-eqz p1, :cond_0

    .line 105
    iget-object v0, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b:Lcom/android/calendar/dy;

    invoke-virtual {v0}, Lcom/android/calendar/dy;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 106
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 109
    iget-object v1, p0, Lcom/android/calendar/FacebookEventSyncServiceICSUP;->b:Lcom/android/calendar/dy;

    invoke-virtual {v1, v0}, Lcom/android/calendar/dy;->sendMessage(Landroid/os/Message;)Z

    .line 111
    :cond_0
    const/4 v0, 0x3

    return v0
.end method
