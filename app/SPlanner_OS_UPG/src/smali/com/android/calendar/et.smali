.class Lcom/android/calendar/et;
.super Lcom/android/calendar/ew;
.source "LinkActionChooserFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/eo;


# direct methods
.method constructor <init>(Lcom/android/calendar/eo;Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/ew;-><init>(Landroid/app/Activity;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    invoke-static {v0}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    invoke-static {v0}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    invoke-static {v2}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/eo;->a(Lcom/android/calendar/eo;Ljava/lang/String;)Ljava/lang/String;

    .line 186
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/et;->a:Lcom/android/calendar/eo;

    invoke-static {v0}, Lcom/android/calendar/eo;->b(Lcom/android/calendar/eo;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 187
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 188
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    invoke-static {}, Lcom/android/calendar/eo;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LinkActionChooserFragment: Browser activity not found."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
