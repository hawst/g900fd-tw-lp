.class Lcom/android/calendar/fl;
.super Landroid/os/AsyncTask;
.source "RecipientAdapter.java"


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic b:Lcom/android/ex/a/b;

.field final synthetic c:Lcom/android/calendar/fk;


# direct methods
.method constructor <init>(Lcom/android/calendar/fk;Landroid/net/Uri;Lcom/android/ex/a/b;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/android/calendar/fl;->c:Lcom/android/calendar/fk;

    iput-object p2, p0, Lcom/android/calendar/fl;->a:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/calendar/fl;->b:Lcom/android/ex/a/b;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 748
    iget-object v0, p0, Lcom/android/calendar/fl;->c:Lcom/android/calendar/fk;

    invoke-static {v0}, Lcom/android/calendar/fk;->b(Lcom/android/calendar/fk;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/fl;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/ft;->a:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 750
    if-eqz v1, :cond_1

    .line 752
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 754
    iget-object v2, p0, Lcom/android/calendar/fl;->b:Lcom/android/ex/a/b;

    invoke-virtual {v2, v0}, Lcom/android/ex/a/b;->a([B)V

    .line 756
    iget-object v2, p0, Lcom/android/calendar/fl;->c:Lcom/android/calendar/fk;

    invoke-static {v2}, Lcom/android/calendar/fk;->l(Lcom/android/calendar/fk;)Landroid/os/Handler;

    move-result-object v2

    new-instance v4, Lcom/android/calendar/fm;

    invoke-direct {v4, p0, v0}, Lcom/android/calendar/fm;-><init>(Lcom/android/calendar/fl;[B)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 765
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 768
    :cond_1
    return-object v3

    .line 765
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/calendar/fl;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
