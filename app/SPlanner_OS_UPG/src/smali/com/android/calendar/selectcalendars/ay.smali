.class Lcom/android/calendar/selectcalendars/ay;
.super Ljava/lang/Object;
.source "ExpandableSelectCalendarsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/au;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/au;)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 480
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->b(Lcom/android/calendar/selectcalendars/au;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->e(Lcom/android/calendar/selectcalendars/au;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 510
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->e(Lcom/android/calendar/selectcalendars/au;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 487
    iget-boolean v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    if-nez v2, :cond_2

    const/4 v1, 0x1

    .line 488
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/au;->f(Lcom/android/calendar/selectcalendars/au;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 489
    iput-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    .line 490
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->e(Lcom/android/calendar/selectcalendars/au;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 491
    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 492
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 493
    iput-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 494
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "contact_birthday"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 495
    iput-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 498
    :cond_5
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "LOCAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "contact_birthday"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 500
    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v4, :cond_6

    .line 501
    sget-object v4, Lcom/android/calendar/hj;->s:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v5}, Lcom/android/calendar/selectcalendars/au;->a(Lcom/android/calendar/selectcalendars/au;)Landroid/app/Activity;

    move-result-object v5

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 503
    :cond_6
    sget-object v4, Lcom/android/calendar/hj;->t:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v5}, Lcom/android/calendar/selectcalendars/au;->a(Lcom/android/calendar/selectcalendars/au;)Landroid/app/Activity;

    move-result-object v5

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-static {v4, v5, v0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 509
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ay;->a:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->d(Lcom/android/calendar/selectcalendars/au;)V

    goto/16 :goto_0
.end method
