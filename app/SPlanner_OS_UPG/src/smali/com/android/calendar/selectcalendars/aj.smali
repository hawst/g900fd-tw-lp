.class public Lcom/android/calendar/selectcalendars/aj;
.super Landroid/app/DialogFragment;
.source "ExpandableCalendarsListFragment.java"


# instance fields
.field private a:Z

.field private b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

.field private c:Lcom/android/calendar/selectcalendars/ae;

.field private d:Lcom/android/calendar/d/g;

.field private e:Lcom/android/calendar/ag;

.field private f:Landroid/app/Activity;

.field private g:Z

.field private final h:Landroid/database/ContentObserver;

.field private i:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 86
    new-instance v0, Lcom/android/calendar/selectcalendars/ak;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/selectcalendars/ak;-><init>(Lcom/android/calendar/selectcalendars/aj;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->h:Landroid/database/ContentObserver;

    .line 98
    new-instance v0, Lcom/android/calendar/selectcalendars/al;

    invoke-direct {v0, p0}, Lcom/android/calendar/selectcalendars/al;-><init>(Lcom/android/calendar/selectcalendars/aj;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->i:Landroid/content/BroadcastReceiver;

    .line 114
    return-void
.end method

.method private a(Landroid/app/Activity;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 138
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 140
    const v1, 0x7f04003b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 141
    new-instance v0, Lcom/android/calendar/selectcalendars/ae;

    invoke-direct {v0, p1}, Lcom/android/calendar/selectcalendars/ae;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    .line 142
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/aj;->c()V

    .line 144
    const v0, 0x7f1200df

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    .line 145
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, v2, v4}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorPaddings(II)V

    .line 151
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 152
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    new-instance v2, Lcom/android/calendar/selectcalendars/am;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/am;-><init>(Lcom/android/calendar/selectcalendars/aj;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setOnGroupClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;)V

    .line 164
    return-object v1

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorGravity(I)V

    .line 149
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorPaddings(II)V

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 18

    .prologue
    .line 226
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 228
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 229
    const-string v2, "calendar_displayName"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 230
    const-string v2, "name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 231
    const-string v2, "calendar_color"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 232
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 233
    const-string v2, "ownerAccount"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 234
    const-string v2, "visible"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 235
    const-string v2, "account_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 236
    const-string v2, "sync_events"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    .line 238
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 240
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 241
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 242
    if-nez v2, :cond_0

    .line 243
    const v2, 0x7f0b0073

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 245
    :cond_0
    new-instance v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v14}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 246
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 247
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 248
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    .line 249
    iput v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 250
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 251
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->f:Ljava/lang/String;

    .line 252
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 253
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    .line 254
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 255
    const/4 v2, 0x0

    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 256
    const/4 v2, 0x1

    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 258
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 252
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 253
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 261
    :cond_3
    return-object v3
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/aj;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/aj;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/aj;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/aj;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11

    .prologue
    const/4 v10, 0x2

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 278
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 280
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 284
    new-instance v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v3}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 285
    const-string v0, "My calendar"

    iput-object v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 286
    const/4 v0, 0x0

    iput v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 288
    new-instance v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v0}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 289
    const-string v4, ""

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 290
    iput v10, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 293
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    .line 294
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 295
    iget-object v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v7, "LOCAL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 296
    new-instance v6, Lcom/android/calendar/selectcalendars/ao;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/android/calendar/selectcalendars/ao;-><init>(Lcom/android/calendar/selectcalendars/ak;)V

    .line 297
    iget-object v7, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    iput-object v7, v6, Lcom/android/calendar/selectcalendars/ao;->a:Ljava/lang/String;

    .line 298
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    iput-object v0, v6, Lcom/android/calendar/selectcalendars/ao;->b:Ljava/lang/String;

    .line 300
    invoke-static {v4, v6}, Lcom/android/calendar/selectcalendars/aj;->a(Ljava/util/Set;Lcom/android/calendar/selectcalendars/ao;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 307
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 308
    iget-object v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v7, "LOCAL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 309
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/app/Activity;)Z

    move-result v6

    iput-boolean v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    goto :goto_1

    .line 314
    :cond_3
    iput-object v2, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 315
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/ao;

    .line 319
    new-instance v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v3}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 320
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/ao;->a:Ljava/lang/String;

    .line 321
    iget-object v5, v0, Lcom/android/calendar/selectcalendars/ao;->b:Ljava/lang/String;

    .line 323
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/ao;->a:Ljava/lang/String;

    iput-object v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 324
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 325
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 326
    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v9, "com.google"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v9, "com.android.exchange"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 329
    :cond_6
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/app/Activity;)Z

    move-result v8

    iput-boolean v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    goto :goto_3

    .line 333
    :cond_7
    iput v10, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 334
    iput-object v6, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 335
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 336
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 339
    :cond_8
    return-object v1
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/aj;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/aj;->c()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/aj;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/aj;->g:Z

    return p1
.end method

.method static a(Ljava/util/Set;Lcom/android/calendar/selectcalendars/ao;)Z
    .locals 3

    .prologue
    .line 265
    const/4 v1, 0x0

    .line 266
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/ao;

    .line 267
    invoke-virtual {v0, p1}, Lcom/android/calendar/selectcalendars/ao;->a(Lcom/android/calendar/selectcalendars/ao;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    .line 273
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/aj;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->b:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/aj;)Lcom/android/calendar/selectcalendars/ae;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->e:Lcom/android/calendar/ag;

    const/4 v1, 0x1

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/aj;->d()Ljava/lang/String;

    move-result-object v5

    const-string v7, "calendar_displayName"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/aj;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 376
    const/4 v1, 0x0

    .line 377
    const-string v0, "deleted!=1"

    .line 379
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    if-nez v3, :cond_0

    .line 380
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    .line 383
    :cond_0
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    invoke-static {v3}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 384
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND account_type!=\'com.osp.app.signin\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 387
    const-string v1, "legalHoliday"

    .line 388
    const-string v1, "legalSubstHoliday"

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND name!=\'legalHoliday\' AND name!=\'legalSubstHoliday\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v1, v2

    .line 393
    :cond_2
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aj;->d:Lcom/android/calendar/d/g;

    invoke-virtual {v3}, Lcom/android/calendar/d/g;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 394
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND name!=\'24SolarTerms\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v1, v2

    .line 397
    :cond_3
    if-eqz v1, :cond_4

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR name IS NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_4
    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ae;->a()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    if-nez v0, :cond_0

    move v0, v1

    .line 363
    :goto_0
    return v0

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ae;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    .line 352
    goto :goto_0

    .line 355
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->c:Lcom/android/calendar/selectcalendars/ae;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ae;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 356
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 357
    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v4, :cond_3

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/r;->b(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/r;->c(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 359
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 363
    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 170
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    .line 171
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/aj;->a:Z

    .line 172
    new-instance v0, Lcom/android/calendar/selectcalendars/an;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/selectcalendars/an;-><init>(Lcom/android/calendar/selectcalendars/aj;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->e:Lcom/android/calendar/ag;

    .line 205
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 206
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aj;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 208
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    .line 129
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 130
    const v1, 0x7f0f0189

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/selectcalendars/aj;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 134
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/aj;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 412
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 413
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aj;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 414
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aj;->h:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 407
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 408
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 213
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/selectcalendars/aj;->h:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 215
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    invoke-static {v2}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/aj;->f:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "key_refresh_state"

    invoke-static {v2, v3, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "key_requery_state"

    invoke-static {v2, v3, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/aj;->g:Z

    .line 219
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/aj;->g:Z

    if-eqz v0, :cond_2

    .line 220
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/aj;->c()V

    .line 221
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/aj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "key_refresh_state"

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 223
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 218
    goto :goto_0
.end method
