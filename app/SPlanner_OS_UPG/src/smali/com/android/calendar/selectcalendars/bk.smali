.class Lcom/android/calendar/selectcalendars/bk;
.super Landroid/content/AsyncQueryHandler;
.source "SelectCalendarsFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/bi;


# direct methods
.method public constructor <init>(Lcom/android/calendar/selectcalendars/bi;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    .line 200
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 201
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 206
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 207
    if-nez p3, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/bi;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 211
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 216
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 218
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-static {v0, p3}, Lcom/android/calendar/selectcalendars/bi;->a(Lcom/android/calendar/selectcalendars/bi;Landroid/database/Cursor;)V

    .line 219
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 223
    :pswitch_1
    check-cast p2, Ljava/util/ArrayList;

    .line 224
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-static {v0, p3}, Lcom/android/calendar/selectcalendars/bi;->b(Lcom/android/calendar/selectcalendars/bi;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    .line 225
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-static {v1, p2, v0}, Lcom/android/calendar/selectcalendars/bi;->a(Lcom/android/calendar/selectcalendars/bi;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    iget-object v1, v1, Lcom/android/calendar/selectcalendars/bi;->d:Lcom/android/calendar/selectcalendars/bl;

    invoke-virtual {v1, v0}, Lcom/android/calendar/selectcalendars/bl;->a(Ljava/util/ArrayList;)V

    .line 227
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/bi;->a(Lcom/android/calendar/selectcalendars/bi;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 228
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bk;->a:Lcom/android/calendar/selectcalendars/bi;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/bi;->a(Lcom/android/calendar/selectcalendars/bi;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    :cond_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
