.class Lcom/android/calendar/selectcalendars/x;
.super Lcom/android/calendar/ag;
.source "DeleteCalendarsFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/w;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/w;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 101
    if-nez p3, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/w;->isAdded()Z

    move-result v1

    if-nez v1, :cond_2

    .line 105
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 110
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 112
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v2, p3}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 113
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/selectcalendars/s;->a(Ljava/util/ArrayList;)V

    .line 114
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    iget-boolean v1, v1, Lcom/android/calendar/selectcalendars/w;->a:Z

    if-eqz v1, :cond_3

    .line 115
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->c(Lcom/android/calendar/selectcalendars/w;)V

    .line 116
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/s;->b()I

    .line 117
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    iput-boolean v0, v1, Lcom/android/calendar/selectcalendars/w;->a:Z

    .line 119
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->d(Lcom/android/calendar/selectcalendars/w;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 120
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 122
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->e(Lcom/android/calendar/selectcalendars/w;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 123
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/s;->getGroupCount()I

    move-result v1

    .line 124
    :goto_1
    if-ge v0, v1, :cond_4

    .line 125
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/w;->e(Lcom/android/calendar/selectcalendars/w;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->e(Lcom/android/calendar/selectcalendars/w;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    goto/16 :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected b(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 138
    if-ne p1, v2, :cond_0

    .line 139
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->d(Lcom/android/calendar/selectcalendars/w;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "key_requery_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 140
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->d(Lcom/android/calendar/selectcalendars/w;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "key_local_add_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 141
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/x;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->d(Lcom/android/calendar/selectcalendars/w;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 143
    :cond_0
    return-void
.end method
