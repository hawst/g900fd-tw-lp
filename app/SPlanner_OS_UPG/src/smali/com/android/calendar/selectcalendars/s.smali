.class public Lcom/android/calendar/selectcalendars/s;
.super Landroid/widget/BaseExpandableListAdapter;
.source "DeleteCalendarsAdapter.java"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Landroid/app/Activity;

.field private c:Landroid/view/ActionMode;

.field private d:Landroid/view/LayoutInflater;

.field private e:Lcom/android/calendar/selectcalendars/w;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/ActionMode;Lcom/android/calendar/selectcalendars/w;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/s;->f:Z

    .line 67
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/s;->b:Landroid/app/Activity;

    .line 68
    iput-object p2, p0, Lcom/android/calendar/selectcalendars/s;->c:Landroid/view/ActionMode;

    .line 69
    iput-object p3, p0, Lcom/android/calendar/selectcalendars/s;->e:Lcom/android/calendar/selectcalendars/w;

    .line 70
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/s;->d:Landroid/view/LayoutInflater;

    .line 71
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/s;->f:Z

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/s;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->c:Landroid/view/ActionMode;

    return-object v0
.end method


# virtual methods
.method protected a(Z)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 335
    .line 336
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 356
    :goto_0
    return v1

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 340
    iput-boolean p1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    .line 341
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 342
    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 343
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 344
    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v4, :cond_2

    .line 345
    iput-boolean p1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 347
    :cond_2
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v0, :cond_1

    .line 348
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    move v1, v0

    .line 352
    goto :goto_1

    .line 353
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->b()I

    .line 354
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 227
    iget-object v2, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 228
    iget-object v3, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 229
    iget-object v4, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 230
    iget-boolean v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 233
    const v0, 0x7f12001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 234
    const v1, 0x7f12001b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 235
    if-eqz v2, :cond_0

    .line 236
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 237
    const-string v6, "My calendar"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 238
    const v2, 0x7f0f02bd

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 246
    :cond_0
    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget v2, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    invoke-static {v2}, Lcom/android/calendar/hj;->b(I)I

    move-result v2

    .line 250
    const v5, 0x7f120018

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 251
    invoke-virtual {v5, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 253
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    const-string v2, "LOCAL"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 256
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    :goto_1
    iget-boolean v1, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    invoke-virtual {p2, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 261
    iget-boolean v1, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 262
    return-void

    .line 239
    :cond_1
    const-string v6, "My Task"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 240
    const v2, 0x7f0f02bf

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 241
    :cond_2
    const-string v6, "My calendars (personal)"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 242
    const v2, 0x7f0f02bb

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 258
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    .line 271
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->notifyDataSetChanged()V

    .line 272
    return-void
.end method

.method protected b()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 200
    .line 204
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 205
    if-eqz v0, :cond_1

    iget-object v5, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 206
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 207
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v0, :cond_0

    .line 208
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 210
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v1, v3

    move v3, v1

    move v1, v0

    .line 214
    goto :goto_0

    .line 216
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->d()I

    move-result v0

    if-ne v3, v0, :cond_3

    .line 217
    const/4 v0, 0x1

    move v3, v0

    .line 220
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iput-boolean v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    .line 221
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 222
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->notifyDataSetChanged()V

    .line 223
    return v1

    :cond_3
    move v3, v2

    goto :goto_2
.end method

.method public c()Ljava/util/Set;
    .locals 6

    .prologue
    .line 275
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 277
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 278
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 279
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 280
    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v4, :cond_1

    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v4, :cond_1

    .line 281
    iget-wide v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 286
    :cond_2
    return-object v1
.end method

.method public d()I
    .locals 4

    .prologue
    .line 290
    const/4 v0, 0x0

    .line 292
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 293
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 294
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 295
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-nez v0, :cond_0

    .line 296
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v1, v0

    .line 300
    goto :goto_0

    .line 301
    :cond_2
    return v1
.end method

.method public e()I
    .locals 4

    .prologue
    .line 305
    const/4 v0, 0x0

    .line 307
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 308
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 309
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 310
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v0, :cond_0

    .line 311
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v1, v0

    .line 315
    goto :goto_0

    .line 316
    :cond_2
    return v1
.end method

.method public f()I
    .locals 5

    .prologue
    .line 320
    const/4 v0, 0x0

    .line 322
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 323
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 324
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 325
    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v4, :cond_0

    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v0, :cond_0

    .line 326
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    move v1, v0

    .line 330
    goto :goto_0

    .line 331
    :cond_2
    return v1
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 114
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 173
    if-nez p4, :cond_0

    .line 174
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f040001

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 180
    const v1, 0x7f120017

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 181
    const v1, 0x7f12001c

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 182
    iget-boolean v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 184
    new-instance v3, Lcom/android/calendar/selectcalendars/u;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/calendar/selectcalendars/u;-><init>(Lcom/android/calendar/selectcalendars/s;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/s;->b()I

    .line 195
    invoke-virtual {p0, v0, p4}, Lcom/android/calendar/selectcalendars/s;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;)V

    .line 196
    return-object p4
.end method

.method public getChildrenCount(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    .line 92
    :goto_0
    return v0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 89
    if-nez v0, :cond_1

    move v0, v1

    .line 90
    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 79
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 109
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 125
    .line 128
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/s;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 129
    if-eqz p3, :cond_5

    instance-of v1, p3, Landroid/widget/LinearLayout;

    if-eqz v1, :cond_5

    .line 131
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/selectcalendars/v;

    move-object v2, p3

    .line 134
    :goto_0
    if-nez v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/s;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f040089

    invoke-virtual {v1, v2, p4, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 136
    new-instance v1, Lcom/android/calendar/selectcalendars/v;

    invoke-direct {v1, v3}, Lcom/android/calendar/selectcalendars/v;-><init>(Lcom/android/calendar/selectcalendars/t;)V

    .line 137
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 140
    :cond_0
    const v3, 0x7f120289

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/calendar/selectcalendars/v;->a(Lcom/android/calendar/selectcalendars/v;Landroid/view/View;)Landroid/view/View;

    .line 141
    invoke-static {v1}, Lcom/android/calendar/selectcalendars/v;->a(Lcom/android/calendar/selectcalendars/v;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 142
    invoke-static {v1}, Lcom/android/calendar/selectcalendars/v;->a(Lcom/android/calendar/selectcalendars/v;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 143
    invoke-static {v1}, Lcom/android/calendar/selectcalendars/v;->a(Lcom/android/calendar/selectcalendars/v;)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/android/calendar/selectcalendars/t;

    invoke-direct {v3, p0, p4, p1}, Lcom/android/calendar/selectcalendars/t;-><init>(Lcom/android/calendar/selectcalendars/s;Landroid/view/ViewGroup;I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const v1, 0x7f12028b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 153
    iget v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 154
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f009d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    return-object v2

    .line 155
    :cond_1
    iget v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-nez v3, :cond_2

    .line 156
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f02bd

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 157
    :cond_2
    iget v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-ne v3, v5, :cond_3

    .line 158
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f02bf

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 159
    :cond_3
    const-string v3, "Task"

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 160
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0428

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 162
    :cond_4
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    goto :goto_1

    :cond_5
    move-object v1, v3

    move-object v2, v3

    goto/16 :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    return v0
.end method
