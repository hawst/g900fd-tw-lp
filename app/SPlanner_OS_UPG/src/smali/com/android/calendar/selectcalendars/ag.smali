.class Lcom/android/calendar/selectcalendars/ag;
.super Ljava/lang/Object;
.source "ExpandableCalendarsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

.field final synthetic b:Lcom/android/calendar/selectcalendars/ae;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/ae;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    iput-object p2, p0, Lcom/android/calendar/selectcalendars/ag;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 225
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ae;->a(Lcom/android/calendar/selectcalendars/ae;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    invoke-static {v0, v3}, Lcom/android/calendar/selectcalendars/ae;->a(Lcom/android/calendar/selectcalendars/ae;Z)Z

    .line 230
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ae;->b(Lcom/android/calendar/selectcalendars/ae;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 231
    sget-object v1, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 232
    if-nez v1, :cond_1

    .line 233
    new-instance v1, Lcom/android/calendar/selectcalendars/d;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/ag;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v1, v3, v2}, Lcom/android/calendar/selectcalendars/d;-><init>(ZLcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V

    .line 235
    :try_start_0
    sget-object v2, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ae;->d(Lcom/android/calendar/selectcalendars/ae;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ag;->b:Lcom/android/calendar/selectcalendars/ae;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/ae;->c(Lcom/android/calendar/selectcalendars/ae;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    const-string v0, "ExpandableCalendarsListAdapter"

    const-string v1, "Fail to launch AddCalendarFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
