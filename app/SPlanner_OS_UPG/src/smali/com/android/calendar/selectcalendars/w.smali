.class public Lcom/android/calendar/selectcalendars/w;
.super Landroid/app/DialogFragment;
.source "DeleteCalendarsFragment.java"


# static fields
.field private static e:Z


# instance fields
.field protected a:Z

.field private b:Z

.field private c:Landroid/app/Activity;

.field private d:Lcom/android/calendar/ag;

.field private f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

.field private g:Lcom/android/calendar/selectcalendars/s;

.field private h:Ljava/util/ArrayList;

.field private i:[J

.field private j:Landroid/app/AlertDialog;

.field private k:Lcom/android/calendar/selectcalendars/ad;

.field private l:Landroid/view/ActionMode;

.field private m:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/w;-><init>(Z)V

    .line 80
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/w;->a:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->j:Landroid/app/AlertDialog;

    .line 83
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/w;->b:Z

    .line 84
    return-void
.end method

.method public constructor <init>(ZLjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/w;->a:Z

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->j:Landroid/app/AlertDialog;

    .line 87
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/w;->b:Z

    .line 88
    iput-object p2, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    .line 89
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/w;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/w;->l:Landroid/view/ActionMode;

    return-object p1
.end method

.method private a(Landroid/app/Activity;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 288
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 290
    const v2, 0x7f040086

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 292
    sget-boolean v0, Lcom/android/calendar/selectcalendars/w;->e:Z

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 296
    :cond_0
    new-instance v0, Lcom/android/calendar/selectcalendars/ad;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/selectcalendars/ad;-><init>(Lcom/android/calendar/selectcalendars/w;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    .line 297
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->l:Landroid/view/ActionMode;

    .line 298
    new-instance v0, Lcom/android/calendar/selectcalendars/s;

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->l:Landroid/view/ActionMode;

    invoke-direct {v0, p1, v3, p0}, Lcom/android/calendar/selectcalendars/s;-><init>(Landroid/app/Activity;Landroid/view/ActionMode;Lcom/android/calendar/selectcalendars/w;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    .line 299
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/android/calendar/selectcalendars/s;->a(Ljava/util/ArrayList;)V

    .line 301
    const v0, 0x7f1200df

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    .line 302
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v0, v5}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setItemsCanFocus(Z)V

    .line 303
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorGravity(I)V

    .line 304
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorPaddings(II)V

    .line 305
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 306
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    new-instance v3, Lcom/android/calendar/selectcalendars/z;

    invoke-direct {v3, p0}, Lcom/android/calendar/selectcalendars/z;-><init>(Lcom/android/calendar/selectcalendars/w;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setOnGroupClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;)V

    .line 318
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 319
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/s;->getGroupCount()I

    move-result v3

    move v0, v1

    .line 320
    :goto_0
    if-ge v0, v3, :cond_1

    .line 321
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 320
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v0, v5}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 325
    return-object v2
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 22

    .prologue
    .line 165
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 166
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 167
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 168
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 170
    const-string v4, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 171
    const-string v4, "calendar_displayName"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 172
    const-string v4, "name"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 173
    const-string v4, "calendar_color"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 174
    const-string v4, "account_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 175
    const-string v4, "ownerAccount"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 176
    const-string v4, "account_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 177
    const-string v4, "sync_events"

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 179
    new-instance v16, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct/range {v16 .. v16}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 180
    const-string v4, "My calendar"

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 181
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iput v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 183
    new-instance v17, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct/range {v17 .. v17}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 184
    const-string v4, ""

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 185
    const/4 v4, 0x2

    move-object/from16 v0, v17

    iput v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    .line 188
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 190
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 191
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 192
    if-nez v4, :cond_1

    .line 193
    const v4, 0x7f0b0073

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 195
    :cond_1
    new-instance v19, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct/range {v19 .. v19}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 196
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 197
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 198
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    .line 199
    move-object/from16 v0, v19

    iput v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 200
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 201
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->f:Ljava/lang/String;

    .line 202
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 203
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    .line 204
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 205
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 206
    const/4 v4, 0x1

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 208
    move-object/from16 v0, v19

    iget-boolean v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v20, "LOCAL"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 210
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 203
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 214
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 215
    move-object/from16 v0, v16

    iput-object v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 216
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_5

    .line 220
    move-object/from16 v0, v17

    iput-object v7, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 221
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_5
    return-object v5
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/w;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/w;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/w;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/w;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a()V
    .locals 10

    .prologue
    .line 148
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->i:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 162
    :cond_0
    return-void

    .line 151
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/w;->i:[J

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 152
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 153
    if-eqz v0, :cond_2

    iget-object v7, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    .line 154
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 155
    iget-wide v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    cmp-long v8, v4, v8

    if-nez v8, :cond_3

    .line 156
    const/4 v8, 0x1

    iput-boolean v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    goto :goto_1

    .line 151
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Long;Z)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 452
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    const-string v3, "My calendar"

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 455
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->d:Lcom/android/calendar/ag;

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-wide/16 v6, 0x0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 456
    return-void

    .line 455
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 354
    if-eqz p1, :cond_4

    .line 355
    const-string v0, "key_dialog_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/w;->b:Z

    .line 356
    const-string v0, "key_group_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    .line 357
    const-string v0, "key_checked_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->i:[J

    .line 358
    iput-boolean v2, p0, Lcom/android/calendar/selectcalendars/w;->a:Z

    .line 359
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->i:[J

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 360
    iget-object v4, p0, Lcom/android/calendar/selectcalendars/w;->i:[J

    array-length v5, v4

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_4

    aget-wide v6, v4, v3

    .line 361
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 362
    if-eqz v0, :cond_2

    iget-object v9, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v9, :cond_2

    .line 363
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 364
    iget-wide v10, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    cmp-long v10, v6, v10

    if-nez v10, :cond_1

    .line 365
    iput-boolean v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    goto :goto_1

    :cond_2
    move v0, v1

    .line 375
    :goto_2
    return v0

    .line 360
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 375
    goto :goto_2
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const v3, 0x7f0f0143

    .line 388
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 392
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 393
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 394
    if-eqz v0, :cond_0

    .line 399
    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 400
    const v0, 0x7f0f0357

    .line 406
    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 407
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 408
    new-instance v0, Lcom/android/calendar/selectcalendars/aa;

    invoke-direct {v0, p0}, Lcom/android/calendar/selectcalendars/aa;-><init>(Lcom/android/calendar/selectcalendars/w;)V

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 415
    const/high16 v0, 0x1040000

    new-instance v2, Lcom/android/calendar/selectcalendars/ab;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/ab;-><init>(Lcom/android/calendar/selectcalendars/w;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 422
    new-instance v0, Lcom/android/calendar/selectcalendars/ac;

    invoke-direct {v0, p0}, Lcom/android/calendar/selectcalendars/ac;-><init>(Lcom/android/calendar/selectcalendars/w;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 430
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->j:Landroid/app/AlertDialog;

    goto :goto_0

    .line 401
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v2}, Lcom/android/calendar/selectcalendars/s;->e()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 402
    const v0, 0x7f0f0356

    goto :goto_1

    .line 404
    :cond_3
    const v0, 0x7f0f0358

    goto :goto_1
.end method

.method private c()V
    .locals 8

    .prologue
    .line 434
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->d()Ljava/util/Set;

    move-result-object v0

    .line 435
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 437
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 438
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 441
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 442
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/android/calendar/selectcalendars/w;->a(Ljava/lang/Long;Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 445
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/w;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->a()V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/w;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private d()Ljava/util/Set;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/s;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private e()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/s;->a()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 486
    :cond_0
    :goto_0
    return v0

    .line 481
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    if-eqz v1, :cond_0

    .line 482
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/s;->f()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/selectcalendars/w;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->f:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/selectcalendars/w;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->c()V

    return-void
.end method

.method static synthetic g(Lcom/android/calendar/selectcalendars/w;)I
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->e()I

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/android/calendar/selectcalendars/w;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->l:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/selectcalendars/w;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->j:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/selectcalendars/w;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->b()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 94
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/w;->setHasOptionsMenu(Z)V

    .line 95
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    .line 96
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/selectcalendars/w;->e:Z

    .line 97
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->m:Landroid/view/LayoutInflater;

    .line 98
    new-instance v0, Lcom/android/calendar/selectcalendars/x;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/selectcalendars/x;-><init>(Lcom/android/calendar/selectcalendars/w;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/w;->d:Lcom/android/calendar/ag;

    .line 145
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-object v2

    .line 259
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/w;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 261
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    invoke-static {v3}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 262
    const-string v5, "deleted!=1"

    .line 267
    :goto_2
    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->d:Lcom/android/calendar/ag;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    const-string v7, "calendar_displayName"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 274
    const v1, 0x7f0f0143

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 275
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/selectcalendars/w;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    .line 276
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 277
    const v1, 0x104000a

    new-instance v3, Lcom/android/calendar/selectcalendars/y;

    invoke-direct {v3, p0}, Lcom/android/calendar/selectcalendars/y;-><init>(Lcom/android/calendar/selectcalendars/w;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 283
    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 284
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0

    .line 259
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 264
    :cond_3
    const-string v5, "deleted!=1 and account_type!=\'com.osp.app.signin\'"

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    :goto_0
    return-object v2

    .line 232
    :cond_0
    invoke-direct {p0, p3}, Lcom/android/calendar/selectcalendars/w;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 234
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/w;->c:Landroid/app/Activity;

    invoke-static {v3}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 235
    const-string v5, "deleted!=1"

    .line 240
    :goto_2
    if-eqz v0, :cond_1

    .line 241
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->d:Lcom/android/calendar/ag;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    const-string v7, "calendar_displayName"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/w;->b:Z

    if-eqz v0, :cond_4

    .line 247
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 232
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 237
    :cond_3
    const-string v5, "deleted!=1 and account_type!=\'com.osp.app.signin\'"

    goto :goto_2

    .line 250
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/w;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v2

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 380
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 381
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/s;->b()I

    .line 382
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ad;->i()V

    .line 383
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/ad;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/ad;->d(Z)V

    .line 384
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/w;->k:Lcom/android/calendar/selectcalendars/ad;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ad;->g()V

    .line 385
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 330
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 331
    const-string v0, "key_group_state"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/w;->g:Lcom/android/calendar/selectcalendars/s;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/s;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 332
    const-string v0, "key_dialog_state"

    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/w;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 334
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/w;->d()Ljava/util/Set;

    move-result-object v1

    .line 335
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 337
    const/4 v0, 0x0

    .line 339
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 340
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 341
    const/4 v0, 0x0

    move v1, v0

    .line 342
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 344
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v2, v6, v8

    if-eqz v2, :cond_2

    .line 345
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    move v0, v2

    :goto_1
    move v1, v0

    .line 347
    goto :goto_0

    :cond_0
    move-object v0, v3

    .line 350
    :cond_1
    const-string v1, "key_checked_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 351
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
