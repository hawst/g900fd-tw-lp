.class public Lcom/android/calendar/selectcalendars/bl;
.super Landroid/widget/BaseAdapter;
.source "SelectCalendarsSimpleAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Ljava/util/ArrayList;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/android/calendar/selectcalendars/bl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/bl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 55
    iput p2, p0, Lcom/android/calendar/selectcalendars/bl;->c:I

    .line 56
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->b:Landroid/view/LayoutInflater;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 58
    const v1, 0x7f0f001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->e:Ljava/lang/String;

    .line 59
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 127
    const-string v0, "LOCAL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const v0, 0x7f020005

    .line 139
    :goto_0
    return v0

    .line 129
    :cond_0
    const-string v0, "com.google"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    const v0, 0x7f020002

    goto :goto_0

    .line 131
    :cond_1
    const-string v0, "com.android.exchange"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    const/high16 v0, 0x7f020000

    goto :goto_0

    .line 133
    :cond_2
    const-string v0, "com.sec.android.app.sns3.facebook"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.sec.android.app.snsaccountfacebook.account_type"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    :cond_3
    const v0, 0x7f020001

    goto :goto_0

    .line 136
    :cond_4
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 137
    const v0, 0x7f020006

    goto :goto_0

    .line 139
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;Lcom/android/calendar/selectcalendars/bm;)V
    .locals 8

    .prologue
    const v7, 0x7f0f02bf

    const/4 v6, 0x0

    .line 85
    iget-object v1, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 86
    iget-object v2, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 87
    iget-object v3, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 88
    iget-boolean v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 91
    iget v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 92
    const v5, 0x7f120018

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-static {p3, v5}, Lcom/android/calendar/selectcalendars/bm;->a(Lcom/android/calendar/selectcalendars/bm;Landroid/view/View;)Landroid/view/View;

    .line 93
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->b(Lcom/android/calendar/selectcalendars/bm;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 96
    const v0, 0x7f12001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p3, v0}, Lcom/android/calendar/selectcalendars/bm;->a(Lcom/android/calendar/selectcalendars/bm;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 97
    const v0, 0x7f12001b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p3, v0}, Lcom/android/calendar/selectcalendars/bm;->b(Lcom/android/calendar/selectcalendars/bm;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 98
    if-eqz v1, :cond_1

    .line 99
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 100
    const-string v5, "My calendar"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 101
    const v1, 0x7f0f02ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 108
    :cond_0
    :goto_0
    const v0, 0x7f12001c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-static {p3, v0}, Lcom/android/calendar/selectcalendars/bm;->b(Lcom/android/calendar/selectcalendars/bm;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 109
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->c(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 110
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "com.osp.app.signin"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v0, :cond_5

    .line 112
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->d(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f042b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    :cond_1
    :goto_1
    invoke-static {v3}, Lcom/android/calendar/selectcalendars/bl;->a(Ljava/lang/String;)I

    move-result v0

    .line 118
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->d(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 120
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->e(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    const-string v0, "LOCAL"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 122
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->e(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bl;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :cond_2
    return-void

    .line 102
    :cond_3
    const-string v5, "My Task"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 103
    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 104
    :cond_4
    const-string v5, "My calendars (personal)"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 105
    const v1, 0x7f0f02bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 114
    :cond_5
    invoke-static {p3}, Lcom/android/calendar/selectcalendars/bm;->d(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/bl;->d:Ljava/util/ArrayList;

    .line 182
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bl;->notifyDataSetChanged()V

    .line 183
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 147
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bl;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 153
    const/4 v0, 0x0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bl;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 161
    const-wide/16 v0, 0x0

    .line 163
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-wide v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 64
    .line 65
    if-nez p2, :cond_1

    .line 66
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bl;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/android/calendar/selectcalendars/bl;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 67
    new-instance v0, Lcom/android/calendar/selectcalendars/bm;

    invoke-direct {v0}, Lcom/android/calendar/selectcalendars/bm;-><init>()V

    .line 68
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v0

    .line 74
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/selectcalendars/bl;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    const v1, 0x7f12001c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/bm;->a(Lcom/android/calendar/selectcalendars/bm;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 77
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/bm;->a(Lcom/android/calendar/selectcalendars/bm;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-boolean v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 79
    invoke-direct {p0, v0, p2, v2}, Lcom/android/calendar/selectcalendars/bl;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;Lcom/android/calendar/selectcalendars/bm;)V

    .line 81
    :cond_0
    return-object p2

    .line 71
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/bm;

    move-object v2, v0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x1

    return v0
.end method
