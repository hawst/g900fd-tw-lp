.class public Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;
.super Lcom/android/calendar/b;
.source "DeleteCalendarActivity.java"


# instance fields
.field private b:Lcom/android/calendar/selectcalendars/w;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 105
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->invalidateOptionsMenu()V

    .line 106
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const v8, 0x7f12006b

    const/4 v3, 0x0

    .line 39
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 52
    :cond_0
    const/4 v0, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 54
    if-eqz v4, :cond_7

    .line 55
    const-string v0, "Groups"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 56
    if-eqz v2, :cond_5

    .line 57
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v3

    .line 59
    :goto_0
    if-ge v1, v5, :cond_1

    .line 60
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "childs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 59
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 64
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 65
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 66
    iget-object v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 67
    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 68
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 69
    iput-boolean v3, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 70
    invoke-static {v1, p0}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/app/Activity;)Z

    move-result v6

    iput-boolean v6, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    .line 71
    iget-boolean v1, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->l:Z

    if-nez v1, :cond_3

    .line 72
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 75
    :cond_4
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_5
    move-object v1, v2

    .line 82
    :goto_3
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->setContentView(I)V

    .line 83
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/w;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->b:Lcom/android/calendar/selectcalendars/w;

    .line 85
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->b:Lcom/android/calendar/selectcalendars/w;

    if-nez v0, :cond_6

    .line 86
    new-instance v0, Lcom/android/calendar/selectcalendars/w;

    invoke-direct {v0, v3, v1}, Lcom/android/calendar/selectcalendars/w;-><init>(ZLjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->b:Lcom/android/calendar/selectcalendars/w;

    .line 87
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->b:Lcom/android/calendar/selectcalendars/w;

    invoke-virtual {v0, v8, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 89
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->b:Lcom/android/calendar/selectcalendars/w;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 90
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 92
    :cond_6
    return-void

    :cond_7
    move-object v1, v0

    goto :goto_3
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/android/calendar/b;->onResume()V

    .line 97
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;->finish()V

    .line 100
    :cond_0
    return-void
.end method
