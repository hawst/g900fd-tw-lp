.class Lcom/android/calendar/selectcalendars/at;
.super Ljava/lang/Object;
.source "ExpandableSelectCalendarActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;Landroid/widget/CheckBox;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/at;->c:Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;

    iput-object p2, p0, Lcom/android/calendar/selectcalendars/at;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/android/calendar/selectcalendars/at;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/at;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/at;->b:Landroid/content/Context;

    const-string v1, "preferences_confirm_add_Samsung_account"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 149
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/at;->c:Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 154
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/at;->b:Landroid/content/Context;

    const-string v1, "preferences_confirm_add_Samsung_account"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
