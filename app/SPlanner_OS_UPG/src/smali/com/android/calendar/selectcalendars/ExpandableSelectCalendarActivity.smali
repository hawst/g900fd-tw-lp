.class public Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;
.super Lcom/android/calendar/b;
.source "ExpandableSelectCalendarActivity.java"


# instance fields
.field private b:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 108
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 109
    const-string v5, "com.osp.app.signin"

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 110
    const/4 v0, 0x1

    .line 113
    :cond_0
    return v0

    .line 108
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 74
    const-string v0, "preferences_confirm_add_Samsung_account"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 76
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 78
    new-instance v1, Lcom/android/calendar/selectcalendars/ap;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/ap;-><init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 85
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-static {p0}, Lcom/android/calendar/hj;->G(Landroid/content/Context;)Z

    move-result v1

    .line 89
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.samsung.android.scloud.proxy.calendar"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.osp.app.signin"

    const-string v5, "com.osp.app.signin.AccountView"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 99
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 101
    if-nez v1, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0

    .line 92
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 117
    .line 118
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 119
    const v1, 0x7f04003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 120
    const v1, 0x7f12007b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 121
    const v2, 0x7f120143

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 123
    const v3, 0x7f12007e

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 126
    new-instance v4, Lcom/android/calendar/selectcalendars/aq;

    invoke-direct {v4, p0, v3}, Lcom/android/calendar/selectcalendars/aq;-><init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v2, 0x7f0f0392

    invoke-virtual {p0, v2}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 135
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0391

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/selectcalendars/at;

    invoke-direct {v2, p0, v3, p1}, Lcom/android/calendar/selectcalendars/at;-><init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/selectcalendars/as;

    invoke-direct {v2, p0, v3, p1}, Lcom/android/calendar/selectcalendars/as;-><init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/selectcalendars/ar;

    invoke-direct {v1, p0, v3, p1}, Lcom/android/calendar/selectcalendars/ar;-><init>(Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 182
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 185
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b:Landroid/app/Fragment;

    .line 186
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b:Landroid/app/Fragment;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Lcom/android/calendar/selectcalendars/ba;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/selectcalendars/ba;-><init>(Z)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b:Landroid/app/Fragment;

    .line 188
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b:Landroid/app/Fragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 190
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 191
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 193
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->setContentView(I)V

    .line 65
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 69
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->b()V

    .line 70
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ExpandableSelectCalendarActivity;->c()V

    .line 71
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 197
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 198
    const/4 v0, 0x1

    .line 200
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/calendar/b;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
