.class Lcom/android/calendar/selectcalendars/i;
.super Lcom/android/calendar/ag;
.source "AddCalendarFragment.java"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/android/calendar/selectcalendars/d;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/d;Landroid/content/Context;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    iput-object p3, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 301
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;I)V

    .line 302
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 308
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    const-string v1, "key_local_add_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 307
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    const-string v1, "key_requery_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    .line 217
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 218
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->b(Lcom/android/calendar/selectcalendars/d;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->b(Lcom/android/calendar/selectcalendars/d;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 222
    sget-object v0, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 227
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 229
    :pswitch_0
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v1

    if-nez v1, :cond_3

    .line 230
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    const v3, 0x7f0f0053

    invoke-virtual {v2, v3}, Lcom/android/calendar/selectcalendars/d;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 255
    :goto_1
    if-eqz p3, :cond_2

    .line 256
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->b(Lcom/android/calendar/selectcalendars/d;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->b(Lcom/android/calendar/selectcalendars/d;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 261
    :catch_1
    move-exception v0

    .line 262
    sget-object v0, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    const-string v1, "Fail to dismiss DialogFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 232
    :cond_3
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 233
    const-string v0, "name"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "calendar_displayName"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v0, "calendar_color"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/d;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    const-string v0, "account_type"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->d(Lcom/android/calendar/selectcalendars/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 238
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->e(Lcom/android/calendar/selectcalendars/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 239
    const-string v0, "calendar_color_index"

    invoke-static {}, Lcom/android/calendar/selectcalendars/d;->g()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 240
    invoke-static {}, Lcom/android/calendar/selectcalendars/i;->a()I

    move-result v2

    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->d(Lcom/android/calendar/selectcalendars/d;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v4}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v4

    iget-object v4, v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v1

    iget-wide v6, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/selectcalendars/i;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 242
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->f(Lcom/android/calendar/selectcalendars/d;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 243
    invoke-static {}, Lcom/android/calendar/selectcalendars/i;->a()I

    move-result v2

    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->d(Lcom/android/calendar/selectcalendars/d;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v4}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v4

    iget-object v4, v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v1

    iget-wide v6, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/selectcalendars/i;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 246
    :cond_5
    invoke-static {}, Lcom/android/calendar/selectcalendars/i;->a()I

    move-result v2

    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v1

    iget-wide v6, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/selectcalendars/i;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 249
    :cond_6
    const-string v0, "LOCAL"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->d(Lcom/android/calendar/selectcalendars/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 250
    const-string v0, "account_name"

    const-string v1, "My calendar"

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_7
    invoke-static {}, Lcom/android/calendar/selectcalendars/i;->a()I

    move-result v2

    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v1, p0

    move-wide v6, v8

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/selectcalendars/i;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V

    goto/16 :goto_1

    .line 267
    :pswitch_1
    if-eqz p3, :cond_8

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->g(Lcom/android/calendar/selectcalendars/d;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_9

    .line 268
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/d;->f()V

    .line 278
    :goto_2
    if-eqz p3, :cond_0

    .line 279
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 271
    :cond_9
    const/4 v1, -0x1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 272
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    new-array v2, v2, [I

    invoke-static {v1, v2}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;[I)[I

    .line 273
    :goto_3
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 274
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->h(Lcom/android/calendar/selectcalendars/d;)[I

    move-result-object v2

    add-int/lit8 v1, v0, 0x1

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    move v0, v1

    goto :goto_3

    .line 276
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/i;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/d;->g(Lcom/android/calendar/selectcalendars/d;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;Landroid/view/View;)V

    goto :goto_2

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(ILjava/lang/Object;Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 290
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;)V

    .line 291
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    const-string v1, "key_local_add_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 296
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/i;->a:Landroid/app/Activity;

    const-string v1, "key_requery_state"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method
