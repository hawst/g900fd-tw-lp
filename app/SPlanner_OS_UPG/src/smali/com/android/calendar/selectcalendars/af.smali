.class Lcom/android/calendar/selectcalendars/af;
.super Ljava/lang/Object;
.source "ExpandableCalendarsListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:I

.field final synthetic c:Lcom/android/calendar/selectcalendars/ae;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/ae;Landroid/view/ViewGroup;I)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/af;->c:Lcom/android/calendar/selectcalendars/ae;

    iput-object p2, p0, Lcom/android/calendar/selectcalendars/af;->a:Landroid/view/ViewGroup;

    iput p3, p0, Lcom/android/calendar/selectcalendars/af;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/af;->a:Landroid/view/ViewGroup;

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    .line 163
    iget v1, p0, Lcom/android/calendar/selectcalendars/af;->b:I

    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->getFlatListPosition(J)I

    move-result v1

    .line 164
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->getSelectedId()J

    move-result-wide v2

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 165
    return-void
.end method
