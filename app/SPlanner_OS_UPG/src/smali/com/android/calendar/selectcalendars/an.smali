.class Lcom/android/calendar/selectcalendars/an;
.super Lcom/android/calendar/ag;
.source "ExpandableCalendarsListFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/aj;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/aj;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-direct {p0, p2}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 175
    if-nez p3, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/aj;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 179
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 184
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 186
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0, p3}, Lcom/android/calendar/selectcalendars/aj;->a(Lcom/android/calendar/selectcalendars/aj;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    .line 187
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/aj;->c(Lcom/android/calendar/selectcalendars/aj;)Lcom/android/calendar/selectcalendars/ae;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v3, v0}, Lcom/android/calendar/selectcalendars/aj;->a(Lcom/android/calendar/selectcalendars/aj;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/calendar/selectcalendars/ae;->a(Ljava/util/ArrayList;)V

    .line 188
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/aj;->d(Lcom/android/calendar/selectcalendars/aj;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 189
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 191
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/aj;->b(Lcom/android/calendar/selectcalendars/aj;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 192
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/aj;->c(Lcom/android/calendar/selectcalendars/aj;)Lcom/android/calendar/selectcalendars/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ae;->getGroupCount()I

    move-result v2

    move v0, v1

    .line 193
    :goto_1
    if-ge v0, v2, :cond_3

    .line 194
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v3}, Lcom/android/calendar/selectcalendars/aj;->b(Lcom/android/calendar/selectcalendars/aj;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/aj;->b(Lcom/android/calendar/selectcalendars/aj;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/an;->a:Lcom/android/calendar/selectcalendars/aj;

    invoke-static {v0, v1}, Lcom/android/calendar/selectcalendars/aj;->a(Lcom/android/calendar/selectcalendars/aj;Z)Z

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
