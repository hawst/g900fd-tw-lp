.class Lcom/android/calendar/selectcalendars/be;
.super Ljava/lang/Object;
.source "ExpandableSelectCalendarsFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/ba;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/ba;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Lcom/sec/android/touchwiz/widget/TwExpandableListView;Landroid/view/View;IJ)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 247
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->collapseGroup(I)Z

    .line 257
    :goto_0
    new-instance v0, Lcom/android/calendar/selectcalendars/bf;

    invoke-direct {v0, p0, p2}, Lcom/android/calendar/selectcalendars/bf;-><init>(Lcom/android/calendar/selectcalendars/be;Landroid/view/View;)V

    const-wide/16 v2, 0x32

    invoke-virtual {p2, v0, v2, v3}, Landroid/view/View;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 263
    return v4

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 251
    if-nez p3, :cond_1

    .line 252
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setTranscriptMode(I)V

    goto :goto_0

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/be;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setTranscriptMode(I)V

    goto :goto_0
.end method
