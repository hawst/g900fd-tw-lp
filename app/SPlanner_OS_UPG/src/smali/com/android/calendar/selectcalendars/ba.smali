.class public Lcom/android/calendar/selectcalendars/ba;
.super Landroid/app/DialogFragment;
.source "ExpandableSelectCalendarsFragment.java"


# static fields
.field protected static final a:Ljava/lang/String;


# instance fields
.field protected b:Lcom/android/calendar/selectcalendars/au;

.field protected c:Landroid/app/Activity;

.field private d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

.field private e:Landroid/content/AsyncQueryHandler;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:Z

.field private j:I

.field private final k:Landroid/database/ContentObserver;

.field private l:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcom/android/calendar/selectcalendars/ba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/ba;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/ba;-><init>(Z)V

    .line 127
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 129
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 87
    iput v0, p0, Lcom/android/calendar/selectcalendars/ba;->j:I

    .line 105
    new-instance v1, Lcom/android/calendar/selectcalendars/bb;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/selectcalendars/bb;-><init>(Lcom/android/calendar/selectcalendars/ba;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->k:Landroid/database/ContentObserver;

    .line 117
    new-instance v1, Lcom/android/calendar/selectcalendars/bc;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/bc;-><init>(Lcom/android/calendar/selectcalendars/ba;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->l:Landroid/content/BroadcastReceiver;

    .line 130
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/ba;->f:Z

    .line 131
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/ba;->setHasOptionsMenu(Z)V

    .line 132
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/android/calendar/selectcalendars/ba;->j:I

    return p1
.end method

.method private a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 18

    .prologue
    .line 466
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 467
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 468
    const-string v2, "calendar_displayName"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 469
    const-string v2, "name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 470
    const-string v2, "calendar_color"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 471
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 472
    const-string v2, "ownerAccount"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 473
    const-string v2, "visible"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 474
    const-string v2, "account_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 475
    const-string v2, "sync_events"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 476
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    .line 477
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 478
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 479
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 480
    if-nez v2, :cond_1

    .line 481
    const v2, 0x7f0b0073

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 483
    :cond_1
    new-instance v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v14}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 484
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 485
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 486
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    .line 487
    iput v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 488
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 489
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->f:Ljava/lang/String;

    .line 490
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 491
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    .line 492
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 493
    const/4 v2, 0x0

    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 494
    const/4 v2, 0x1

    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 495
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v2

    const-string v15, "JAPAN"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 496
    const-string v2, "com.android.nttdocomo"

    iget-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 497
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    :cond_2
    :goto_3
    const-string v2, "My calendar"

    iget-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v15, "LOCAL"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 506
    iget v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/calendar/selectcalendars/ba;->h:I

    .line 507
    iget-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/calendar/selectcalendars/ba;->i:Z

    goto/16 :goto_0

    .line 490
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 491
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 500
    :cond_5
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 511
    :cond_6
    return-object v3
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/selectcalendars/ba;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 549
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 550
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 551
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 552
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 553
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "LOCAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 554
    new-instance v4, Lcom/android/calendar/selectcalendars/bh;

    invoke-direct {v4, v6}, Lcom/android/calendar/selectcalendars/bh;-><init>(Lcom/android/calendar/selectcalendars/bb;)V

    .line 555
    iget-object v5, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/calendar/selectcalendars/bh;->a:Ljava/lang/String;

    .line 556
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    iput-object v0, v4, Lcom/android/calendar/selectcalendars/bh;->b:Ljava/lang/String;

    .line 558
    invoke-static {v2, v4}, Lcom/android/calendar/selectcalendars/ba;->a(Ljava/util/Set;Lcom/android/calendar/selectcalendars/bh;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 563
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 564
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "LOCAL"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 565
    new-instance v4, Lcom/android/calendar/selectcalendars/bh;

    invoke-direct {v4, v6}, Lcom/android/calendar/selectcalendars/bh;-><init>(Lcom/android/calendar/selectcalendars/bb;)V

    .line 566
    iget-object v5, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/calendar/selectcalendars/bh;->a:Ljava/lang/String;

    .line 567
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    iput-object v0, v4, Lcom/android/calendar/selectcalendars/bh;->b:Ljava/lang/String;

    .line 569
    invoke-static {v2, v4}, Lcom/android/calendar/selectcalendars/ba;->a(Ljava/util/Set;Lcom/android/calendar/selectcalendars/bh;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 570
    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 575
    :cond_3
    invoke-direct {p0, v1, p1, p2}, Lcom/android/calendar/selectcalendars/ba;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 580
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/bh;

    .line 581
    new-instance v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v3}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 582
    iget-object v4, v0, Lcom/android/calendar/selectcalendars/bh;->a:Ljava/lang/String;

    .line 583
    iget-object v5, v0, Lcom/android/calendar/selectcalendars/bh;->b:Ljava/lang/String;

    .line 585
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/bh;->a:Ljava/lang/String;

    iput-object v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 586
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 587
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 588
    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 589
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 592
    :cond_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 593
    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 594
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 597
    :cond_7
    const/4 v0, 0x2

    iput v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 598
    iput-object v6, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 599
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 602
    :cond_8
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->d()Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    move-result-object v0

    .line 603
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 604
    return-object v1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 161
    if-eqz p1, :cond_2

    .line 162
    const-string v0, "key_group_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 163
    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->a(Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->b()V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v1, v0}, Lcom/android/calendar/selectcalendars/au;->a(Ljava/util/ArrayList;)V

    .line 167
    const-string v0, "key_expand_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 168
    if-eqz v0, :cond_0

    .line 169
    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/ba;->a([Z)V

    goto :goto_0

    .line 173
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->b()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->b()V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/ba;->b(Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    .line 608
    new-instance v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v1}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 609
    new-instance v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v2}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 610
    const-string v0, "My calendar"

    iput-object v0, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 611
    const/4 v0, 0x0

    iput v0, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 612
    const-string v0, "My Task"

    iput-object v0, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 613
    const/4 v0, 0x1

    iput v0, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 614
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 615
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 617
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 618
    iget-object v6, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v7, "LOCAL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 619
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 623
    :cond_1
    iput-object v3, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 624
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 626
    iget-object v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v5, "LOCAL"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 627
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 630
    :cond_3
    iput-object v4, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 631
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    return-void
.end method

.method private a([Z)V
    .locals 2

    .prologue
    .line 340
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 341
    aget-boolean v1, p1, v0

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 340
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->collapseGroup(I)Z

    goto :goto_1

    .line 347
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ba;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/ba;->g:Z

    return p1
.end method

.method private static a(Ljava/util/ArrayList;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static a(Ljava/util/Set;Lcom/android/calendar/selectcalendars/bh;)Z
    .locals 3

    .prologue
    .line 538
    const/4 v1, 0x0

    .line 539
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/bh;

    .line 540
    invoke-virtual {v0, p1}, Lcom/android/calendar/selectcalendars/bh;->a(Lcom/android/calendar/selectcalendars/bh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    const/4 v0, 0x1

    .line 545
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 515
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 516
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 517
    const-string v0, "displayName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 518
    const-string v0, "_sync_account_key"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 519
    const-string v0, "_sync_account"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 520
    const-string v0, "selected"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 521
    const-string v0, "_sync_account_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 522
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    new-instance v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v9}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 524
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 525
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 526
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v0, v10}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 527
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 528
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 529
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 530
    iput-boolean v1, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 531
    iput-boolean v1, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 532
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 528
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 534
    :cond_1
    return-object v2
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/ba;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/ba;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 184
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const-string v5, "deleted!=1"

    .line 189
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->e:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    const-string v7, "calendar_displayName"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-void

    .line 187
    :cond_0
    const-string v5, "deleted!=1 and account_type!=\'com.osp.app.signin\'"

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 652
    const/4 v0, 0x1

    .line 653
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 654
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 655
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v0, :cond_0

    .line 656
    const/4 v0, 0x0

    :goto_1
    move v1, v0

    .line 660
    goto :goto_0

    .line 661
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/au;->a(Z)V

    .line 662
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/ba;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->e:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method private c()[Z
    .locals 4

    .prologue
    .line 359
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/au;->getGroupCount()I

    move-result v1

    .line 360
    new-array v2, v1, [Z

    .line 361
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 362
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v3, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->isGroupExpanded(I)Z

    move-result v3

    aput-boolean v3, v2, v0

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 364
    :cond_0
    return-object v2
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/ba;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/android/calendar/selectcalendars/ba;->j:I

    return v0
.end method

.method private d()Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 635
    new-instance v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    invoke-direct {v1}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;-><init>()V

    .line 636
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 637
    const/4 v2, 0x3

    iput v2, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    .line 638
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 639
    new-instance v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v3}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 640
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f009b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 641
    iget v4, p0, Lcom/android/calendar/selectcalendars/ba;->h:I

    iput v4, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 642
    iput-boolean v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 643
    const-string v4, "contact_birthday"

    iput-object v4, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 644
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calendar/hj;->h(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 645
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->i:Z

    iput-boolean v0, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 646
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    iput-object v2, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 648
    return-object v1
.end method


# virtual methods
.method protected a(Landroid/app/Activity;)Landroid/view/View;
    .locals 3

    .prologue
    .line 222
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 224
    const v1, 0x7f040086

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 225
    new-instance v1, Lcom/android/calendar/selectcalendars/au;

    invoke-direct {v1, p1}, Lcom/android/calendar/selectcalendars/au;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    .line 226
    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/view/View;)V

    .line 227
    return-object v0
.end method

.method a()V
    .locals 14

    .prologue
    const/4 v9, 0x3

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 295
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/au;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 296
    if-nez v0, :cond_1

    .line 337
    :cond_0
    return-void

    .line 299
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 300
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 303
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-ne v1, v9, :cond_5

    .line 304
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 305
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "preferences_hide_contacts_events"

    iget-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v1, :cond_3

    move v1, v7

    :goto_1
    invoke-static {v4, v5, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 307
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v4, "preferences_hide_contacts_events"

    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v0, :cond_4

    move v0, v7

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v0}, Lcom/android/calendar/preference/GeneralPreferences;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v8

    .line 305
    goto :goto_1

    :cond_4
    move v0, v8

    .line 307
    goto :goto_2

    .line 312
    :cond_5
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 315
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 316
    iget-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v1, :cond_6

    move v1, v7

    .line 317
    :goto_4
    iget-boolean v3, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v3, :cond_7

    .line 318
    const/4 v3, 0x4

    .line 319
    sget-object v5, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    iget-wide v12, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-static {v5, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 320
    const-string v5, "selected"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move v1, v3

    move-object v3, v0

    .line 333
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->e:Landroid/content/AsyncQueryHandler;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move v1, v8

    .line 316
    goto :goto_4

    .line 323
    :cond_7
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-static {v3, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 324
    const-string v5, "visible"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 329
    iget-boolean v5, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    if-nez v5, :cond_8

    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v0, :cond_8

    .line 330
    const-string v0, "sync_events"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_8
    move v1, v9

    goto :goto_5
.end method

.method protected a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    const v0, 0x7f1200df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    .line 232
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/au;->a(Landroid/view/ViewGroup;)V

    .line 233
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/au;->b(Landroid/view/ViewGroup;)V

    .line 235
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    iget-object v1, v1, Lcom/android/calendar/selectcalendars/au;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->addHeaderView(Landroid/view/View;)V

    .line 236
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setItemsCanFocus(Z)V

    .line 237
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorGravity(I)V

    .line 239
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorPaddings(II)V

    .line 243
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 244
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    new-instance v1, Lcom/android/calendar/selectcalendars/be;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/be;-><init>(Lcom/android/calendar/selectcalendars/ba;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setOnGroupClickListener(Lcom/sec/android/touchwiz/widget/TwExpandableListView$OnGroupClickListener;)V

    .line 266
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->d:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setIndicatorPaddings(II)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 137
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    .line 138
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 139
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 141
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x0

    .line 218
    :goto_0
    return-object v0

    .line 199
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "key_dialog_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    const-string v0, "key_dialog_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->f:Z

    .line 202
    :cond_1
    new-instance v0, Lcom/android/calendar/selectcalendars/bg;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/selectcalendars/bg;-><init>(Lcom/android/calendar/selectcalendars/ba;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->e:Landroid/content/AsyncQueryHandler;

    .line 203
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 204
    const v1, 0x7f0f001e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 205
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    .line 206
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 207
    const v1, 0x7f0f0187

    new-instance v2, Lcom/android/calendar/selectcalendars/bd;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/bd;-><init>(Lcom/android/calendar/selectcalendars/ba;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/os/Bundle;)V

    .line 218
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 383
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 384
    const v0, 0x7f11000d

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 385
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    .line 148
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->f:Z

    .line 151
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->f:Z

    if-eqz v0, :cond_2

    .line 152
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 154
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v0

    .line 155
    new-instance v1, Lcom/android/calendar/selectcalendars/bg;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/selectcalendars/bg;-><init>(Lcom/android/calendar/selectcalendars/ba;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->e:Landroid/content/AsyncQueryHandler;

    .line 156
    invoke-direct {p0, p3}, Lcom/android/calendar/selectcalendars/ba;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 376
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 377
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->a()V

    .line 378
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 379
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 389
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 400
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 391
    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    const-class v3, Lcom/android/calendar/selectcalendars/EditCalendarActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 393
    invoke-virtual {p0, v1}, Lcom/android/calendar/selectcalendars/ba;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 396
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->a()V

    .line 397
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 389
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f120323 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->a()V

    .line 370
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->k:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 371
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 372
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 282
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 283
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/ba;->k:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 287
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "key_requery_state"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->g:Z

    .line 288
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/ba;->g:Z

    if-eqz v0, :cond_1

    .line 289
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->b()V

    .line 290
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ba;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "key_requery_state"

    invoke-static {v0, v1, v4}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 292
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 351
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 352
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/ba;->c()[Z

    move-result-object v0

    .line 353
    const-string v1, "key_expand_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 354
    const-string v0, "key_group_state"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/au;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 355
    const-string v0, "key_dialog_state"

    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/ba;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 356
    return-void
.end method
