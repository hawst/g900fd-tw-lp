.class public Lcom/android/calendar/selectcalendars/ae;
.super Landroid/widget/BaseExpandableListAdapter;
.source "ExpandableCalendarsListAdapter.java"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Z

.field private f:Ljava/util/ArrayList;

.field private g:Landroid/content/Context;

.field private h:Z

.field private i:Landroid/os/Handler;

.field private j:Z

.field private k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 59
    iput-boolean v1, p0, Lcom/android/calendar/selectcalendars/ae;->e:Z

    .line 64
    iput-boolean v1, p0, Lcom/android/calendar/selectcalendars/ae;->h:Z

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->i:Landroid/os/Handler;

    .line 66
    iput-boolean v1, p0, Lcom/android/calendar/selectcalendars/ae;->j:Z

    .line 248
    new-instance v0, Lcom/android/calendar/selectcalendars/ah;

    invoke-direct {v0, p0}, Lcom/android/calendar/selectcalendars/ah;-><init>(Lcom/android/calendar/selectcalendars/ae;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->k:Ljava/lang/Runnable;

    .line 69
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ae;->g:Landroid/content/Context;

    .line 70
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->a:Landroid/view/LayoutInflater;

    .line 71
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/ae;->j:Z

    .line 73
    const v0, 0x7f0a000a

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/ae;->e:Z

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ae;->b:Landroid/graphics/drawable/Drawable;

    .line 76
    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/ae;->c:Landroid/graphics/drawable/Drawable;

    .line 77
    const v1, 0x7f02012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->d:Landroid/graphics/drawable/Drawable;

    .line 78
    return-void
.end method

.method private a(Landroid/widget/ImageView;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 341
    if-nez p1, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 345
    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 346
    const-string v1, "com.google"

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 347
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 353
    :goto_1
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 348
    :cond_2
    const-string v1, "com.android.exchange"

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 349
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 351
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;)V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const v11, 0x7f0f02c2

    const v10, 0x7f0f02bc

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 256
    iget-object v2, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 257
    iget-object v5, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 258
    iget-object v6, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 261
    iget v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    .line 262
    const v1, 0x7f120018

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 263
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 266
    const v0, 0x7f12001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 267
    const v1, 0x7f12001b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 268
    if-eqz v2, :cond_0

    .line 269
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 270
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 271
    const-string v7, "My calendar"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 272
    const v2, 0x7f0f02bd

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 273
    const/16 v7, 0x64

    if-ne v3, v7, :cond_4

    .line 274
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 300
    :cond_0
    :goto_0
    const-string v3, "contact_birthday"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 301
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v4, v2

    .line 304
    :goto_1
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 305
    const v2, 0x7f120019

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 306
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 307
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c03df

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 309
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    :cond_1
    const v2, 0x7f12001c

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 313
    invoke-virtual {v2, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 315
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    const-string v0, "LOCAL"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "contact_birthday"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 320
    :cond_2
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 325
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->g:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {p1, v0}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 326
    invoke-virtual {v2, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 328
    :cond_3
    return-void

    .line 275
    :cond_4
    const/16 v7, 0x65

    if-ne v3, v7, :cond_0

    .line 276
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX II"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 278
    :cond_5
    const-string v7, "My Task"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 279
    const v2, 0x7f0f02bf

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    const/16 v7, 0x64

    if-ne v3, v7, :cond_6

    .line 281
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 282
    :cond_6
    const/16 v7, 0x65

    if-ne v3, v7, :cond_0

    .line 283
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX II"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 285
    :cond_7
    const-string v3, "My calendars (personal)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 286
    const v2, 0x7f0f02bb

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 287
    :cond_8
    const-string v3, "My task (personal)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 288
    const v2, 0x7f0f02c1

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 289
    :cond_9
    const-string v3, "My calendars (KNOX)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 290
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 291
    :cond_a
    const-string v3, "My calendars (KNOX II)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 292
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX II"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 293
    :cond_b
    const-string v3, "My task (KNOX)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 294
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 295
    :cond_c
    const-string v3, "My task (KNOX II)"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 296
    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    const-string v4, "KNOX II"

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 322
    :cond_d
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_e
    move-object v4, v2

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ae;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/ae;->h:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/ae;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/ae;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/ae;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/ae;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->k:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/ae;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->i:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    .line 332
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/ae;->notifyDataSetChanged()V

    .line 333
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 130
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 209
    if-eqz p4, :cond_0

    instance-of v0, p4, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 213
    :cond_0
    if-eqz p4, :cond_1

    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->a:Landroid/view/LayoutInflater;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 220
    const v1, 0x7f120017

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 221
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 222
    new-instance v2, Lcom/android/calendar/selectcalendars/ag;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/selectcalendars/ag;-><init>(Lcom/android/calendar/selectcalendars/ae;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    invoke-direct {p0, v0, p4}, Lcom/android/calendar/selectcalendars/ae;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;)V

    .line 245
    return-object p4
.end method

.method public getChildrenCount(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    .line 108
    :goto_0
    return v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 105
    if-nez v0, :cond_1

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 125
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 141
    .line 144
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ae;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 146
    if-eqz p3, :cond_8

    instance-of v1, p3, Landroid/widget/LinearLayout;

    if-eqz v1, :cond_8

    .line 148
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/selectcalendars/ai;

    move-object v2, p3

    .line 151
    :goto_0
    if-nez v1, :cond_7

    .line 152
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ae;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f040089

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 153
    new-instance v1, Lcom/android/calendar/selectcalendars/ai;

    invoke-direct {v1, v4}, Lcom/android/calendar/selectcalendars/ai;-><init>(Lcom/android/calendar/selectcalendars/af;)V

    .line 154
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v2

    move-object v2, v1

    .line 157
    :goto_1
    const v1, 0x7f120289

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/ai;->a(Lcom/android/calendar/selectcalendars/ai;Landroid/view/View;)Landroid/view/View;

    .line 158
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->a(Lcom/android/calendar/selectcalendars/ai;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 159
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->a(Lcom/android/calendar/selectcalendars/ai;)Landroid/view/View;

    move-result-object v1

    new-instance v5, Lcom/android/calendar/selectcalendars/af;

    invoke-direct {v5, p0, p4, p1}, Lcom/android/calendar/selectcalendars/af;-><init>(Lcom/android/calendar/selectcalendars/ae;Landroid/view/ViewGroup;I)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const v1, 0x7f12028b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/ai;->a(Lcom/android/calendar/selectcalendars/ai;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 169
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->b(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 171
    invoke-virtual {v1}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 172
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->b(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    :cond_0
    const v1, 0x7f12028a

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/ai;->a(Lcom/android/calendar/selectcalendars/ai;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 176
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->c(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 177
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->c(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->c(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 182
    :cond_1
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_2

    .line 183
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    :goto_2
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->b(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    if-eqz p2, :cond_6

    .line 197
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->b(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0424

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 201
    :goto_3
    return-object v3

    .line 184
    :cond_2
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-nez v1, :cond_3

    .line 185
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 186
    :cond_3
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-ne v1, v6, :cond_4

    .line 187
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 188
    :cond_4
    const-string v1, "Task"

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 189
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 191
    :cond_5
    iget-object v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 192
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->c(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-direct {p0, v4, v0}, Lcom/android/calendar/selectcalendars/ae;->a(Landroid/widget/ImageView;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;)V

    move-object v0, v1

    goto :goto_2

    .line 199
    :cond_6
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/ai;->b(Lcom/android/calendar/selectcalendars/ai;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0427

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_7
    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_1

    :cond_8
    move-object v1, v4

    move-object v2, v4

    goto/16 :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    return v0
.end method
