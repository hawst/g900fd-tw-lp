.class public Lcom/android/calendar/selectcalendars/au;
.super Landroid/widget/BaseExpandableListAdapter;
.source "ExpandableSelectCalendarsAdapter.java"


# static fields
.field private static p:Ljava/lang/String;


# instance fields
.field protected a:Landroid/view/View;

.field protected b:Landroid/view/View;

.field private c:Ljava/util/ArrayList;

.field private d:Landroid/app/Activity;

.field private e:Landroid/content/Context;

.field private f:Landroid/view/LayoutInflater;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/widget/CheckBox;

.field private m:Landroid/view/accessibility/AccessibilityManager;

.field private n:Landroid/view/View;

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->m:Landroid/view/accessibility/AccessibilityManager;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/au;->o:Z

    move-object v0, p1

    .line 100
    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->d:Landroid/app/Activity;

    .line 101
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/au;->e:Landroid/content/Context;

    .line 102
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->f:Landroid/view/LayoutInflater;

    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/dz;->w(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/selectcalendars/au;->o:Z

    .line 106
    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/au;->g:Landroid/graphics/drawable/Drawable;

    .line 107
    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/au;->h:Landroid/graphics/drawable/Drawable;

    .line 108
    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/au;->i:Landroid/graphics/drawable/Drawable;

    .line 109
    const v1, 0x7f020006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/au;->j:Landroid/graphics/drawable/Drawable;

    .line 110
    const v1, 0x7f02012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->k:Landroid/graphics/drawable/Drawable;

    .line 111
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->m:Landroid/view/accessibility/AccessibilityManager;

    .line 113
    invoke-static {p1}, Lcom/android/calendar/hj;->I(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    .line 114
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/au;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 223
    if-nez p1, :cond_1

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 227
    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 228
    const-string v1, "com.google"

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 229
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    :goto_1
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 230
    :cond_2
    const-string v1, "com.sec.android.app.sns3.facebook"

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 231
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 232
    :cond_3
    const-string v1, "com.osp.app.signin"

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 233
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 234
    :cond_4
    const-string v1, "com.android.exchange"

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 235
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 237
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method static a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 324
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 325
    iget-object v4, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 326
    iget-object v5, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 327
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 331
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v0, :cond_3

    .line 332
    iget v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 336
    :goto_0
    const v1, 0x7f120018

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 337
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 340
    const v0, 0x7f12001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 341
    const v1, 0x7f12001b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 342
    if-eqz v2, :cond_0

    .line 343
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 344
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    .line 345
    const-string v8, "My calendar"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 346
    const v2, 0x7f0f02bd

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 347
    const/16 v8, 0x64

    if-ne v7, v8, :cond_5

    .line 348
    sget-object v2, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 349
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 390
    :cond_0
    :goto_1
    const-string v3, "contact_birthday"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 391
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 393
    :goto_2
    const v2, 0x7f12001c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 394
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 395
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    const-string v3, "com.android.exchange"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 398
    iget-boolean v3, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v3, :cond_11

    .line 399
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0428

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    :goto_3
    const-string v3, "LOCAL"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "contact_birthday"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 408
    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    :goto_4
    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 413
    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 414
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 415
    const v0, 0x7f120017

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 416
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 417
    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    if-nez v1, :cond_2

    .line 418
    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 419
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 421
    :cond_2
    return-void

    .line 334
    :cond_3
    iget v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    goto/16 :goto_0

    .line 351
    :cond_4
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 353
    :cond_5
    const/16 v8, 0x65

    if-ne v7, v8, :cond_0

    .line 354
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX II"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 356
    :cond_6
    const-string v8, "My Task"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 357
    const v2, 0x7f0f02bf

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 358
    const/16 v8, 0x64

    if-ne v7, v8, :cond_8

    .line 359
    sget-object v2, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 360
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 362
    :cond_7
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 364
    :cond_8
    const/16 v8, 0x65

    if-ne v7, v8, :cond_0

    .line 365
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX II"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 367
    :cond_9
    const-string v7, "My calendars (personal)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 368
    const v2, 0x7f0f02bb

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 369
    :cond_a
    const-string v7, "My task (personal)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 370
    const v2, 0x7f0f02c1

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 371
    :cond_b
    const-string v7, "My calendars (KNOX)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 372
    sget-object v2, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 373
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 375
    :cond_c
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 377
    :cond_d
    const-string v7, "My calendars (KNOX II)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 378
    const v2, 0x7f0f02bc

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX II"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 379
    :cond_e
    const-string v7, "My task (KNOX)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 380
    sget-object v2, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 381
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lcom/android/calendar/selectcalendars/au;->p:Ljava/lang/String;

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 383
    :cond_f
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 385
    :cond_10
    const-string v7, "My task (KNOX II)"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 386
    const v2, 0x7f0f02c2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "KNOX II"

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 401
    :cond_11
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f018c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 404
    :cond_12
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 410
    :cond_13
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_14
    move-object v3, v2

    goto/16 :goto_2
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/au;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->m:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method private c()Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 303
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/au;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/au;->c()Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 307
    const/4 v0, 0x1

    .line 308
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 309
    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 310
    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v0, :cond_0

    move v0, v2

    :goto_1
    move v1, v0

    .line 315
    goto :goto_0

    .line 316
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 317
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iput-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    .line 320
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/au;->notifyDataSetChanged()V

    .line 321
    return-void

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/au;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/au;->d()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/selectcalendars/au;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/selectcalendars/au;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 456
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04008a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->a:Landroid/view/View;

    .line 457
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->a:Landroid/view/View;

    const v1, 0x7f12028f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 458
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 459
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 461
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->a:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/selectcalendars/ax;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/ax;-><init>(Lcom/android/calendar/selectcalendars/au;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 429
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    .line 430
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/au;->notifyDataSetChanged()V

    .line 431
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 432
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/au;->d()V

    .line 434
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iput-boolean p1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->c:Z

    .line 438
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 441
    :cond_0
    return-void
.end method

.method protected b()V
    .locals 5

    .prologue
    .line 444
    const-string v0, "authorities"

    .line 445
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 446
    const-string v1, "authorities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.android.calendar"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 453
    :goto_0
    return-void

    .line 450
    :catch_0
    move-exception v0

    .line 451
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected b(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 472
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04008b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->b:Landroid/view/View;

    .line 473
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->b:Landroid/view/View;

    const v1, 0x7f120291

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->n:Landroid/view/View;

    .line 474
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->n:Landroid/view/View;

    const v1, 0x7f120292

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    .line 475
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->n:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 477
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->n:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/selectcalendars/ay;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/ay;-><init>(Lcom/android/calendar/selectcalendars/au;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 512
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 147
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 247
    if-eqz p4, :cond_0

    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->f:Landroid/view/LayoutInflater;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1, p5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 253
    const v1, 0x7f12001c

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 255
    iget-boolean v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 256
    invoke-virtual {p4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 258
    const v1, 0x7f120017

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 259
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 261
    new-instance v2, Lcom/android/calendar/selectcalendars/aw;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/selectcalendars/aw;-><init>(Lcom/android/calendar/selectcalendars/au;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 295
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->e:Landroid/content/Context;

    invoke-static {v0, p4, v1}, Lcom/android/calendar/selectcalendars/au;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/view/View;Landroid/content/Context;)V

    .line 296
    return-object p4
.end method

.method public getChildrenCount(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    .line 125
    :goto_0
    return v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    .line 122
    if-nez v0, :cond_1

    move v0, v1

    .line 123
    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 142
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 158
    .line 161
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/au;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    .line 162
    if-eqz p3, :cond_8

    instance-of v1, p3, Landroid/widget/LinearLayout;

    if-eqz v1, :cond_8

    .line 164
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/selectcalendars/az;

    move-object v2, p3

    .line 167
    :goto_0
    if-nez v1, :cond_7

    .line 168
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/au;->f:Landroid/view/LayoutInflater;

    const v2, 0x7f040089

    invoke-virtual {v1, v2, p4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 169
    new-instance v1, Lcom/android/calendar/selectcalendars/az;

    invoke-direct {v1, v4}, Lcom/android/calendar/selectcalendars/az;-><init>(Lcom/android/calendar/selectcalendars/av;)V

    .line 170
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v2

    move-object v2, v1

    .line 173
    :goto_1
    const v1, 0x7f120289

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;Landroid/view/View;)Landroid/view/View;

    .line 174
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 175
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 176
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;)Landroid/view/View;

    move-result-object v1

    new-instance v5, Lcom/android/calendar/selectcalendars/av;

    invoke-direct {v5, p0, p4, p1}, Lcom/android/calendar/selectcalendars/av;-><init>(Lcom/android/calendar/selectcalendars/au;Landroid/view/ViewGroup;I)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    const v1, 0x7f12028b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 186
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->b(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 188
    invoke-virtual {v1}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 189
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->b(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    :cond_0
    const v1, 0x7f12028a

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v2, v1}, Lcom/android/calendar/selectcalendars/az;->a(Lcom/android/calendar/selectcalendars/az;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 195
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->c(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 196
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->c(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 197
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->c(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    :cond_1
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_2

    .line 201
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_2
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->b(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    if-eqz p2, :cond_6

    .line 215
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->b(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0424

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 219
    :goto_3
    return-object v3

    .line 202
    :cond_2
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-nez v1, :cond_3

    .line 203
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 204
    :cond_3
    iget v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->b:I

    if-ne v1, v6, :cond_4

    .line 205
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 206
    :cond_4
    const-string v1, "Task"

    iget-object v4, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 207
    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 209
    :cond_5
    iget-object v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->a:Ljava/lang/String;

    .line 210
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->c(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-direct {p0, v4, v0}, Lcom/android/calendar/selectcalendars/au;->a(Landroid/widget/ImageView;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;)V

    move-object v0, v1

    goto :goto_2

    .line 217
    :cond_6
    invoke-static {v2}, Lcom/android/calendar/selectcalendars/az;->b(Lcom/android/calendar/selectcalendars/az;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0f0427

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_7
    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_1

    :cond_8
    move-object v1, v4

    move-object v2, v4

    goto/16 :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 425
    const/4 v0, 0x0

    return v0
.end method
