.class Lcom/android/calendar/selectcalendars/p;
.super Landroid/widget/BaseAdapter;
.source "AddCalendarFragment.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/d;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/res/Resources;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/selectcalendars/d;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 509
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 510
    iput-object p2, p0, Lcom/android/calendar/selectcalendars/p;->b:Landroid/content/Context;

    .line 511
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/p;->d:I

    .line 512
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/p;->c:Landroid/content/res/Resources;

    .line 513
    return-void
.end method

.method private a(I)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 535
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 536
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 537
    aget v1, v0, v3

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 538
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method private b(I)Landroid/graphics/drawable/StateListDrawable;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 542
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/p;->a(I)I

    move-result v0

    .line 543
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 544
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 545
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/d;->k(Lcom/android/calendar/selectcalendars/d;)I

    move-result v2

    invoke-virtual {v1, v5, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 546
    invoke-virtual {v1, v5}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 548
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 549
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 550
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->k(Lcom/android/calendar/selectcalendars/d;)I

    move-result v0

    invoke-virtual {v2, v5, v0}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 551
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 553
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 554
    new-array v3, v5, [I

    const v4, 0x10100a7

    aput v4, v3, v6

    invoke-virtual {v0, v3, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 555
    new-array v2, v5, [I

    const v3, -0x10100a7

    aput v3, v2, v6

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 556
    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->h(Lcom/android/calendar/selectcalendars/d;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->h(Lcom/android/calendar/selectcalendars/d;)[I

    move-result-object v0

    array-length v0, v0

    .line 519
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->h(Lcom/android/calendar/selectcalendars/d;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->h(Lcom/android/calendar/selectcalendars/d;)[I

    move-result-object v0

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 526
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 531
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 561
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 562
    const v1, 0x7f040026

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 563
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    iget v2, p0, Lcom/android/calendar/selectcalendars/p;->d:I

    iget v3, p0, Lcom/android/calendar/selectcalendars/p;->d:I

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 564
    invoke-virtual {p0, p1}, Lcom/android/calendar/selectcalendars/p;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 565
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v6, [Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, Lcom/android/calendar/hj;->b(I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/android/calendar/selectcalendars/p;->b(I)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 567
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v3}, Lcom/android/calendar/selectcalendars/d;->e(Lcom/android/calendar/selectcalendars/d;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 568
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/p;->c:Landroid/content/res/Resources;

    sget-object v4, Lcom/android/calendar/selectcalendars/r;->b:[I

    aget v4, v4, p1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 570
    :cond_0
    invoke-static {}, Lcom/android/calendar/selectcalendars/d;->g()I

    move-result v3

    if-ne v3, p1, :cond_1

    .line 571
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, Lcom/android/calendar/hj;->b(I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/android/calendar/selectcalendars/p;->b(I)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    aput-object v2, v3, v5

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/p;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f02001b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-direct {v0, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 573
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 574
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/p;->a:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/d;->l(Lcom/android/calendar/selectcalendars/d;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/android/calendar/selectcalendars/d;->a(Lcom/android/calendar/selectcalendars/d;I)V

    .line 575
    return-object v1
.end method
