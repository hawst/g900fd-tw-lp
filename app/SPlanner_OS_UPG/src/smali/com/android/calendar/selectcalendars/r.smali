.class public Lcom/android/calendar/selectcalendars/r;
.super Ljava/lang/Object;
.source "CalendarsUtils.java"


# static fields
.field public static a:[Ljava/lang/String;

.field public static b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 35
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "#04b5ff"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "#42a500"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "#b39e0f"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "#a0522d"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "#f27200"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "#ee82ee"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "#8a2be2"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "#596990"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "#002882"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "#007374"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "#556b2f"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "#660000"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "#ee2e24"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "#ec008c"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "#4b0082"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "#696969"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/selectcalendars/r;->a:[Ljava/lang/String;

    .line 40
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/selectcalendars/r;->b:[I

    return-void

    :array_0
    .array-data 4
        0x7f0f00d3
        0x7f0f00de
        0x7f0f00d7
        0x7f0f00db
        0x7f0f00da
        0x7f0f00dd
        0x7f0f00cd
        0x7f0f00d9
        0x7f0f00cf
        0x7f0f00dc
        0x7f0f00d1
        0x7f0f00d6
        0x7f0f00ce
        0x7f0f00d2
        0x7f0f00d8
        0x7f0f00d4
    .end array-data
.end method

.method public static a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z
    .locals 2

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v1, "LOCAL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "My calendar"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;Landroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    const-string v2, "LOCAL"

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 54
    instance-of v2, p1, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;

    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 54
    goto :goto_0

    .line 57
    :cond_2
    invoke-static {p0}, Lcom/android/calendar/selectcalendars/r;->d(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 58
    goto :goto_0

    .line 61
    :cond_3
    const-string v2, "LOCAL"

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, p1, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;

    if-nez v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v3, "legalHoliday"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v3, "legalSubstHoliday"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v1

    .line 63
    goto :goto_0

    .line 64
    :cond_5
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v3, "24SolarTerms"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 65
    goto :goto_0
.end method

.method public static b(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z
    .locals 2

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z
    .locals 2

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v1, "com.android.exchange"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    const/4 v0, 0x1

    .line 74
    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v1, :cond_3

    .line 75
    iget-wide v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    const-string v2, "task_personal"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_3
    iget-wide v2, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    const-string v2, "calendar_personal"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v2, "legalHoliday"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v2, "24SolarTerms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    const-string v2, "legalSubstHoliday"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0
.end method
