.class public Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;
.super Lcom/android/calendar/selectcalendars/bi;
.source "SelectCalendarsFragmentForSetting.java"


# static fields
.field private static final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/bi;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/android/calendar/selectcalendars/bi;->onAttach(Landroid/app/Activity;)V

    .line 47
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "account_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "account_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->e:Landroid/accounts/Account;

    .line 53
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    const v0, 0x7f040088

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->b:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->b:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->c:Landroid/widget/ListView;

    .line 60
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/SelectCalendarsFragmentForSetting;->b:Landroid/view/View;

    return-object v0
.end method
