.class public Lcom/android/calendar/selectcalendars/d;
.super Landroid/app/DialogFragment;
.source "AddCalendarFragment.java"


# static fields
.field public static final a:Ljava/lang/String;

.field private static o:Lcom/android/calendar/selectcalendars/q;

.field private static p:I


# instance fields
.field private b:Landroid/app/AlertDialog;

.field private c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/Button;

.field private k:Z

.field private l:Lcom/android/calendar/ag;

.field private m:Landroid/widget/GridView;

.field private n:Lcom/android/calendar/selectcalendars/p;

.field private q:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    .line 103
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/selectcalendars/d;->p:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/d;-><init>(Z)V

    .line 109
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 88
    const-string v0, "#04b5ff"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    .line 91
    const-string v0, "LOCAL"

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 112
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 113
    return-void
.end method

.method public constructor <init>(ZLcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 88
    const-string v0, "#04b5ff"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    .line 91
    const-string v0, "LOCAL"

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 116
    iput-boolean p1, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 117
    iput-object p2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 118
    iget v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    .line 119
    iget-object v0, p2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    .line 120
    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    .prologue
    .line 73
    sput p0, Lcom/android/calendar/selectcalendars/d;->p:I

    return p0
.end method

.method static synthetic a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 73
    invoke-static {p0, p1, p2}, Lcom/android/calendar/selectcalendars/d;->b(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/app/Activity;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 348
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 350
    const v1, 0x7f040007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/d;->d:Landroid/view/View;

    .line 351
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 352
    const v2, 0x7f0b003b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->g:I

    .line 353
    const v0, 0x7f120027

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    .line 354
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    new-instance v3, Lcom/android/calendar/en;

    invoke-direct {v3, p1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 355
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/android/calendar/selectcalendars/k;

    invoke-direct {v2, p0, p1}, Lcom/android/calendar/selectcalendars/k;-><init>(Lcom/android/calendar/selectcalendars/d;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 375
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/android/calendar/selectcalendars/l;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/l;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 388
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/android/calendar/selectcalendars/m;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/m;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 395
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "My calendar"

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v2, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 406
    :cond_0
    :goto_0
    const v0, 0x7f120028

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->e:Landroid/view/View;

    .line 407
    invoke-virtual {p1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 409
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/d;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 410
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->e()V

    .line 415
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 416
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 417
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/d;->i()V

    .line 418
    return-object v1

    .line 398
    :cond_1
    const-string v0, "My calendars (personal)"

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v2, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 400
    :cond_2
    const-string v0, "My calendars (KNOX)"

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v2, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 401
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "KNOX"

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 403
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v2, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 412
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->f()V

    .line 413
    invoke-direct {p0, v1}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/d;->j:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    return-object v0
.end method

.method private a(Landroid/app/Dialog;)V
    .locals 3

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 320
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 321
    const/4 v2, -0x2

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    .line 322
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/d;->b(Landroid/view/View;)V

    .line 424
    new-instance v0, Lcom/android/calendar/selectcalendars/n;

    invoke-direct {v0, p0}, Lcom/android/calendar/selectcalendars/n;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    sput-object v0, Lcom/android/calendar/selectcalendars/d;->o:Lcom/android/calendar/selectcalendars/q;

    .line 438
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;I)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/d;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/app/Dialog;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/d;[I)[I
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/d;->q:[I

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/d;I)I
    .locals 0

    .prologue
    .line 73
    iput p1, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/d;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private static b(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 686
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/p;->getCount()I

    move-result v2

    .line 496
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 497
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/p;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 498
    sput v1, Lcom/android/calendar/selectcalendars/d;->p:I

    .line 496
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 502
    :cond_1
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 462
    const v0, 0x7f120098

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->m:Landroid/widget/GridView;

    .line 463
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 464
    if-nez v0, :cond_0

    .line 491
    :goto_0
    return-void

    .line 467
    :cond_0
    new-instance v1, Lcom/android/calendar/selectcalendars/p;

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/selectcalendars/p;-><init>(Lcom/android/calendar/selectcalendars/d;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    .line 468
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 470
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->m:Landroid/widget/GridView;

    new-instance v1, Lcom/android/calendar/selectcalendars/f;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/f;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 482
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->m:Landroid/widget/GridView;

    const v1, 0x7f02011a

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(I)V

    .line 490
    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/d;->b(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/selectcalendars/d;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->j:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/selectcalendars/d;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/selectcalendars/d;)Z
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/d;->j()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/selectcalendars/d;)Z
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/d;->k()Z

    move-result v0

    return v0
.end method

.method static synthetic g()I
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/android/calendar/selectcalendars/d;->p:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/selectcalendars/d;)Landroid/view/View;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h()Lcom/android/calendar/selectcalendars/q;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/android/calendar/selectcalendars/d;->o:Lcom/android/calendar/selectcalendars/q;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/selectcalendars/d;)[I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->q:[I

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/selectcalendars/d;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 441
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/selectcalendars/o;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/o;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 455
    return-void
.end method

.method static synthetic j(Lcom/android/calendar/selectcalendars/d;)Lcom/android/calendar/selectcalendars/p;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->n:Lcom/android/calendar/selectcalendars/p;

    return-object v0
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 580
    const-string v0, "com.google"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic k(Lcom/android/calendar/selectcalendars/d;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->g:I

    return v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 584
    const-string v0, "com.android.exchange"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/selectcalendars/d;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 601
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 602
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f02ba

    invoke-virtual {p0, v1}, Lcom/android/calendar/selectcalendars/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    const-string v0, "My calendar"

    .line 612
    :goto_0
    return-object v0

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f02bb

    invoke-virtual {p0, v1}, Lcom/android/calendar/selectcalendars/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 605
    const-string v0, "My calendars (personal)"

    goto :goto_0

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f02bc

    invoke-virtual {p0, v1}, Lcom/android/calendar/selectcalendars/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "KNOX"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 607
    const-string v0, "My calendars (KNOX)"

    goto :goto_0

    .line 609
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 612
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    return v0
.end method

.method public c()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 620
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->a()Ljava/lang/String;

    move-result-object v9

    .line 622
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    .line 654
    :goto_0
    return v0

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v0, :cond_1

    .line 627
    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget v1, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    .line 628
    goto :goto_0

    .line 632
    :cond_1
    const-string v3, "calendar_displayName=? AND account_type=? AND deleted!=1"

    .line 635
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    aput-object v9, v4, v6

    const-string v0, "LOCAL"

    aput-object v0, v4, v7

    .line 638
    const/4 v8, 0x0

    .line 640
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v10, "_id"

    aput-object v10, v2, v5

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 643
    if-eqz v1, :cond_5

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_5

    .line 645
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget v2, v2, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    .line 653
    if-eqz v1, :cond_2

    .line 654
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move v0, v7

    goto :goto_0

    .line 653
    :cond_3
    if-eqz v1, :cond_4

    .line 654
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v6

    goto :goto_0

    .line 653
    :cond_5
    if-eqz v1, :cond_6

    .line 654
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    move v0, v7

    goto :goto_0

    .line 653
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_1
    if-eqz v1, :cond_7

    .line 654
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 653
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public d()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 660
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->a()Ljava/lang/String;

    move-result-object v4

    .line 661
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 662
    const-string v5, "calendar_displayName=? AND account_type=?"

    .line 664
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->l:Lcom/android/calendar/ag;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const-string v4, "LOCAL"

    aput-object v4, v6, v1

    const-string v7, "_id"

    move-object v4, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    :cond_0
    return-void
.end method

.method public e()V
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 670
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v7, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 672
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v8, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 673
    const-string v5, "account_type=? AND account_name=? AND color_type=?"

    .line 677
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->l:Lcom/android/calendar/ag;

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Colors;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->c:[Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v7, v6, v9

    const/4 v7, 0x1

    aput-object v8, v6, v7

    const-string v7, "0"

    aput-object v7, v6, v1

    const-string v7, "CAST(color_index as integer) ASC"

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    :cond_0
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 694
    const/4 v0, 0x0

    .line 695
    sget-object v1, Lcom/android/calendar/selectcalendars/r;->a:[Ljava/lang/String;

    array-length v1, v1

    .line 696
    new-array v2, v1, [I

    iput-object v2, p0, Lcom/android/calendar/selectcalendars/d;->q:[I

    .line 697
    :goto_0
    if-ge v0, v1, :cond_0

    .line 698
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->q:[I

    sget-object v3, Lcom/android/calendar/selectcalendars/r;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 699
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 701
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 214
    new-instance v0, Lcom/android/calendar/selectcalendars/i;

    invoke-direct {v0, p0, p1, p1}, Lcom/android/calendar/selectcalendars/i;-><init>(Lcom/android/calendar/selectcalendars/d;Landroid/content/Context;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->l:Lcom/android/calendar/ag;

    .line 310
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 315
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/app/Dialog;)V

    .line 316
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    move-object v0, v1

    .line 205
    :goto_0
    return-object v0

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 153
    goto :goto_0

    .line 155
    :cond_1
    if-eqz p1, :cond_4

    .line 156
    const-string v0, "key_dialog_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    const-string v0, "key_dialog_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 159
    :cond_2
    const-string v0, "key_color_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    const-string v0, "key_color_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    .line 162
    :cond_3
    const-string v0, "key_data_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 163
    const-string v0, "key_data_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 167
    :cond_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 168
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v2, :cond_5

    .line 169
    const v2, 0x7f0f0189

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 173
    :goto_1
    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 174
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/android/calendar/selectcalendars/e;

    invoke-direct {v2, p0}, Lcom/android/calendar/selectcalendars/e;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 186
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 188
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    .line 189
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/calendar/selectcalendars/g;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/g;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 205
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->b:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 171
    :cond_5
    const v2, 0x7f0f002f

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const/4 v0, 0x0

    .line 143
    :goto_0
    return-object v0

    .line 128
    :cond_0
    if-eqz p3, :cond_3

    .line 129
    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    .line 132
    :cond_1
    const-string v0, "key_color_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    const-string v0, "key_color_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    .line 135
    :cond_2
    const-string v0, "key_data_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    const-string v0, "key_data_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 139
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    if-eqz v0, :cond_4

    .line 140
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 143
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/calendar/selectcalendars/d;->a(Landroid/app/Activity;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 326
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 327
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/d;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 328
    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 332
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/selectcalendars/j;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/j;-><init>(Lcom/android/calendar/selectcalendars/d;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 345
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 589
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 590
    const-string v0, "key_color_state"

    iget v1, p0, Lcom/android/calendar/selectcalendars/d;->f:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 591
    const-string v0, "key_color_picker_state"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 593
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    if-eqz v0, :cond_0

    .line 594
    const-string v0, "key_data_state"

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/d;->c:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 597
    :cond_0
    const-string v0, "key_dialog_state"

    iget-boolean v1, p0, Lcom/android/calendar/selectcalendars/d;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 598
    return-void
.end method
