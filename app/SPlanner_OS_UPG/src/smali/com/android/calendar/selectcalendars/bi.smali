.class public Lcom/android/calendar/selectcalendars/bi;
.super Landroid/app/Fragment;
.source "SelectCalendarsFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# static fields
.field protected static a:I

.field private static final g:Ljava/lang/String;


# instance fields
.field protected b:Landroid/view/View;

.field protected c:Landroid/widget/ListView;

.field protected d:Lcom/android/calendar/selectcalendars/bl;

.field protected e:Landroid/accounts/Account;

.field protected f:Landroid/app/Activity;

.field private h:Landroid/content/AsyncQueryHandler;

.field private i:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/android/calendar/selectcalendars/bi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/bi;->g:Ljava/lang/String;

    .line 56
    const v0, 0x7f040094

    sput v0, Lcom/android/calendar/selectcalendars/bi;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->b:Landroid/view/View;

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/bi;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/bi;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/selectcalendars/bi;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 301
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->e:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 303
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 306
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 308
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 309
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 311
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    .line 312
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 314
    :cond_2
    return-object v1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 170
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->e:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 173
    sget-object v0, Lcom/android/calendar/selectcalendars/bi;->g:Ljava/lang/String;

    const-string v1, "queryTasks: No calendar"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .line 180
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x2

    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/bi;->b(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->b:[Ljava/lang/String;

    const-string v7, "_id asc"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void

    .line 175
    :cond_0
    const-string v5, "_sync_account=?"

    .line 176
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bi;->e:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v1, v6, v0

    goto :goto_0
.end method

.method private a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 113
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 114
    iget-boolean v0, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 115
    :goto_0
    iget-boolean v1, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    if-eqz v1, :cond_2

    .line 116
    const/4 v1, 0x4

    .line 117
    sget-object v3, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    iget-wide v6, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-static {v3, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 118
    const-string v5, "selected"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/content/AsyncQueryHandler;->startUpdate(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    .line 132
    return-void

    .line 114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_2
    const/4 v1, 0x3

    .line 121
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v6, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    invoke-static {v3, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 122
    const-string v5, "visible"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    iget-boolean v5, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    if-nez v5, :cond_0

    iget-boolean v5, p1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v5, :cond_0

    .line 128
    const-string v5, "sync_events"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/bi;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/bi;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/selectcalendars/bi;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/bi;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V

    return-void
.end method

.method private b(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 18

    .prologue
    .line 240
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 241
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 242
    const-string v2, "calendar_displayName"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 243
    const-string v2, "name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 244
    const-string v2, "calendar_color"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 245
    const-string v2, "account_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 246
    const-string v2, "ownerAccount"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    .line 247
    const-string v2, "visible"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 248
    const-string v2, "account_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 249
    const-string v2, "sync_events"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 250
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/selectcalendars/bi;->f:Landroid/app/Activity;

    .line 251
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 252
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 253
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 254
    if-nez v2, :cond_0

    .line 255
    const v2, 0x7f0b0073

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 257
    :cond_0
    new-instance v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v14}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 258
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 259
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 260
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->c:Ljava/lang/String;

    .line 261
    iput v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 262
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 263
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->f:Ljava/lang/String;

    .line 264
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 265
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->h:Z

    .line 266
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 267
    const/4 v2, 0x0

    iput-boolean v2, v14, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 268
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 265
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 270
    :cond_3
    return-object v3
.end method

.method static synthetic b(Lcom/android/calendar/selectcalendars/bi;Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/calendar/selectcalendars/bi;->c(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 152
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->e:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 155
    sget-object v0, Lcom/android/calendar/selectcalendars/bi;->g:Ljava/lang/String;

    const-string v3, "queryCalendars: No calendar"

    invoke-static {v0, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v2

    move-object v5, v2

    .line 162
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    const-string v7, "_id asc"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-void

    .line 157
    :cond_0
    const-string v5, "account_name=?"

    .line 158
    new-array v6, v1, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/bi;->e:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v6, v0

    goto :goto_0
.end method

.method private c(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 274
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 275
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 276
    const-string v0, "displayName"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 277
    const-string v0, "_sync_account_key"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 278
    const-string v0, "_sync_account"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 279
    const-string v0, "selected"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 280
    const-string v0, "_sync_account_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 281
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    new-instance v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {v9}, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;-><init>()V

    .line 283
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->a:J

    .line 284
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->b:Ljava/lang/String;

    .line 285
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bi;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v0, v10}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->d:I

    .line 286
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->e:Ljava/lang/String;

    .line 287
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 288
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    .line 289
    iput-boolean v1, v9, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->j:Z

    .line 290
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 287
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 292
    :cond_1
    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 144
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/AsyncQueryHandler;->cancelOperation(I)V

    .line 145
    invoke-direct {p0}, Lcom/android/calendar/selectcalendars/bi;->b()V

    .line 147
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 193
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bi;->a()V

    .line 196
    :cond_0
    return-void
.end method

.method public g()J
    .locals 2

    .prologue
    .line 188
    const-wide/16 v0, 0x80

    return-wide v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 94
    new-instance v0, Lcom/android/calendar/selectcalendars/bl;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bi;->f:Landroid/app/Activity;

    sget v2, Lcom/android/calendar/selectcalendars/bi;->a:I

    invoke-direct {v0, v1, v2}, Lcom/android/calendar/selectcalendars/bl;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->d:Lcom/android/calendar/selectcalendars/bl;

    .line 95
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bi;->d:Lcom/android/calendar/selectcalendars/bl;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/android/calendar/selectcalendars/bj;

    invoke-direct {v1, p0}, Lcom/android/calendar/selectcalendars/bj;-><init>(Lcom/android/calendar/selectcalendars/bi;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 77
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/bi;->f:Landroid/app/Activity;

    .line 78
    new-instance v0, Lcom/android/calendar/selectcalendars/bk;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/selectcalendars/bk;-><init>(Lcom/android/calendar/selectcalendars/bi;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->h:Landroid/content/AsyncQueryHandler;

    .line 79
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 84
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 85
    const v0, 0x7f040087

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->b:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->b:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->c:Landroid/widget/ListView;

    .line 87
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->b:Landroid/view/View;

    const v1, 0x7f120288

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->i:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bi;->b:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 137
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/bi;->a()V

    .line 138
    return-void
.end method
