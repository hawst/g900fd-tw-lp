.class Lcom/android/calendar/selectcalendars/k;
.super Ljava/lang/Object;
.source "AddCalendarFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/android/calendar/selectcalendars/d;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/d;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/k;->b:Lcom/android/calendar/selectcalendars/d;

    iput-object p2, p0, Lcom/android/calendar/selectcalendars/k;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/k;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/d;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/k;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 372
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/k;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->c(Lcom/android/calendar/selectcalendars/d;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/k;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/d;->c(Lcom/android/calendar/selectcalendars/d;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/k;->b:Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/d;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 361
    :cond_0
    return-void
.end method
