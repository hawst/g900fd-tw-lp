.class public Lcom/android/calendar/selectcalendars/AccountQueryConstant;
.super Ljava/lang/Object;
.source "AccountQueryConstant.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    const-class v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->d:Ljava/lang/String;

    .line 36
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "account_type"

    aput-object v1, v0, v5

    const-string v1, "ownerAccount"

    aput-object v1, v0, v6

    const-string v1, "name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "visible"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sync_events"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "(account_name=ownerAccount) AS \"primary\""

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->a:[Ljava/lang/String;

    .line 51
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_sync_account"

    aput-object v1, v0, v4

    const-string v1, "_sync_account_type"

    aput-object v1, v0, v5

    const-string v1, "_sync_account_key"

    aput-object v1, v0, v6

    const-string v1, "displayName"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "selected"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->b:[Ljava/lang/String;

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "account_type"

    aput-object v1, v0, v5

    const-string v1, "color"

    aput-object v1, v0, v6

    const-string v1, "color_index"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "color_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->d:Ljava/lang/String;

    return-object v0
.end method
