.class Lcom/android/calendar/selectcalendars/bg;
.super Landroid/content/AsyncQueryHandler;
.source "ExpandableSelectCalendarsFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/ba;


# direct methods
.method public constructor <init>(Lcom/android/calendar/selectcalendars/ba;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    .line 406
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 407
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 412
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 413
    if-nez p3, :cond_1

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/ba;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 417
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 422
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 425
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/ba;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v5, v6

    .line 430
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->c(Lcom/android/calendar/selectcalendars/ba;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v2, p3}, Lcom/android/calendar/selectcalendars/ba;->a(Lcom/android/calendar/selectcalendars/ba;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lcom/android/calendar/hf;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/selectcalendars/AccountQueryConstant;->b:[Ljava/lang/String;

    const-string v7, "_id asc"

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 428
    :cond_3
    const-string v5, "_sync_account_type!=\'com.osp.app.signin\'"

    goto :goto_1

    .line 438
    :pswitch_1
    check-cast p2, Ljava/util/ArrayList;

    .line 439
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0, p3}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    .line 440
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v3, p2, v0}, Lcom/android/calendar/selectcalendars/ba;->a(Lcom/android/calendar/selectcalendars/ba;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 441
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v3}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->getHeaderViewsCount()I

    move-result v3

    if-ge v3, v1, :cond_4

    .line 442
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    iget-object v3, v3, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    iget-object v3, v3, Lcom/android/calendar/selectcalendars/au;->b:Landroid/view/View;

    invoke-virtual {v1, v3}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->addHeaderView(Landroid/view/View;)V

    .line 444
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    iget-object v1, v1, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v1, v0}, Lcom/android/calendar/selectcalendars/au;->a(Ljava/util/ArrayList;)V

    .line 445
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v1, v0}, Lcom/android/calendar/selectcalendars/ba;->a(Lcom/android/calendar/selectcalendars/ba;Ljava/util/ArrayList;)V

    .line 446
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 447
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/ba;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/au;->getGroupCount()I

    move-result v1

    .line 448
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->d(Lcom/android/calendar/selectcalendars/ba;)I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0, v1}, Lcom/android/calendar/selectcalendars/ba;->a(Lcom/android/calendar/selectcalendars/ba;I)I

    .line 450
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    move v0, v2

    .line 451
    :goto_2
    if-ge v0, v1, :cond_5

    .line 452
    iget-object v3, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v3}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->expandGroup(I)Z

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 454
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/ba;->b(Lcom/android/calendar/selectcalendars/ba;)Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwExpandableListView;->setExpandableListAnimationEnabled(Z)V

    .line 455
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/bg;->a:Lcom/android/calendar/selectcalendars/ba;

    invoke-static {v0, v2}, Lcom/android/calendar/selectcalendars/ba;->a(Lcom/android/calendar/selectcalendars/ba;Z)Z

    goto/16 :goto_0

    .line 422
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
