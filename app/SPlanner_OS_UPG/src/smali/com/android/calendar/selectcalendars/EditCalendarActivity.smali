.class public Lcom/android/calendar/selectcalendars/EditCalendarActivity;
.super Lcom/android/calendar/b;
.source "EditCalendarActivity.java"


# instance fields
.field private b:Lcom/android/calendar/selectcalendars/aj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 123
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->invalidateOptionsMenu()V

    .line 124
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    const/4 v1, 0x1

    .line 44
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/aj;

    iput-object v0, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    .line 48
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 51
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    if-nez v1, :cond_1

    .line 56
    new-instance v1, Lcom/android/calendar/selectcalendars/aj;

    invoke-direct {v1}, Lcom/android/calendar/selectcalendars/aj;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    .line 57
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 58
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 59
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 61
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 66
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 116
    :goto_0
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 89
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->finish()V

    move v0, v2

    .line 90
    goto :goto_1

    .line 93
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 94
    if-nez v0, :cond_0

    .line 95
    new-instance v1, Lcom/android/calendar/selectcalendars/d;

    invoke-direct {v1, v2}, Lcom/android/calendar/selectcalendars/d;-><init>(Z)V

    move-object v0, v1

    .line 96
    check-cast v0, Lcom/android/calendar/selectcalendars/d;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/d;->f()V

    .line 98
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v3, Lcom/android/calendar/selectcalendars/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    move v0, v2

    .line 103
    goto :goto_1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    const-string v0, "EditCalendarActivity"

    const-string v1, "Fail to launch AddCalendarFragment"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 106
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/aj;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/selectcalendars/DeleteCalendarActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    .line 109
    const-string v0, "Groups"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 110
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 111
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_1

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "childs"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarGroup;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 111
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 114
    :cond_1
    invoke-virtual {p0, v3}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 87
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f120324 -> :sswitch_2
        0x7f12033c -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 72
    const v0, 0x7f120324

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->b:Lcom/android/calendar/selectcalendars/aj;

    invoke-virtual {v1}, Lcom/android/calendar/selectcalendars/aj;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 74
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/android/calendar/b;->onResume()V

    .line 80
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/android/calendar/selectcalendars/EditCalendarActivity;->finish()V

    .line 83
    :cond_0
    return-void
.end method
