.class final Lcom/android/calendar/selectcalendars/ad;
.super Lcom/android/calendar/common/a/a;
.source "DeleteCalendarsFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/w;


# direct methods
.method public constructor <init>(Lcom/android/calendar/selectcalendars/w;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    .line 492
    invoke-direct {p0, p2}, Lcom/android/calendar/common/a/a;-><init>(Landroid/content/Context;)V

    .line 493
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->g(Lcom/android/calendar/selectcalendars/w;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ActionMode;Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 557
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 558
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1, p1}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 559
    const v1, 0x7f110003

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 560
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->h(Lcom/android/calendar/selectcalendars/w;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f120324

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 561
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 562
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    const v2, 0x7f0f0398

    invoke-virtual {v1, v2}, Lcom/android/calendar/selectcalendars/w;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 563
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 502
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/s;->a(Z)I

    .line 503
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/s;->a(Z)I

    .line 508
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/s;->notifyDataSetChanged()V

    .line 534
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 589
    invoke-super {p0, p1}, Lcom/android/calendar/common/a/a;->a(Z)V

    .line 590
    return-void
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 512
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v1

    if-nez v1, :cond_1

    .line 517
    :cond_0
    :goto_0
    return v0

    .line 515
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->g(Lcom/android/calendar/selectcalendars/w;)I

    move-result v1

    .line 516
    iget-object v2, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v2}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/selectcalendars/s;->e()I

    move-result v2

    .line 517
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->h(Lcom/android/calendar/selectcalendars/w;)Landroid/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f120324

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 523
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->g(Lcom/android/calendar/selectcalendars/w;)I

    move-result v1

    .line 524
    if-lez v1, :cond_0

    .line 525
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 529
    :goto_0
    return-void

    .line 527
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 543
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 552
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 545
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->i(Lcom/android/calendar/selectcalendars/w;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->i(Lcom/android/calendar/selectcalendars/w;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->j(Lcom/android/calendar/selectcalendars/w;)V

    .line 548
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 543
    :pswitch_data_0
    .packed-switch 0x7f120324
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 567
    invoke-super {p0, p1, p2}, Lcom/android/calendar/common/a/a;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 568
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 574
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/s;->a(Z)I

    .line 575
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/w;->b(Lcom/android/calendar/selectcalendars/w;)Lcom/android/calendar/selectcalendars/s;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/w;->a(Lcom/android/calendar/selectcalendars/w;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/selectcalendars/s;->a(Ljava/util/ArrayList;)V

    .line 576
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/ad;->a:Lcom/android/calendar/selectcalendars/w;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/w;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 577
    if-nez v0, :cond_0

    .line 581
    :goto_0
    return-void

    .line 580
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0

    .prologue
    .line 586
    return-void
.end method
