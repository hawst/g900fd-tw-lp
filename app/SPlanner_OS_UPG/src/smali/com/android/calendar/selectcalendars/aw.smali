.class Lcom/android/calendar/selectcalendars/aw;
.super Ljava/lang/Object;
.source "ExpandableSelectCalendarsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

.field final synthetic b:Lcom/android/calendar/selectcalendars/au;


# direct methods
.method constructor <init>(Lcom/android/calendar/selectcalendars/au;Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    iput-object p2, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 264
    const v0, 0x7f12001c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 265
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 266
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 268
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    const-string v1, "LOCAL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-boolean v0, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-eqz v0, :cond_5

    .line 270
    sget-object v0, Lcom/android/calendar/hj;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/au;->a(Lcom/android/calendar/selectcalendars/au;)Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v3, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 276
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->b(Lcom/android/calendar/selectcalendars/au;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/r;->a(Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->c(Lcom/android/calendar/selectcalendars/au;)Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_2

    .line 284
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-boolean v1, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    iput-boolean v1, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->k:Z

    .line 285
    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-boolean v1, v1, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    if-nez v1, :cond_2

    .line 286
    iput-boolean v2, v0, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->g:Z

    .line 289
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-virtual {v0}, Lcom/android/calendar/selectcalendars/au;->notifyDataSetChanged()V

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v0}, Lcom/android/calendar/selectcalendars/au;->d(Lcom/android/calendar/selectcalendars/au;)V

    .line 292
    return-void

    :cond_4
    move v1, v2

    .line 265
    goto :goto_0

    .line 272
    :cond_5
    sget-object v0, Lcom/android/calendar/hj;->t:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/selectcalendars/aw;->b:Lcom/android/calendar/selectcalendars/au;

    invoke-static {v1}, Lcom/android/calendar/selectcalendars/au;->a(Lcom/android/calendar/selectcalendars/au;)Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/android/calendar/selectcalendars/aw;->a:Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;

    iget-object v3, v3, Lcom/android/calendar/selectcalendars/AccountQueryConstant$CalendarChild;->i:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method
