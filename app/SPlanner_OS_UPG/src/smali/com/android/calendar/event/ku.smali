.class Lcom/android/calendar/event/ku;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SelectStickerFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectStickerFragment;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/SelectStickerFragment;)V
    .locals 0

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/SelectStickerFragment;Lcom/android/calendar/event/kl;)V
    .locals 0

    .prologue
    .line 1076
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ku;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1081
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1136
    :cond_0
    :goto_0
    return v2

    .line 1086
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    sub-int v1, v0, v1

    .line 1087
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1088
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    .line 1089
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1091
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    .line 1092
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    float-to-int v5, v5

    .line 1094
    const/16 v6, 0x32

    if-lt v0, v6, :cond_2

    if-gt v0, v3, :cond_3

    :cond_2
    const/16 v0, 0xc8

    if-lt v4, v0, :cond_0

    if-le v4, v5, :cond_0

    .line 1095
    :cond_3
    const/4 v0, 0x1

    .line 1096
    if-lez v1, :cond_8

    const/4 v3, 0x0

    cmpg-float v3, p3, v3

    if-gez v3, :cond_8

    .line 1097
    mul-int/lit8 v1, v1, -0x1

    move v3, v1

    move v1, v0

    .line 1100
    :cond_4
    :goto_1
    if-eqz v1, :cond_7

    .line 1101
    if-lez v3, :cond_5

    .line 1102
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v4, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    .line 1103
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    if-gez v0, :cond_6

    .line 1104
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    goto :goto_0

    .line 1133
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1109
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v4, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    .line 1110
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    iget-object v4, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v4}, Lcom/android/calendar/event/SelectStickerFragment;->i(Lcom/android/calendar/event/SelectStickerFragment;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v0, v4, :cond_6

    .line 1111
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->i(Lcom/android/calendar/event/SelectStickerFragment;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    goto :goto_0

    .line 1116
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v4, v4, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    invoke-virtual {v0}, Lcom/android/calendar/event/kx;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    move v1, v2

    .line 1117
    goto :goto_1

    .line 1121
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v3, v3, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1122
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->c(I)V

    .line 1123
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 1124
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->c()V

    .line 1125
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->b()V

    .line 1127
    iget-object v0, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->j(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getOverScrollMode()I

    move-result v0

    .line 1128
    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->j(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/ScrollView;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->setOverScrollMode(I)V

    .line 1129
    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->j(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ScrollView;->invalidate()V

    .line 1130
    iget-object v1, p0, Lcom/android/calendar/event/ku;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->j(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/ScrollView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setOverScrollMode(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :cond_8
    move v3, v1

    move v1, v0

    goto/16 :goto_1
.end method
