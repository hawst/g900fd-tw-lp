.class Lcom/android/calendar/event/gs;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1881
    iput-object p1, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1885
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1900
    :cond_0
    :goto_0
    return-void

    .line 1888
    :cond_1
    if-nez p3, :cond_2

    .line 1889
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->w(Lcom/android/calendar/event/fx;)V

    goto :goto_0

    .line 1891
    :cond_2
    add-int/lit8 v0, p3, -0x1

    .line 1892
    iget-object v1, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1, v0}, Lcom/android/calendar/event/fx;->e(Lcom/android/calendar/event/fx;I)I

    .line 1893
    iget-object v1, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    iget-object v2, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->x(Lcom/android/calendar/event/fx;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;

    .line 1894
    iget-object v1, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    iget-object v2, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->y(Lcom/android/calendar/event/fx;)[Ljava/lang/Integer;

    move-result-object v2

    aget-object v0, v2, v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;I)I

    .line 1895
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->A(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->z(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1896
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->A(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1897
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1898
    iget-object v0, p0, Lcom/android/calendar/event/gs;->a:Lcom/android/calendar/event/fx;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/event/fx;->n:Landroid/view/View;

    goto :goto_0
.end method
