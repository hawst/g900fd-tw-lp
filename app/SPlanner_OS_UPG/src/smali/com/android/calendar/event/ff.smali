.class public Lcom/android/calendar/event/ff;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;

.field private b:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 2516
    iput-object p1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517
    iput-object p2, p0, Lcom/android/calendar/event/ff;->b:Landroid/text/format/Time;

    .line 2518
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2521
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2535
    :goto_0
    return-void

    .line 2525
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ff;->b:Landroid/text/format/Time;

    .line 2529
    :cond_1
    iget-object v10, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    new-instance v0, Lcom/android/calendar/common/extension/a;

    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/fg;

    iget-object v3, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-direct {v2, v3, p1}, Lcom/android/calendar/event/fg;-><init>(Lcom/android/calendar/event/ay;Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/calendar/event/ff;->b:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    iget-object v4, p0, Lcom/android/calendar/event/ff;->b:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    iget-object v5, p0, Lcom/android/calendar/event/ff;->b:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f03b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v7}, Lcom/android/calendar/event/ay;->q(Lcom/android/calendar/event/ay;)Z

    move-result v7

    if-nez v7, :cond_2

    move v7, v8

    :goto_1
    iget-object v11, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v11}, Lcom/android/calendar/event/ay;->q(Lcom/android/calendar/event/ay;)Z

    move-result v11

    if-nez v11, :cond_3

    :goto_2
    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/common/extension/a;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;IIILjava/lang/String;ZZ)V

    iput-object v0, v10, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    .line 2533
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0, v9}, Lcom/android/calendar/common/extension/a;->setCanceledOnTouchOutside(Z)V

    .line 2534
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->show()V

    goto :goto_0

    :cond_2
    move v7, v9

    .line 2529
    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2
.end method

.method public b(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 2540
    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v1}, Lcom/android/calendar/common/extension/c;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2556
    :goto_0
    return-void

    .line 2544
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->r:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    move v8, v0

    .line 2545
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v7, v0

    .line 2547
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v1, v1, Landroid/text/format/Time;->second:I

    iget-object v2, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iget-object v3, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    iget-object v5, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v5

    iget v5, v5, Landroid/text/format/Time;->month:I

    iget-object v6, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v6}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v6

    iget v6, v6, Landroid/text/format/Time;->year:I

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v10

    .line 2548
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v1, v1, Landroid/text/format/Time;->second:I

    iget-object v2, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iget-object v3, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    iget-object v4, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    iget-object v5, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v5

    iget v5, v5, Landroid/text/format/Time;->month:I

    iget-object v6, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v6}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v6

    iget v6, v6, Landroid/text/format/Time;->year:I

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v0

    .line 2549
    iget-object v2, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 2550
    iget-object v2, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 2551
    iget-object v10, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    new-instance v0, Lcom/android/calendar/common/extension/c;

    iget-object v1, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/fh;

    iget-object v3, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-direct {v2, v3, p1}, Lcom/android/calendar/event/fh;-><init>(Lcom/android/calendar/event/ay;Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move v6, v8

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/common/extension/c;-><init>(Landroid/content/Context;Lcom/android/calendar/common/extension/i;Landroid/text/format/Time;Landroid/text/format/Time;ZZI)V

    iput-object v0, v10, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    .line 2554
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0, v9}, Lcom/android/calendar/common/extension/c;->setCanceledOnTouchOutside(Z)V

    .line 2555
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->show()V

    goto/16 :goto_0

    :cond_1
    move v8, v9

    .line 2544
    goto/16 :goto_1

    .line 2545
    :cond_2
    const/4 v7, 0x2

    goto/16 :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2560
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 2564
    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->G(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ff;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2566
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/ff;->b(Landroid/view/View;)V

    .line 2570
    :goto_0
    return-void

    .line 2568
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/ff;->a(Landroid/view/View;)V

    goto :goto_0
.end method
