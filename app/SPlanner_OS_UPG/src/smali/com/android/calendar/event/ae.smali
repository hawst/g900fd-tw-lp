.class Lcom/android/calendar/event/ae;
.super Ljava/lang/Object;
.source "EasyEditEventView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/v;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/v;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/v;Lcom/android/calendar/event/w;)V
    .locals 0

    .prologue
    .line 414
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ae;-><init>(Lcom/android/calendar/event/v;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0b0061

    const v5, 0x7f0b0028

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 418
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 420
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->e(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v2}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 423
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->g(Lcom/android/calendar/event/v;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->h(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->e(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/common/extension/EasyTimePicker;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->i(Lcom/android/calendar/event/v;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->j(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->k(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/calendar/common/extension/EasyTimePicker;->setVisibility(I)V

    .line 431
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 432
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 433
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 434
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->k(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v2}, Lcom/android/calendar/event/v;->m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 438
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->g(Lcom/android/calendar/event/v;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->h(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->e(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/calendar/common/extension/EasyTimePicker;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->i(Lcom/android/calendar/event/v;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->j(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->k(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/common/extension/EasyTimePicker;->setVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 447
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->d(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 448
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 449
    iget-object v0, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->l(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ae;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_0
.end method
