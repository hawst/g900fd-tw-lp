.class Lcom/android/calendar/event/s;
.super Ljava/lang/Object;
.source "EasyEditEventFragment.java"

# interfaces
.implements Lcom/android/calendar/event/ax;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/m;

.field private b:I


# direct methods
.method constructor <init>(Lcom/android/calendar/event/m;)V
    .locals 1

    .prologue
    .line 567
    iput-object p1, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/s;->b:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 572
    iput p1, p0, Lcom/android/calendar/event/s;->b:I

    .line 573
    return-void
.end method

.method public run()V
    .locals 11

    .prologue
    const v7, 0x7f0f0112

    const/4 v10, 0x3

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 579
    .line 580
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->l(Lcom/android/calendar/event/m;)I

    move-result v0

    if-nez v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0, v10}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 587
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->m(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/t;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->m(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/t;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/event/t;->d:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v4, v8

    if-nez v0, :cond_b

    .line 589
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iput-boolean v6, v0, Lcom/android/calendar/as;->aq:Z

    .line 597
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 598
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    move v0, v6

    .line 603
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->az:Ljava/lang/String;

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->l(Lcom/android/calendar/event/m;)I

    move-result v3

    if-ne v3, v10, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v4}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v4

    iget-object v4, v4, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v3, v0

    move v0, v6

    .line 610
    :goto_2
    iget-object v4, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v4}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v4

    if-eqz v4, :cond_14

    iget-object v4, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v4}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v4

    invoke-static {v4}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v4

    if-eqz v4, :cond_14

    iget v4, p0, Lcom/android/calendar/event/s;->b:I

    if-eq v4, v6, :cond_14

    if-nez v3, :cond_4

    if-eqz v0, :cond_14

    .line 613
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 615
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 616
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->n(Lcom/android/calendar/event/m;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iput-object v2, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    move v0, v6

    .line 621
    :goto_3
    iget v3, p0, Lcom/android/calendar/event/s;->b:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_5
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/as;->e()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->o(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/av;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v4}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v5}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v5

    iget-object v8, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v8}, Lcom/android/calendar/event/m;->l(Lcom/android/calendar/event/m;)I

    move-result v8

    iget-object v9, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v9}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v3, v4, v5, v8, v9}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 628
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->p(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/calendar/event/v;->a()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 629
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0f01a3

    invoke-static {v3, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 634
    :cond_6
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_11

    .line 635
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 636
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 637
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 638
    const v0, 0x7f0f039b

    .line 664
    :goto_4
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 666
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 667
    const-string v0, "GATE"

    const-string v3, "<GATE-M>EVENT_CREATED</GATE-M>"

    invoke-static {v0, v3}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_7
    iget v0, p0, Lcom/android/calendar/event/s;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_a

    .line 675
    iget v0, p0, Lcom/android/calendar/event/s;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 676
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 677
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/as;->y:J

    .line 678
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v8, v0, Lcom/android/calendar/as;->B:J

    .line 679
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_8

    .line 682
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 683
    new-instance v2, Landroid/text/format/Time;

    const-string v3, "UTC"

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 684
    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 685
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 686
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    .line 688
    const-string v3, "UTC"

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 689
    invoke-virtual {v2, v8, v9}, Landroid/text/format/Time;->set(J)V

    .line 690
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 691
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    .line 695
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-virtual {v0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_9

    .line 697
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 702
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 703
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 705
    iget-object v2, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 706
    if-eqz v2, :cond_a

    .line 707
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 708
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 712
    :cond_a
    return-void

    .line 591
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iput-boolean v1, v0, Lcom/android/calendar/as;->aq:Z

    goto/16 :goto_0

    :cond_c
    move v0, v7

    .line 640
    goto/16 :goto_4

    .line 643
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 644
    const v0, 0x7f0f039c

    goto/16 :goto_4

    .line 646
    :cond_e
    const v0, 0x7f0f0113

    goto/16 :goto_4

    .line 650
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 651
    const v0, 0x7f0f039c

    goto/16 :goto_4

    .line 653
    :cond_10
    const v0, 0x7f0f0113

    goto/16 :goto_4

    .line 657
    :cond_11
    iget-object v3, p0, Lcom/android/calendar/event/s;->a:Lcom/android/calendar/event/m;

    invoke-static {v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v3, :cond_12

    if-eqz v0, :cond_13

    .line 658
    :cond_12
    const v0, 0x7f0f039b

    goto/16 :goto_4

    :cond_13
    move v0, v7

    .line 660
    goto/16 :goto_4

    :cond_14
    move v0, v1

    goto/16 :goto_3

    :cond_15
    move v3, v0

    move v0, v1

    goto/16 :goto_2

    :cond_16
    move v0, v1

    goto/16 :goto_1

    :cond_17
    move v0, v1

    move v3, v1

    goto/16 :goto_2
.end method
