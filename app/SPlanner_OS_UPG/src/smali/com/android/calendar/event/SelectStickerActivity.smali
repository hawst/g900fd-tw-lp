.class public Lcom/android/calendar/event/SelectStickerActivity;
.super Lcom/android/calendar/b;
.source "SelectStickerActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# instance fields
.field private b:Lcom/android/calendar/event/SelectStickerFragment;

.field private c:Z

.field private d:Landroid/net/Uri;

.field private e:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    .line 56
    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->e:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectStickerActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectStickerActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/16 v4, 0x96

    const/4 v3, 0x1

    .line 345
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.camera.action.CROP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 348
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 350
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 351
    const-string v1, "scale"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 352
    const-string v1, "crop"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 353
    const-string v1, "output"

    iget-object v2, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 354
    const-string v1, "return-data"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    const/16 v1, 0x2329

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/event/SelectStickerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :goto_0
    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 358
    const-string v0, "SelectStickerActivity"

    const-string v1, "Error: Could not find STICKER_CROP_REQEUST activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectStickerActivity;)Lcom/android/calendar/event/SelectStickerFragment;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 98
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->c:Z

    if-eqz v0, :cond_0

    .line 99
    const v0, 0x7f12006b

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 100
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 102
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 103
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 104
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 106
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 107
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 119
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 121
    :cond_0
    return-void

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 110
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0446

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 113
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 114
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 116
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 117
    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 178
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/CharSequence;

    .line 179
    const/4 v1, 0x0

    const v2, 0x7f0f010f

    invoke-virtual {p0, v2}, Lcom/android/calendar/event/SelectStickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 180
    const v1, 0x7f0f0110

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/SelectStickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 182
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    const v2, 0x7f0f0111

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/event/ki;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ki;-><init>(Lcom/android/calendar/event/SelectStickerActivity;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 228
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 229
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 230
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 231
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->e:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 240
    const v1, 0x7f0f02c8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 241
    const v1, 0x7f0f02c9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 242
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/event/kj;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/kj;-><init>(Lcom/android/calendar/event/SelectStickerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 249
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->e:Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 279
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 281
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 289
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e()V

    .line 292
    if-eqz p3, :cond_0

    .line 293
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iput-boolean v5, v0, Lcom/android/calendar/event/SelectStickerFragment;->f:Z

    .line 294
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    const-string v1, "tabId"

    invoke-virtual {p3, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    goto :goto_0

    .line 301
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e()V

    goto :goto_0

    .line 306
    :sswitch_2
    if-eqz p3, :cond_3

    .line 307
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 308
    const-string v1, "SelectStickerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "imgUri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    if-eqz v1, :cond_2

    .line 310
    const-string v1, "SelectStickerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCropUri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_2
    if-eqz v0, :cond_3

    .line 313
    const-string v1, "content"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 314
    invoke-direct {p0, p3}, Lcom/android/calendar/event/SelectStickerActivity;->a(Landroid/content/Intent;)V

    .line 325
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e()V

    .line 327
    if-eqz p3, :cond_0

    .line 328
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iput-boolean v5, v0, Lcom/android/calendar/event/SelectStickerFragment;->f:Z

    .line 329
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    const-string v1, "tabId"

    invoke-virtual {p3, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    goto/16 :goto_0

    .line 315
    :cond_4
    const-string v1, "file"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 316
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 317
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/calendar/gx;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 319
    :cond_5
    invoke-direct {p0, p3}, Lcom/android/calendar/event/SelectStickerActivity;->a(Landroid/content/Intent;)V

    goto :goto_1

    .line 334
    :sswitch_3
    if-eqz p3, :cond_0

    .line 335
    invoke-direct {p0, p3}, Lcom/android/calendar/event/SelectStickerActivity;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 285
    :sswitch_data_0
    .sparse-switch
        0x321 -> :sswitch_0
        0x385 -> :sswitch_1
        0x2329 -> :sswitch_2
        0x232a -> :sswitch_3
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 256
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 258
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerActivity;->b()V

    .line 259
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 260
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 261
    new-instance v1, Lcom/android/calendar/event/kk;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/kk;-><init>(Lcom/android/calendar/event/SelectStickerActivity;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 268
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->invalidateOptionsMenu()V

    .line 269
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const v2, 0x7f12006b

    .line 62
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerActivity;->setContentView(I)V

    .line 65
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->c:Z

    .line 67
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    .line 69
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/android/calendar/event/SelectStickerFragment;

    invoke-direct {v0}, Lcom/android/calendar/event/SelectStickerFragment;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    .line 71
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 73
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 74
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 77
    :cond_0
    invoke-static {p0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 79
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 82
    :cond_1
    const v0, 0x7f0f03ce

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerActivity;->setTitle(I)V

    .line 84
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerActivity;->b()V

    .line 85
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 125
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 126
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11000e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 128
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 131
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0f03ce

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public onModeChanged(Z)V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 367
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 137
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 172
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 140
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerActivity;->finish()V

    .line 141
    const/4 v0, 0x0

    const v2, 0x7f050012

    invoke-virtual {p0, v0, v2}, Lcom/android/calendar/event/SelectStickerActivity;->overridePendingTransition(II)V

    move v0, v1

    .line 142
    goto :goto_0

    .line 144
    :sswitch_1
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerActivity;->c()V

    move v0, v1

    .line 145
    goto :goto_0

    .line 147
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/calendar/sticker/StickerDeleteActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 148
    const v2, 0x20008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 149
    const/16 v2, 0x385

    invoke-virtual {p0, v0, v2}, Lcom/android/calendar/event/SelectStickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 150
    goto :goto_0

    .line 152
    :sswitch_3
    invoke-static {p0}, Lcom/android/calendar/hj;->x(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/android/calendar/sticker/StickerDownloadListActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    const/4 v0, 0x2

    .line 155
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 156
    const/4 v0, 0x3

    .line 158
    :cond_0
    const-string v3, "category_id"

    iget-object v4, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget v4, v4, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    if-gt v4, v1, :cond_1

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    const/16 v0, 0x321

    invoke-virtual {p0, v2, v0}, Lcom/android/calendar/event/SelectStickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_2
    move v0, v1

    .line 169
    goto :goto_0

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    goto :goto_1

    .line 167
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerActivity;->d()V

    goto :goto_2

    .line 137
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f120348 -> :sswitch_1
        0x7f120349 -> :sswitch_2
        0x7f12034a -> :sswitch_3
    .end sparse-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 393
    const-string v0, "key_crop_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const-string v0, "key_crop_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    .line 396
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0}, Lcom/android/calendar/b;->onResume()V

    .line 90
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/calendar/event/EditEventActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    const-string v1, "event_create"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 95
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 386
    const-string v0, "key_crop_uri"

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 387
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 388
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 373
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    .line 375
    :cond_0
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerActivity;->b:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 382
    :cond_0
    return-void
.end method
