.class final Lcom/android/calendar/event/kt;
.super Ljava/lang/Object;
.source "SelectStickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1037
    const/4 v0, 0x0

    .line 1038
    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 1039
    iget v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    const/4 v4, 0x4

    if-gt v3, v4, :cond_0

    .line 1040
    iget-object v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/calendar/event/SelectStickerFragment;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1041
    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1042
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->g()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 1043
    :try_start_0
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v5

    iget-wide v6, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1044
    monitor-exit v4

    .line 1047
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1048
    goto :goto_0

    .line 1044
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1049
    :cond_1
    const-string v0, "SelectStickerFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load complete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    return-void
.end method
