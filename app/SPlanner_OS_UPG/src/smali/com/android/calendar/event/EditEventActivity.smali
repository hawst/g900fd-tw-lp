.class public Lcom/android/calendar/event/EditEventActivity;
.super Lcom/android/calendar/b;
.source "EditEventActivity.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;
.implements Landroid/app/ActionBar$TabListener;
.implements Lcom/android/calendar/event/la;


# static fields
.field public static final c:Landroid/net/Uri;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field private static f:Z

.field private static r:I


# instance fields
.field private A:Z

.field private B:Z

.field private C:Landroid/view/View$OnClickListener;

.field public b:Ljava/util/ArrayList;

.field private g:Lcom/android/calendar/event/m;

.field private h:Lcom/android/calendar/event/ak;

.field private i:Lcom/android/calendar/event/fq;

.field private j:Lcom/android/calendar/aq;

.field private k:Ljava/util/ArrayList;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private n:Z

.field private o:Lcom/android/calendar/gl;

.field private p:I

.field private q:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Landroid/graphics/Bitmap;

.field private w:Z

.field private x:I

.field private y:Landroid/os/Handler;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 179
    sput v2, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 198
    const-string v0, "content://com.android.calendar/maps"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    .line 200
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "event_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/calendar/event/EditEventActivity;->d:[Ljava/lang/String;

    .line 206
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "map"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/calendar/event/EditEventActivity;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    .line 138
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    .line 140
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    .line 142
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    .line 157
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->l:Landroid/widget/Button;

    .line 159
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->m:Landroid/widget/Button;

    .line 161
    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->n:Z

    .line 171
    iput v1, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    .line 173
    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    .line 181
    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->s:Z

    .line 192
    const v0, 0x55d4a81

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->t:I

    .line 194
    const v0, 0xaba9501

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->u:I

    .line 196
    iput-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    .line 236
    iput-boolean v3, p0, Lcom/android/calendar/event/EditEventActivity;->w:Z

    .line 238
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    .line 242
    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->z:Z

    .line 244
    iput-boolean v3, p0, Lcom/android/calendar/event/EditEventActivity;->A:Z

    .line 246
    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->B:Z

    .line 1581
    new-instance v0, Lcom/android/calendar/event/ai;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/ai;-><init>(Lcom/android/calendar/event/EditEventActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->C:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a(J)J
    .locals 11

    .prologue
    const/16 v10, 0x76e

    const/16 v5, 0x3b

    const/16 v2, 0xb

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 667
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 668
    const/16 v1, 0x7f4

    const/16 v3, 0x1f

    const/16 v4, 0x17

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 670
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 671
    invoke-virtual {v7, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 673
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    .line 674
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/al;->i()V

    .line 675
    invoke-virtual {v0, v2, v8}, Ljava/util/Calendar;->set(II)V

    .line 676
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 677
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v8}, Ljava/util/Calendar;->set(II)V

    .line 678
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p1

    .line 688
    :cond_0
    :goto_0
    return-wide p1

    :cond_1
    move v1, v10

    move v2, v8

    move v3, v9

    move v4, v8

    move v5, v8

    move v6, v8

    .line 680
    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 681
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    if-ne v0, v9, :cond_0

    .line 682
    invoke-static {p0}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/al;->i()V

    .line 684
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    move v1, v8

    move v2, v8

    move v3, v8

    move v4, v9

    move v5, v8

    move v6, v10

    .line 685
    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 686
    invoke-virtual {v0, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide p1

    goto :goto_0
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 531
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 532
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 533
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 534
    instance-of v3, v0, Landroid/widget/Spinner;

    if-eqz v3, :cond_1

    .line 544
    :cond_0
    :goto_1
    return-object v0

    .line 537
    :cond_1
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 538
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 539
    if-eqz v0, :cond_2

    instance-of v3, v0, Landroid/widget/Spinner;

    if-nez v3, :cond_0

    .line 532
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 544
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/os/Bundle;)Lcom/android/calendar/aq;
    .locals 12

    .prologue
    .line 580
    new-instance v4, Lcom/android/calendar/aq;

    invoke-direct {v4}, Lcom/android/calendar/aq;-><init>()V

    .line 581
    const-wide/16 v0, -0x1

    .line 583
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 584
    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 586
    if-eqz v2, :cond_4

    .line 588
    :try_start_0
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 596
    :cond_0
    :goto_0
    const-string v2, "beginTime"

    const-wide/16 v6, -0x1

    invoke-virtual {v5, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 597
    const-string v2, "endTime"

    const-wide/16 v8, -0x1

    invoke-virtual {v5, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 599
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-eqz v8, :cond_1

    .line 600
    invoke-direct {p0, v6, v7}, Lcom/android/calendar/event/EditEventActivity;->a(J)J

    move-result-wide v6

    .line 601
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    iput-object v8, v4, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    .line 602
    iget-object v8, v4, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v8, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 603
    iget-object v8, v4, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/text/format/Time;->normalize(Z)J

    .line 605
    :cond_1
    const-wide/16 v8, -0x1

    cmp-long v8, v2, v8

    if-eqz v8, :cond_2

    .line 606
    invoke-direct {p0, v2, v3}, Lcom/android/calendar/event/EditEventActivity;->a(J)J

    move-result-wide v2

    .line 607
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    iput-object v8, v4, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    .line 608
    iget-object v8, v4, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v8, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 611
    :cond_2
    iput-wide v0, v4, Lcom/android/calendar/aq;->c:J

    .line 613
    const-string v8, "allDay"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 614
    const-wide/16 v8, 0x10

    iput-wide v8, v4, Lcom/android/calendar/aq;->p:J

    .line 619
    :goto_1
    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    if-nez v8, :cond_6

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 620
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 621
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    :cond_3
    move-object v0, v4

    .line 663
    :goto_2
    return-object v0

    .line 589
    :catch_0
    move-exception v2

    .line 590
    const-string v2, "EditEventActivity"

    const-string v3, "Create new event"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 592
    :cond_4
    if-eqz p1, :cond_0

    const-string v2, "key_event_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 593
    const-string v0, "key_event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 616
    :cond_5
    const-wide/16 v8, 0x0

    iput-wide v8, v4, Lcom/android/calendar/aq;->p:J

    goto :goto_1

    .line 626
    :cond_6
    const-string v8, "edit_mode"

    const-wide/16 v10, -0x1

    invoke-virtual {v5, v8, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-wide/16 v10, 0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_7

    .line 627
    const/4 v8, 0x1

    sput v8, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 630
    :cond_7
    const-string v8, "task"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 631
    const/4 v8, 0x2

    sput v8, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 632
    const-string v8, "selected"

    const-wide/16 v10, -0x1

    invoke-virtual {v5, v8, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    .line 633
    const/4 v8, 0x2

    iput v8, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    .line 635
    :cond_8
    const-string v8, "task"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 638
    :cond_9
    const-string v8, "event_create"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 639
    const/4 v8, 0x1

    sput v8, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 640
    const-string v8, "event_create"

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 643
    :cond_a
    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-eqz v0, :cond_b

    .line 644
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    .line 645
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 648
    :cond_b
    const-string v0, "copyEvent"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 649
    const-wide/32 v0, 0x200000

    iput-wide v0, v4, Lcom/android/calendar/aq;->a:J

    .line 652
    :cond_c
    const-string v0, "simple_add_flag"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v4, Lcom/android/calendar/aq;->m:Z

    .line 653
    const-string v0, "simple_add_account_position"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v4, Lcom/android/calendar/aq;->n:I

    .line 659
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-lez v0, :cond_d

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_d

    sub-long v0, v2, v6

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    .line 660
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    :cond_d
    move-object v0, v4

    .line 663
    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/android/calendar/event/EditEventActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/calendar/event/EditEventActivity;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->k:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1470
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1473
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1476
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1478
    :cond_0
    return-void
.end method

.method private a(Landroid/app/ActionBar;)V
    .locals 6

    .prologue
    const-wide/32 v4, 0x200000

    .line 384
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-eqz v0, :cond_1

    .line 385
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 386
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 387
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-eqz v0, :cond_1

    .line 388
    const/4 v0, 0x0

    .line 389
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v1, :pswitch_data_0

    .line 405
    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(I)V

    .line 408
    :cond_1
    return-void

    .line 391
    :pswitch_0
    const v0, 0x7f0f018a

    .line 392
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-wide v2, v1, Lcom/android/calendar/aq;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 393
    const v0, 0x7f0f0103

    goto :goto_0

    .line 397
    :pswitch_1
    const v0, 0x7f0f01a2

    .line 398
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-wide v2, v1, Lcom/android/calendar/aq;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 399
    const v0, 0x7f0f0104

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    .locals 1

    .prologue
    .line 1489
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Fragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1490
    invoke-virtual {p0, p1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1491
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/EditEventActivity;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/calendar/event/EditEventActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/EditEventActivity;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/EditEventActivity;->a(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/EditEventActivity;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->z:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/EditEventActivity;Z)Z
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/android/calendar/event/EditEventActivity;->A:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    return-object v0
.end method

.method private b(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1495
    const/4 v0, 0x0

    .line 1496
    if-eqz p1, :cond_3

    .line 1497
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1498
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1499
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    move v3, v4

    .line 1500
    :goto_0
    if-ge v3, v5, :cond_2

    .line 1501
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1502
    const-string v7, ";"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1503
    array-length v8, v7

    .line 1504
    if-le v8, v2, :cond_1

    .line 1505
    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    move v0, v2

    .line 1506
    :goto_1
    if-ge v0, v8, :cond_1

    .line 1507
    aget-object v9, v7, v0

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1508
    add-int/lit8 v9, v8, -0x1

    if-ge v0, v9, :cond_0

    .line 1509
    const/16 v9, 0x3b

    invoke-virtual {v6, v9}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1506
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1513
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1500
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1516
    :cond_3
    return-object v0
.end method

.method private b(Landroid/app/ActionBar;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 411
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-nez v0, :cond_1

    .line 415
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->j()V

    .line 420
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 422
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->k()V

    .line 424
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->o()V

    .line 425
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/gl;->a(I)V

    .line 427
    sget-boolean v0, Lcom/android/calendar/event/EditEventActivity;->f:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_0

    .line 428
    invoke-virtual {p1, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 430
    :cond_0
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 432
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/android/calendar/event/EditEventActivity;)Z
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->r()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/m;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 376
    if-nez v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/ActionBar;)V

    .line 380
    invoke-direct {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->b(Landroid/app/ActionBar;)V

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 435
    invoke-static {p0}, Lcom/android/calendar/dz;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-static {p0}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->w:Z

    .line 438
    :cond_0
    sget-boolean v0, Lcom/android/calendar/event/EditEventActivity;->f:Z

    if-eqz v0, :cond_2

    .line 439
    const v0, 0x7f12006b

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 440
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->w:Z

    if-nez v0, :cond_4

    .line 442
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setDimAmount(F)V

    .line 443
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 444
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 445
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 447
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 448
    invoke-virtual {v3, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 460
    :goto_1
    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    .line 462
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 436
    goto :goto_0

    .line 450
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/Window;->setDimAmount(F)V

    .line 451
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 452
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0c0446

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 454
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 455
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 457
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 458
    invoke-virtual {v3, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1
.end method

.method private j()V
    .locals 3

    .prologue
    .line 490
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 492
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    if-nez v1, :cond_0

    .line 493
    new-instance v1, Lcom/android/calendar/gl;

    const v2, 0x7f040006

    invoke-direct {v1, p0, v2, v0}, Lcom/android/calendar/gl;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    .line 495
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    const v1, 0x7f040005

    invoke-virtual {v0, v1}, Lcom/android/calendar/gl;->setDropDownViewResource(I)V

    .line 497
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    invoke-virtual {v0, v1, p0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 499
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 506
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->l()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 507
    invoke-direct {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 508
    if-eqz v1, :cond_0

    .line 509
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 510
    instance-of v2, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v2, :cond_0

    .line 511
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 512
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c036a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 514
    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 515
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0172

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 517
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 520
    :cond_0
    return-void
.end method

.method private l()Landroid/view/View;
    .locals 5

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 524
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 526
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "action_bar"

    const-string v3, "id"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 527
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 715
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 716
    const-string v0, "EditEventActivity"

    const-string v1, "press onBackPressed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_1

    .line 718
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->f()V

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 721
    :cond_1
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->c()V

    goto :goto_0

    .line 726
    :cond_2
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 728
    :pswitch_0
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_3

    .line 729
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    if-eqz v0, :cond_0

    .line 730
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-virtual {v0}, Lcom/android/calendar/event/m;->b()V

    goto :goto_0

    .line 733
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->f()V

    goto :goto_0

    .line 739
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->c()V

    goto :goto_0

    .line 726
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private n()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 1041
    .line 1044
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    .line 1045
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    .line 1047
    :goto_0
    new-array v7, v1, [Z

    .line 1048
    invoke-static {v7, v2}, Ljava/util/Arrays;->fill([ZZ)V

    move v6, v2

    move v5, v2

    move v3, v2

    .line 1049
    :goto_1
    if-ge v6, v1, :cond_3

    .line 1050
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1051
    const-string v8, ";"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1052
    const-string v0, ""

    .line 1053
    array-length v9, v8

    if-le v9, v4, :cond_0

    .line 1054
    aget-object v0, v8, v4

    .line 1056
    :cond_0
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1057
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1058
    aput-boolean v4, v7, v6

    move v3, v4

    .line 1068
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v8, :cond_8

    .line 1069
    iget-object v8, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v8, v0}, Lcom/android/calendar/event/ak;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1070
    aput-boolean v4, v7, v6

    move v0, v4

    .line 1049
    :goto_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v0

    goto :goto_1

    .line 1062
    :cond_2
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1063
    aput-boolean v4, v7, v6

    move v3, v4

    .line 1064
    goto :goto_2

    .line 1076
    :cond_3
    add-int/lit8 v0, v1, -0x1

    :goto_4
    if-ltz v0, :cond_5

    .line 1077
    aget-boolean v1, v7, v0

    if-eqz v1, :cond_4

    .line 1078
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1076
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 1082
    :cond_5
    if-eqz v3, :cond_7

    .line 1083
    const v0, 0x7f0f024b

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1087
    :cond_6
    :goto_5
    return-void

    .line 1084
    :cond_7
    if-eqz v5, :cond_6

    if-nez v3, :cond_6

    .line 1085
    const v0, 0x7f0f0454

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_5

    :cond_8
    move v0, v5

    goto :goto_3

    :cond_9
    move v1, v2

    goto/16 :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1090
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1092
    if-nez v0, :cond_0

    .line 1093
    const-string v0, "EditEventActivity"

    const-string v1, "ActionBar is null."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    :goto_0
    return-void

    .line 1095
    :cond_0
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v1

    const v2, 0x7f0f01b3

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 1097
    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v1

    const v2, 0x7f0f01b5

    invoke-virtual {v1, v2}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    goto :goto_0
.end method

.method private p()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 1402
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->q()V

    .line 1403
    new-instance v0, Lcom/android/calendar/event/aj;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/aj;-><init>(Lcom/android/calendar/event/EditEventActivity;Landroid/content/ContentResolver;)V

    .line 1405
    new-array v4, v10, [Ljava/lang/String;

    const-string v1, "display_name"

    aput-object v1, v4, v8

    .line 1408
    const-string v5, "_id=?"

    .line 1409
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1410
    iput-boolean v8, p0, Lcom/android/calendar/event/EditEventActivity;->A:Z

    .line 1412
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v1, v8

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1413
    const-string v6, ";"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v7, v3, v8

    .line 1414
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v6, v10, [Ljava/lang/String;

    aput-object v7, v6, v8

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/aj;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    add-int/lit8 v1, v1, 0x1

    .line 1419
    goto :goto_0

    .line 1420
    :cond_1
    return-void
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1423
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1424
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->k:Ljava/util/ArrayList;

    move v0, v1

    .line 1425
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1426
    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->k:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1428
    :cond_0
    return-void
.end method

.method private r()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1481
    .line 1482
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1483
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    move v1, v0

    .line 1484
    goto :goto_0

    .line 1483
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1485
    :cond_1
    return v1
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1567
    sget-boolean v0, Lcom/android/calendar/event/EditEventActivity;->f:Z

    if-nez v0, :cond_3

    .line 1568
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_1

    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-ne v0, v1, :cond_1

    .line 1570
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_1

    .line 1571
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/view/View;)V

    .line 1573
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_3

    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-ne v0, v2, :cond_3

    .line 1575
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_3

    .line 1576
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;)V

    .line 1579
    :cond_3
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 1249
    iput p1, p0, Lcom/android/calendar/event/EditEventActivity;->t:I

    .line 1250
    iput p2, p0, Lcom/android/calendar/event/EditEventActivity;->u:I

    .line 1251
    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/event/ak;->a(JLjava/lang/String;)V

    .line 1530
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 1262
    iput-object p1, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    .line 1263
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1035
    iput-object p1, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    .line 1036
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->n()V

    .line 1037
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->p()V

    .line 1038
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 560
    iput-boolean p1, p0, Lcom/android/calendar/event/EditEventActivity;->B:Z

    .line 561
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 791
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 795
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 793
    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    goto :goto_0
.end method

.method public b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1533
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_2

    .line 1534
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1535
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1536
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1537
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1538
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 1539
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1540
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f040038

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 1542
    const v0, 0x7f1200d4

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1543
    const v1, 0x7f1200d5

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1544
    if-eqz v1, :cond_0

    .line 1545
    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1547
    :cond_0
    if-eqz v0, :cond_1

    .line 1548
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1564
    :cond_1
    :goto_0
    return-void

    .line 1552
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->l:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 1553
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1556
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->m:Landroid/widget/Button;

    if-eqz v0, :cond_4

    .line 1557
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->m:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1560
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1561
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1562
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1254
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->t:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1258
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->u:I

    return v0
.end method

.method public e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1520
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1524
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v2, 0x9

    const/4 v1, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 853
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 855
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->h()V

    .line 859
    :cond_0
    if-eq p2, v5, :cond_5

    .line 860
    sparse-switch p1, :sswitch_data_0

    .line 914
    :cond_1
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    if-eq v0, v5, :cond_2

    .line 915
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 916
    const-string v1, "action_view_focus"

    iget v2, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 917
    iput v5, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    .line 1032
    :cond_2
    :goto_1
    return-void

    .line 862
    :sswitch_0
    iput v2, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    goto :goto_0

    .line 866
    :sswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 867
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 869
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ak;->a(Z)V

    .line 870
    iput v1, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    goto :goto_0

    .line 874
    :sswitch_2
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 875
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 876
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->w()V

    goto :goto_0

    .line 882
    :sswitch_3
    if-eqz p3, :cond_1

    .line 885
    const-string v0, "request_module"

    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 886
    const/16 v1, 0xa

    .line 887
    const-string v2, "osp"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 888
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 890
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_1

    .line 891
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ay;->e(Z)V

    .line 893
    if-eqz v2, :cond_1

    const-string v0, "SAC_0301"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 894
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->b(I)V

    goto/16 :goto_0

    .line 900
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_4

    .line 901
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/fx;->b(Z)V

    .line 904
    :cond_4
    if-eqz v2, :cond_1

    const-string v0, "SAC_0301"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 905
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/fx;->a(I)V

    goto/16 :goto_0

    .line 923
    :cond_5
    sparse-switch p1, :sswitch_data_1

    .line 1027
    :cond_6
    :goto_2
    iget v0, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    if-eq v0, v5, :cond_2

    .line 1028
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1029
    const-string v1, "action_view_focus"

    iget v2, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1030
    iput v5, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    goto/16 :goto_1

    .line 925
    :sswitch_4
    iput v1, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    .line 926
    if-eqz p3, :cond_6

    .line 927
    const-string v0, "result"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Ljava/util/ArrayList;)V

    .line 928
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ak;->a(Z)V

    .line 930
    sget-object v0, Lcom/android/calendar/hj;->q:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_2

    .line 935
    :sswitch_5
    iput v2, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    .line 938
    const-string v0, "latitude"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 939
    const-string v1, "longitude"

    invoke-virtual {p3, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 940
    const-string v2, "location"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 945
    const-string v3, "map"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v3

    .line 946
    if-eqz v3, :cond_6

    .line 947
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 948
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 949
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v4, v4, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v4, :cond_6

    .line 950
    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->t:I

    .line 951
    iput v1, p0, Lcom/android/calendar/event/EditEventActivity;->u:I

    .line 952
    iput-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    .line 953
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v3, v2}, Lcom/android/calendar/event/ay;->a(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    goto :goto_2

    .line 960
    :sswitch_6
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->x:I

    .line 961
    const-string v0, "_id"

    const-wide/16 v2, 0x0

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 962
    const-string v2, "filepath"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 963
    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v3, v3, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    if-eqz v3, :cond_7

    .line 964
    iget-object v3, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v3, v3, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 966
    :cond_7
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/android/calendar/event/ag;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/android/calendar/event/ag;-><init>(Lcom/android/calendar/event/EditEventActivity;JLjava/lang/String;)V

    const-wide/16 v0, 0xc8

    invoke-virtual {v3, v4, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 978
    :sswitch_7
    if-eqz p3, :cond_6

    .line 981
    const-string v0, "request_module"

    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 984
    packed-switch v0, :pswitch_data_1

    goto/16 :goto_2

    .line 986
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_6

    .line 987
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/ay;->e(Z)V

    goto/16 :goto_2

    .line 992
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v0, :cond_6

    .line 993
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/fx;->b(Z)V

    goto/16 :goto_2

    .line 1000
    :sswitch_8
    if-eqz p3, :cond_6

    .line 1001
    const-string v0, "EditEventActivity"

    const-string v1, "get new ContactInfo from Contact"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1004
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_6

    .line 1005
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ay;->a(Landroid/net/Uri;)V

    goto/16 :goto_2

    .line 1011
    :sswitch_9
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/android/calendar/event/ah;

    invoke-direct {v1, p0, p3}, Lcom/android/calendar/event/ah;-><init>(Lcom/android/calendar/event/EditEventActivity;Landroid/content/Intent;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 860
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_0
        0x72 -> :sswitch_2
        0x1f4 -> :sswitch_3
    .end sparse-switch

    .line 888
    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 923
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_4
        0x65 -> :sswitch_5
        0x72 -> :sswitch_6
        0x78 -> :sswitch_8
        0x131 -> :sswitch_9
        0x1f4 -> :sswitch_7
    .end sparse-switch

    .line 984
    :pswitch_data_1
    .packed-switch 0x1f5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 698
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->m()V

    .line 699
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 700
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 701
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->r()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 709
    :cond_2
    invoke-super {p0}, Lcom/android/calendar/b;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1271
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1273
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1274
    if-nez v0, :cond_0

    .line 1296
    :goto_0
    return-void

    .line 1278
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->i()V

    .line 1280
    sget-boolean v1, Lcom/android/calendar/event/EditEventActivity;->f:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    if-nez v1, :cond_1

    .line 1281
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->w:Z

    if-eqz v1, :cond_2

    .line 1283
    iput-boolean v2, p0, Lcom/android/calendar/event/EditEventActivity;->s:Z

    .line 1284
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1285
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->s:Z

    .line 1291
    :goto_1
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 1294
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->invalidateOptionsMenu()V

    .line 1295
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->s()V

    goto :goto_0

    .line 1288
    :cond_2
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1289
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    sget v2, Lcom/android/calendar/event/EditEventActivity;->r:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/android/calendar/gl;->a(I)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const-wide/16 v12, -0x1

    const v10, 0x7f12006b

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 250
    const-string v0, "onCreate.EditEventActivity"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 251
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 253
    invoke-static {}, Lcom/android/calendar/hj;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    const v0, 0x7f0f02fe

    invoke-static {p0, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 256
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->finish()V

    .line 259
    :cond_0
    const v0, 0x7f040099

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/EditEventActivity;->setContentView(I)V

    .line 261
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_1

    .line 263
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 264
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v3, v3, 0x3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 266
    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 269
    :cond_1
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/event/EditEventActivity;->f:Z

    .line 271
    invoke-direct {p0, p1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/os/Bundle;)Lcom/android/calendar/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    .line 273
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    .line 275
    if-eqz p1, :cond_2

    .line 276
    const-string v0, "mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 277
    const-string v0, "tab_state_edit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-wide v2, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v2, v12

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-ltz v0, :cond_3

    .line 284
    sput v8, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 287
    :cond_3
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->h()V

    .line 289
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 290
    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 292
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_4

    .line 293
    sput v8, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 294
    const-string v0, "EasyEditEventFragment"

    invoke-virtual {v3, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/m;

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    .line 300
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 301
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v0, :pswitch_data_0

    .line 365
    :goto_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->y:Landroid/os/Handler;

    .line 367
    iput-boolean v8, p0, Lcom/android/calendar/event/EditEventActivity;->n:Z

    .line 369
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->i()V

    .line 371
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 372
    return-void

    .line 303
    :pswitch_0
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_6

    .line 304
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    if-nez v0, :cond_5

    .line 305
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v6, v12

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 308
    :goto_1
    new-instance v1, Lcom/android/calendar/event/m;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v1, v2, v0}, Lcom/android/calendar/event/m;-><init>(Lcom/android/calendar/aq;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    .line 309
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    const-string v1, "EasyEditEventFragment"

    invoke-virtual {v4, v10, v0, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 312
    :cond_5
    invoke-virtual {p0, v8}, Lcom/android/calendar/event/EditEventActivity;->b(Z)V

    .line 313
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 337
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 339
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_9

    .line 340
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 344
    :goto_3
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 315
    :cond_6
    const-string v0, "EditEventFragment"

    invoke-virtual {v3, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ak;

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    .line 316
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-nez v0, :cond_8

    .line 317
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v6, v12

    if-eqz v0, :cond_c

    .line 321
    :goto_4
    new-instance v0, Lcom/android/calendar/event/ak;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v0, v2, v9, v1}, Lcom/android/calendar/event/ak;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    .line 322
    if-eqz v1, :cond_7

    .line 323
    const-string v0, "contact_uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    .line 324
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 325
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->p()V

    .line 328
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    const-string v1, "EditEventFragment"

    invoke-virtual {v4, v10, v0, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 331
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "editMode"

    invoke-virtual {v1, v2, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/calendar/event/ak;->c:Z

    .line 334
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_2

    .line 342
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 348
    :pswitch_1
    const-string v0, "EditTaskFragment"

    invoke-virtual {v3, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/fq;

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    .line 349
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-nez v0, :cond_a

    .line 350
    new-instance v0, Lcom/android/calendar/event/fq;

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v0, v1, v9, v2}, Lcom/android/calendar/event/fq;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    .line 351
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    const-string v1, "EditTaskFragment"

    invoke-virtual {v4, v10, v0, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 354
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 355
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v0, :cond_b

    .line 356
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-static {v4, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    .line 358
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 359
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    :cond_c
    move-object v1, v2

    goto :goto_4

    :cond_d
    move-object v0, v2

    goto/16 :goto_1

    .line 301
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f1200d5

    const v2, 0x7f1200d4

    .line 751
    const-string v0, "onCreateOptionsMenu.EditEventActivity"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 752
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 754
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-nez v0, :cond_0

    .line 755
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 757
    sget-boolean v0, Lcom/android/calendar/event/EditEventActivity;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    .line 759
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 760
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f020057

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 785
    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 786
    const/4 v0, 0x1

    return v0

    .line 766
    :cond_1
    const-string v0, "tg"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 767
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->y:Landroid/os/Handler;

    new-instance v1, Lcom/android/calendar/event/af;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/af;-><init>(Lcom/android/calendar/event/EditEventActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 781
    :cond_2
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 782
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->B:Z

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 552
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->v:Landroid/graphics/Bitmap;

    .line 554
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->B:Z

    .line 556
    :cond_1
    invoke-super {p0}, Lcom/android/calendar/b;->onDestroy()V

    .line 557
    return-void
.end method

.method public onNavigationItemSelected(IJ)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f12006b

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1173
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    add-int/lit8 v1, p1, 0x1

    if-ne v0, v1, :cond_0

    .line 1245
    :goto_0
    return v3

    .line 1176
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->o:Lcom/android/calendar/gl;

    invoke-virtual {v0, p1}, Lcom/android/calendar/gl;->a(I)V

    .line 1178
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1180
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1182
    :pswitch_0
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v1, :pswitch_data_1

    .line 1194
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v1, v1, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    if-eqz v1, :cond_1

    .line 1195
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    iget-object v1, v1, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v1}, Lcom/android/calendar/event/fx;->a()Z

    .line 1198
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-nez v1, :cond_5

    .line 1199
    new-instance v1, Lcom/android/calendar/event/ak;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v1, v2, v4, v6}, Lcom/android/calendar/event/ak;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    .line 1200
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    const-string v2, "EditEventFragment"

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1206
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1207
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1208
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_3

    .line 1209
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->j()V

    .line 1211
    :cond_3
    sput v3, Lcom/android/calendar/event/EditEventActivity;->r:I

    goto :goto_0

    .line 1184
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v1, :cond_4

    .line 1185
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v1}, Lcom/android/calendar/event/fq;->d()V

    .line 1187
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_1

    .line 1202
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_2

    .line 1203
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->k()V

    goto :goto_2

    .line 1214
    :pswitch_2
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v1, :pswitch_data_2

    .line 1226
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_6

    .line 1227
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->c()Z

    .line 1231
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-nez v1, :cond_7

    .line 1232
    new-instance v1, Lcom/android/calendar/event/fq;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v1, v2, v4, v6}, Lcom/android/calendar/event/fq;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    .line 1233
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    const-string v2, "EditTaskFragment"

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1235
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1236
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1237
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_8

    .line 1238
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->e()V

    .line 1240
    :cond_8
    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    goto/16 :goto_0

    .line 1216
    :pswitch_3
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_9

    .line 1217
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->i()V

    .line 1219
    :cond_9
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_3

    .line 1180
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1182
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch

    .line 1214
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 799
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 848
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    .line 801
    :sswitch_0
    invoke-direct {p0}, Lcom/android/calendar/event/EditEventActivity;->m()V

    .line 802
    invoke-static {p0}, Lcom/android/calendar/hj;->p(Landroid/content/Context;)V

    .line 803
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 804
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->finish()V

    goto :goto_0

    .line 809
    :sswitch_1
    iget-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v1, :cond_2

    .line 810
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-virtual {v1}, Lcom/android/calendar/event/m;->a()V

    goto :goto_1

    .line 812
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->b()V

    .line 813
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-ne v1, v0, :cond_3

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_3

    .line 814
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->e()V

    goto :goto_1

    .line 815
    :cond_3
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v1, :cond_1

    .line 816
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v1}, Lcom/android/calendar/event/fq;->b()V

    goto :goto_1

    .line 822
    :sswitch_2
    iget-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->A:Z

    if-eqz v1, :cond_0

    .line 823
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 824
    iget-boolean v1, p0, Lcom/android/calendar/event/EditEventActivity;->q:Z

    if-eqz v1, :cond_5

    .line 825
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->g:Lcom/android/calendar/event/m;

    invoke-virtual {v1}, Lcom/android/calendar/event/m;->c()Z

    .line 826
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->finish()V

    .line 840
    :cond_4
    :goto_2
    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 828
    :cond_5
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->b()V

    .line 829
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-ne v1, v0, :cond_7

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_7

    .line 830
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 831
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->b()Z

    move-result v1

    if-nez v1, :cond_4

    .line 832
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->d()V

    goto :goto_2

    .line 834
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->d()V

    goto :goto_2

    .line 836
    :cond_7
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v1, :cond_4

    .line 837
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v1}, Lcom/android/calendar/event/fq;->a()V

    goto :goto_2

    .line 799
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f1200d4 -> :sswitch_1
        0x7f1200d5 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->z:Z

    .line 1310
    invoke-super {p0}, Lcom/android/calendar/b;->onPause()V

    .line 1311
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 573
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 575
    const-string v0, "mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    .line 576
    const-string v0, "tab_state_edit"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    .line 577
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 1300
    const-string v0, "onResume.EditEventActivity"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 1301
    invoke-super {p0}, Lcom/android/calendar/b;->onResume()V

    .line 1302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->z:Z

    .line 1303
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/gx;->a(Landroid/content/ContentResolver;)V

    .line 1304
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 1305
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 565
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 567
    const-string v0, "mode"

    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 568
    const-string v0, "tab_state_edit"

    iget v1, p0, Lcom/android/calendar/event/EditEventActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 569
    return-void
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 1104
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f12006b

    const/4 v3, 0x0

    .line 1108
    sget v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/EditEventActivity;->s:Z

    if-eqz v0, :cond_1

    .line 1164
    :cond_0
    :goto_0
    return-void

    .line 1112
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1114
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1116
    :pswitch_0
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v1, :pswitch_data_1

    .line 1127
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-nez v1, :cond_2

    .line 1128
    new-instance v1, Lcom/android/calendar/event/ak;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v1, v2, v3, v5}, Lcom/android/calendar/event/ak;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    .line 1129
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    const-string v2, "EditEventFragment"

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1131
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1132
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1133
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v0, :cond_3

    .line 1134
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->j()V

    .line 1136
    :cond_3
    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    goto :goto_0

    .line 1118
    :pswitch_1
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v1, :cond_4

    .line 1119
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v1}, Lcom/android/calendar/event/fq;->d()V

    .line 1121
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_1

    .line 1139
    :pswitch_2
    sget v1, Lcom/android/calendar/event/EditEventActivity;->r:I

    packed-switch v1, :pswitch_data_2

    .line 1150
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-nez v1, :cond_5

    .line 1151
    new-instance v1, Lcom/android/calendar/event/fq;

    iget-object v2, p0, Lcom/android/calendar/event/EditEventActivity;->j:Lcom/android/calendar/aq;

    invoke-direct {v1, v2, v3, v5}, Lcom/android/calendar/event/fq;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    iput-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    .line 1152
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    const-string v2, "EditTaskFragment"

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1154
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1155
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    if-eqz v0, :cond_6

    .line 1157
    iget-object v0, p0, Lcom/android/calendar/event/EditEventActivity;->i:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->e()V

    .line 1159
    :cond_6
    const/4 v0, 0x2

    sput v0, Lcom/android/calendar/event/EditEventActivity;->r:I

    goto :goto_0

    .line 1141
    :pswitch_3
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    if-eqz v1, :cond_7

    .line 1142
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-virtual {v1}, Lcom/android/calendar/event/ak;->i()V

    .line 1144
    :cond_7
    iget-object v1, p0, Lcom/android/calendar/event/EditEventActivity;->h:Lcom/android/calendar/event/ak;

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    goto :goto_2

    .line 1114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1116
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch

    .line 1139
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0

    .prologue
    .line 1168
    return-void
.end method
