.class Lcom/android/calendar/event/i;
.super Ljava/lang/Object;
.source "AttendeesView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/h;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/h;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 330
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 336
    packed-switch p2, :pswitch_data_0

    .line 383
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0

    .line 338
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-boolean v1, v1, Lcom/android/calendar/event/h;->f:Z

    if-eqz v1, :cond_2

    .line 339
    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v1, v1, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 340
    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v1, v1, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    const-string v2, "contact_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 341
    iget-object v2, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v2, v2, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 343
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 347
    const-string v1, "finishActivityOnSaveCompleted"

    invoke-virtual {v2, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 351
    :catch_0
    move-exception v0

    .line 352
    const-string v0, "AttendeesView"

    const-string v1, "Error: Could not find Contacts.CONTENT_URI activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 356
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.INSERT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    const-string v2, "vnd.android.cursor.dir/raw_contact"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 359
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 360
    iget-object v2, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v2, v2, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 361
    const-string v2, "phone"

    iget-object v3, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v3, v3, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    :goto_2
    const-string v2, "finishActivityOnSaveCompleted"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 371
    const/16 v2, 0x78

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 363
    :cond_3
    const-string v2, "email"

    iget-object v3, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v3, v3, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 366
    :cond_4
    const-string v2, "email"

    iget-object v3, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v3, v3, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 375
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v1, v1, Lcom/android/calendar/event/h;->d:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;Landroid/view/View;)V

    .line 376
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->c(Lcom/android/calendar/event/AttendeesView;)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v1, v1, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v0, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v0, v0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->c(Lcom/android/calendar/event/AttendeesView;)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/i;->a:Lcom/android/calendar/event/h;

    iget-object v1, v1, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v1}, Lcom/android/calendar/event/AttendeesView;->c(Lcom/android/calendar/event/AttendeesView;)Landroid/widget/MultiAutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setSelection(I)V

    goto/16 :goto_1

    .line 336
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
