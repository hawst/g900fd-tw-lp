.class public Lcom/android/calendar/event/f;
.super Lcom/b/a/a/b;
.source "AccountValidationCheckActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/AccountValidationCheckActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-direct {p0}, Lcom/b/a/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public b(IZLandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 87
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "onReceiveChecklistValidation"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    if-eqz p2, :cond_1

    .line 90
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "isSuccess"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "preferences_samsung_account_validation"

    invoke-static {v0, v1, p2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 93
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->b(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 98
    const-string v0, "AccountValidationCheckActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Error code]:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [Message] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->b(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0204"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "tnc_acceptance_required"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z

    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "name_verification _required"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->b(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z

    .line 105
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "email_validation_required"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->c(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z

    .line 106
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v1, "mandatory_input_required"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->d(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z

    .line 108
    const-string v0, "AccountValidationCheckActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tnc_acceptance_required "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->c(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v0, "AccountValidationCheckActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "name_verification "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->d(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    const-string v0, "AccountValidationCheckActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "email_validation_required "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->e(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const-string v0, "AccountValidationCheckActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mandatory_input_required "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->f(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAC_0301"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/android/calendar/event/f;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V

    goto/16 :goto_0
.end method

.method public c(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public d(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public e(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public f(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method
