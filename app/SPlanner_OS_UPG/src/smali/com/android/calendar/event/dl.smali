.class Lcom/android/calendar/event/dl;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 5478
    iput-object p1, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 5481
    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5491
    :cond_0
    :goto_0
    return-void

    .line 5484
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, p2}, Lcom/android/calendar/event/ay;->k(Lcom/android/calendar/event/ay;I)I

    .line 5485
    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->B:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->R(Lcom/android/calendar/event/ay;)[Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->S(Lcom/android/calendar/event/ay;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5486
    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 5487
    iget-object v0, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/dl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->S(Lcom/android/calendar/event/ay;)I

    move-result v1

    iput v1, v0, Lcom/android/calendar/as;->J:I

    .line 5490
    :cond_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method
