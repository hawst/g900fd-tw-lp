.class Lcom/android/calendar/event/id;
.super Ljava/lang/Thread;
.source "EventViewUtils.java"


# instance fields
.field a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/event/hp;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/hp;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 563
    iput-object p2, p0, Lcom/android/calendar/event/id;->a:Landroid/view/View;

    .line 564
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 568
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 570
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->b(Lcom/android/calendar/event/hp;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x2694

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 574
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/id;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v1}, Lcom/android/calendar/event/hp;->c(Lcom/android/calendar/event/hp;)Landroid/widget/ImageButton;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 575
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->d(Lcom/android/calendar/event/hp;)V

    .line 579
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->b(Lcom/android/calendar/event/hp;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/ie;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ie;-><init>(Lcom/android/calendar/event/id;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 585
    const-wide/16 v0, 0x12c

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 586
    :catch_0
    move-exception v0

    .line 587
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 577
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->e(Lcom/android/calendar/event/hp;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 590
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    invoke-virtual {v0}, Lcom/android/calendar/event/id;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    invoke-virtual {v0}, Lcom/android/calendar/event/id;->interrupt()V

    .line 592
    iget-object v0, p0, Lcom/android/calendar/event/id;->b:Lcom/android/calendar/event/hp;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    .line 594
    :cond_2
    return-void
.end method
