.class Lcom/android/calendar/event/hl;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;

.field private final b:I

.field private c:I

.field private d:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/fx;I)V
    .locals 1

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/android/calendar/event/hl;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1239
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/hl;->c:I

    .line 1240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/hl;->d:Landroid/widget/Toast;

    .line 1243
    iput p2, p0, Lcom/android/calendar/event/hl;->b:I

    .line 1244
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1269
    iget-object v0, p0, Lcom/android/calendar/event/hl;->d:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1270
    iget-object v0, p0, Lcom/android/calendar/event/hl;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0292

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1273
    iget-object v1, p0, Lcom/android/calendar/event/hl;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hl;->d:Landroid/widget/Toast;

    .line 1275
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/hl;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1276
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1249
    iget v0, p0, Lcom/android/calendar/event/hl;->b:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v1

    sub-int v2, p6, p5

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 1254
    iget v1, p0, Lcom/android/calendar/event/hl;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/calendar/event/hl;->c:I

    if-ltz v1, :cond_1

    :cond_0
    if-nez v0, :cond_1

    .line 1255
    iget v1, p0, Lcom/android/calendar/event/hl;->b:I

    invoke-direct {p0, v1}, Lcom/android/calendar/event/hl;->a(I)V

    .line 1257
    :cond_1
    iput v0, p0, Lcom/android/calendar/event/hl;->c:I

    .line 1258
    if-gtz v0, :cond_2

    .line 1259
    const-string v0, ""

    .line 1264
    :goto_0
    return-object v0

    .line 1260
    :cond_2
    sub-int v1, p3, p2

    if-lt v0, v1, :cond_3

    .line 1261
    const/4 v0, 0x0

    goto :goto_0

    .line 1263
    :cond_3
    iget v1, p0, Lcom/android/calendar/event/hl;->b:I

    invoke-direct {p0, v1}, Lcom/android/calendar/event/hl;->a(I)V

    .line 1264
    add-int/2addr v0, p2

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
