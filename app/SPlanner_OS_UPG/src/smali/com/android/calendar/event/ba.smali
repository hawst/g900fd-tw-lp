.class Lcom/android/calendar/event/ba;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 1297
    iput-object p1, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v5, 0x7f0f01ab

    const/16 v4, 0x8

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1304
    sget-boolean v0, Lcom/android/calendar/event/ay;->ar:Z

    if-nez v0, :cond_0

    .line 1305
    sput-boolean v6, Lcom/android/calendar/event/ay;->ar:Z

    .line 1306
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->v(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1307
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->performValidation()V

    .line 1308
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v1, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1310
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1311
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ad:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1312
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/AttendeesView;->setVisibility(I)V

    .line 1313
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->o(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1314
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1388
    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1389
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f02f3

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1391
    :cond_1
    return-void

    .line 1316
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1320
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1321
    const-string v0, "intent.action.INTERACTION_TAB"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1322
    const-string v0, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1324
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->v(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1325
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v2, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1327
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0f024b

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1337
    :cond_5
    :goto_1
    const-string v0, "existingRecipientCount"

    iget-object v2, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v2}, Lcom/android/calendar/event/AttendeesView;->getAttendeeItemCount()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1338
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1339
    const-string v0, "additional"

    const-string v2, "email-phone-multi"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1343
    :goto_2
    const-string v0, "logblock"

    const-string v2, "isLogsTabBlock"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1344
    const/high16 v0, 0x24000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1345
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1346
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->w(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1354
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 1356
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1357
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->sendAccessibilityEvent(I)V

    .line 1359
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/android/calendar/event/bb;

    invoke-direct {v2, p0, v1}, Lcom/android/calendar/event/bb;-><init>(Lcom/android/calendar/event/ba;Landroid/content/Intent;)V

    const-wide/16 v4, 0x2bc

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1330
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->performValidation()V

    .line 1331
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    iget-object v2, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v3, v3, Lcom/android/calendar/event/ay;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1334
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1341
    :cond_7
    const-string v0, "additional"

    const-string v2, "email-multi"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_2

    .line 1349
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->V:Landroid/widget/ImageButton;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->w(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1376
    :cond_9
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1377
    :catch_0
    move-exception v0

    .line 1378
    invoke-static {}, Lcom/android/calendar/dz;->o()Ljava/lang/String;

    move-result-object v0

    .line 1379
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1380
    iget-object v1, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1382
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/ba;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "com.android.contacts"

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
