.class public Lcom/android/calendar/event/kx;
.super Landroid/widget/BaseAdapter;
.source "SelectStickerFragment.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectStickerFragment;

.field private b:Ljava/util/ArrayList;

.field private final c:Landroid/content/Context;

.field private d:Landroid/widget/GridView;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/SelectStickerFragment;Landroid/content/Context;Landroid/widget/GridView;)V
    .locals 1

    .prologue
    .line 834
    iput-object p1, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    .line 835
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 829
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    .line 836
    iput-object p2, p0, Lcom/android/calendar/event/kx;->c:Landroid/content/Context;

    .line 837
    iput-object p3, p0, Lcom/android/calendar/event/kx;->d:Landroid/widget/GridView;

    .line 838
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/kx;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 828
    iget-object v0, p0, Lcom/android/calendar/event/kx;->d:Landroid/widget/GridView;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 986
    iput-object p1, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    .line 987
    invoke-virtual {p0}, Lcom/android/calendar/event/kx;->notifyDataSetChanged()V

    .line 988
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 967
    const/4 v0, 0x0

    .line 969
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 975
    const/4 v0, 0x0

    .line 977
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 982
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v7, 0x5f

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 842
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    move-object v2, v4

    .line 961
    :goto_0
    return-object v2

    .line 847
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 848
    if-eqz p2, :cond_1

    instance-of v0, p2, Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 849
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/kx;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 850
    const v1, 0x7f0400a5

    iget-object v2, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v2}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 898
    :cond_2
    :goto_1
    invoke-virtual {v2, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 899
    invoke-virtual {v2, v6}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 900
    new-instance v0, Lcom/android/calendar/event/ky;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/event/ky;-><init>(Lcom/android/calendar/event/kx;I)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 922
    new-instance v0, Lcom/android/calendar/event/kz;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/event/kz;-><init>(Lcom/android/calendar/event/kx;I)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0

    :cond_3
    move-object v2, p2

    .line 853
    goto :goto_1

    .line 856
    :cond_4
    if-eqz p2, :cond_5

    instance-of v0, p2, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    .line 857
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/kx;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 858
    const v1, 0x7f0400a4

    iget-object v2, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v2}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 860
    new-instance v1, Lcom/android/calendar/event/kv;

    invoke-direct {v1, v4}, Lcom/android/calendar/event/kv;-><init>(Lcom/android/calendar/event/kl;)V

    .line 861
    iput-object v0, v1, Lcom/android/calendar/event/kv;->a:Landroid/widget/ImageView;

    .line 862
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v0

    move-object v3, v1

    .line 867
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    iput-object v0, v3, Lcom/android/calendar/event/kv;->b:Ljava/lang/String;

    .line 868
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-wide v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    iput-wide v0, v3, Lcom/android/calendar/event/kv;->c:J

    .line 872
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 873
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-wide v8, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 875
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_9

    move-object v1, v2

    .line 876
    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v0, v5

    .line 881
    :goto_3
    if-eqz v0, :cond_6

    .line 882
    new-instance v0, Lcom/android/calendar/event/lc;

    invoke-direct {v0, v4}, Lcom/android/calendar/event/lc;-><init>(Lcom/android/calendar/event/kl;)V

    new-array v1, v6, [Lcom/android/calendar/event/kv;

    aput-object v3, v1, v5

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/lc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 885
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/kx;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->b:Ljava/lang/String;

    .line 886
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".png"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 889
    iget-object v1, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v3, "string"

    iget-object v4, p0, Lcom/android/calendar/event/kx;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 890
    if-eqz v1, :cond_8

    .line 891
    iget-object v0, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 865
    :cond_7
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kv;

    move-object v2, p2

    move-object v3, v0

    goto/16 :goto_2

    .line 892
    :cond_8
    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 893
    const-string v1, "day"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 894
    iget-object v1, p0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f012d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 895
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    move v0, v6

    goto/16 :goto_3
.end method
