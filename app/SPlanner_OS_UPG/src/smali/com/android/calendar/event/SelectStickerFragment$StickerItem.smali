.class public Lcom/android/calendar/event/SelectStickerFragment$StickerItem;
.super Ljava/lang/Object;
.source "SelectStickerFragment.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:J

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field e:I

.field f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 808
    new-instance v0, Lcom/android/calendar/event/lb;

    invoke-direct {v0}, Lcom/android/calendar/event/lb;-><init>()V

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(JLjava/lang/String;Ljava/lang/String;III)V
    .locals 1

    .prologue
    .line 766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 767
    iput-wide p1, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    .line 768
    iput-object p3, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->b:Ljava/lang/String;

    .line 769
    iput-object p4, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    .line 770
    iput p5, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    .line 771
    iput p6, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    .line 772
    iput p7, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->f:I

    .line 773
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 776
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    .line 777
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->b:Ljava/lang/String;

    .line 778
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    .line 779
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    .line 780
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    .line 781
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/calendar/event/kl;)V
    .locals 0

    .prologue
    .line 758
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/android/calendar/event/SelectStickerFragment$StickerItem;)I
    .locals 2

    .prologue
    .line 795
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    iget v1, p1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    if-ge v0, v1, :cond_0

    .line 796
    const/4 v0, -0x1

    .line 800
    :goto_0
    return v0

    .line 797
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    iget v1, p1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    if-le v0, v1, :cond_1

    .line 798
    const/4 v0, 0x1

    goto :goto_0

    .line 800
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 758
    check-cast p1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a(Lcom/android/calendar/event/SelectStickerFragment$StickerItem;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 805
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 785
    iget-wide v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 786
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 787
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 788
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 789
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 790
    return-void
.end method
