.class Lcom/android/calendar/event/u;
.super Landroid/content/AsyncQueryHandler;
.source "EasyEditEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/m;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/m;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    .line 495
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 496
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 501
    if-nez p3, :cond_0

    .line 543
    :goto_0
    return-void

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-virtual {v0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 508
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 509
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 512
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 540
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 514
    :pswitch_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 517
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 518
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->g(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/s;->a(I)V

    .line 519
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->g(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/s;->run()V

    goto :goto_0

    .line 522
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    new-instance v2, Lcom/android/calendar/as;

    invoke-direct {v2}, Lcom/android/calendar/as;-><init>()V

    invoke-static {v0, v2}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;Lcom/android/calendar/as;)Lcom/android/calendar/as;

    .line 523
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 524
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 525
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 527
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->i(Lcom/android/calendar/event/m;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 529
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->i(Lcom/android/calendar/event/m;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 530
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->j(Lcom/android/calendar/event/m;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->x:J

    .line 531
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->k(Lcom/android/calendar/event/m;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->A:J

    .line 532
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v2

    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->j(Lcom/android/calendar/event/m;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v6, v0, Lcom/android/calendar/as;->y:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, v2, Lcom/android/calendar/as;->w:Z

    .line 533
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->j(Lcom/android/calendar/event/m;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 534
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->k(Lcom/android/calendar/event/m;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 536
    iget-object v0, p0, Lcom/android/calendar/event/u;->a:Lcom/android/calendar/event/m;

    invoke-static {v0, v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;I)V

    goto/16 :goto_0

    .line 532
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 512
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
