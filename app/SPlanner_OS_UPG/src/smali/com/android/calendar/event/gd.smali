.class Lcom/android/calendar/event/gd;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Lcom/android/calendar/common/extension/i;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1317
    iput-object p1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;IIIIIZ)V
    .locals 8

    .prologue
    .line 1321
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1322
    iget-object v1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1323
    iget-object v7, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    const/4 v1, 0x0

    move v2, p7

    move v3, p6

    move v4, p5

    move v5, p4

    move v6, p3

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v0

    invoke-static {v7, v0, v1}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;J)J

    .line 1324
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->d(Lcom/android/calendar/event/fx;I)I

    .line 1325
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V

    .line 1327
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1328
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;I)V

    .line 1331
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1332
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1333
    iget-object v0, p0, Lcom/android/calendar/event/gd;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1335
    :cond_1
    return-void
.end method

.method public a(Landroid/widget/DatePicker;Landroid/widget/TimePicker;Landroid/widget/DatePicker;Landroid/widget/TimePicker;Z)V
    .locals 0

    .prologue
    .line 1340
    return-void
.end method
