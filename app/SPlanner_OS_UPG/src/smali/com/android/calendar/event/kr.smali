.class Lcom/android/calendar/event/kr;
.super Ljava/lang/Object;
.source "SelectStickerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectStickerFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectStickerFragment;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 585
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    invoke-virtual {v0}, Lcom/android/calendar/event/kx;->getCount()I

    move-result v0

    if-gt v0, p3, :cond_1

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    invoke-virtual {v0, p3}, Lcom/android/calendar/event/kx;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 590
    if-eqz v0, :cond_0

    .line 593
    iget-object v1, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->d(Lcom/android/calendar/event/SelectStickerFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 594
    iget-object v1, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v1}, Lcom/android/calendar/event/SelectStickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/la;

    iget-wide v2, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/android/calendar/event/la;->a(JLjava/lang/String;)V

    .line 595
    iget-object v0, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->dismiss()V

    goto :goto_0

    .line 597
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 598
    const-string v2, "_id"

    iget-wide v4, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 599
    const-string v2, "sticker_name"

    iget-object v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    const-string v2, "filepath"

    iget-object v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 601
    const-string v2, "sticker_group"

    iget v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    invoke-static {v3}, Lcom/android/calendar/gx;->b(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 602
    const-string v2, "recently"

    iget v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 603
    iget-object v0, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 604
    iget-object v0, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 605
    iget-object v0, p0, Lcom/android/calendar/event/kr;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f050012

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0
.end method
