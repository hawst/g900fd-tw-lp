.class Lcom/android/calendar/event/dh;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 5351
    iput-object p1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const/16 v8, 0xc

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5354
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5450
    :cond_0
    :goto_0
    return-void

    .line 5358
    :cond_1
    invoke-static {}, Lcom/android/calendar/event/ay;->x()I

    move-result v0

    if-eq v0, p2, :cond_2

    .line 5359
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v7, v6}, Lcom/android/calendar/event/ay;->a(ZI)V

    .line 5361
    :cond_2
    invoke-static {p2}, Lcom/android/calendar/event/ay;->f(I)I

    .line 5363
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 5364
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/event/ay;->x()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 5366
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 5367
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 5371
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5373
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5375
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;

    .line 5377
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v0

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 5380
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v4, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v4

    const/16 v5, 0xb

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;

    .line 5383
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v4, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->u(Lcom/android/calendar/event/ay;)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;Ljava/lang/String;)Ljava/lang/String;

    .line 5385
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->N(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->N(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5387
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3, v7}, Lcom/android/calendar/event/ay;->e(Lcom/android/calendar/event/ay;Z)Z

    .line 5392
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->N(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->N(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v3

    const-string v4, "preferences_samsung_account_validation"

    invoke-static {v3, v4, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_5

    .line 5397
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5399
    const-string v1, "request_module"

    const/16 v2, 0x1f5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5402
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5408
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aD:Lcom/android/calendar/event/fc;

    invoke-virtual {v0}, Lcom/android/calendar/event/fc;->notifyDataSetChanged()V

    .line 5409
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto/16 :goto_0

    .line 5389
    :cond_4
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3, v6}, Lcom/android/calendar/event/ay;->e(Lcom/android/calendar/event/ay;Z)Z

    goto :goto_1

    .line 5404
    :catch_0
    move-exception v0

    .line 5405
    const-string v0, "EditEvent"

    const-string v1, "Error: Could not find AccountValidationCheckActivity.ACTION activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 5413
    :cond_5
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 5414
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->N(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 5417
    :cond_6
    const-string v3, "My calendar"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 5418
    iget-object v1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f02ba

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 5421
    :cond_7
    if-nez v0, :cond_8

    .line 5422
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0073

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 5426
    :cond_8
    iget-object v3, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    invoke-static {v3, v0}, Lcom/android/calendar/event/ay;->i(Lcom/android/calendar/event/ay;I)I

    .line 5428
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->O(Lcom/android/calendar/event/ay;)V

    .line 5430
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5431
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->z:Landroid/view/View;

    iget-object v1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->P(Lcom/android/calendar/event/ay;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 5432
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/ay;->b(Ljava/lang/String;)V

    .line 5435
    :cond_9
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5436
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    const v1, 0x7f1200fe

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;I)Landroid/widget/MultiAutoCompleteTextView;

    .line 5439
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->P(Lcom/android/calendar/event/ay;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->h(Lcom/android/calendar/event/ay;I)I

    .line 5440
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->f()V

    .line 5442
    invoke-static {}, Lcom/android/calendar/event/ay;->x()I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/event/ay;->g(I)I

    .line 5443
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aD:Lcom/android/calendar/event/fc;

    invoke-virtual {v0}, Lcom/android/calendar/event/fc;->notifyDataSetChanged()V

    .line 5445
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 5447
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5448
    iget-object v0, p0, Lcom/android/calendar/event/dh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->Q(Lcom/android/calendar/event/ay;)V

    goto/16 :goto_0
.end method
