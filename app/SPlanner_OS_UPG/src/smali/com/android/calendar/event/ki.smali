.class Lcom/android/calendar/event/ki;
.super Ljava/lang/Object;
.source "SelectStickerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectStickerActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectStickerActivity;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/16 v2, 0x96

    const/4 v3, 0x1

    .line 189
    iget-object v0, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-static {}, Lcom/android/calendar/gx;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectStickerActivity;->a(Lcom/android/calendar/event/SelectStickerActivity;Landroid/net/Uri;)Landroid/net/Uri;

    .line 190
    packed-switch p2, :pswitch_data_0

    .line 224
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 226
    :goto_0
    return-void

    .line 192
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 193
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v1, "crop"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 196
    const-string v1, "aspectX"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 197
    const-string v1, "aspectY"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 198
    const-string v1, "scale"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199
    const-string v1, "outputX"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    const-string v1, "outputY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    const-string v1, "output"

    iget-object v2, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectStickerActivity;->a(Lcom/android/calendar/event/SelectStickerActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 202
    const-string v1, "return-data"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 203
    const-string v1, "scale"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 205
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    const/16 v2, 0x2329

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/event/SelectStickerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    const-string v0, "SelectStickerActivity"

    const-string v1, "Error: Could not find STICKER_CROP_REQEUST activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 212
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerActivity;->b(Lcom/android/calendar/event/SelectStickerActivity;)Lcom/android/calendar/event/SelectStickerFragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerActivity;->b(Lcom/android/calendar/event/SelectStickerActivity;)Lcom/android/calendar/event/SelectStickerFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectStickerActivity;->b(Lcom/android/calendar/event/SelectStickerActivity;)Lcom/android/calendar/event/SelectStickerFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/calendar/event/SelectStickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/ki;->a:Lcom/android/calendar/event/SelectStickerActivity;

    const/16 v2, 0x232a

    invoke-virtual {v1, v0, v2}, Lcom/android/calendar/event/SelectStickerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 218
    :catch_1
    move-exception v0

    .line 219
    const-string v0, "SelectStickerActivity"

    const-string v1, "Error: Could not find MediaStore.ACTION_IMAGE_CAPTURE activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
