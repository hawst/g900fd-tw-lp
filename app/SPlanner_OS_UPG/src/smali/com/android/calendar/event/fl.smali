.class Lcom/android/calendar/event/fl;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field a:I

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 6109
    iput-object p1, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a(I)V
    .locals 0

    .prologue
    .line 6149
    iput p1, p0, Lcom/android/calendar/event/fl;->a:I

    .line 6150
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 6115
    iget v0, p0, Lcom/android/calendar/event/fl;->a:I

    if-nez v0, :cond_0

    .line 6116
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget v1, p0, Lcom/android/calendar/event/fl;->a:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;I)I

    .line 6117
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget v1, p0, Lcom/android/calendar/event/fl;->a:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;I)I

    .line 6118
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->af(Lcom/android/calendar/event/ay;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->h(Lcom/android/calendar/event/ay;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6119
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->ae(Lcom/android/calendar/event/ay;)V

    .line 6120
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;Z)Z

    .line 6145
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 6146
    :goto_1
    return-void

    .line 6122
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/fl;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6124
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->A(Lcom/android/calendar/event/ay;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;I)I

    .line 6125
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->ae(Lcom/android/calendar/event/ay;)V

    .line 6126
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_1

    .line 6130
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget v1, p0, Lcom/android/calendar/event/fl;->a:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;I)I

    .line 6131
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget v1, p0, Lcom/android/calendar/event/fl;->a:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;I)I

    .line 6132
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->aa(Lcom/android/calendar/event/ay;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6133
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->ag(Lcom/android/calendar/event/ay;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;I)I

    .line 6135
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 6136
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->ah(Lcom/android/calendar/event/ay;)I

    move-result v1

    iput v1, v0, Lcom/android/a/c;->d:I

    .line 6142
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->i()V

    goto :goto_0

    .line 6138
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->ai(Lcom/android/calendar/event/ay;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;I)I

    .line 6140
    iget-object v0, p0, Lcom/android/calendar/event/fl;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/a/c;->d:I

    goto :goto_2
.end method
