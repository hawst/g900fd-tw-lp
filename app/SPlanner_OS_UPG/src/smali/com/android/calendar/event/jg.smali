.class Lcom/android/calendar/event/jg;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/j;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 166
    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->c(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/Geocoder;

    move-result-object v1

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 171
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 172
    if-eqz v0, :cond_1

    .line 173
    iget-object v1, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1, p1}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Z

    .line 174
    iget-object v1, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-virtual {v0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)Z

    .line 175
    iget-object v1, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    iget-object v2, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v2, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->d(Lcom/android/calendar/event/SelectMapActivity;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/calendar/event/jg;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/event/SelectMapActivity;->c(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    .line 180
    :cond_0
    monitor-exit v1

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
