.class public Lcom/android/calendar/event/iv;
.super Landroid/app/DialogFragment;
.source "QuickAddFragment.java"


# static fields
.field public static final a:Ljava/lang/String;

.field public static final c:[Ljava/lang/String;


# instance fields
.field b:I

.field private d:Landroid/content/Context;

.field private e:Lcom/android/calendar/al;

.field private f:Landroid/view/View;

.field private g:Lcom/android/calendar/event/QuickAddEditText;

.field private h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Lcom/android/calendar/event/jd;

.field private r:Ljava/lang/String;

.field private s:Landroid/content/AsyncQueryHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const-class v0, Lcom/android/calendar/event/iv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    .line 115
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "account_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/iv;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 102
    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->o:Z

    .line 103
    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    .line 110
    iput v0, p0, Lcom/android/calendar/event/iv;->b:I

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JJZ)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 102
    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->o:Z

    .line 103
    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    .line 110
    iput v0, p0, Lcom/android/calendar/event/iv;->b:I

    .line 132
    iput-object p1, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    .line 133
    iput-wide p2, p0, Lcom/android/calendar/event/iv;->i:J

    .line 134
    iput-wide p4, p0, Lcom/android/calendar/event/iv;->j:J

    .line 135
    iput-boolean p6, p0, Lcom/android/calendar/event/iv;->n:Z

    .line 136
    return-void
.end method

.method private a(Landroid/database/Cursor;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 680
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    .line 709
    :cond_0
    :goto_0
    return v0

    .line 684
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    const-string v3, "preference_defaultCalendar"

    const-string v4, "local"

    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 688
    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 689
    sget-object v2, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    .line 692
    :cond_2
    if-nez v2, :cond_3

    .line 693
    const/4 v0, 0x0

    goto :goto_0

    .line 696
    :cond_3
    const-string v3, "ownerAccount"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 698
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 699
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 700
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 701
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 703
    if-lt v1, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/iv;Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/calendar/event/iv;->a(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/iv;J)J
    .locals 1

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/calendar/event/iv;->i:J

    return-wide p1
.end method

.method private a(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 241
    const-string v0, ""

    .line 242
    const-string v0, ""

    .line 243
    const-string v0, ""

    .line 244
    const-string v0, ""

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    const/4 v2, 0x3

    .line 248
    const/16 v0, 0xb01

    .line 251
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 252
    const/16 v0, 0xb81

    .line 255
    :cond_0
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 256
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 257
    iget-wide v6, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-virtual {v3, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 258
    iget-wide v6, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 260
    iget-wide v4, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-static {v4, v5, p1, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 262
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 263
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 264
    iget-boolean v2, p0, Lcom/android/calendar/event/iv;->n:Z

    if-nez v2, :cond_1

    .line 265
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    iget-wide v4, p0, Lcom/android/calendar/event/iv;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 266
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-static {p1, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 268
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 269
    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-static {p1, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 271
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 280
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 275
    :cond_2
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 276
    const-string v0, " - "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    iget-wide v4, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-static {v4, v5, p1, v2}, Lcom/android/calendar/hj;->a(JLandroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 278
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/iv;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/iv;Z)Z
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/calendar/event/iv;->o:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/iv;J)J
    .locals 1

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/calendar/event/iv;->j:J

    return-wide p1
.end method

.method private b(Landroid/content/Context;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 284
    const-string v0, ""

    .line 288
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/iv;->e:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->g()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 289
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    const/16 v0, 0x5f

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 292
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.samsung.android.service.hermes.collect/support_lang"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v4, "language"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 298
    if-eqz v1, :cond_1

    .line 300
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 301
    const-string v0, "language"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    .line 308
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v0

    .line 312
    :cond_1
    if-eqz v7, :cond_2

    .line 313
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_1
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 318
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 319
    aget-object v0, v0, v1

    .line 323
    :goto_2
    return-object v0

    .line 308
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 315
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 321
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0482

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v7

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/QuickAddEditText;->getWritingBuddy(Z)Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    .line 209
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    const-string v1, "WATCH_ACTION"

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setPrivateCommnad(Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    new-instance v1, Lcom/android/calendar/event/ix;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ix;-><init>(Lcom/android/calendar/event/iv;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnPrivateCommandListner(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnPrivateCommandListener;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/event/iv;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->o:Z

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/event/iv;Z)Z
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/calendar/event/iv;->n:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/event/iv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    return-object v0
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 327
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 328
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 329
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 330
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 332
    iget v2, v0, Landroid/text/format/Time;->monthDay:I

    iget v3, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/text/format/Time;->month:I

    iget v3, v1, Landroid/text/format/Time;->month:I

    if-ne v2, v3, :cond_0

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-eq v0, v1, :cond_1

    .line 335
    :cond_0
    const/4 v0, 0x0

    .line 337
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private d()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 341
    new-instance v0, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 342
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 343
    new-instance v1, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 344
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 346
    invoke-static {v0, v1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v0

    if-lez v0, :cond_0

    .line 347
    const/4 v0, 0x1

    .line 349
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/event/iv;)Z
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->c()Z

    move-result v0

    return v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    const-string v1, "action_cancel_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    const-string v0, "action_done_event"

    iput-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    .line 355
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-wide v0, p0, Lcom/android/calendar/event/iv;->i:J

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/calendar/event/iv;->j:J

    .line 371
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->g()V

    .line 372
    iget-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    const-string v1, "action_done_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 373
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->h()V

    .line 377
    :cond_1
    :goto_0
    return-void

    .line 359
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->n:Z

    .line 361
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/event/iy;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/iy;-><init>(Lcom/android/calendar/event/iv;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 374
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    const-string v1, "action_create_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->i()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/event/iv;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->n:Z

    return v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 381
    :try_start_0
    new-instance v0, Lcom/samsung/android/hermes/HermesServiceManager;

    iget-object v1, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/hermes/HermesServiceManager;-><init>(Landroid/content/Context;)V

    .line 382
    new-instance v1, Lcom/android/calendar/event/iz;

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/event/iz;-><init>(Lcom/android/calendar/event/iv;Lcom/samsung/android/hermes/HermesServiceManager;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/hermes/HermesServiceManager;->setHermesCallBack(Lcom/samsung/android/hermes/HermesServiceManager$IHermesCallBack;)V

    .line 437
    iget-object v1, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 438
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v1, v2

    .line 439
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/samsung/android/hermes/HermesServiceManager;->analysis(ILjava/lang/Object;II)V

    .line 440
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->o:Z

    .line 447
    :goto_0
    return-void

    .line 442
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->e()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    .line 445
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/event/iv;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->e()V

    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 463
    const-string v0, "@"

    .line 465
    iget-object v1, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 466
    iput-object v4, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    .line 467
    iput-object v4, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 470
    new-instance v1, Ljava/util/StringTokenizer;

    iget-object v2, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 471
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 473
    iget-object v2, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 474
    iget-object v3, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 475
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    .line 477
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 478
    iput-object v4, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    goto :goto_0

    .line 480
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    .line 481
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    iput-object v4, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    goto :goto_0

    .line 486
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    .line 487
    iput-object v4, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/event/iv;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->f()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/event/iv;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    return-object v0
.end method

.method private h()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 493
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 495
    const-string v0, "title"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    const-string v0, "eventLocation"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    .line 499
    iget-wide v4, p0, Lcom/android/calendar/event/iv;->j:J

    .line 500
    new-instance v7, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 501
    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 502
    new-instance v2, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 503
    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 506
    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->n:Z

    if-eqz v0, :cond_1

    .line 507
    const-string v0, "UTC"

    .line 508
    iput v8, v7, Landroid/text/format/Time;->hour:I

    .line 509
    iput v8, v7, Landroid/text/format/Time;->minute:I

    .line 510
    iput v8, v7, Landroid/text/format/Time;->second:I

    .line 511
    iput-object v0, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 512
    invoke-virtual {v7, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 514
    iput v8, v2, Landroid/text/format/Time;->hour:I

    .line 515
    iput v8, v2, Landroid/text/format/Time;->minute:I

    .line 516
    iput v8, v2, Landroid/text/format/Time;->second:I

    .line 517
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 518
    invoke-virtual {v2, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    const-wide/32 v8, 0x5265c00

    add-long/2addr v2, v8

    .line 520
    const-string v7, "allDay"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 531
    :goto_0
    const-string v7, "dtstart"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 532
    const-string v4, "dtend"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 533
    const-string v2, "hasAttendeeData"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 534
    const-string v2, "eventTimezone"

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v0, "calendar_id"

    iget v2, p0, Lcom/android/calendar/event/iv;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 539
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 541
    sget-object v0, Lcom/android/calendar/hj;->D:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    :goto_1
    if-eqz v1, :cond_0

    .line 547
    iget-object v0, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/android/calendar/event/ja;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ja;-><init>(Lcom/android/calendar/event/iv;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 553
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->finish()V

    .line 555
    :cond_0
    return-void

    .line 522
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 523
    iput-object v0, v7, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 524
    invoke-virtual {v7, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 525
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 526
    invoke-virtual {v2, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 528
    const-string v7, "allDay"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 542
    :catch_0
    move-exception v0

    .line 543
    sget-object v2, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic i(Lcom/android/calendar/event/iv;)J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/calendar/event/iv;->i:J

    return-wide v0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 558
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EDIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 559
    iget-object v1, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    const-class v2, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 560
    const-string v1, "event_create"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 561
    const-string v1, "edit_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 562
    const-string v1, "launch_from_inside"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 563
    const-string v1, "beginTime"

    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 564
    const-string v1, "endTime"

    iget-wide v2, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 565
    const-string v1, "allDay"

    iget-boolean v2, p0, Lcom/android/calendar/event/iv;->n:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 566
    const-string v1, "title"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 567
    const-string v1, "eventLocation"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 569
    invoke-virtual {p0, v0}, Lcom/android/calendar/event/iv;->startActivity(Landroid/content/Intent;)V

    .line 570
    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->dismissAllowingStateLoss()V

    .line 571
    return-void
.end method

.method static synthetic j(Lcom/android/calendar/event/iv;)J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/calendar/event/iv;->j:J

    return-wide v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 623
    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 624
    if-nez v0, :cond_0

    .line 636
    :goto_0
    return-void

    .line 627
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 629
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 630
    invoke-static {v0}, Lcom/android/calendar/g/h;->d(Landroid/view/Window;)V

    .line 631
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 632
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 633
    const/16 v2, 0x11

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 634
    iget-object v2, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c039e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 635
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/event/iv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/event/iv;)Lcom/samsung/android/writingbuddy/WritingBuddyImpl;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 450
    const-string v0, "action_cancel_event"

    iput-object v0, p0, Lcom/android/calendar/event/iv;->r:Ljava/lang/String;

    .line 451
    iget-object v0, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 452
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->n:Z

    if-nez v0, :cond_0

    .line 453
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->e()V

    .line 460
    :goto_0
    return-void

    .line 455
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->f()V

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->finish()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 145
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 146
    iput-object p1, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    .line 147
    iget-object v1, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/iv;->e:Lcom/android/calendar/al;

    .line 148
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/calendar/event/iv;->setStyle(II)V

    .line 151
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/android/calendar/event/jd;

    move-object v1, v0

    iput-object v1, p0, Lcom/android/calendar/event/iv;->q:Lcom/android/calendar/event/jd;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    invoke-static {p1}, Lcom/android/calendar/hj;->d(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/android/calendar/event/iv;->dismissAllowingStateLoss()V

    .line 166
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v1

    .line 153
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement OnDestroyListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 162
    :cond_0
    new-instance v1, Lcom/android/calendar/event/jc;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/calendar/event/jc;-><init>(Lcom/android/calendar/event/iv;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/android/calendar/event/iv;->s:Landroid/content/AsyncQueryHandler;

    .line 163
    iget-object v1, p0, Lcom/android/calendar/event/iv;->s:Landroid/content/AsyncQueryHandler;

    const/4 v2, 0x1

    sget-object v4, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v5, Lcom/android/calendar/event/iv;->c:[Ljava/lang/String;

    const-string v6, "calendar_access_level>=500 AND deleted!=1"

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v1 .. v8}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    if-eqz p3, :cond_2

    .line 171
    const-string v0, "start_date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "start_date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/iv;->i:J

    .line 174
    :cond_0
    const-string v0, "end_date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    const-string v0, "end_date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/iv;->j:J

    .line 177
    :cond_1
    const-string v0, "allday_event"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    const-string v0, "allday_event"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->n:Z

    .line 182
    :cond_2
    const v0, 0x7f040079

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/iv;->f:Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcom/android/calendar/event/iv;->f:Landroid/view/View;

    const v1, 0x7f12025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/QuickAddEditText;

    iput-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    .line 185
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/QuickAddEditText;->setWritingBuddyEnabled(Z)V

    .line 186
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/QuickAddEditText;->setCursorVisible(Z)V

    .line 187
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    const-string v1, "inputType=SPlanner"

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/QuickAddEditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/QuickAddEditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 189
    const-string v1, "titleText"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/android/calendar/event/iv;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "guideText"

    iget-object v2, p0, Lcom/android/calendar/event/iv;->d:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/android/calendar/event/iv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    new-instance v1, Lcom/android/calendar/event/iw;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/iw;-><init>(Lcom/android/calendar/event/iv;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/QuickAddEditText;->setOnBackKeyListener(Lcom/android/calendar/event/iu;)V

    .line 199
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->b()V

    .line 200
    invoke-direct {p0}, Lcom/android/calendar/event/iv;->j()V

    .line 201
    iget-object v0, p0, Lcom/android/calendar/event/iv;->f:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->finish()V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/QuickAddEditText;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/android/calendar/event/iv;->q:Lcom/android/calendar/event/jd;

    invoke-interface {v0}, Lcom/android/calendar/event/jd;->j()V

    .line 619
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 620
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 587
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    .line 588
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 589
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 595
    iget-object v0, p0, Lcom/android/calendar/event/iv;->h:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 596
    iget-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    if-eqz v0, :cond_1

    .line 597
    iget-object v0, p0, Lcom/android/calendar/event/iv;->g:Lcom/android/calendar/event/QuickAddEditText;

    iget-object v1, p0, Lcom/android/calendar/event/iv;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/QuickAddEditText;->setText(Ljava/lang/CharSequence;)V

    .line 598
    const-wide/16 v0, 0x2ee

    .line 602
    :goto_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/android/calendar/event/jb;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/jb;-><init>(Lcom/android/calendar/event/iv;)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 608
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/iv;->p:Z

    .line 609
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 610
    return-void

    .line 600
    :cond_1
    const-wide/16 v0, 0x15e

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 575
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 576
    iget-wide v0, p0, Lcom/android/calendar/event/iv;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 577
    const-string v0, "start_date"

    iget-wide v2, p0, Lcom/android/calendar/event/iv;->i:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 579
    :cond_0
    iget-wide v0, p0, Lcom/android/calendar/event/iv;->j:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 580
    const-string v0, "end_date"

    iget-wide v2, p0, Lcom/android/calendar/event/iv;->j:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 582
    :cond_1
    const-string v0, "allday_event"

    iget-boolean v1, p0, Lcom/android/calendar/event/iv;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 583
    return-void
.end method
