.class Lcom/android/calendar/event/ed;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 6373
    iput-object p1, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6376
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->M(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6400
    :cond_0
    :goto_0
    return-void

    .line 6379
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, p2}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;I)I

    .line 6381
    if-eqz p2, :cond_3

    .line 6382
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1, p2}, Lcom/android/calendar/event/ay;->n(Lcom/android/calendar/event/ay;I)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 6383
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->Z(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 6393
    :cond_2
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 6395
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->h(Lcom/android/calendar/event/ay;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->aj(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_leap_event"

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6398
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3, v4}, Lcom/android/calendar/event/ay;->a(Landroid/app/Activity;ZZZ)V

    goto :goto_0

    .line 6384
    :cond_3
    if-nez p2, :cond_2

    .line 6385
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, p2}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;I)I

    .line 6386
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, v4}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;Z)Z

    .line 6387
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v0, Lcom/android/calendar/event/ay;->C:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->af(Lcom/android/calendar/event/ay;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->h(Lcom/android/calendar/event/ay;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 6389
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 6390
    iget-object v0, p0, Lcom/android/calendar/event/ed;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    goto :goto_1
.end method
