.class Lcom/android/calendar/event/aj;
.super Landroid/content/AsyncQueryHandler;
.source "EditEventActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/EditEventActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/EditEventActivity;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 1431
    iput-object p1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    .line 1432
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 1433
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1437
    if-nez p3, :cond_1

    .line 1465
    :cond_0
    :goto_0
    return-void

    .line 1440
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v0, p1}, Lcom/android/calendar/event/EditEventActivity;->a(Lcom/android/calendar/event/EditEventActivity;I)V

    .line 1441
    const-string v0, ""

    .line 1442
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1443
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1445
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v1, p1, v0}, Lcom/android/calendar/event/EditEventActivity;->a(Lcom/android/calendar/event/EditEventActivity;ILjava/lang/String;)V

    .line 1446
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v0}, Lcom/android/calendar/event/EditEventActivity;->c(Lcom/android/calendar/event/EditEventActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1447
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1449
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    iget-object v1, v1, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Lcom/android/calendar/event/EditEventActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1450
    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v1}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v1}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v1

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_3

    .line 1451
    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v1}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v1

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ay;->a(Ljava/util/ArrayList;)V

    .line 1460
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->a(Lcom/android/calendar/event/EditEventActivity;Z)Z

    .line 1462
    :cond_4
    if-eqz p3, :cond_0

    .line 1463
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1456
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v0}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v0}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_3

    .line 1457
    iget-object v0, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-static {v0}, Lcom/android/calendar/event/EditEventActivity;->b(Lcom/android/calendar/event/EditEventActivity;)Lcom/android/calendar/event/ak;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/aj;->a:Lcom/android/calendar/event/EditEventActivity;

    iget-object v1, v1, Lcom/android/calendar/event/EditEventActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(Ljava/util/ArrayList;)V

    goto :goto_1
.end method
