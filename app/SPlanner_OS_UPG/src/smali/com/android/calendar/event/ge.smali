.class Lcom/android/calendar/event/ge;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1363
    iput-object p1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x1

    .line 1367
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1368
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1369
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1374
    :cond_0
    :goto_0
    iput p2, v0, Landroid/text/format/Time;->year:I

    .line 1375
    iput p3, v0, Landroid/text/format/Time;->month:I

    .line 1376
    iput p4, v0, Landroid/text/format/Time;->monthDay:I

    .line 1378
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1379
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v3, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Ljava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/android/calendar/event/fx;->d(Lcom/android/calendar/event/fx;J)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;J)J

    .line 1380
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 1381
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V

    .line 1382
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;I)V

    .line 1393
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V

    .line 1396
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;JLjava/lang/Long;)V

    .line 1397
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/widget/TextView;J)V

    .line 1398
    return-void

    .line 1370
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1371
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_0

    .line 1384
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1385
    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;J)J

    .line 1386
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0, v6}, Lcom/android/calendar/event/fx;->d(Lcom/android/calendar/event/fx;I)I

    .line 1387
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1388
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1389
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V

    .line 1390
    iget-object v0, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/ge;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;I)V

    goto/16 :goto_1
.end method
