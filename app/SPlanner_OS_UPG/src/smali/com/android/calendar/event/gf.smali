.class Lcom/android/calendar/event/gf;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field final synthetic a:Landroid/text/format/Time;

.field final synthetic b:Ljava/lang/Long;

.field final synthetic c:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 1419
    iput-object p1, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iput-object p2, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iput-object p3, p0, Lcom/android/calendar/event/gf;->b:Ljava/lang/Long;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1424
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->p(Lcom/android/calendar/event/fx;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1425
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->d(Lcom/android/calendar/event/fx;I)I

    .line 1428
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1465
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->b:Ljava/lang/Long;

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    .line 1468
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/widget/TextView;J)V

    .line 1469
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;I)V

    .line 1470
    return-void

    .line 1430
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->q(Lcom/android/calendar/event/fx;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1431
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0, v2}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;Z)Z

    .line 1432
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->r(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1434
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->r(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto :goto_0

    .line 1436
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->b:Ljava/lang/Long;

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto :goto_0

    .line 1438
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->s(Lcom/android/calendar/event/fx;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1439
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0, v2}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;Z)Z

    .line 1440
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1441
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto :goto_0

    .line 1443
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->b:Ljava/lang/Long;

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto :goto_0

    .line 1449
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->r(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto/16 :goto_0

    .line 1453
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->p(Lcom/android/calendar/event/fx;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1454
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    .line 1455
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V

    goto/16 :goto_0

    .line 1457
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto/16 :goto_0

    .line 1462
    :pswitch_3
    iget-object v0, p0, Lcom/android/calendar/event/gf;->c:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gf;->a:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/gf;->b:Ljava/lang/Long;

    invoke-static {v0, v1, v2, p2, p3}, Lcom/android/calendar/event/fx;->a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V

    goto/16 :goto_0

    .line 1428
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
