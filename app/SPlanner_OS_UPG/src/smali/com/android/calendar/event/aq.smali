.class Lcom/android/calendar/event/aq;
.super Ljava/lang/Object;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/android/calendar/event/ak;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ak;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 908
    iput-object p1, p0, Lcom/android/calendar/event/aq;->c:Lcom/android/calendar/event/ak;

    iput-object p2, p0, Lcom/android/calendar/event/aq;->a:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/android/calendar/event/aq;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 911
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 914
    iget-object v1, p0, Lcom/android/calendar/event/aq;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 915
    iget-object v1, p0, Lcom/android/calendar/event/aq;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 916
    if-lez v4, :cond_1

    move v1, v0

    move v2, v0

    .line 917
    :goto_0
    if-ge v1, v4, :cond_1

    .line 918
    iget-object v0, p0, Lcom/android/calendar/event/aq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 919
    add-int/lit8 v2, v2, 0x1

    .line 921
    if-ge v2, v4, :cond_0

    .line 922
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v5, 0x3b

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 917
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 924
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 930
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 932
    iget-object v1, p0, Lcom/android/calendar/event/aq;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 937
    iget-object v1, p0, Lcom/android/calendar/event/aq;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x5f0

    if-ge v1, v2, :cond_2

    .line 938
    const-string v1, "smsto"

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 939
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 940
    const-string v2, "com.android.mms"

    const-string v3, "com.android.mms.ui.NoConfirmationSendService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 942
    const-string v2, "android.intent.action.RESPOND_VIA_MESSAGE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 943
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 944
    const-string v0, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/android/calendar/event/aq;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 946
    iget-object v0, p0, Lcom/android/calendar/event/aq;->c:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 949
    :cond_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 950
    iget-object v0, p0, Lcom/android/calendar/event/aq;->c:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->d()V

    .line 951
    return-void
.end method
