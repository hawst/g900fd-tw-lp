.class public Lcom/android/calendar/event/fx;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static P:I

.field private static Q:I

.field private static final Z:[I

.field private static as:Landroid/content/Context;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/widget/Button;

.field private C:Landroid/widget/ImageButton;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/widget/TextView;

.field private I:[Ljava/lang/String;

.field private J:[Ljava/lang/Integer;

.field private K:Ljava/lang/String;

.field private L:I

.field private M:Landroid/database/Cursor;

.field private N:Landroid/widget/Spinner;

.field private O:I

.field private R:Landroid/widget/Spinner;

.field private S:Landroid/widget/TextView;

.field private T:Landroid/view/View;

.field private U:Landroid/widget/CheckBox;

.field private V:Landroid/widget/TextView;

.field private W:[Ljava/lang/CharSequence;

.field private X:[Ljava/lang/CharSequence;

.field private Y:[I

.field a:Landroid/widget/ScrollView;

.field private aA:Landroid/widget/LinearLayout;

.field private aB:Landroid/animation/LayoutTransition;

.field private aa:I

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Landroid/widget/TextView;

.field private ae:Landroid/widget/ImageView;

.field private af:I

.field private ag:I

.field private ah:Z

.field private ai:Landroid/app/AlertDialog;

.field private final aj:Landroid/app/Activity;

.field private final ak:Landroid/view/View;

.field private al:Lcom/android/calendar/hh;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/Long;

.field private ao:Ljava/lang/Long;

.field private ap:J

.field private aq:Ljava/lang/String;

.field private ar:J

.field private at:Landroid/database/Cursor;

.field private au:Landroid/widget/ListView;

.field private av:I

.field private aw:Landroid/view/View;

.field private ax:Z

.field private final ay:Lcom/android/calendar/d/g;

.field private az:Landroid/widget/Button;

.field public b:I

.field c:Lcom/android/calendar/event/hj;

.field d:Lcom/android/calendar/event/hk;

.field public e:Landroid/widget/Spinner;

.field f:Ljava/util/ArrayList;

.field public g:[Ljava/lang/String;

.field public h:I

.field public i:Lcom/android/calendar/common/extension/c;

.field public j:Lcom/android/calendar/common/extension/a;

.field public k:Lcom/android/calendar/common/extension/k;

.field public l:Z

.field public m:Z

.field public n:Landroid/view/View;

.field public o:Z

.field private final p:Ljava/lang/String;

.field private q:I

.field private r:[Ljava/lang/CharSequence;

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Landroid/widget/EditText;

.field private w:Landroid/widget/RelativeLayout;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/EditText;

.field private z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 170
    sput v0, Lcom/android/calendar/event/fx;->P:I

    .line 171
    sput v0, Lcom/android/calendar/event/fx;->Q:I

    .line 187
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/event/fx;->Z:[I

    .line 229
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    return-void

    .line 187
    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/event/fw;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v2, 0x140

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const-string v0, "extra_prefs_show_button_bar"

    iput-object v0, p0, Lcom/android/calendar/event/fx;->p:Ljava/lang/String;

    .line 131
    iput v5, p0, Lcom/android/calendar/event/fx;->q:I

    .line 133
    iput-object v3, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    .line 134
    iput v4, p0, Lcom/android/calendar/event/fx;->s:I

    .line 136
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->t:Z

    .line 137
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->u:Z

    .line 141
    iput-object v3, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    .line 142
    iput-object v3, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    .line 143
    iput-object v3, p0, Lcom/android/calendar/event/fx;->x:Landroid/widget/TextView;

    .line 144
    iput-object v3, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    .line 146
    iput-object v3, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    .line 147
    iput-object v3, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    .line 148
    iput-object v3, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    .line 149
    iput-object v3, p0, Lcom/android/calendar/event/fx;->C:Landroid/widget/ImageButton;

    .line 150
    iput-object v3, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    .line 152
    iput-object v3, p0, Lcom/android/calendar/event/fx;->E:Landroid/view/View;

    .line 153
    iput-object v3, p0, Lcom/android/calendar/event/fx;->F:Landroid/view/View;

    .line 155
    iput-object v3, p0, Lcom/android/calendar/event/fx;->G:Landroid/view/View;

    .line 156
    iput-object v3, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    .line 158
    iput v4, p0, Lcom/android/calendar/event/fx;->b:I

    .line 159
    iput-object v3, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    .line 160
    iput-object v3, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    .line 161
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    .line 162
    iput v4, p0, Lcom/android/calendar/event/fx;->L:I

    .line 165
    iput-object v3, p0, Lcom/android/calendar/event/fx;->c:Lcom/android/calendar/event/hj;

    .line 166
    iput-object v3, p0, Lcom/android/calendar/event/fx;->d:Lcom/android/calendar/event/hk;

    .line 168
    iput-object v3, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    .line 169
    iput v4, p0, Lcom/android/calendar/event/fx;->O:I

    .line 173
    iput-object v3, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    .line 174
    iput-object v3, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    .line 176
    iput-object v3, p0, Lcom/android/calendar/event/fx;->S:Landroid/widget/TextView;

    .line 179
    iput-object v3, p0, Lcom/android/calendar/event/fx;->T:Landroid/view/View;

    .line 180
    iput-object v3, p0, Lcom/android/calendar/event/fx;->U:Landroid/widget/CheckBox;

    .line 182
    iput-object v3, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    .line 184
    iput-object v3, p0, Lcom/android/calendar/event/fx;->W:[Ljava/lang/CharSequence;

    .line 185
    iput-object v3, p0, Lcom/android/calendar/event/fx;->X:[Ljava/lang/CharSequence;

    .line 186
    iput-object v3, p0, Lcom/android/calendar/event/fx;->Y:[I

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    .line 192
    iput-object v3, p0, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    .line 194
    iput v4, p0, Lcom/android/calendar/event/fx;->h:I

    .line 195
    iput v4, p0, Lcom/android/calendar/event/fx;->aa:I

    .line 196
    const-string v0, "My Task"

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 197
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    .line 199
    iput-object v3, p0, Lcom/android/calendar/event/fx;->ad:Landroid/widget/TextView;

    .line 200
    iput-object v3, p0, Lcom/android/calendar/event/fx;->ae:Landroid/widget/ImageView;

    .line 202
    iput v4, p0, Lcom/android/calendar/event/fx;->af:I

    .line 204
    iput v4, p0, Lcom/android/calendar/event/fx;->ag:I

    .line 205
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->ah:Z

    .line 207
    iput-object v3, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    .line 208
    iput-object v3, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    .line 209
    iput-object v3, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    .line 211
    iput-object v3, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 220
    iput-object v3, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    .line 221
    iput-object v3, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    .line 222
    iput-object v3, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    .line 224
    iput-wide v6, p0, Lcom/android/calendar/event/fx;->ap:J

    .line 225
    iput-object v3, p0, Lcom/android/calendar/event/fx;->aq:Ljava/lang/String;

    .line 227
    iput-wide v6, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 231
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->m:Z

    .line 236
    iput v4, p0, Lcom/android/calendar/event/fx;->av:I

    .line 237
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->o:Z

    .line 239
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->ax:Z

    .line 240
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ay:Lcom/android/calendar/d/g;

    .line 243
    iput-object v3, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    .line 244
    iput-object v3, p0, Lcom/android/calendar/event/fx;->aA:Landroid/widget/LinearLayout;

    .line 305
    iput-object p1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    .line 306
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    sput-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    .line 307
    iput-object p2, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    .line 309
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    .line 310
    const v0, 0x7f0a0002

    invoke-static {p1, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/fx;->ax:Z

    .line 311
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const v1, 0x7f0a000b

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    .line 317
    const v0, 0x7f1200e3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    .line 319
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    iput v0, p0, Lcom/android/calendar/event/fx;->av:I

    .line 320
    const v1, 0x8c001

    .line 325
    const v0, 0x7f120153

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    .line 326
    iget v0, p0, Lcom/android/calendar/event/fx;->av:I

    if-gt v0, v2, :cond_0

    .line 327
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 330
    :cond_0
    const v0, 0x7f120154

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    .line 331
    const v0, 0x7f120155

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->x:Landroid/widget/TextView;

    .line 332
    const v0, 0x7f120156

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->T:Landroid/view/View;

    .line 333
    const v0, 0x7f120157

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->U:Landroid/widget/CheckBox;

    .line 336
    const v0, 0x7f120150

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->F:Landroid/view/View;

    .line 337
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_3

    .line 338
    const v0, 0x7f120168

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    .line 345
    :goto_0
    const v0, 0x7f120162

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->G:Landroid/view/View;

    .line 346
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_4

    .line 347
    const v0, 0x7f12016b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    .line 353
    :goto_1
    const v0, 0x7f12015f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->E:Landroid/view/View;

    .line 354
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_5

    .line 355
    const v0, 0x7f12016a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    .line 361
    :goto_2
    const v0, 0x7f120159

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    .line 362
    const v0, 0x7f12015c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    .line 363
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/fy;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/fy;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    const v0, 0x7f12015b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    .line 373
    const v0, 0x7f12015d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->C:Landroid/widget/ImageButton;

    .line 374
    iget-object v0, p0, Lcom/android/calendar/event/fx;->C:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/event/gj;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gj;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    const v0, 0x7f120158

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    .line 384
    const v0, 0x7f12015e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    .line 385
    iget v0, p0, Lcom/android/calendar/event/fx;->av:I

    if-gt v0, v2, :cond_1

    .line 386
    const v0, 0xac001

    .line 387
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 391
    :cond_1
    const v0, 0x7f1200f9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    .line 392
    const v0, 0x7f1200fa

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->aA:Landroid/widget/LinearLayout;

    .line 394
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->C()V

    .line 396
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->t()V

    .line 399
    new-instance v0, Lcom/android/calendar/en;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 400
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 404
    new-instance v0, Lcom/android/calendar/em;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/em;-><init>(Landroid/content/Context;)V

    .line 406
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    new-array v2, v5, [Landroid/text/InputFilter;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 410
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;)V

    .line 412
    iget-object v0, p0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 413
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 417
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "EditEventFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ak;

    .line 419
    if-eqz v0, :cond_2

    .line 420
    iget-object v1, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_2
    return-void

    .line 340
    :cond_3
    const v0, 0x7f120152

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ad:Landroid/widget/TextView;

    .line 341
    const v0, 0x7f120151

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ae:Landroid/widget/ImageView;

    goto/16 :goto_0

    .line 349
    :cond_4
    const v0, 0x7f120164

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    goto/16 :goto_1

    .line 357
    :cond_5
    const v0, 0x7f120161

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    goto/16 :goto_2
.end method

.method static synthetic A(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    return-object v0
.end method

.method private A()V
    .locals 4

    .prologue
    .line 1954
    iget-object v0, p0, Lcom/android/calendar/event/fx;->n:Landroid/view/View;

    const v1, 0x7f1200df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->au:Landroid/widget/ListView;

    .line 1955
    iget-object v0, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1956
    invoke-virtual {p0}, Lcom/android/calendar/event/fx;->c()V

    .line 1959
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1961
    const/4 v1, 0x0

    sget-object v2, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1963
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const v3, 0x7f04009a

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1965
    iget-object v0, p0, Lcom/android/calendar/event/fx;->au:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1966
    iget-object v0, p0, Lcom/android/calendar/event/fx;->au:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 1967
    return-void
.end method

.method static synthetic B(Lcom/android/calendar/event/fx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    return-object v0
.end method

.method private B()V
    .locals 4

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 2049
    :cond_0
    :goto_0
    return-void

    .line 1973
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1974
    const v1, 0x7f0f0428

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1975
    iget-object v1, p0, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1976
    iget-object v1, p0, Lcom/android/calendar/event/fx;->c:Lcom/android/calendar/event/hj;

    sget v2, Lcom/android/calendar/event/fx;->P:I

    new-instance v3, Lcom/android/calendar/event/gw;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/gw;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2037
    :cond_2
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/gx;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gx;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2046
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2047
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method static synthetic C(Lcom/android/calendar/event/fx;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ae:Landroid/widget/ImageView;

    return-object v0
.end method

.method private C()V
    .locals 3

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    .line 2053
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->W:[Ljava/lang/CharSequence;

    .line 2054
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->X:[Ljava/lang/CharSequence;

    .line 2056
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/calendar/event/fx;->Y:[I

    .line 2057
    iget-object v0, p0, Lcom/android/calendar/event/fx;->Y:[I

    const/4 v1, 0x0

    const v2, 0x7f02013c

    aput v2, v0, v1

    .line 2058
    iget-object v0, p0, Lcom/android/calendar/event/fx;->Y:[I

    const/4 v1, 0x2

    const v2, 0x7f02013b

    aput v2, v0, v1

    .line 2060
    return-void
.end method

.method static synthetic D(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ad:Landroid/widget/TextView;

    return-object v0
.end method

.method private D()V
    .locals 5

    .prologue
    .line 2239
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_2

    .line 2240
    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    if-nez v0, :cond_1

    .line 2241
    iget-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2274
    :cond_0
    :goto_0
    return-void

    .line 2243
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v2, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 2246
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 2247
    iget-object v1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    const-string v2, "My Task"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2248
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2250
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2251
    const-string v2, "com.osp.app.signin"

    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v3, v4}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2253
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v2, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v1, v0, Lcom/android/calendar/task/ab;->g:Ljava/lang/String;

    .line 2255
    :cond_3
    const-string v2, "com.android.exchange"

    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v4, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v3, v4}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2257
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v2, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/event/fx;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v1, v0, Lcom/android/calendar/task/ab;->g:Ljava/lang/String;

    .line 2261
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ad:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2262
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ae:Landroid/widget/ImageView;

    sget-object v2, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    iget v3, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v2, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 2265
    const/4 v0, 0x0

    .line 2266
    iget-object v2, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    .line 2267
    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2268
    sput v2, Lcom/android/calendar/event/fx;->P:I

    goto/16 :goto_0

    .line 2271
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 2272
    goto :goto_2

    :cond_6
    move-object v1, v0

    goto/16 :goto_1
.end method

.method static synthetic E(Lcom/android/calendar/event/fx;)I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/android/calendar/event/fx;->aa:I

    return v0
.end method

.method private E()V
    .locals 4

    .prologue
    const v3, 0x7f12015e

    const/4 v2, 0x0

    .line 2379
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_0

    .line 2380
    iget-object v0, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setNextFocusDownId(I)V

    .line 2381
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    const v1, 0x7f12016a

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    .line 2384
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ay:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2385
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_2

    .line 2386
    iget-object v0, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/fx;->L:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2395
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    if-nez v0, :cond_4

    .line 2396
    iget-object v0, p0, Lcom/android/calendar/event/fx;->G:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2398
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_1

    .line 2399
    iget-object v0, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setNextFocusDownId(I)V

    .line 2400
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    const v1, 0x7f12016b

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    .line 2406
    :cond_1
    :goto_1
    return-void

    .line 2388
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2389
    iget-object v0, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    iget v1, p0, Lcom/android/calendar/event/fx;->L:I

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    .line 2391
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2392
    iget-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0

    .line 2403
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->G:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic F(Lcom/android/calendar/event/fx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    return-object v0
.end method

.method private F()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2410
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2412
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 2413
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    .line 2424
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    .line 2426
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v0, :cond_3

    .line 2427
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2428
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2434
    :goto_1
    return-void

    .line 2416
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/fx;->s:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2421
    :goto_2
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    goto :goto_0

    .line 2417
    :catch_0
    move-exception v0

    .line 2418
    iput v4, p0, Lcom/android/calendar/event/fx;->s:I

    .line 2419
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/fx;->s:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2431
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2432
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method private G()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2437
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iget v1, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/fx;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 2439
    if-eqz v0, :cond_0

    const-string v1, "com.android.sharepoint"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2440
    iget-object v0, p0, Lcom/android/calendar/event/fx;->T:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2441
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2446
    :goto_0
    return-void

    .line 2443
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->T:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2444
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic G(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->E()V

    return-void
.end method

.method static synthetic H(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->G()V

    return-void
.end method

.method static synthetic I(Lcom/android/calendar/event/fx;)Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aw:Landroid/view/View;

    return-object v0
.end method

.method static synthetic J(Lcom/android/calendar/event/fx;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;)I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    return v0
.end method

.method private a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2277
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 2278
    :goto_0
    if-ge v1, v3, :cond_1

    .line 2279
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    if-ne v0, p2, :cond_0

    move v0, v1

    .line 2284
    :goto_1
    return v0

    .line 2278
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2284
    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;J)J
    .locals 3

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/fx;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->S:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    return-object p1
.end method

.method private a(JLjava/lang/Long;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1628
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1629
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1631
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1633
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1634
    iget v2, v0, Landroid/text/format/Time;->hour:I

    iput v2, v1, Landroid/text/format/Time;->hour:I

    .line 1635
    iget v0, v0, Landroid/text/format/Time;->minute:I

    iput v0, v1, Landroid/text/format/Time;->minute:I

    .line 1636
    const/4 v0, 0x0

    iput v0, v1, Landroid/text/format/Time;->second:I

    .line 1637
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1638
    return-void
.end method

.method private a(Landroid/text/format/Time;)V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 1317
    new-instance v2, Lcom/android/calendar/event/gd;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gd;-><init>(Lcom/android/calendar/event/fx;)V

    .line 1343
    new-instance v0, Lcom/android/calendar/common/extension/c;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    iget v3, p1, Landroid/text/format/Time;->year:I

    iget v4, p1, Landroid/text/format/Time;->month:I

    iget v5, p1, Landroid/text/format/Time;->monthDay:I

    iget v6, p1, Landroid/text/format/Time;->hour:I

    iget v7, p1, Landroid/text/format/Time;->minute:I

    iget-object v8, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v8}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v8

    iget-object v10, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0f0375

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/android/calendar/common/extension/c;-><init>(Landroid/content/Context;Lcom/android/calendar/common/extension/i;IIIIIZZLjava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    .line 1347
    iget-object v0, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0, v9}, Lcom/android/calendar/common/extension/c;->setCanceledOnTouchOutside(Z)V

    .line 1348
    iget-object v0, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->show()V

    .line 1349
    return-void
.end method

.method private a(Landroid/text/format/Time;Ljava/lang/Long;II)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1605
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1606
    iput p3, p1, Landroid/text/format/Time;->hour:I

    .line 1607
    iput p4, p1, Landroid/text/format/Time;->minute:I

    .line 1608
    iput v2, p1, Landroid/text/format/Time;->second:I

    .line 1610
    iget-object v0, p1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1611
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    .line 1613
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setLenient(Z)V

    .line 1614
    iget v1, p1, Landroid/text/format/Time;->year:I

    iget v2, p1, Landroid/text/format/Time;->month:I

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v6, 0x0

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Ljava/util/Calendar;->set(IIIIII)V

    .line 1615
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 1616
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    .line 1617
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1623
    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1625
    :goto_0
    return-void

    .line 1618
    :catch_0
    move-exception v0

    .line 1619
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1623
    iput-wide v8, p0, Lcom/android/calendar/event/fx;->ar:J

    goto :goto_0

    .line 1620
    :catch_1
    move-exception v0

    .line 1621
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1623
    iput-wide v8, p0, Lcom/android/calendar/event/fx;->ar:J

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-wide v8, p0, Lcom/android/calendar/event/fx;->ar:J

    throw v0
.end method

.method private a(Landroid/view/View;J)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 747
    const-string v0, "  "

    .line 748
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v5

    .line 751
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->a(J)[Ljava/lang/String;

    move-result-object v1

    .line 753
    iget-object v2, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 754
    iget-object v0, p0, Lcom/android/calendar/event/fx;->x:Landroid/widget/TextView;

    aget-object v2, v1, v4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 755
    iget-object v0, p0, Lcom/android/calendar/event/fx;->x:Landroid/widget/TextView;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 769
    :goto_0
    return-void

    .line 757
    :cond_0
    iget-boolean v2, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 758
    invoke-static {}, Lcom/android/calendar/hj;->s()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 759
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 760
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v2, v1, v5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    :cond_1
    :goto_1
    move-object v0, p1

    .line 766
    check-cast v0, Landroid/widget/TextView;

    aget-object v2, v1, v4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 767
    aget-object v0, v1, v5

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 762
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 763
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v5

    goto :goto_1
.end method

.method private a(Landroid/widget/TextView;J)V
    .locals 2

    .prologue
    .line 856
    invoke-direct {p0, p2, p3}, Lcom/android/calendar/event/fx;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 857
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;I)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/calendar/event/fx;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;JLjava/lang/Long;)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;II)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/fx;->a(Landroid/text/format/Time;Ljava/lang/Long;II)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Landroid/view/View;J)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Landroid/widget/TextView;J)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/fx;Z)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/calendar/event/fx;->c(Z)V

    return-void
.end method

.method private a(Ljava/lang/Long;)V
    .locals 8

    .prologue
    .line 1410
    new-instance v4, Landroid/text/format/Time;

    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1412
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 1415
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1506
    :cond_0
    :goto_0
    return-void

    .line 1419
    :cond_1
    new-instance v2, Lcom/android/calendar/event/gf;

    invoke-direct {v2, p0, v4, p1}, Lcom/android/calendar/event/gf;-><init>(Lcom/android/calendar/event/fx;Landroid/text/format/Time;Ljava/lang/Long;)V

    .line 1473
    new-instance v0, Lcom/android/calendar/common/extension/k;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    iget v3, v4, Landroid/text/format/Time;->hour:I

    iget v4, v4, Landroid/text/format/Time;->minute:I

    iget-object v5, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    sget-object v6, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f03cf

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/android/calendar/common/extension/k;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZLjava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    .line 1477
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/calendar/common/extension/k;->setCanceledOnTouchOutside(Z)V

    .line 1478
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->show()V

    .line 1482
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    new-instance v1, Lcom/android/calendar/event/gg;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gg;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/common/extension/k;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1492
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/common/extension/k;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 1493
    if-eqz v0, :cond_0

    .line 1494
    new-instance v1, Lcom/android/calendar/event/gh;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gh;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 633
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 637
    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 638
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 639
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 641
    :cond_2
    instance-of v0, p2, Landroid/widget/RadioGroup;

    if-eqz v0, :cond_3

    move-object v0, p2

    .line 642
    check-cast v0, Landroid/widget/RadioGroup;

    .line 643
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 644
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 645
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 647
    :cond_3
    instance-of v0, p2, Landroid/widget/Spinner;

    if-eqz v0, :cond_4

    .line 648
    check-cast p2, Landroid/widget/Spinner;

    .line 649
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 650
    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 651
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 655
    :cond_4
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 656
    check-cast p2, Landroid/view/ViewGroup;

    .line 657
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 658
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    .line 659
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/android/calendar/event/fx;->a(Ljava/lang/StringBuilder;Landroid/view/View;)V

    .line 658
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a(J)[Ljava/lang/String;
    .locals 13

    .prologue
    .line 772
    const/4 v0, 0x2

    new-array v9, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, ""

    aput-object v1, v9, v0

    const/4 v0, 0x1

    const-string v1, ""

    aput-object v1, v9, v0

    .line 776
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    if-nez v0, :cond_0

    move-object v0, v9

    .line 837
    :goto_0
    return-object v0

    .line 780
    :cond_0
    new-instance v2, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    .line 782
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 783
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0001

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 785
    if-eqz v2, :cond_a

    .line 786
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "ko"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 788
    :cond_1
    const-string v0, "MDY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 789
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 790
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f02a2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 817
    :goto_1
    invoke-static {}, Lcom/android/calendar/hj;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 818
    const/16 v0, 0x2c

    const/16 v2, 0x60c

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    .line 821
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v10

    .line 824
    sub-long v0, p1, v10

    const-wide v2, -0x1f3a565e880L

    cmp-long v0, v0, v2

    if-gez v0, :cond_8

    .line 825
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 826
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x76e

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 827
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    .line 834
    :cond_2
    :goto_3
    const/4 v0, 0x0

    sub-long v2, p1, v10

    invoke-static {v8, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v0

    .line 835
    const/4 v0, 0x1

    sub-long v2, p1, v10

    invoke-static {v7, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v0

    move-object v0, v9

    .line 837
    goto/16 :goto_0

    .line 792
    :cond_3
    const-string v0, "YMD"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 793
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 794
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0491

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    .line 797
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 798
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f013d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 802
    :cond_5
    const-string v3, "MDY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 803
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 804
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 812
    :cond_6
    :goto_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "kn"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 813
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EEEE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v7, v0

    goto/16 :goto_1

    .line 806
    :cond_7
    const-string v3, "YMD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 807
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 808
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0003

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 828
    :cond_8
    sub-long v0, p1, v10

    const-wide v2, 0x1ec4d45f520L

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 829
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 830
    const/4 v1, 0x0

    const/16 v2, 0x3b

    const/16 v3, 0x17

    const/16 v4, 0x1f

    const/16 v5, 0xb

    const/16 v6, 0x7f4

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 831
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    goto/16 :goto_3

    :cond_9
    move-object v8, v1

    goto/16 :goto_2

    :cond_a
    move-object v7, v0

    goto/16 :goto_1
.end method

.method static synthetic b(I)I
    .locals 0

    .prologue
    .line 98
    sput p0, Lcom/android/calendar/event/fx;->P:I

    return p0
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->x:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    return-object p1
.end method

.method private b(J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 841
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 842
    const-string v0, ""

    .line 851
    :goto_0
    return-object v0

    .line 843
    :cond_0
    const/16 v0, 0x301

    .line 845
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 846
    const/16 v0, 0x381

    .line 849
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 851
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    sub-long v2, p1, v2

    invoke-static {v1, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PM"

    const-string v2, " PM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AM"

    const-string v2, " AM"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2298
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2299
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2300
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    if-ne v0, p2, :cond_0

    .line 2302
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    .line 2305
    :goto_1
    return-object v0

    .line 2299
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2305
    :cond_1
    const-string v0, "local"

    goto :goto_1
.end method

.method private b(Landroid/text/format/Time;)V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1363
    new-instance v2, Lcom/android/calendar/event/ge;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ge;-><init>(Lcom/android/calendar/event/fx;)V

    .line 1401
    new-instance v0, Lcom/android/calendar/common/extension/a;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    iget v3, p1, Landroid/text/format/Time;->year:I

    iget v4, p1, Landroid/text/format/Time;->month:I

    iget v5, p1, Landroid/text/format/Time;->monthDay:I

    sget-object v6, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f03bd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/calendar/event/fx;->l:Z

    if-nez v7, :cond_0

    move v7, v8

    :goto_0
    iget-boolean v10, p0, Lcom/android/calendar/event/fx;->l:Z

    if-nez v10, :cond_1

    :goto_1
    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/common/extension/a;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;IIILjava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    .line 1405
    iget-object v0, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0, v9}, Lcom/android/calendar/common/extension/a;->setCanceledOnTouchOutside(Z)V

    .line 1406
    iget-object v0, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->show()V

    .line 1407
    return-void

    :cond_0
    move v7, v9

    .line 1401
    goto :goto_0

    :cond_1
    move v8, v9

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;I)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/calendar/event/fx;->f(I)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;J)V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/fx;->f(J)V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/event/fx;Z)Z
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/android/calendar/event/fx;->t:Z

    return p1
.end method

.method static synthetic c(I)I
    .locals 0

    .prologue
    .line 98
    sput p0, Lcom/android/calendar/event/fx;->Q:I

    return p0
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/android/calendar/event/fx;->q:I

    return p1
.end method

.method private c(J)J
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1201
    .line 1203
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1204
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1205
    iput v3, v0, Landroid/text/format/Time;->second:I

    .line 1206
    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 1208
    return-wide v0
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;J)J
    .locals 1

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/android/calendar/event/fx;->ar:J

    return-wide p1
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/calendar/event/fx;->a(Ljava/lang/Long;)V

    return-void
.end method

.method private c(Z)V
    .locals 6

    .prologue
    const v5, 0x7f120157

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 911
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 913
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 914
    if-eqz p1, :cond_2

    .line 915
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 916
    const v1, 0x7f120153

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setNextFocusUpId(I)V

    .line 917
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 918
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    if-eq v0, v4, :cond_0

    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 919
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 920
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 921
    iput v2, p0, Lcom/android/calendar/event/fx;->s:I

    .line 922
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/fx;->s:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 923
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    .line 930
    :cond_1
    :goto_0
    return-void

    .line 926
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 927
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setNextFocusUpId(I)V

    .line 928
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/event/fx;Z)Z
    .locals 0

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/android/calendar/event/fx;->u:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/fx;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/android/calendar/event/fx;->s:I

    return p1
.end method

.method private d(J)J
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1212
    .line 1214
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1215
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1216
    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 1217
    iput v3, v0, Landroid/text/format/Time;->minute:I

    .line 1218
    iput v3, v0, Landroid/text/format/Time;->second:I

    .line 1219
    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 1221
    return-wide v0
.end method

.method static synthetic d(Lcom/android/calendar/event/fx;J)J
    .locals 1

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/android/calendar/event/fx;->ap:J

    return-wide p1
.end method

.method static synthetic d(Lcom/android/calendar/event/fx;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->U:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private d(I)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v4, -0x1

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1642
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1643
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1646
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1762
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1768
    :goto_0
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-wide v4, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    .line 1770
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v1, :cond_f

    .line 1771
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1772
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1778
    :goto_1
    iput p1, p0, Lcom/android/calendar/event/fx;->s:I

    .line 1779
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    .line 1783
    :goto_2
    return-void

    .line 1650
    :pswitch_0
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-nez v1, :cond_6

    .line 1651
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->t:Z

    if-eqz v1, :cond_4

    .line 1652
    iput-boolean v0, p0, Lcom/android/calendar/event/fx;->t:Z

    .line 1653
    iget-object v1, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 1654
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-object v1, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    .line 1670
    :cond_2
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1671
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1672
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1674
    iput p1, p0, Lcom/android/calendar/event/fx;->s:I

    .line 1675
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    goto :goto_2

    .line 1656
    :cond_3
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-wide v4, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    goto :goto_3

    .line 1658
    :cond_4
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->u:Z

    if-eqz v1, :cond_2

    .line 1659
    iput-boolean v0, p0, Lcom/android/calendar/event/fx;->u:Z

    .line 1660
    iget-object v1, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 1661
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    goto :goto_3

    .line 1663
    :cond_5
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-wide v4, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    goto :goto_3

    .line 1667
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    goto :goto_3

    .line 1680
    :pswitch_1
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/event/fx;->c(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1682
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    .line 1683
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-object v1, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    .line 1686
    :try_start_1
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-nez v1, :cond_7

    .line 1687
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1694
    :cond_7
    :goto_4
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v1, :cond_8

    .line 1695
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1696
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1701
    :goto_5
    iput-boolean v6, p0, Lcom/android/calendar/event/fx;->t:Z

    .line 1703
    iput p1, p0, Lcom/android/calendar/event/fx;->s:I

    .line 1704
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    goto/16 :goto_2

    .line 1689
    :catch_0
    move-exception v1

    .line 1691
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move p1, v0

    goto :goto_4

    .line 1698
    :cond_8
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1699
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    .line 1709
    :pswitch_2
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/event/fx;->c(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1711
    iget-object v1, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 1712
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    .line 1717
    :goto_6
    iput p1, p0, Lcom/android/calendar/event/fx;->s:I

    .line 1719
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v1, :cond_a

    .line 1720
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/calendar/event/fx;->a(Ljava/lang/Long;)V

    .line 1721
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1722
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1737
    :goto_7
    iput-boolean v6, p0, Lcom/android/calendar/event/fx;->u:Z

    goto/16 :goto_2

    .line 1714
    :cond_9
    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-wide v4, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v2, v3, v1}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    goto :goto_6

    .line 1724
    :cond_a
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v1, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    .line 1726
    :try_start_2
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1732
    :goto_8
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1733
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1735
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    goto :goto_7

    .line 1727
    :catch_1
    move-exception v1

    .line 1729
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 1742
    :pswitch_3
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_b

    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_d

    .line 1743
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 1744
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1746
    :cond_c
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/fx;->c(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 1749
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->S:Landroid/widget/TextView;

    .line 1751
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v0, :cond_e

    .line 1752
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/calendar/event/fx;->a(JLjava/lang/Long;)V

    .line 1753
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/fx;->e(J)V

    goto/16 :goto_2

    .line 1755
    :cond_e
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/fx;->f(J)V

    goto/16 :goto_2

    .line 1763
    :catch_2
    move-exception v1

    .line 1765
    iget-object v1, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    move p1, v0

    goto/16 :goto_0

    .line 1774
    :cond_f
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1775
    iget-object v1, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1

    .line 1646
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 2288
    const/4 v0, 0x0

    .line 2289
    iget-object v1, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2290
    if-ge p1, v1, :cond_0

    .line 2291
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    .line 2294
    :cond_0
    return v0
.end method

.method static synthetic e(Lcom/android/calendar/event/fx;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/android/calendar/event/fx;->L:I

    return p1
.end method

.method static synthetic e(Lcom/android/calendar/event/fx;)J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    return-wide v0
.end method

.method private e(J)V
    .locals 3

    .prologue
    .line 1308
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1309
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1310
    iget-object v1, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/fx;->i:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v1}, Lcom/android/calendar/common/extension/c;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1314
    :goto_0
    return-void

    .line 1313
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->a(Landroid/text/format/Time;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/event/fx;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/android/calendar/event/fx;->ag:I

    return p1
.end method

.method static synthetic f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    return-object v0
.end method

.method private f(I)V
    .locals 5

    .prologue
    const v4, 0x7f1200f7

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2643
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 2644
    if-nez p1, :cond_1

    .line 2645
    iget-object v0, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2646
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2647
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2648
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 2649
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 2650
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 2667
    :cond_0
    :goto_0
    return-void

    .line 2651
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-nez v0, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 2652
    iget-object v0, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2653
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2654
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2655
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 2656
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 2657
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 2659
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2660
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2661
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2662
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 2663
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 2664
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method

.method private f(J)V
    .locals 3

    .prologue
    .line 1352
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1353
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 1355
    iget-object v1, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v1}, Lcom/android/calendar/common/extension/a;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1359
    :goto_0
    return-void

    .line 1357
    :cond_0
    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->b(Landroid/text/format/Time;)V

    goto :goto_0
.end method

.method static synthetic g()I
    .locals 1

    .prologue
    .line 98
    sget v0, Lcom/android/calendar/event/fx;->P:I

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/event/fx;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/android/calendar/event/fx;->aa:I

    return p1
.end method

.method static synthetic g(Lcom/android/calendar/event/fx;)J
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    return-wide v0
.end method

.method static synthetic h()Landroid/content/Context;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/event/fx;)[Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->W:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/event/fx;)Landroid/animation/LayoutTransition;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    return-object v0
.end method

.method private i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 267
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    if-nez v1, :cond_0

    .line 268
    const/4 v0, 0x0

    .line 300
    :goto_0
    return v0

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    .line 274
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/fx;->aq:Ljava/lang/String;

    .line 277
    iget-object v1, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 278
    :cond_1
    const-string v1, ""

    iput-object v1, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    .line 281
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v2, p0, Lcom/android/calendar/event/fx;->h:I

    iput v2, v1, Lcom/android/calendar/hh;->d:I

    .line 282
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 283
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    iput-object v2, v1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 284
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aq:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    .line 285
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iput-object v2, v1, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    .line 286
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-string v2, "preference_defaultTaskId"

    iget v3, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v1, v2, v3}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 287
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v2, p0, Lcom/android/calendar/event/fx;->q:I

    iput v2, v1, Lcom/android/calendar/hh;->o:I

    .line 288
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->j:I

    iget v2, p0, Lcom/android/calendar/event/fx;->ag:I

    if-eq v1, v2, :cond_3

    .line 289
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v2, p0, Lcom/android/calendar/event/fx;->ag:I

    iput v2, v1, Lcom/android/calendar/hh;->j:I

    .line 290
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iput-boolean v0, v1, Lcom/android/calendar/hh;->G:Z

    .line 293
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v1, v1, Lcom/android/calendar/hh;->j:I

    if-gtz v1, :cond_4

    .line 294
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iput v0, v1, Lcom/android/calendar/hh;->j:I

    .line 297
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v2, p0, Lcom/android/calendar/event/fx;->s:I

    iput v2, v1, Lcom/android/calendar/hh;->p:I

    .line 298
    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    iput-wide v2, v1, Lcom/android/calendar/hh;->r:J

    goto/16 :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aA:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 614
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 616
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    if-nez v1, :cond_1

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 620
    iget-object v2, p0, Lcom/android/calendar/event/fx;->ak:Landroid/view/View;

    invoke-direct {p0, v1, v2}, Lcom/android/calendar/event/fx;->a(Ljava/lang/StringBuilder;Landroid/view/View;)V

    .line 621
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 623
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 624
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 625
    iget-object v3, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 626
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 627
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 629
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/android/calendar/event/fx;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    return-object v0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 732
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    .line 734
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->am:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 736
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 737
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 739
    new-instance v0, Lcom/android/calendar/event/hl;

    const/16 v1, 0x3e8

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/hl;-><init>(Lcom/android/calendar/event/fx;I)V

    .line 740
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 744
    return-void
.end method

.method static synthetic l(Lcom/android/calendar/event/fx;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 861
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/android/calendar/event/gu;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gu;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 879
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 880
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    .line 883
    iget-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/fx;->c(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    .line 888
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ap:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 890
    return-void

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    goto :goto_0
.end method

.method static synthetic m(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->S:Landroid/widget/TextView;

    return-object v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 893
    iget-object v0, p0, Lcom/android/calendar/event/fx;->T:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/hd;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hd;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 907
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->c(Z)V

    .line 908
    return-void
.end method

.method static synthetic n(Lcom/android/calendar/event/fx;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    return-object v0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lcom/android/calendar/event/fx;->F:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/he;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/he;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 940
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->D()V

    .line 941
    return-void
.end method

.method static synthetic o(Lcom/android/calendar/event/fx;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 944
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ay:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 945
    iget-object v0, p0, Lcom/android/calendar/event/fx;->G:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/hf;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hf;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 951
    iget v0, p0, Lcom/android/calendar/event/fx;->L:I

    if-nez v0, :cond_0

    .line 952
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    .line 958
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 959
    iget-object v0, p0, Lcom/android/calendar/event/fx;->H:Landroid/widget/TextView;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 961
    :cond_1
    return-void
.end method

.method private p()V
    .locals 2

    .prologue
    .line 964
    iget-object v0, p0, Lcom/android/calendar/event/fx;->E:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/hg;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hg;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->s()V

    .line 974
    return-void
.end method

.method static synthetic p(Lcom/android/calendar/event/fx;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    return v0
.end method

.method private q()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 977
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    .line 979
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/hh;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hh;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 985
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v0, :cond_2

    .line 986
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 987
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 994
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ah:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 996
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 997
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    .line 1002
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/hi;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hi;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1009
    return-void

    .line 990
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 991
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 999
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/fx;->s:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1000
    iget v0, p0, Lcom/android/calendar/event/fx;->s:I

    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->f(I)V

    goto :goto_1
.end method

.method static synthetic q(Lcom/android/calendar/event/fx;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->t:Z

    return v0
.end method

.method static synthetic r(Lcom/android/calendar/event/fx;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    return-object v0
.end method

.method private r()V
    .locals 4

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    .line 1015
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1017
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 1018
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 1020
    new-instance v0, Lcom/android/calendar/event/hl;

    const/16 v1, 0x3e8

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/hl;-><init>(Lcom/android/calendar/event/fx;I)V

    .line 1021
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1024
    return-void
.end method

.method private s()V
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1027
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_1

    .line 1028
    iget v0, p0, Lcom/android/calendar/event/fx;->q:I

    if-le v0, v1, :cond_0

    .line 1029
    iput v1, p0, Lcom/android/calendar/event/fx;->q:I

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    sget-object v1, Lcom/android/calendar/event/fx;->Z:[I

    iget v2, p0, Lcom/android/calendar/event/fx;->q:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1045
    :goto_0
    return-void

    .line 1033
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->W:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/android/calendar/event/fx;->q:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1035
    iget v0, p0, Lcom/android/calendar/event/fx;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 1036
    iget-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1037
    iget-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    sget-object v1, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/fx;->Y:[I

    iget v3, p0, Lcom/android/calendar/event/fx;->q:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1041
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1042
    iget-object v0, p0, Lcom/android/calendar/event/fx;->V:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method static synthetic s(Lcom/android/calendar/event/fx;)Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->u:Z

    return v0
.end method

.method static synthetic t(Lcom/android/calendar/event/fx;)Landroid/view/View;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->D:Landroid/view/View;

    return-object v0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 1052
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->k()V

    .line 1055
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->l()V

    .line 1058
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->m()V

    .line 1060
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_1

    .line 1062
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 1064
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1066
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const v3, 0x7f04002c

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1068
    const v1, 0x7f04009c

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 1069
    iget-object v1, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1070
    iget-object v0, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/fx;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1072
    iget-object v0, p0, Lcom/android/calendar/event/fx;->R:Landroid/widget/Spinner;

    new-instance v1, Lcom/android/calendar/event/fz;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/fz;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1097
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->q()V

    .line 1100
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->r()V

    .line 1102
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    if-nez v0, :cond_0

    .line 1103
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    .line 1105
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->u()V

    .line 1109
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/ga;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ga;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1125
    return-void

    .line 1087
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->n()V

    .line 1090
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->p()V

    .line 1093
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->o()V

    goto :goto_0
.end method

.method static synthetic u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private u()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x2

    const-wide/16 v4, 0x12c

    .line 1128
    .line 1130
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aA:Landroid/widget/LinearLayout;

    const-string v1, "alpha"

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1131
    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1132
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1135
    new-instance v1, Lcom/android/calendar/event/gb;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gb;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1170
    iget-object v1, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    const-string v2, "alpha"

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1171
    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1173
    iget-object v2, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    invoke-virtual {v2, v6, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 1174
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    invoke-virtual {v1, v6, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 1176
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aB:Landroid/animation/LayoutTransition;

    new-instance v2, Lcom/android/calendar/event/gc;

    invoke-direct {v2, p0, v0}, Lcom/android/calendar/event/gc;-><init>(Lcom/android/calendar/event/fx;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 1197
    return-void

    .line 1130
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1170
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private v()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1510
    iget-object v0, p0, Lcom/android/calendar/event/fx;->an:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 1512
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1514
    new-array v0, v4, [Ljava/lang/CharSequence;

    .line 1515
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    aput-object v1, v0, v5

    .line 1516
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v3

    aput-object v1, v0, v2

    .line 1517
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v4

    aput-object v1, v0, v3

    .line 1541
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1542
    const v2, 0x7f0f0375

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/android/calendar/event/gl;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/gl;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0163

    new-instance v3, Lcom/android/calendar/event/gk;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/gk;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/android/calendar/event/gi;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gi;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 1599
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1600
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 1602
    :cond_0
    :goto_1
    return-void

    .line 1520
    :cond_1
    new-array v0, v3, [Ljava/lang/CharSequence;

    .line 1521
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    aput-object v1, v0, v5

    .line 1522
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v4

    aput-object v1, v0, v2

    goto :goto_0

    .line 1526
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 1528
    new-array v0, v3, [Ljava/lang/CharSequence;

    .line 1529
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v3

    aput-object v1, v0, v5

    .line 1530
    iget-object v1, p0, Lcom/android/calendar/event/fx;->r:[Ljava/lang/CharSequence;

    aget-object v1, v1, v4

    aput-object v1, v0, v2

    goto :goto_0

    .line 1534
    :cond_3
    invoke-direct {p0, v4}, Lcom/android/calendar/event/fx;->d(I)V

    goto :goto_1
.end method

.method static synthetic v(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->s()V

    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 1786
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1787
    const v1, 0x7f0f0345

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1788
    iget-object v1, p0, Lcom/android/calendar/event/fx;->X:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->W:[Ljava/lang/CharSequence;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/calendar/event/fx;->q:I

    sub-int/2addr v2, v3

    new-instance v3, Lcom/android/calendar/event/gm;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/gm;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1801
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/gn;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gn;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1811
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1812
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 1814
    :cond_0
    return-void
.end method

.method static synthetic w(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->y()V

    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 1818
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1819
    const v1, 0x7f0f00b2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1820
    const v1, 0x7f0f00b1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1821
    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/event/gp;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gp;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/go;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/go;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1838
    const v1, 0x7f0f009e

    new-instance v2, Lcom/android/calendar/event/gr;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gr;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/gq;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gq;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1864
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1865
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 1867
    :cond_0
    return-void
.end method

.method static synthetic x(Lcom/android/calendar/event/fx;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    return-object v0
.end method

.method private y()V
    .locals 3

    .prologue
    .line 1938
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-nez v0, :cond_0

    .line 1939
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1941
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1942
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-class v2, Lcom/android/calendar/task/GroupCreateActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1943
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1944
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1945
    return-void
.end method

.method static synthetic y(Lcom/android/calendar/event/fx;)[Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic z(Lcom/android/calendar/event/fx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/calendar/event/fx;->K:Ljava/lang/String;

    return-object v0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 1948
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1950
    const v1, 0x7f0400aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->n:Landroid/view/View;

    .line 1951
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1285
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1305
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 1287
    :pswitch_1
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->v()V

    goto :goto_0

    .line 1290
    :pswitch_2
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->w()V

    goto :goto_0

    .line 1293
    :pswitch_3
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->B()V

    goto :goto_0

    .line 1296
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/calendar/event/fx;->b()V

    goto :goto_0

    .line 1299
    :pswitch_5
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->x()V

    goto :goto_0

    .line 1285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2309
    if-nez p1, :cond_1

    .line 2337
    :cond_0
    :goto_0
    return-void

    .line 2313
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/fx;->b:I

    .line 2314
    iget v0, p0, Lcom/android/calendar/event/fx;->b:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    .line 2315
    iget v0, p0, Lcom/android/calendar/event/fx;->b:I

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    move v0, v1

    .line 2317
    :goto_1
    iget v2, p0, Lcom/android/calendar/event/fx;->b:I

    if-ge v0, v2, :cond_3

    .line 2318
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2319
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2320
    if-eqz v2, :cond_2

    const-string v3, "Default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2321
    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2323
    :cond_2
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2324
    iget-object v4, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    aput-object v2, v4, v0

    .line 2326
    iget-object v2, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 2317
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2330
    :cond_3
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_4

    .line 2331
    invoke-virtual {p0}, Lcom/android/calendar/event/fx;->d()V

    .line 2334
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->at:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 2335
    iget-object v0, p0, Lcom/android/calendar/event/fx;->at:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2063
    iput-object p1, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    .line 2064
    iput-object p2, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    .line 2066
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2092
    :goto_0
    return-void

    .line 2069
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/fx;->af:I

    .line 2070
    iget v0, p0, Lcom/android/calendar/event/fx;->af:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    .line 2072
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    .line 2074
    if-nez v1, :cond_2

    .line 2076
    iget-object v4, p0, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    iget-object v5, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0288

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 2082
    :goto_2
    iget-object v4, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v4, v4, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2083
    iget-object v4, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget-object v4, v4, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    iget v4, v4, Lcom/android/calendar/hh;->d:I

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    if-ne v4, v0, :cond_1

    .line 2085
    sput v1, Lcom/android/calendar/event/fx;->P:I

    .line 2088
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2089
    goto :goto_1

    .line 2079
    :cond_2
    iget-object v4, p0, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    iget-object v5, v0, Lcom/android/calendar/task/ab;->g:Ljava/lang/String;

    aput-object v5, v4, v1

    goto :goto_2

    .line 2091
    :cond_3
    new-instance v0, Lcom/android/calendar/event/hj;

    sget-object v1, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/android/calendar/event/hj;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->c:Lcom/android/calendar/event/hj;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x80000

    const v2, -0x80001

    .line 2602
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 2603
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->ax:Z

    if-eqz v0, :cond_2

    .line 2604
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    .line 2606
    :goto_0
    if-eqz v0, :cond_4

    .line 2607
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 2608
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    or-int/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2610
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    const-string v1, "inputType=PredictionOff"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 2612
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 2613
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    or-int/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2615
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    const-string v1, "inputType=PredictionOff"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 2629
    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    .line 2631
    if-eqz p1, :cond_2

    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 2632
    check-cast p1, Landroid/widget/EditText;

    .line 2633
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2635
    invoke-virtual {v0, p1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 2640
    :cond_2
    return-void

    .line 2604
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2618
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_5

    .line 2619
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2621
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 2623
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 2624
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 2626
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/android/calendar/hh;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 451
    iput-object p1, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    .line 457
    if-nez p1, :cond_1

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 466
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 467
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 468
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 471
    :cond_2
    iget-object v0, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 472
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 473
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 474
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 478
    :cond_3
    iget v0, p1, Lcom/android/calendar/hh;->F:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const/16 v1, 0x63

    if-le v0, v1, :cond_4

    .line 479
    iput-object v4, p1, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    .line 483
    :cond_4
    iget-object v0, p1, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 484
    iget v0, p1, Lcom/android/calendar/hh;->d:I

    iput v0, p0, Lcom/android/calendar/event/fx;->h:I

    .line 485
    iget-object v0, p1, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 486
    iget v0, p1, Lcom/android/calendar/hh;->d:I

    sput v0, Lcom/android/calendar/event/fx;->P:I

    .line 536
    :cond_5
    :goto_1
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->D()V

    .line 539
    iget v0, p1, Lcom/android/calendar/hh;->o:I

    iput v0, p0, Lcom/android/calendar/event/fx;->q:I

    .line 540
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->s()V

    .line 543
    iget v0, p1, Lcom/android/calendar/hh;->j:I

    iput v0, p0, Lcom/android/calendar/event/fx;->ag:I

    move v0, v2

    .line 547
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    array-length v1, v1

    if-ge v0, v1, :cond_12

    .line 548
    iget-object v1, p0, Lcom/android/calendar/event/fx;->J:[Ljava/lang/Integer;

    aget-object v1, v1, v0

    iget v5, p0, Lcom/android/calendar/event/fx;->ag:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v1, v3

    .line 554
    :goto_3
    if-eqz v1, :cond_e

    .line 555
    if-nez v0, :cond_c

    .line 556
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v1, :cond_b

    .line 557
    iput v3, p0, Lcom/android/calendar/event/fx;->L:I

    .line 575
    :goto_4
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->E()V

    .line 578
    iget-wide v0, p1, Lcom/android/calendar/hh;->r:J

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ar:J

    .line 579
    iget v0, p1, Lcom/android/calendar/hh;->p:I

    iput v0, p0, Lcom/android/calendar/event/fx;->s:I

    .line 581
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->F()V

    .line 584
    iget-object v0, p0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/android/calendar/event/fx;->F:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 587
    iget-object v0, p0, Lcom/android/calendar/event/fx;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/android/calendar/event/fx;->A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 591
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->j()V

    .line 592
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->G()V

    .line 594
    iget-object v0, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 595
    iget-object v0, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/fx;->ap:J

    .line 596
    iget-object v0, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    .line 597
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    iget-wide v4, p0, Lcom/android/calendar/event/fx;->ap:J

    invoke-direct {p0, v0, v4, v5}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 603
    :goto_5
    invoke-virtual {p1}, Lcom/android/calendar/hh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/android/calendar/event/fx;->az:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aA:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getImeOptions()I

    move-result v1

    or-int/lit8 v1, v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 608
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 609
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto/16 :goto_0

    .line 488
    :cond_6
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    const-string v1, "preference_defaultTaskId"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/fx;->h:I

    .line 492
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    .line 493
    if-eqz v0, :cond_9

    iget-object v6, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    if-eqz v6, :cond_9

    .line 494
    iget v6, p0, Lcom/android/calendar/event/fx;->h:I

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    if-ne v6, v0, :cond_9

    .line 495
    sput v1, Lcom/android/calendar/event/fx;->P:I

    .line 496
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    sget v5, Lcom/android/calendar/event/fx;->P:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 503
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_8

    .line 504
    iput v2, p0, Lcom/android/calendar/event/fx;->h:I

    .line 505
    const-string v0, "My Task"

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 506
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const-string v1, "preference_defaultTaskId"

    iget v5, p0, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v0, v1, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 513
    :cond_8
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    sget v1, Lcom/android/calendar/event/fx;->P:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :goto_7
    if-eqz v0, :cond_5

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    const-string v1, "preferences_samsung_account_validation"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 522
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 523
    const-string v1, "request_module"

    const/16 v5, 0x1f6

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 526
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const/16 v5, 0x1f4

    invoke-virtual {v1, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 532
    :goto_8
    sput v2, Lcom/android/calendar/event/fx;->Q:I

    goto/16 :goto_1

    .line 500
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 501
    goto :goto_6

    .line 514
    :catch_0
    move-exception v0

    .line 515
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    move-object v0, v4

    goto :goto_7

    .line 528
    :catch_1
    move-exception v0

    .line 529
    const-string v0, "EditTaskView"

    const-string v1, "Error: Could not find AccountValidationCheckActivity.ACTION activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 547
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 559
    :cond_b
    iput v0, p0, Lcom/android/calendar/event/fx;->L:I

    goto/16 :goto_4

    .line 562
    :cond_c
    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v1, :cond_d

    .line 563
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/fx;->L:I

    goto/16 :goto_4

    .line 565
    :cond_d
    iput v0, p0, Lcom/android/calendar/event/fx;->L:I

    goto/16 :goto_4

    .line 569
    :cond_e
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_f

    .line 570
    iput v3, p0, Lcom/android/calendar/event/fx;->L:I

    goto/16 :goto_4

    .line 572
    :cond_f
    iput v2, p0, Lcom/android/calendar/event/fx;->L:I

    goto/16 :goto_4

    .line 599
    :cond_10
    iput-object v4, p0, Lcom/android/calendar/event/fx;->ao:Ljava/lang/Long;

    .line 600
    iget-object v0, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    if-nez v0, :cond_11

    move v0, v3

    :goto_9
    invoke-direct {p0, v0}, Lcom/android/calendar/event/fx;->c(Z)V

    goto/16 :goto_5

    :cond_11
    move v0, v2

    goto :goto_9

    :cond_12
    move v1, v2

    goto/16 :goto_3
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 429
    if-eqz p1, :cond_0

    .line 430
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 432
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 435
    :cond_0
    if-eqz p2, :cond_1

    .line 436
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 437
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 438
    iget-object v1, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 440
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 665
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    if-nez v0, :cond_0

    .line 679
    :goto_0
    return-void

    .line 668
    :cond_0
    if-eqz p1, :cond_1

    .line 669
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 671
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    const-string v1, "inputType=PredictionOff"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 677
    :goto_1
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 678
    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    goto :goto_0

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v1

    const v2, -0x80001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 675
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(ZI)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x64

    const/4 v3, 0x0

    .line 2449
    .line 2451
    if-nez p2, :cond_2

    .line 2452
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2453
    const-string v0, "action_view_focus"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2454
    const-string v2, "action_view_focus"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2459
    :goto_0
    if-eqz p1, :cond_1

    .line 2460
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/android/calendar/event/fx;->m:Z

    if-nez v1, :cond_0

    .line 2461
    const/4 v0, 0x1

    .line 2464
    :cond_0
    iput-boolean v3, p0, Lcom/android/calendar/event/fx;->m:Z

    .line 2466
    sparse-switch v0, :sswitch_data_0

    .line 2547
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, p2

    .line 2456
    goto :goto_0

    .line 2469
    :sswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->aw:Landroid/view/View;

    .line 2470
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2473
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aw:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/fx;->aw:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 2474
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aw:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/gz;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gz;-><init>(Lcom/android/calendar/event/fx;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 2492
    :sswitch_1
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_4

    .line 2493
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/event/ha;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ha;-><init>(Lcom/android/calendar/event/fx;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 2513
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fx;->v:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/event/hb;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hb;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 2528
    :sswitch_2
    iget-object v0, p0, Lcom/android/calendar/event/fx;->y:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/event/hc;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hc;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 2466
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/android/calendar/event/fx;->al:Lcom/android/calendar/hh;

    if-nez v0, :cond_0

    .line 257
    const/4 v0, 0x0

    .line 259
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->i()Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1870
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->z()V

    .line 1871
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->A()V

    .line 1873
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1874
    const v1, 0x7f0f021b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1875
    iget-object v1, p0, Lcom/android/calendar/event/fx;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1876
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 1881
    iget-object v1, p0, Lcom/android/calendar/event/fx;->au:Landroid/widget/ListView;

    new-instance v2, Lcom/android/calendar/event/gs;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gs;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1903
    new-instance v1, Lcom/android/calendar/event/gt;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gt;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1913
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/gv;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/gv;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1924
    iget-object v1, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1925
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 1927
    :cond_0
    return-void
.end method

.method public b(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 2097
    iput-object p1, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    .line 2098
    iput-object p2, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    .line 2100
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 2101
    :cond_0
    const-string v0, "EditTaskView"

    const-string v1, "setCalendarsCursor : no calendar account found "

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2110
    :goto_0
    return-void

    .line 2105
    :cond_1
    new-instance v0, Lcom/android/calendar/event/hk;

    sget-object v1, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/android/calendar/event/hk;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/fx;->d:Lcom/android/calendar/event/hk;

    .line 2106
    iget-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->d:Lcom/android/calendar/event/hk;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2107
    iget-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/fx;->O:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2108
    iget-object v0, p0, Lcom/android/calendar/event/fx;->N:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 2583
    sget v0, Lcom/android/calendar/event/fx;->Q:I

    .line 2585
    if-eqz p1, :cond_2

    .line 2586
    sget v0, Lcom/android/calendar/event/fx;->P:I

    move v1, v0

    .line 2589
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 2599
    :goto_1
    return-void

    .line 2592
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    iput v0, p0, Lcom/android/calendar/event/fx;->h:I

    .line 2593
    iget-object v0, p0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 2595
    if-nez p1, :cond_1

    .line 2596
    sget v0, Lcom/android/calendar/event/fx;->Q:I

    sput v0, Lcom/android/calendar/event/fx;->P:I

    .line 2598
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->D()V

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public c()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1930
    iget-object v0, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1931
    sget-object v1, Lcom/android/calendar/event/fv;->h:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/event/fv;->c:[Ljava/lang/String;

    const-string v5, "_accountId ASC, group_order ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->at:Landroid/database/Cursor;

    .line 1934
    iget-object v0, p0, Lcom/android/calendar/event/fx;->at:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/fx;->a(Landroid/database/Cursor;)V

    .line 1935
    return-void
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2341
    sget-object v0, Lcom/android/calendar/event/fx;->as:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f010d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2342
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343
    iput v4, p0, Lcom/android/calendar/event/fx;->L:I

    .line 2345
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/android/calendar/event/fx;->b:I

    if-ge v0, v2, :cond_0

    .line 2346
    iget-object v2, p0, Lcom/android/calendar/event/fx;->I:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2345
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2348
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/calendar/event/fx;->aj:Landroid/app/Activity;

    const v3, 0x7f04002c

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2351
    const v1, 0x7f04009c

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 2352
    iget-object v1, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2353
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->o:Z

    if-nez v0, :cond_1

    .line 2354
    iput-boolean v4, p0, Lcom/android/calendar/event/fx;->o:Z

    .line 2355
    iget-object v0, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/fx;->L:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2358
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->e:Landroid/widget/Spinner;

    new-instance v1, Lcom/android/calendar/event/gy;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/gy;-><init>(Lcom/android/calendar/event/fx;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2376
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 2551
    iget-object v0, p0, Lcom/android/calendar/event/fx;->w:Landroid/widget/RelativeLayout;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ap:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 2552
    iget-object v0, p0, Lcom/android/calendar/event/fx;->B:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/view/View;J)V

    .line 2553
    iget-object v0, p0, Lcom/android/calendar/event/fx;->z:Landroid/widget/Button;

    iget-wide v2, p0, Lcom/android/calendar/event/fx;->ar:J

    invoke-direct {p0, v0, v2, v3}, Lcom/android/calendar/event/fx;->a(Landroid/widget/TextView;J)V

    .line 2555
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2558
    iget-object v0, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    .line 2559
    iget-object v0, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->dismiss()V

    .line 2560
    iput-object v1, p0, Lcom/android/calendar/event/fx;->j:Lcom/android/calendar/common/extension/a;

    .line 2563
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    if-eqz v0, :cond_1

    .line 2564
    iget-object v0, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->dismiss()V

    .line 2565
    iput-object v1, p0, Lcom/android/calendar/event/fx;->k:Lcom/android/calendar/common/extension/k;

    .line 2568
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2569
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2570
    iput-object v1, p0, Lcom/android/calendar/event/fx;->ai:Landroid/app/AlertDialog;

    .line 2572
    :cond_2
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 2114
    iput p3, p0, Lcom/android/calendar/event/fx;->O:I

    .line 2116
    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    iput v0, p0, Lcom/android/calendar/event/fx;->aa:I

    .line 2117
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    .line 2118
    invoke-direct {p0, p3}, Lcom/android/calendar/event/fx;->e(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/fx;->h:I

    .line 2119
    iget-object v0, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2120
    iget-object v0, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    const-string v1, "_sync_account"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 2121
    iget-object v1, p0, Lcom/android/calendar/event/fx;->M:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    .line 2124
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    iget v1, p0, Lcom/android/calendar/event/fx;->aa:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    iget v1, p0, Lcom/android/calendar/event/fx;->aa:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2126
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->E()V

    .line 2127
    iget-boolean v0, p0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_3

    .line 2128
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/fx;->L:I

    .line 2132
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/event/fx;->G()V

    .line 2134
    :cond_2
    iget v0, p0, Lcom/android/calendar/event/fx;->h:I

    iput v0, p0, Lcom/android/calendar/event/fx;->aa:I

    .line 2135
    iget-object v0, p0, Lcom/android/calendar/event/fx;->ab:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/fx;->ac:Ljava/lang/String;

    .line 2137
    return-void

    .line 2130
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/fx;->L:I

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 2142
    return-void
.end method
