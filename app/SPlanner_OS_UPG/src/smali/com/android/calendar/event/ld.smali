.class public Lcom/android/calendar/event/ld;
.super Landroid/app/DialogFragment;
.source "SimpleAddDialogFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static e:I

.field private static h:Landroid/app/Activity;

.field private static k:Z

.field private static l:Z


# instance fields
.field private A:I

.field private B:I

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Landroid/database/Cursor;

.field private G:Landroid/database/Cursor;

.field private H:Landroid/widget/Spinner;

.field private I:Landroid/widget/Spinner;

.field private J:J

.field b:Lcom/android/calendar/event/ll;

.field c:Lcom/android/calendar/event/lm;

.field public f:Ljava/util/ArrayList;

.field g:Landroid/text/format/Time;

.field private i:Lcom/android/calendar/al;

.field private j:Landroid/content/AsyncQueryHandler;

.field private m:Landroid/view/View;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 79
    const-class v0, Lcom/android/calendar/event/ld;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    .line 180
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "account_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/ld;->d:[Ljava/lang/String;

    .line 235
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/event/ld;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 241
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/ld;->w:I

    .line 133
    iput v1, p0, Lcom/android/calendar/event/ld;->x:I

    .line 134
    iput v1, p0, Lcom/android/calendar/event/ld;->y:I

    .line 136
    iput v1, p0, Lcom/android/calendar/event/ld;->z:I

    .line 137
    iput v1, p0, Lcom/android/calendar/event/ld;->A:I

    .line 139
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ld;->B:I

    .line 147
    iput-boolean v1, p0, Lcom/android/calendar/event/ld;->D:Z

    .line 149
    iput-boolean v1, p0, Lcom/android/calendar/event/ld;->E:Z

    .line 156
    iput-object v2, p0, Lcom/android/calendar/event/ld;->b:Lcom/android/calendar/event/ll;

    .line 161
    iput-object v2, p0, Lcom/android/calendar/event/ld;->c:Lcom/android/calendar/event/lm;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ld;->f:Ljava/util/ArrayList;

    .line 239
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    .line 1259
    iput-object v2, p0, Lcom/android/calendar/event/ld;->g:Landroid/text/format/Time;

    .line 242
    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 244
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 131
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/ld;->w:I

    .line 133
    iput v2, p0, Lcom/android/calendar/event/ld;->x:I

    .line 134
    iput v2, p0, Lcom/android/calendar/event/ld;->y:I

    .line 136
    iput v2, p0, Lcom/android/calendar/event/ld;->z:I

    .line 137
    iput v2, p0, Lcom/android/calendar/event/ld;->A:I

    .line 139
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ld;->B:I

    .line 147
    iput-boolean v2, p0, Lcom/android/calendar/event/ld;->D:Z

    .line 149
    iput-boolean v2, p0, Lcom/android/calendar/event/ld;->E:Z

    .line 156
    iput-object v3, p0, Lcom/android/calendar/event/ld;->b:Lcom/android/calendar/event/ll;

    .line 161
    iput-object v3, p0, Lcom/android/calendar/event/ld;->c:Lcom/android/calendar/event/lm;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ld;->f:Ljava/util/ArrayList;

    .line 239
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    .line 1259
    iput-object v3, p0, Lcom/android/calendar/event/ld;->g:Landroid/text/format/Time;

    .line 245
    sput-boolean p1, Lcom/android/calendar/event/ld;->k:Z

    .line 246
    sput-boolean p2, Lcom/android/calendar/event/ld;->l:Z

    .line 248
    sget-boolean v0, Lcom/android/calendar/event/ld;->l:Z

    if-eqz v0, :cond_0

    .line 249
    sput-boolean v2, Lcom/android/calendar/event/ld;->k:Z

    .line 251
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ld;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ld;I)I
    .locals 0

    .prologue
    .line 77
    iput p1, p0, Lcom/android/calendar/event/ld;->w:I

    return p1
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 1118
    iput-object p1, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    .line 1119
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1120
    :cond_0
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v1, "setCalendarsCursor : no calendar account found "

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    :goto_0
    return-void

    .line 1124
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ld;->c(Landroid/database/Cursor;)I

    move-result v0

    .line 1126
    iget v1, p0, Lcom/android/calendar/event/ld;->B:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/android/calendar/event/ld;->B:I

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1127
    iget v0, p0, Lcom/android/calendar/event/ld;->B:I

    iput v0, p0, Lcom/android/calendar/event/ld;->x:I

    .line 1132
    :goto_1
    new-instance v0, Lcom/android/calendar/event/ll;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/android/calendar/event/ll;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/ld;->b:Lcom/android/calendar/event/ll;

    .line 1133
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/calendar/event/ld;->b:Lcom/android/calendar/event/ll;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1134
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1135
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    .line 1129
    :cond_2
    iput v0, p0, Lcom/android/calendar/event/ld;->x:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/ld;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ld;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method private a(Landroid/content/Context;JLandroid/widget/EditText;JZ)Z
    .locals 8

    .prologue
    .line 694
    if-nez p1, :cond_0

    .line 695
    const/4 v0, 0x0

    .line 761
    :goto_0
    return v0

    .line 697
    :cond_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 699
    const-string v0, "title"

    invoke-virtual {p4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    new-instance v2, Landroid/text/format/Time;

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 704
    invoke-virtual {v2, p2, p3}, Landroid/text/format/Time;->set(J)V

    .line 707
    if-eqz p7, :cond_2

    .line 708
    const-string v1, "UTC"

    .line 709
    const/4 v0, 0x0

    iput v0, v2, Landroid/text/format/Time;->hour:I

    .line 710
    const/4 v0, 0x0

    iput v0, v2, Landroid/text/format/Time;->minute:I

    .line 711
    const/4 v0, 0x0

    iput v0, v2, Landroid/text/format/Time;->second:I

    .line 712
    iput-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 713
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 714
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v4

    .line 716
    const-string v0, "allDay"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 717
    const-string v7, "duration"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 727
    :goto_1
    const-string v1, "description"

    const-string v7, ""

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    const-string v1, "dtstart"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 729
    const-string v1, "dtend"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 730
    const-string v1, "hasAttendeeData"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 731
    const-string v1, "eventTimezone"

    invoke-virtual {v6, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    const-string v0, "calendar_id"

    iget-object v1, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 735
    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 738
    iget-object v1, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 739
    iget-object v2, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 740
    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 741
    const-string v3, "preference_defaultCalendar"

    invoke-static {p1, v3, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const-string v0, "preference_defaultCalendar_account_type"

    invoke-static {p1, v0, v1}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    const-string v0, "preference_defaultCalendar_display_name"

    invoke-static {p1, v0, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    :cond_1
    const/4 v1, 0x0

    .line 752
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 757
    :goto_2
    if-eqz v0, :cond_3

    .line 758
    const v0, 0x7f0f0112

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 759
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 720
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 721
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 722
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 723
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v4

    .line 724
    const-string v1, "allDay"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 753
    :catch_0
    move-exception v0

    .line 754
    sget-object v2, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_2

    .line 761
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;JLjava/lang/String;I)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 766
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 771
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 772
    :cond_0
    const-string p4, ""

    .line 774
    :cond_1
    const-string v0, "subject"

    invoke-virtual {v4, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    const-string v0, "body"

    const-string v1, ""

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    const-string v0, "body_size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 781
    const-string v0, "due_date"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 782
    const-string v0, "utc_due_date"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 786
    if-nez p5, :cond_4

    .line 788
    const-string v0, "accountKey"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 789
    const-string v0, "accountName"

    const-string v1, "My task"

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const-string v0, "preference_defaultTaskId"

    invoke-static {p1, v0, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 817
    :goto_0
    const-string v0, "importance"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 818
    const-string v0, "reminder_type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 819
    const-string v0, "reminder_set"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 820
    const-string v0, "reminder_time"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 821
    const-string v0, "_sync_dirty"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 822
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 825
    if-eqz v1, :cond_2

    .line 826
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 828
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.android.calendar"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_2

    .line 836
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 839
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.android.calendar"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_4

    .line 848
    :cond_2
    :goto_2
    if-eqz v1, :cond_5

    .line 849
    const v0, 0x7f0f01d9

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 850
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 851
    const-string v0, "GATE"

    const-string v1, "<GATE-M>TASK_CREATED</GATE-M>"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v0, v3

    .line 855
    :goto_3
    return v0

    .line 794
    :cond_4
    const-string v1, ""

    .line 798
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/event/ld;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v5, v0, Lcom/android/calendar/task/ab;->c:I
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    .line 804
    const-string v0, "accountKey"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 805
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 807
    iget-object v6, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 808
    iget-object v0, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    const-string v1, "_sync_account"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 809
    iget-object v1, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 810
    const-string v1, "preference_defaultTaskId"

    invoke-static {p1, v1, v5}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 814
    :goto_4
    const-string v1, "accountName"

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 799
    :catch_0
    move-exception v0

    .line 800
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v1, "IndexOutOfBoundsException...Failed to saveTask"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 801
    goto :goto_3

    .line 829
    :catch_1
    move-exception v0

    .line 830
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v4, "RemoteException"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 831
    :catch_2
    move-exception v0

    .line 832
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    .line 833
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v4, "OperationApplicationException"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 840
    :catch_3
    move-exception v0

    .line 841
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v4, "RemoteException"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 842
    :catch_4
    move-exception v0

    .line 843
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    .line 844
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v4, "OperationApplicationException"

    invoke-static {v0, v4}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_5
    move v0, v2

    .line 855
    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_4
.end method

.method static synthetic b(Lcom/android/calendar/event/ld;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    return-object v0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 1141
    iput-object p1, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    .line 1142
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1143
    :cond_0
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v1, "setCalendarsCursor : no calendar account found "

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    :goto_0
    return-void

    .line 1147
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ld;->d(Landroid/database/Cursor;)I

    move-result v0

    .line 1148
    iget v1, p0, Lcom/android/calendar/event/ld;->B:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1149
    iget v0, p0, Lcom/android/calendar/event/ld;->B:I

    iput v0, p0, Lcom/android/calendar/event/ld;->z:I

    .line 1154
    :goto_1
    new-instance v0, Lcom/android/calendar/event/lm;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/android/calendar/event/lm;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/android/calendar/event/ld;->c:Lcom/android/calendar/event/lm;

    .line 1155
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/android/calendar/event/ld;->c:Lcom/android/calendar/event/lm;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->z:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1157
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    .line 1151
    :cond_2
    iput v0, p0, Lcom/android/calendar/event/ld;->z:I

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/event/ld;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ld;->b(Landroid/database/Cursor;)V

    return-void
.end method

.method private c(Landroid/database/Cursor;)I
    .locals 7

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 1161
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    move v1, v0

    .line 1198
    :cond_0
    :goto_0
    return v1

    .line 1165
    :cond_1
    sget-object v2, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v3, "preference_defaultCalendar"

    const-string v4, "local"

    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1167
    sget-object v3, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v4, "preference_defaultCalendar_account_type"

    const-string v5, "local"

    invoke-static {v3, v4, v5}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1169
    sget-object v4, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v5, "preference_defaultCalendar_display_name"

    const-string v6, "My calendar"

    invoke-static {v4, v5, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1173
    sget-object v5, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 1174
    sget-object v2, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    .line 1177
    :cond_2
    if-eqz v2, :cond_0

    .line 1181
    const-string v5, "ownerAccount"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 1183
    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 1184
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1185
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x4

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1188
    const-string v6, "local"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1191
    add-int/lit8 v0, v0, 0x1

    .line 1192
    goto :goto_1

    :cond_3
    move v1, v0

    .line 1194
    goto :goto_0

    .line 1196
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic c(Lcom/android/calendar/event/ld;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/android/calendar/event/ld;->x:I

    return v0
.end method

.method static synthetic c()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    return-object v0
.end method

.method private d(Landroid/database/Cursor;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1202
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1203
    const/4 v0, -0x1

    .line 1212
    :cond_0
    :goto_0
    return v0

    .line 1206
    :cond_1
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v2, "preference_defaultTaskId"

    invoke-static {v0, v2, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 1209
    if-gtz v0, :cond_0

    move v0, v1

    .line 1212
    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/event/ld;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/android/calendar/event/ld;->z:I

    return v0
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 77
    sget-boolean v0, Lcom/android/calendar/event/ld;->k:Z

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/event/ld;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 643
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 644
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 645
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 650
    :goto_0
    return-void

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 648
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/event/ld;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 653
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 654
    iget-object v0, p0, Lcom/android/calendar/event/ld;->r:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f03e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 660
    :goto_0
    return-void

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ld;->r:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f03e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private g()V
    .locals 8

    .prologue
    .line 663
    iget-object v0, p0, Lcom/android/calendar/event/ld;->i:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v2

    .line 665
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 666
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    iget v0, p0, Lcom/android/calendar/event/ld;->x:I

    int-to-long v5, v0

    sget-boolean v7, Lcom/android/calendar/event/ld;->k:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/calendar/event/ld;->a(Landroid/content/Context;JLandroid/widget/EditText;JZ)Z

    .line 673
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->h()V

    .line 674
    return-void

    .line 669
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/android/calendar/event/ld;->z:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/event/ld;->a(Landroid/content/Context;JLjava/lang/String;I)Z

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/event/ld;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->h()V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 689
    :goto_0
    return-void

    .line 681
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 683
    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 685
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 686
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic h(Lcom/android/calendar/event/ld;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->g()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 515
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 516
    const-string v0, "EEEE dd/MM/yyyy"

    .line 518
    if-eqz v1, :cond_0

    .line 519
    invoke-static {}, Lcom/android/calendar/hj;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 520
    const-string v0, "MDY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 541
    :cond_0
    :goto_0
    return-object v0

    .line 522
    :cond_1
    const-string v0, "YMD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 523
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 525
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 528
    :cond_3
    const-string v2, "MDY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 529
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 530
    :cond_4
    const-string v2, "YMD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 531
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 532
    invoke-static {}, Lcom/android/calendar/hj;->j()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 533
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0263

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 534
    :cond_5
    invoke-static {}, Lcom/android/calendar/hj;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0276

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public a(I)V
    .locals 7

    .prologue
    const v6, 0x7f12001e

    const/16 v2, 0x8

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 545
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 549
    :cond_1
    if-eq p1, v4, :cond_2

    if-ne p1, v5, :cond_3

    .line 550
    :cond_2
    iput p1, p0, Lcom/android/calendar/event/ld;->w:I

    .line 553
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    .line 554
    sget-boolean v1, Lcom/android/calendar/event/ld;->k:Z

    if-eqz v1, :cond_a

    if-nez v0, :cond_a

    .line 555
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 556
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 558
    iget-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    if-nez v0, :cond_4

    .line 561
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f01b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 567
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 568
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 571
    const/16 v2, 0x10

    if-lt v0, v2, :cond_6

    .line 572
    add-int/lit8 v0, v1, -0x2

    .line 577
    :goto_1
    if-eq v1, v0, :cond_4

    .line 578
    iget-object v1, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    int-to-float v2, v0

    invoke-virtual {v1, v4, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 579
    iget-object v1, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/Button;->setTextSize(IF)V

    .line 583
    :cond_4
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    if-ne v0, v4, :cond_8

    .line 585
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    const v1, 0x7f0f01a5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 586
    iget-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    if-eqz v0, :cond_7

    .line 587
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    const v1, 0x7f020185

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 588
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    const v1, 0x7f020184

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 589
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 591
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 593
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 594
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 634
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 636
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->e()V

    .line 637
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->f()V

    .line 639
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ld;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 562
    :catch_0
    move-exception v0

    .line 563
    sget-object v0, Lcom/android/calendar/event/ld;->a:Ljava/lang/String;

    const-string v1, "IllegalStateException occured, Simple AddFragment already detached"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 573
    :cond_6
    const/16 v2, 0xc

    if-lt v0, v2, :cond_c

    .line 574
    add-int/lit8 v0, v1, -0x1

    goto/16 :goto_1

    .line 596
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    const v1, 0x7f020183

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 597
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    const v1, 0x7f020186

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 598
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setSelected(Z)V

    .line 599
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto :goto_2

    .line 601
    :cond_8
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    if-ne v0, v5, :cond_5

    .line 603
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    const v1, 0x7f0f01a5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 604
    iget-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    if-eqz v0, :cond_9

    .line 605
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    const v1, 0x7f020184

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 606
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    const v1, 0x7f020185

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 607
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 609
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 611
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 612
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    goto/16 :goto_2

    .line 614
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    const v1, 0x7f020186

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 615
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    const v1, 0x7f020183

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 616
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setSelected(Z)V

    .line 617
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setSelected(Z)V

    goto/16 :goto_2

    .line 620
    :cond_a
    sget-boolean v0, Lcom/android/calendar/event/ld;->l:Z

    if-eqz v0, :cond_b

    .line 621
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 623
    iput v5, p0, Lcom/android/calendar/event/ld;->w:I

    .line 624
    iget-object v0, p0, Lcom/android/calendar/event/ld;->v:Landroid/widget/TextView;

    const v1, 0x7f0f01b5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 627
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 628
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 629
    iput v4, p0, Lcom/android/calendar/event/ld;->w:I

    .line 630
    iget-object v0, p0, Lcom/android/calendar/event/ld;->v:Landroid/widget/TextView;

    const v1, 0x7f0f01b3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(JZ)V
    .locals 7

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 490
    invoke-static {v0}, Lcom/android/calendar/hj;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 491
    if-eqz p3, :cond_1

    .line 492
    iget-object v2, p0, Lcom/android/calendar/event/ld;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->a()Ljava/lang/String;

    move-result-object v3

    sub-long v0, p1, v0

    invoke-static {v3, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/android/calendar/event/ld;->t:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 511
    :goto_0
    return-void

    .line 499
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/ld;->t:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->a()Ljava/lang/String;

    move-result-object v3

    sub-long v0, p1, v0

    invoke-static {v3, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 503
    const/16 v6, 0x81

    .line 508
    :goto_1
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-wide v2, p1

    move-wide v4, p1

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    .line 509
    iget-object v1, p0, Lcom/android/calendar/event/ld;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 505
    :cond_2
    const/16 v6, 0x141

    goto :goto_1
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1243
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1245
    new-instance v1, Lcom/android/calendar/event/lj;

    invoke-direct {v1, p0, p1}, Lcom/android/calendar/event/lj;-><init>(Lcom/android/calendar/event/ld;Landroid/view/View;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1257
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 1266
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    .line 1267
    if-eqz p2, :cond_1

    .line 1268
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1279
    :cond_0
    :goto_0
    return-void

    .line 1270
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->y:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 1272
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 1273
    if-eqz p2, :cond_3

    .line 1274
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->z:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    .line 1276
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    iget v1, p0, Lcom/android/calendar/event/ld;->A:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 860
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v3, "JAPAN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 861
    iget-object v0, p0, Lcom/android/calendar/event/ld;->j:Landroid/content/AsyncQueryHandler;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/ld;->d:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    :goto_0
    sget-boolean v0, Lcom/android/calendar/event/ld;->k:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/calendar/event/ld;->l:Z

    if-eqz v0, :cond_1

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ld;->j:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x2

    sget-object v3, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    :cond_1
    return-void

    .line 866
    :cond_2
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->C(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 867
    iget-object v0, p0, Lcom/android/calendar/event/ld;->j:Landroid/content/AsyncQueryHandler;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/ld;->d:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND calendar_displayName!=\'Direct Share\' AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 873
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ld;->j:Landroid/content/AsyncQueryHandler;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/ld;->d:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1284
    packed-switch p1, :pswitch_data_0

    .line 1315
    :cond_0
    :goto_0
    return-void

    .line 1287
    :pswitch_0
    if-eqz p3, :cond_0

    .line 1289
    const-string v0, "request_module"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1291
    if-eq p2, v3, :cond_1

    .line 1292
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 1294
    :pswitch_1
    invoke-virtual {p0, v2, v1}, Lcom/android/calendar/event/ld;->a(ZZ)V

    goto :goto_0

    .line 1298
    :pswitch_2
    invoke-virtual {p0, v1, v1}, Lcom/android/calendar/event/ld;->a(ZZ)V

    goto :goto_0

    .line 1303
    :cond_1
    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 1305
    :pswitch_3
    invoke-virtual {p0, v2, v2}, Lcom/android/calendar/event/ld;->a(ZZ)V

    goto :goto_0

    .line 1309
    :pswitch_4
    invoke-virtual {p0, v1, v2}, Lcom/android/calendar/event/ld;->a(ZZ)V

    goto :goto_0

    .line 1284
    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
    .end packed-switch

    .line 1292
    :pswitch_data_1
    .packed-switch 0x1f7
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1303
    :pswitch_data_2
    .packed-switch 0x1f7
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 255
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 256
    sput-object p1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    .line 257
    invoke-static {p1}, Lcom/android/calendar/al;->a(Landroid/content/Context;)Lcom/android/calendar/al;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ld;->i:Lcom/android/calendar/al;

    .line 258
    new-instance v0, Lcom/android/calendar/event/lk;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/lk;-><init>(Lcom/android/calendar/event/ld;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ld;->j:Landroid/content/AsyncQueryHandler;

    .line 260
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    .line 261
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 453
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 454
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 265
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 267
    const/4 v1, 0x2

    .line 268
    const v0, 0x7f10005e

    .line 269
    iget-boolean v2, p0, Lcom/android/calendar/event/ld;->C:Z

    if-eqz v2, :cond_0

    .line 270
    const/4 v1, 0x1

    .line 271
    const/4 v0, 0x0

    .line 273
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/ld;->setStyle(II)V

    .line 275
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 279
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 448
    :goto_0
    return-object v0

    .line 283
    :cond_0
    if-eqz p3, :cond_7

    .line 284
    const-string v0, "mIsAlldayEvent"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    const-string v0, "mIsAlldayEvent"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/event/ld;->k:Z

    .line 287
    :cond_1
    const-string v0, "mIsTaskEvent"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 288
    const-string v0, "mIsTaskEvent"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/calendar/event/ld;->l:Z

    .line 290
    :cond_2
    const-string v0, "mode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    const-string v0, "mode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ld;->w:I

    .line 293
    :cond_3
    const-string v0, "date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 294
    const-string v0, "date"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    .line 297
    :cond_4
    const-string v0, "checked"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 298
    const-string v0, "checked"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ld;->D:Z

    .line 302
    :cond_5
    const-string v0, "keypress"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 303
    const-string v0, "keypress"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ld;->E:Z

    .line 305
    :cond_6
    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 306
    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ld;->B:I

    .line 309
    :cond_7
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    .line 312
    iget-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_b

    .line 313
    iget-object v0, p0, Lcom/android/calendar/event/ld;->i:Lcom/android/calendar/al;

    invoke-virtual {v0}, Lcom/android/calendar/al;->b()J

    move-result-wide v0

    .line 314
    iput-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    .line 318
    :goto_1
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 320
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 322
    iget-boolean v0, p0, Lcom/android/calendar/event/ld;->C:Z

    if-eqz v0, :cond_c

    .line 323
    const v0, 0x7f040096

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    .line 328
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    .line 329
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    .line 330
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->p:Landroid/widget/Button;

    .line 331
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->q:Landroid/widget/Button;

    .line 332
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->H:Landroid/widget/Spinner;

    .line 333
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->I:Landroid/widget/Spinner;

    .line 335
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->r:Landroid/widget/Button;

    .line 336
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    .line 338
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->v:Landroid/widget/TextView;

    .line 339
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->u:Landroid/widget/TextView;

    .line 340
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    const v1, 0x7f1202ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ld;->t:Landroid/widget/TextView;

    .line 342
    sget-boolean v0, Lcom/android/calendar/event/ld;->k:Z

    sget-boolean v1, Lcom/android/calendar/event/ld;->l:Z

    or-int/2addr v0, v1

    invoke-virtual {p0, v6, v7, v0}, Lcom/android/calendar/event/ld;->a(JZ)V

    .line 343
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->b()V

    .line 345
    iget-object v0, p0, Lcom/android/calendar/event/ld;->n:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/le;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/le;-><init>(Lcom/android/calendar/event/ld;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v0, p0, Lcom/android/calendar/event/ld;->o:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/lf;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/lf;-><init>(Lcom/android/calendar/event/ld;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    iget-object v0, p0, Lcom/android/calendar/event/ld;->r:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/lg;

    invoke-direct {v1, p0, v4}, Lcom/android/calendar/event/lg;-><init>(Lcom/android/calendar/event/ld;Landroid/text/format/Time;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v0, p0, Lcom/android/calendar/event/ld;->p:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/lh;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/lh;-><init>(Lcom/android/calendar/event/ld;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 416
    iget-object v0, p0, Lcom/android/calendar/event/ld;->q:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/li;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/li;-><init>(Lcom/android/calendar/event/ld;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    if-eqz v0, :cond_a

    .line 424
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setMaxWidth(I)V

    .line 428
    new-instance v0, Lcom/android/calendar/en;

    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 429
    if-eqz v0, :cond_8

    .line 430
    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 437
    :cond_8
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 438
    :cond_9
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x168

    if-lt v0, v1, :cond_a

    .line 439
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getImeOptions()I

    move-result v0

    .line 440
    const v1, -0x2000001

    and-int/2addr v0, v1

    .line 441
    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 446
    :cond_a
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ld;->a(I)V

    .line 448
    iget-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    goto/16 :goto_0

    .line 316
    :cond_b
    iget-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    goto/16 :goto_1

    .line 325
    :cond_c
    const v0, 0x7f040095

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ld;->m:Landroid/view/View;

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 485
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 486
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/high16 v4, 0x10000000

    const/16 v3, 0x1f4

    const/4 v2, 0x0

    .line 1050
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1102
    :cond_0
    :goto_0
    return-void

    .line 1054
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 1056
    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    .line 1057
    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1058
    iput p3, p0, Lcom/android/calendar/event/ld;->x:I

    .line 1062
    iget-object v0, p0, Lcom/android/calendar/event/ld;->F:Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1064
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v1, "preferences_samsung_account_validation"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1068
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1069
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1070
    const-string v1, "request_module"

    const/16 v2, 0x1f7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1071
    invoke-virtual {p0, v0, v3}, Lcom/android/calendar/event/ld;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1076
    :cond_2
    iget v0, p0, Lcom/android/calendar/event/ld;->x:I

    iput v0, p0, Lcom/android/calendar/event/ld;->y:I

    goto :goto_0

    .line 1077
    :cond_3
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    if-eqz v0, :cond_4

    .line 1080
    iget-object v0, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1081
    iput p3, p0, Lcom/android/calendar/event/ld;->z:I

    .line 1085
    iget-object v0, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    const-string v1, "_sync_account_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 1087
    iget-object v1, p0, Lcom/android/calendar/event/ld;->G:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1088
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v1, "preferences_samsung_account_validation"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1092
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1093
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1094
    const-string v1, "request_module"

    const/16 v2, 0x1f8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1095
    invoke-virtual {p0, v0, v3}, Lcom/android/calendar/event/ld;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1100
    :cond_4
    iget v0, p0, Lcom/android/calendar/event/ld;->z:I

    iput v0, p0, Lcom/android/calendar/event/ld;->A:I

    goto/16 :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 1106
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 1233
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1235
    sget-object v1, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1236
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1237
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1239
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 1240
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1219
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 1220
    sget-object v0, Lcom/android/calendar/event/ld;->h:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1222
    iget-object v1, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/android/calendar/event/ld;->s:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ld;->a(Landroid/view/View;)V

    .line 1225
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/ld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1226
    invoke-direct {p0}, Lcom/android/calendar/event/ld;->h()V

    .line 1228
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 459
    const-string v0, "mIsAlldayEvent"

    sget-boolean v1, Lcom/android/calendar/event/ld;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 460
    const-string v0, "mIsTaskEvent"

    sget-boolean v1, Lcom/android/calendar/event/ld;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 461
    const-string v0, "mode"

    iget v1, p0, Lcom/android/calendar/event/ld;->w:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 462
    iget-wide v0, p0, Lcom/android/calendar/event/ld;->J:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 463
    const-string v0, "date"

    iget-wide v2, p0, Lcom/android/calendar/event/ld;->J:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 466
    :cond_0
    const-string v0, "checked"

    iget-boolean v1, p0, Lcom/android/calendar/event/ld;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 467
    const-string v0, "keypress"

    iget-boolean v1, p0, Lcom/android/calendar/event/ld;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 469
    iget v0, p0, Lcom/android/calendar/event/ld;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 470
    const-string v0, "account"

    iget v1, p0, Lcom/android/calendar/event/ld;->x:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 474
    :goto_0
    return-void

    .line 472
    :cond_1
    const-string v0, "account"

    iget v1, p0, Lcom/android/calendar/event/ld;->z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
