.class Lcom/android/calendar/event/as;
.super Ljava/lang/Object;
.source "EditEventFragment.java"

# interfaces
.implements Lcom/android/calendar/event/ax;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ak;

.field private b:I


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ak;)V
    .locals 1

    .prologue
    .line 980
    iput-object p1, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 981
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/as;->b:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 985
    iput p1, p0, Lcom/android/calendar/event/as;->b:I

    .line 986
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    const v11, 0x7f0f0112

    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 992
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;Z)Z

    .line 994
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->k(Lcom/android/calendar/event/ak;)I

    move-result v0

    if-nez v0, :cond_0

    .line 997
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0, v8}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    .line 1000
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v0, :cond_3

    .line 1001
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v6, 0x200000

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/event/at;->d:J

    const-wide/32 v6, 0x200000

    cmp-long v0, v4, v6

    if-nez v0, :cond_11

    :cond_2
    move v0, v9

    :goto_0
    iput-boolean v0, v3, Lcom/android/calendar/as;->aq:Z

    .line 1007
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-nez v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v6, v0, Lcom/android/calendar/as;->c:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1c

    :cond_4
    move v0, v9

    .line 1024
    :goto_1
    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v3

    if-eqz v3, :cond_1b

    iget v3, p0, Lcom/android/calendar/event/as;->b:I

    if-eq v3, v9, :cond_1b

    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v3, v3, Lcom/android/calendar/as;->aq:Z

    if-nez v3, :cond_1b

    if-eqz v0, :cond_1b

    .line 1027
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 1029
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 1030
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->l(Lcom/android/calendar/event/ak;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1032
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput-object v2, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 1033
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_5

    .line 1034
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v9}, Lcom/android/calendar/event/ay;->d(Z)V

    :cond_5
    move v10, v9

    .line 1043
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v3, "com.android.exchange"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->k(Lcom/android/calendar/event/ak;)I

    move-result v0

    if-ne v0, v8, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1047
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->l(Lcom/android/calendar/event/ak;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "original_sync_id =? "

    new-array v5, v9, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v6, v6, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v6, v6, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1054
    :cond_7
    iget v0, p0, Lcom/android/calendar/event/as;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-virtual {v0}, Lcom/android/calendar/as;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->m(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/av;

    move-result-object v3

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v5

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->k(Lcom/android/calendar/event/ak;)I

    move-result v6

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v7

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v8, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual/range {v3 .. v8}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1061
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->t()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1062
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    const v3, 0x7f0f01a3

    invoke-static {v0, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1067
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_17

    .line 1068
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1069
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1070
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 1071
    const v0, 0x7f0f039b

    .line 1096
    :goto_3
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1097
    const-string v3, "GATE"

    const-string v4, "<GATE-M>EVENT_CREATED</GATE-M>"

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    :cond_a
    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v3, :cond_1a

    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v3}, Lcom/android/calendar/event/ay;->v()Z

    move-result v3

    if-eqz v3, :cond_b

    if-eqz v10, :cond_1a

    .line 1101
    :cond_b
    iget-object v3, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1108
    :cond_c
    :goto_4
    iget v0, p0, Lcom/android/calendar/event/as;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_f

    .line 1111
    iget v0, p0, Lcom/android/calendar/event/as;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d

    .line 1112
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-eqz v0, :cond_d

    .line 1113
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->y:J

    .line 1114
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v6, v0, Lcom/android/calendar/as;->B:J

    .line 1115
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_d

    .line 1118
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 1119
    new-instance v2, Landroid/text/format/Time;

    const-string v3, "UTC"

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1120
    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1121
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1122
    invoke-virtual {v2, v9}, Landroid/text/format/Time;->toMillis(Z)J

    .line 1124
    const-string v3, "UTC"

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1125
    invoke-virtual {v2, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1126
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1127
    invoke-virtual {v2, v9}, Landroid/text/format/Time;->toMillis(Z)J

    .line 1131
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 1132
    if-eqz v2, :cond_f

    .line 1133
    instance-of v0, v2, Lcom/android/calendar/event/EditEventActivity;

    if-eqz v0, :cond_e

    move-object v0, v2

    .line 1134
    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0, v9}, Lcom/android/calendar/event/EditEventActivity;->a(Z)V

    .line 1136
    :cond_e
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 1142
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1143
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1145
    iget-object v2, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    invoke-static {v2}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 1146
    if-eqz v2, :cond_10

    .line 1147
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1148
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 1151
    :cond_10
    return-void

    :cond_11
    move v0, v1

    .line 1001
    goto/16 :goto_0

    :cond_12
    move v0, v11

    .line 1073
    goto/16 :goto_3

    .line 1076
    :cond_13
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_14

    .line 1077
    const v0, 0x7f0f039c

    goto/16 :goto_3

    .line 1079
    :cond_14
    const v0, 0x7f0f0113

    goto/16 :goto_3

    .line 1083
    :cond_15
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 1084
    const v0, 0x7f0f039c

    goto/16 :goto_3

    .line 1086
    :cond_16
    const v0, 0x7f0f0113

    goto/16 :goto_3

    .line 1090
    :cond_17
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-nez v0, :cond_18

    if-eqz v10, :cond_19

    .line 1091
    :cond_18
    const v0, 0x7f0f039b

    goto/16 :goto_3

    :cond_19
    move v0, v11

    .line 1093
    goto/16 :goto_3

    .line 1103
    :cond_1a
    iget-object v0, p0, Lcom/android/calendar/event/as;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->d(Z)V

    goto/16 :goto_4

    :cond_1b
    move v10, v1

    goto/16 :goto_2

    :cond_1c
    move v0, v1

    goto/16 :goto_1
.end method
