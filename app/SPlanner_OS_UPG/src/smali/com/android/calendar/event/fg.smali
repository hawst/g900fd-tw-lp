.class Lcom/android/calendar/event/fg;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 704
    iput-object p1, p0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 705
    iput-object p2, p0, Lcom/android/calendar/event/fg;->a:Landroid/view/View;

    .line 706
    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 18

    .prologue
    .line 714
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    .line 715
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v12

    .line 717
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 718
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v9, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 723
    const-wide/16 v4, 0x0

    .line 724
    const-wide/16 v6, 0x0

    .line 728
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    iget-object v8, v8, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 775
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    iget-object v8, v8, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v8}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;)Landroid/widget/Button;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 793
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 794
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v5

    iget v5, v5, Landroid/text/format/Time;->hour:I

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p2

    invoke-static/range {v2 .. v8}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    .line 797
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3, v9, v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/text/format/Time;Landroid/text/format/Time;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 798
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3, v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/text/format/Time;)Landroid/text/format/Time;

    .line 799
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;I)I

    .line 800
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->g(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/event/ay;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 802
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;)Landroid/widget/Button;

    move-result-object v5

    invoke-static {v4, v5, v2, v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iget v3, v12, Landroid/text/format/Time;->second:I

    iget v4, v2, Landroid/text/format/Time;->second:I

    sub-int v13, v3, v4

    .line 731
    iget v3, v12, Landroid/text/format/Time;->minute:I

    iget v4, v2, Landroid/text/format/Time;->minute:I

    sub-int v14, v3, v4

    .line 732
    iget v3, v12, Landroid/text/format/Time;->hour:I

    iget v4, v2, Landroid/text/format/Time;->hour:I

    sub-int v15, v3, v4

    .line 733
    iget v3, v12, Landroid/text/format/Time;->monthDay:I

    iget v4, v2, Landroid/text/format/Time;->monthDay:I

    sub-int v16, v3, v4

    .line 734
    iget v3, v12, Landroid/text/format/Time;->month:I

    iget v4, v2, Landroid/text/format/Time;->month:I

    sub-int v17, v3, v4

    .line 735
    iget v3, v12, Landroid/text/format/Time;->year:I

    iget v4, v2, Landroid/text/format/Time;->year:I

    sub-int v9, v3, v4

    .line 737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v5

    iget v5, v5, Landroid/text/format/Time;->hour:I

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p2

    invoke-static/range {v2 .. v8}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v10

    .line 741
    invoke-static {v2, v12}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-lez v3, :cond_7

    .line 742
    if-lez v15, :cond_6

    .line 743
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    add-int v6, v3, v15

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    .line 751
    :goto_1
    invoke-static {v12}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 752
    const v3, 0x259d23

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 765
    :cond_2
    :goto_2
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 766
    iget v3, v12, Landroid/text/format/Time;->hour:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v12, Landroid/text/format/Time;->hour:I

    .line 769
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    cmp-long v3, v10, v4

    if-lez v3, :cond_4

    .line 771
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 774
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;)V

    move-wide v4, v10

    .line 806
    :cond_5
    :goto_3
    cmp-long v3, v4, v6

    if-lez v3, :cond_9

    .line 807
    invoke-virtual {v12, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-wide v2, v4

    .line 810
    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    iget-object v7, v7, Lcom/android/calendar/event/ay;->j:Landroid/widget/LinearLayout;

    invoke-static {v6, v7, v4, v5}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V

    .line 811
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    iget-object v5, v5, Lcom/android/calendar/event/ay;->k:Landroid/widget/LinearLayout;

    invoke-static {v4, v5, v2, v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Landroid/view/View;J)V

    .line 812
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v2}, Lcom/android/calendar/event/ay;->s()V

    goto/16 :goto_0

    .line 747
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v6, v3, Landroid/text/format/Time;->hour:I

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    goto/16 :goto_1

    .line 755
    :cond_7
    add-int v9, v9, p2

    .line 756
    add-int v8, p3, v17

    .line 757
    add-int v7, p4, v16

    .line 758
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    add-int v6, v3, v15

    move-object v3, v12

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    goto/16 :goto_2

    .line 777
    :cond_8
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    .line 781
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v4, v3, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v5, v3, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    iget v6, v3, Landroid/text/format/Time;->hour:I

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-static/range {v3 .. v9}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;IIIIII)J

    move-result-wide v6

    .line 785
    invoke-static {v2, v12}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-ltz v3, :cond_a

    .line 786
    invoke-virtual {v12, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 787
    iget v3, v2, Landroid/text/format/Time;->hour:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v12, Landroid/text/format/Time;->hour:I

    .line 788
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 789
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/fg;->b:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->e(Lcom/android/calendar/event/ay;)V

    move-wide v4, v10

    goto/16 :goto_3

    :cond_9
    move-wide v2, v6

    goto/16 :goto_4

    :cond_a
    move-wide v4, v10

    goto/16 :goto_3
.end method
