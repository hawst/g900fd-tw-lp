.class Lcom/android/calendar/event/kg;
.super Ljava/lang/Object;
.source "SelectMapChinaActivity.java"

# interfaces
.implements Lcom/amap/api/services/geocoder/GeocodeSearch$OnGeocodeSearchListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapChinaActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 774
    iput-object p1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0, p1}, Lcom/android/calendar/event/kg;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    return-void
.end method


# virtual methods
.method public onGeocodeSearched(Lcom/amap/api/services/geocoder/GeocodeResult;I)V
    .locals 0

    .prologue
    .line 777
    return-void
.end method

.method public onRegeocodeSearched(Lcom/amap/api/services/geocoder/RegeocodeResult;I)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 781
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onRegeocodeSearched:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    if-nez p2, :cond_0

    if-nez p1, :cond_2

    .line 791
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-virtual {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f0290

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->l(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 793
    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 849
    :cond_1
    :goto_0
    return-void

    .line 797
    :cond_2
    if-eqz p1, :cond_1

    .line 798
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->m(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 799
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z

    .line 800
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getCity()Ljava/lang/String;

    move-result-object v0

    .line 801
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 804
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getProvince()Ljava/lang/String;

    move-result-object v0

    .line 806
    :cond_3
    iget-object v3, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->n(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 807
    iget-object v3, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 809
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->g:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 811
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->o(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->p(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 812
    :cond_6
    const-string v0, "SelectMapChinaActivityAMAP"

    const-string v3, "myMKSearchListener onGetAddrResult - isLocation || isLongPress"

    invoke-static {v0, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->o(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_c

    move v7, v1

    .line 819
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z

    .line 820
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->d(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z

    .line 822
    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v8

    .line 823
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 825
    :cond_7
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {v1, v0}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 827
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->q(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iget-wide v2, v0, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->q(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v0

    iget-wide v4, v0, Lcom/amap/api/maps/model/LatLng;->longitude:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 828
    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 829
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 830
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 835
    :goto_2
    invoke-static {}, Lcom/android/calendar/event/SelectMapChinaActivity;->c()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 836
    const-string v1, "SelectMapChinaActivityAMAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get location from NLP ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    :cond_8
    :goto_3
    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)V

    .line 840
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->q(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v7}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 832
    :catch_0
    move-exception v0

    .line 833
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_9
    move-object v0, v8

    goto :goto_2

    .line 842
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->r(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 843
    const-string v0, "SelectMapChinaActivityAMAP"

    const-string v1, "myMKSearchListener onGetAddrResult - isLaunched"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->e(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z

    .line 845
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->s(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)V

    .line 846
    iget-object v0, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v1, p0, Lcom/android/calendar/event/kg;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->q(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/services/geocoder/RegeocodeResult;->getRegeocodeAddress()Lcom/amap/api/services/geocoder/RegeocodeAddress;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/services/geocoder/RegeocodeAddress;->getFormatAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_b
    move-object v0, v8

    goto :goto_3

    :cond_c
    move v7, v2

    goto/16 :goto_1
.end method
