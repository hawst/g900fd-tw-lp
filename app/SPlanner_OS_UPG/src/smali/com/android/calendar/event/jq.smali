.class Lcom/android/calendar/event/jq;
.super Landroid/os/Handler;
.source "SelectMapChinaActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapChinaActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/high16 v6, 0x43480000    # 200.0f

    .line 216
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 245
    :goto_0
    return-void

    .line 221
    :pswitch_0
    new-instance v0, Lcom/amap/api/services/geocoder/RegeocodeQuery;

    new-instance v1, Lcom/amap/api/services/core/LatLonPoint;

    iget-object v2, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v2, v2, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v4, v4, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/services/core/LatLonPoint;-><init>(DD)V

    sget-object v2, Lcom/amap/api/services/geocoder/GeocodeSearch;->AMAP:Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2}, Lcom/amap/api/services/geocoder/RegeocodeQuery;-><init>(Lcom/amap/api/services/core/LatLonPoint;FLjava/lang/String;)V

    .line 222
    iget-object v1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    iget-object v3, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v3, v3, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    iget-object v3, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v3, v3, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v1, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    .line 223
    iget-object v1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/services/geocoder/GeocodeSearch;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->getFromLocationAsyn(Lcom/amap/api/services/geocoder/RegeocodeQuery;)V

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v0, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/SearchView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :pswitch_2
    invoke-static {}, Lcom/android/calendar/event/SelectMapChinaActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SEARCH_SELECTED_LOCATION mLatitude "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SEARCH_SELECTED_LOCATION mLongitude "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->d(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    new-instance v0, Lcom/amap/api/services/geocoder/RegeocodeQuery;

    new-instance v1, Lcom/amap/api/services/core/LatLonPoint;

    iget-object v2, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v2

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(I)D

    move-result-wide v2

    iget-object v4, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v4}, Lcom/android/calendar/event/SelectMapChinaActivity;->d(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v4

    invoke-static {v4}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(I)D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/services/core/LatLonPoint;-><init>(DD)V

    sget-object v2, Lcom/amap/api/services/geocoder/GeocodeSearch;->AMAP:Ljava/lang/String;

    invoke-direct {v0, v1, v6, v2}, Lcom/amap/api/services/geocoder/RegeocodeQuery;-><init>(Lcom/amap/api/services/core/LatLonPoint;FLjava/lang/String;)V

    .line 237
    iget-object v1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    new-instance v2, Lcom/amap/api/maps/model/LatLng;

    iget-object v3, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v3

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(I)D

    move-result-wide v4

    iget-object v3, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->d(Lcom/android/calendar/event/SelectMapChinaActivity;)I

    move-result v3

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(I)D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/amap/api/maps/model/LatLng;-><init>(DD)V

    invoke-static {v1, v2}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;

    .line 239
    iget-object v1, p0, Lcom/android/calendar/event/jq;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/services/geocoder/GeocodeSearch;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->getFromLocationAsyn(Lcom/amap/api/services/geocoder/RegeocodeQuery;)V

    goto/16 :goto_0

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
