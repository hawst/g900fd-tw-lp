.class Lcom/android/calendar/event/af;
.super Ljava/lang/Object;
.source "EditEventActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/EditEventActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/EditEventActivity;)V
    .locals 0

    .prologue
    .line 767
    iput-object p1, p0, Lcom/android/calendar/event/af;->a:Lcom/android/calendar/event/EditEventActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x3f666666    # 0.9f

    .line 770
    iget-object v0, p0, Lcom/android/calendar/event/af;->a:Lcom/android/calendar/event/EditEventActivity;

    const v1, 0x7f1200d5

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 771
    iget-object v1, p0, Lcom/android/calendar/event/af;->a:Lcom/android/calendar/event/EditEventActivity;

    const v2, 0x7f1200d4

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 773
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 774
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v2

    .line 775
    mul-float v3, v2, v4

    invoke-virtual {v0, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 776
    mul-float v0, v2, v4

    invoke-virtual {v1, v5, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 778
    :cond_0
    return-void
.end method
