.class Lcom/android/calendar/event/is;
.super Ljava/lang/Object;
.source "ParticipantScheduleView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:I

.field b:I

.field c:F

.field d:J

.field final synthetic e:Lcom/android/calendar/event/ip;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/ip;)V
    .locals 0

    .prologue
    .line 986
    iput-object p1, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/ip;Lcom/android/calendar/event/iq;)V
    .locals 0

    .prologue
    .line 986
    invoke-direct {p0, p1}, Lcom/android/calendar/event/is;-><init>(Lcom/android/calendar/event/ip;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/16 v1, 0x3c

    .line 997
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/is;->a:I

    .line 998
    if-lez p1, :cond_2

    .line 999
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/is;->a:I

    .line 1003
    :cond_0
    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/is;->b:I

    .line 1006
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    if-le v0, v1, :cond_1

    .line 1007
    iput v1, p0, Lcom/android/calendar/event/is;->b:I

    .line 1009
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/calendar/event/is;->c:F

    .line 1010
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0xb4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/calendar/event/is;->d:J

    .line 1011
    return-void

    .line 1000
    :cond_2
    if-gez p1, :cond_0

    .line 1001
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/is;->a:I

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1015
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1018
    iget-wide v2, p0, Lcom/android/calendar/event/is;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1021
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_2

    .line 1022
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/calendar/event/is;->b:I

    .line 1028
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    if-gez v0, :cond_0

    .line 1029
    iput v4, p0, Lcom/android/calendar/event/is;->b:I

    .line 1033
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/is;->a:I

    if-ne v0, v5, :cond_3

    .line 1034
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    iget v1, p0, Lcom/android/calendar/event/is;->b:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ip;->a(Lcom/android/calendar/event/ip;I)I

    .line 1039
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;)I

    move-result v0

    if-gez v0, :cond_4

    .line 1040
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0, v4}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;I)I

    .line 1041
    iput v4, p0, Lcom/android/calendar/event/is;->b:I

    .line 1047
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->e(Lcom/android/calendar/event/ip;)V

    .line 1049
    iget v0, p0, Lcom/android/calendar/event/is;->b:I

    if-lez v0, :cond_5

    .line 1050
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, p0, v2, v3}, Lcom/android/calendar/event/ip;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1058
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-virtual {v0}, Lcom/android/calendar/event/ip;->invalidate()V

    .line 1059
    return-void

    .line 1024
    :cond_2
    iget v0, p0, Lcom/android/calendar/event/is;->c:F

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/is;->c:F

    .line 1025
    iget v0, p0, Lcom/android/calendar/event/is;->c:F

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/event/is;->b:I

    goto :goto_0

    .line 1036
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    iget v1, p0, Lcom/android/calendar/event/is;->b:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/ip;->b(Lcom/android/calendar/event/ip;I)I

    goto :goto_1

    .line 1042
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;)I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v1}, Lcom/android/calendar/event/ip;->d(Lcom/android/calendar/event/ip;)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 1043
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    iget-object v1, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v1}, Lcom/android/calendar/event/ip;->d(Lcom/android/calendar/event/ip;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;I)I

    .line 1044
    iput v4, p0, Lcom/android/calendar/event/is;->b:I

    goto :goto_2

    .line 1053
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0, v4}, Lcom/android/calendar/event/ip;->a(Lcom/android/calendar/event/ip;Z)Z

    .line 1054
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->f(Lcom/android/calendar/event/ip;)V

    .line 1055
    iget-object v0, p0, Lcom/android/calendar/event/is;->e:Lcom/android/calendar/event/ip;

    invoke-static {v0, v5}, Lcom/android/calendar/event/ip;->b(Lcom/android/calendar/event/ip;Z)Z

    goto :goto_3
.end method
