.class public Lcom/android/calendar/event/AccountValidationCheckActivity;
.super Lcom/android/calendar/b;
.source "AccountValidationCheckActivity.java"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Lcom/android/calendar/event/c;

.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/app/AlertDialog;

.field private j:D

.field private k:Lcom/b/a/a/d;

.field private l:Lcom/b/a/a/a;

.field private m:Ljava/lang/String;

.field private n:Landroid/content/Context;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/android/calendar/b;-><init>()V

    .line 51
    const-string v0, "AccountValidationCheckActivity"

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->b:Ljava/lang/String;

    .line 55
    const-string v0, "tivhn39mr9"

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->c:Ljava/lang/String;

    .line 56
    const-string v0, "348652748C0787A06903EDBAAF3359E3"

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->d:Ljava/lang/String;

    .line 57
    const-string v0, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->e:Ljava/lang/String;

    .line 58
    const-string v0, "extra_prefs_show_button_bar"

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->f:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    .line 65
    iput-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->k:Lcom/b/a/a/d;

    .line 66
    iput-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->l:Lcom/b/a/a/a;

    .line 140
    new-instance v0, Lcom/android/calendar/event/a;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/a;-><init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->u:Landroid/content/ServiceConnection;

    .line 284
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/a;)Lcom/b/a/a/a;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->l:Lcom/b/a/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/d;)Lcom/b/a/a/d;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->k:Lcom/b/a/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 274
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "finishActivityWithResult"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 276
    invoke-virtual {p0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 277
    iget-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->o:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 278
    const-string v1, "osp"

    iget-object v2, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->setResult(ILandroid/content/Intent;)V

    .line 281
    invoke-virtual {p0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->finish()V

    .line 282
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->r:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->q:Z

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->s:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->r:Z

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/event/AccountValidationCheckActivity;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->t:Z

    return p1
.end method

.method static synthetic e(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->s:Z

    return v0
.end method

.method static synthetic f(Lcom/android/calendar/event/AccountValidationCheckActivity;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->t:Z

    return v0
.end method

.method static synthetic g(Lcom/android/calendar/event/AccountValidationCheckActivity;)Lcom/b/a/a/a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->l:Lcom/b/a/a/a;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/event/AccountValidationCheckActivity;)Lcom/b/a/a/d;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->k:Lcom/b/a/a/d;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 260
    invoke-super {p0, p1, p2, p3}, Lcom/android/calendar/b;->onActivityResult(IILandroid/content/Intent;)V

    .line 261
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Get result of validation request"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    if-ne p2, v2, :cond_1

    .line 264
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Validation request has succeed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-direct {p0, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(I)V

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    if-nez p2, :cond_0

    .line 267
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Validation request has failed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const v0, 0x7f0f01e9

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 269
    invoke-direct {p0, v3}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/android/calendar/b;->onCreate(Landroid/os/Bundle;)V

    .line 174
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {p0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->n:Landroid/content/Context;

    .line 177
    :try_start_0
    invoke-virtual {p0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 178
    const-string v1, "com.osp.app.signin"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 179
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 180
    const-string v1, "AccountValidationCheckActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "version:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->j:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_0
    iget-wide v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->j:D

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.osp.app.signin"

    const-string v3, "com.msc.sa.service.RequestService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 189
    iget-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->u:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 193
    :goto_1
    return-void

    .line 191
    :cond_0
    new-instance v0, Lcom/android/calendar/event/c;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/c;-><init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->g:Lcom/android/calendar/event/c;

    goto :goto_1

    .line 182
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 197
    invoke-super {p0}, Lcom/android/calendar/b;->onStart()V

    .line 198
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Onstart"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-wide v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->j:D

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 200
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 201
    const-string v1, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->g:Lcom/android/calendar/event/c;

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 204
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.VALIDATION_CHECK_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 205
    const-string v1, "client_id"

    const-string v2, "tivhn39mr9"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v1, "client_secret"

    const-string v2, "348652748C0787A06903EDBAAF3359E3"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-string v1, "mypackage"

    invoke-virtual {p0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v1, "MODE"

    const-string v2, "VALIDATION_CHECK"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    invoke-virtual {p0, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 212
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Send Broadcast to check validation"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :cond_0
    const/4 v0, 0x0

    const v1, 0x7f0f00b3

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    .line 216
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 217
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/android/calendar/event/b;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/b;-><init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 223
    return-void
.end method

.method protected onStop()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 227
    invoke-super {p0}, Lcom/android/calendar/b;->onStop()V

    .line 228
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "OnStop"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 231
    iput-object v4, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->h:Landroid/app/ProgressDialog;

    .line 233
    :cond_0
    iget-wide v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->j:D

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->k:Lcom/b/a/a/d;

    if-eqz v0, :cond_1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->k:Lcom/b/a/a/d;

    iget-object v1, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/b/a/a/d;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :cond_1
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->u:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 256
    :cond_2
    :goto_1
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    const-string v1, "AccountValidationCheckActivity"

    const-string v2, "Fail mISaService.unregisterCallback!!"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 245
    :catch_1
    move-exception v0

    .line 246
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "IllegalArgumentException: Service not registered"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 249
    :cond_3
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "mVersion <= 1.5"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->g:Lcom/android/calendar/event/c;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 251
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 252
    iget-object v0, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 253
    iput-object v4, p0, Lcom/android/calendar/event/AccountValidationCheckActivity;->i:Landroid/app/AlertDialog;

    goto :goto_1
.end method
