.class Lcom/android/calendar/event/lc;
.super Landroid/os/AsyncTask;
.source "SelectStickerFragment.java"


# instance fields
.field private a:Lcom/android/calendar/event/kv;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 991
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/kl;)V
    .locals 0

    .prologue
    .line 991
    invoke-direct {p0}, Lcom/android/calendar/event/lc;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Lcom/android/calendar/event/kv;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 996
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    .line 997
    iget-object v0, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    iget-object v0, v0, Lcom/android/calendar/event/kv;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 998
    :cond_0
    const/4 v0, 0x0

    .line 1001
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    iget-object v0, v0, Lcom/android/calendar/event/kv;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 1007
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1008
    new-instance v0, Landroid/util/LruCache;

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->a(Landroid/util/LruCache;)Landroid/util/LruCache;

    .line 1010
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1011
    iget-object v0, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    iget-object v0, v0, Lcom/android/calendar/event/kv;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1012
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1013
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->g()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1014
    :try_start_0
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/lc;->a:Lcom/android/calendar/event/kv;

    iget-wide v2, v2, Lcom/android/calendar/event/kv;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1015
    monitor-exit v1

    .line 1018
    :cond_1
    return-void

    .line 1015
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 991
    check-cast p1, [Lcom/android/calendar/event/kv;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/lc;->a([Lcom/android/calendar/event/kv;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 991
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/lc;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
