.class public Lcom/android/calendar/event/av;
.super Ljava/lang/Object;
.source "EditEventHelper.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field protected static b:I

.field public static final c:[Ljava/lang/String;

.field public static final e:[I

.field public static final f:[Ljava/lang/String;

.field static final g:[Ljava/lang/String;

.field static final h:[Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static j:Landroid/content/Context;

.field private static l:Lcom/android/calendar/d/g;

.field private static m:Lcom/android/calendar/d/a/a;


# instance fields
.field protected d:Z

.field private k:Lcom/android/calendar/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    const-class v0, Lcom/android/calendar/event/av;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    .line 85
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "description"

    aput-object v1, v0, v5

    const-string v1, "eventLocation"

    aput-object v1, v0, v6

    const-string v1, "allDay"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "calendar_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "dtstart"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dtend"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "availability"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "accessLevel"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ownerAccount"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "original_sync_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "original_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "availabilityStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "account_type"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "eventColor_index"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "rdate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/av;->a:[Ljava/lang/String;

    .line 149
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/event/av;->b:I

    .line 151
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "minutes"

    aput-object v1, v0, v4

    const-string v1, "method"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/event/av;->c:[Ljava/lang/String;

    .line 195
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/av;->m:Lcom/android/calendar/d/a/a;

    .line 198
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/event/av;->e:[I

    .line 212
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "calendar_displayName"

    aput-object v1, v0, v4

    const-string v1, "ownerAccount"

    aput-object v1, v0, v5

    const-string v1, "calendar_color"

    aput-object v1, v0, v6

    const-string v1, "canOrganizerRespond"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "calendar_access_level"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "visible"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "maxReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "allowedReminders"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "allowedAttendeeTypes"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "allowedAvailability"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "account_name"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    .line 275
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "attendeeName"

    aput-object v1, v0, v4

    const-string v1, "attendeeEmail"

    aput-object v1, v0, v5

    const-string v1, "attendeeRelationship"

    aput-object v1, v0, v6

    const-string v1, "attendeeStatus"

    aput-object v1, v0, v7

    sput-object v0, Lcom/android/calendar/event/av;->g:[Ljava/lang/String;

    .line 291
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "account_type"

    aput-object v1, v0, v5

    const-string v1, "color"

    aput-object v1, v0, v6

    const-string v1, "color_index"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "color_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/av;->h:[Ljava/lang/String;

    return-void

    .line 198
    :array_0
    .array-data 4
        0x0
        0x1
        0x4
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/as;)V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/av;->d:Z

    .line 330
    sput-object p1, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    .line 331
    instance-of v0, p1, Lcom/android/calendar/b;

    if-eqz v0, :cond_0

    .line 332
    check-cast p1, Lcom/android/calendar/b;

    invoke-virtual {p1}, Lcom/android/calendar/b;->a()Lcom/android/calendar/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/av;->k:Lcom/android/calendar/ag;

    .line 336
    :goto_0
    return-void

    .line 334
    :cond_0
    new-instance v0, Lcom/android/calendar/ag;

    invoke-direct {v0, p1}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/av;->k:Lcom/android/calendar/ag;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/android/b/b;)Ljava/util/LinkedHashSet;
    .locals 6

    .prologue
    .line 777
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 778
    invoke-static {p0, v1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;Ljava/util/Collection;)V

    .line 779
    if-nez p1, :cond_0

    move-object v0, v1

    .line 801
    :goto_0
    return-object v0

    .line 785
    :cond_0
    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 786
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 787
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/util/Rfc822Token;

    .line 788
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 789
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/android/b/b;->isValid(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 791
    sget-object v3, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dropping invalid attendee email address: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 795
    :cond_2
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/android/b/b;->isValid(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 796
    sget-object v3, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dropping invalid attendee email address: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 801
    goto :goto_0
.end method

.method static a(ILcom/android/calendar/as;I)V
    .locals 8

    .prologue
    .line 1200
    new-instance v2, Lcom/android/a/c;

    invoke-direct {v2}, Lcom/android/a/c;-><init>()V

    .line 1202
    iget v0, p1, Lcom/android/calendar/as;->S:I

    if-lez v0, :cond_0

    .line 1203
    iget v0, p1, Lcom/android/calendar/as;->S:I

    iput v0, v2, Lcom/android/a/c;->e:I

    .line 1206
    :cond_0
    const/4 v0, 0x0

    iput v0, v2, Lcom/android/a/c;->o:I

    .line 1207
    const/4 v0, 0x0

    iput v0, v2, Lcom/android/a/c;->q:I

    .line 1209
    if-nez p0, :cond_2

    .line 1210
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1402
    :cond_1
    :goto_0
    return-void

    .line 1212
    :cond_2
    const/4 v0, 0x5

    if-eq p0, v0, :cond_1

    .line 1215
    const/4 v0, 0x1

    if-ne p0, v0, :cond_9

    .line 1216
    const/4 v0, 0x4

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1342
    :cond_3
    :goto_1
    invoke-static {p2}, Lcom/android/a/c;->a(I)I

    move-result v0

    iput v0, v2, Lcom/android/a/c;->f:I

    .line 1346
    iget v0, p1, Lcom/android/calendar/as;->N:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1e

    iget-boolean v0, p1, Lcom/android/calendar/as;->U:Z

    if-nez v0, :cond_1e

    .line 1348
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1349
    iget-object v1, p1, Lcom/android/calendar/as;->O:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    .line 1351
    iget-boolean v1, p1, Lcom/android/calendar/as;->G:Z

    if-nez v1, :cond_4

    .line 1352
    new-instance v1, Landroid/text/format/Time;

    iget-object v3, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1353
    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1355
    iget-object v3, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 1356
    const/4 v3, 0x0

    iput-boolean v3, v0, Landroid/text/format/Time;->allDay:Z

    .line 1357
    iget v3, v1, Landroid/text/format/Time;->hour:I

    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 1358
    iget v3, v1, Landroid/text/format/Time;->minute:I

    iput v3, v0, Landroid/text/format/Time;->minute:I

    .line 1359
    iget v1, v1, Landroid/text/format/Time;->second:I

    iput v1, v0, Landroid/text/format/Time;->second:I

    .line 1360
    const-string v1, "UTC"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 1362
    :cond_4
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 1369
    :goto_2
    iget-boolean v0, p1, Lcom/android/calendar/as;->U:Z

    if-eqz v0, :cond_5

    .line 1370
    const/4 v0, 0x0

    iput-object v0, v2, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 1371
    const/4 v0, 0x0

    iput v0, v2, Lcom/android/a/c;->d:I

    .line 1380
    :cond_5
    invoke-virtual {v2}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1382
    iget v0, v2, Lcom/android/a/c;->e:I

    const/4 v3, 0x1

    if-ge v0, v3, :cond_6

    .line 1383
    const/4 v0, 0x1

    iput v0, v2, Lcom/android/a/c;->e:I

    .line 1386
    :cond_6
    const-string v0, ""

    .line 1387
    invoke-virtual {v2}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1389
    iget-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 1390
    new-instance v0, Lcom/android/a/c;

    invoke-direct {v0}, Lcom/android/a/c;-><init>()V

    .line 1391
    iget-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 1392
    iget v3, v0, Lcom/android/a/c;->e:I

    const/4 v4, 0x1

    if-ge v3, v4, :cond_7

    .line 1393
    const/4 v3, 0x1

    iput v3, v0, Lcom/android/a/c;->e:I

    .line 1395
    :cond_7
    invoke-virtual {v0}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1398
    :cond_8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1399
    iput-object v1, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 1217
    :cond_9
    const/4 v0, 0x6

    if-ne p0, v0, :cond_c

    .line 1218
    const/4 v0, 0x5

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1219
    const/4 v1, 0x5

    .line 1220
    new-array v3, v1, [I

    .line 1221
    new-array v4, v1, [I

    .line 1223
    const-string v0, "FRISAT"

    sget-object v5, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/calendar/hj;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1225
    const/4 v0, 0x0

    const/high16 v5, 0x20000

    aput v5, v3, v0

    .line 1226
    const/4 v0, 0x1

    const/high16 v5, 0x40000

    aput v5, v3, v0

    .line 1227
    const/4 v0, 0x2

    const/high16 v5, 0x80000

    aput v5, v3, v0

    .line 1228
    const/4 v0, 0x3

    const/high16 v5, 0x100000

    aput v5, v3, v0

    .line 1229
    const/4 v0, 0x4

    const/high16 v5, 0x200000

    aput v5, v3, v0

    .line 1237
    :goto_3
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_b

    .line 1238
    const/4 v5, 0x0

    aput v5, v4, v0

    .line 1237
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1231
    :cond_a
    const/4 v0, 0x0

    const/high16 v5, 0x10000

    aput v5, v3, v0

    .line 1232
    const/4 v0, 0x1

    const/high16 v5, 0x20000

    aput v5, v3, v0

    .line 1233
    const/4 v0, 0x2

    const/high16 v5, 0x40000

    aput v5, v3, v0

    .line 1234
    const/4 v0, 0x3

    const/high16 v5, 0x80000

    aput v5, v3, v0

    .line 1235
    const/4 v0, 0x4

    const/high16 v5, 0x100000

    aput v5, v3, v0

    goto :goto_3

    .line 1241
    :cond_b
    iput-object v3, v2, Lcom/android/a/c;->m:[I

    .line 1242
    iput-object v4, v2, Lcom/android/a/c;->n:[I

    .line 1243
    iput v1, v2, Lcom/android/a/c;->o:I

    goto/16 :goto_1

    .line 1244
    :cond_c
    const/4 v0, 0x2

    if-ne p0, v0, :cond_17

    .line 1245
    const/4 v0, 0x5

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1246
    const/4 v1, 0x7

    .line 1247
    new-array v3, v1, [I

    .line 1248
    new-array v4, v1, [I

    .line 1249
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1250
    new-instance v0, Landroid/text/format/Time;

    iget-object v6, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v0, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1251
    iget-wide v6, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1253
    const/4 v6, 0x0

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x0

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_e

    const/high16 v0, 0x10000

    :goto_5
    aput v0, v3, v6

    .line 1254
    const/4 v6, 0x1

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x1

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_f

    const/high16 v0, 0x20000

    :goto_6
    aput v0, v3, v6

    .line 1255
    const/4 v6, 0x2

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x2

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_10

    const/high16 v0, 0x40000

    :goto_7
    aput v0, v3, v6

    .line 1256
    const/4 v6, 0x3

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x3

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_11

    const/high16 v0, 0x80000

    :goto_8
    aput v0, v3, v6

    .line 1257
    const/4 v6, 0x4

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x4

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_12

    const/high16 v0, 0x100000

    :goto_9
    aput v0, v3, v6

    .line 1258
    const/4 v6, 0x5

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x5

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_13

    const/high16 v0, 0x200000

    :goto_a
    aput v0, v3, v6

    .line 1259
    const/4 v6, 0x6

    iget-object v0, p1, Lcom/android/calendar/as;->V:[Z

    const/4 v7, 0x6

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_14

    const/high16 v0, 0x400000

    :goto_b
    aput v0, v3, v6

    .line 1260
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v1, :cond_15

    .line 1261
    const/4 v6, 0x0

    aput v6, v4, v0

    .line 1262
    aget v6, v3, v0

    if-eqz v6, :cond_d

    aget v6, v3, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1260
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1253
    :cond_e
    const/4 v0, 0x0

    goto :goto_5

    .line 1254
    :cond_f
    const/4 v0, 0x0

    goto :goto_6

    .line 1255
    :cond_10
    const/4 v0, 0x0

    goto :goto_7

    .line 1256
    :cond_11
    const/4 v0, 0x0

    goto :goto_8

    .line 1257
    :cond_12
    const/4 v0, 0x0

    goto :goto_9

    .line 1258
    :cond_13
    const/4 v0, 0x0

    goto :goto_a

    .line 1259
    :cond_14
    const/4 v0, 0x0

    goto :goto_b

    .line 1265
    :cond_15
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1266
    new-array v6, v3, [I

    .line 1267
    const/4 v0, 0x0

    move v1, v0

    :goto_d
    array-length v0, v6

    if-ge v1, v0, :cond_16

    .line 1268
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v6, v1

    .line 1267
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 1270
    :cond_16
    iput-object v6, v2, Lcom/android/a/c;->m:[I

    .line 1271
    iput-object v4, v2, Lcom/android/a/c;->n:[I

    .line 1272
    iput v3, v2, Lcom/android/a/c;->o:I

    goto/16 :goto_1

    .line 1273
    :cond_17
    const/4 v0, 0x7

    if-ne p0, v0, :cond_18

    .line 1274
    const/4 v0, 0x5

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1275
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 1276
    const/4 v1, 0x1

    .line 1277
    new-array v3, v1, [I

    .line 1278
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1279
    iget-wide v6, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1281
    const/4 v5, 0x0

    iget v4, v4, Landroid/text/format/Time;->weekDay:I

    invoke-static {v4}, Lcom/android/a/c;->b(I)I

    move-result v4

    aput v4, v0, v5

    .line 1283
    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    .line 1285
    iput-object v0, v2, Lcom/android/a/c;->m:[I

    .line 1286
    iput-object v3, v2, Lcom/android/a/c;->n:[I

    .line 1287
    iput v1, v2, Lcom/android/a/c;->o:I

    .line 1288
    const/4 v0, 0x2

    iput v0, v2, Lcom/android/a/c;->e:I

    goto/16 :goto_1

    .line 1289
    :cond_18
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1d

    .line 1290
    const/4 v0, 0x6

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1292
    new-instance v1, Landroid/text/format/Time;

    iget-object v0, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1293
    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1295
    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1296
    iget v0, p1, Lcom/android/calendar/as;->T:I

    if-nez v0, :cond_1b

    .line 1297
    const/4 v0, 0x6

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1298
    const/4 v0, 0x0

    iput v0, v2, Lcom/android/a/c;->o:I

    .line 1299
    const/4 v0, 0x1

    iput v0, v2, Lcom/android/a/c;->q:I

    .line 1300
    const/4 v0, 0x1

    new-array v3, v0, [I

    .line 1301
    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1302
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    .line 1305
    invoke-static {}, Lcom/android/calendar/event/av;->b()Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1306
    iget-boolean v4, p1, Lcom/android/calendar/as;->ay:Z

    if-eqz v4, :cond_1a

    .line 1307
    sget-object v0, Lcom/android/calendar/event/av;->m:Lcom/android/calendar/d/a/a;

    if-nez v0, :cond_19

    .line 1308
    sget-object v0, Lcom/android/calendar/event/av;->l:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->a()Lcom/android/calendar/d/a/a;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/av;->m:Lcom/android/calendar/d/a/a;

    .line 1310
    :cond_19
    sget-object v0, Lcom/android/calendar/event/av;->m:Lcom/android/calendar/d/a/a;

    iget v4, v1, Landroid/text/format/Time;->year:I

    iget v5, v1, Landroid/text/format/Time;->month:I

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v0, v4, v5, v1}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 1312
    sget-object v0, Lcom/android/calendar/event/av;->m:Lcom/android/calendar/d/a/a;

    invoke-virtual {v0}, Lcom/android/calendar/d/a/a;->c()I

    move-result v0

    .line 1317
    :cond_1a
    const/4 v1, 0x0

    aput v0, v3, v1

    .line 1318
    iput-object v3, v2, Lcom/android/a/c;->p:[I

    goto/16 :goto_1

    .line 1319
    :cond_1b
    iget v0, p1, Lcom/android/calendar/as;->T:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 1320
    const/4 v0, 0x6

    iput v0, v2, Lcom/android/a/c;->b:I

    .line 1321
    const/4 v0, 0x1

    iput v0, v2, Lcom/android/a/c;->o:I

    .line 1322
    const/4 v0, 0x0

    iput v0, v2, Lcom/android/a/c;->q:I

    .line 1324
    const/4 v0, 0x1

    new-array v3, v0, [I

    .line 1325
    const/4 v0, 0x1

    new-array v4, v0, [I

    .line 1326
    iget-wide v6, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1328
    iget v0, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    .line 1329
    const/4 v5, 0x5

    if-ne v0, v5, :cond_1c

    .line 1330
    const/4 v0, -0x1

    .line 1332
    :cond_1c
    const/4 v5, 0x0

    aput v0, v4, v5

    .line 1333
    const/4 v0, 0x0

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    invoke-static {v1}, Lcom/android/a/c;->b(I)I

    move-result v1

    aput v1, v3, v0

    .line 1334
    iput-object v3, v2, Lcom/android/a/c;->m:[I

    .line 1335
    iput-object v4, v2, Lcom/android/a/c;->n:[I

    goto/16 :goto_1

    .line 1337
    :cond_1d
    const/4 v0, 0x4

    if-ne p0, v0, :cond_3

    .line 1338
    const/4 v0, 0x7

    iput v0, v2, Lcom/android/a/c;->b:I

    goto/16 :goto_1

    .line 1365
    :cond_1e
    iget v0, p1, Lcom/android/calendar/as;->R:I

    iput v0, v2, Lcom/android/a/c;->d:I

    goto/16 :goto_2
.end method

.method public static a(Lcom/android/calendar/as;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/16 v9, 0x1b

    const/4 v8, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1412
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 1413
    :cond_0
    sget-object v0, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v1, "Attempted to build non-existent model or from an incorrect query."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    :cond_1
    :goto_0
    return-void

    .line 1417
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/as;->b()V

    .line 1418
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1420
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, p0, Lcom/android/calendar/as;->b:J

    .line 1421
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 1422
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 1423
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 1424
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/as;->G:Z

    .line 1425
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/android/calendar/as;->H:Z

    .line 1426
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, p0, Lcom/android/calendar/as;->c:J

    .line 1427
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/as;->y:J

    .line 1428
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1429
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1430
    iput-object v0, p0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 1432
    :cond_3
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1433
    iput-object v4, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1434
    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1435
    iput-object v5, p0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 1438
    if-eqz v4, :cond_4

    .line 1439
    const-string v0, "UNTIL"

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "COUNT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1441
    iput-boolean v1, p0, Lcom/android/calendar/as;->U:Z

    .line 1446
    :cond_4
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    .line 1447
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/android/calendar/as;->I:Z

    .line 1448
    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->J:I

    .line 1449
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1450
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 1451
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/android/calendar/as;->W:Z

    .line 1452
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 1453
    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/calendar/as;->aa:J

    .line 1454
    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 1455
    iget-object v0, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/as;->v:Z

    .line 1456
    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/android/calendar/as;->ad:Z

    .line 1458
    const/16 v0, 0x16

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    .line 1460
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->ai:I

    .line 1461
    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->aj:I

    .line 1462
    const/16 v0, 0x19

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/calendar/as;->ar:J

    .line 1464
    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->h:I

    .line 1466
    invoke-interface {p1, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1467
    iput v8, p0, Lcom/android/calendar/as;->i:I

    .line 1472
    :goto_6
    if-lez v3, :cond_f

    .line 1476
    add-int/lit8 v0, v3, -0x1

    .line 1478
    :goto_7
    iput v0, p0, Lcom/android/calendar/as;->au:I

    .line 1480
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    .line 1483
    :goto_8
    if-nez v0, :cond_5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1484
    :cond_5
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->D:Ljava/lang/String;

    .line 1489
    :goto_9
    iput-boolean v1, p0, Lcom/android/calendar/as;->at:Z

    .line 1492
    invoke-static {}, Lcom/android/calendar/event/av;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/android/calendar/event/av;->b:I

    if-eq v0, v8, :cond_1

    .line 1493
    sget v0, Lcom/android/calendar/event/av;->b:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_e

    :goto_a
    iput-boolean v1, p0, Lcom/android/calendar/as;->ay:Z

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1424
    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 1425
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 1447
    goto/16 :goto_3

    :cond_9
    move v0, v2

    .line 1451
    goto/16 :goto_4

    :cond_a
    move v0, v2

    .line 1456
    goto :goto_5

    .line 1469
    :cond_b
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->i:I

    goto :goto_6

    :cond_c
    move v0, v2

    .line 1480
    goto :goto_8

    .line 1486
    :cond_d
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/as;->B:J

    goto :goto_9

    :cond_e
    move v1, v2

    .line 1493
    goto :goto_a

    :cond_f
    move v0, v3

    goto :goto_7
.end method

.method public static a(Landroid/database/Cursor;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1753
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1754
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    move v1, v0

    .line 1755
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1756
    const/16 v3, 0xc

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1757
    const-string v4, "com.google"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1758
    const/4 v0, 0x1

    .line 1762
    :cond_0
    return v0

    .line 1760
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 1755
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/android/calendar/as;)Z
    .locals 1

    .prologue
    .line 1543
    invoke-static {p0}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/as;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/as;->ad:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/android/calendar/as;Lcom/android/calendar/as;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1028
    if-nez p1, :cond_1

    .line 1036
    :cond_0
    :goto_0
    return v0

    .line 1032
    :cond_1
    iget-wide v2, p0, Lcom/android/calendar/as;->b:J

    iget-wide v4, p1, Lcom/android/calendar/as;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1033
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/util/ArrayList;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1100
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p4, :cond_0

    .line 1125
    :goto_0
    return v0

    .line 1105
    :cond_0
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1107
    const-string v3, "event_id=?"

    new-array v4, v1, [Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 1108
    invoke-virtual {v2, v0, p1}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    .line 1109
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1111
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1112
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    .line 1115
    :goto_1
    if-ge v2, v4, :cond_1

    .line 1116
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 1118
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 1119
    const-string v5, "minutes"

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1120
    const-string v5, "method"

    invoke-virtual {v0}, Lcom/android/calendar/au;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1121
    sget-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1122
    const-string v5, "event_id"

    invoke-virtual {v0, v5, p1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 1123
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1115
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1125
    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;JLjava/util/ArrayList;Ljava/util/ArrayList;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1053
    invoke-virtual {p3, p4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p5, :cond_0

    .line 1081
    :goto_0
    return v0

    .line 1058
    :cond_0
    const-string v2, "event_id=?"

    .line 1059
    new-array v3, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 1062
    sget-object v4, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 1064
    invoke-virtual {v4, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 1065
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1067
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1068
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    .line 1071
    :goto_1
    if-ge v2, v4, :cond_1

    .line 1072
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/au;

    .line 1074
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 1075
    const-string v5, "minutes"

    invoke-virtual {v0}, Lcom/android/calendar/au;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1076
    const-string v5, "method"

    invoke-virtual {v0}, Lcom/android/calendar/au;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1077
    const-string v0, "event_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1078
    sget-object v0, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1079
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1071
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1081
    goto :goto_0
.end method

.method public static a()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 1786
    sget-object v0, Lcom/android/calendar/event/av;->a:[Ljava/lang/String;

    .line 1788
    invoke-static {}, Lcom/android/calendar/event/av;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1790
    invoke-static {v0}, Lcom/android/calendar/event/av;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1792
    :cond_0
    return-object v0
.end method

.method protected static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1798
    array-length v0, p0

    .line 1799
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/String;

    .line 1800
    invoke-static {p0, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1801
    const-string v2, "setLunar"

    aput-object v2, v1, v0

    .line 1802
    sput v0, Lcom/android/calendar/event/av;->b:I

    .line 1803
    return-object v1
.end method

.method protected static b(J)J
    .locals 2

    .prologue
    .line 830
    const-wide/32 v0, 0x36ee80

    add-long/2addr v0, p0

    return-wide v0
.end method

.method public static b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1769
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1770
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 1771
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1772
    const/16 v2, 0xc

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1773
    const-string v3, "com.google"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1774
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1780
    :goto_1
    return-object v0

    .line 1778
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 1771
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1780
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected static b()Z
    .locals 2

    .prologue
    .line 1809
    const/4 v0, 0x0

    .line 1810
    sget-object v1, Lcom/android/calendar/event/av;->l:Lcom/android/calendar/d/g;

    if-nez v1, :cond_0

    .line 1811
    invoke-static {}, Lcom/android/calendar/d/g;->h()Lcom/android/calendar/d/g;

    move-result-object v1

    sput-object v1, Lcom/android/calendar/event/av;->l:Lcom/android/calendar/d/g;

    .line 1813
    :cond_0
    sget-object v1, Lcom/android/calendar/event/av;->l:Lcom/android/calendar/d/g;

    if-eqz v1, :cond_1

    .line 1814
    sget-object v0, Lcom/android/calendar/event/av;->l:Lcom/android/calendar/d/g;

    invoke-virtual {v0}, Lcom/android/calendar/d/g;->c()Z

    move-result v0

    .line 1816
    :cond_1
    return v0
.end method

.method public static b(Lcom/android/calendar/as;)Z
    .locals 4

    .prologue
    .line 1547
    iget v0, p0, Lcom/android/calendar/as;->ah:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    iget-wide v0, p0, Lcom/android/calendar/as;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/android/calendar/as;Landroid/database/Cursor;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1507
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 1508
    :cond_0
    sget-object v1, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v2, "Attempted to build non-existent model or from an incorrect query."

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 1539
    :cond_1
    :goto_0
    return v0

    .line 1512
    :cond_2
    iget-wide v2, p0, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1516
    iget-boolean v2, p0, Lcom/android/calendar/as;->at:Z

    if-nez v2, :cond_3

    .line 1517
    sget-object v1, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v2, "Can\'t update model with a Calendar cursor until it has seen an Event cursor."

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1522
    :cond_3
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1523
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1524
    iget-wide v2, p0, Lcom/android/calendar/as;->c:J

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 1525
    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    :cond_5
    iput-boolean v0, p0, Lcom/android/calendar/as;->ag:Z

    .line 1527
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->ah:I

    .line 1528
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->d:Ljava/lang/String;

    .line 1529
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->e:I

    .line 1531
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/as;->f:I

    .line 1532
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->g:Ljava/lang/String;

    .line 1533
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 1535
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/as;->m:Ljava/lang/String;

    move v0, v1

    .line 1536
    goto :goto_0
.end method

.method static b(Lcom/android/calendar/as;Lcom/android/calendar/as;)Z
    .locals 4

    .prologue
    .line 1131
    iget-wide v0, p0, Lcom/android/calendar/as;->x:J

    iget-wide v2, p1, Lcom/android/calendar/as;->y:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)Landroid/content/ContentProviderOperation$Builder;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1879
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1880
    const-string v1, "recently"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1881
    sget-object v1, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1884
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    long-to-int v1, p1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_4

    .line 1886
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    long-to-int v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 1887
    iget v1, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    if-nez v1, :cond_1

    .line 1888
    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 1889
    iget v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    .line 1890
    iget v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    const/16 v5, 0x14

    if-le v4, v5, :cond_0

    .line 1891
    const/4 v4, 0x0

    iput v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    goto :goto_0

    .line 1895
    :cond_1
    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1896
    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 1897
    iget v4, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    iget v5, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    if-le v4, v5, :cond_2

    .line 1898
    iget v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    goto :goto_1

    .line 1903
    :cond_3
    iput v6, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    .line 1905
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    if-eqz v0, :cond_4

    .line 1906
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/kx;->a(Ljava/util/ArrayList;)V

    .line 1910
    :cond_4
    return-object v2
.end method

.method public static c(Lcom/android/calendar/as;)Z
    .locals 2

    .prologue
    .line 1552
    iget v0, p0, Lcom/android/calendar/as;->ah:I

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/android/calendar/as;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1567
    invoke-static {p0}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1585
    :cond_0
    :goto_0
    return v0

    .line 1571
    :cond_1
    iget-boolean v2, p0, Lcom/android/calendar/as;->v:Z

    if-nez v2, :cond_2

    move v0, v1

    .line 1572
    goto :goto_0

    .line 1575
    :cond_2
    iget-boolean v2, p0, Lcom/android/calendar/as;->ag:Z

    if-eqz v2, :cond_0

    .line 1581
    iget-boolean v2, p0, Lcom/android/calendar/as;->W:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1585
    goto :goto_0
.end method

.method private static f(Lcom/android/calendar/as;)Z
    .locals 2

    .prologue
    .line 1821
    const/4 v0, 0x0

    .line 1822
    iget-boolean v1, p0, Lcom/android/calendar/as;->ay:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1823
    const/4 v0, 0x1

    .line 1825
    :cond_0
    return v0
.end method

.method private g(Lcom/android/calendar/as;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 1831
    new-instance v1, Lcom/android/calendar/b/b;

    invoke-direct {v1}, Lcom/android/calendar/b/b;-><init>()V

    .line 1832
    new-instance v2, Lcom/android/calendar/b/a;

    iget-object v3, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/android/calendar/b/a;-><init>(Ljava/lang/String;)V

    .line 1833
    iput-boolean v9, v2, Lcom/android/calendar/b/a;->a:Z

    .line 1837
    iget-boolean v3, p1, Lcom/android/calendar/as;->U:Z

    if-ne v3, v9, :cond_0

    .line 1838
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";UNTIL=20361230T235900Z"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1840
    :cond_0
    new-instance v3, Lcom/android/a/ad;

    iget-object v4, p1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-direct {v3, v4, v0, v0, v0}, Lcom/android/a/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    iget-wide v4, p1, Lcom/android/calendar/as;->y:J

    invoke-virtual {v2, v4, v5}, Lcom/android/calendar/b/a;->set(J)V

    .line 1842
    iget-boolean v4, p1, Lcom/android/calendar/as;->G:Z

    iput-boolean v4, v2, Lcom/android/calendar/b/a;->allDay:Z

    .line 1843
    iget-boolean v4, v2, Lcom/android/calendar/b/a;->allDay:Z

    if-ne v4, v9, :cond_1

    .line 1844
    iput v8, v2, Lcom/android/calendar/b/a;->hour:I

    .line 1845
    iput v8, v2, Lcom/android/calendar/b/a;->minute:I

    .line 1846
    iput v8, v2, Lcom/android/calendar/b/a;->second:I

    .line 1848
    :cond_1
    invoke-virtual {v2, v9}, Lcom/android/calendar/b/a;->normalize(Z)J

    .line 1851
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v2, v4}, Lcom/android/calendar/b/a;->toMillis(Z)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/b/b;->a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J
    :try_end_0
    .catch Lcom/android/a/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1859
    :goto_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1860
    if-eqz v0, :cond_3

    .line 1861
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1862
    new-instance v4, Landroid/text/format/Time;

    iget-object v1, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v4, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1864
    array-length v5, v0

    move v1, v8

    move v2, v9

    :goto_1
    if-ge v1, v5, :cond_3

    aget-wide v6, v0, v1

    .line 1865
    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 1866
    if-nez v2, :cond_2

    .line 1867
    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1870
    :cond_2
    invoke-virtual {v4}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1864
    add-int/lit8 v1, v1, 0x1

    move v2, v8

    goto :goto_1

    .line 1874
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1852
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected a(J)J
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 811
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 812
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 813
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 814
    const/16 v1, 0x1e

    iput v1, v0, Landroid/text/format/Time;->minute:I

    .line 815
    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 816
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    .line 819
    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v2, 0x1b7740

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;Lcom/android/calendar/as;J)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 929
    iget-boolean v1, p2, Lcom/android/calendar/as;->G:Z

    .line 930
    iget-object v0, p2, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 933
    new-instance v8, Lcom/android/a/c;

    invoke-direct {v8}, Lcom/android/a/c;-><init>()V

    .line 934
    invoke-virtual {v8, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 937
    iget-wide v4, p2, Lcom/android/calendar/as;->y:J

    .line 938
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 939
    iget-object v3, p2, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v3, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 940
    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 942
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 944
    iget v3, v8, Lcom/android/a/c;->d:I

    if-lez v3, :cond_1

    .line 955
    new-instance v3, Lcom/android/a/ad;

    iget-object v1, p2, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-direct {v3, v1, v7, v7, v7}, Lcom/android/a/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    new-instance v1, Lcom/android/a/ab;

    invoke-direct {v1}, Lcom/android/a/ab;-><init>()V

    move-wide v6, p3

    .line 959
    :try_start_0
    invoke-virtual/range {v1 .. v7}, Lcom/android/a/ab;->a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J
    :try_end_0
    .catch Lcom/android/a/a; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 964
    array-length v3, v1

    if-nez v3, :cond_0

    .line 965
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "can\'t use this method on first instance"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 960
    :catch_0
    move-exception v0

    .line 961
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 968
    :cond_0
    new-instance v3, Lcom/android/a/c;

    invoke-direct {v3}, Lcom/android/a/c;-><init>()V

    .line 969
    invoke-virtual {v3, v0}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 970
    iget v0, v3, Lcom/android/a/c;->d:I

    array-length v4, v1

    sub-int/2addr v0, v4

    iput v0, v3, Lcom/android/a/c;->d:I

    .line 971
    invoke-virtual {v3}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v0

    .line 973
    array-length v1, v1

    iput v1, v8, Lcom/android/a/c;->d:I

    .line 1009
    :goto_0
    const-string v1, "rrule"

    invoke-virtual {v8}, Lcom/android/a/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    const-string v1, "dtstart"

    invoke-virtual {v2, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1011
    iget-object v1, p2, Lcom/android/calendar/as;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 1014
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1016
    return-object v0

    .line 980
    :cond_1
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 981
    const-string v4, "UTC"

    iput-object v4, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 985
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 986
    :cond_2
    const-wide/32 v4, 0x5265c00

    sub-long v4, p3, v4

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 991
    :goto_1
    if-eqz v1, :cond_3

    .line 992
    iput v6, v3, Landroid/text/format/Time;->hour:I

    .line 993
    iput v6, v3, Landroid/text/format/Time;->minute:I

    .line 994
    iput v6, v3, Landroid/text/format/Time;->second:I

    .line 995
    iput-boolean v10, v3, Landroid/text/format/Time;->allDay:Z

    .line 996
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 1000
    iput v6, v2, Landroid/text/format/Time;->hour:I

    .line 1001
    iput v6, v2, Landroid/text/format/Time;->minute:I

    .line 1002
    iput v6, v2, Landroid/text/format/Time;->second:I

    .line 1003
    iput-boolean v10, v2, Landroid/text/format/Time;->allDay:Z

    .line 1004
    const-string v1, "UTC"

    iput-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1006
    :cond_3
    invoke-virtual {v3}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/android/a/c;->c:Ljava/lang/String;

    goto :goto_0

    .line 988
    :cond_4
    const-wide/16 v4, 0x3e8

    sub-long v4, p3, v4

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    goto :goto_1
.end method

.method a(Landroid/content/ContentValues;Lcom/android/calendar/as;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0x5265c00

    const/4 v1, 0x0

    .line 1136
    iget-object v2, p2, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1138
    const-string v0, "rrule"

    invoke-virtual {p1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    iget-wide v4, p2, Lcom/android/calendar/as;->B:J

    .line 1140
    iget-wide v6, p2, Lcom/android/calendar/as;->y:J

    .line 1141
    iget-object v0, p2, Lcom/android/calendar/as;->D:Ljava/lang/String;

    .line 1143
    iget-boolean v3, p2, Lcom/android/calendar/as;->G:Z

    .line 1144
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1145
    cmp-long v0, v4, v6

    if-ltz v0, :cond_4

    .line 1146
    if-eqz v3, :cond_3

    .line 1148
    sub-long/2addr v4, v6

    add-long/2addr v4, v10

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    div-long/2addr v4, v10

    .line 1150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "D"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1167
    :cond_0
    :goto_0
    const-string v3, "duration"

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    const-string v3, "dtend"

    move-object v0, v1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1170
    invoke-static {}, Lcom/android/calendar/event/av;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/android/calendar/event/av;->f(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1173
    invoke-direct {p0, p2}, Lcom/android/calendar/event/av;->g(Lcom/android/calendar/as;)Ljava/lang/String;

    move-result-object v3

    .line 1174
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1175
    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 1176
    :goto_1
    if-nez v0, :cond_7

    .line 1177
    const-string v0, "rdate"

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    :goto_2
    new-instance v0, Lcom/android/a/c;

    invoke-direct {v0}, Lcom/android/a/c;-><init>()V

    .line 1183
    invoke-virtual {v0, v2}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 1184
    iget v1, v0, Lcom/android/a/c;->b:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_1

    iget v0, v0, Lcom/android/a/c;->b:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 1185
    :cond_1
    const-string v0, "rrule"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1189
    :cond_2
    return-void

    .line 1153
    :cond_3
    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "S"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1159
    :cond_4
    if-eqz v3, :cond_5

    .line 1160
    const-string v0, "P1D"

    goto :goto_0

    .line 1162
    :cond_5
    const-string v0, "P3600S"

    goto :goto_0

    .line 1175
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 1179
    :cond_7
    const-string v0, "duration"

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1180
    const-string v0, "dtend"

    iget-wide v4, p2, Lcom/android/calendar/as;->B:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_2
.end method

.method a(Lcom/android/calendar/as;Lcom/android/calendar/as;Landroid/content/ContentValues;I)V
    .locals 17

    .prologue
    .line 839
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/android/calendar/as;->x:J

    .line 840
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/calendar/as;->A:J

    .line 841
    move-object/from16 v0, p1

    iget-boolean v6, v0, Lcom/android/calendar/as;->G:Z

    .line 842
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 843
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 845
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/android/calendar/as;->y:J

    .line 846
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/android/calendar/as;->B:J

    .line 847
    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/android/calendar/as;->G:Z

    .line 848
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 849
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 852
    cmp-long v16, v2, v12

    if-nez v16, :cond_1

    cmp-long v4, v4, v10

    if-nez v4, :cond_1

    if-ne v6, v14, :cond_1

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v8, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 855
    const-string v2, "dtstart"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 856
    const-string v2, "dtend"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 857
    const-string v2, "duration"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 858
    const-string v2, "allDay"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 859
    const-string v2, "rrule"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 860
    const-string v2, "eventTimezone"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 864
    :cond_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 875
    const/4 v4, 0x3

    move/from16 v0, p4

    if-ne v0, v4, :cond_0

    .line 876
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/calendar/as;->y:J

    .line 877
    cmp-long v4, v2, v12

    if-eqz v4, :cond_3

    .line 879
    sub-long v2, v12, v2

    .line 880
    add-long/2addr v2, v10

    .line 900
    :goto_1
    if-eqz v14, :cond_2

    .line 901
    new-instance v4, Landroid/text/format/Time;

    const-string v5, "UTC"

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 902
    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 903
    const/4 v2, 0x0

    iput v2, v4, Landroid/text/format/Time;->hour:I

    .line 904
    const/4 v2, 0x0

    iput v2, v4, Landroid/text/format/Time;->minute:I

    .line 905
    const/4 v2, 0x0

    iput v2, v4, Landroid/text/format/Time;->second:I

    .line 906
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 908
    :cond_2
    const-string v4, "dtstart"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 881
    :cond_3
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 883
    :try_start_0
    new-instance v4, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v4, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 884
    new-instance v3, Lcom/android/a/ab;

    invoke-direct {v3}, Lcom/android/a/ab;-><init>()V

    .line 888
    new-instance v5, Lcom/android/a/ad;

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v9, v2, v6, v7}, Lcom/android/a/ad;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    invoke-virtual {v4, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 891
    iput-boolean v14, v4, Landroid/text/format/Time;->allDay:Z

    .line 892
    const-wide/16 v6, 0x0

    const-wide/16 v8, -0x1

    invoke-virtual/range {v3 .. v9}, Lcom/android/a/ab;->a(Landroid/text/format/Time;Lcom/android/a/ad;JJ)[J

    move-result-object v2

    .line 893
    array-length v3, v2

    if-lez v3, :cond_4

    .line 894
    const/4 v3, 0x0

    aget-wide v2, v2, v3
    :try_end_0
    .catch Lcom/android/a/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 896
    :catch_0
    move-exception v2

    .line 897
    sget-object v3, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v4, "RecurrenceProcessor error "

    invoke-static {v3, v4, v2}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    move-wide v2, v10

    goto :goto_1
.end method

.method public a(Lcom/android/calendar/as;Lcom/android/calendar/as;I)Z
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 354
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;Lcom/android/calendar/event/ay;)Z

    move-result v0

    return v0
.end method

.method public a(Lcom/android/calendar/as;Lcom/android/calendar/as;ILandroid/app/Activity;Lcom/android/calendar/event/ay;)Z
    .locals 19

    .prologue
    .line 360
    const/4 v10, 0x0

    .line 367
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/calendar/event/av;->d:Z

    if-nez v4, :cond_0

    .line 371
    const/4 v4, 0x0

    .line 772
    :goto_0
    return v4

    .line 376
    :cond_0
    if-nez p1, :cond_1

    .line 377
    sget-object v4, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v5, "Attempted to save null model."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v4, 0x0

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/as;->a()Z

    move-result v4

    if-nez v4, :cond_2

    .line 381
    sget-object v4, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v5, "Attempted to save invalid model."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v4, 0x0

    goto :goto_0

    .line 384
    :cond_2
    if-eqz p2, :cond_3

    invoke-static/range {p1 .. p2}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 385
    sget-object v4, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v5, "Attempted to update existing event but models didn\'t refer to the same event."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/4 v4, 0x0

    goto :goto_0

    .line 389
    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual/range {p1 .. p2}, Lcom/android/calendar/as;->a(Lcom/android/calendar/as;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 390
    if-eqz p5, :cond_4

    .line 391
    const/4 v4, 0x0

    goto :goto_0

    .line 395
    :cond_4
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 396
    const/4 v6, -0x1

    .line 398
    invoke-virtual/range {p0 .. p1}, Lcom/android/calendar/event/av;->e(Lcom/android/calendar/as;)Landroid/content/ContentValues;

    move-result-object v14

    .line 400
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v4, :cond_5

    if-nez p2, :cond_5

    .line 401
    sget-object v4, Lcom/android/calendar/event/av;->i:Ljava/lang/String;

    const-string v5, "Existing event but no originalModel provided. Aborting save."

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v4, 0x0

    goto :goto_0

    .line 404
    :cond_5
    const/4 v4, 0x0

    .line 405
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    if-eqz v7, :cond_36

    move-object/from16 v0, p1

    iget-boolean v7, v0, Lcom/android/calendar/as;->aq:Z

    if-nez v7, :cond_36

    .line 406
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    move-object v11, v4

    .line 409
    :goto_1
    const-string v4, "sticker_type"

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/as;->ar:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 410
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/as;->ar:J

    const-wide/16 v12, 0x0

    cmp-long v4, v8, v12

    if-lez v4, :cond_6

    .line 411
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/calendar/as;->ar:J

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/android/calendar/event/av;->c(J)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 412
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    sget-object v4, Lcom/android/calendar/hj;->y:Ljava/lang/String;

    sget-object v7, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v4, v7}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 418
    :cond_6
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 419
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 420
    const-string v9, "hasAlarm"

    if-lez v7, :cond_13

    const/4 v4, 0x1

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v14, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 422
    if-lez v7, :cond_7

    .line 423
    sget-object v4, Lcom/android/calendar/hj;->C:Ljava/lang/String;

    sget-object v7, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v4, v7}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 426
    :cond_7
    if-nez v11, :cond_14

    .line 428
    const-string v4, "hasAttendeeData"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 429
    const-string v4, "eventStatus"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 430
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 431
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 434
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    const/4 v10, 0x1

    move v13, v6

    .line 529
    :goto_3
    const/4 v4, -0x1

    if-eq v13, v4, :cond_20

    const/4 v4, 0x1

    move v12, v4

    .line 531
    :goto_4
    if-eqz p2, :cond_21

    .line 532
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 537
    :goto_5
    if-eqz v12, :cond_22

    .line 538
    const-string v4, "com.android.sharepoint"

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 539
    invoke-static {v5, v13, v8, v9, v10}, Lcom/android/calendar/event/av;->a(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    .line 549
    :cond_8
    :goto_6
    if-eqz p1, :cond_23

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    if-lez v4, :cond_23

    .line 550
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/calendar/as;->W:Z

    .line 552
    sget-object v4, Lcom/android/calendar/hj;->z:Ljava/lang/String;

    sget-object v6, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v4, v6}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 558
    :goto_7
    const/4 v4, 0x0

    .line 559
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    if-eqz v6, :cond_9

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    const-string v7, "com.android.exchange"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 560
    const/4 v4, 0x1

    .line 562
    :cond_9
    if-eqz v4, :cond_a

    .line 563
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/calendar/as;->W:Z

    .line 568
    :cond_a
    if-eqz p1, :cond_24

    if-eqz p2, :cond_24

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->size()I

    move-result v6

    if-eq v4, v6, :cond_24

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_24

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    const-string v6, "com.osp.app.signin"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_24

    .line 572
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/calendar/as;->W:Z

    .line 578
    :cond_b
    :goto_8
    move-object/from16 v0, p1

    iget-boolean v8, v0, Lcom/android/calendar/as;->W:Z

    .line 579
    if-nez v8, :cond_c

    .line 580
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 584
    :cond_c
    if-eqz v8, :cond_26

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/as;->Y:I

    const/4 v6, -0x1

    if-ne v4, v6, :cond_26

    .line 585
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 587
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 588
    if-eqz v6, :cond_10

    .line 589
    const-string v4, "attendeeEmail"

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    const-string v4, "attendeeRelationship"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 591
    const-string v4, "attendeeType"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 592
    const/4 v4, 0x1

    .line 593
    if-eqz p2, :cond_d

    .line 594
    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/as;->X:I

    .line 598
    :cond_d
    const-string v7, "calendar.google.com"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 599
    const/4 v4, 0x0

    .line 601
    :cond_e
    const-string v6, "attendeeStatus"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v14, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 602
    const-string v4, "event_id"

    if-eqz v12, :cond_25

    const-wide/16 v6, -0x1

    :goto_9
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 604
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 605
    if-eqz v12, :cond_f

    .line 606
    const-string v6, "event_id"

    invoke-virtual {v4, v6, v13}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 608
    :cond_f
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    :cond_10
    :goto_a
    if-eqz v8, :cond_2f

    if-nez v12, :cond_11

    if-eqz v11, :cond_2f

    .line 628
    :cond_11
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/as;->c()Ljava/lang/String;

    move-result-object v6

    .line 630
    if-eqz p2, :cond_27

    .line 631
    invoke-virtual/range {p2 .. p2}, Lcom/android/calendar/as;->c()Ljava/lang/String;

    move-result-object v4

    .line 637
    :goto_b
    if-nez v12, :cond_12

    invoke-static {v4, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2f

    .line 641
    :cond_12
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    .line 642
    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    .line 646
    if-eqz v11, :cond_28

    invoke-static {v11}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    move-wide v8, v6

    .line 651
    :goto_c
    if-nez v12, :cond_2d

    if-eqz p2, :cond_2d

    .line 652
    invoke-virtual {v15}, Ljava/util/LinkedList;->clear()V

    .line 653
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    .line 654
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 655
    invoke-virtual {v10, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_29

    .line 657
    invoke-virtual {v10, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_d

    .line 420
    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 436
    :cond_14
    if-eqz p2, :cond_1f

    .line 437
    const-string v4, "organizer"

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 441
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v14, v3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;Landroid/content/ContentValues;I)V

    .line 442
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v13, v6

    goto/16 :goto_3

    .line 443
    :cond_15
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_16

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 446
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v13, v6

    goto/16 :goto_3

    .line 447
    :cond_16
    const/4 v4, 0x1

    move/from16 v0, p3

    if-ne v0, v4, :cond_18

    .line 450
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/as;->x:J

    .line 451
    const-string v4, "original_sync_id"

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v14, v4, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v4, "originalInstanceTime"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 453
    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/android/calendar/as;->G:Z

    .line 454
    const-string v6, "originalAllDay"

    if-eqz v4, :cond_17

    const/4 v4, 0x1

    :goto_e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v14, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455
    const-string v4, "eventStatus"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 458
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 461
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    const/4 v10, 0x1

    move v13, v6

    .line 464
    goto/16 :goto_3

    .line 454
    :cond_17
    const/4 v4, 0x0

    goto :goto_e

    .line 464
    :cond_18
    const/4 v4, 0x2

    move/from16 v0, p3

    if-ne v0, v4, :cond_1d

    .line 465
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 470
    invoke-static/range {p1 .. p2}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;Lcom/android/calendar/as;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 471
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    :goto_f
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 478
    const-string v6, "eventStatus"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479
    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    :goto_10
    const/4 v10, 0x1

    move v13, v4

    .line 507
    goto/16 :goto_3

    .line 475
    :cond_19
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/as;->x:J

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1, v6, v7}, Lcom/android/calendar/event/av;->a(Ljava/util/ArrayList;Lcom/android/calendar/as;J)Ljava/lang/String;

    goto :goto_f

    .line 482
    :cond_1a
    invoke-static/range {p1 .. p2}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;Lcom/android/calendar/as;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 483
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v14, v3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;Landroid/content/ContentValues;I)V

    .line 484
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 486
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v6

    .line 487
    goto :goto_10

    .line 494
    :cond_1b
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/as;->x:J

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v5, v1, v6, v7}, Lcom/android/calendar/event/av;->a(Ljava/util/ArrayList;Lcom/android/calendar/as;J)Ljava/lang/String;

    move-result-object v4

    .line 495
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 496
    const-string v6, "rrule"

    invoke-virtual {v14, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_1c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 501
    const-string v6, "eventStatus"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 502
    sget-object v6, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 508
    :cond_1d
    const/4 v4, 0x3

    move/from16 v0, p3

    if-ne v0, v4, :cond_1f

    .line 511
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1e

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 515
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 518
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    const/4 v10, 0x1

    move v13, v6

    goto/16 :goto_3

    .line 522
    :cond_1e
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v14, v3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Lcom/android/calendar/as;Landroid/content/ContentValues;I)V

    .line 523
    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1f
    move v13, v6

    goto/16 :goto_3

    .line 529
    :cond_20
    const/4 v4, 0x0

    move v12, v4

    goto/16 :goto_4

    .line 534
    :cond_21
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_5

    .line 542
    :cond_22
    if-eqz v11, :cond_8

    .line 543
    invoke-static {v11}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    .line 544
    invoke-static/range {v5 .. v10}, Lcom/android/calendar/event/av;->a(Ljava/util/ArrayList;JLjava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    goto/16 :goto_6

    .line 554
    :cond_23
    const/4 v4, 0x0

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/calendar/as;->W:Z

    goto/16 :goto_7

    .line 573
    :cond_24
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v4

    if-eqz v4, :cond_b

    if-eqz p1, :cond_b

    if-eqz p2, :cond_b

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/android/calendar/as;->ax:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->size()I

    move-result v6

    if-eq v4, v6, :cond_b

    .line 575
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/calendar/as;->W:Z

    goto/16 :goto_8

    .line 602
    :cond_25
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/calendar/as;->b:J

    goto/16 :goto_9

    .line 610
    :cond_26
    if-eqz v8, :cond_10

    if-eqz p2, :cond_10

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/as;->X:I

    move-object/from16 v0, p2

    iget v6, v0, Lcom/android/calendar/as;->X:I

    if-eq v4, v6, :cond_10

    move-object/from16 v0, p1

    iget v4, v0, Lcom/android/calendar/as;->Y:I

    const/4 v6, -0x1

    if-eq v4, v6, :cond_10

    .line 616
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/as;->Y:I

    int-to-long v6, v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 618
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 619
    const-string v6, "attendeeStatus"

    move-object/from16 v0, p1

    iget v7, v0, Lcom/android/calendar/as;->X:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 620
    const-string v6, "event_id"

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v14, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 621
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 622
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    .line 633
    :cond_27
    const-string v4, ""

    goto/16 :goto_b

    .line 646
    :cond_28
    const-wide/16 v6, -0x1

    move-wide v8, v6

    goto/16 :goto_c

    .line 660
    :cond_29
    invoke-virtual {v15, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d

    .line 665
    :cond_2a
    invoke-virtual {v15}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-lez v4, :cond_2d

    .line 666
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v16

    .line 668
    invoke-virtual {v15}, Ljava/util/LinkedList;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 669
    const/4 v4, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v17, v4

    .line 670
    const/4 v4, 0x1

    .line 671
    new-instance v18, Ljava/lang/StringBuilder;

    const-string v6, "event_id=? AND attendeeEmail IN ("

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 672
    invoke-virtual {v15}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v6, v4

    :goto_11
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 673
    const/4 v7, 0x1

    if-le v6, v7, :cond_2b

    .line 674
    const/16 v7, 0x2c

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 676
    :cond_2b
    const/16 v7, 0x3f

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 677
    add-int/lit8 v7, v6, 0x1

    aput-object v4, v17, v6

    move v6, v7

    .line 678
    goto :goto_11

    .line 679
    :cond_2c
    const/16 v4, 0x29

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 680
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 681
    invoke-virtual/range {v16 .. v16}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    :cond_2d
    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2f

    .line 687
    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_12
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/calendar/at;

    .line 688
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 689
    const-string v7, "attendeeName"

    iget-object v10, v4, Lcom/android/calendar/at;->a:Ljava/lang/String;

    invoke-virtual {v14, v7, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    const-string v7, "attendeeEmail"

    iget-object v4, v4, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {v14, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const-string v4, "attendeeRelationship"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 692
    const-string v4, "attendeeType"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 693
    const-string v4, "attendeeStatus"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 695
    if-eqz v12, :cond_2e

    .line 696
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 698
    const-string v7, "event_id"

    invoke-virtual {v4, v7, v13}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 704
    :goto_13
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 700
    :cond_2e
    const-string v4, "event_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v14, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 701
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    goto :goto_13

    .line 711
    :cond_2f
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_34

    .line 712
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 713
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 714
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x32

    invoke-virtual {v6, v7, v8, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 715
    const-string v6, "map"

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v14, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 718
    if-eqz v11, :cond_33

    if-eqz p4, :cond_33

    if-nez v12, :cond_33

    .line 719
    invoke-static {v11}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    long-to-int v4, v6

    .line 720
    const-string v6, "event_id"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v14, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 722
    invoke-virtual/range {p4 .. p4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    sget-object v8, Lcom/android/calendar/event/EditEventActivity;->d:[Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "event_id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 724
    if-eqz v6, :cond_32

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_32

    .line 725
    sget-object v4, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v8, v7

    invoke-static {v4, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 727
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 732
    :goto_14
    if-eqz v6, :cond_30

    .line 733
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 742
    :cond_30
    :goto_15
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    :cond_31
    :goto_16
    const-string v4, "availabilityStatus"

    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/calendar/as;->J:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 766
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/event/av;->k:Lcom/android/calendar/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/av;->k:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v8

    const/4 v9, 0x0

    const-string v10, "com.android.calendar"

    const-wide/16 v12, 0x0

    move-object v11, v5

    invoke-virtual/range {v7 .. v13}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 770
    sget-object v4, Lcom/android/calendar/hj;->x:Ljava/lang/String;

    sget-object v5, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    const-string v6, "EVENT"

    invoke-static {v4, v5, v6}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 772
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 729
    :cond_32
    sget-object v4, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    goto :goto_14

    .line 737
    :cond_33
    sget-object v4, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    .line 739
    const-string v6, "event_id"

    invoke-virtual {v4, v6, v13}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_15

    .line 747
    :cond_34
    if-eqz v11, :cond_31

    if-eqz p4, :cond_31

    if-nez v12, :cond_31

    .line 748
    invoke-static {v11}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    long-to-int v4, v6

    .line 749
    invoke-virtual/range {p4 .. p4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    sget-object v8, Lcom/android/calendar/event/EditEventActivity;->d:[Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "event_id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 751
    if-eqz v4, :cond_35

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_35

    .line 752
    sget-object v6, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-long v8, v7

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 754
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    .line 755
    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    :cond_35
    if-eqz v4, :cond_31

    .line 758
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_16

    :cond_36
    move-object v11, v4

    goto/16 :goto_1
.end method

.method e(Lcom/android/calendar/as;)Landroid/content/ContentValues;
    .locals 20

    .prologue
    .line 1597
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 1598
    move-object/from16 v0, p1

    iget-boolean v9, v0, Lcom/android/calendar/as;->G:Z

    .line 1599
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1600
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 1601
    if-nez v2, :cond_0

    .line 1602
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    .line 1604
    :cond_0
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1605
    new-instance v11, Landroid/text/format/Time;

    invoke-direct {v11, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1607
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/as;->y:J

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1608
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/calendar/as;->B:J

    invoke-virtual {v11, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1610
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 1614
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/calendar/as;->c:J

    .line 1615
    if-eqz v9, :cond_6

    .line 1618
    const-string v6, "UTC"

    .line 1619
    const/4 v2, 0x0

    iput v2, v3, Landroid/text/format/Time;->hour:I

    .line 1620
    const/4 v2, 0x0

    iput v2, v3, Landroid/text/format/Time;->minute:I

    .line 1621
    const/4 v2, 0x0

    iput v2, v3, Landroid/text/format/Time;->second:I

    .line 1622
    iput-object v6, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1623
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 1625
    const/4 v2, 0x0

    iput v2, v11, Landroid/text/format/Time;->hour:I

    .line 1626
    const/4 v2, 0x0

    iput v2, v11, Landroid/text/format/Time;->minute:I

    .line 1627
    const/4 v2, 0x0

    iput v2, v11, Landroid/text/format/Time;->second:I

    .line 1628
    iput-object v6, v11, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1629
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 1630
    const-wide/32 v16, 0x5265c00

    add-long v16, v16, v4

    cmp-long v7, v2, v16

    if-gez v7, :cond_12

    .line 1633
    const-wide/32 v2, 0x5265c00

    add-long/2addr v2, v4

    move-wide/from16 v18, v2

    move-wide v2, v4

    move-wide/from16 v4, v18

    .line 1640
    :goto_0
    move-object/from16 v0, p1

    iget v7, v0, Lcom/android/calendar/as;->i:I

    const/4 v11, -0x1

    if-le v7, v11, :cond_7

    .line 1641
    const-string v7, "eventColor_index"

    move-object/from16 v0, p1

    iget v11, v0, Lcom/android/calendar/as;->i:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v12, v7, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1647
    :cond_1
    :goto_1
    const-string v7, "calendar_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v12, v7, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1648
    const-string v7, "eventTimezone"

    invoke-virtual {v12, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1649
    const-string v6, "title"

    invoke-virtual {v12, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    const-string v7, "allDay"

    if-eqz v9, :cond_8

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v12, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1651
    const-string v6, "dtstart"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1652
    const-string v2, "rrule"

    invoke-virtual {v12, v2, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    const-string v3, ""

    .line 1656
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1657
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v12, v1}, Lcom/android/calendar/event/av;->a(Landroid/content/ContentValues;Lcom/android/calendar/as;)V

    .line 1659
    new-instance v2, Lcom/android/a/c;

    invoke-direct {v2}, Lcom/android/a/c;-><init>()V

    .line 1660
    invoke-virtual {v2, v10}, Lcom/android/a/c;->a(Ljava/lang/String;)V

    .line 1662
    iget v2, v2, Lcom/android/a/c;->b:I

    packed-switch v2, :pswitch_data_0

    :goto_3
    move-object v2, v3

    .line 1683
    :goto_4
    sget-object v3, Lcom/android/calendar/hj;->H:Ljava/lang/String;

    sget-object v4, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v3, v4, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 1685
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1686
    const-string v2, "description"

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1688
    sget-object v2, Lcom/android/calendar/hj;->B:Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 1692
    :goto_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 1693
    const-string v2, "eventLocation"

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1695
    sget-object v2, Lcom/android/calendar/hj;->A:Ljava/lang/String;

    sget-object v3, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 1699
    :goto_6
    const-string v3, "availability"

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/as;->I:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1701
    sget-object v3, Lcom/android/calendar/hj;->F:Ljava/lang/String;

    sget-object v4, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/as;->I:Z

    if-eqz v2, :cond_d

    const-string v2, "Available"

    :goto_8
    invoke-static {v3, v4, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 1704
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/as;->J:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 1705
    const-string v2, "availabilityStatus"

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/as;->J:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1708
    :cond_2
    const-string v3, "hasAttendeeData"

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/as;->W:Z

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1710
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/calendar/as;->au:I

    .line 1711
    if-lez v2, :cond_3

    .line 1714
    add-int/lit8 v2, v2, 0x1

    .line 1716
    :cond_3
    const-string v3, "accessLevel"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1718
    sget-object v3, Lcom/android/calendar/hj;->G:Ljava/lang/String;

    sget-object v4, Lcom/android/calendar/event/av;->j:Landroid/content/Context;

    const/4 v5, 0x3

    if-ne v2, v5, :cond_f

    const-string v2, "Public"

    :goto_a
    invoke-static {v3, v4, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 1721
    const-string v2, "latitude"

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/as;->ai:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1722
    const-string v2, "longitude"

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/calendar/as;->aj:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1725
    invoke-static {}, Lcom/android/calendar/event/av;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1726
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/as;->ay:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1727
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/android/calendar/as;->ay:Z

    .line 1729
    :cond_4
    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    .line 1730
    sget v2, Lcom/android/calendar/event/av;->b:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_5

    .line 1731
    const-string v3, "setLunar"

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/android/calendar/as;->ay:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_b
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1734
    :cond_5
    return-object v12

    .line 1636
    :cond_6
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 1637
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    move-wide/from16 v18, v6

    move-object v6, v2

    move-wide/from16 v2, v18

    goto/16 :goto_0

    .line 1642
    :cond_7
    const/4 v7, -0x1

    move-object/from16 v0, p1

    iget v11, v0, Lcom/android/calendar/as;->i:I

    if-ne v7, v11, :cond_1

    const-string v7, "com.google"

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1643
    const-string v7, "eventColor"

    invoke-virtual {v12, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1644
    const-string v7, "eventColor_index"

    invoke-virtual {v12, v7}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1650
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 1664
    :pswitch_0
    const-string v2, "Daily"

    goto/16 :goto_4

    .line 1667
    :pswitch_1
    const-string v2, "Weekly"

    goto/16 :goto_4

    .line 1670
    :pswitch_2
    const-string v2, "Monthly"

    goto/16 :goto_4

    .line 1673
    :pswitch_3
    const-string v2, "Yearly"

    goto/16 :goto_4

    .line 1679
    :cond_9
    const-string v6, "duration"

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    const-string v2, "dtend"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v12, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    .line 1690
    :cond_a
    const-string v3, "description"

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1697
    :cond_b
    const-string v3, "eventLocation"

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1699
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 1701
    :cond_d
    const-string v2, ""

    goto/16 :goto_8

    .line 1708
    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 1718
    :cond_f
    const/4 v5, 0x2

    if-ne v2, v5, :cond_10

    const-string v2, "Private"

    goto/16 :goto_a

    :cond_10
    const-string v2, ""

    goto/16 :goto_a

    .line 1731
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_12
    move-wide/from16 v18, v2

    move-wide v2, v4

    move-wide/from16 v4, v18

    goto/16 :goto_0

    .line 1662
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
