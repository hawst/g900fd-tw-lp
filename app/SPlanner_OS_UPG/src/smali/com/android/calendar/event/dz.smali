.class Lcom/android/calendar/event/dz;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 5837
    iput-object p1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5840
    const v0, 0x2000006

    .line 5841
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v1

    const v4, 0x7f120280

    if-ne v1, v4, :cond_1

    .line 5842
    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 5843
    if-eqz p2, :cond_2

    .line 5844
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v1

    iput-boolean v2, v1, Lcom/android/calendar/as;->U:Z

    .line 5845
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v1

    const/4 v4, 0x0

    iput-object v4, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 5846
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->aa(Lcom/android/calendar/event/ay;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 5855
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->ad(Lcom/android/calendar/event/ay;)Landroid/widget/Spinner;

    move-result-object v4

    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/calendar/as;->U:Z

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 5856
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->f(Lcom/android/calendar/event/ay;)Landroid/widget/Button;

    move-result-object v4

    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/calendar/as;->U:Z

    if-nez v1, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 5857
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->aa(Lcom/android/calendar/event/ay;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v4

    iget-boolean v4, v4, Lcom/android/calendar/as;->U:Z

    if-nez v4, :cond_5

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 5859
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 5860
    iget-object v0, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 5861
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5862
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->M:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 5865
    :cond_1
    return-void

    .line 5848
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v1

    iput-boolean v3, v1, Lcom/android/calendar/as;->U:Z

    .line 5849
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->l(Lcom/android/calendar/event/ay;)Lcom/android/a/c;

    move-result-object v1

    iget-object v4, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/android/a/c;->c:Ljava/lang/String;

    .line 5851
    iget-object v1, p0, Lcom/android/calendar/event/dz;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->ac(Lcom/android/calendar/event/ay;)I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 5852
    const v0, 0x2000005

    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 5855
    goto :goto_1

    :cond_4
    move v1, v3

    .line 5856
    goto :goto_2

    :cond_5
    move v2, v3

    .line 5857
    goto :goto_3
.end method
