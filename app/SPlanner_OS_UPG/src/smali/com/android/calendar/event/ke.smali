.class public Lcom/android/calendar/event/ke;
.super Landroid/widget/BaseAdapter;
.source "SelectMapChinaActivity.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;

.field private c:Landroid/view/LayoutInflater;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 989
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 990
    iput-object p1, p0, Lcom/android/calendar/event/ke;->a:Landroid/content/Context;

    .line 991
    iput-object p2, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    .line 992
    iput p3, p0, Lcom/android/calendar/event/ke;->d:I

    .line 993
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 997
    iget-object v0, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1007
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1012
    .line 1013
    if-nez p2, :cond_0

    .line 1014
    new-instance v1, Lcom/android/calendar/event/kf;

    invoke-direct {v1}, Lcom/android/calendar/event/kf;-><init>()V

    .line 1015
    iget-object v0, p0, Lcom/android/calendar/event/ke;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/calendar/event/ke;->c:Landroid/view/LayoutInflater;

    .line 1017
    iget-object v0, p0, Lcom/android/calendar/event/ke;->c:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/android/calendar/event/ke;->d:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1018
    const v0, 0x7f120060

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/event/kf;->a:Landroid/widget/TextView;

    .line 1019
    const v0, 0x7f12029b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/event/kf;->b:Landroid/widget/TextView;

    .line 1020
    const v0, 0x7f12029c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/android/calendar/event/kf;->c:Landroid/widget/TextView;

    .line 1021
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1025
    :goto_0
    iget-object v2, v1, Lcom/android/calendar/event/kf;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/core/PoiItem;

    invoke-virtual {v0}, Lcom/amap/api/services/core/PoiItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1026
    iget-object v2, v1, Lcom/android/calendar/event/kf;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/core/PoiItem;

    invoke-virtual {v0}, Lcom/amap/api/services/core/PoiItem;->getTel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1027
    iget-object v1, v1, Lcom/android/calendar/event/kf;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/calendar/event/ke;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/services/core/PoiItem;

    invoke-virtual {v0}, Lcom/amap/api/services/core/PoiItem;->getSnippet()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1028
    return-object p2

    .line 1023
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kf;

    move-object v1, v0

    goto :goto_0
.end method
