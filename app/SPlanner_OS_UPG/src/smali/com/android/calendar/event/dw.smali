.class Lcom/android/calendar/event/dw;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 5743
    iput-object p1, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 5746
    move v2, v3

    move v0, v3

    .line 5748
    :goto_0
    const/4 v4, 0x7

    if-ge v2, v4, :cond_3

    .line 5749
    iget-object v4, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->Y(Lcom/android/calendar/event/ay;)[Landroid/widget/ToggleButton;

    move-result-object v4

    aget-object v4, v4, v2

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5750
    iget-object v4, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->Y(Lcom/android/calendar/event/ay;)[Landroid/widget/ToggleButton;

    move-result-object v4

    aget-object v4, v4, v2

    invoke-virtual {v4}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5751
    iget-object v4, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->O:[Z

    aput-boolean v1, v4, v2

    .line 5757
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->O:[Z

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_1

    move v0, v1

    .line 5748
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5753
    :cond_2
    iget-object v4, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->O:[Z

    aput-boolean v3, v4, v2

    goto :goto_1

    .line 5762
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->Z(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 5764
    iget-object v1, p0, Lcom/android/calendar/event/dw;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->Z(Lcom/android/calendar/event/ay;)Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 5766
    :cond_4
    return-void
.end method
