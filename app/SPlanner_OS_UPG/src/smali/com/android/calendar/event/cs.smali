.class Lcom/android/calendar/event/cs;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 4456
    iput-object p1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const v4, 0x7f0f024b

    const/4 v3, 0x0

    .line 4459
    if-eqz p2, :cond_1

    .line 4460
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    .line 4462
    if-eqz v0, :cond_1

    .line 4463
    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->o(Lcom/android/calendar/event/ay;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/event/ay;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4466
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4467
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4487
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 4488
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Z)Z

    .line 4491
    :cond_1
    return-void

    .line 4470
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->o(Lcom/android/calendar/event/ay;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v2, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4473
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4474
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4478
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/ex/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4481
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->o(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4482
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 4484
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/cs;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0
.end method
