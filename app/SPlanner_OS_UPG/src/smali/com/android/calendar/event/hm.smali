.class public Lcom/android/calendar/event/hm;
.super Ljava/lang/Object;
.source "EventViewUtils.java"


# static fields
.field private static a:Landroid/app/Activity;

.field private static b:I

.field private static c:Landroid/app/AlertDialog;

.field private static d:Ljava/util/ArrayList;

.field private static e:Ljava/lang/String;

.field private static f:I

.field private static g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/event/hm;->b:I

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    .line 68
    const-string v0, "15"

    sput-object v0, Lcom/android/calendar/event/hm;->e:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/event/hm;->f:I

    .line 70
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/event/hm;->g:Z

    return-void
.end method

.method public static a(JLandroid/database/Cursor;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 985
    .line 987
    if-eqz p2, :cond_1

    .line 988
    const-string v0, "_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 990
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 991
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 992
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v3, p0, v4

    if-nez v3, :cond_0

    .line 999
    :goto_1
    return v0

    .line 995
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 999
    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;I)I
    .locals 3

    .prologue
    .line 140
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 141
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 143
    const-string v0, "EventViewUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find minutes ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") in list"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v0, 0x0

    .line 146
    :cond_0
    return v0
.end method

.method static synthetic a(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 62
    sput-object p0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    return-object p0
.end method

.method static synthetic a(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/hm;->b(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;IZ)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 90
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 95
    rem-int/lit8 v2, p1, 0x3c

    if-eqz v2, :cond_2

    .line 97
    if-ne p1, v3, :cond_1

    .line 98
    const v2, 0x7f0f0193

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 125
    :goto_0
    if-eq p1, v3, :cond_0

    .line 126
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    :cond_0
    return-object v0

    .line 100
    :cond_1
    const v2, 0x7f0f019a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 102
    :cond_2
    rem-int/lit16 v2, p1, 0x5a0

    if-eqz v2, :cond_4

    .line 103
    div-int/lit8 p1, p1, 0x3c

    .line 104
    if-ne p1, v3, :cond_3

    .line 105
    const v2, 0x7f0f0192

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_3
    const v2, 0x7f0f0199

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 109
    :cond_4
    rem-int/lit16 v2, p1, 0x2760

    if-eqz v2, :cond_6

    .line 110
    div-int/lit16 p1, p1, 0x5a0

    .line 111
    if-ne p1, v3, :cond_5

    .line 112
    const v2, 0x7f0f0191

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_5
    const v2, 0x7f0f0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 117
    :cond_6
    div-int/lit16 p1, p1, 0x2760

    .line 118
    if-ne p1, v3, :cond_7

    .line 119
    const v2, 0x7f0f0194

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_7
    const v2, 0x7f0f019b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1071
    const-string v0, ""

    .line 1074
    if-nez p0, :cond_1

    .line 1096
    :cond_0
    :goto_0
    return-object v0

    .line 1079
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const-string v3, "data1 =? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1084
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1085
    :cond_2
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1091
    :goto_1
    if-eqz v1, :cond_0

    .line 1092
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1087
    :cond_3
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1088
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 1091
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    if-eqz v1, :cond_4

    .line 1092
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1091
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/res/Resources;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/16 v8, 0x11

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 198
    const/4 v0, 0x0

    .line 200
    const/16 v1, 0xd

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    .line 203
    new-array v4, v8, [Ljava/lang/String;

    move v1, v2

    .line 205
    :goto_0
    if-ge v1, v8, :cond_0

    .line 206
    packed-switch v1, :pswitch_data_0

    .line 254
    :goto_1
    aput-object v0, v4, v1

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :pswitch_0
    const v0, 0x7f0f0197

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 212
    :pswitch_1
    const v0, 0x7f0f0193

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 222
    :pswitch_2
    const v0, 0x7f0f019a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 223
    new-array v5, v7, [Ljava/lang/Object;

    aget v6, v3, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 227
    :pswitch_3
    const v0, 0x7f0f0192

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 233
    :pswitch_4
    const v0, 0x7f0f0199

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 234
    new-array v5, v7, [Ljava/lang/Object;

    aget v6, v3, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 238
    :pswitch_5
    const v0, 0x7f0f0191

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 242
    :pswitch_6
    const v0, 0x7f0f0195

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 246
    :pswitch_7
    const v0, 0x7f0f0194

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 250
    :pswitch_8
    const v0, 0x7f0f0196

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 258
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 259
    return-object v0

    .line 200
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x5
        0xa
        0xf
        0x14
        0x19
        0x1e
        0x2d
        0x1
        0x2
        0x3
        0xc
    .end array-data

    .line 206
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 1062
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1063
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1064
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1067
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 178
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 179
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 181
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 182
    const v1, 0x7f12014b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 183
    const v5, 0x7f12014c

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 184
    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 185
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    .line 186
    if-ltz v1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    if-ltz v5, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v5, v0, :cond_1

    .line 180
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 190
    :cond_1
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 191
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 192
    invoke-static {v1, v0}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 194
    :cond_2
    return-object v4
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 264
    sget-object v0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 265
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    .line 267
    :cond_0
    return-void
.end method

.method public static a(I)V
    .locals 0

    .prologue
    .line 973
    sput p0, Lcom/android/calendar/event/hm;->f:I

    .line 974
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 951
    if-eqz p1, :cond_0

    .line 952
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 954
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 956
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 275
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 276
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 295
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-static {p0, p3, v0}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    .line 284
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 285
    :goto_1
    if-ge v1, v3, :cond_2

    .line 286
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p3, v0, :cond_1

    .line 287
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 288
    invoke-virtual {p2, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 285
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 293
    :cond_2
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    invoke-virtual {p2, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 307
    const-string v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 308
    array-length v0, v1

    new-array v2, v0, [I

    .line 310
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 312
    :try_start_0
    aget-object v3, v1, v0

    const/16 v4, 0xa

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v2, v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :catch_0
    move-exception v2

    .line 314
    const-string v2, "EventViewUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad allowed-strings list: \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v0, v1, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' in \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/calendar/ey;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    return-void

    .line 322
    :cond_1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    .line 323
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 326
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_2

    .line 327
    aget v4, v2, v0

    if-ne v3, v4, :cond_4

    .line 331
    :cond_2
    if-gez v0, :cond_3

    .line 332
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 333
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 322
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 326
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method public static a(Z)V
    .locals 0

    .prologue
    .line 981
    sput-boolean p0, Lcom/android/calendar/event/hm;->g:Z

    .line 982
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/android/calendar/au;ILandroid/widget/AdapterView$OnItemSelectedListener;Z)Z
    .locals 9

    .prologue
    .line 379
    sput-object p0, Lcom/android/calendar/event/hm;->a:Landroid/app/Activity;

    .line 381
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    move/from16 v0, p9

    if-lt v1, v0, :cond_0

    .line 382
    const/4 v1, 0x0

    .line 937
    :goto_0
    return v1

    .line 385
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 386
    const v1, 0x7f1200f8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 387
    const v3, 0x7f040043

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Landroid/widget/LinearLayout;

    .line 390
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 391
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 393
    if-eqz p11, :cond_1

    .line 394
    sget-object v1, Lcom/android/calendar/event/hm;->a:Landroid/app/Activity;

    const v2, 0x7f05000c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 395
    new-instance v2, Landroid/view/animation/interpolator/SineInOut60;

    invoke-direct {v2}, Landroid/view/animation/interpolator/SineInOut60;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 396
    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 400
    :cond_1
    const v1, 0x7f12014e

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 401
    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    const v1, 0x7f12014b

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    .line 409
    const v1, 0x7f12014c

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/Spinner;

    .line 412
    invoke-virtual/range {p8 .. p8}, Lcom/android/calendar/au;->a()I

    move-result v1

    invoke-static {p4, v1}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;I)I

    move-result v5

    .line 414
    new-instance v1, Lcom/android/calendar/event/hn;

    invoke-direct {v1, v3}, Lcom/android/calendar/event/hn;-><init>(Landroid/widget/Spinner;)V

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 423
    new-instance v1, Lcom/android/calendar/event/ho;

    invoke-direct {v1, v3}, Lcom/android/calendar/event/ho;-><init>(Landroid/widget/Spinner;)V

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 436
    if-eqz p10, :cond_2

    .line 437
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setTag(Ljava/lang/Object;)V

    .line 438
    move-object/from16 v0, p10

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 914
    :goto_1
    invoke-virtual/range {p8 .. p8}, Lcom/android/calendar/au;->b()I

    move-result v1

    invoke-static {p6, v1}, Lcom/android/calendar/event/hm;->b(Ljava/util/ArrayList;I)I

    move-result v2

    .line 915
    const v1, 0x7f12014d

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 916
    move-object/from16 v0, p7

    invoke-static {p0, v8, v0, v2, v1}, Lcom/android/calendar/event/hm;->b(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;

    move-result-object v1

    .line 919
    if-eqz p10, :cond_3

    .line 920
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/Spinner;->setTag(Ljava/lang/Object;)V

    .line 921
    move-object/from16 v0, p10

    invoke-virtual {v8, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 935
    :goto_2
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 440
    :cond_2
    new-instance v1, Lcom/android/calendar/event/hp;

    move-object v2, p0

    move-object v4, p5

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/calendar/event/hp;-><init>(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILjava/util/ArrayList;)V

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_1

    .line 923
    :cond_3
    new-instance v2, Lcom/android/calendar/event/if;

    invoke-direct {v2, v1}, Lcom/android/calendar/event/if;-><init>(Lcom/android/calendar/gl;)V

    invoke-virtual {v8, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 942
    const-string v0, "my calendar"

    .line 943
    const-string v1, "local"

    .line 945
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 1004
    packed-switch p0, :pswitch_data_0

    .line 1015
    const/4 v0, 0x0

    .line 1017
    :goto_0
    return v0

    .line 1006
    :pswitch_0
    const/4 v0, 0x1

    .line 1007
    goto :goto_0

    .line 1009
    :pswitch_1
    const/4 v0, 0x4

    .line 1010
    goto :goto_0

    .line 1012
    :pswitch_2
    const/4 v0, 0x2

    .line 1013
    goto :goto_0

    .line 1004
    nop

    :pswitch_data_0
    .packed-switch 0x7f120148
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ljava/util/ArrayList;I)I
    .locals 2

    .prologue
    .line 160
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 161
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 164
    :cond_0
    return v0
.end method

.method private static b(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 342
    new-instance v1, Lcom/android/calendar/gl;

    sget-object v0, Lcom/android/calendar/event/hm;->a:Landroid/app/Activity;

    const v2, 0x7f04002c

    invoke-static {p2}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/android/calendar/gl;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 344
    const v0, 0x7f04009c

    invoke-virtual {v1, v0}, Lcom/android/calendar/gl;->setDropDownViewResource(I)V

    .line 345
    invoke-virtual {v1, p3}, Lcom/android/calendar/gl;->a(I)V

    .line 346
    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 347
    invoke-virtual {p1, p3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 349
    invoke-virtual {p1}, Landroid/widget/Spinner;->getId()I

    move-result v0

    const v2, 0x7f12014b

    if-ne v0, v2, :cond_0

    .line 350
    sput-object p2, Lcom/android/calendar/event/hm;->d:Ljava/util/ArrayList;

    .line 353
    :cond_0
    invoke-virtual {p1}, Landroid/widget/Spinner;->getId()I

    move-result v0

    const v2, 0x7f12014c

    if-ne v0, v2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    .line 354
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 355
    if-eqz p4, :cond_1

    .line 356
    invoke-virtual {p4, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    :cond_1
    return-object v1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 959
    sget-object v0, Lcom/android/calendar/event/hm;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 965
    sput-object p0, Lcom/android/calendar/event/hm;->e:Ljava/lang/String;

    .line 966
    return-void
.end method

.method static synthetic b(Z)Z
    .locals 0

    .prologue
    .line 62
    sput-boolean p0, Lcom/android/calendar/event/hm;->g:Z

    return p0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 969
    sget v0, Lcom/android/calendar/event/hm;->f:I

    return v0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 1022
    packed-switch p0, :pswitch_data_0

    .line 1033
    :pswitch_0
    const/4 v0, -0x1

    .line 1035
    :goto_0
    return v0

    .line 1024
    :pswitch_1
    const v0, 0x7f120148

    .line 1025
    goto :goto_0

    .line 1027
    :pswitch_2
    const v0, 0x7f120149

    .line 1028
    goto :goto_0

    .line 1030
    :pswitch_3
    const v0, 0x7f12014a

    .line 1031
    goto :goto_0

    .line 1022
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic c(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 62
    sput-object p0, Lcom/android/calendar/event/hm;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(I)I
    .locals 0

    .prologue
    .line 62
    sput p0, Lcom/android/calendar/event/hm;->b:I

    return p0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 977
    sget-boolean v0, Lcom/android/calendar/event/hm;->g:Z

    return v0
.end method

.method static synthetic e()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/android/calendar/event/hm;->c:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    .prologue
    .line 62
    sget v0, Lcom/android/calendar/event/hm;->b:I

    return v0
.end method

.method static synthetic g()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/android/calendar/event/hm;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/android/calendar/event/hm;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i()Z
    .locals 1

    .prologue
    .line 62
    sget-boolean v0, Lcom/android/calendar/event/hm;->g:Z

    return v0
.end method

.method static synthetic j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/android/calendar/event/hm;->e:Ljava/lang/String;

    return-object v0
.end method
