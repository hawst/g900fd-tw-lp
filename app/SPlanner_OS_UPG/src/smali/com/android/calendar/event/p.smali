.class Lcom/android/calendar/event/p;
.super Ljava/lang/Object;
.source "EasyEditEventFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcom/android/calendar/event/m;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/m;ZZZ)V
    .locals 0

    .prologue
    .line 754
    iput-object p1, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    iput-boolean p2, p0, Lcom/android/calendar/event/p;->a:Z

    iput-boolean p3, p0, Lcom/android/calendar/event/p;->b:Z

    iput-boolean p4, p0, Lcom/android/calendar/event/p;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 757
    if-nez p2, :cond_4

    .line 760
    iget-boolean v0, p0, Lcom/android/calendar/event/p;->a:Z

    if-eqz v0, :cond_2

    .line 761
    iget-boolean v0, p0, Lcom/android/calendar/event/p;->b:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/event/p;->c:Z

    if-nez v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v2}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    .line 770
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->l(Lcom/android/calendar/event/m;)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 771
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    iget-boolean v0, p0, Lcom/android/calendar/event/p;->a:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v1, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 772
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    iput-wide v2, v0, Lcom/android/calendar/as;->aa:J

    .line 783
    :cond_0
    :goto_2
    return-void

    .line 764
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    goto :goto_0

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    goto :goto_0

    .line 771
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    goto :goto_1

    .line 774
    :cond_4
    if-ne p2, v1, :cond_6

    .line 775
    iget-boolean v0, p0, Lcom/android/calendar/event/p;->b:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/event/p;->c:Z

    if-nez v0, :cond_5

    .line 776
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v2}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    goto :goto_2

    .line 778
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    goto :goto_2

    .line 780
    :cond_6
    if-ne p2, v2, :cond_0

    .line 781
    iget-object v0, p0, Lcom/android/calendar/event/p;->d:Lcom/android/calendar/event/m;

    invoke-static {v0, v3}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;I)I

    goto :goto_2
.end method
