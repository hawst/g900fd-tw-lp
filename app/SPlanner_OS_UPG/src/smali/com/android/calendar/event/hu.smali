.class Lcom/android/calendar/event/hu;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/AdapterView;

.field final synthetic b:Landroid/widget/Spinner;

.field final synthetic c:Lcom/android/calendar/event/hp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/hp;Landroid/widget/AdapterView;Landroid/widget/Spinner;)V
    .locals 0

    .prologue
    .line 791
    iput-object p1, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    iput-object p2, p0, Lcom/android/calendar/event/hu;->a:Landroid/widget/AdapterView;

    iput-object p3, p0, Lcom/android/calendar/event/hu;->b:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 794
    .line 796
    :try_start_0
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 797
    const v1, 0x7f0f0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 799
    const v2, 0x7f0f003f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 801
    const v3, 0x7f0f003e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 803
    const v4, 0x7f0f0042

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 806
    iget-object v0, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 808
    if-gtz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/android/calendar/event/hu;->a:Landroid/widget/AdapterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 810
    const-string v0, "15"

    invoke-static {v0}, Lcom/android/calendar/event/hm;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 854
    :goto_0
    return-void

    .line 814
    :cond_0
    iget-object v5, p0, Lcom/android/calendar/event/hu;->b:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 816
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_3

    .line 817
    mul-int/lit8 v0, v0, 0x1

    .line 831
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    iget-object v1, v1, Lcom/android/calendar/event/hp;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 832
    invoke-static {v1, v0, v6}, Lcom/android/calendar/event/hm;->a(Landroid/content/Context;IZ)Ljava/lang/String;

    move-result-object v2

    .line 835
    invoke-static {}, Lcom/android/calendar/event/hm;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 837
    if-ne v1, v7, :cond_2

    .line 838
    invoke-static {}, Lcom/android/calendar/event/hm;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 839
    invoke-static {}, Lcom/android/calendar/event/hm;->h()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 840
    iget-object v2, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    iget-object v2, v2, Lcom/android/calendar/event/hp;->h:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move v0, v1

    .line 843
    iget-object v1, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    iget-object v3, v3, Lcom/android/calendar/event/hp;->e:Landroid/widget/Spinner;

    invoke-static {}, Lcom/android/calendar/event/hm;->h()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v2, v3, v4, v0, v8}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/event/hp;->a:Lcom/android/calendar/gl;

    .line 846
    iget-object v1, p0, Lcom/android/calendar/event/hu;->c:Lcom/android/calendar/event/hp;

    iget-object v1, v1, Lcom/android/calendar/event/hp;->e:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 848
    const-string v0, "15"

    invoke-static {v0}, Lcom/android/calendar/event/hm;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 849
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/calendar/event/hm;->b(Z)Z

    .line 850
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 851
    invoke-static {v8}, Lcom/android/calendar/event/hm;->a(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 853
    invoke-static {v7}, Lcom/android/calendar/event/hm;->a(I)V

    goto :goto_0

    .line 818
    :cond_3
    :try_start_1
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 819
    mul-int/lit8 v0, v0, 0x3c

    goto :goto_1

    .line 820
    :cond_4
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 821
    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit8 v0, v0, 0x18

    goto :goto_1

    .line 822
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    .line 823
    mul-int/lit8 v0, v0, 0x3c

    mul-int/lit8 v0, v0, 0x18

    mul-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 825
    :catch_0
    move-exception v0

    .line 827
    iget-object v0, p0, Lcom/android/calendar/event/hu;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0, v6}, Landroid/widget/AdapterView;->setSelection(I)V

    goto/16 :goto_0
.end method
