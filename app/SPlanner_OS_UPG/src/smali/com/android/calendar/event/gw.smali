.class Lcom/android/calendar/event/gw;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1977
    iput-object p1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1980
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2033
    :cond_0
    :goto_0
    return-void

    .line 1983
    :cond_1
    invoke-static {p2}, Lcom/android/calendar/event/fx;->b(I)I

    .line 1984
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-static {}, Lcom/android/calendar/event/fx;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->d:Ljava/lang/String;

    .line 1986
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/calendar/event/fx;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "preferences_samsung_account_validation"

    invoke-static {v0, v1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1990
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.calendar.event.action.accountvalidationcheck"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1992
    const-string v1, "request_module"

    const/16 v2, 0x1f6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1995
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2000
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->c:Lcom/android/calendar/event/hj;

    invoke-virtual {v0}, Lcom/android/calendar/event/hj;->notifyDataSetChanged()V

    .line 2001
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 1997
    :catch_0
    move-exception v0

    .line 1998
    const-string v0, "EditTaskView"

    const-string v1, "Error: Could not find AccountValidationCheckActivity.ACTION activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2005
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget v1, v1, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;I)I

    .line 2006
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->B(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;

    .line 2007
    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-static {}, Lcom/android/calendar/event/fx;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget v0, v0, Lcom/android/calendar/task/ab;->c:I

    iput v0, v1, Lcom/android/calendar/event/fx;->h:I

    .line 2008
    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->f:Ljava/util/ArrayList;

    invoke-static {}, Lcom/android/calendar/event/fx;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/task/ab;

    iget-object v0, v0, Lcom/android/calendar/task/ab;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/android/calendar/event/fx;->c(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;

    .line 2009
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->C(Lcom/android/calendar/event/fx;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/event/fx;->h()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget v2, v2, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v1, v2}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 2011
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->D(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v1, v1, Lcom/android/calendar/event/fx;->g:[Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/event/fx;->g()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2013
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget v0, v0, Lcom/android/calendar/event/fx;->h:I

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->E(Lcom/android/calendar/event/fx;)I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget v0, v0, Lcom/android/calendar/event/fx;->h:I

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->E(Lcom/android/calendar/event/fx;)I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->F(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->B(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2016
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->G(Lcom/android/calendar/event/fx;)V

    .line 2017
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-boolean v0, v0, Lcom/android/calendar/event/fx;->l:Z

    if-eqz v0, :cond_5

    .line 2018
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->e(Lcom/android/calendar/event/fx;I)I

    .line 2022
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->A(Lcom/android/calendar/event/fx;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->z(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2023
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->H(Lcom/android/calendar/event/fx;)V

    .line 2025
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget v1, v1, Lcom/android/calendar/event/fx;->h:I

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->g(Lcom/android/calendar/event/fx;I)I

    .line 2026
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v1, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->B(Lcom/android/calendar/event/fx;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/fx;->b(Lcom/android/calendar/event/fx;Ljava/lang/String;)Ljava/lang/String;

    .line 2028
    invoke-static {}, Lcom/android/calendar/event/fx;->g()I

    move-result v0

    invoke-static {v0}, Lcom/android/calendar/event/fx;->c(I)I

    .line 2029
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->c:Lcom/android/calendar/event/hj;

    invoke-virtual {v0}, Lcom/android/calendar/event/hj;->notifyDataSetChanged()V

    .line 2030
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto/16 :goto_0

    .line 2020
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/gw;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0, v3}, Lcom/android/calendar/event/fx;->e(Lcom/android/calendar/event/fx;I)I

    goto :goto_2
.end method
