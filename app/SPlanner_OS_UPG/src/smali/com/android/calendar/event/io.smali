.class Lcom/android/calendar/event/io;
.super Landroid/content/AsyncQueryHandler;
.source "ParticipantScheduleActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ParticipantScheduleActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ParticipantScheduleActivity;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    .line 400
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 401
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 406
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 408
    if-eqz p3, :cond_d

    .line 409
    :try_start_0
    const-string v0, "to"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 410
    const-string v0, "displayName"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 411
    const-string v0, "mergedFreeBusy"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 417
    :cond_0
    :goto_0
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 419
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 420
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 421
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 423
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_10

    .line 424
    :cond_1
    const-string v0, "444444444444444444444444444444444444444444444444"

    move-object v2, v0

    .line 428
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-static {v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a(Lcom/android/calendar/event/ParticipantScheduleActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 429
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-static {v8}, Lcom/android/calendar/event/ParticipantScheduleActivity;->b(Lcom/android/calendar/event/ParticipantScheduleActivity;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v7

    .line 430
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v0, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 499
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_2

    .line 500
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 432
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->k:[Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    .line 433
    :goto_2
    iget-object v9, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v9, v9, Lcom/android/calendar/event/ParticipantScheduleActivity;->k:[Ljava/lang/String;

    array-length v9, v9

    if-ge v0, v9, :cond_0

    .line 434
    iget-object v9, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v9, v9, Lcom/android/calendar/event/ParticipantScheduleActivity;->k:[Ljava/lang/String;

    aget-object v9, v9, v0

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 435
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 436
    iget-object v9, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v9, v9, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    add-int/lit8 v10, v0, 0x1

    aput-object v8, v9, v10

    .line 438
    :cond_4
    iget-object v9, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v9, v9, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    add-int/lit8 v10, v0, 0x1

    aput-object v2, v9, v10

    .line 433
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 445
    :cond_6
    const-string v0, "ParticipantsScheduleActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " newNames: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v4, v4, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ",  newNames[1]:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v4, v4, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 452
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 453
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v5, v0

    .line 454
    :goto_3
    if-ge v1, v5, :cond_8

    .line 455
    const-string v6, "444444444444444444444444444444444444444444444444"

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 456
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 462
    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_9

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_c

    .line 464
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->b()V

    .line 465
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-static {v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->c(Lcom/android/calendar/event/ParticipantScheduleActivity;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 466
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 499
    :cond_a
    if-eqz p3, :cond_b

    .line 500
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 502
    :cond_b
    :goto_4
    return-void

    .line 471
    :cond_c
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 473
    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 474
    iget-object v0, v0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 475
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 476
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 486
    :goto_5
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    invoke-static {v0, v1}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a(Lcom/android/calendar/event/ParticipantScheduleActivity;Landroid/text/format/Time;)Landroid/text/format/Time;

    .line 487
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iput-object v2, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    .line 488
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iput-object v4, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    .line 489
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 491
    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/android/calendar/event/ip;->a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 492
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 493
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 495
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 499
    :cond_d
    if-eqz p3, :cond_b

    .line 500
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 478
    :cond_e
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 479
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v1, v1, Lcom/android/calendar/event/ParticipantScheduleActivity;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_5

    .line 482
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 483
    iget-object v0, p0, Lcom/android/calendar/event/io;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v0, v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :cond_10
    move-object v2, v0

    goto/16 :goto_1
.end method
