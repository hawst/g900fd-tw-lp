.class Lcom/android/calendar/event/ha;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 2493
    iput-object p1, p0, Lcom/android/calendar/event/ha;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2497
    invoke-static {}, Lcom/android/calendar/event/fx;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2500
    iget-object v0, p0, Lcom/android/calendar/event/ha;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->l(Lcom/android/calendar/event/fx;)Landroid/widget/EditText;

    move-result-object v0

    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->sendAccessibilityEvent(I)V

    .line 2504
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ha;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->l(Lcom/android/calendar/event/fx;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2505
    iget-object v0, p0, Lcom/android/calendar/event/ha;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2507
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_1

    .line 2508
    iget-object v1, p0, Lcom/android/calendar/event/ha;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->l(Lcom/android/calendar/event/fx;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2510
    :cond_1
    return-void
.end method
