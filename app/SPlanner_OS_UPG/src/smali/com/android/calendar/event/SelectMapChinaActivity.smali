.class public Lcom/android/calendar/event/SelectMapChinaActivity;
.super Landroid/app/Activity;
.source "SelectMapChinaActivity.java"

# interfaces
.implements Lcom/amap/api/maps/AMap$OnMapLongClickListener;


# static fields
.field public static final b:Ljava/util/ArrayList;

.field private static final h:Z


# instance fields
.field private A:Lcom/android/calendar/event/kg;

.field private B:Lcom/amap/api/services/poisearch/PoiSearch;

.field private C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

.field private D:Lcom/android/calendar/event/kh;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:I

.field private H:Landroid/graphics/Bitmap;

.field private I:Ljava/lang/String;

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Z

.field private O:Landroid/app/AlertDialog;

.field private P:Lcom/android/calendar/ex;

.field private Q:I

.field private R:Lcom/amap/api/maps/model/LatLng;

.field private final S:Landroid/widget/AdapterView$OnItemClickListener;

.field private T:Landroid/view/View$OnClickListener;

.field a:Landroid/widget/LinearLayout;

.field public c:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

.field public d:Lcom/android/calendar/event/kc;

.field e:Landroid/location/Location;

.field public f:I

.field g:Landroid/os/Handler;

.field private i:Landroid/widget/SearchView;

.field private j:Landroid/widget/ImageButton;

.field private k:Lcom/amap/api/maps/MapView;

.field private l:Lcom/amap/api/maps/AMap;

.field private m:Landroid/widget/ListView;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/widget/LinearLayout;

.field private q:Lcom/android/calendar/event/ke;

.field private r:Lcom/amap/api/location/LocationManagerProxy;

.field private s:Lcom/android/calendar/event/kd;

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lcom/amap/api/services/geocoder/GeocodeSearch;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106
    const-string v2, "ro.debuggable"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/android/calendar/event/SelectMapChinaActivity;->h:Z

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    return-void

    :cond_0
    move v0, v1

    .line 106
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 110
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->a:Landroid/widget/LinearLayout;

    .line 112
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    .line 114
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->j:Landroid/widget/ImageButton;

    .line 116
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    .line 118
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    .line 132
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->c:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    .line 134
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    .line 136
    new-instance v0, Lcom/android/calendar/event/kc;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/kc;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    .line 138
    new-instance v0, Lcom/android/calendar/event/kd;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/kd;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->s:Lcom/android/calendar/event/kd;

    .line 140
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    .line 142
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->t:Z

    .line 144
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->u:Z

    .line 146
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->v:Z

    .line 148
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->w:Z

    .line 150
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->x:Z

    .line 152
    iput-boolean v3, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    .line 156
    new-instance v0, Lcom/android/calendar/event/kg;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/kg;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->A:Lcom/android/calendar/event/kg;

    .line 162
    new-instance v0, Lcom/android/calendar/event/kh;

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/kh;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->D:Lcom/android/calendar/event/kh;

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->F:Ljava/lang/String;

    .line 174
    iput v3, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->G:I

    .line 180
    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->J:I

    .line 182
    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->K:I

    .line 208
    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->Q:I

    .line 210
    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->f:I

    .line 214
    new-instance v0, Lcom/android/calendar/event/jq;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/jq;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->g:Landroid/os/Handler;

    .line 486
    new-instance v0, Lcom/android/calendar/event/kb;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/kb;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->S:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1059
    new-instance v0, Lcom/android/calendar/event/jt;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/jt;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->T:Landroid/view/View$OnClickListener;

    .line 1172
    return-void
.end method

.method static synthetic A(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/MapView;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    return-object v0
.end method

.method static synthetic B(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/android/calendar/event/kd;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->s:Lcom/android/calendar/event/kd;

    return-object v0
.end method

.method private static a(D)I
    .locals 2

    .prologue
    .line 1180
    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 915
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 917
    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;I)I
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->Q:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/LatLng;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->R:Lcom/amap/api/maps/model/LatLng;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/services/geocoder/GeocodeSearch;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->z:Lcom/amap/api/services/geocoder/GeocodeSearch;

    return-object v0
.end method

.method private a(Lcom/amap/api/maps/model/LatLng;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/amap/api/maps/model/LatLng;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/high16 v0, 0x41600000    # 14.0f

    const/high16 v4, 0x3f000000    # 0.5f

    .line 752
    .line 753
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-virtual {v1}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 754
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-virtual {v1}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v1

    iget v1, v1, Lcom/amap/api/maps/model/CameraPosition;->zoom:F

    .line 757
    :goto_0
    if-eqz p3, :cond_0

    iget-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    if-eqz v2, :cond_0

    .line 763
    :goto_1
    const-string v1, "SelectMapChinaActivityAMAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addMarker "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-static {p1, v0}, Lcom/amap/api/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/amap/api/maps/model/LatLng;F)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/AMap;->moveCamera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 767
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMap;->clear()V

    .line 768
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    new-instance v1, Lcom/amap/api/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, p1}, Lcom/amap/api/maps/model/MarkerOptions;->position(Lcom/amap/api/maps/model/LatLng;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Lcom/amap/api/maps/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/amap/api/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/amap/api/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    const v2, 0x7f02029b

    invoke-static {v2}, Lcom/amap/api/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/amap/api/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps/model/MarkerOptions;->icon(Lcom/amap/api/maps/model/BitmapDescriptor;)Lcom/amap/api/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->addMarker(Lcom/amap/api/maps/model/MarkerOptions;)Lcom/amap/api/maps/model/Marker;

    .line 772
    return-void

    .line 759
    :cond_0
    if-eqz p3, :cond_1

    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    if-nez v0, :cond_1

    .line 760
    const/high16 v0, 0x40e00000    # 7.0f

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/amap/api/maps/model/LatLng;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 519
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    if-nez v0, :cond_2

    .line 523
    const-string v0, "SelectMapChinaActivityAMAP"

    const-string v1, "mlocInfo init error"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 527
    :cond_2
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSearchList add "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    invoke-virtual {v0, p1}, Lcom/android/calendar/ex;->a(Ljava/lang/String;)Z

    .line 530
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    invoke-virtual {v0}, Lcom/android/calendar/ex;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->F:Ljava/lang/String;

    .line 532
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSearchKey = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->F:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCurrentCity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 536
    :cond_3
    const-string v0, "\u5317\u4eac"

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    .line 538
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->f:I

    .line 539
    new-instance v0, Lcom/amap/api/services/poisearch/PoiSearch$Query;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->F:Ljava/lang/String;

    const-string v2, ""

    iget-object v3, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/amap/api/services/poisearch/PoiSearch$Query;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    .line 540
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->setPageSize(I)V

    .line 541
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    iget v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->f:I

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch$Query;->setPageNum(I)V

    .line 542
    new-instance v0, Lcom/amap/api/services/poisearch/PoiSearch;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    invoke-direct {v0, p0, v1}, Lcom/amap/api/services/poisearch/PoiSearch;-><init>(Landroid/content/Context;Lcom/amap/api/services/poisearch/PoiSearch$Query;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->B:Lcom/amap/api/services/poisearch/PoiSearch;

    .line 543
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->B:Lcom/amap/api/services/poisearch/PoiSearch;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->D:Lcom/android/calendar/event/kh;

    invoke-virtual {v0, v1}, Lcom/amap/api/services/poisearch/PoiSearch;->setOnPoiSearchListener(Lcom/amap/api/services/poisearch/PoiSearch$OnPoiSearchListener;)V

    .line 544
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->B:Lcom/amap/api/services/poisearch/PoiSearch;

    invoke-virtual {v0}, Lcom/amap/api/services/poisearch/PoiSearch;->searchPOIAsyn()V

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1118
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1121
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1123
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->v:Z

    return p1
.end method

.method static synthetic b(I)D
    .locals 2

    .prologue
    .line 103
    invoke-static {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->c(I)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/SearchView;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/amap/api/maps/model/LatLng;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Lcom/amap/api/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 927
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 929
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->twSetSelection(I)V

    .line 931
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->u:Z

    return p1
.end method

.method private static c(I)D
    .locals 4

    .prologue
    .line 1184
    int-to-double v0, p0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectMapChinaActivity;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->J:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectMapChinaActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 103
    sget-boolean v0, Lcom/android/calendar/event/SelectMapChinaActivity;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->t:Z

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/SelectMapChinaActivity;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->K:I

    return v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 501
    const-string v0, "SelectMapChinaActivityAMAP"

    const-string v1, "getCurrentLocation"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 505
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 507
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 508
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/MapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 510
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->t:Z

    .line 512
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    const-string v1, "lbs"

    const-wide/16 v2, 0x1388

    const/high16 v4, 0x41200000    # 10.0f

    iget-object v5, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/location/LocationManagerProxy;->requestLocationUpdates(Ljava/lang/String;JFLcom/amap/api/location/AMapLocationListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 516
    :goto_0
    return-void

    .line 513
    :catch_0
    move-exception v0

    .line 514
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->w:Z

    return p1
.end method

.method private e()V
    .locals 6

    .prologue
    .line 548
    const-string v0, "SelectMapChinaActivityAMAP"

    const-string v1, "getCurrentCity"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->u:Z

    .line 552
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0}, Lcom/amap/api/maps/AMap;->clear()V

    .line 555
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    const-string v1, "lbs"

    const-wide/16 v2, 0x1388

    const/high16 v4, 0x41200000    # 10.0f

    iget-object v5, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/location/LocationManagerProxy;->requestLocationUpdates(Ljava/lang/String;JFLcom/amap/api/location/AMapLocationListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 559
    :goto_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 560
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 561
    return-void

    .line 556
    :catch_0
    move-exception v0

    .line 557
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic e(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->e()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/event/SelectMapChinaActivity;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->x:Z

    return p1
.end method

.method static synthetic f(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->O:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1033
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1034
    const v1, 0x7f0f01aa

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1035
    const v1, 0x7f0f0119

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1036
    const v1, 0x7f0f029a

    new-instance v2, Lcom/android/calendar/event/jr;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/jr;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1047
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/js;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/js;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1054
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1055
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->O:Landroid/app/AlertDialog;

    .line 1057
    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->f()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->d()V

    return-void
.end method

.method static synthetic i(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/services/poisearch/PoiSearch$Query;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->C:Lcom/amap/api/services/poisearch/PoiSearch$Query;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/services/poisearch/PoiSearch;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->B:Lcom/amap/api/services/poisearch/PoiSearch;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/event/SelectMapChinaActivity;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->Q:I

    return v0
.end method

.method static synthetic l(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->F:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/event/SelectMapChinaActivity;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->u:Z

    return v0
.end method

.method static synthetic n(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->E:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/event/SelectMapChinaActivity;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->t:Z

    return v0
.end method

.method static synthetic p(Lcom/android/calendar/event/SelectMapChinaActivity;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->w:Z

    return v0
.end method

.method static synthetic q(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/model/LatLng;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->R:Lcom/amap/api/maps/model/LatLng;

    return-object v0
.end method

.method static synthetic r(Lcom/android/calendar/event/SelectMapChinaActivity;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->x:Z

    return v0
.end method

.method static synthetic s(Lcom/android/calendar/event/SelectMapChinaActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic t(Lcom/android/calendar/event/SelectMapChinaActivity;)Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->v:Z

    return v0
.end method

.method static synthetic u(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/android/calendar/event/ke;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->q:Lcom/android/calendar/event/ke;

    return-object v0
.end method

.method static synthetic v(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic w(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->n:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic x(Lcom/android/calendar/event/SelectMapChinaActivity;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->o:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic y(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/maps/AMap;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    return-object v0
.end method

.method static synthetic z(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/location/LocationManagerProxy;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1090
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1091
    if-eqz v0, :cond_1

    .line 1092
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1093
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1094
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1095
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 1096
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1097
    const v1, 0x7f04007e

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 1099
    const v0, 0x7f1200d4

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1100
    const v1, 0x7f1200d5

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1102
    if-eqz v1, :cond_0

    .line 1103
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1105
    :cond_0
    if-eqz v0, :cond_1

    .line 1106
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->T:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->N:Z

    if-nez v0, :cond_2

    .line 1111
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1112
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1113
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 1114
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1116
    :cond_2
    return-void
.end method

.method public a(I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 600
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMapViewHeight nSize = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 607
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 609
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    .line 610
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 611
    invoke-static {p0}, Lcom/android/calendar/hj;->v(Landroid/content/Context;)I

    move-result v5

    .line 613
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 614
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    .line 616
    :goto_0
    sub-int/2addr v2, v4

    sub-int/2addr v2, v5

    sub-int v5, v2, v0

    .line 617
    invoke-static {p0, v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Landroid/content/Context;I)I

    move-result v0

    sub-int v0, v5, v0

    div-int/lit8 v0, v0, 0x2

    move v2, v3

    .line 620
    :goto_1
    if-gt v2, p1, :cond_0

    .line 621
    const/16 v1, 0x32

    invoke-static {p0, v1}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Landroid/content/Context;I)I

    move-result v1

    mul-int/2addr v1, v2

    invoke-static {p0, v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Landroid/content/Context;I)I

    move-result v4

    add-int/lit8 v6, v2, -0x1

    mul-int/2addr v4, v6

    add-int/2addr v4, v1

    .line 623
    if-le v4, v0, :cond_1

    move v1, v0

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 630
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 631
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 633
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 634
    sub-int v1, v5, v1

    invoke-static {p0, v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Landroid/content/Context;I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 635
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/MapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 637
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMapViewHeight mSearchListView = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMapViewHeight mMapView = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v2}, Lcom/amap/api/maps/MapView;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMapViewHeight groupHeight = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    return-void

    .line 620
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto/16 :goto_1

    :cond_2
    move v0, v1

    goto/16 :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 653
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    .line 655
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 656
    const-string v1, "latitude"

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v2}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-static {v2, v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(D)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 657
    const-string v1, "longitude"

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v2}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/maps/AMap;->getCameraPosition()Lcom/amap/api/maps/model/CameraPosition;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/maps/model/CameraPosition;->target:Lcom/amap/api/maps/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-static {v2, v3}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(D)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 658
    const-string v1, "location"

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 659
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/SelectMapChinaActivity;->b(Landroid/graphics/Bitmap;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 660
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 661
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->H:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x32

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 662
    const-string v2, "map"

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 664
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->setResult(ILandroid/content/Intent;)V

    .line 666
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 670
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->finish()V

    .line 671
    return-void
.end method

.method b()Z
    .locals 10

    .prologue
    const/16 v9, 0x1c6

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1130
    .line 1136
    const-string v0, "PAP"

    invoke-static {}, Lcom/android/calendar/dz;->G()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1169
    :cond_0
    :goto_0
    return v1

    .line 1140
    :cond_1
    const-string v0, "gsm.operator.iso-country"

    const-string v3, ""

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1142
    const-string v3, "SelectMapChinaActivityAMAP"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isoCountryCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    const-string v3, "cn"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 1147
    :goto_1
    const-string v3, "gsm.operator.numeric"

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 1148
    const-string v4, "gsm.operator.numeric2"

    const-string v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 1152
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v8, :cond_6

    .line 1153
    invoke-virtual {v3, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1155
    :goto_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v8, :cond_2

    .line 1156
    invoke-virtual {v4, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1159
    :cond_2
    const-string v4, "SelectMapChinaActivityAMAP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "opNumSIM1Code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " opNumSIM2Code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    const/16 v4, 0x1cc

    if-eq v3, v4, :cond_3

    const/16 v4, 0x1c7

    if-eq v3, v4, :cond_3

    if-eq v3, v9, :cond_3

    const/16 v4, 0x1cc

    if-eq v2, v4, :cond_3

    const/16 v4, 0x1c7

    if-eq v2, v4, :cond_3

    if-ne v2, v9, :cond_4

    :cond_3
    move v0, v1

    .line 1165
    :cond_4
    if-nez v3, :cond_5

    if-nez v2, :cond_5

    invoke-static {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v1, v0

    goto/16 :goto_0

    :cond_6
    move v3, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_1
.end method

.method protected b(Landroid/graphics/Bitmap;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 674
    .line 675
    if-nez p1, :cond_0

    .line 691
    :goto_0
    return v2

    .line 680
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 681
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 682
    iget v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->M:I

    if-le v0, v1, :cond_2

    .line 683
    iget v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->M:I

    sub-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x2

    .line 684
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->M:I

    .line 686
    :goto_1
    iget v4, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->L:I

    if-le v3, v4, :cond_1

    .line 687
    iget v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->L:I

    sub-int v2, v3, v2

    div-int/lit8 v3, v2, 0x2

    .line 688
    iget v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->L:I

    .line 690
    :goto_2
    invoke-static {p1, v1, v3, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->H:Landroid/graphics/Bitmap;

    .line 691
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    move v5, v3

    move v3, v2

    move v2, v5

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 586
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 588
    const/4 v0, 0x0

    .line 589
    sget-object v1, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 590
    sget-object v0, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 592
    :cond_0
    if-lez v0, :cond_1

    .line 593
    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->a(I)V

    .line 596
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->invalidateOptionsMenu()V

    .line 597
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 250
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 252
    :try_start_0
    const-string v0, "android.os.AsyncTask"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 256
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->a()V

    .line 258
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    .line 259
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate isInChinaArea : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    new-instance v0, Lcom/android/calendar/ex;

    invoke-direct {v0}, Lcom/android/calendar/ex;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    .line 263
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 264
    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    .line 265
    const-string v2, "mode"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->G:I

    .line 266
    const-string v2, "map_latitude"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->J:I

    .line 267
    const-string v2, "map_longtitude"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->K:I

    .line 269
    const v0, 0x7f04008f

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->setContentView(I)V

    .line 271
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->N:Z

    .line 273
    const v0, 0x7f120294

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->j:Landroid/widget/ImageButton;

    .line 275
    const v0, 0x7f12003c

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    .line 277
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    const v2, 0x10000003

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 278
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    new-instance v2, Lcom/android/calendar/event/ju;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ju;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v2, v4, v5}, Landroid/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 288
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    new-instance v2, Lcom/android/calendar/event/jv;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/jv;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 301
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    const-string v2, "My_Location"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 303
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 304
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    if-lez v0, :cond_7

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->twSetSelection(I)V

    .line 307
    :cond_0
    const v0, 0x7f120295

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/amap/api/maps/MapView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    .line 308
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    if-nez v0, :cond_1

    .line 309
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->finish()V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0, v6}, Lcom/amap/api/maps/MapView;->setLongClickable(Z)V

    .line 313
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 317
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->getMap()Lcom/amap/api/maps/AMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    .line 322
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    invoke-virtual {v0, p0}, Lcom/amap/api/maps/AMap;->setOnMapLongClickListener(Lcom/amap/api/maps/AMap$OnMapLongClickListener;)V

    .line 324
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    if-eqz v0, :cond_8

    .line 325
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v1}, Lcom/amap/api/maps/CameraUpdateFactory;->zoomTo(F)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->moveCamera(Lcom/amap/api/maps/CameraUpdate;)V

    .line 330
    :goto_3
    const v0, 0x7f120298

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    .line 332
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 333
    const v1, 0x7f040090

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->n:Landroid/widget/LinearLayout;

    .line 334
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 335
    const v0, 0x7f120299

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->o:Landroid/widget/LinearLayout;

    .line 336
    const v0, 0x7f12029a

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->p:Landroid/widget/LinearLayout;

    .line 338
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    if-nez v0, :cond_3

    .line 339
    invoke-static {p0}, Lcom/amap/api/location/LocationManagerProxy;->getInstance(Landroid/app/Activity;)Lcom/amap/api/location/LocationManagerProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    .line 340
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    if-eqz v0, :cond_3

    .line 342
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    const-string v1, "lbs"

    const-wide/16 v2, 0x1388

    const/high16 v4, 0x41200000    # 10.0f

    iget-object v5, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/location/LocationManagerProxy;->requestLocationUpdates(Ljava/lang/String;JFLcom/amap/api/location/AMapLocationListener;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 349
    :cond_3
    :goto_4
    new-instance v0, Lcom/android/calendar/event/jw;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/jw;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->c:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    .line 365
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->c:Lcom/amap/api/maps/AMap$OnMarkerClickListener;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->setOnMarkerClickListener(Lcom/amap/api/maps/AMap$OnMarkerClickListener;)V

    .line 366
    new-instance v0, Landroid/location/Location;

    const-string v1, "TempLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    .line 368
    new-instance v0, Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-direct {v0, p0}, Lcom/amap/api/services/geocoder/GeocodeSearch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->z:Lcom/amap/api/services/geocoder/GeocodeSearch;

    .line 369
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->z:Lcom/amap/api/services/geocoder/GeocodeSearch;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->A:Lcom/android/calendar/event/kg;

    invoke-virtual {v0, v1}, Lcom/amap/api/services/geocoder/GeocodeSearch;->setOnGeocodeSearchListener(Lcom/amap/api/services/geocoder/GeocodeSearch$OnGeocodeSearchListener;)V

    .line 371
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/event/jx;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jx;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 393
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->j:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/calendar/event/jy;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jy;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 418
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->o:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/event/jz;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jz;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 427
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->p:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/event/ka;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ka;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->S:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 442
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0216

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->L:I

    .line 445
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->N:Z

    if-nez v0, :cond_a

    .line 446
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 448
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 450
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 451
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 453
    if-eqz v2, :cond_4

    const/4 v0, 0x2

    if-ne v2, v0, :cond_9

    .line 454
    :cond_4
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 460
    :goto_5
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0210

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 461
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    .line 462
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->M:I

    .line 468
    :goto_6
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 469
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    .line 472
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 473
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->d()V

    .line 481
    :cond_6
    :goto_7
    new-instance v0, Lcom/android/calendar/event/ke;

    sget-object v1, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    const v2, 0x7f040091

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/event/ke;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->q:Lcom/android/calendar/event/ke;

    .line 483
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->q:Lcom/android/calendar/event/ke;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 484
    return-void

    :cond_7
    move v0, v1

    .line 304
    goto/16 :goto_1

    .line 318
    :catch_0
    move-exception v0

    .line 319
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_2

    .line 327
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->l:Lcom/amap/api/maps/AMap;

    const/high16 v1, 0x40e00000    # 7.0f

    invoke-static {v1}, Lcom/amap/api/maps/CameraUpdateFactory;->zoomTo(F)Lcom/amap/api/maps/CameraUpdate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps/AMap;->moveCamera(Lcom/amap/api/maps/CameraUpdate;)V

    goto/16 :goto_3

    .line 343
    :catch_1
    move-exception v0

    .line 344
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_4

    .line 456
    :cond_9
    iget v0, v1, Landroid/graphics/Point;->y:I

    goto :goto_5

    .line 464
    :cond_a
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->M:I

    goto :goto_6

    .line 474
    :cond_b
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->G:I

    if-ne v0, v6, :cond_c

    .line 475
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->e()V

    goto :goto_7

    .line 476
    :cond_c
    iget v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->G:I

    if-eq v0, v6, :cond_6

    .line 477
    iput-boolean v6, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->x:Z

    .line 478
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->g:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_7

    .line 253
    :catch_2
    move-exception v0

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 580
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 581
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 696
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    invoke-virtual {v0, v1}, Lcom/amap/api/location/LocationManagerProxy;->removeUpdates(Lcom/amap/api/location/AMapLocationListener;)V

    .line 698
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    invoke-virtual {v0}, Lcom/amap/api/location/LocationManagerProxy;->destory()V

    .line 699
    iput-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->r:Lcom/amap/api/location/LocationManagerProxy;

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->onDestroy()V

    .line 705
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    if-eqz v0, :cond_2

    .line 706
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    invoke-virtual {v0}, Lcom/android/calendar/ex;->d()V

    .line 707
    iput-object v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->P:Lcom/android/calendar/ex;

    .line 710
    :cond_2
    sget-object v0, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 711
    sget-object v0, Lcom/android/calendar/event/SelectMapChinaActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 713
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 714
    return-void
.end method

.method public onMapLongClick(Lcom/amap/api/maps/model/LatLng;)V
    .locals 6

    .prologue
    .line 735
    sget-boolean v0, Lcom/android/calendar/event/SelectMapChinaActivity;->h:Z

    if-eqz v0, :cond_0

    .line 736
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMapLongClick: Latitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Longitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->m:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 740
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 741
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 742
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps/MapView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 744
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->w:Z

    .line 746
    new-instance v0, Lcom/amap/api/services/geocoder/RegeocodeQuery;

    new-instance v1, Lcom/amap/api/services/core/LatLonPoint;

    iget-wide v2, p1, Lcom/amap/api/maps/model/LatLng;->latitude:D

    iget-wide v4, p1, Lcom/amap/api/maps/model/LatLng;->longitude:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/services/core/LatLonPoint;-><init>(DD)V

    const/high16 v2, 0x43480000    # 200.0f

    sget-object v3, Lcom/amap/api/services/geocoder/GeocodeSearch;->AMAP:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/amap/api/services/geocoder/RegeocodeQuery;-><init>(Lcom/amap/api/services/core/LatLonPoint;FLjava/lang/String;)V

    .line 747
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->R:Lcom/amap/api/maps/model/LatLng;

    .line 748
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->z:Lcom/amap/api/services/geocoder/GeocodeSearch;

    invoke-virtual {v1, v0}, Lcom/amap/api/services/geocoder/GeocodeSearch;->getFromLocationAsyn(Lcom/amap/api/services/geocoder/RegeocodeQuery;)V

    .line 749
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 644
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->onPause()V

    .line 566
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 567
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 718
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 719
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->I:Ljava/lang/String;

    .line 720
    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->J:I

    .line 721
    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->K:I

    .line 722
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapChinaActivity;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    .line 572
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume isInChinaArea : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->y:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0}, Lcom/amap/api/maps/MapView;->onResume()V

    .line 575
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 576
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 726
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 727
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->k:Lcom/amap/api/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/amap/api/maps/MapView;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 728
    const-string v0, "location"

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->i:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    const-string v0, "latitude"

    iget v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->J:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 730
    const-string v0, "longitude"

    iget v1, p0, Lcom/android/calendar/event/SelectMapChinaActivity;->K:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 731
    return-void
.end method
