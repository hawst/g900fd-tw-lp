.class Lcom/android/calendar/event/cg;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 4087
    iput-object p1, p0, Lcom/android/calendar/event/cg;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 4091
    const-string v0, "authorities"

    .line 4093
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4094
    const-string v1, "authorities"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "com.android.calendar"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 4098
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/cg;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4103
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/cg;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, v5}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;Z)Z

    .line 4104
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 4105
    return-void

    .line 4099
    :catch_0
    move-exception v0

    .line 4100
    const-string v0, "EditEvent"

    const-string v1, "Error: Could not find Settings.ACTION_ADD_ACCOUNT activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
