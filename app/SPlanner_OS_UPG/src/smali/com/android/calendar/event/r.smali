.class Lcom/android/calendar/event/r;
.super Landroid/content/AsyncQueryHandler;
.source "EasyEditEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/m;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/m;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    .line 300
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 301
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 305
    if-nez p3, :cond_0

    .line 363
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-virtual {v0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 311
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 315
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 360
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 317
    :pswitch_0
    iget-object v0, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v0, v1}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;I)I

    .line 320
    :try_start_0
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 326
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 327
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v1

    iget-boolean v1, v1, Lcom/android/calendar/aq;->m:Z

    if-nez v1, :cond_4

    .line 328
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v1, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 329
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    iget-object v2, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v2, p3}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;Landroid/database/Cursor;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/calendar/event/m;->b(Lcom/android/calendar/event/m;I)I

    .line 330
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->e(Lcom/android/calendar/event/m;)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, v1, Lcom/android/calendar/as;->c:J

    .line 331
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->a(Lcom/android/calendar/event/m;)Landroid/app/Activity;

    move-result-object v1

    const-string v2, "preference_defaultCalendar"

    const-string v3, "local"

    invoke-static {v1, v2, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 333
    iget-object v2, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v2

    iput-object v1, v2, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 350
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;I)V

    .line 352
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->f(Lcom/android/calendar/event/m;)V

    .line 353
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 336
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v1

    iget v1, v1, Lcom/android/calendar/aq;->n:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v2

    iget v2, v2, Lcom/android/calendar/aq;->n:I

    if-gt v1, v2, :cond_5

    .line 338
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Lcom/android/calendar/aq;->n:I

    .line 340
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v2}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v2

    iget v2, v2, Lcom/android/calendar/aq;->n:I

    add-int/lit8 v2, v2, 0x1

    int-to-long v2, v2

    iput-wide v2, v1, Lcom/android/calendar/as;->c:J

    .line 341
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;

    move-result-object v1

    iget v1, v1, Lcom/android/calendar/aq;->n:I

    invoke-interface {p3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 343
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 345
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    const/16 v2, 0xc

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/calendar/as;->az:Ljava/lang/String;

    .line 347
    iget-object v1, p0, Lcom/android/calendar/event/r;->a:Lcom/android/calendar/event/m;

    invoke-static {v1}, Lcom/android/calendar/event/m;->d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/calendar/as;->G:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 355
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 315
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
