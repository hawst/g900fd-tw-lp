.class Lcom/android/calendar/event/hw;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/hp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/android/calendar/event/hw;->a:Lcom/android/calendar/event/hp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 642
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 647
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 651
    .line 652
    iget-object v0, p0, Lcom/android/calendar/event/hw;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 653
    const/4 v0, 0x0

    .line 655
    :goto_0
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 656
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 659
    iget-object v0, p0, Lcom/android/calendar/event/hw;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 660
    invoke-static {v1}, Lcom/android/calendar/event/hm;->b(Z)Z

    .line 662
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
