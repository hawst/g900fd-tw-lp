.class public Lcom/android/calendar/event/ViewSlider;
.super Landroid/widget/SlidingDrawer;
.source "ViewSlider.java"


# static fields
.field private static d:I


# instance fields
.field private a:Z

.field private b:I

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    sput v0, Lcom/android/calendar/event/ViewSlider;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/SlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    iput-boolean v1, p0, Lcom/android/calendar/event/ViewSlider;->c:Z

    .line 33
    const-string v2, "android"

    const-string v3, "orientation"

    invoke-interface {p2, v2, v3, v0}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 35
    const-string v3, "android"

    const-string v4, "topOffset"

    invoke-interface {p2, v3, v4, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    .line 36
    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/event/ViewSlider;->a:Z

    .line 37
    return-void

    :cond_0
    move v0, v1

    .line 36
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SlidingDrawer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    iput-boolean v1, p0, Lcom/android/calendar/event/ViewSlider;->c:Z

    .line 21
    const-string v2, "android"

    const-string v3, "orientation"

    invoke-interface {p2, v2, v3, v0}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 23
    const-string v3, "android"

    const-string v4, "topOffset"

    invoke-interface {p2, v3, v4, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    .line 24
    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/calendar/event/ViewSlider;->a:Z

    .line 25
    return-void

    :cond_0
    move v0, v1

    .line 24
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;I)I
    .locals 3

    .prologue
    .line 86
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f120121

    if-ne v0, v1, :cond_3

    .line 88
    const v0, 0x7f12010f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->isOpened()Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0152

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 106
    :cond_0
    :goto_0
    return p2

    .line 93
    :cond_1
    iget-boolean v1, p0, Lcom/android/calendar/event/ViewSlider;->c:Z

    if-eqz v1, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/calendar/event/ViewSlider;->d:I

    .line 95
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 97
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 100
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f120137

    if-ne v0, v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->isOpened()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    .line 51
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 52
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 54
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 55
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 57
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getHandle()Landroid/view/View;

    move-result-object v4

    .line 58
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getContent()Landroid/view/View;

    move-result-object v5

    .line 59
    invoke-virtual {p0, v4, p1, p2}, Lcom/android/calendar/event/ViewSlider;->measureChild(Landroid/view/View;II)V

    .line 61
    iget-boolean v6, p0, Lcom/android/calendar/event/ViewSlider;->a:Z

    if-eqz v6, :cond_2

    .line 62
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v3, v0

    iget v1, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    sub-int/2addr v0, v1

    .line 64
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, p1, v0}, Landroid/view/View;->measure(II)V

    .line 65
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    add-int/2addr v0, v1

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 66
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 67
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-le v2, v0, :cond_0

    .line 68
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :cond_0
    move v7, v1

    move v1, v0

    move v0, v7

    .line 81
    :cond_1
    :goto_0
    invoke-virtual {p0, v5, v0}, Lcom/android/calendar/event/ViewSlider;->a(Landroid/view/View;I)I

    move-result v0

    .line 82
    invoke-virtual {p0, v1, v0}, Lcom/android/calendar/event/ViewSlider;->setMeasuredDimension(II)V

    .line 83
    return-void

    .line 71
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    sub-int/2addr v1, v2

    .line 72
    invoke-virtual {p0}, Lcom/android/calendar/event/ViewSlider;->getContent()Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v0, p2}, Landroid/view/View;->measure(II)V

    .line 74
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/ViewSlider;->b:I

    add-int/2addr v0, v1

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v0

    .line 75
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 76
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 77
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method
