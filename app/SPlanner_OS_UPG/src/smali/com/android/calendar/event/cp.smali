.class Lcom/android/calendar/event/cp;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/animation/ValueAnimator;

.field final synthetic c:Landroid/widget/LinearLayout;

.field final synthetic d:Landroid/widget/LinearLayout;

.field final synthetic e:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;Landroid/view/View;Landroid/animation/ValueAnimator;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 4358
    iput-object p1, p0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    iput-object p2, p0, Lcom/android/calendar/event/cp;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/android/calendar/event/cp;->b:Landroid/animation/ValueAnimator;

    iput-object p4, p0, Lcom/android/calendar/event/cp;->c:Landroid/widget/LinearLayout;

    iput-object p5, p0, Lcom/android/calendar/event/cp;->d:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 4366
    iget-object v0, p0, Lcom/android/calendar/event/cp;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 4368
    iget-object v0, p0, Lcom/android/calendar/event/cp;->c:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/calendar/event/cq;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/cq;-><init>(Lcom/android/calendar/event/cp;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 4390
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 4394
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 4361
    iget-object v0, p0, Lcom/android/calendar/event/cp;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 4362
    return-void
.end method
