.class Lcom/android/calendar/event/fi;
.super Landroid/os/AsyncTask;
.source "EditEventView.java"


# instance fields
.field a:I

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 1

    .prologue
    .line 1948
    iput-object p1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1946
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/fi;->a:I

    .line 1949
    return-void
.end method


# virtual methods
.method protected a([Lcom/android/calendar/event/fn;)V
    .locals 10

    .prologue
    const/16 v9, 0x5f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2008
    if-nez p1, :cond_0

    .line 2071
    :goto_0
    return-void

    .line 2011
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2012
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :cond_1
    move v3, v4

    .line 2015
    :goto_1
    iget v0, p0, Lcom/android/calendar/event/fi;->a:I

    if-ge v3, v0, :cond_9

    .line 2016
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04009d

    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2019
    if-eqz v0, :cond_2

    .line 2020
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/android/calendar/event/fn;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2021
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/android/calendar/event/fn;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 2022
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setId(I)V

    .line 2023
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 2026
    :cond_2
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/android/calendar/event/fn;->b:Ljava/lang/String;

    .line 2028
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".png"

    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2d

    invoke-virtual {v1, v2, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    .line 2030
    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v6, "string"

    iget-object v7, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v7}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v1, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2031
    if-eqz v2, :cond_7

    .line 2032
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2038
    :cond_3
    :goto_2
    new-instance v1, Lcom/android/calendar/event/fj;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/fj;-><init>(Lcom/android/calendar/event/fi;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2049
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 2050
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04009e

    iget-object v6, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v6, v6, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2053
    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_8

    move v2, v5

    .line 2054
    :goto_3
    iget v6, p0, Lcom/android/calendar/event/fi;->a:I

    add-int/lit8 v6, v6, -0x1

    if-ne v3, v6, :cond_4

    if-eqz v2, :cond_4

    .line 2055
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 2056
    iget-object v6, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v6}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c012d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2057
    iget-object v7, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v7}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c012e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 2058
    iput v6, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 2059
    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 2060
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2062
    :cond_4
    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2063
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 2065
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_6

    .line 2066
    iget-object v1, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2015
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 2033
    :cond_7
    const-string v2, "day"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2034
    const-string v2, "day"

    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2035
    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0f012d

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v4

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2036
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_8
    move v2, v4

    .line 2053
    goto/16 :goto_3

    .line 2070
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0, v4}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;Z)Z

    goto/16 :goto_0
.end method

.method protected varargs a([Ljava/lang/Void;)[Lcom/android/calendar/event/fn;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v11, 0x1

    .line 1953
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/fi;->a:I

    .line 1956
    iget v0, p0, Lcom/android/calendar/event/fi;->a:I

    new-array v8, v0, [Lcom/android/calendar/event/fn;

    .line 1958
    const-string v0, "recently ASC"

    .line 1959
    const-string v0, "recently!=0"

    .line 1963
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/gx;->b:[Ljava/lang/String;

    const-string v3, "recently!=0"

    const/4 v4, 0x0

    const-string v5, "recently ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1968
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1970
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v7

    .line 1973
    :cond_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1977
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1978
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1979
    const/4 v6, 0x2

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1980
    invoke-static {v6, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1982
    new-instance v7, Lcom/android/calendar/event/fn;

    iget-object v9, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    const/4 v10, 0x0

    invoke-direct {v7, v9, v10}, Lcom/android/calendar/event/fn;-><init>(Lcom/android/calendar/event/ay;Lcom/android/calendar/event/az;)V

    .line 1984
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v7, Lcom/android/calendar/event/fn;->a:Ljava/lang/Long;

    .line 1985
    iput-object v3, v7, Lcom/android/calendar/event/fn;->b:Ljava/lang/String;

    .line 1986
    iput-object v6, v7, Lcom/android/calendar/event/fn;->c:Ljava/lang/String;

    .line 1987
    iput-object v2, v7, Lcom/android/calendar/event/fn;->d:Landroid/graphics/Bitmap;

    .line 1989
    aput-object v7, v8, v0

    .line 1991
    add-int/lit8 v0, v0, 0x1

    .line 1993
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/calendar/event/fi;->a:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-lt v0, v2, :cond_0

    .line 1996
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v2, "preference_recent_updated"

    invoke-static {v0, v2, v11}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 1998
    if-eqz v1, :cond_2

    .line 1999
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2003
    :cond_2
    return-object v8

    .line 1996
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    iget-object v2, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    const-string v3, "preference_recent_updated"

    invoke-static {v2, v3, v11}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 1998
    if-eqz v1, :cond_3

    .line 1999
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1996
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1945
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/fi;->a([Ljava/lang/Void;)[Lcom/android/calendar/event/fn;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/android/calendar/event/fi;->b:Lcom/android/calendar/event/ay;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;Z)Z

    .line 2076
    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1945
    check-cast p1, [Lcom/android/calendar/event/fn;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/fi;->a([Lcom/android/calendar/event/fn;)V

    return-void
.end method
