.class Lcom/android/calendar/event/jf;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 781
    iput-object p1, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 8

    .prologue
    .line 785
    if-eqz p1, :cond_1

    .line 786
    const-string v0, "SelectMapActivity"

    const-string v1, "onLocationChanged"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->d(Lcom/android/calendar/event/SelectMapActivity;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 788
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0, v2}, Lcom/android/calendar/event/SelectMapActivity;->c(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    .line 789
    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->l(Lcom/android/calendar/event/SelectMapActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 790
    new-instance v0, Lcom/android/calendar/event/jp;

    iget-object v2, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/android/calendar/event/jp;-><init>(Lcom/android/calendar/event/SelectMapActivity;Lcom/android/calendar/event/je;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v4}, Lcom/android/calendar/event/SelectMapActivity;->m(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/jp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 792
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->n(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->o(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->n(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/jf;->a:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->o(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 798
    :cond_1
    return-void

    .line 792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 801
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 804
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 807
    return-void
.end method
