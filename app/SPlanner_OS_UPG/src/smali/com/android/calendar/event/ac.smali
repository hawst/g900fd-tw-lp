.class public Lcom/android/calendar/event/ac;
.super Ljava/lang/Object;
.source "EasyEditEventView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/v;

.field private b:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/v;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 640
    iput-object p2, p0, Lcom/android/calendar/event/ac;->b:Landroid/text/format/Time;

    .line 641
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 645
    iget-object v0, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v9, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    new-instance v0, Lcom/android/calendar/common/extension/a;

    iget-object v1, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    invoke-static {v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/ad;

    iget-object v3, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    invoke-direct {v2, v3, p1}, Lcom/android/calendar/event/ad;-><init>(Lcom/android/calendar/event/v;Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/calendar/event/ac;->b:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    iget-object v4, p0, Lcom/android/calendar/event/ac;->b:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    iget-object v5, p0, Lcom/android/calendar/event/ac;->b:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f03b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v8}, Lcom/android/calendar/common/extension/a;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;IIILjava/lang/String;ZZ)V

    iput-object v0, v9, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    .line 655
    iget-object v0, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0, v8}, Lcom/android/calendar/common/extension/a;->setCanceledOnTouchOutside(Z)V

    .line 656
    iget-object v0, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->show()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/calendar/event/ac;->a:Lcom/android/calendar/event/v;

    invoke-static {v0}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 662
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/ac;->a(Landroid/view/View;)V

    .line 663
    return-void
.end method
