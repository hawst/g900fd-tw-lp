.class Lcom/android/calendar/event/bn;
.super Landroid/content/AsyncQueryHandler;
.source "EditEventView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/bm;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/bm;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 1779
    iput-object p1, p0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 17

    .prologue
    .line 1782
    if-nez p3, :cond_0

    .line 1818
    :goto_0
    return-void

    .line 1785
    :cond_0
    const-string v2, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 1786
    const-string v3, "sticker_name"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 1787
    const-string v3, "filepath"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 1788
    const-string v3, "sticker_group"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 1789
    const-string v3, "recently"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 1790
    const-string v3, "packageId"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    .line 1792
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v3, v3, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lcom/android/calendar/event/ay;->aJ:Ljava/util/ArrayList;

    .line 1794
    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1795
    :goto_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1796
    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1798
    invoke-static {v3}, Lcom/android/calendar/gx;->a(I)I

    move-result v8

    .line 1800
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v3, v3, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v3, Lcom/android/calendar/event/ay;->aJ:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    new-instance v3, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-direct/range {v3 .. v10}, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;-><init>(JLjava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1805
    :cond_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->close()V

    .line 1807
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v2, v2, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->aJ:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 1808
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v2, v2, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->aJ:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/android/calendar/event/SelectStickerFragment;->a(Ljava/util/ArrayList;)V

    .line 1811
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v3, v3, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/android/calendar/event/SelectStickerActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1813
    const v3, 0x20008000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1814
    const-string v3, "caller"

    const-string v4, "event"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1815
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v3, v3, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    iget-object v3, v3, Lcom/android/calendar/event/ay;->g:Landroid/widget/ImageButton;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1817
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/bn;->a:Lcom/android/calendar/event/bm;

    iget-object v3, v3, Lcom/android/calendar/event/bm;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v3

    const/16 v4, 0x72

    invoke-virtual {v3, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method
