.class Lcom/android/calendar/event/kc;
.super Ljava/lang/Object;
.source "SelectMapChinaActivity.java"

# interfaces
.implements Lcom/amap/api/location/AMapLocationListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectMapChinaActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V
    .locals 0

    .prologue
    .line 933
    iput-object p1, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/SelectMapChinaActivity;Lcom/android/calendar/event/jq;)V
    .locals 0

    .prologue
    .line 933
    invoke-direct {p0, p1}, Lcom/android/calendar/event/kc;-><init>(Lcom/android/calendar/event/SelectMapChinaActivity;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 962
    return-void
.end method

.method public onLocationChanged(Lcom/amap/api/location/AMapLocation;)V
    .locals 4

    .prologue
    .line 936
    if-nez p1, :cond_1

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 940
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->o(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->m(Lcom/android/calendar/event/SelectMapChinaActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 941
    :cond_2
    invoke-static {}, Lcom/android/calendar/event/SelectMapChinaActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 942
    const-string v0, "SelectMapChinaActivityAMAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLocationChanged_Provider:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Latitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Longitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Accuracy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getAccuracy()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 947
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 948
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 949
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->e:Landroid/location/Location;

    invoke-virtual {p1}, Lcom/amap/api/location/AMapLocation;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setBearing(F)V

    .line 951
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v0, v0, Lcom/android/calendar/event/SelectMapChinaActivity;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 955
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->z(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/location/LocationManagerProxy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 956
    iget-object v0, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapChinaActivity;->z(Lcom/android/calendar/event/SelectMapChinaActivity;)Lcom/amap/api/location/LocationManagerProxy;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/kc;->a:Lcom/android/calendar/event/SelectMapChinaActivity;

    iget-object v1, v1, Lcom/android/calendar/event/SelectMapChinaActivity;->d:Lcom/android/calendar/event/kc;

    invoke-virtual {v0, v1}, Lcom/amap/api/location/LocationManagerProxy;->removeUpdates(Lcom/amap/api/location/AMapLocationListener;)V

    goto/16 :goto_0
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 966
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 970
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 974
    return-void
.end method
