.class Lcom/android/calendar/event/cr;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 4407
    iput-object p1, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4410
    if-eqz p2, :cond_2

    .line 4411
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ex/a/b;

    .line 4413
    if-eqz v0, :cond_2

    .line 4414
    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/hj;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v2, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v2, v2, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4416
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4417
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f024b

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4430
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->U:Landroid/widget/MultiAutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 4431
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;Z)Z

    .line 4434
    :cond_2
    return-void

    .line 4421
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/ex/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/ex/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/event/AttendeesView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 4424
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->o(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4425
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 4427
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/cr;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->p(Lcom/android/calendar/event/ay;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method
