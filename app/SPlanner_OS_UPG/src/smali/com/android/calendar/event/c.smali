.class public Lcom/android/calendar/event/c;
.super Landroid/content/BroadcastReceiver;
.source "AccountValidationCheckActivity.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/AccountValidationCheckActivity;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 288
    const-string v0, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Get result of validation check"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->j(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->j(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->j(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 293
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 297
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 300
    :cond_1
    const-string v0, "result_code"

    const/16 v1, -0x3e7

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 303
    if-ne v0, v3, :cond_a

    .line 304
    :try_start_0
    const-string v0, "validation_result"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 307
    if-eqz v0, :cond_4

    .line 308
    const-string v1, "AccountValidationCheckActivity"

    const-string v2, "Validation is OK"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v2, "preferences_samsung_account_validation"

    invoke-static {v1, v2, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 313
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 407
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 408
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 409
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0, v4}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V

    .line 414
    :cond_3
    return-void

    .line 315
    :cond_4
    :try_start_1
    const-string v1, "check_list"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 328
    and-int/lit8 v2, v1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 329
    const-string v2, "AccountValidationCheckActivity"

    const-string v3, "require Tnc Agreement"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_5
    and-int/lit8 v2, v1, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_6

    .line 332
    const-string v2, "AccountValidationCheckActivity"

    const-string v3, "require Name Verification"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_6
    and-int/lit8 v2, v1, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_7

    .line 335
    const-string v2, "AccountValidationCheckActivity"

    const-string v3, "require  E-mail Certificate"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_7
    and-int/lit8 v2, v1, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_8

    .line 338
    const-string v2, "AccountValidationCheckActivity"

    const-string v3, "need to fill out required fields"

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_8
    if-lez v1, :cond_2

    .line 342
    const-string v1, "REQUIRED_PROCESS_ACTION"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    invoke-virtual {p2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    :try_start_2
    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    :goto_1
    :try_start_3
    const-string v1, "AccountValidationCheckActivity"

    const-string v2, "Request to validate"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const-string v2, "preferences_samsung_account_validation"

    invoke-static {v1, v2, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 407
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 408
    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_9

    .line 409
    iget-object v1, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v1, v4}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V

    :cond_9
    throw v0

    .line 350
    :catch_0
    move-exception v1

    .line 351
    :try_start_4
    const-string v1, "AccountValidationCheckActivity"

    const-string v2, "Error: Could not find REQUIRED_PROCESS_ACTION activity."

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 361
    :cond_a
    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 362
    const-string v0, "error_message"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    const-string v1, "AccountValidationCheckActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to get validation info : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const v1, 0x7f0f01e9

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 367
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;I)V

    goto/16 :goto_0

    .line 369
    :cond_b
    const-string v0, "error_message"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370
    const-string v1, "AccountValidationCheckActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Network Error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    iget-object v0, p0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f00b1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f009e

    new-instance v3, Lcom/android/calendar/event/e;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/e;-><init>(Lcom/android/calendar/event/c;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/d;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/d;-><init>(Lcom/android/calendar/event/c;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
