.class Lcom/android/calendar/event/hk;
.super Landroid/widget/ResourceCursorAdapter;
.source "EditTaskView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 2200
    const v0, 0x7f04001e

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    .line 2201
    const v0, 0x7f04001d

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/hk;->setDropDownViewResource(I)V

    .line 2202
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 2206
    const v0, 0x7f120087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2207
    const-string v1, "_sync_account_key"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 2208
    const-string v2, "_sync_account"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 2209
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2210
    if-eqz v0, :cond_0

    .line 2211
    invoke-static {}, Lcom/android/calendar/event/fx;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/android/calendar/task/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2214
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2216
    const v0, 0x7f120088

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2217
    if-eqz v0, :cond_2

    .line 2218
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2220
    if-nez v3, :cond_1

    .line 2221
    const v1, 0x7f0f0288

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2223
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2226
    :cond_2
    const v0, 0x7f120089

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2227
    if-eqz v0, :cond_3

    .line 2228
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2231
    :cond_3
    const v0, 0x7f12008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2232
    if-eqz v0, :cond_4

    .line 2233
    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 2235
    :cond_4
    return-void
.end method
