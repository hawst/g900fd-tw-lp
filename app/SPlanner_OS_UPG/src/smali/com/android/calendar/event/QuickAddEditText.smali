.class public Lcom/android/calendar/event/QuickAddEditText;
.super Landroid/widget/EditText;
.source "QuickAddEditText.java"


# instance fields
.field private a:Lcom/android/calendar/event/iu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/QuickAddEditText;->a:Lcom/android/calendar/event/iu;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/QuickAddEditText;->a:Lcom/android/calendar/event/iu;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/QuickAddEditText;->a:Lcom/android/calendar/event/iu;

    .line 25
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 30
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/android/calendar/event/QuickAddEditText;->a:Lcom/android/calendar/event/iu;

    invoke-interface {v0}, Lcom/android/calendar/event/iu;->a()V

    .line 33
    :cond_0
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOnBackKeyListener(Lcom/android/calendar/event/iu;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/android/calendar/event/QuickAddEditText;->a:Lcom/android/calendar/event/iu;

    .line 40
    return-void
.end method
