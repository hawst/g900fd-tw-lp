.class Lcom/android/calendar/event/a;
.super Ljava/lang/Object;
.source "AccountValidationCheckActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/AccountValidationCheckActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {p2}, Lcom/b/a/a/e;->a(Landroid/os/IBinder;)Lcom/b/a/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/d;)Lcom/b/a/a/d;

    .line 145
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    new-instance v1, Lcom/android/calendar/event/f;

    iget-object v2, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-direct {v1, v2}, Lcom/android/calendar/event/f;-><init>(Lcom/android/calendar/event/AccountValidationCheckActivity;)V

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/a;)Lcom/b/a/a/a;

    .line 147
    :try_start_0
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    iget-object v1, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->h(Lcom/android/calendar/event/AccountValidationCheckActivity;)Lcom/b/a/a/d;

    move-result-object v1

    const-string v2, "tivhn39mr9"

    const-string v3, "348652748C0787A06903EDBAAF3359E3"

    iget-object v4, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-virtual {v4}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v5}, Lcom/android/calendar/event/AccountValidationCheckActivity;->g(Lcom/android/calendar/event/AccountValidationCheckActivity;)Lcom/b/a/a/a;

    move-result-object v5

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/b/a/a/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/b/a/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/AccountValidationCheckActivity;->c(Lcom/android/calendar/event/AccountValidationCheckActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 152
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 154
    array-length v0, v0

    if-lez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->h(Lcom/android/calendar/event/AccountValidationCheckActivity;)Lcom/b/a/a/d;

    move-result-object v0

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->i(Lcom/android/calendar/event/AccountValidationCheckActivity;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/b/a/a/d;->b(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 165
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/d;)Lcom/b/a/a/d;

    .line 167
    iget-object v0, p0, Lcom/android/calendar/event/a;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0, v2}, Lcom/android/calendar/event/AccountValidationCheckActivity;->a(Lcom/android/calendar/event/AccountValidationCheckActivity;Lcom/b/a/a/a;)Lcom/b/a/a/a;

    .line 168
    return-void
.end method
