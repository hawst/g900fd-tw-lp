.class Lcom/android/calendar/event/jp;
.super Landroid/os/AsyncTask;
.source "SelectMapActivity.java"


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/calendar/event/SelectMapActivity;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/SelectMapActivity;)V
    .locals 1

    .prologue
    .line 398
    iput-object p1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 400
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/jp;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/SelectMapActivity;Lcom/android/calendar/event/je;)V
    .locals 0

    .prologue
    .line 398
    invoke-direct {p0, p1}, Lcom/android/calendar/event/jp;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Object;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 404
    .line 406
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-object v7

    .line 410
    :cond_1
    aget-object v0, p1, v1

    instance-of v0, v0, Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v0, :cond_2

    .line 411
    const-string v0, "SelectMapActivity"

    const-string v1, "SearchFromGeoPoint start"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    .line 414
    const-string v1, "SelectMapActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Latitude:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Longitude:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->c(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/Geocoder;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    move-object v7, v0

    .line 419
    goto :goto_0

    .line 416
    :catch_0
    move-exception v0

    .line 417
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v7

    goto :goto_1

    .line 420
    :cond_2
    aget-object v0, p1, v1

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 421
    const-string v0, "SelectMapActivity"

    const-string v1, "SearchFromKeyword start"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v0, 0x0

    :try_start_1
    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 425
    const-string v1, "SelectMapActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyword : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->c(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/Geocoder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v7

    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/jp;->a:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 429
    :catch_1
    move-exception v0

    .line 430
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 440
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 441
    const-string v1, "SelectMapActivity"

    const-string v2, "AddressList has result"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    iget-object v1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/maps/c;->c()V

    :cond_0
    move v1, v0

    .line 445
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 446
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 447
    if-eqz v0, :cond_2

    .line 448
    iget-object v2, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v2, v0}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/android/calendar/event/SelectMapActivity;Landroid/location/Address;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    .line 449
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    .line 450
    iget-object v3, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    iget-object v4, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v4, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Landroid/location/Address;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Ljava/lang/String;)V

    .line 451
    iget-object v3, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v3, v2}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Z

    .line 453
    :cond_1
    iget-object v3, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-virtual {v0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v2, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)Z

    .line 445
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 456
    :cond_3
    iget-boolean v1, p0, Lcom/android/calendar/event/jp;->a:Z

    if-eqz v1, :cond_4

    .line 457
    iget-object v1, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-virtual {v3}, Lcom/android/calendar/event/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0290

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/jp;->b:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapActivity;->h(Lcom/android/calendar/event/SelectMapActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 461
    :cond_4
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 398
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/jp;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 398
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/android/calendar/event/jp;->a(Ljava/util/List;)V

    return-void
.end method
