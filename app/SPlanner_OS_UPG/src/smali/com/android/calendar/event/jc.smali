.class Lcom/android/calendar/event/jc;
.super Landroid/content/AsyncQueryHandler;
.source "QuickAddFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/iv;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/iv;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/android/calendar/event/jc;->a:Lcom/android/calendar/event/iv;

    .line 640
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 641
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 645
    if-nez p3, :cond_0

    .line 676
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/jc;->a:Lcom/android/calendar/event/iv;

    invoke-virtual {v0}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 650
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 651
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 655
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 673
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 658
    :pswitch_0
    :try_start_0
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 659
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 660
    :cond_3
    sget-object v1, Lcom/android/calendar/event/iv;->a:Ljava/lang/String;

    const-string v2, "setCalendarsCursor : no calendar account found "

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 665
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/event/jc;->a:Lcom/android/calendar/event/iv;

    iget-object v2, p0, Lcom/android/calendar/event/jc;->a:Lcom/android/calendar/event/iv;

    invoke-static {v2, v0}, Lcom/android/calendar/event/iv;->a(Lcom/android/calendar/event/iv;Landroid/database/Cursor;)I

    move-result v2

    iput v2, v1, Lcom/android/calendar/event/iv;->b:I

    .line 666
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 668
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 655
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
