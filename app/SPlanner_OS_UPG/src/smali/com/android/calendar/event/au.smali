.class Lcom/android/calendar/event/au;
.super Landroid/content/AsyncQueryHandler;
.source "EditEventFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ak;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ak;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    .line 157
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 158
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    .line 163
    if-nez p3, :cond_0

    .line 506
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 175
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 503
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 177
    :sswitch_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 180
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 181
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/as;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/as;->a(I)V

    .line 182
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;Z)Z

    .line 183
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    goto :goto_0

    .line 186
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    new-instance v1, Lcom/android/calendar/as;

    invoke-direct {v1}, Lcom/android/calendar/as;-><init>()V

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;Lcom/android/calendar/as;)Lcom/android/calendar/as;

    .line 187
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 188
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;Landroid/database/Cursor;)V

    .line 189
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 191
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->a:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->d(Lcom/android/calendar/event/ak;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->x:J

    .line 195
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->e(Lcom/android/calendar/event/ak;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->A:J

    .line 196
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->d(Lcom/android/calendar/event/ak;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-wide v4, v0, Lcom/android/calendar/as;->y:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, Lcom/android/calendar/as;->w:Z

    .line 197
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 198
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->d(Lcom/android/calendar/event/ak;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 199
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->e(Lcom/android/calendar/event/ak;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v8, v0, Lcom/android/calendar/as;->b:J

    .line 205
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v2, v2, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0, v8, v9, v1, v2}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;JLcom/android/calendar/as;Lcom/android/calendar/as;)Z

    .line 208
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->W:Z

    if-eqz v0, :cond_c

    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-eqz v0, :cond_c

    .line 209
    sget-object v3, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    .line 210
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 213
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/event/av;->g:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->H:Z

    if-eqz v0, :cond_d

    .line 223
    sget-object v3, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    .line 224
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 227
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    sget-object v4, Lcom/android/calendar/event/av;->c:[Ljava/lang/String;

    const-string v5, "event_id=?"

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/calendar/event/at;->d:J

    const-wide/32 v2, 0x200000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x200000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 245
    :cond_a
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 247
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    goto/16 :goto_0

    .line 196
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 218
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    goto/16 :goto_2

    .line 232
    :cond_d
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    goto/16 :goto_3

    .line 253
    :cond_e
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->C(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 254
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND calendar_displayName!=\'Direct Share\' AND deleted!=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 265
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 266
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 272
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1 AND account_type!=\'com.osp.app.signin\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 282
    :cond_11
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 285
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "_id=?"

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 297
    :goto_5
    :sswitch_1
    :try_start_0
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 298
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 299
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 300
    const/4 v2, 0x4

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 301
    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 303
    const/4 v4, 0x2

    if-ne v3, v4, :cond_13

    .line 304
    if-eqz v1, :cond_12

    .line 305
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput-object v1, v3, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 306
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v4, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, v4, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/calendar/as;->v:Z

    .line 308
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v3

    iput-object v1, v3, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 309
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v4}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v4

    iget-object v4, v4, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/calendar/as;->v:Z

    .line 313
    :cond_12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 314
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v4, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, v4, Lcom/android/calendar/as;->t:Ljava/lang/String;

    iput-object v4, v3, Lcom/android/calendar/as;->u:Ljava/lang/String;

    .line 315
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v4}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v4

    iget-object v4, v4, Lcom/android/calendar/as;->t:Ljava/lang/String;

    iput-object v4, v3, Lcom/android/calendar/as;->u:Ljava/lang/String;

    .line 322
    :cond_13
    :goto_6
    if-eqz v1, :cond_15

    .line 323
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 325
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 327
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput v0, v1, Lcom/android/calendar/as;->Y:I

    .line 328
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput v2, v1, Lcom/android/calendar/as;->X:I

    .line 329
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v1

    iput v0, v1, Lcom/android/calendar/as;->Y:I

    .line 330
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iput v2, v0, Lcom/android/calendar/as;->X:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    .line 340
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 317
    :cond_14
    :try_start_1
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v3, v3, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput-object v0, v3, Lcom/android/calendar/as;->u:Ljava/lang/String;

    .line 318
    iget-object v3, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v3}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v3

    iput-object v0, v3, Lcom/android/calendar/as;->u:Ljava/lang/String;

    goto :goto_6

    .line 334
    :cond_15
    new-instance v3, Lcom/android/calendar/at;

    invoke-direct {v3, v0, v1}, Lcom/android/calendar/at;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iput v2, v3, Lcom/android/calendar/at;->c:I

    .line 336
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-virtual {v0, v3}, Lcom/android/calendar/as;->a(Lcom/android/calendar/at;)V

    .line 337
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/calendar/as;->a(Lcom/android/calendar/at;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_5

    .line 340
    :cond_16
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 343
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    goto/16 :goto_0

    .line 348
    :goto_7
    :sswitch_2
    :try_start_2
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 349
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 350
    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 351
    invoke-static {v0, v1}, Lcom/android/calendar/au;->a(II)Lcom/android/calendar/au;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v1, v1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_7

    .line 360
    :catchall_1
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 357
    :cond_17
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 358
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 360
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 363
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    goto/16 :goto_0

    .line 367
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-boolean v0, v0, Lcom/android/calendar/aq;->m:Z

    if-nez v0, :cond_19

    :cond_18
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->c:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1e

    .line 371
    :cond_19
    const/4 v0, -0x1

    .line 373
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    .line 381
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v1

    if-eqz v1, :cond_1c

    .line 382
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-boolean v1, v0, Lcom/android/calendar/aq;->m:Z

    .line 383
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget v0, v0, Lcom/android/calendar/aq;->n:I

    .line 388
    :goto_8
    iget-object v2, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v2, v2, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v2, Lcom/android/calendar/as;->c:J

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-eqz v2, :cond_2d

    .line 389
    const/4 v1, 0x1

    .line 390
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v0, Lcom/android/calendar/as;->c:J

    invoke-static {v4, v5, p3}, Lcom/android/calendar/event/hm;->a(JLandroid/database/Cursor;)I

    move-result v0

    move v2, v1

    .line 397
    :goto_9
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2c

    invoke-virtual {v3}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-gt v1, v0, :cond_2c

    .line 399
    const/4 v0, 0x0

    move v1, v0

    .line 402
    :goto_a
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v4, v3, v0, v2, v1}, Lcom/android/calendar/event/ay;->a(Landroid/database/Cursor;ZZI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 448
    :cond_1a
    :goto_c
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 450
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preference_defaultCalendar_account_type"

    const-string v2, "LOCAL"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 452
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 453
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;I)I

    .line 455
    :cond_1b
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    .line 456
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->j(Lcom/android/calendar/event/ak;)V

    .line 458
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;

    move-result-object v0

    const/16 v1, 0x10

    const/4 v2, 0x0

    sget-object v3, Landroid/provider/CalendarContract$Colors;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->h:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 385
    :cond_1c
    const/4 v1, 0x0

    goto :goto_8

    .line 402
    :cond_1d
    const/4 v0, 0x0

    goto :goto_b

    .line 405
    :cond_1e
    :try_start_5
    const-string v0, ""

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->az:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    if-eqz v0, :cond_22

    :cond_1f
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v2, 0x200000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_22

    :cond_20
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/calendar/event/at;->d:J

    const-wide/32 v2, 0x200000

    cmp-long v0, v0, v2

    if-eqz v0, :cond_22

    :cond_21
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 414
    :cond_22
    const/4 v0, -0x1

    .line 416
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v1

    if-eqz v1, :cond_23

    .line 417
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget-boolean v1, v0, Lcom/android/calendar/aq;->m:Z

    .line 418
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;

    move-result-object v0

    iget v0, v0, Lcom/android/calendar/aq;->n:I

    .line 423
    :goto_d
    iget-object v2, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v2, v2, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2b

    .line 424
    const/4 v1, 0x1

    .line 425
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v0, Lcom/android/calendar/as;->c:J

    invoke-static {v2, v3, p3}, Lcom/android/calendar/event/hm;->a(JLandroid/database/Cursor;)I

    move-result v0

    move v2, v1

    move v1, v0

    .line 430
    :goto_e
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v3

    .line 431
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v4, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-virtual {v0}, Lcom/android/calendar/event/ak;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    :goto_f
    invoke-virtual {v4, v3, v0, v2, v1}, Lcom/android/calendar/event/ay;->a(Landroid/database/Cursor;ZZI)V

    .line 434
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;Landroid/database/Cursor;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_c

    .line 448
    :catchall_2
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 420
    :cond_23
    const/4 v1, 0x0

    goto :goto_d

    .line 431
    :cond_24
    const/4 v0, 0x0

    goto :goto_f

    .line 437
    :cond_25
    :try_start_6
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;Landroid/database/Cursor;)Z

    .line 438
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    invoke-static {v0}, Lcom/android/calendar/event/ak;->b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/android/calendar/event/av;->b(Lcom/android/calendar/as;Landroid/database/Cursor;)Z

    .line 439
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_1a

    .line 440
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->f(Ljava/lang/String;)V

    .line 441
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 443
    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ay;->g(Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->f:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->e(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto/16 :goto_c

    .line 464
    :sswitch_4
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 465
    new-instance v0, Lcom/android/calendar/colorpicker/e;

    invoke-direct {v0}, Lcom/android/calendar/colorpicker/e;-><init>()V

    .line 466
    new-instance v1, Lcom/android/calendar/colorpicker/e;

    invoke-direct {v1}, Lcom/android/calendar/colorpicker/e;-><init>()V

    .line 468
    :cond_26
    const/4 v2, 0x4

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 469
    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 470
    const/4 v4, 0x5

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 471
    invoke-static {v3}, Lcom/android/calendar/hj;->b(I)I

    move-result v3

    .line 472
    const/4 v5, 0x1

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 474
    const/4 v6, 0x2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 477
    const/4 v7, 0x1

    if-ne v4, v7, :cond_29

    .line 478
    invoke-virtual {v0, v5, v6, v3, v2}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 485
    :goto_10
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_26

    .line 486
    new-instance v2, Lcom/android/calendar/colorpicker/l;

    invoke-direct {v2}, Lcom/android/calendar/colorpicker/l;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/calendar/colorpicker/e;->a(Ljava/util/Comparator;)V

    .line 487
    new-instance v2, Lcom/android/calendar/colorpicker/l;

    invoke-direct {v2}, Lcom/android/calendar/colorpicker/l;-><init>()V

    invoke-virtual {v1, v2}, Lcom/android/calendar/colorpicker/e;->a(Ljava/util/Comparator;)V

    .line 489
    iget-object v2, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v2, v2, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput-object v0, v2, Lcom/android/calendar/as;->aA:Lcom/android/calendar/colorpicker/e;

    .line 490
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput-object v1, v0, Lcom/android/calendar/as;->aB:Lcom/android/calendar/colorpicker/e;

    .line 492
    :cond_27
    if-eqz p3, :cond_28

    .line 493
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 496
    :cond_28
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->a(Lcom/android/calendar/event/ak;I)V

    .line 498
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v1, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->h:I

    if-nez v0, :cond_2a

    const/4 v0, 0x0

    :goto_11
    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ay;->c(I)V

    goto/16 :goto_0

    .line 481
    :cond_29
    invoke-virtual {v1, v5, v6, v3, v2}, Lcom/android/calendar/colorpicker/e;->a(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_10

    .line 498
    :cond_2a
    iget-object v0, p0, Lcom/android/calendar/event/au;->a:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget v0, v0, Lcom/android/calendar/as;->h:I

    invoke-static {v0}, Lcom/android/calendar/hj;->b(I)I

    move-result v0

    goto :goto_11

    :cond_2b
    move v2, v1

    move v1, v0

    goto/16 :goto_e

    :cond_2c
    move v1, v0

    goto/16 :goto_a

    :cond_2d
    move v2, v1

    goto/16 :goto_9

    .line 175
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method
