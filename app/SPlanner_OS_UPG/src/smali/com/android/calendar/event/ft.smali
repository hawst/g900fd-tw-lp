.class Lcom/android/calendar/event/ft;
.super Ljava/lang/Object;
.source "EditTaskFragment.java"

# interfaces
.implements Lcom/android/calendar/event/fw;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fq;

.field private b:I


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fq;)V
    .locals 1

    .prologue
    .line 484
    iput-object p1, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ft;->b:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 489
    iput p1, p0, Lcom/android/calendar/event/ft;->b:I

    .line 490
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 496
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;Z)Z

    .line 498
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-boolean v0, v0, Lcom/android/calendar/hh;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v0, v0, Lcom/android/calendar/hh;->d:I

    iget-object v3, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v3, v3, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v3, v3, Lcom/android/calendar/hh;->e:I

    if-eq v0, v3, :cond_0

    .line 500
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v4, v0, Lcom/android/calendar/hh;->b:J

    .line 502
    sget-object v0, Lcom/android/calendar/he;->a:Landroid/net/Uri;

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 503
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->i(Lcom/android/calendar/event/fq;)Landroid/content/AsyncQueryHandler;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/AsyncQueryHandler;->startDelete(ILjava/lang/Object;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iput-object v2, v0, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    .line 506
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iput v1, v0, Lcom/android/calendar/hh;->F:I

    .line 509
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/ft;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0}, Lcom/android/calendar/event/fx;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->j(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fv;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v2, v2, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v3, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    iget-object v3, v3, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/event/fv;->a(Lcom/android/calendar/hh;Lcom/android/calendar/event/fx;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    const v0, 0x7f0f01d9

    .line 514
    iget-object v2, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v2}, Lcom/android/calendar/event/fq;->d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 516
    sget-object v0, Lcom/android/calendar/hj;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v2}, Lcom/android/calendar/event/fq;->d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 517
    invoke-static {}, Lcom/android/calendar/dz;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 518
    const-string v0, "GATE"

    const-string v2, "<GATE-M>TASK_CREATED</GATE-M>"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->g(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ft;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 526
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_2

    .line 528
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 534
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 535
    iget-object v0, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_3

    .line 537
    iget-object v2, p0, Lcom/android/calendar/event/ft;->a:Lcom/android/calendar/event/fq;

    invoke-static {v2}, Lcom/android/calendar/event/fq;->k(Lcom/android/calendar/event/fq;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 538
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 541
    :cond_3
    return-void
.end method
