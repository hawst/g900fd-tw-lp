.class Lcom/android/calendar/event/e;
.super Ljava/lang/Object;
.source "AccountValidationCheckActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/c;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/c;)V
    .locals 0

    .prologue
    .line 374
    iput-object p1, p0, Lcom/android/calendar/event/e;->a:Lcom/android/calendar/event/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 379
    iget-object v0, p0, Lcom/android/calendar/event/e;->a:Lcom/android/calendar/event/c;

    iget-object v0, v0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/e;->a:Lcom/android/calendar/event/c;

    iget-object v0, v0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/android/calendar/event/e;->a:Lcom/android/calendar/event/c;

    iget-object v0, v0, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-static {v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->k(Lcom/android/calendar/event/AccountValidationCheckActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 382
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 384
    const-string v1, "extra_prefs_show_button_bar"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 386
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/e;->a:Lcom/android/calendar/event/c;

    iget-object v1, v1, Lcom/android/calendar/event/c;->a:Lcom/android/calendar/event/AccountValidationCheckActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/AccountValidationCheckActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 387
    :catch_0
    move-exception v0

    .line 388
    const-string v0, "AccountValidationCheckActivity"

    const-string v1, "Error: Could not find Settings.ACTION_WIFI_SETTINGS activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
