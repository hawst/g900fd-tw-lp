.class Lcom/android/calendar/event/cq;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/cp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/cp;)V
    .locals 0

    .prologue
    .line 4368
    iput-object p1, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 4371
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v1, v1, Lcom/android/calendar/event/cp;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 4372
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v1, v1, Lcom/android/calendar/event/cp;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4374
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v1, v1, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->g(Lcom/android/calendar/event/ay;I)V

    .line 4376
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 4377
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v1, v1, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 4378
    const v1, 0x7f12014e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 4379
    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 4387
    :goto_0
    return-void

    .line 4381
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->q(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4382
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    .line 4384
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/cq;->a:Lcom/android/calendar/event/cp;

    iget-object v0, v0, Lcom/android/calendar/event/cp;->e:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->T:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    goto :goto_0
.end method
