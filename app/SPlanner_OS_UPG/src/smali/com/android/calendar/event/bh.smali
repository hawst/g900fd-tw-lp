.class Lcom/android/calendar/event/bh;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 1564
    iput-object p1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ILandroid/widget/AdapterView;)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v6, 0x4

    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 1588
    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1590
    const v1, 0x7f040082

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 1594
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1, v7}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;I)I

    .line 1596
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    iget-object v2, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1597
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1598
    iget-object v2, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1601
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v3, v1, Landroid/text/format/Time;->year:I

    .line 1602
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v2, v1, Landroid/text/format/Time;->month:I

    .line 1603
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    .line 1608
    iget-object v4, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v4}, Lcom/android/calendar/d/g;->c()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->C(Lcom/android/calendar/event/ay;)I

    move-result v4

    if-lez v4, :cond_5

    .line 1609
    iget-object v4, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v4

    invoke-virtual {v4, v3, v2, v1}, Lcom/android/calendar/d/a/a;->a(III)V

    .line 1611
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v4

    .line 1612
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v3

    .line 1613
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I

    move-result v2

    .line 1614
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->d()Z

    move-result v1

    .line 1616
    if-le p1, v7, :cond_3

    if-ge p1, v6, :cond_3

    .line 1617
    add-int/lit8 v6, p1, 0x1

    move v9, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v9

    .line 1625
    :goto_0
    packed-switch v6, :pswitch_data_0

    .line 1650
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    .line 1656
    :goto_1
    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    iget-object v5, v5, Lcom/android/calendar/event/ay;->aH:Lcom/android/calendar/d/g;

    invoke-virtual {v5}, Lcom/android/calendar/d/g;->c()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->C(Lcom/android/calendar/event/ay;)I

    move-result v5

    if-lez v5, :cond_1

    .line 1658
    const/16 v5, 0xb

    if-le v2, v5, :cond_0

    .line 1660
    add-int/lit8 v2, v2, -0xc

    .line 1661
    add-int/lit8 v3, v3, 0x1

    .line 1663
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v5

    invoke-virtual {v5, v3, v2, v1, v4}, Lcom/android/calendar/d/a/a;->a(IIIZ)V

    .line 1666
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->a()I

    move-result v3

    .line 1667
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->b()I

    move-result v2

    .line 1668
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->D(Lcom/android/calendar/event/ay;)Lcom/android/calendar/d/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/calendar/d/a/a;->c()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1677
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v4}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v4

    invoke-virtual {v4, v1, v2, v3}, Landroid/text/format/Time;->set(III)V

    .line 1679
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    invoke-static {v1}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 1680
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    const v2, 0x259d23

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1683
    :cond_2
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0379

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02e3

    new-instance v2, Lcom/android/calendar/event/bj;

    invoke-direct {v2, p0, p1}, Lcom/android/calendar/event/bj;-><init>(Lcom/android/calendar/event/bh;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v2, Lcom/android/calendar/event/bi;

    invoke-direct {v2, p0, p2}, Lcom/android/calendar/event/bi;-><init>(Lcom/android/calendar/event/bh;Landroid/widget/AdapterView;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1704
    return-object v0

    .line 1618
    :cond_3
    if-lt p1, v6, :cond_4

    .line 1619
    add-int/lit8 v6, p1, 0x2

    move v9, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v9

    goto/16 :goto_0

    .line 1627
    :pswitch_1
    iget-object v6, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v6, v5}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;I)I

    .line 1628
    add-int/lit8 v2, v2, 0x1

    .line 1629
    goto/16 :goto_1

    .line 1634
    :pswitch_2
    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5, v7}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;I)I

    .line 1635
    add-int/lit8 v2, v2, 0x6

    .line 1636
    goto/16 :goto_1

    .line 1640
    :pswitch_3
    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5, v8}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;I)I

    .line 1641
    add-int/lit8 v3, v3, 0x1

    .line 1642
    goto/16 :goto_1

    .line 1645
    :pswitch_4
    iget-object v5, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v5, v8}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;I)I

    .line 1646
    add-int/lit8 v3, v3, 0x5

    .line 1647
    goto/16 :goto_1

    .line 1669
    :catch_0
    move-exception v1

    .line 1670
    const-string v2, "EditEvent"

    const-string v3, "Wrong date argument in solarLunar converter"

    invoke-static {v2, v3, v1}, Lcom/android/calendar/ey;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1671
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v3, v1, Landroid/text/format/Time;->year:I

    .line 1672
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v2, v1, Landroid/text/format/Time;->month:I

    .line 1673
    iget-object v1, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v1

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_4
    move v6, p1

    move v9, v2

    move v2, v3

    move v3, v4

    move v4, v1

    move v1, v9

    goto/16 :goto_0

    :cond_5
    move v4, v5

    move v6, p1

    goto/16 :goto_0

    .line 1625
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 1569
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->A(Lcom/android/calendar/event/ay;)I

    move-result v0

    if-eq p3, v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->B(Lcom/android/calendar/event/ay;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1571
    if-eqz p2, :cond_0

    .line 1572
    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 1574
    :cond_0
    invoke-direct {p0, p3, p1}, Lcom/android/calendar/event/bh;->a(ILandroid/widget/AdapterView;)Landroid/app/Dialog;

    move-result-object v0

    .line 1575
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1579
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;Z)Z

    .line 1580
    return-void

    .line 1576
    :cond_2
    if-nez p3, :cond_1

    .line 1577
    iget-object v0, p0, Lcom/android/calendar/event/bh;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0, p3}, Lcom/android/calendar/event/ay;->c(Lcom/android/calendar/event/ay;I)I

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 1584
    return-void
.end method
