.class Lcom/android/calendar/event/iz;
.super Ljava/lang/Object;
.source "QuickAddFragment.java"

# interfaces
.implements Lcom/samsung/android/hermes/HermesServiceManager$IHermesCallBack;


# instance fields
.field final synthetic a:Lcom/samsung/android/hermes/HermesServiceManager;

.field final synthetic b:Lcom/android/calendar/event/iv;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/iv;Lcom/samsung/android/hermes/HermesServiceManager;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    iput-object p2, p0, Lcom/android/calendar/event/iz;->a:Lcom/samsung/android/hermes/HermesServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Object;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 385
    iget-object v0, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v0, v1}, Lcom/android/calendar/event/iv;->a(Lcom/android/calendar/event/iv;Z)Z

    .line 386
    iget-object v0, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-virtual {v0}, Lcom/android/calendar/event/iv;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    :goto_0
    return-void

    .line 389
    :cond_0
    if-nez p1, :cond_1

    .line 390
    iget-object v0, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->f(Lcom/android/calendar/event/iv;)V

    goto :goto_0

    .line 394
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/iz;->a:Lcom/samsung/android/hermes/HermesServiceManager;

    monitor-enter v2

    .line 395
    :try_start_0
    instance-of v0, p1, Lcom/samsung/android/hermes/HermesServiceManager$HermesResult;

    if-eqz v0, :cond_7

    .line 396
    check-cast p1, Lcom/samsung/android/hermes/HermesServiceManager$HermesResult;

    invoke-virtual {p1}, Lcom/samsung/android/hermes/HermesServiceManager$HermesResult;->getData()Ljava/lang/Object;

    move-result-object v0

    .line 397
    instance-of v1, v0, Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 398
    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/hermes/KerykeionResult;

    .line 399
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getResultType()I

    move-result v1

    sget-object v4, Lcom/samsung/android/hermes/HermesServiceManager$AnalyzerResultType;->Time:Lcom/samsung/android/hermes/HermesServiceManager$AnalyzerResultType;

    invoke-virtual {v4}, Lcom/samsung/android/hermes/HermesServiceManager$AnalyzerResultType;->ordinal()I

    move-result v4

    if-eq v1, v4, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getResultType()I

    move-result v1

    const/16 v4, 0x15

    if-ne v1, v4, :cond_2

    .line 400
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getExtraDatas()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 402
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-virtual {v5}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 403
    new-instance v5, Landroid/text/format/Time;

    iget-object v6, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-virtual {v6}, Lcom/android/calendar/event/iv;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 405
    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 406
    iget-object v6, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v6}, Lcom/android/calendar/event/iv;->i(Lcom/android/calendar/event/iv;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 407
    iget v6, v5, Landroid/text/format/Time;->hour:I

    iput v6, v4, Landroid/text/format/Time;->hour:I

    .line 408
    iget v6, v5, Landroid/text/format/Time;->minute:I

    iput v6, v4, Landroid/text/format/Time;->minute:I

    .line 409
    iget-object v6, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Lcom/android/calendar/event/iv;->a(Lcom/android/calendar/event/iv;J)J

    .line 411
    const/4 v6, 0x0

    aget-object v6, v1, v6

    const/4 v7, 0x1

    aget-object v7, v1, v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v6}, Lcom/android/calendar/event/iv;->d(Lcom/android/calendar/event/iv;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 412
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    iget-object v4, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v4}, Lcom/android/calendar/event/iv;->i(Lcom/android/calendar/event/iv;)J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    add-long/2addr v4, v6

    invoke-static {v1, v4, v5}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;J)J

    .line 421
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;Z)Z

    .line 422
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v1}, Lcom/android/calendar/event/iv;->k(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getStartPos()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 423
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 424
    const-string v4, "@"

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 425
    iget-object v4, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    iget-object v5, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v5}, Lcom/android/calendar/event/iv;->k(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v5}, Lcom/android/calendar/event/iv;->k(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getStartPos()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;

    .line 427
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    iget-object v4, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v4}, Lcom/android/calendar/event/iv;->k(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/hermes/KerykeionResult;->getSrc()Ljava/lang/String;

    move-result-object v0

    const-string v5, ""

    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 414
    :cond_5
    const/4 v6, 0x1

    :try_start_1
    aget-object v1, v1, v6

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 415
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v1}, Lcom/android/calendar/event/iv;->j(Lcom/android/calendar/event/iv;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 416
    iget v1, v5, Landroid/text/format/Time;->hour:I

    iput v1, v4, Landroid/text/format/Time;->hour:I

    .line 417
    iget v1, v5, Landroid/text/format/Time;->minute:I

    iput v1, v4, Landroid/text/format/Time;->minute:I

    .line 418
    iget-object v1, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;J)J

    goto/16 :goto_2

    .line 431
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/iz;->b:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->f(Lcom/android/calendar/event/iv;)V

    .line 433
    :cond_7
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
