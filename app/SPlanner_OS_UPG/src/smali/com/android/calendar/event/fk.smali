.class Lcom/android/calendar/event/fk;
.super Landroid/content/AsyncQueryHandler;
.source "EditEventView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 7195
    iput-object p1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    .line 7196
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 7197
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 7202
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7203
    :cond_0
    if-eqz p3, :cond_1

    .line 7204
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 7290
    :cond_1
    :goto_0
    return-void

    .line 7209
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 7211
    if-eqz p3, :cond_d

    .line 7212
    :try_start_0
    const-string v1, "to"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 7213
    const-string v1, "displayName"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 7214
    const-string v1, "mergedFreeBusy"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 7220
    :cond_3
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 7221
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 7222
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 7223
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 7225
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_f

    .line 7226
    :cond_4
    const-string v1, "444444444444444444444444444444444444444444444444"

    move-object v2, v1

    .line 7230
    :goto_2
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v1

    iget-object v1, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 7231
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v8}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0f0295

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v1, v7

    .line 7232
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v1, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 7286
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_5

    .line 7287
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 7234
    :cond_6
    :try_start_1
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->ay:[Ljava/lang/String;

    if-eqz v1, :cond_3

    move v1, v0

    .line 7235
    :goto_3
    iget-object v9, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v9, v9, Lcom/android/calendar/event/ay;->ay:[Ljava/lang/String;

    array-length v9, v9

    if-ge v1, v9, :cond_3

    .line 7236
    iget-object v9, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v9, v9, Lcom/android/calendar/event/ay;->ay:[Ljava/lang/String;

    aget-object v9, v9, v1

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 7237
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 7238
    iget-object v9, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v9, v9, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    add-int/lit8 v10, v1, 0x1

    aput-object v8, v9, v10

    .line 7240
    :cond_7
    iget-object v9, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v9, v9, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    add-int/lit8 v10, v1, 0x1

    aput-object v2, v9, v10

    .line 7235
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 7247
    :cond_9
    const-string v1, "EditEvent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " newNames: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ",  newNames[1]:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v4, v4, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 7249
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->aw:[Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7250
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->ax:[Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7253
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 7254
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 7255
    :goto_4
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 7256
    const-string v5, "444444444444444444444444444444444444444444444444"

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 7257
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7258
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7255
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 7263
    :cond_b
    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_c

    if-eqz v4, :cond_c

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 7265
    :cond_c
    iget-object v0, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->p()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7286
    :cond_d
    :goto_5
    if-eqz p3, :cond_1

    .line 7287
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 7267
    :cond_e
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 7268
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    const-class v3, Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 7269
    const-string v1, "owner_account"

    iget-object v3, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->j(Lcom/android/calendar/event/ay;)Lcom/android/calendar/as;

    move-result-object v3

    iget-object v3, v3, Lcom/android/calendar/as;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7271
    const-string v1, "recipients"

    iget-object v3, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->ao(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 7273
    const-string v1, "timezone"

    iget-object v3, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->g(Lcom/android/calendar/event/ay;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 7274
    const-string v1, "start_time"

    iget-object v3, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7276
    const-string v1, "end_time"

    iget-object v3, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v3}, Lcom/android/calendar/event/ay;->b(Lcom/android/calendar/event/ay;)Landroid/text/format/Time;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 7278
    const-string v1, "name"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 7279
    const-string v1, "schedule"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 7280
    iget-object v1, p0, Lcom/android/calendar/event/fk;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :cond_f
    move-object v2, v1

    goto/16 :goto_2
.end method
