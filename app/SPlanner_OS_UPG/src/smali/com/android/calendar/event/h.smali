.class Lcom/android/calendar/event/h;
.super Ljava/lang/Object;
.source "AttendeesView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Landroid/widget/ImageView;

.field e:Z

.field f:Z

.field g:Landroid/database/Cursor;

.field final synthetic h:Lcom/android/calendar/event/AttendeesView;

.field private i:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/AttendeesView;Lcom/android/calendar/at;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 281
    iput-object p1, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    .line 273
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    .line 274
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/h;->c:Ljava/lang/String;

    .line 275
    iput-object v1, p0, Lcom/android/calendar/event/h;->d:Landroid/widget/ImageView;

    .line 276
    iput-boolean v2, p0, Lcom/android/calendar/event/h;->e:Z

    .line 277
    iput-boolean v2, p0, Lcom/android/calendar/event/h;->f:Z

    .line 278
    iput-object v1, p0, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    .line 279
    iput-object v1, p0, Lcom/android/calendar/event/h;->i:[Ljava/lang/String;

    .line 282
    if-eqz p2, :cond_0

    .line 283
    iget-object v0, p2, Lcom/android/calendar/at;->a:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/calendar/at;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/android/calendar/event/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_0
    if-eqz p3, :cond_1

    .line 286
    iput-object p3, p0, Lcom/android/calendar/event/h;->d:Landroid/widget/ImageView;

    .line 288
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 291
    iput-object p1, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    .line 292
    iput-object p2, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    .line 293
    iget-boolean v0, p0, Lcom/android/calendar/event/h;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/h;->c:Ljava/lang/String;

    .line 299
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/calendar/event/h;->i:[Ljava/lang/String;

    .line 302
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/h;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/h;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 307
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    .line 308
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/h;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/calendar/hj;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 310
    new-array v0, v7, [Ljava/lang/String;

    const-string v2, "contact_id"

    aput-object v2, v0, v6

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->a([Ljava/lang/String;)[Ljava/lang/String;

    .line 311
    const-string v0, "display_name =? AND data1 =? "

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/event/AttendeesView;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/android/calendar/event/AttendeesView;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/calendar/event/h;->i:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    .line 316
    iget-object v0, p0, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/h;->g:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 317
    const v0, 0x7f09000d

    .line 318
    iput-boolean v7, p0, Lcom/android/calendar/event/h;->f:Z

    .line 324
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v2}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/calendar/event/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/i;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/i;-><init>(Lcom/android/calendar/event/h;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 388
    iget-object v0, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/android/calendar/event/h;->h:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v0}, Lcom/android/calendar/event/AttendeesView;->b(Lcom/android/calendar/event/AttendeesView;)Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/j;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/j;-><init>(Lcom/android/calendar/event/h;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 400
    :cond_1
    return-void

    .line 320
    :cond_2
    const v0, 0x7f090016

    .line 321
    iput-boolean v6, p0, Lcom/android/calendar/event/h;->f:Z

    goto :goto_0
.end method
