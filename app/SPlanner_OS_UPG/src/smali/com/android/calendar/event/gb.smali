.class Lcom/android/calendar/event/gb;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Z

.field f:F

.field g:F

.field final synthetic h:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1135
    iput-object p1, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1148
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/gb;->f:F

    .line 1150
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v2

    long-to-float v0, v2

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    long-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/event/gb;->g:F

    .line 1151
    iget v0, p0, Lcom/android/calendar/event/gb;->f:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    .line 1152
    iput-boolean v1, p0, Lcom/android/calendar/event/gb;->e:Z

    .line 1162
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/gb;->c:I

    int-to-float v0, v0

    iget v2, p0, Lcom/android/calendar/event/gb;->g:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/event/gb;->b:I

    .line 1163
    iget v0, p0, Lcom/android/calendar/event/gb;->b:I

    iget v2, p0, Lcom/android/calendar/event/gb;->a:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/event/gb;->d:I

    .line 1164
    iget v0, p0, Lcom/android/calendar/event/gb;->b:I

    iput v0, p0, Lcom/android/calendar/event/gb;->a:I

    .line 1166
    iget-object v0, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    iget v2, p0, Lcom/android/calendar/event/gb;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 1167
    return-void

    .line 1153
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/gb;->e:Z

    if-nez v0, :cond_0

    .line 1154
    iget-boolean v0, p0, Lcom/android/calendar/event/gb;->e:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/android/calendar/event/gb;->e:Z

    .line 1155
    iget-object v0, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    invoke-static {v2}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v2, v2, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/event/gb;->c:I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1154
    goto :goto_1

    .line 1158
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v0, v0, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v2, v2, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/android/calendar/event/gb;->h:Lcom/android/calendar/event/fx;

    iget-object v2, v2, Lcom/android/calendar/event/fx;->a:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/android/calendar/event/gb;->c:I

    goto :goto_0
.end method
