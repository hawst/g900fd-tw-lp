.class Lcom/android/calendar/event/fp;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:I

.field final synthetic b:Lcom/android/calendar/event/ay;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ay;III)V
    .locals 0

    .prologue
    .line 6242
    iput-object p1, p0, Lcom/android/calendar/event/fp;->b:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6243
    iput p2, p0, Lcom/android/calendar/event/fp;->a:I

    .line 6244
    iput p4, p0, Lcom/android/calendar/event/fp;->c:I

    .line 6245
    iput p3, p0, Lcom/android/calendar/event/fp;->d:I

    .line 6246
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 0

    .prologue
    .line 6279
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 6251
    const/4 v2, 0x0

    .line 6254
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6259
    :goto_0
    iget v3, p0, Lcom/android/calendar/event/fp;->a:I

    if-ge v0, v3, :cond_1

    .line 6260
    iget v0, p0, Lcom/android/calendar/event/fp;->a:I

    .line 6268
    :goto_1
    if-eqz v1, :cond_0

    .line 6269
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 6270
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 6274
    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/calendar/event/fp;->a(I)V

    .line 6275
    return-void

    .line 6255
    :catch_0
    move-exception v0

    .line 6256
    iget v0, p0, Lcom/android/calendar/event/fp;->d:I

    goto :goto_0

    .line 6262
    :cond_1
    iget v3, p0, Lcom/android/calendar/event/fp;->c:I

    if-le v0, v3, :cond_2

    .line 6264
    iget v0, p0, Lcom/android/calendar/event/fp;->c:I

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 6283
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 6287
    return-void
.end method
