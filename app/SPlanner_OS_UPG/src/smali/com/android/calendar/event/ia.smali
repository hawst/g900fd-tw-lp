.class Lcom/android/calendar/event/ia;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/hp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 722
    iput-object p1, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0x42

    const/16 v3, 0x17

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 726
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    if-eq p2, v4, :cond_0

    if-ne p2, v3, :cond_1

    .line 732
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-static {v2, v0}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Z)Z

    .line 733
    iget-object v2, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-static {v2}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 734
    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 735
    iget-object v1, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-static {v1, p1}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Landroid/view/View;)V

    .line 748
    :goto_0
    return v0

    .line 738
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    if-eq p2, v4, :cond_2

    if-ne p2, v3, :cond_3

    .line 744
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-static {v2, v1}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Z)Z

    .line 745
    iget-object v1, p0, Lcom/android/calendar/event/ia;->a:Lcom/android/calendar/event/hp;

    invoke-virtual {v1}, Lcom/android/calendar/event/hp;->a()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 748
    goto :goto_0
.end method
