.class Lcom/android/calendar/event/it;
.super Ljava/lang/Object;
.source "ParticipantScheduleView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:I

.field b:I

.field c:Z

.field final synthetic d:Lcom/android/calendar/event/ip;


# direct methods
.method private constructor <init>(Lcom/android/calendar/event/ip;)V
    .locals 1

    .prologue
    .line 1142
    iput-object p1, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/it;->c:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/calendar/event/ip;Lcom/android/calendar/event/iq;)V
    .locals 0

    .prologue
    .line 1142
    invoke-direct {p0, p1}, Lcom/android/calendar/event/it;-><init>(Lcom/android/calendar/event/ip;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1155
    iget-boolean v0, p0, Lcom/android/calendar/event/it;->c:Z

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/it;->b:I

    if-ge v0, v1, :cond_1

    .line 1157
    const/16 v0, 0xd

    iput v0, p0, Lcom/android/calendar/event/it;->a:I

    .line 1161
    :goto_0
    iput-boolean v2, p0, Lcom/android/calendar/event/it;->c:Z

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v0}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/it;->a:I

    add-int/2addr v0, v1

    .line 1165
    if-gez v0, :cond_2

    .line 1166
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v0, v2}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;I)I

    .line 1167
    iput v2, p0, Lcom/android/calendar/event/it;->a:I

    .line 1179
    :goto_1
    iget v0, p0, Lcom/android/calendar/event/it;->a:I

    if-eqz v0, :cond_7

    .line 1180
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-virtual {v0, p0}, Lcom/android/calendar/event/ip;->post(Ljava/lang/Runnable;)Z

    .line 1186
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-virtual {v0}, Lcom/android/calendar/event/ip;->invalidate()V

    .line 1187
    return-void

    .line 1159
    :cond_1
    const/16 v0, -0xd

    iput v0, p0, Lcom/android/calendar/event/it;->a:I

    goto :goto_0

    .line 1168
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v1}, Lcom/android/calendar/event/ip;->d(Lcom/android/calendar/event/ip;)I

    move-result v1

    if-le v0, v1, :cond_3

    .line 1169
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    iget-object v1, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v1}, Lcom/android/calendar/event/ip;->d(Lcom/android/calendar/event/ip;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;I)I

    .line 1170
    iput v2, p0, Lcom/android/calendar/event/it;->a:I

    goto :goto_1

    .line 1171
    :cond_3
    iget v1, p0, Lcom/android/calendar/event/it;->a:I

    if-lez v1, :cond_4

    iget v1, p0, Lcom/android/calendar/event/it;->b:I

    if-ge v0, v1, :cond_5

    :cond_4
    iget v1, p0, Lcom/android/calendar/event/it;->a:I

    if-gez v1, :cond_6

    iget v1, p0, Lcom/android/calendar/event/it;->b:I

    if-ge v0, v1, :cond_6

    .line 1173
    :cond_5
    iput v2, p0, Lcom/android/calendar/event/it;->a:I

    goto :goto_1

    .line 1175
    :cond_6
    iget-object v1, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v1, v0}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;I)I

    .line 1176
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    iget-object v1, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v1}, Lcom/android/calendar/event/ip;->c(Lcom/android/calendar/event/ip;)I

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v2}, Lcom/android/calendar/event/ip;->g(Lcom/android/calendar/event/ip;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v2}, Lcom/android/calendar/event/ip;->g(Lcom/android/calendar/event/ip;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/android/calendar/event/ip;->d(Lcom/android/calendar/event/ip;I)I

    goto :goto_1

    .line 1182
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/it;->d:Lcom/android/calendar/event/ip;

    invoke-static {v0, v3}, Lcom/android/calendar/event/ip;->b(Lcom/android/calendar/event/ip;Z)Z

    .line 1183
    iput-boolean v3, p0, Lcom/android/calendar/event/it;->c:Z

    goto :goto_2
.end method
