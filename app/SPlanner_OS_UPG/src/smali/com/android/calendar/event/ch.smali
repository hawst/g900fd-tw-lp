.class Lcom/android/calendar/event/ch;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 4075
    iput-object p1, p0, Lcom/android/calendar/event/ch;->b:Lcom/android/calendar/event/ay;

    iput-object p2, p0, Lcom/android/calendar/event/ch;->a:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4078
    iget-object v0, p0, Lcom/android/calendar/event/ch;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4079
    iget-object v0, p0, Lcom/android/calendar/event/ch;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, "preferences_confirm_new_event_account"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 4083
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ch;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0, v2}, Lcom/android/calendar/event/ay;->d(Lcom/android/calendar/event/ay;Z)Z

    .line 4084
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 4085
    return-void
.end method
