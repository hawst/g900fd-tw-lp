.class Lcom/android/calendar/event/ix;
.super Ljava/lang/Object;
.source "QuickAddFragment.java"

# interfaces
.implements Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnPrivateCommandListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/iv;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/iv;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrivateCommand(ILjava/lang/CharSequence;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 214
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CLOSED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->a(Lcom/android/calendar/event/iv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-virtual {v0}, Lcom/android/calendar/event/iv;->dismissAllowingStateLoss()V

    .line 235
    :cond_0
    :goto_0
    return v2

    .line 218
    :cond_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "service_cb_private"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    const-string v1, "command"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/iv;->a(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->c(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "action_composing_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    const-string v1, "result"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/event/iv;->b(Lcom/android/calendar/event/iv;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->c(Lcom/android/calendar/event/iv;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "action_cancel_event"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-virtual {v0}, Lcom/android/calendar/event/iv;->a()V

    goto :goto_0

    .line 228
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->d(Lcom/android/calendar/event/iv;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->e(Lcom/android/calendar/event/iv;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 229
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->f(Lcom/android/calendar/event/iv;)V

    goto :goto_0

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ix;->a:Lcom/android/calendar/event/iv;

    invoke-static {v0}, Lcom/android/calendar/event/iv;->g(Lcom/android/calendar/event/iv;)V

    goto :goto_0
.end method
