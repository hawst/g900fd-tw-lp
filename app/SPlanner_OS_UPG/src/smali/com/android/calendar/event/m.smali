.class public Lcom/android/calendar/event/m;
.super Landroid/app/Fragment;
.source "EasyEditEventFragment.java"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/android/calendar/event/v;

.field private c:Landroid/app/Activity;

.field private d:Lcom/android/calendar/event/s;

.field private e:Landroid/view/inputmethod/InputMethodManager;

.field private f:Lcom/android/calendar/aq;

.field private g:Lcom/android/calendar/event/t;

.field private h:Landroid/net/Uri;

.field private i:J

.field private j:J

.field private k:Lcom/android/calendar/as;

.field private l:Lcom/android/calendar/as;

.field private m:Lcom/android/calendar/as;

.field private n:Lcom/android/calendar/as;

.field private o:Lcom/android/calendar/event/u;

.field private p:Lcom/android/calendar/event/av;

.field private q:I

.field private r:Landroid/content/Intent;

.field private s:Landroid/content/AsyncQueryHandler;

.field private t:I

.field private u:Z

.field private v:I

.field private w:Landroid/app/AlertDialog;

.field private x:Landroid/content/AsyncQueryHandler;

.field private y:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/android/calendar/event/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, v0, v0}, Lcom/android/calendar/event/m;-><init>(Lcom/android/calendar/aq;Landroid/content/Intent;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/aq;Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 83
    new-instance v0, Lcom/android/calendar/event/s;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/s;-><init>(Lcom/android/calendar/event/m;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    .line 97
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/calendar/event/m;->q:I

    .line 101
    iput v1, p0, Lcom/android/calendar/event/m;->t:I

    .line 103
    iput-boolean v1, p0, Lcom/android/calendar/event/m;->u:Z

    .line 104
    iput v1, p0, Lcom/android/calendar/event/m;->v:I

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    .line 115
    iput-object p1, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    .line 116
    iput-object p2, p0, Lcom/android/calendar/event/m;->r:Landroid/content/Intent;

    .line 117
    return-void
.end method

.method private a(Landroid/database/Cursor;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 367
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    move v0, v1

    .line 397
    :cond_0
    :goto_0
    return v0

    .line 371
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    const-string v3, "preference_defaultCalendar"

    const-string v4, "local"

    invoke-static {v2, v3, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 375
    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    .line 376
    sget-object v2, Lcom/android/calendar/hj;->h:Ljava/lang/String;

    .line 379
    :cond_2
    if-nez v2, :cond_3

    .line 380
    const/4 v0, 0x0

    goto :goto_0

    .line 383
    :cond_3
    const-string v3, "ownerAccount"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 385
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 386
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 388
    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iput-object v2, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 389
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 391
    if-lt v1, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/m;I)I
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/android/calendar/event/m;->q:I

    return p1
.end method

.method static synthetic a(Lcom/android/calendar/event/m;Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/event/m;->a(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/m;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/m;Lcom/android/calendar/as;)Lcom/android/calendar/as;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/calendar/event/m;->l:Lcom/android/calendar/as;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 547
    monitor-enter p0

    .line 548
    :try_start_0
    iget v0, p0, Lcom/android/calendar/event/m;->q:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/m;->q:I

    .line 549
    iget v0, p0, Lcom/android/calendar/event/m;->q:I

    if-nez v0, :cond_5

    .line 550
    iget-object v0, p0, Lcom/android/calendar/event/m;->m:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/android/calendar/event/m;->m:Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    .line 553
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/event/m;->u:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/event/m;->v:I

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-eqz v0, :cond_4

    .line 555
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 556
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->d()V

    .line 561
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/as;)V

    .line 563
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/as;)V

    .line 564
    monitor-exit p0

    .line 565
    return-void

    .line 558
    :cond_6
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/m;->v:I

    goto :goto_0

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/android/calendar/event/m;I)I
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/android/calendar/event/m;->t:I

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/m;)Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/m;)Lcom/android/calendar/aq;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/m;I)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/android/calendar/event/m;->a(I)V

    return-void
.end method

.method static synthetic d(Lcom/android/calendar/event/m;I)I
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lcom/android/calendar/event/m;->v:I

    return p1
.end method

.method static synthetic d(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic e(Lcom/android/calendar/event/m;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/event/m;->t:I

    return v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 207
    new-instance v0, Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/as;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    .line 209
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 215
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 216
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 221
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    if-eqz v0, :cond_2

    .line 226
    const-string v0, "UTC"

    .line 227
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 228
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v4, v3, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 231
    iput v6, v1, Landroid/text/format/Time;->hour:I

    .line 232
    iput v6, v1, Landroid/text/format/Time;->minute:I

    .line 233
    iput v6, v1, Landroid/text/format/Time;->second:I

    .line 234
    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 235
    iget-object v3, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    invoke-virtual {v1, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/as;->y:J

    .line 237
    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v4, v1, Lcom/android/calendar/as;->B:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 238
    iput v6, v2, Landroid/text/format/Time;->hour:I

    .line 239
    iput v6, v2, Landroid/text/format/Time;->minute:I

    .line 240
    iput v6, v2, Landroid/text/format/Time;->second:I

    .line 241
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 244
    invoke-virtual {v2, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    .line 246
    iget-object v2, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iput-wide v0, v2, Lcom/android/calendar/as;->B:J

    .line 248
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iput-boolean v7, v0, Lcom/android/calendar/as;->G:Z

    .line 256
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    iput-wide v2, v0, Lcom/android/calendar/as;->b:J

    .line 258
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    goto :goto_0

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    goto :goto_1

    .line 251
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->G:Z

    iput-boolean v1, v0, Lcom/android/calendar/as;->G:Z

    .line 252
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->y:J

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 253
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->B:J

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    goto :goto_2
.end method

.method private f()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v3, 0x0

    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 401
    iput-object v2, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    .line 402
    iput-wide v10, p0, Lcom/android/calendar/event/m;->i:J

    .line 403
    iput-wide v10, p0, Lcom/android/calendar/event/m;->j:J

    .line 404
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    if-eqz v0, :cond_a

    .line 405
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v4, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_7

    .line 406
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v4, v4, Lcom/android/calendar/aq;->c:J

    iput-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 407
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v4, v4, Lcom/android/calendar/aq;->c:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    .line 412
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    .line 418
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v6, v8

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    iput-boolean v0, v4, Lcom/android/calendar/as;->aq:Z

    .line 429
    :cond_2
    :goto_2
    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_3

    iget-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_3

    .line 431
    iget-object v0, p0, Lcom/android/calendar/event/m;->p:Lcom/android/calendar/event/av;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/event/av;->a(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    .line 432
    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    .line 435
    :cond_3
    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    cmp-long v0, v4, v12

    if-gtz v0, :cond_5

    iget-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    cmp-long v0, v4, v12

    if-eqz v0, :cond_4

    iget-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_5

    .line 438
    :cond_4
    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    .line 440
    :cond_5
    iget-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    iget-wide v6, p0, Lcom/android/calendar/event/m;->i:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 442
    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    .line 446
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    if-nez v0, :cond_d

    move v0, v1

    .line 447
    :goto_3
    if-nez v0, :cond_e

    .line 448
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iput v3, v0, Lcom/android/calendar/as;->ah:I

    .line 449
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/event/m;->q:I

    .line 454
    iget-object v0, p0, Lcom/android/calendar/event/m;->o:Lcom/android/calendar/event/u;

    iget-object v3, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :goto_4
    return-void

    .line 410
    :cond_7
    iget-object v4, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->p:J

    const-wide/16 v8, 0x10

    cmp-long v0, v6, v8

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    iput-boolean v0, v4, Lcom/android/calendar/as;->G:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_5

    :cond_9
    move v0, v3

    .line 418
    goto :goto_1

    .line 419
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    if-eqz v0, :cond_2

    .line 420
    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v4, v0, Lcom/android/calendar/event/t;->a:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_b

    .line 421
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v4, v4, Lcom/android/calendar/event/t;->a:J

    iput-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 422
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v4, v4, Lcom/android/calendar/event/t;->a:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    .line 424
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v4, v0, Lcom/android/calendar/event/t;->b:J

    iput-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    .line 425
    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v4, v0, Lcom/android/calendar/event/t;->c:J

    iput-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    .line 426
    iget-object v4, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    iget-wide v6, v0, Lcom/android/calendar/event/t;->d:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v6, v8

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    iput-boolean v0, v4, Lcom/android/calendar/as;->aq:Z

    goto/16 :goto_2

    :cond_c
    move v0, v3

    goto :goto_6

    :cond_d
    move v0, v3

    .line 446
    goto :goto_3

    .line 462
    :cond_e
    iput v1, p0, Lcom/android/calendar/event/m;->q:I

    .line 466
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v4, p0, Lcom/android/calendar/event/m;->i:J

    iput-wide v4, v0, Lcom/android/calendar/as;->y:J

    .line 467
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v4, p0, Lcom/android/calendar/event/m;->j:J

    iput-wide v4, v0, Lcom/android/calendar/as;->B:J

    .line 468
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iput v1, v0, Lcom/android/calendar/as;->X:I

    .line 471
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v3, "JAPAN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 472
    iget-object v0, p0, Lcom/android/calendar/event/m;->o:Lcom/android/calendar/event/u;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    :goto_7
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/m;->v:I

    goto/16 :goto_4

    .line 476
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/m;->o:Lcom/android/calendar/event/u;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/u;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method static synthetic f(Lcom/android/calendar/event/m;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/android/calendar/event/m;->e()V

    return-void
.end method

.method static synthetic g(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/s;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/event/m;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->l:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/event/m;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->h:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/m;)J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/android/calendar/event/m;->i:J

    return-wide v0
.end method

.method static synthetic k(Lcom/android/calendar/event/m;)J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/android/calendar/event/m;->j:J

    return-wide v0
.end method

.method static synthetic l(Lcom/android/calendar/event/m;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/android/calendar/event/m;->v:I

    return v0
.end method

.method static synthetic m(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/t;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->g:Lcom/android/calendar/event/t;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/event/m;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->x:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/av;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->p:Lcom/android/calendar/event/av;

    return-object v0
.end method

.method static synthetic p(Lcom/android/calendar/event/m;)Lcom/android/calendar/event/v;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/s;->a(I)V

    .line 192
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0}, Lcom/android/calendar/event/s;->run()V

    .line 194
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 196
    iget-object v0, p0, Lcom/android/calendar/event/m;->e:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v1, v1, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 197
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 201
    if-eqz v0, :cond_1

    .line 202
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 204
    :cond_1
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    if-nez v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    invoke-virtual {v0}, Lcom/android/calendar/event/v;->b()Z

    .line 268
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->n:Lcom/android/calendar/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/as;->d(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 269
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->c()Z

    goto :goto_0

    .line 271
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    const v1, 0x7f0f01d8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x1

    .line 277
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    invoke-virtual {v0}, Lcom/android/calendar/event/v;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    iget v0, p0, Lcom/android/calendar/event/m;->v:I

    if-nez v0, :cond_1

    .line 281
    iput v1, p0, Lcom/android/calendar/event/m;->v:I

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/s;->a(I)V

    .line 284
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0}, Lcom/android/calendar/event/s;->run()V

    .line 295
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->c(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/m;->l:Lcom/android/calendar/as;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    invoke-virtual {v0}, Lcom/android/calendar/event/v;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/s;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0}, Lcom/android/calendar/event/s;->run()V

    goto :goto_0

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/s;->a(I)V

    .line 293
    iget-object v0, p0, Lcom/android/calendar/event/m;->d:Lcom/android/calendar/event/s;

    invoke-virtual {v0}, Lcom/android/calendar/event/s;->run()V

    goto :goto_0
.end method

.method protected d()V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 716
    iget v0, p0, Lcom/android/calendar/event/m;->v:I

    if-nez v0, :cond_2

    .line 717
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 718
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-boolean v4, v0, Lcom/android/calendar/as;->w:Z

    .line 719
    iget-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v1, "com.android.exchange"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 721
    const/4 v1, 0x0

    .line 724
    if-eqz v3, :cond_4

    .line 727
    if-nez v4, :cond_0

    if-eqz v5, :cond_3

    .line 730
    :cond_0
    new-array v0, v2, [Ljava/lang/CharSequence;

    move-object v2, v0

    .line 743
    :goto_0
    if-nez v4, :cond_7

    if-nez v5, :cond_7

    .line 744
    add-int/lit8 v0, v1, 0x1

    iget-object v6, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    const v7, 0x7f0f0435

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v2, v1

    .line 746
    :goto_1
    add-int/lit8 v1, v0, 0x1

    iget-object v1, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    const v6, 0x7f0f0048

    invoke-virtual {v1, v6}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v2, v0

    .line 749
    iget-object v0, p0, Lcom/android/calendar/event/m;->w:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 750
    iget-object v0, p0, Lcom/android/calendar/event/m;->w:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 751
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/m;->w:Landroid/app/AlertDialog;

    .line 753
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0190

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/p;

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/android/calendar/event/p;-><init>(Lcom/android/calendar/event/m;ZZZ)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/m;->w:Landroid/app/AlertDialog;

    .line 786
    iget-object v0, p0, Lcom/android/calendar/event/m;->w:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/calendar/event/q;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/q;-><init>(Lcom/android/calendar/event/m;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 796
    :cond_2
    return-void

    .line 732
    :cond_3
    new-array v0, v6, [Ljava/lang/CharSequence;

    move-object v2, v0

    goto :goto_0

    .line 735
    :cond_4
    if-nez v4, :cond_5

    if-eqz v5, :cond_6

    .line 736
    :cond_5
    new-array v0, v6, [Ljava/lang/CharSequence;

    .line 740
    :goto_2
    iget-object v6, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    const v7, 0x7f0f02e6

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v0, v1

    move v1, v2

    move-object v2, v0

    goto :goto_0

    .line 738
    :cond_6
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 160
    iput-object p1, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    .line 162
    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/calendar/event/m;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 165
    new-instance v0, Lcom/android/calendar/event/av;

    invoke-direct {v0, p1, v2}, Lcom/android/calendar/event/av;-><init>(Landroid/content/Context;Lcom/android/calendar/as;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->p:Lcom/android/calendar/event/av;

    .line 166
    new-instance v0, Lcom/android/calendar/event/u;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/u;-><init>(Lcom/android/calendar/event/m;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->o:Lcom/android/calendar/event/u;

    .line 167
    new-instance v0, Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/m;->r:Landroid/content/Intent;

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/as;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    .line 169
    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-wide v0, v0, Lcom/android/calendar/aq;->c:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 170
    invoke-direct {p0}, Lcom/android/calendar/event/m;->f()V

    .line 173
    :cond_0
    new-instance v0, Lcom/android/calendar/event/r;

    invoke-virtual {p0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/r;-><init>(Lcom/android/calendar/event/m;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->s:Landroid/content/AsyncQueryHandler;

    .line 174
    iget-object v0, p0, Lcom/android/calendar/event/m;->s:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x1

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    new-instance v0, Lcom/android/calendar/event/o;

    iget-object v1, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/o;-><init>(Lcom/android/calendar/event/m;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/m;->x:Landroid/content/AsyncQueryHandler;

    .line 180
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 122
    if-eqz p1, :cond_1

    .line 123
    monitor-enter p0

    .line 124
    :try_start_0
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/event/m;->m:Lcom/android/calendar/as;

    .line 128
    :cond_0
    monitor-exit p0

    .line 130
    :cond_1
    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 185
    const v0, 0x7f040037

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/android/calendar/event/v;

    iget-object v2, p0, Lcom/android/calendar/event/m;->c:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/event/m;->f:Lcom/android/calendar/aq;

    iget-object v4, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/android/calendar/event/v;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/aq;Lcom/android/calendar/as;)V

    iput-object v1, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    .line 187
    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 136
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    invoke-virtual {v0}, Lcom/android/calendar/event/v;->c()V

    .line 141
    iget-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    .line 144
    iget-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 145
    iget-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    new-instance v1, Lcom/android/calendar/event/n;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/n;-><init>(Lcom/android/calendar/event/m;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 156
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 800
    sget-boolean v0, Lcom/android/calendar/dz;->c:Z

    if-eqz v0, :cond_0

    .line 801
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/android/calendar/event/m;->b:Lcom/android/calendar/event/v;

    iget-object v0, v0, Lcom/android/calendar/event/v;->b:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->onSaveInstanceState()Landroid/os/Bundle;

    .line 805
    :cond_0
    const-string v0, "key_model"

    iget-object v1, p0, Lcom/android/calendar/event/m;->k:Lcom/android/calendar/as;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 806
    invoke-virtual {p0}, Lcom/android/calendar/event/m;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/m;->y:Landroid/view/View;

    .line 807
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 808
    return-void
.end method
