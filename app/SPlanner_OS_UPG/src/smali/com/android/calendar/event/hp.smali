.class final Lcom/android/calendar/event/hp;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field a:Lcom/android/calendar/gl;

.field b:Lcom/android/calendar/event/id;

.field c:Ljava/lang/String;

.field final synthetic d:Landroid/app/Activity;

.field final synthetic e:Landroid/widget/Spinner;

.field final synthetic f:Ljava/util/ArrayList;

.field final synthetic g:I

.field final synthetic h:Ljava/util/ArrayList;

.field private i:Landroid/widget/ImageButton;

.field private j:Landroid/widget/ImageButton;

.field private k:Landroid/widget/EditText;

.field private l:Z

.field private m:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILjava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 440
    iput-object p1, p0, Lcom/android/calendar/event/hp;->d:Landroid/app/Activity;

    iput-object p2, p0, Lcom/android/calendar/event/hp;->e:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/android/calendar/event/hp;->f:Ljava/util/ArrayList;

    iput p4, p0, Lcom/android/calendar/event/hp;->g:I

    iput-object p5, p0, Lcom/android/calendar/event/hp;->h:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iget-object v0, p0, Lcom/android/calendar/event/hp;->d:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/hp;->e:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/android/calendar/event/hp;->f:Ljava/util/ArrayList;

    iget v3, p0, Lcom/android/calendar/event/hp;->g:I

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/calendar/event/hm;->a(Landroid/app/Activity;Landroid/widget/Spinner;Ljava/util/ArrayList;ILandroid/widget/TextView;)Lcom/android/calendar/gl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hp;->a:Lcom/android/calendar/gl;

    .line 557
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/hp;->m:Landroid/os/Handler;

    .line 559
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 537
    iget-object v0, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/android/calendar/event/hp;->l:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 542
    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    if-nez v0, :cond_1

    .line 543
    new-instance v0, Lcom/android/calendar/event/id;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/event/id;-><init>(Lcom/android/calendar/event/hp;Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    .line 544
    iget-object v0, p0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    invoke-virtual {v0}, Lcom/android/calendar/event/id;->start()V

    .line 545
    const-wide/16 v0, 0x32

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    :cond_1
    :goto_1
    return-void

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/android/calendar/event/hp;->l:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 548
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method private a(Landroid/widget/AdapterView;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 598
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 600
    const v1, 0x7f040015

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 602
    const v1, 0x7f12004f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    .line 603
    const v1, 0x7f120051

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 606
    const v2, 0x7f12004e

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    .line 607
    const v2, 0x7f120050

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    .line 609
    invoke-static {}, Lcom/android/calendar/event/hm;->c()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 610
    invoke-static {v7}, Lcom/android/calendar/event/hm;->a(I)V

    .line 613
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 614
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 615
    const v4, 0x7f0f0040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    const v4, 0x7f0f003f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    const v4, 0x7f0f003e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    const v4, 0x7f0f0042

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    new-instance v3, Lcom/android/calendar/gl;

    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f04002b

    invoke-static {v2}, Lcom/android/calendar/event/hm;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v3, v4, v5, v2}, Lcom/android/calendar/gl;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 622
    const v2, 0x7f04009c

    invoke-virtual {v3, v2}, Lcom/android/calendar/gl;->setDropDownViewResource(I)V

    .line 623
    invoke-static {}, Lcom/android/calendar/event/hm;->c()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/android/calendar/gl;->a(I)V

    .line 624
    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 625
    invoke-static {}, Lcom/android/calendar/event/hm;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 626
    new-instance v2, Lcom/android/calendar/event/hv;

    invoke-direct {v2, p0, v3}, Lcom/android/calendar/event/hv;-><init>(Lcom/android/calendar/event/hp;Lcom/android/calendar/gl;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 639
    iget-object v2, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    new-instance v3, Lcom/android/calendar/event/hw;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/hw;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 664
    iget-object v2, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    new-instance v3, Lcom/android/calendar/event/hx;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/hx;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 674
    iget-object v2, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    new-instance v3, Lcom/android/calendar/event/hy;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/hy;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 690
    new-instance v2, Lcom/android/calendar/event/hz;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/hz;-><init>(Lcom/android/calendar/event/hp;)V

    .line 722
    new-instance v3, Lcom/android/calendar/event/ia;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ia;-><init>(Lcom/android/calendar/event/hp;)V

    .line 752
    new-instance v4, Lcom/android/calendar/event/ib;

    invoke-direct {v4, p0, v0}, Lcom/android/calendar/event/ib;-><init>(Lcom/android/calendar/event/hp;Landroid/widget/LinearLayout;)V

    .line 763
    iget-object v5, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 764
    iget-object v5, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 765
    iget-object v5, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 766
    invoke-virtual {v1, v4}, Landroid/widget/Spinner;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 768
    iget-object v4, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 769
    iget-object v4, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 770
    iget-object v2, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 771
    iget-object v2, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 772
    iget-object v2, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/calendar/event/ic;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ic;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 779
    iget-object v2, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    new-instance v3, Lcom/android/calendar/event/hr;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/hr;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 787
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 788
    const v3, 0x7f0f011d

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0f0041

    new-instance v4, Lcom/android/calendar/event/hu;

    invoke-direct {v4, p0, p1, v1}, Lcom/android/calendar/event/hu;-><init>(Lcom/android/calendar/event/hp;Landroid/widget/AdapterView;Landroid/widget/Spinner;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0f00a4

    new-instance v4, Lcom/android/calendar/event/ht;

    invoke-direct {v4, p0}, Lcom/android/calendar/event/ht;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v3, Lcom/android/calendar/event/hs;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/hs;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 887
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/event/hm;->a(Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 889
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 890
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-static {}, Lcom/android/calendar/event/hm;->i()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 893
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setNextFocusDownId(I)V

    .line 896
    :cond_1
    const-string v0, "15"

    invoke-static {v0}, Lcom/android/calendar/event/hm;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 897
    iget-object v0, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-static {}, Lcom/android/calendar/event/hm;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 898
    iget-object v0, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 899
    invoke-static {}, Lcom/android/calendar/event/hm;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 900
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 903
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/hp;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/android/calendar/event/hp;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/hp;)Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/android/calendar/event/hp;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/hp;Z)Z
    .locals 0

    .prologue
    .line 440
    iput-boolean p1, p0, Lcom/android/calendar/event/hp;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/hp;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/calendar/event/hp;->m:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 501
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 503
    iget-object v2, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 504
    iget-object v0, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    .line 507
    iget-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 508
    iget-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 511
    :goto_0
    const/16 v2, 0x63

    if-le v0, v2, :cond_0

    move v0, v1

    .line 515
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    .line 516
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/event/hp;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 519
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 521
    iget-object v2, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 522
    iget-object v0, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    .line 524
    iget-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 528
    :goto_0
    if-gez v0, :cond_0

    .line 529
    const/16 v0, 0x63

    .line 532
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/hp;->c:Ljava/lang/String;

    .line 533
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0}, Lcom/android/calendar/event/hp;->b()V

    return-void
.end method

.method static synthetic e(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0}, Lcom/android/calendar/event/hp;->c()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/android/calendar/event/hp;->k:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/android/calendar/event/hp;->i:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/android/calendar/event/hp;->l:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 554
    iget-object v0, p0, Lcom/android/calendar/event/hp;->j:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/android/calendar/event/hp;->l:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 555
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 452
    iget-object v0, p0, Lcom/android/calendar/event/hp;->a:Lcom/android/calendar/gl;

    invoke-virtual {v0}, Lcom/android/calendar/gl;->getCount()I

    move-result v0

    if-gt v0, p3, :cond_0

    move p3, v1

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/hp;->a:Lcom/android/calendar/gl;

    invoke-virtual {v0, p3}, Lcom/android/calendar/gl;->a(I)V

    .line 457
    iget-object v0, p0, Lcom/android/calendar/event/hp;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 458
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 459
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 461
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/event/hp;->a(Landroid/widget/AdapterView;)V

    .line 463
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 464
    invoke-static {}, Lcom/android/calendar/event/hm;->e()Landroid/app/AlertDialog;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/hq;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/hq;-><init>(Lcom/android/calendar/event/hp;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 490
    :cond_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 907
    return-void
.end method
