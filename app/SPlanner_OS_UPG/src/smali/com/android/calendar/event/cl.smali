.class Lcom/android/calendar/event/cl;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 1059
    iput-object p1, p0, Lcom/android/calendar/event/cl;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1062
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1066
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 1070
    .line 1071
    if-eq p2, p3, :cond_0

    .line 1072
    iget-object v0, p0, Lcom/android/calendar/event/cl;->a:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->H:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/android/calendar/event/fo;

    iget-object v2, p0, Lcom/android/calendar/event/cl;->a:Lcom/android/calendar/event/ay;

    invoke-static {v2}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/calendar/event/fo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1075
    :cond_0
    return-void
.end method
