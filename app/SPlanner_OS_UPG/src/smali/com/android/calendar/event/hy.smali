.class Lcom/android/calendar/event/hy;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/hp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 678
    iget-object v0, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isCursorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 681
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 682
    iget-object v0, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 683
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 685
    iget-object v1, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v1}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 686
    iget-object v0, p0, Lcom/android/calendar/event/hy;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 687
    return-void
.end method
