.class Lcom/android/calendar/event/cb;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;)V
    .locals 0

    .prologue
    .line 3646
    iput-object p1, p0, Lcom/android/calendar/event/cb;->a:Lcom/android/calendar/event/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 3649
    iget-object v0, p0, Lcom/android/calendar/event/cb;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->I(Lcom/android/calendar/event/ay;)Lcom/android/calendar/event/ax;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/android/calendar/event/ax;->a(I)V

    .line 3650
    iget-object v0, p0, Lcom/android/calendar/event/cb;->a:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->I(Lcom/android/calendar/event/ay;)Lcom/android/calendar/event/ax;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/calendar/event/ax;->run()V

    .line 3651
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 3652
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.ADD_ACCOUNT_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3653
    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.android.calendar"

    aput-object v3, v1, v2

    .line 3656
    const-string v2, "authorities"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 3657
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3660
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/cb;->a:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->m(Lcom/android/calendar/event/ay;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3665
    :cond_0
    :goto_0
    return-void

    .line 3661
    :catch_0
    move-exception v0

    .line 3662
    const-string v0, "EditEvent"

    const-string v1, "Error: Could not find Settings.ACTION_ADD_ACCOUNT activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
