.class public Lcom/android/calendar/event/ParticipantScheduleActivity;
.super Landroid/app/Activity;
.source "ParticipantScheduleActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private A:Landroid/content/AsyncQueryHandler;

.field protected b:Landroid/widget/ViewSwitcher;

.field protected c:Landroid/view/animation/Animation;

.field protected d:Landroid/view/animation/Animation;

.field protected e:Landroid/view/animation/Animation;

.field protected f:Landroid/view/animation/Animation;

.field g:Ljava/util/ArrayList;

.field h:Ljava/util/ArrayList;

.field i:[Ljava/lang/String;

.field j:[Ljava/lang/String;

.field k:[Ljava/lang/String;

.field l:Ljava/lang/Boolean;

.field m:Landroid/text/format/Time;

.field private final n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/util/ArrayList;

.field private r:Ljava/lang/String;

.field private s:Landroid/text/format/Time;

.field private t:Z

.field private u:J

.field private v:Landroid/view/View;

.field private w:Landroid/view/View;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-string v0, "content://com.android.exchange.directory.provider/resolverecipients/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/ParticipantScheduleActivity;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const-string v0, "ParticipantsScheduleActivity"

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->n:Ljava/lang/String;

    .line 55
    iput-boolean v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->t:Z

    .line 57
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->u:J

    .line 74
    iput-boolean v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->z:Z

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->k:[Ljava/lang/String;

    .line 83
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->l:Ljava/lang/Boolean;

    .line 84
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    .line 398
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ParticipantScheduleActivity;Landroid/text/format/Time;)Landroid/text/format/Time;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/ParticipantScheduleActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/text/format/Time;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 10

    .prologue
    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->z:Z

    .line 241
    const/4 v0, 0x0

    .line 242
    if-eqz p2, :cond_0

    .line 243
    array-length v0, p2

    .line 245
    :cond_0
    add-int/lit8 v1, v0, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    .line 246
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->p:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 248
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->i:[Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {p2, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    const/4 v0, 0x0

    .line 252
    if-eqz p3, :cond_1

    .line 253
    array-length v0, p3

    .line 254
    :cond_1
    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    .line 255
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 256
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->j:[Ljava/lang/String;

    const-string v2, "444444444444444444444444444444444444444444444444"

    aput-object v2, v1, v0

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_2
    iget-wide v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->u:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 261
    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 262
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "emailAddress = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->o:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 269
    if-eqz v1, :cond_3

    .line 270
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 271
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->u:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :cond_3
    if-eqz v1, :cond_4

    .line 276
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 282
    :cond_4
    const/4 v0, 0x5

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "to"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "displayName"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "email"

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "mergedFreeBusy"

    aput-object v1, v4, v0

    .line 287
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v0, "accountId=? AND startTime=\'?\' AND endTime=\'?\' AND "

    invoke-direct {v5, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 289
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 292
    if-eqz p3, :cond_7

    .line 293
    array-length v2, p3

    .line 294
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_7

    .line 295
    aget-object v3, p3, v0

    .line 296
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 297
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 298
    const-string v6, "<"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v6, ">"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 299
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 300
    aput-object v3, p3, v0

    .line 301
    const-string v3, " OR to=\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v6, p3, v0

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const/16 v6, 0x27

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 294
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 275
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_6

    .line 276
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 308
    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_8

    .line 309
    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "(to=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 310
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 314
    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 317
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->r:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 318
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 319
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    .line 322
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 323
    invoke-virtual {v2, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 324
    const/4 v3, 0x0

    iput v3, v2, Landroid/text/format/Time;->hour:I

    .line 325
    const/4 v3, 0x0

    iput v3, v2, Landroid/text/format/Time;->minute:I

    .line 326
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 327
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sub-long/2addr v6, v0

    invoke-virtual {v2, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 328
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 330
    const-string v3, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 332
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 333
    invoke-virtual {v3, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 334
    const/16 v6, 0x17

    iput v6, v3, Landroid/text/format/Time;->hour:I

    .line 335
    const/16 v6, 0x3b

    iput v6, v3, Landroid/text/format/Time;->minute:I

    .line 336
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 337
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sub-long v0, v6, v0

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 338
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 340
    const-string v0, "%Y-%m-%dT%H:%M:%S.000Z"

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 342
    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v8, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->u:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v1

    const/4 v1, 0x1

    aput-object v2, v6, v1

    const/4 v1, 0x2

    aput-object v0, v6, v1

    .line 346
    const-string v0, "ParticipantsScheduleActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selection = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const-string v0, "ParticipantsScheduleActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAccountId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->u:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iput-object p3, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->k:[Ljava/lang/String;

    .line 350
    iput-object p4, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->l:Ljava/lang/Boolean;

    .line 351
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 353
    new-instance v0, Lcom/android/calendar/event/io;

    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/io;-><init>(Lcom/android/calendar/event/ParticipantScheduleActivity;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->A:Landroid/content/AsyncQueryHandler;

    .line 354
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->A:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/android/calendar/event/ParticipantScheduleActivity;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    return-void

    .line 312
    :cond_8
    const-string v0, "to=\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/android/calendar/event/ParticipantScheduleActivity;Z)Z
    .locals 0

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->t:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/ParticipantScheduleActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/ParticipantScheduleActivity;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->t:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 194
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    iget-wide v6, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 195
    iget-object v4, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->v:Landroid/view/View;

    const v0, 0x24dc87

    if-le v3, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->w:Landroid/view/View;

    const v4, 0x259d23

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 197
    return-void

    :cond_0
    move v0, v2

    .line 195
    goto :goto_0

    :cond_1
    move v1, v2

    .line 196
    goto :goto_1
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 202
    invoke-virtual {v0}, Lcom/android/calendar/event/ip;->getFirstNameIndex()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 204
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 211
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/android/calendar/event/ip;->a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 212
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 213
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 214
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 207
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;Z)V
    .locals 4

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->z:Z

    if-eqz v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 222
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 223
    new-array v3, v2, [Ljava/lang/String;

    .line 224
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 225
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v3, v1

    .line 224
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 227
    :cond_1
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v3, v3, v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a(Landroid/text/format/Time;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 229
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->b()V

    .line 230
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->finish()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 360
    const v0, 0x7f0f0202

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 361
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    return-void
.end method

.method public makeView()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 365
    new-instance v0, Lcom/android/calendar/event/ip;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/ip;-><init>(Lcom/android/calendar/event/ParticipantScheduleActivity;)V

    .line 366
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ip;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    return-object v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    invoke-virtual {v0}, Lcom/android/calendar/event/ip;->b()V

    .line 386
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->z:Z

    .line 387
    const-string v0, "ParticipantsScheduleActivity"

    const-string v1, "onAnimationEnd"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 375
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->z:Z

    .line 380
    const-string v0, "ParticipantsScheduleActivity"

    const-string v1, "onAnimationStart"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v0, 0x7f040076

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 93
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 95
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0295

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->p:Ljava/lang/String;

    .line 96
    const-string v2, "owner_account"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->o:Ljava/lang/String;

    .line 97
    const-string v2, "recipients"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->q:Ljava/util/ArrayList;

    .line 98
    const-string v2, "start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 99
    const-string v4, "timezone"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->r:Ljava/lang/String;

    .line 100
    new-instance v4, Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->r:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    .line 101
    iget-object v4, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    invoke-virtual {v4, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 103
    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    .line 104
    const-string v2, "schedule"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    .line 106
    const v0, 0x7f05000e

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->c:Landroid/view/animation/Animation;

    .line 107
    const v0, 0x7f05000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->d:Landroid/view/animation/Animation;

    .line 108
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 109
    const v0, 0x7f050010

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->e:Landroid/view/animation/Animation;

    .line 110
    const v0, 0x7f050011

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->f:Landroid/view/animation/Animation;

    .line 111
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 112
    const v0, 0x7f120255

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    .line 113
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, p0}, Landroid/widget/ViewSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 115
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040002

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 116
    const v0, 0x7f12001d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->v:Landroid/view/View;

    .line 117
    const v0, 0x7f120021

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->w:Landroid/view/View;

    .line 118
    const v0, 0x7f12001f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->x:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f120020

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->y:Landroid/widget/TextView;

    .line 121
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->v:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/im;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/im;-><init>(Lcom/android/calendar/event/ParticipantScheduleActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->w:Landroid/view/View;

    new-instance v3, Lcom/android/calendar/event/in;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/in;-><init>(Lcom/android/calendar/event/ParticipantScheduleActivity;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    if-eqz v1, :cond_0

    .line 142
    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 143
    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 144
    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 145
    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 146
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 148
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 179
    const-string v0, "beginTime"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 180
    iget-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 181
    const-string v0, "name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    .line 182
    const-string v0, "schedule"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    .line 183
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 154
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 155
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    iget-object v0, v0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->m:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 158
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 168
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/ip;

    .line 169
    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/calendar/event/ip;->a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 170
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 171
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 173
    invoke-virtual {p0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a()V

    .line 174
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 161
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 165
    iget-object v0, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->b:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 188
    const-string v0, "beginTime"

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->s:Landroid/text/format/Time;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 189
    const-string v0, "name"

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 190
    const-string v0, "schedule"

    iget-object v1, p0, Lcom/android/calendar/event/ParticipantScheduleActivity;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 191
    return-void
.end method
