.class public Lcom/android/calendar/event/ip;
.super Landroid/view/View;
.source "ParticipantScheduleView.java"


# instance fields
.field private A:Landroid/graphics/Rect;

.field private B:Landroid/graphics/Rect;

.field private C:Landroid/graphics/Paint;

.field private D:Landroid/graphics/Paint;

.field private E:Landroid/graphics/Bitmap;

.field private F:Landroid/graphics/Canvas;

.field private G:Z

.field private H:Z

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private final Q:I

.field private R:I

.field private S:I

.field private T:I

.field private U:I

.field private V:I

.field private W:I

.field protected a:Lcom/android/calendar/event/ParticipantScheduleActivity;

.field private aa:I

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:I

.field private af:I

.field private ag:I

.field private ah:I

.field private ai:I

.field private aj:I

.field private ak:I

.field private al:I

.field private am:[Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:Ljava/lang/String;

.field private ap:Z

.field private aq:Ljava/lang/String;

.field private ar:Landroid/view/GestureDetector;

.field private final as:Landroid/content/Context;

.field private at:Ljava/lang/Runnable;

.field private au:Ljava/util/regex/Pattern;

.field b:Landroid/text/format/Time;

.field c:Landroid/text/format/Time;

.field d:Ljava/util/ArrayList;

.field e:Ljava/util/ArrayList;

.field protected f:I

.field protected final g:Landroid/content/res/Resources;

.field private h:Ljava/lang/String;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private n:Z

.field private final o:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private s:Lcom/android/calendar/event/is;

.field private t:Lcom/android/calendar/event/it;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/ParticipantScheduleActivity;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 216
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 57
    const-string v0, "ParticipantScheduleView"

    iput-object v0, p0, Lcom/android/calendar/event/ip;->h:Ljava/lang/String;

    .line 59
    iput v1, p0, Lcom/android/calendar/event/ip;->i:I

    .line 60
    iput v2, p0, Lcom/android/calendar/event/ip;->j:I

    .line 61
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/event/ip;->k:I

    .line 62
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/ip;->l:I

    .line 63
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/calendar/event/ip;->m:I

    .line 69
    const-string v0, "first_cell"

    iput-object v0, p0, Lcom/android/calendar/event/ip;->o:Ljava/lang/String;

    .line 70
    const-string v0, "num_day"

    iput-object v0, p0, Lcom/android/calendar/event/ip;->p:Ljava/lang/String;

    .line 71
    const-string v0, "saved_first_hour"

    iput-object v0, p0, Lcom/android/calendar/event/ip;->q:Ljava/lang/String;

    .line 72
    const-string v0, "saved_first_hour_offset"

    iput-object v0, p0, Lcom/android/calendar/event/ip;->r:Ljava/lang/String;

    .line 74
    new-instance v0, Lcom/android/calendar/event/is;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/event/is;-><init>(Lcom/android/calendar/event/ip;Lcom/android/calendar/event/iq;)V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->s:Lcom/android/calendar/event/is;

    .line 75
    new-instance v0, Lcom/android/calendar/event/it;

    invoke-direct {v0, p0, v3}, Lcom/android/calendar/event/it;-><init>(Lcom/android/calendar/event/ip;Lcom/android/calendar/event/iq;)V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->t:Lcom/android/calendar/event/it;

    .line 91
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->z:Landroid/graphics/Rect;

    .line 92
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->A:Landroid/graphics/Rect;

    .line 93
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->B:Landroid/graphics/Rect;

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    .line 100
    iput-boolean v2, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 101
    iput-boolean v1, p0, Lcom/android/calendar/event/ip;->H:Z

    .line 114
    iput v1, p0, Lcom/android/calendar/event/ip;->P:I

    .line 116
    const/16 v0, 0x30

    iput v0, p0, Lcom/android/calendar/event/ip;->Q:I

    .line 141
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/calendar/event/ip;->f:I

    .line 146
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ip;->ak:I

    .line 154
    iput-boolean v1, p0, Lcom/android/calendar/event/ip;->ap:Z

    .line 162
    new-instance v0, Lcom/android/calendar/event/iq;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/iq;-><init>(Lcom/android/calendar/event/ip;)V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->at:Ljava/lang/Runnable;

    .line 851
    const-string v0, "[\t\n],"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->au:Ljava/util/regex/Pattern;

    .line 217
    iput-object p1, p0, Lcom/android/calendar/event/ip;->as:Landroid/content/Context;

    .line 218
    invoke-virtual {p1}, Lcom/android/calendar/event/ParticipantScheduleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->g:Landroid/content/res/Resources;

    .line 219
    iput-object p1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    .line 220
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/android/calendar/event/ir;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ir;-><init>(Lcom/android/calendar/event/ip;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->ar:Landroid/view/GestureDetector;

    .line 221
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ip;->a(Landroid/content/Context;)V

    .line 222
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ip;I)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ip;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/calendar/event/ip;->as:Landroid/content/Context;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 462
    iput p1, p0, Lcom/android/calendar/event/ip;->ab:I

    .line 463
    iput p2, p0, Lcom/android/calendar/event/ip;->aa:I

    .line 465
    iget v0, p0, Lcom/android/calendar/event/ip;->I:I

    iput v0, p0, Lcom/android/calendar/event/ip;->aj:I

    .line 467
    iget v0, p0, Lcom/android/calendar/event/ip;->aj:I

    sub-int v0, p2, v0

    iput v0, p0, Lcom/android/calendar/event/ip;->ac:I

    .line 468
    iget v0, p0, Lcom/android/calendar/event/ip;->ac:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->ag:I

    .line 471
    iget v0, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x18

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/ip;->W:I

    .line 472
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/event/ip;->W:I

    if-eq v0, v1, :cond_2

    :cond_0
    if-lez p1, :cond_2

    iget v0, p0, Lcom/android/calendar/event/ip;->W:I

    if-lez v0, :cond_2

    .line 474
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 476
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    .line 479
    :cond_1
    :try_start_0
    iget v0, p0, Lcom/android/calendar/event/ip;->W:I

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 485
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/android/calendar/event/ip;->T:I

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 487
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/android/calendar/event/ip;->F:Landroid/graphics/Canvas;

    .line 490
    :cond_2
    iget v0, p0, Lcom/android/calendar/event/ip;->W:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ac:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->V:I

    .line 492
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->c()V

    .line 494
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/event/ip;->al:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    .line 496
    return-void

    .line 480
    :catch_0
    move-exception v0

    .line 481
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 482
    :catch_1
    move-exception v0

    .line 483
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 225
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/ip;->setFocusable(Z)V

    .line 229
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/ip;->setFocusableInTouchMode(Z)V

    .line 230
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/ip;->setClickable(Z)V

    .line 231
    iget-object v0, p0, Lcom/android/calendar/event/ip;->g:Landroid/content/res/Resources;

    .line 233
    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->S:I

    .line 234
    const v1, 0x7f0b004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->T:I

    .line 235
    const v1, 0x7f0b00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->R:I

    .line 237
    const v1, 0x7f0b00bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->M:I

    .line 238
    const v1, 0x7f0b00c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->N:I

    .line 239
    const v1, 0x7f0b00c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->O:I

    .line 241
    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/ip;->f:I

    .line 242
    const v1, 0x7f0c029b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->I:I

    .line 243
    const v1, 0x7f0c0297

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->ae:I

    .line 244
    const v1, 0x7f0c0298

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->ad:I

    .line 245
    const v1, 0x7f0c029a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->ah:I

    .line 246
    const v1, 0x7f0c029c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->J:I

    .line 247
    const v1, 0x7f0c0299

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->K:I

    .line 248
    const v1, 0x7f0c0296

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->L:I

    .line 249
    const v1, 0x7f0c0397

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/event/ip;->ai:I

    .line 251
    iget-object v0, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 252
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 254
    iget-object v1, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    const v2, -0x373738

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 255
    iget-object v1, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 256
    iget-object v1, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 257
    iget-object v1, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 259
    iput v4, p0, Lcom/android/calendar/event/ip;->u:I

    .line 260
    sget-object v1, Lcom/android/calendar/ar;->a:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/calendar/event/ip;->am:[Ljava/lang/String;

    .line 262
    invoke-static {v4}, Lcom/android/calendar/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ip;->an:Ljava/lang/String;

    .line 263
    invoke-static {v3}, Lcom/android/calendar/gm;->a(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/ip;->ao:Ljava/lang/String;

    .line 265
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ip;->at:Ljava/lang/Runnable;

    invoke-static {p1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    .line 266
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 267
    iget-object v1, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 269
    new-instance v1, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/android/calendar/event/ip;->at:Ljava/lang/Runnable;

    invoke-static {p1, v4}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/calendar/event/ip;->c:Landroid/text/format/Time;

    .line 270
    iget-object v1, p0, Lcom/android/calendar/event/ip;->c:Landroid/text/format/Time;

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 272
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0292

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/event/ip;->ag:I

    .line 273
    iget v1, p0, Lcom/android/calendar/event/ip;->K:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 275
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 527
    iget-object v1, p0, Lcom/android/calendar/event/ip;->z:Landroid/graphics/Rect;

    .line 529
    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/event/ip;->a(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 530
    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/event/ip;->b(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 532
    iget-object v0, p0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ip;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/android/calendar/event/ip;->h:Ljava/lang/String;

    const-string v2, "DRAW Schedule"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    invoke-direct {p0, p1, v1}, Lcom/android/calendar/event/ip;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 536
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 19

    .prologue
    .line 539
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ip;->g:Landroid/content/res/Resources;

    .line 540
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 541
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->ah:I

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p2

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 543
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->ad:I

    add-int/lit8 v16, v4, 0x1

    .line 545
    const/4 v9, 0x0

    .line 546
    const v4, 0x7f0b00d0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 547
    const v4, 0x7f0b00d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 548
    const v4, 0x7f0b00cf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 549
    const v4, 0x7f0b00d1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 551
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v17, v3, 0x1

    .line 552
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 553
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->u:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v4, v10

    if-le v4, v3, :cond_1

    move v4, v3

    .line 556
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->u:I

    move v14, v3

    :goto_1
    if-ge v14, v4, :cond_5

    .line 557
    const/4 v12, 0x4

    .line 558
    const/4 v11, -0x1

    .line 559
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int v3, v3, v16

    add-int/lit8 v3, v3, -0x4

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 560
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ip;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v18

    .line 561
    const/4 v10, 0x0

    move v13, v12

    :goto_2
    const/16 v3, 0x30

    if-ge v10, v3, :cond_4

    .line 562
    const/4 v3, 0x0

    .line 566
    move/from16 v0, v18

    if-ge v10, v0, :cond_7

    .line 567
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ip;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    .line 569
    :goto_3
    if-eq v13, v12, :cond_6

    .line 570
    const/4 v3, -0x1

    if-eq v11, v3, :cond_3

    .line 572
    div-int/lit8 v3, v10, 0x2

    mul-int v3, v3, v17

    rem-int/lit8 v11, v10, 0x2

    div-int/lit8 v13, v17, 0x2

    mul-int/2addr v11, v13

    add-int/2addr v3, v11

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 573
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v9, v15, v2}, Lcom/android/calendar/event/ip;->a(Landroid/graphics/Rect;ILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    .line 576
    const/4 v3, 0x4

    if-eq v12, v3, :cond_2

    .line 578
    div-int/lit8 v3, v10, 0x2

    mul-int v3, v3, v17

    rem-int/lit8 v11, v10, 0x2

    div-int/lit8 v13, v17, 0x2

    mul-int/2addr v11, v13

    add-int/2addr v3, v11

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 579
    packed-switch v12, :pswitch_data_0

    move v3, v9

    :goto_4
    move v9, v3

    move v3, v10

    .line 619
    :goto_5
    const/16 v11, 0x2f

    if-ne v10, v11, :cond_0

    const/4 v11, 0x4

    if-eq v12, v11, :cond_0

    .line 620
    mul-int/lit8 v11, v17, 0x18

    move-object/from16 v0, p2

    iput v11, v0, Landroid/graphics/Rect;->bottom:I

    .line 621
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v9, v15, v2}, Lcom/android/calendar/event/ip;->a(Landroid/graphics/Rect;ILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    .line 561
    :cond_0
    add-int/lit8 v10, v10, 0x1

    move v13, v12

    move v11, v3

    goto :goto_2

    .line 553
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->u:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v3, v4

    move v4, v3

    goto/16 :goto_0

    :pswitch_0
    move v3, v5

    .line 582
    goto :goto_4

    :pswitch_1
    move v3, v6

    .line 585
    goto :goto_4

    :pswitch_2
    move v3, v7

    .line 588
    goto :goto_4

    :pswitch_3
    move v3, v8

    .line 590
    goto :goto_4

    .line 594
    :cond_2
    const/4 v3, -0x1

    goto :goto_5

    .line 599
    :cond_3
    div-int/lit8 v3, v10, 0x2

    mul-int v3, v3, v17

    rem-int/lit8 v11, v10, 0x2

    div-int/lit8 v13, v17, 0x2

    mul-int/2addr v11, v13

    add-int/2addr v3, v11

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 601
    packed-switch v12, :pswitch_data_1

    move v3, v10

    goto :goto_5

    :pswitch_4
    move v3, v10

    move v9, v5

    .line 604
    goto :goto_5

    :pswitch_5
    move v3, v10

    move v9, v6

    .line 607
    goto :goto_5

    :pswitch_6
    move v3, v10

    move v9, v7

    .line 610
    goto :goto_5

    :pswitch_7
    move v3, v10

    move v9, v8

    .line 612
    goto :goto_5

    .line 625
    :cond_4
    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int v3, v3, v16

    move-object/from16 v0, p2

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 556
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    goto/16 :goto_1

    .line 627
    :cond_5
    return-void

    :cond_6
    move v3, v11

    goto :goto_5

    :cond_7
    move v12, v3

    goto/16 :goto_3

    .line 579
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 601
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(Landroid/graphics/Rect;ILandroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 630
    iget v0, p1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 631
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 632
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 633
    invoke-virtual {p3, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 634
    const/16 v0, 0x99

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 635
    invoke-virtual {p4, p1, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 637
    iget-object v0, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 638
    iget-object v0, p0, Lcom/android/calendar/event/ip;->D:Landroid/graphics/Paint;

    invoke-virtual {p4, p1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 639
    return-void
.end method

.method private a(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10

    .prologue
    .line 642
    invoke-virtual {p3}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v8

    .line 645
    const/4 v0, 0x0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 646
    iget v0, p0, Lcom/android/calendar/event/ip;->W:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 647
    const/4 v0, 0x0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 648
    iget v0, p0, Lcom/android/calendar/event/ip;->ab:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 649
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 650
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 651
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 654
    iget v0, p0, Lcom/android/calendar/event/ip;->M:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 655
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 656
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 657
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 660
    iget v0, p0, Lcom/android/calendar/event/ip;->ah:I

    int-to-float v1, v0

    .line 661
    iget v0, p0, Lcom/android/calendar/event/ip;->ab:I

    int-to-float v3, v0

    .line 662
    const/4 v2, 0x0

    .line 663
    iget v0, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v9, v0

    .line 664
    const/4 v0, 0x0

    move v6, v0

    move v0, v2

    :goto_0
    const/16 v2, 0x18

    if-ge v6, v2, :cond_0

    .line 665
    add-float v7, v0, v9

    .line 666
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v2, v7, v0

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v4, v7, v0

    move-object v0, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 664
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v0, v7

    goto :goto_0

    .line 669
    :cond_0
    const/4 v2, 0x0

    .line 670
    iget v0, p0, Lcom/android/calendar/event/ip;->N:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 671
    const/4 v0, 0x0

    move v6, v0

    move v0, v2

    :goto_1
    const/16 v2, 0x30

    if-ge v6, v2, :cond_1

    .line 672
    add-float v7, v0, v9

    .line 673
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, v7, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, v7, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v0, v4

    move-object v0, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 671
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v0, v7

    goto :goto_1

    .line 677
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ip;->O:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 678
    const/4 v2, 0x0

    .line 679
    iget v0, p0, Lcom/android/calendar/event/ip;->W:I

    int-to-float v4, v0

    .line 680
    iget v0, p0, Lcom/android/calendar/event/ip;->ah:I

    int-to-float v1, v0

    .line 681
    iget v0, p0, Lcom/android/calendar/event/ip;->ad:I

    add-int/lit8 v0, v0, 0x1

    int-to-float v7, v0

    .line 682
    const/4 v0, 0x0

    move v6, v0

    :goto_2
    iget v0, p0, Lcom/android/calendar/event/ip;->f:I

    if-ge v6, v0, :cond_2

    move-object v0, p2

    move v3, v1

    move-object v5, p3

    .line 683
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 684
    add-float/2addr v1, v7

    .line 682
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    .line 688
    :cond_2
    invoke-virtual {p3, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 689
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 690
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ip;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/event/ip;->ap:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/ip;I)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    return v0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 719
    iget-object v0, p0, Lcom/android/calendar/event/ip;->A:Landroid/graphics/Rect;

    .line 720
    iget-object v1, p0, Lcom/android/calendar/event/ip;->B:Landroid/graphics/Rect;

    .line 722
    iget v2, p0, Lcom/android/calendar/event/ip;->aj:I

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 723
    iget v2, p0, Lcom/android/calendar/event/ip;->aa:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 724
    iput v4, v1, Landroid/graphics/Rect;->left:I

    .line 725
    iget v2, p0, Lcom/android/calendar/event/ip;->ab:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 727
    iget v2, p0, Lcom/android/calendar/event/ip;->T:I

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 729
    iget-object v2, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 730
    iget v3, p0, Lcom/android/calendar/event/ip;->S:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 731
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 733
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 735
    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 736
    iget v2, p0, Lcom/android/calendar/event/ip;->ab:I

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 737
    iget v2, p0, Lcom/android/calendar/event/ip;->U:I

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 738
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/android/calendar/event/ip;->ac:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 740
    iget-object v2, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 741
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 742
    return-void
.end method

.method private b(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 694
    iget v1, p0, Lcom/android/calendar/event/ip;->R:I

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 695
    iget v1, p0, Lcom/android/calendar/event/ip;->K:I

    int-to-float v1, v1

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 696
    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 697
    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 699
    invoke-virtual {p3}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v1, v1

    .line 700
    iget v2, p0, Lcom/android/calendar/event/ip;->ah:I

    iget v3, p0, Lcom/android/calendar/event/ip;->ai:I

    sub-int v3, v2, v3

    .line 701
    div-int/lit8 v1, v1, 0x2

    move v2, v1

    move v1, v0

    .line 703
    :goto_0
    const/16 v0, 0x18

    if-ge v1, v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/android/calendar/event/ip;->as:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 707
    sget-object v0, Lcom/android/calendar/ar;->b:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 711
    :goto_1
    iget v4, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    .line 712
    int-to-float v4, v3

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    invoke-virtual {p2, v0, v4, v5, p3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 713
    if-nez v1, :cond_2

    .line 716
    :cond_0
    return-void

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ip;->am:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_1

    .line 703
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/android/calendar/event/ip;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->e()V

    return-void
.end method

.method static synthetic b(Lcom/android/calendar/event/ip;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/android/calendar/event/ip;->G:Z

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/event/ip;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    return v0
.end method

.method static synthetic c(Lcom/android/calendar/event/ip;I)I
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lcom/android/calendar/event/ip;->U:I

    return p1
.end method

.method private c(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 745
    iget-object v0, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 746
    iget-object v1, p0, Lcom/android/calendar/event/ip;->z:Landroid/graphics/Rect;

    .line 748
    iget-object v2, p0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 749
    invoke-direct {p0, v1, p1, v0}, Lcom/android/calendar/event/ip;->c(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ip;->as:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 751
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ip;->d(Landroid/graphics/Canvas;)V

    .line 753
    :cond_1
    return-void
.end method

.method private c(Landroid/graphics/Rect;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 17

    .prologue
    .line 788
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->M:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 789
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 790
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->ab:I

    int-to-float v5, v2

    const/4 v6, 0x0

    move-object/from16 v2, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 791
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->I:I

    int-to-float v4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->ab:I

    int-to-float v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->I:I

    int-to-float v6, v2

    move-object/from16 v2, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 794
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/event/ip;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020066

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 795
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 796
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->I:I

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 797
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 798
    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/event/ip;->getWidth()I

    move-result v3

    move-object/from16 v0, p1

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 799
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 800
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 803
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->J:I

    int-to-float v2, v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 804
    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 805
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->ah:I

    .line 806
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/calendar/event/ip;->ad:I

    .line 808
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 809
    const/high16 v2, -0x1000000

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 810
    const/16 v2, 0x7f

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 813
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->I:I

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->J:I

    div-int/lit8 v4, v4, 0x4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    .line 814
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 815
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->u:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v4, v5

    if-le v4, v2, :cond_1

    move v8, v2

    .line 817
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->u:I

    move v9, v2

    move v5, v3

    :goto_1
    if-ge v9, v8, :cond_6

    .line 818
    int-to-float v2, v5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->ad:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float v7, v2, v3

    .line 819
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 820
    const-string v3, " "

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v10

    .line 821
    array-length v2, v10

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 822
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->I:I

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->J:I

    div-int/lit8 v3, v3, 0x6

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 826
    :goto_2
    array-length v12, v10

    const/4 v3, 0x0

    move v4, v3

    move v6, v2

    :goto_3
    if-ge v4, v12, :cond_5

    aget-object v13, v10, v4

    .line 827
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    .line 828
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->ad:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 829
    const-string v2, ""

    .line 830
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v14

    .line 831
    const/4 v3, 0x1

    :goto_4
    if-gt v3, v14, :cond_0

    .line 832
    const/4 v15, 0x0

    invoke-virtual {v13, v15, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 833
    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v15

    .line 834
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/calendar/event/ip;->ad:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_3

    .line 835
    const/4 v2, 0x0

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v13, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 839
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v7, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 843
    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->J:I

    int-to-float v2, v2

    add-float v3, v6, v2

    .line 826
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v6, v3

    goto :goto_3

    .line 815
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->u:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v2, v4

    move v8, v2

    goto/16 :goto_0

    .line 824
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/event/ip;->I:I

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->J:I

    div-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    goto :goto_2

    .line 831
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 841
    :cond_4
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v13, v7, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 846
    :cond_5
    add-int v10, v5, v11

    .line 847
    add-int/lit8 v2, v10, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/event/ip;->f:I

    rem-int v3, v9, v3

    add-int/2addr v2, v3

    int-to-float v3, v2

    const/4 v4, 0x0

    add-int/lit8 v2, v10, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/calendar/event/ip;->f:I

    rem-int v5, v9, v5

    add-int/2addr v2, v5

    int-to-float v5, v2

    const/high16 v2, 0x40800000    # 4.0f

    sub-float/2addr v6, v2

    move-object/from16 v2, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 817
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v5, v10

    goto/16 :goto_1

    .line 849
    :cond_6
    return-void
.end method

.method static synthetic d(Lcom/android/calendar/event/ip;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/calendar/event/ip;->V:I

    return v0
.end method

.method static synthetic d(Lcom/android/calendar/event/ip;I)I
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lcom/android/calendar/event/ip;->ak:I

    return p1
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/16 v6, 0xc

    .line 756
    iget-object v3, p0, Lcom/android/calendar/event/ip;->C:Landroid/graphics/Paint;

    .line 758
    iget v0, p0, Lcom/android/calendar/event/ip;->R:I

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 759
    const/16 v0, 0x99

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 760
    iget v0, p0, Lcom/android/calendar/event/ip;->L:I

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 761
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 762
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 763
    iget-object v0, p0, Lcom/android/calendar/event/ip;->an:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 764
    iget v1, p0, Lcom/android/calendar/event/ip;->ak:I

    if-lt v1, v6, :cond_0

    .line 765
    iget-object v0, p0, Lcom/android/calendar/event/ip;->ao:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 767
    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-int v4, v1

    .line 768
    iget v1, p0, Lcom/android/calendar/event/ip;->aj:I

    iget v2, p0, Lcom/android/calendar/event/ip;->al:I

    add-int/2addr v1, v2

    mul-int/lit8 v2, v4, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 769
    iget v2, p0, Lcom/android/calendar/event/ip;->ak:I

    if-nez v2, :cond_1

    .line 770
    iget v2, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v1, v2

    .line 771
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/ip;->an:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v5, 0x2

    if-le v2, v5, :cond_3

    iget v2, p0, Lcom/android/calendar/event/ip;->ai:I

    div-int/lit8 v2, v2, 0x2

    .line 773
    :goto_0
    iget v5, p0, Lcom/android/calendar/event/ip;->ah:I

    sub-int v2, v5, v2

    .line 774
    int-to-float v5, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v5, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 776
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    if-ge v0, v6, :cond_2

    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ag:I

    add-int/2addr v0, v1

    if-le v0, v6, :cond_2

    .line 778
    iget-object v0, p0, Lcom/android/calendar/event/ip;->ao:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 779
    iget v1, p0, Lcom/android/calendar/event/ip;->aj:I

    iget v5, p0, Lcom/android/calendar/event/ip;->al:I

    add-int/2addr v1, v5

    iget v5, p0, Lcom/android/calendar/event/ip;->ak:I

    rsub-int/lit8 v5, v5, 0xc

    iget v6, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v6, v6, 0x1

    mul-int/2addr v5, v6

    add-int/2addr v1, v5

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    add-int/lit8 v1, v1, 0x1

    .line 782
    int-to-float v2, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 784
    :cond_2
    return-void

    .line 771
    :cond_3
    iget v2, p0, Lcom/android/calendar/event/ip;->ai:I

    goto :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 364
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 365
    iget-object v2, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ip;->v:I

    .line 366
    iget v0, p0, Lcom/android/calendar/event/ip;->v:I

    iget v1, p0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/calendar/event/ip;->w:I

    .line 367
    invoke-direct {p0, v4}, Lcom/android/calendar/event/ip;->setDayPrevNextBtnState(Z)V

    .line 368
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/event/ip;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->g()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 422
    iget v0, p0, Lcom/android/calendar/event/ip;->y:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ak:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_1

    .line 423
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/ip;->y:I

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ip;->y:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v2, p0, Lcom/android/calendar/event/ip;->ag:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x3

    if-le v0, v1, :cond_0

    .line 425
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ag:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/calendar/event/ip;->y:I

    goto :goto_0
.end method

.method static synthetic f(Lcom/android/calendar/event/ip;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->f()V

    return-void
.end method

.method static synthetic g(Lcom/android/calendar/event/ip;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/calendar/event/ip;->ae:I

    return v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 443
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->ak:I

    .line 444
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ae:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/calendar/event/ip;->U:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->al:I

    .line 445
    return-void
.end method

.method private setDayPrevNextBtnState(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 371
    iget-object v0, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    const v1, 0x7f12001d

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ParticipantScheduleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    const v2, 0x7f120021

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ParticipantScheduleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 373
    if-eqz p1, :cond_2

    .line 374
    iget v2, p0, Lcom/android/calendar/event/ip;->v:I

    const v3, 0x24dc87

    if-gt v2, v3, :cond_0

    .line 375
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 379
    :goto_0
    iget v0, p0, Lcom/android/calendar/event/ip;->v:I

    const v2, 0x259d23

    if-lt v0, v2, :cond_1

    .line 380
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 389
    :goto_1
    return-void

    .line 377
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 382
    :cond_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1

    .line 385
    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 386
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method


# virtual methods
.method public a(Z)Landroid/text/format/Time;
    .locals 2

    .prologue
    .line 937
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 938
    if-eqz p1, :cond_0

    .line 939
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 943
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 945
    return-object v0

    .line 941
    :cond_0
    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    goto :goto_0
.end method

.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 345
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Lcom/android/calendar/hj;->e()[C

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 346
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 347
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0001

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 348
    if-eqz v1, :cond_0

    .line 349
    const-string v4, "MDY"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 350
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    :cond_0
    :goto_0
    invoke-static {v0, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->aq:Ljava/lang/String;

    .line 356
    const-string v0, " "

    .line 357
    iget-object v1, p0, Lcom/android/calendar/event/ip;->aq:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 359
    iget-object v1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a(Ljava/lang/String;)V

    .line 360
    iget-object v1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    aget-object v0, v0, v5

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->b(Ljava/lang/String;)V

    .line 361
    return-void

    .line 351
    :cond_1
    const-string v4, "YMD"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/text/format/Time;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 329
    iput-object p2, p0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    .line 330
    iput-object p3, p0, Lcom/android/calendar/event/ip;->e:Ljava/util/ArrayList;

    .line 331
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-virtual {v0, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    iput v0, p0, Lcom/android/calendar/event/ip;->y:I

    .line 333
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 334
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 335
    iget-object v2, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ip;->x:I

    .line 336
    iput p4, p0, Lcom/android/calendar/event/ip;->u:I

    .line 338
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->e()V

    .line 339
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->a()V

    .line 340
    iput-boolean v4, p0, Lcom/android/calendar/event/ip;->H:Z

    .line 341
    iput-boolean v4, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 342
    return-void
.end method

.method a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 863
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/ip;->P:I

    .line 864
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->n:Z

    .line 866
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ip;->s:Lcom/android/calendar/event/is;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 867
    return-void
.end method

.method a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 873
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    .line 874
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 878
    iget v2, p0, Lcom/android/calendar/event/ip;->P:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 879
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 880
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 881
    iget v3, p0, Lcom/android/calendar/event/ip;->U:I

    iput v3, p0, Lcom/android/calendar/event/ip;->af:I

    .line 885
    mul-int/lit8 v0, v0, 0x2

    if-lt v2, v0, :cond_0

    .line 886
    const/16 v0, 0x20

    iput v0, p0, Lcom/android/calendar/event/ip;->P:I

    .line 890
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/ip;->P:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_2

    .line 891
    iget v0, p0, Lcom/android/calendar/event/ip;->af:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    .line 892
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    if-gez v0, :cond_3

    .line 893
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    .line 897
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->g()V

    .line 900
    :cond_2
    iput-boolean v4, p0, Lcom/android/calendar/event/ip;->ap:Z

    .line 901
    iput-boolean v4, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 902
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->invalidate()V

    .line 903
    return-void

    .line 894
    :cond_3
    iget v0, p0, Lcom/android/calendar/event/ip;->U:I

    iget v1, p0, Lcom/android/calendar/event/ip;->V:I

    if-le v0, v1, :cond_1

    .line 895
    iget v0, p0, Lcom/android/calendar/event/ip;->V:I

    iput v0, p0, Lcom/android/calendar/event/ip;->U:I

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 397
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->invalidate()V

    .line 398
    return-void
.end method

.method b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 4

    .prologue
    .line 906
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/ip;->P:I

    .line 907
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->n:Z

    .line 908
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    sub-int/2addr v0, v1

    .line 909
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 910
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 911
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 913
    sget v3, Lcom/android/calendar/hj;->j:I

    if-lt v1, v3, :cond_2

    if-le v1, v2, :cond_2

    .line 914
    iget v1, p0, Lcom/android/calendar/event/ip;->u:I

    .line 915
    if-gez v0, :cond_1

    iget v2, p0, Lcom/android/calendar/event/ip;->u:I

    iget v3, p0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/calendar/event/ip;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 916
    iget v0, p0, Lcom/android/calendar/event/ip;->f:I

    add-int/2addr v0, v1

    .line 922
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    invoke-virtual {v1, v0}, Lcom/android/calendar/event/ParticipantScheduleActivity;->a(I)V

    .line 929
    :cond_0
    :goto_1
    return-void

    .line 917
    :cond_1
    if-lez v0, :cond_0

    iget v0, p0, Lcom/android/calendar/event/ip;->u:I

    if-eqz v0, :cond_0

    .line 918
    iget v0, p0, Lcom/android/calendar/event/ip;->f:I

    sub-int v0, v1, v0

    goto :goto_0

    .line 927
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ip;->s:Lcom/android/calendar/event/is;

    float-to-int v1, p4

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/is;->a(I)V

    .line 928
    iget-object v0, p0, Lcom/android/calendar/event/ip;->s:Lcom/android/calendar/event/is;

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/ip;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public c()V
    .locals 2

    .prologue
    .line 430
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 431
    iget-object v0, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    iput v0, p0, Lcom/android/calendar/event/ip;->ak:I

    .line 435
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/calendar/event/ip;->al:I

    .line 436
    return-void

    .line 432
    :cond_1
    iget v0, p0, Lcom/android/calendar/event/ip;->ak:I

    iget v1, p0, Lcom/android/calendar/event/ip;->ag:I

    add-int/2addr v0, v1

    const/16 v1, 0x18

    if-le v0, v1, :cond_0

    .line 433
    iget v0, p0, Lcom/android/calendar/event/ip;->ag:I

    rsub-int/lit8 v0, v0, 0x18

    iput v0, p0, Lcom/android/calendar/event/ip;->ak:I

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1067
    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->H:Z

    .line 1068
    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 1069
    return-void
.end method

.method public getFirstJulianDay()I
    .locals 1

    .prologue
    .line 1130
    iget v0, p0, Lcom/android/calendar/event/ip;->v:I

    return v0
.end method

.method public getFirstNameIndex()I
    .locals 1

    .prologue
    .line 1195
    iget v0, p0, Lcom/android/calendar/event/ip;->u:I

    return v0
.end method

.method public getLastJulianDay()I
    .locals 1

    .prologue
    .line 1134
    iget v0, p0, Lcom/android/calendar/event/ip;->w:I

    return v0
.end method

.method public getSelectedDay()Landroid/text/format/Time;
    .locals 4

    .prologue
    const v1, 0x259d23

    const v0, 0x24dc87

    .line 292
    new-instance v3, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-direct {v3, v2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 293
    iget v2, p0, Lcom/android/calendar/event/ip;->x:I

    .line 294
    if-ge v2, v0, :cond_0

    .line 298
    :goto_0
    invoke-static {v3, v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;I)J

    .line 299
    invoke-static {v3}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 300
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->normalize(Z)J

    .line 301
    return-object v3

    .line 296
    :cond_0
    if-le v2, v1, :cond_1

    move v0, v1

    .line 297
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method getSelectedDayInMillis()J
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getSelectedDay()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedTime()Landroid/text/format/Time;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 305
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/ip;->a:Lcom/android/calendar/event/ParticipantScheduleActivity;

    iget-object v2, p0, Lcom/android/calendar/event/ip;->at:Ljava/lang/Runnable;

    invoke-static {v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 307
    invoke-static {v0}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 308
    invoke-virtual {v0, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v1

    .line 310
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ip;->b:Landroid/text/format/Time;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    .line 311
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/android/calendar/event/ip;->x:I

    iget v5, p0, Lcom/android/calendar/event/ip;->v:I

    sub-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 312
    invoke-static {v2}, Lcom/android/calendar/hj;->a(Landroid/text/format/Time;)V

    .line 313
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iget-wide v6, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/calendar/hj;->a(JJ)I

    move-result v3

    .line 314
    if-ne v1, v3, :cond_0

    .line 315
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 316
    iget v0, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Landroid/text/format/Time;->hour:I

    .line 323
    :goto_0
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->normalize(Z)J

    .line 324
    return-object v2

    .line 318
    :cond_0
    const/16 v0, 0x8

    iput v0, v2, Landroid/text/format/Time;->hour:I

    goto :goto_0
.end method

.method public getSelectedTimeInMillis()J
    .locals 2

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1073
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->d()V

    .line 1074
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1076
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    .line 1078
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1079
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 501
    iget-boolean v0, p0, Lcom/android/calendar/event/ip;->H:Z

    if-eqz v0, :cond_0

    .line 502
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/ip;->a(II)V

    .line 503
    iput-boolean v2, p0, Lcom/android/calendar/event/ip;->H:Z

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 523
    :goto_0
    return-void

    .line 511
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/ip;->G:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/event/ip;->F:Landroid/graphics/Canvas;

    if-eqz v0, :cond_2

    .line 512
    iget-object v0, p0, Lcom/android/calendar/event/ip;->F:Landroid/graphics/Canvas;

    invoke-direct {p0, v0}, Lcom/android/calendar/event/ip;->a(Landroid/graphics/Canvas;)V

    .line 513
    iput-boolean v2, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 516
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ip;->E:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 517
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ip;->b(Landroid/graphics/Canvas;)V

    .line 521
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ip;->c(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 950
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 952
    packed-switch v1, :pswitch_data_0

    .line 977
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/android/calendar/event/ip;->ar:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 980
    :cond_1
    :goto_1
    return v0

    .line 954
    :pswitch_1
    iget-boolean v1, p0, Lcom/android/calendar/event/ip;->n:Z

    if-nez v1, :cond_1

    .line 959
    iget-boolean v1, p0, Lcom/android/calendar/event/ip;->ap:Z

    if-eqz v1, :cond_0

    .line 960
    iput-boolean v2, p0, Lcom/android/calendar/event/ip;->ap:Z

    .line 961
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->f()V

    .line 962
    iput-boolean v0, p0, Lcom/android/calendar/event/ip;->G:Z

    .line 963
    invoke-virtual {p0}, Lcom/android/calendar/event/ip;->invalidate()V

    goto :goto_0

    .line 969
    :pswitch_2
    iget-object v1, p0, Lcom/android/calendar/event/ip;->ar:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 970
    iput-boolean v2, p0, Lcom/android/calendar/event/ip;->ap:Z

    .line 971
    invoke-direct {p0}, Lcom/android/calendar/event/ip;->f()V

    goto :goto_1

    .line 980
    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    .line 952
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
