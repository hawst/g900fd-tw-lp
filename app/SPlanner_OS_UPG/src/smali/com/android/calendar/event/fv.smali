.class public Lcom/android/calendar/event/fv;
.super Ljava/lang/Object;
.source "EditTaskHelper.java"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;


# instance fields
.field protected b:Z

.field private i:Lcom/android/calendar/ag;

.field private j:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    const-string v1, "body"

    aput-object v1, v0, v5

    const-string v1, "due_date"

    aput-object v1, v0, v6

    const-string v1, "utc_due_date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "groupId"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "reminder_time"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "date_completed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/fv;->a:[Ljava/lang/String;

    .line 97
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "_accountId"

    aput-object v1, v0, v4

    const-string v1, "groupName"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/calendar/event/fv;->c:[Ljava/lang/String;

    .line 109
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "task_id"

    aput-object v1, v0, v4

    const-string v1, "reminder_time"

    aput-object v1, v0, v5

    const-string v1, "state"

    aput-object v1, v0, v6

    const-string v1, "subject"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "start_date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "reminder_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/calendar/event/fv;->d:[Ljava/lang/String;

    .line 137
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    .line 141
    const-string v0, "content://com.android.calendar/TasksReminders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/fv;->f:Landroid/net/Uri;

    .line 146
    const-string v0, "content://com.android.calendar/TasksAccounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    .line 150
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/fv;->h:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/calendar/hh;)V
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/fv;->b:Z

    .line 159
    new-instance v0, Lcom/android/calendar/ag;

    invoke-direct {v0, p1}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    .line 160
    iput-object p1, p0, Lcom/android/calendar/event/fv;->j:Landroid/content/Context;

    .line 161
    return-void
.end method

.method public static a(Lcom/android/calendar/hh;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 267
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    .line 268
    :cond_0
    const-string v0, "EditTaskHelper"

    const-string v1, " getReminderFromCursor Attempted to build non-existent model or from an incorrect query."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_1
    :goto_0
    return-void

    .line 277
    :cond_2
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 278
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 279
    iget-wide v0, p0, Lcom/android/calendar/hh;->b:J

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 280
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->u:I

    .line 281
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->v:J

    .line 282
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->w:I

    .line 283
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->x:I

    goto :goto_0
.end method

.method public static b(Lcom/android/calendar/hh;Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 299
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 300
    :cond_0
    const-string v0, "EditTaskHelper"

    const-string v1, "Attempted to build non-existent model or from an incorrect query."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->f(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :goto_0
    return-void

    .line 307
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/hh;->a()V

    .line 308
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 310
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->b:J

    .line 311
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    .line 312
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    .line 313
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 315
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->o:I

    .line 316
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->d:I

    .line 317
    iget v0, p0, Lcom/android/calendar/hh;->d:I

    iput v0, p0, Lcom/android/calendar/hh;->e:I

    .line 318
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    .line 320
    iget v0, p0, Lcom/android/calendar/hh;->d:I

    if-eqz v0, :cond_5

    .line 321
    iput-boolean v5, p0, Lcom/android/calendar/hh;->I:Z

    .line 322
    iput v4, p0, Lcom/android/calendar/hh;->j:I

    .line 327
    :goto_1
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->p:I

    .line 328
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->q:I

    .line 329
    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->r:J

    .line 330
    iget-wide v0, p0, Lcom/android/calendar/hh;->r:J

    iput-wide v0, p0, Lcom/android/calendar/hh;->s:J

    .line 332
    iget-wide v0, p0, Lcom/android/calendar/hh;->r:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/android/calendar/hh;->p:I

    if-nez v0, :cond_2

    .line 333
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->r:J

    .line 335
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 336
    iget-wide v2, p0, Lcom/android/calendar/hh;->r:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 337
    iput v5, v0, Landroid/text/format/Time;->second:I

    .line 339
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->r:J

    .line 342
    :cond_2
    iget v0, p0, Lcom/android/calendar/hh;->p:I

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/android/calendar/hh;->r:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    .line 343
    iput-boolean v4, p0, Lcom/android/calendar/hh;->t:Z

    .line 346
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    .line 350
    :cond_4
    iput-boolean v4, p0, Lcom/android/calendar/hh;->H:Z

    .line 352
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->y:I

    .line 353
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/hh;->z:J

    goto/16 :goto_0

    .line 324
    :cond_5
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/hh;->j:I

    goto :goto_1
.end method


# virtual methods
.method protected a(J)J
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 435
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 436
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 437
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 438
    const/16 v1, 0x1e

    iput v1, v0, Landroid/text/format/Time;->minute:I

    .line 439
    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 440
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    .line 443
    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v2, 0x1b7740

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/hh;)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    .line 235
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 237
    const-string v0, "task_id"

    iget-wide v6, p1, Lcom/android/calendar/hh;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 238
    const-string v0, "reminder_time"

    iget-wide v6, p1, Lcom/android/calendar/hh;->r:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 239
    const-string v0, "state"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 240
    const-string v0, "start_date"

    iget-object v1, p1, Lcom/android/calendar/hh;->k:Ljava/lang/Long;

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 241
    const-string v0, "due_date"

    iget-object v1, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    const-string v0, "accountkey"

    iget v1, p1, Lcom/android/calendar/hh;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 243
    const-string v0, "subject"

    iget-object v1, p1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, "reminder_type"

    iget v1, p1, Lcom/android/calendar/hh;->p:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 250
    iget-boolean v0, p1, Lcom/android/calendar/hh;->t:Z

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/android/calendar/hh;->u:I

    if-eqz v0, :cond_1

    .line 252
    iget-wide v0, p1, Lcom/android/calendar/hh;->r:J

    iget-wide v6, p1, Lcom/android/calendar/hh;->s:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    .line 253
    const-string v0, "state"

    iget v1, p1, Lcom/android/calendar/hh;->w:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 255
    :cond_0
    sget-object v0, Lcom/android/calendar/event/fv;->f:Landroid/net/Uri;

    iget v1, p1, Lcom/android/calendar/hh;->u:I

    int-to-long v6, v1

    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 256
    iget-object v1, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    iget-object v0, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v2

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v1 .. v9}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 263
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    iget-object v0, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v2

    sget-object v4, Lcom/android/calendar/event/fv;->f:Landroid/net/Uri;

    move-wide v6, v8

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;J)V

    goto :goto_0
.end method

.method public a(Lcom/android/calendar/hh;Lcom/android/calendar/event/fx;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 180
    iget-boolean v1, p0, Lcom/android/calendar/event/fv;->b:Z

    if-nez v1, :cond_0

    .line 230
    :goto_0
    return v0

    .line 189
    :cond_0
    if-nez p1, :cond_1

    .line 190
    const-string v1, "EditTaskHelper"

    const-string v2, "Attempted to save null model."

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 194
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 196
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/fv;->b(Lcom/android/calendar/hh;)Landroid/content/ContentValues;

    move-result-object v0

    .line 198
    iget v1, p1, Lcom/android/calendar/hh;->F:I

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/calendar/hh;->F:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 199
    :cond_2
    const-string v1, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    sget-object v1, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    :cond_3
    :goto_1
    iget v0, p1, Lcom/android/calendar/hh;->p:I

    if-eqz v0, :cond_4

    .line 221
    invoke-virtual {p0, p1}, Lcom/android/calendar/event/fv;->a(Lcom/android/calendar/hh;)Z

    .line 224
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    iget-object v0, p0, Lcom/android/calendar/event/fv;->i:Lcom/android/calendar/ag;

    invoke-static {}, Lcom/android/calendar/ag;->a()I

    move-result v2

    const/4 v3, 0x0

    const-string v4, "com.android.calendar"

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 228
    sget-object v0, Lcom/android/calendar/hj;->x:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/event/fv;->j:Landroid/content/Context;

    const-string v2, "TASK"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    move v0, v8

    .line 230
    goto :goto_0

    .line 209
    :cond_5
    iget v1, p1, Lcom/android/calendar/hh;->F:I

    if-ne v1, v8, :cond_3

    .line 210
    sget-object v1, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    iget-wide v2, p1, Lcom/android/calendar/hh;->b:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 216
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method b(Lcom/android/calendar/hh;)Landroid/content/ContentValues;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 367
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 373
    const-string v1, "subject"

    iget-object v2, p1, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v1, "body"

    iget-object v2, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v1, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 378
    const-string v1, "body_size"

    iget-object v2, p1, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 379
    const-string v1, "bodyType"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 385
    :goto_0
    const-string v1, "start_date"

    iget-object v2, p1, Lcom/android/calendar/hh;->k:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 386
    const-string v1, "utc_start_date"

    iget-object v2, p1, Lcom/android/calendar/hh;->k:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 389
    const-string v1, "due_date"

    iget-object v2, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 390
    const-string v1, "utc_due_date"

    iget-object v2, p1, Lcom/android/calendar/hh;->l:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 393
    const-string v1, "accountKey"

    iget v2, p1, Lcom/android/calendar/hh;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394
    iget v1, p1, Lcom/android/calendar/hh;->d:I

    if-nez v1, :cond_4

    .line 396
    const-string v1, "accountName"

    const-string v2, "My task"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :goto_1
    const-string v1, "importance"

    iget v2, p1, Lcom/android/calendar/hh;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 405
    iget v1, p1, Lcom/android/calendar/hh;->F:I

    if-ne v1, v3, :cond_0

    iget-boolean v1, p1, Lcom/android/calendar/hh;->G:Z

    if-eqz v1, :cond_1

    .line 406
    :cond_0
    const-string v1, "groupId"

    iget v2, p1, Lcom/android/calendar/hh;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    :cond_1
    const-string v1, "reminder_type"

    iget v2, p1, Lcom/android/calendar/hh;->p:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    iget v1, p1, Lcom/android/calendar/hh;->p:I

    if-nez v1, :cond_5

    .line 412
    const/4 v1, 0x0

    .line 413
    const-string v2, "reminder_set"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    const-string v2, "reminder_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :goto_2
    iget v1, p1, Lcom/android/calendar/hh;->y:I

    if-eqz v1, :cond_2

    iget v1, p1, Lcom/android/calendar/hh;->F:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 421
    const-string v1, "complete"

    iget v2, p1, Lcom/android/calendar/hh;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 422
    const-string v1, "date_completed"

    iget-wide v2, p1, Lcom/android/calendar/hh;->z:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    :cond_2
    return-object v0

    .line 381
    :cond_3
    const-string v1, "body_size"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 398
    :cond_4
    const-string v1, "accountName"

    iget-object v2, p1, Lcom/android/calendar/hh;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 416
    :cond_5
    const-string v1, "reminder_set"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 417
    const-string v1, "reminder_time"

    iget-wide v2, p1, Lcom/android/calendar/hh;->r:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_2
.end method
