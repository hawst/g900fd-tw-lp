.class Lcom/android/calendar/event/fu;
.super Landroid/content/AsyncQueryHandler;
.source "EditTaskFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fq;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/fq;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    .line 119
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 120
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 125
    if-nez p3, :cond_0

    .line 284
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-virtual {v0}, Lcom/android/calendar/event/fq;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 141
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 281
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 143
    :sswitch_0
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 146
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 147
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/ft;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/android/calendar/event/ft;->a(I)V

    .line 148
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v4}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;Z)Z

    .line 149
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/ft;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/calendar/event/ft;->run()V

    goto :goto_0

    .line 153
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    invoke-static {v0, p3}, Lcom/android/calendar/event/fv;->b(Lcom/android/calendar/hh;Landroid/database/Cursor;)V

    .line 154
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 155
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v3}, Lcom/android/calendar/event/fq;->b(Lcom/android/calendar/event/fq;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/calendar/hh;->a:Ljava/lang/String;

    .line 156
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-wide v4, v0, Lcom/android/calendar/hh;->b:J

    .line 159
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-boolean v0, v0, Lcom/android/calendar/hh;->t:Z

    if-nez v0, :cond_4

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    .line 160
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->c(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fu;

    move-result-object v0

    sget-object v3, Lcom/android/calendar/event/fv;->f:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/fv;->d:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_1
    const-string v0, " "

    .line 171
    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v3}, Lcom/android/calendar/event/fq;->d(Lcom/android/calendar/event/fq;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v3}, Lcom/android/calendar/event/fq;->e(Lcom/android/calendar/event/fq;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v3}, Lcom/android/calendar/event/fq;->f(Lcom/android/calendar/event/fq;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 176
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->c(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fu;

    move-result-object v3

    const/16 v4, 0x8

    sget-object v6, Lcom/android/calendar/event/fv;->g:Landroid/net/Uri;

    move-object v5, v2

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-virtual/range {v3 .. v10}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v0, v0, Lcom/android/calendar/hh;->F:I

    if-ne v0, v1, :cond_6

    .line 180
    const-string v7, " _id desc"

    .line 182
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->c(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fu;

    move-result-object v0

    const/16 v1, 0x10

    sget-object v3, Lcom/android/calendar/event/fv;->e:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/fv;->a:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v11}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    goto/16 :goto_0

    .line 165
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    goto :goto_1

    .line 190
    :sswitch_1
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_8

    .line 191
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 192
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    goto/16 :goto_0

    .line 197
    :cond_8
    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    invoke-static {v0, p3}, Lcom/android/calendar/event/fv;->a(Lcom/android/calendar/hh;Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 202
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v1}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    goto/16 :goto_0

    .line 199
    :catchall_0
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 205
    :sswitch_2
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_9

    .line 206
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 211
    :cond_9
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    invoke-virtual {v0, p3}, Lcom/android/calendar/event/fx;->a(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 213
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 216
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0, v8}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    .line 217
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->g(Lcom/android/calendar/event/fq;)V

    goto/16 :goto_0

    .line 213
    :catchall_1
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 220
    :sswitch_3
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    if-eqz v0, :cond_a

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_b

    .line 221
    :cond_a
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 227
    :cond_b
    :try_start_2
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->h(Lcom/android/calendar/event/fq;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 228
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {p3}, Lcom/android/calendar/task/ab;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/event/fq;->c:Ljava/util/ArrayList;

    .line 229
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 230
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 231
    iget-object v1, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v1, v1, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v3, v3, Lcom/android/calendar/event/fq;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v3}, Lcom/android/calendar/event/fx;->b(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 240
    :goto_2
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 244
    new-array v6, v11, [Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v4

    .line 248
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {v0}, Lcom/android/calendar/event/fq;->c(Lcom/android/calendar/event/fq;)Lcom/android/calendar/event/fu;

    move-result-object v0

    sget-object v3, Lcom/android/calendar/event/fv;->h:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/fv;->c:[Ljava/lang/String;

    const-string v5, "_accountId=?"

    const-string v7, "_accountId ASC, group_order ASC"

    move v1, v8

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/fu;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/android/calendar/event/fq;->a(Lcom/android/calendar/event/fq;I)V

    goto/16 :goto_0

    .line 233
    :cond_c
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    invoke-static {p3}, Lcom/android/calendar/task/ab;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/event/fq;->c:Ljava/util/ArrayList;

    .line 234
    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 235
    invoke-static {p3}, Lcom/android/calendar/hj;->a(Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v1, v1, Lcom/android/calendar/event/fq;->b:Lcom/android/calendar/event/fx;

    iget-object v3, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v3, v3, Lcom/android/calendar/event/fq;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v3}, Lcom/android/calendar/event/fx;->a(Landroid/database/Cursor;Ljava/util/ArrayList;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    .line 240
    :catchall_2
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 256
    :sswitch_4
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_d

    .line 257
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    const-wide/16 v2, 0x1

    iput-wide v2, v0, Lcom/android/calendar/hh;->b:J

    .line 258
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 263
    :cond_d
    :try_start_4
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    .line 264
    const/4 v0, 0x0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    .line 266
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget v0, v0, Lcom/android/calendar/hh;->F:I

    if-ne v0, v1, :cond_e

    .line 267
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iput-wide v2, v0, Lcom/android/calendar/hh;->c:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 277
    :goto_3
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 269
    :cond_e
    :try_start_5
    iget-object v0, p0, Lcom/android/calendar/event/fu;->a:Lcom/android/calendar/event/fq;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iput-wide v2, v0, Lcom/android/calendar/hh;->b:J
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_3

    .line 277
    :catchall_3
    move-exception v0

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method
