.class Lcom/android/calendar/event/an;
.super Ljava/lang/Object;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lcom/android/calendar/event/ak;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ak;ZZZ)V
    .locals 0

    .prologue
    .line 834
    iput-object p1, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iput-boolean p2, p0, Lcom/android/calendar/event/an;->a:Z

    iput-boolean p3, p0, Lcom/android/calendar/event/an;->b:Z

    iput-boolean p4, p0, Lcom/android/calendar/event/an;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 837
    if-nez p2, :cond_4

    .line 840
    iget-boolean v0, p0, Lcom/android/calendar/event/an;->a:Z

    if-eqz v0, :cond_3

    .line 841
    iget-boolean v0, p0, Lcom/android/calendar/event/an;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/event/an;->c:Z

    if-nez v0, :cond_2

    .line 842
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    .line 860
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_1

    .line 861
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v1}, Lcom/android/calendar/event/ak;->k(Lcom/android/calendar/event/ak;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(I)V

    .line 862
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->d()V

    .line 864
    :cond_1
    return-void

    .line 844
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v2}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    goto :goto_0

    .line 847
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v3}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    .line 848
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->Z:Ljava/lang/String;

    .line 849
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v0, v0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    iget-object v1, v1, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    iput-wide v2, v0, Lcom/android/calendar/as;->aa:J

    goto :goto_0

    .line 851
    :cond_4
    if-ne p2, v3, :cond_6

    .line 852
    iget-boolean v0, p0, Lcom/android/calendar/event/an;->b:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/event/an;->c:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/android/calendar/event/an;->a:Z

    if-nez v0, :cond_5

    .line 853
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v1}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    goto :goto_0

    .line 855
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v2}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    goto :goto_0

    .line 857
    :cond_6
    if-ne p2, v1, :cond_0

    .line 858
    iget-object v0, p0, Lcom/android/calendar/event/an;->d:Lcom/android/calendar/event/ak;

    invoke-static {v0, v2}, Lcom/android/calendar/event/ak;->c(Lcom/android/calendar/event/ak;I)I

    goto :goto_0
.end method
