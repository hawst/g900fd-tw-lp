.class Lcom/android/calendar/event/dd;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/res/Resources;

.field final synthetic b:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 5138
    iput-object p1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iput-object p2, p0, Lcom/android/calendar/event/dd;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 5141
    iget-object v0, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    invoke-static {v0}, Lcom/android/calendar/event/ay;->L(Lcom/android/calendar/event/ay;)I

    move-result v0

    .line 5142
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->p:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 5143
    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/event/dd;->a:Landroid/content/res/Resources;

    const v2, 0x7f0c0170

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 5145
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->ao:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 5146
    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/calendar/event/dd;->a:Landroid/content/res/Resources;

    const v2, 0x7f0c0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 5148
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    invoke-static {v1}, Lcom/android/calendar/event/ay;->K(Lcom/android/calendar/event/ay;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 5149
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 5151
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v1}, Lcom/android/calendar/event/AttendeesView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 5152
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v1}, Lcom/android/calendar/event/AttendeesView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 5154
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    .line 5155
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->af:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 5157
    :cond_4
    iget-object v1, p0, Lcom/android/calendar/event/dd;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->a:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 5158
    return-void
.end method
