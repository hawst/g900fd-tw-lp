.class Lcom/android/calendar/event/gr;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;)V
    .locals 0

    .prologue
    .line 1838
    iput-object p1, p0, Lcom/android/calendar/event/gr;->a:Lcom/android/calendar/event/fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1842
    iget-object v0, p0, Lcom/android/calendar/event/gr;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/gr;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844
    iget-object v0, p0, Lcom/android/calendar/event/gr;->a:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->u(Lcom/android/calendar/event/fx;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1845
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1847
    const-string v1, "extra_prefs_show_button_bar"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1849
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/gr;->a:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->f(Lcom/android/calendar/event/fx;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1854
    :cond_0
    :goto_0
    return-void

    .line 1850
    :catch_0
    move-exception v0

    .line 1851
    const-string v0, "EditTaskView"

    const-string v1, "Error: Could not find Settings.ACTION_WIFI_SETTINGS activity."

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
