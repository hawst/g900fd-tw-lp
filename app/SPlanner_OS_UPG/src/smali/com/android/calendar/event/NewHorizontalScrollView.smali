.class public Lcom/android/calendar/event/NewHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "NewHorizontalScrollView.java"


# instance fields
.field private a:Lcom/android/calendar/event/NewHorizontalScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 38
    return-void
.end method


# virtual methods
.method public fling(I)V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/NewHorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 54
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setFollowView(Lcom/android/calendar/event/NewHorizontalScrollView;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/android/calendar/event/NewHorizontalScrollView;->a:Lcom/android/calendar/event/NewHorizontalScrollView;

    .line 42
    return-void
.end method
