.class public Lcom/android/calendar/event/SelectMapActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SelectMapActivity.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private e:Landroid/widget/SearchView;

.field private f:Landroid/graphics/Bitmap;

.field private g:D

.field private h:D

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/maps/c;

.field private k:Landroid/location/Geocoder;

.field private l:Lcom/google/android/gms/maps/model/MarkerOptions;

.field private m:Landroid/location/LocationManager;

.field private n:Lcom/google/android/gms/maps/model/LatLng;

.field private final o:Ljava/lang/Object;

.field private p:Landroid/location/Criteria;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z

.field private t:Landroid/app/AlertDialog;

.field private u:Landroid/view/View$OnClickListener;

.field private v:Landroid/location/LocationListener;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 76
    const-string v0, "SelectMapActivity"

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->a:Ljava/lang/String;

    .line 81
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/SelectMapActivity;->b:I

    .line 91
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->f:Landroid/graphics/Bitmap;

    .line 92
    iput-wide v2, p0, Lcom/android/calendar/event/SelectMapActivity;->g:D

    .line 93
    iput-wide v2, p0, Lcom/android/calendar/event/SelectMapActivity;->h:D

    .line 94
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->i:Ljava/lang/String;

    .line 102
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    .line 104
    iput-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    .line 105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->o:Ljava/lang/Object;

    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapActivity;->r:Z

    .line 648
    new-instance v0, Lcom/android/calendar/event/jn;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/jn;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->u:Landroid/view/View$OnClickListener;

    .line 781
    new-instance v0, Lcom/android/calendar/event/jf;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/jf;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    return-void
.end method

.method private a(I)D
    .locals 4

    .prologue
    .line 855
    int-to-double v0, p1

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private a(D)I
    .locals 3

    .prologue
    .line 851
    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapActivity;->f:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    return-object v0
.end method

.method private a(Landroid/location/Address;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 590
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 592
    if-eqz p1, :cond_2

    .line 593
    invoke-virtual {p1}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v2

    .line 594
    const/4 v0, 0x0

    :goto_0
    if-gt v0, v2, :cond_2

    .line 595
    invoke-virtual {p1, v0}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v3

    .line 596
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 597
    if-nez v0, :cond_1

    .line 598
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 600
    :cond_1
    const/16 v4, 0x20

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 606
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;Landroid/location/Address;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 378
    if-eqz p1, :cond_1

    .line 380
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 385
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 386
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    if-lez v0, :cond_2

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->twSetSelection(I)V

    .line 387
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 390
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 396
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 386
    goto :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x7d0

    .line 481
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 482
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    .line 483
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/c;->c()V

    .line 484
    const/high16 v1, 0x41500000    # 13.0f

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_0

    const/high16 v1, 0x41880000    # 17.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-static {p1, v1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    .line 490
    :goto_0
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 491
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    .line 494
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 495
    const/4 v0, 0x1

    .line 497
    :goto_1
    return v0

    .line 488
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-static {p1}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 497
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 466
    if-eqz p2, :cond_0

    .line 467
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->l:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    .line 468
    iput-object p2, p0, Lcom/android/calendar/event/SelectMapActivity;->i:Ljava/lang/String;

    .line 472
    :goto_0
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iput-wide v0, p0, Lcom/android/calendar/event/SelectMapActivity;->g:D

    .line 473
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iput-wide v0, p0, Lcom/android/calendar/event/SelectMapActivity;->h:D

    .line 474
    const/4 v0, 0x1

    .line 476
    :goto_1
    return v0

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->l:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/d;

    goto :goto_0

    .line 476
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapActivity;)I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/android/calendar/event/SelectMapActivity;->b:I

    return v0
.end method

.method private b(Landroid/location/Address;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 9

    .prologue
    .line 811
    const v0, 0x55d4a80

    const v1, -0x55d4a80

    .line 812
    const v2, 0xaba9500

    const v3, -0xaba9500

    .line 814
    invoke-virtual {p1}, Landroid/location/Address;->getLatitude()D

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/calendar/event/SelectMapActivity;->a(D)I

    move-result v4

    .line 815
    invoke-virtual {p1}, Landroid/location/Address;->getLongitude()D

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/android/calendar/event/SelectMapActivity;->a(D)I

    move-result v5

    .line 816
    const-string v6, "SelectMapActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getValidGeoPointFromAddress/latitude:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " longitude:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 819
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 820
    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 821
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 823
    sub-int/2addr v1, v0

    .line 824
    sub-int/2addr v3, v2

    .line 826
    const-string v4, "SelectMapActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getValidGeoPointFromAddress/latitude:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-int/lit8 v6, v1, 0x2

    add-int/2addr v6, v0

    invoke-direct {p0, v6}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " longitude:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-int/lit8 v6, v3, 0x2

    add-int/2addr v6, v2

    invoke-direct {p0, v6}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v2

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v4
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapActivity;Landroid/location/Address;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->b(Landroid/location/Address;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 143
    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 144
    const-string v1, "mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/SelectMapActivity;->b:I

    .line 145
    const-string v1, "map_latitude"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/calendar/event/SelectMapActivity;->g:D

    .line 146
    const-string v1, "map_longtitude"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/SelectMapActivity;->h:D

    .line 147
    return-void
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLng;)Z
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 863
    :goto_0
    return v0

    .line 862
    :cond_0
    const-string v0, "SelectMapActivity"

    const-string v1, "Search Point hasn\'t been changed"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/Geocoder;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->k:Landroid/location/Geocoder;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectMapActivity;Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    return-object p1
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->k:Landroid/location/Geocoder;

    .line 152
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f120295

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment;->b()Lcom/google/android/gms/maps/c;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    .line 153
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    new-instance v1, Lcom/android/calendar/event/je;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/je;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/i;)V

    .line 163
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    new-instance v1, Lcom/android/calendar/event/jg;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jg;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/j;)V

    .line 190
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    new-instance v1, Lcom/android/calendar/event/jh;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jh;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/k;)V

    .line 202
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/c;->c(Z)V

    .line 203
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/c;->b(Z)V

    .line 204
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/c;->a(Z)Z

    .line 205
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->c()V

    .line 206
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    const v1, 0x7f02029b

    invoke-static {v1}, Lcom/google/android/gms/maps/model/b;->a(I)Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->l:Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/calendar/event/SelectMapActivity;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->o:Ljava/lang/Object;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 210
    const v0, 0x7f12003c

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    .line 211
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    const v2, 0x10000003

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    .line 212
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    new-instance v2, Lcom/android/calendar/event/ji;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/ji;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 225
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    const-string v2, "My_Location"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iput-boolean v4, p0, Lcom/android/calendar/event/SelectMapActivity;->r:Z

    .line 228
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 229
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 230
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    if-lez v0, :cond_3

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->twSetSelection(I)V

    .line 231
    new-instance v0, Lcom/android/calendar/event/jp;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/calendar/event/jp;-><init>(Lcom/android/calendar/event/SelectMapActivity;Lcom/android/calendar/event/je;)V

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/jp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 234
    :cond_0
    const v0, 0x7f120294

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 236
    new-instance v1, Lcom/android/calendar/event/jj;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jj;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 258
    new-instance v1, Lcom/android/calendar/event/jk;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/jk;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 270
    const-string v0, ""

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->k()V

    .line 283
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 230
    goto :goto_0

    .line 278
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    goto :goto_1
.end method

.method private e()V
    .locals 4

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0216

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapActivity;->c:I

    .line 289
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapActivity;->s:Z

    if-nez v0, :cond_2

    .line 290
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 292
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 294
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 295
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 297
    if-eqz v2, :cond_0

    const/4 v0, 0x2

    if-ne v2, v0, :cond_1

    .line 298
    :cond_0
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 304
    :goto_0
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0210

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 306
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    .line 308
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/SelectMapActivity;->d:I

    .line 312
    :goto_1
    return-void

    .line 300
    :cond_1
    iget v0, v1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 310
    :cond_2
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0217

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/SelectMapActivity;->d:I

    goto :goto_1
.end method

.method static synthetic e(Lcom/android/calendar/event/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->f()V

    return-void
.end method

.method static synthetic f(Lcom/android/calendar/event/SelectMapActivity;)Landroid/widget/SearchView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 357
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 365
    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 368
    new-instance v0, Lcom/android/calendar/event/jp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/jp;-><init>(Lcom/android/calendar/event/SelectMapActivity;Lcom/android/calendar/event/je;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/jp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 370
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 371
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 616
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->t:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 617
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->t:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 622
    const v1, 0x7f0f01aa

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 623
    const v1, 0x7f0f0119

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 624
    const v1, 0x7f0f029a

    new-instance v2, Lcom/android/calendar/event/jl;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/jl;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 636
    const v1, 0x7f0f0163

    new-instance v2, Lcom/android/calendar/event/jm;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/jm;-><init>(Lcom/android/calendar/event/SelectMapActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 643
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 644
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->t:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method static synthetic g(Lcom/android/calendar/event/SelectMapActivity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->k()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/event/SelectMapActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 671
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_1

    .line 673
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 674
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 675
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 676
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 677
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 678
    const v1, 0x7f04007e

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 680
    const v0, 0x7f1200d4

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 681
    const v1, 0x7f1200d5

    invoke-virtual {p0, v1}, Lcom/android/calendar/event/SelectMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 682
    if-eqz v1, :cond_0

    .line 683
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 685
    iget v2, p0, Lcom/android/calendar/event/SelectMapActivity;->b:I

    if-nez v2, :cond_0

    .line 686
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 689
    :cond_0
    if-eqz v0, :cond_1

    .line 690
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 694
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapActivity;->s:Z

    if-nez v0, :cond_2

    .line 695
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 696
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 697
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 698
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 700
    :cond_2
    return-void
.end method

.method static synthetic i(Lcom/android/calendar/event/SelectMapActivity;)I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/android/calendar/event/SelectMapActivity;->d:I

    return v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 703
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 704
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/event/SelectMapActivity;)I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/android/calendar/event/SelectMapActivity;->c:I

    return v0
.end method

.method private j()Lcom/google/android/gms/maps/model/LatLng;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 711
    const-string v0, "SelectMapActivity"

    const-string v1, "get My Lastest Location"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 716
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 717
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    .line 720
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    if-nez v0, :cond_1

    .line 721
    new-instance v0, Landroid/location/Criteria;

    invoke-direct {v0}, Landroid/location/Criteria;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    .line 722
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 723
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    invoke-virtual {v0, v9}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 724
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    invoke-virtual {v0, v9}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 725
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    invoke-virtual {v0, v9}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 728
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->p:Landroid/location/Criteria;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/location/LocationManager;->getBestProvider(Landroid/location/Criteria;Z)Ljava/lang/String;

    move-result-object v0

    .line 729
    if-nez v0, :cond_2

    .line 730
    const-string v0, "network"

    .line 733
    :cond_2
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    invoke-virtual {v1, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 734
    const/4 v7, 0x0

    .line 736
    if-nez v0, :cond_6

    .line 738
    :try_start_0
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v8

    .line 739
    if-eqz v8, :cond_3

    .line 740
    const-string v1, "SelectMapActivity"

    const-string v2, "NETWORK_PROVIDER is enabled"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 742
    :try_start_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v5, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_9

    move-object v0, v6

    :cond_3
    move v1, v8

    .line 755
    :goto_0
    if-nez v1, :cond_6

    .line 757
    :try_start_2
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    .line 758
    if-eqz v1, :cond_4

    .line 759
    const-string v1, "SelectMapActivity"

    const-string v2, "GPS_PROVIDER is enabled"

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_5

    move-result-object v6

    .line 761
    :try_start_3
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x447a0000    # 1000.0f

    iget-object v5, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_6

    move-object v0, v6

    :cond_4
    move-object v6, v0

    .line 773
    :goto_1
    if-eqz v6, :cond_5

    .line 774
    const-string v0, "SelectMapActivity"

    const-string v1, "Got Valid Location"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 778
    :goto_2
    return-object v0

    .line 744
    :catch_0
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 746
    :goto_3
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move-object v0, v6

    move v1, v9

    .line 753
    goto :goto_0

    .line 747
    :catch_1
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 749
    :goto_4
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    move-object v0, v6

    move v1, v9

    .line 753
    goto :goto_0

    .line 750
    :catch_2
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 752
    :goto_5
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    move-object v0, v6

    move v1, v9

    goto :goto_0

    .line 763
    :catch_3
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 764
    :goto_6
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 765
    :catch_4
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 766
    :goto_7
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 767
    :catch_5
    move-exception v1

    move-object v6, v0

    move-object v0, v1

    .line 768
    :goto_8
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 767
    :catch_6
    move-exception v0

    goto :goto_8

    .line 765
    :catch_7
    move-exception v0

    goto :goto_7

    .line 763
    :catch_8
    move-exception v0

    goto :goto_6

    .line 750
    :catch_9
    move-exception v0

    goto :goto_5

    .line 747
    :catch_a
    move-exception v0

    goto :goto_4

    .line 744
    :catch_b
    move-exception v0

    goto :goto_3

    :cond_5
    move-object v0, v7

    goto :goto_2

    :cond_6
    move-object v6, v0

    goto :goto_1
.end method

.method static synthetic k(Lcom/android/calendar/event/SelectMapActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->f:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private k()V
    .locals 5

    .prologue
    .line 832
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->j()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 833
    if-eqz v0, :cond_3

    .line 834
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 835
    :try_start_0
    invoke-direct {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->b(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 836
    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v0, :cond_1

    .line 839
    new-instance v0, Lcom/android/calendar/event/jp;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/calendar/event/jp;-><init>(Lcom/android/calendar/event/SelectMapActivity;Lcom/android/calendar/event/je;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/android/calendar/event/jp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 841
    :cond_1
    monitor-exit v1

    .line 848
    :cond_2
    :goto_0
    return-void

    .line 841
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 843
    :cond_3
    const-string v0, "SelectMapActivity"

    const-string v1, "Search Point is NULL"

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 845
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->g()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/android/calendar/event/SelectMapActivity;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectMapActivity;->r:Z

    return v0
.end method

.method static synthetic m(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic o(Lcom/android/calendar/event/SelectMapActivity;)Landroid/location/LocationListener;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 7

    .prologue
    .line 561
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 562
    const v0, 0x7f0f0456

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    const/4 v1, 0x0

    .line 565
    const/high16 v0, 0x41700000    # 15.0f

    .line 566
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    if-eqz v2, :cond_3

    .line 567
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/c;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    .line 568
    if-eqz v2, :cond_3

    .line 569
    iget-object v1, v2, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    .line 570
    iget v0, v2, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    move v6, v0

    move-object v0, v1

    move v1, v6

    .line 574
    :goto_1
    if-nez v0, :cond_2

    .line 575
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    .line 578
    :cond_2
    iget-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 579
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 580
    const-string v3, "latitude"

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-direct {p0, v4, v5}, Lcom/android/calendar/event/SelectMapActivity;->a(D)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 581
    const-string v3, "longitude"

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {p0, v4, v5}, Lcom/android/calendar/event/SelectMapActivity;->a(D)I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 582
    const-string v0, "location"

    iget-object v3, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 583
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    if-eqz v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->j:Lcom/google/android/gms/maps/c;

    new-instance v3, Lcom/android/calendar/event/jo;

    invoke-direct {v3, p0, p0, v2, v1}, Lcom/android/calendar/event/jo;-><init>(Lcom/android/calendar/event/SelectMapActivity;Landroid/app/Activity;Landroid/content/Intent;F)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/l;)V

    goto :goto_0

    :cond_3
    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 611
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 612
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->invalidateOptionsMenu()V

    .line 613
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 126
    :try_start_0
    const-string v0, "android.os.AsyncTask"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    const v0, 0x7f0a000a

    invoke-static {p0, v0}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectMapActivity;->s:Z

    .line 133
    const v0, 0x7f04008d

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectMapActivity;->setContentView(I)V

    .line 134
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->b()V

    .line 135
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->c()V

    .line 136
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->d()V

    .line 137
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->e()V

    .line 138
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->h()V

    .line 139
    return-void

    .line 127
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 349
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 350
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->q:Ljava/lang/String;

    .line 351
    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/SelectMapActivity;->g:D

    .line 352
    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/calendar/event/SelectMapActivity;->h:D

    .line 353
    const-string v0, "snippet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->i:Ljava/lang/String;

    .line 354
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 324
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 325
    invoke-static {p0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectMapActivity;->finish()V

    .line 328
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 340
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 341
    const-string v0, "location"

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->e:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v0, "latitude"

    iget-wide v2, p0, Lcom/android/calendar/event/SelectMapActivity;->g:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 343
    const-string v0, "longitude"

    iget-wide v2, p0, Lcom/android/calendar/event/SelectMapActivity;->h:D

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 344
    const-string v0, "snippet"

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 316
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 317
    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 318
    :try_start_0
    invoke-direct {p0}, Lcom/android/calendar/event/SelectMapActivity;->j()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->n:Lcom/google/android/gms/maps/model/LatLng;

    .line 319
    monitor-exit v1

    .line 320
    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 332
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 333
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/android/calendar/event/SelectMapActivity;->m:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/android/calendar/event/SelectMapActivity;->v:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 336
    :cond_0
    return-void
.end method
