.class Lcom/android/calendar/event/ad;
.super Ljava/lang/Object;
.source "EasyEditEventView.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field a:Landroid/view/View;

.field final synthetic b:Lcom/android/calendar/event/v;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/v;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 559
    iput-object p2, p0, Lcom/android/calendar/event/ad;->a:Landroid/view/View;

    .line 560
    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 18

    .prologue
    .line 565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v2}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v2

    .line 566
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v12

    .line 570
    const-wide/16 v4, 0x0

    .line 571
    const-wide/16 v6, 0x0

    .line 573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v8}, Lcom/android/calendar/event/v;->h(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 575
    iget v3, v12, Landroid/text/format/Time;->second:I

    iget v4, v2, Landroid/text/format/Time;->second:I

    sub-int v13, v3, v4

    .line 576
    iget v3, v12, Landroid/text/format/Time;->minute:I

    iget v4, v2, Landroid/text/format/Time;->minute:I

    sub-int v14, v3, v4

    .line 577
    iget v3, v12, Landroid/text/format/Time;->hour:I

    iget v4, v2, Landroid/text/format/Time;->hour:I

    sub-int v15, v3, v4

    .line 578
    iget v3, v12, Landroid/text/format/Time;->monthDay:I

    iget v4, v2, Landroid/text/format/Time;->monthDay:I

    sub-int v16, v3, v4

    .line 579
    iget v3, v12, Landroid/text/format/Time;->month:I

    iget v4, v2, Landroid/text/format/Time;->month:I

    sub-int v17, v3, v4

    .line 580
    iget v3, v12, Landroid/text/format/Time;->year:I

    iget v4, v2, Landroid/text/format/Time;->year:I

    sub-int v9, v3, v4

    .line 582
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v4}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v5}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v5

    iget v5, v5, Landroid/text/format/Time;->hour:I

    move/from16 v6, p4

    move/from16 v7, p3

    move/from16 v8, p2

    invoke-virtual/range {v2 .. v8}, Landroid/text/format/Time;->set(IIIIII)V

    .line 584
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 587
    invoke-static {v2, v12}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-lez v3, :cond_3

    .line 588
    if-lez v15, :cond_2

    .line 589
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    add-int v6, v3, v15

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    .line 597
    :goto_0
    invoke-static {v12}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 598
    const v3, 0x259d23

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 608
    :cond_0
    :goto_1
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    move-wide v4, v10

    .line 624
    :cond_1
    :goto_2
    cmp-long v3, v4, v6

    if-lez v3, :cond_6

    .line 625
    invoke-virtual {v12, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    move-wide v2, v4

    .line 629
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v6}, Lcom/android/calendar/event/v;->h(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v7, v4, v5}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 630
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v4}, Lcom/android/calendar/event/v;->j(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v5, v2, v3}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 631
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->k(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;

    move-result-object v3

    invoke-static {v2, v3, v12}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/event/v;Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v2}, Lcom/android/calendar/event/v;->o(Lcom/android/calendar/event/v;)V

    .line 633
    return-void

    .line 593
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v6, v3, Landroid/text/format/Time;->hour:I

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    goto/16 :goto_0

    .line 601
    :cond_3
    add-int v9, v9, p2

    .line 602
    add-int v8, p3, v17

    .line 603
    add-int v7, p4, v16

    .line 604
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->second:I

    add-int v4, v3, v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->minute:I

    add-int v5, v3, v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v3, v3, Landroid/text/format/Time;->hour:I

    add-int v6, v3, v15

    move-object v3, v12

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    goto/16 :goto_1

    .line 609
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v8}, Lcom/android/calendar/event/v;->j(Lcom/android/calendar/event/v;)Landroid/widget/Button;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 611
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    .line 612
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v4, v3, Landroid/text/format/Time;->second:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v5, v3, Landroid/text/format/Time;->minute:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;

    move-result-object v3

    iget v6, v3, Landroid/text/format/Time;->hour:I

    move-object v3, v12

    move/from16 v7, p4

    move/from16 v8, p3

    move/from16 v9, p2

    invoke-virtual/range {v3 .. v9}, Landroid/text/format/Time;->set(IIIIII)V

    .line 613
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 616
    invoke-static {v2, v12}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-lez v3, :cond_5

    .line 617
    invoke-virtual {v12, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 618
    iget v3, v2, Landroid/text/format/Time;->hour:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v12, Landroid/text/format/Time;->hour:I

    .line 619
    const/4 v3, 0x1

    invoke-virtual {v12, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 620
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/event/ad;->b:Lcom/android/calendar/event/v;

    invoke-static {v3}, Lcom/android/calendar/event/v;->n(Lcom/android/calendar/event/v;)V

    :cond_5
    move-wide v4, v10

    goto/16 :goto_2

    :cond_6
    move-wide v2, v6

    goto/16 :goto_3
.end method
