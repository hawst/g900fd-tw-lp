.class Lcom/android/calendar/event/co;
.super Ljava/lang/Object;
.source "EditEventView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field a:I

.field b:Landroid/view/ViewGroup$LayoutParams;

.field final synthetic c:Landroid/widget/LinearLayout;

.field final synthetic d:I

.field final synthetic e:Landroid/widget/LinearLayout;

.field final synthetic f:Lcom/android/calendar/event/ay;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/ay;Landroid/widget/LinearLayout;ILandroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 4339
    iput-object p1, p0, Lcom/android/calendar/event/co;->f:Lcom/android/calendar/event/ay;

    iput-object p2, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    iput p3, p0, Lcom/android/calendar/event/co;->d:I

    iput-object p4, p0, Lcom/android/calendar/event/co;->e:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    .line 4345
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 4346
    iget-object v1, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getTop()I

    move-result v1

    iput v1, p0, Lcom/android/calendar/event/co;->a:I

    .line 4347
    iget-object v1, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/event/co;->b:Landroid/view/ViewGroup$LayoutParams;

    .line 4348
    iget-object v1, p0, Lcom/android/calendar/event/co;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/android/calendar/event/co;->d:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/calendar/event/co;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 4349
    iget-object v0, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 4350
    iget-object v0, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/calendar/event/co;->b:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4352
    iget-object v0, p0, Lcom/android/calendar/event/co;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->invalidate()V

    .line 4353
    return-void
.end method
