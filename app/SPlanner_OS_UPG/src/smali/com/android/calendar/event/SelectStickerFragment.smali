.class public Lcom/android/calendar/event/SelectStickerFragment;
.super Landroid/app/DialogFragment;
.source "SelectStickerFragment.java"


# static fields
.field public static b:Lcom/android/calendar/event/kx;

.field public static d:Ljava/util/ArrayList;

.field public static e:Ljava/util/ArrayList;

.field private static w:Landroid/util/LruCache;

.field private static final x:Ljava/lang/Object;

.field private static y:Z


# instance fields
.field private A:Landroid/graphics/Rect;

.field public a:Ljava/util/ArrayList;

.field public c:I

.field public f:Z

.field public g:Landroid/graphics/Rect;

.field protected h:Landroid/widget/AdapterView$OnItemClickListener;

.field protected i:Landroid/widget/AdapterView$OnItemClickListener;

.field private j:Landroid/app/Activity;

.field private k:Landroid/content/AsyncQueryHandler;

.field private l:Z

.field private m:Z

.field private n:Landroid/view/GestureDetector;

.field private o:[Ljava/lang/String;

.field private p:Landroid/widget/ScrollView;

.field private q:Landroid/widget/GridView;

.field private r:Landroid/widget/GridView;

.field private s:Landroid/widget/LinearLayout;

.field private t:Landroid/widget/HorizontalScrollView;

.field private u:Landroid/widget/LinearLayout;

.field private v:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->x:Ljava/lang/Object;

    .line 112
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/calendar/event/SelectStickerFragment;->y:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 124
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 92
    iput v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    .line 93
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->p:Landroid/widget/ScrollView;

    .line 94
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    .line 95
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    .line 96
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->s:Landroid/widget/LinearLayout;

    .line 97
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->t:Landroid/widget/HorizontalScrollView;

    .line 98
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    .line 100
    const/16 v0, 0xc

    iput v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    .line 120
    iput-boolean v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->f:Z

    .line 122
    iput-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    .line 408
    new-instance v0, Lcom/android/calendar/event/kp;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/kp;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 580
    new-instance v0, Lcom/android/calendar/event/kr;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/kr;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->i:Landroid/widget/AdapterView$OnItemClickListener;

    .line 125
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/android/calendar/event/SelectStickerFragment;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectStickerFragment;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/calendar/event/SelectStickerFragment;->A:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic a(Landroid/util/LruCache;)Landroid/util/LruCache;
    .locals 0

    .prologue
    .line 76
    sput-object p0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    return-object p0
.end method

.method static synthetic a(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->n:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static a()Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 181
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 182
    new-instance v1, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    const-wide/16 v2, 0x0

    const-string v4, "empty"

    const-string v5, ""

    move v7, v6

    move v8, v6

    invoke-direct/range {v1 .. v8}, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;-><init>(JLjava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 184
    iget v2, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->e:I

    if-eqz v2, :cond_0

    .line 185
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 188
    :cond_1
    new-instance v0, Lcom/android/calendar/event/kw;

    invoke-direct {v0}, Lcom/android/calendar/event/kw;-><init>()V

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 189
    sput-object v9, Lcom/android/calendar/event/SelectStickerFragment;->e:Ljava/util/ArrayList;

    .line 190
    return-object v9
.end method

.method static a(I)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 205
    if-nez p0, :cond_2

    .line 207
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 208
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 210
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 219
    :cond_1
    return-object v1

    .line 212
    :cond_2
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 213
    iget v3, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->d:I

    if-ne v3, p0, :cond_3

    .line 214
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 1025
    const-string v0, "SelectStickerFragment"

    const-string v1, "loadBmp() "

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1027
    sput-object p0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    .line 1032
    :cond_0
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    if-nez v0, :cond_1

    .line 1033
    new-instance v0, Landroid/util/LruCache;

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    .line 1034
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/calendar/event/kt;

    invoke-direct {v1}, Lcom/android/calendar/event/kt;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1053
    :cond_1
    return-void
.end method

.method private static b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1056
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1057
    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1058
    sget-boolean v1, Lcom/android/calendar/event/SelectStickerFragment;->y:Z

    if-eqz v1, :cond_0

    .line 1059
    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1061
    :cond_0
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 1062
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1063
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 1064
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 1066
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/event/SelectStickerFragment;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->l:Z

    return v0
.end method

.method static synthetic e(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic f()Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic f(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->x:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/event/SelectStickerFragment;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->i()V

    return-void
.end method

.method static synthetic h(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->A:Landroid/graphics/Rect;

    return-object v0
.end method

.method private h()Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 298
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 300
    const v1, 0x7f040092

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 301
    const v0, 0x7f12029f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->p:Landroid/widget/ScrollView;

    .line 302
    const v0, 0x7f12029d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->t:Landroid/widget/HorizontalScrollView;

    .line 303
    const v0, 0x7f12029e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    .line 304
    const v0, 0x7f1202a2

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    .line 305
    const v0, 0x7f1202a1

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    .line 306
    const v0, 0x7f1202a0

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->s:Landroid/widget/LinearLayout;

    .line 308
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 309
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 311
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    new-instance v2, Lcom/android/calendar/event/ku;

    invoke-direct {v2, p0, v7}, Lcom/android/calendar/event/ku;-><init>(Lcom/android/calendar/event/SelectStickerFragment;Lcom/android/calendar/event/kl;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->n:Landroid/view/GestureDetector;

    .line 312
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->p:Landroid/widget/ScrollView;

    new-instance v1, Lcom/android/calendar/event/kl;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/kl;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 320
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->o:[Ljava/lang/String;

    .line 327
    :goto_0
    new-instance v5, Lcom/android/calendar/event/km;

    invoke-direct {v5, p0}, Lcom/android/calendar/event/km;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    move v2, v3

    .line 348
    :goto_1
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    if-ge v2, v0, :cond_2

    .line 349
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04009f

    iget-object v6, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 350
    const v1, 0x7f1202b8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 352
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/android/calendar/hj;->k()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 353
    iget-object v6, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-static {v6}, Lcom/android/calendar/dz;->f(Landroid/content/Context;)I

    move-result v6

    if-le v6, v8, :cond_0

    if-ne v2, v8, :cond_0

    .line 354
    const v6, 0x3f4ccccd    # 0.8f

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 358
    :cond_0
    iget-object v6, p0, Lcom/android/calendar/event/SelectStickerFragment;->o:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 361
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 348
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 324
    :cond_1
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->o:[Ljava/lang/String;

    goto :goto_0

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/kn;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/kn;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 378
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 379
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 380
    iput-object v7, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    .line 382
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    .line 383
    :goto_2
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    if-ge v3, v0, :cond_4

    .line 384
    new-instance v0, Lcom/android/calendar/event/kx;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/event/kx;-><init>(Lcom/android/calendar/event/SelectStickerFragment;Landroid/content/Context;Landroid/widget/GridView;)V

    .line 385
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 388
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    new-instance v1, Lcom/android/calendar/event/ko;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ko;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 399
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->h:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 400
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    iget v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 401
    new-instance v0, Lcom/android/calendar/event/kx;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/calendar/event/kx;-><init>(Lcom/android/calendar/event/SelectStickerFragment;Landroid/content/Context;Landroid/widget/GridView;)V

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    .line 402
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerFragment;->c(I)V

    .line 403
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->c()V

    .line 405
    return-object v4
.end method

.method static synthetic i(Lcom/android/calendar/event/SelectStickerFragment;)I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    return v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 629
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 645
    :goto_0
    return-void

    :cond_0
    move v1, v2

    .line 633
    :goto_1
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    if-ge v1, v0, :cond_1

    .line 634
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->a(I)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/kx;->a(Ljava/util/ArrayList;)V

    .line 633
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 636
    :cond_1
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/kx;->a(Ljava/util/ArrayList;)V

    .line 638
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    invoke-virtual {v0}, Lcom/android/calendar/event/kx;->getCount()I

    move-result v0

    if-ge v0, v4, :cond_2

    .line 639
    iput v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    .line 640
    iput-boolean v4, p0, Lcom/android/calendar/event/SelectStickerFragment;->f:Z

    .line 642
    :cond_2
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 643
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    .line 644
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->j()V

    goto :goto_0
.end method

.method static synthetic j(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->p:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private j()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 700
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 701
    :goto_0
    if-ge v2, v3, :cond_2

    .line 702
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 703
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 705
    if-eqz v0, :cond_0

    .line 706
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v5, v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    invoke-virtual {v0}, Lcom/android/calendar/event/kx;->getCount()I

    move-result v0

    if-ge v0, v5, :cond_1

    const/16 v0, 0x8

    .line 709
    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 701
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 707
    goto :goto_1

    .line 713
    :cond_2
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 716
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->z:Z

    .line 717
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 726
    :cond_0
    :goto_0
    return-void

    .line 720
    :cond_1
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    .line 721
    iget v0, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->f:I

    if-lez v0, :cond_2

    .line 722
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->z:Z

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 17

    .prologue
    .line 150
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 151
    const-string v3, "sticker_name"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 152
    const-string v3, "filepath"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 153
    const-string v3, "sticker_group"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    .line 154
    const-string v3, "recently"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    .line 155
    const-string v3, "packageId"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 157
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 159
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/calendar/event/SelectStickerFragment;->z:Z

    .line 160
    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 161
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 162
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 163
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 164
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 165
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 166
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 167
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 168
    if-lez v10, :cond_0

    .line 169
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iput-boolean v8, v0, Lcom/android/calendar/event/SelectStickerFragment;->z:Z

    .line 172
    :cond_0
    invoke-static {v3}, Lcom/android/calendar/gx;->a(I)I

    move-result v8

    .line 174
    new-instance v3, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    invoke-direct/range {v3 .. v10}, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;-><init>(JLjava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    :cond_1
    return-object v16
.end method

.method protected b()V
    .locals 5

    .prologue
    .line 445
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    .line 446
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 447
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 448
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 450
    if-eqz v0, :cond_0

    .line 451
    iget v4, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v4, v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->t:Landroid/widget/HorizontalScrollView;

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/widget/HorizontalScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 453
    invoke-virtual {v3}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 454
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 446
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 459
    :cond_1
    return-void
.end method

.method protected b(I)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 504
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 505
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 508
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c02e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v1, v0

    .line 512
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c02e5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0c02e6

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v2, v0

    .line 516
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c02e7

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c02e4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v6, v0

    .line 520
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    .line 521
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    .line 526
    :goto_0
    iget v7, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v7, v0

    .line 527
    sub-int/2addr v0, v2

    div-int/2addr v0, v1

    .line 528
    const-string v1, "SelectStickerFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "numColumn:"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    if-gtz v0, :cond_3

    move v2, v3

    .line 535
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 536
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 537
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/event/kx;

    invoke-virtual {v1}, Lcom/android/calendar/event/kx;->getCount()I

    move-result v1

    .line 538
    div-int v7, v1, v2

    rem-int/2addr v1, v2

    if-nez v1, :cond_0

    const/4 v3, 0x0

    :cond_0
    add-int v1, v7, v3

    .line 539
    mul-int v3, v4, v1

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, v5

    add-int/2addr v1, v3

    add-int/2addr v1, v6

    .line 541
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 542
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 543
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 544
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 546
    :cond_1
    return-void

    .line 524
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/hj;->f(Landroid/app/Activity;)Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method protected c()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v4, 0x0

    .line 462
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    .line 464
    invoke-static {v4}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    .line 465
    const-string v0, "sec-roboto-light"

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    .line 466
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 467
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    move v3, v4

    .line 471
    :goto_0
    if-ge v3, v5, :cond_5

    .line 472
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 473
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 475
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 476
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    if-ge v0, v2, :cond_2

    .line 477
    const v0, 0x7f1202b8

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 478
    const v2, 0x7f1202b9

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 479
    iget v11, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v11, v1, :cond_1

    .line 480
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->t:Landroid/widget/HorizontalScrollView;

    const/4 v11, 0x0

    invoke-virtual {v1, v11, v10}, Landroid/widget/HorizontalScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 481
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 482
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 483
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 484
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 471
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 486
    :cond_1
    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v10, v1}, Landroid/view/View;->setAlpha(F)V

    .line 487
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 488
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 489
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 492
    :cond_2
    iget v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    move-object v0, v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v11, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    sub-int/2addr v0, v11

    if-eq v2, v0, :cond_3

    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    add-int/lit8 v0, v0, 0x1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    sub-int/2addr v1, v2

    if-ne v0, v1, :cond_4

    .line 494
    :cond_3
    invoke-virtual {v10, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 496
    :cond_4
    invoke-virtual {v10, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 501
    :cond_5
    return-void
.end method

.method protected c(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 549
    if-nez p1, :cond_1

    .line 550
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    new-instance v1, Lcom/android/calendar/event/kq;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/kq;-><init>(Lcom/android/calendar/event/SelectStickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 563
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->i:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 564
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->b:Lcom/android/calendar/event/kx;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 565
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setHoverScrollMode(Z)V

    .line 567
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 568
    if-eqz v0, :cond_0

    const-string v1, "caller"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "caller"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->r:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c02e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c02e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected d()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 611
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v3

    .line 612
    :goto_0
    if-ge v2, v4, :cond_0

    .line 613
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 614
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 615
    if-eqz v1, :cond_1

    move-object v0, v1

    .line 616
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v6, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    if-ge v0, v6, :cond_1

    .line 617
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 618
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->t:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 624
    :cond_0
    return-void

    .line 612
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 1070
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1071
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1072
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    .line 1074
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 134
    iput-object p1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    .line 136
    invoke-static {}, Lcom/android/calendar/dz;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->v:I

    .line 140
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 142
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v1, 0x140

    if-ge v0, v1, :cond_1

    .line 143
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/calendar/event/SelectStickerFragment;->y:Z

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->m:Z

    .line 147
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 746
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 747
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/android/calendar/event/SelectStickerFragment;->g:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 751
    :cond_0
    iget v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerFragment;->b(I)V

    .line 752
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 263
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/calendar/event/SelectStickerFragment;->setHasOptionsMenu(Z)V

    .line 264
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 282
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    :goto_0
    return-object v0

    .line 285
    :cond_0
    if-eqz p1, :cond_1

    const-string v1, "key_dialog_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    const-string v1, "key_dialog_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->l:Z

    .line 288
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 289
    const v2, 0x7f0f03ce

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 290
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->h()Landroid/view/View;

    move-result-object v2

    .line 291
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 292
    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 293
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    const/4 v0, 0x0

    .line 277
    :goto_0
    return-object v0

    .line 271
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    const-string v0, "key_dialog_state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->l:Z

    .line 274
    :cond_1
    iget-boolean v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->l:Z

    if-eqz v0, :cond_2

    .line 275
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 277
    :cond_2
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->h()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 227
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    .line 229
    invoke-virtual {v0, v2}, Lcom/android/calendar/event/kx;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->q:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/kx;

    .line 232
    invoke-virtual {v0, v2}, Lcom/android/calendar/event/kx;->a(Ljava/util/ArrayList;)V

    .line 233
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 234
    iput-object v2, p0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    .line 237
    :cond_1
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 238
    sget-object v2, Lcom/android/calendar/event/SelectStickerFragment;->x:Ljava/lang/Object;

    monitor-enter v2

    .line 239
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    :try_start_0
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 240
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;

    iget-wide v4, v0, Lcom/android/calendar/event/SelectStickerFragment$StickerItem;->a:J

    .line 241
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 242
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 239
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 245
    :cond_3
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 246
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->w:Landroid/util/LruCache;

    .line 247
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :cond_4
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->e()V

    .line 252
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 253
    return-void

    .line 247
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 257
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 258
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 730
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 731
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 741
    const v0, 0x7f120349

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->z:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 742
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 649
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 650
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 654
    :cond_0
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 655
    new-instance v0, Lcom/android/calendar/event/ks;

    iget-object v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/ks;-><init>(Lcom/android/calendar/event/SelectStickerFragment;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->k:Landroid/content/AsyncQueryHandler;

    .line 688
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->k:Landroid/content/AsyncQueryHandler;

    const/4 v1, 0x0

    sget-object v3, Lcom/android/calendar/gx;->a:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/gx;->b:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/content/AsyncQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :goto_0
    return-void

    .line 692
    :cond_1
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->k()V

    .line 693
    iget-object v0, p0, Lcom/android/calendar/event/SelectStickerFragment;->j:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 694
    invoke-direct {p0}, Lcom/android/calendar/event/SelectStickerFragment;->i()V

    .line 695
    invoke-virtual {p0}, Lcom/android/calendar/event/SelectStickerFragment;->b()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 735
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 736
    const-string v0, "key_dialog_state"

    iget-boolean v1, p0, Lcom/android/calendar/event/SelectStickerFragment;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 737
    return-void
.end method
