.class public Lcom/android/calendar/event/ak;
.super Landroid/app/Fragment;
.source "EditEventFragment.java"

# interfaces
.implements Lcom/android/calendar/ap;


# instance fields
.field private A:Landroid/content/Intent;

.field private B:Z

.field private C:Landroid/content/AsyncQueryHandler;

.field private D:Landroid/view/View;

.field private E:Landroid/content/BroadcastReceiver;

.field a:Lcom/android/calendar/as;

.field b:Lcom/android/calendar/event/ay;

.field public c:Z

.field private final d:Z

.field private final e:Z

.field private f:Z

.field private g:Z

.field private h:I

.field private i:Lcom/android/calendar/event/av;

.field private j:Lcom/android/calendar/as;

.field private k:Lcom/android/calendar/as;

.field private l:Lcom/android/calendar/as;

.field private m:Lcom/android/calendar/event/au;

.field private n:Landroid/app/AlertDialog;

.field private o:Landroid/app/AlertDialog;

.field private p:Landroid/app/AlertDialog;

.field private q:I

.field private r:Lcom/android/calendar/aq;

.field private s:Lcom/android/calendar/event/at;

.field private t:Landroid/net/Uri;

.field private u:J

.field private v:J

.field private w:Landroid/app/Activity;

.field private x:Lcom/android/calendar/event/as;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 532
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/android/calendar/event/ak;-><init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V

    .line 533
    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/aq;ZLandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 535
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 87
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->d:Z

    .line 88
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->e:Z

    .line 109
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->f:Z

    .line 111
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->g:Z

    .line 117
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/android/calendar/event/ak;->h:I

    .line 130
    iput v1, p0, Lcom/android/calendar/event/ak;->q:I

    .line 139
    new-instance v0, Lcom/android/calendar/event/as;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/as;-><init>(Lcom/android/calendar/event/ak;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    .line 141
    iput-boolean v2, p0, Lcom/android/calendar/event/ak;->y:Z

    .line 142
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->z:Z

    .line 143
    iput-boolean v1, p0, Lcom/android/calendar/event/ak;->c:Z

    .line 146
    iput-boolean v2, p0, Lcom/android/calendar/event/ak;->B:Z

    .line 1542
    new-instance v0, Lcom/android/calendar/event/ar;

    invoke-direct {v0, p0}, Lcom/android/calendar/event/ar;-><init>(Lcom/android/calendar/event/ak;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->E:Landroid/content/BroadcastReceiver;

    .line 536
    iput-object p1, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    .line 537
    iput-boolean p2, p0, Lcom/android/calendar/event/ak;->z:Z

    .line 538
    iput-object p3, p0, Lcom/android/calendar/event/ak;->A:Landroid/content/Intent;

    .line 539
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/ak;Lcom/android/calendar/as;)Lcom/android/calendar/as;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    return-object p1
.end method

.method static synthetic a(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/as;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 510
    monitor-enter p0

    .line 511
    :try_start_0
    iget v0, p0, Lcom/android/calendar/event/ak;->h:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/android/calendar/event/ak;->h:I

    .line 512
    iget v0, p0, Lcom/android/calendar/event/ak;->h:I

    if-nez v0, :cond_5

    .line 513
    iget-object v0, p0, Lcom/android/calendar/event/ak;->k:Lcom/android/calendar/as;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/android/calendar/event/ak;->k:Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    .line 516
    :cond_0
    iget-boolean v0, p0, Lcom/android/calendar/event/ak;->c:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/calendar/event/ak;->q:I

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->aq:Z

    if-eqz v0, :cond_4

    .line 518
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 519
    :cond_3
    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->a()V

    .line 525
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget v1, p0, Lcom/android/calendar/event/ak;->q:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(I)V

    .line 526
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/as;)V

    .line 528
    :cond_5
    monitor-exit p0

    .line 529
    return-void

    .line 521
    :cond_6
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/ak;->q:I

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ak;I)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/android/calendar/event/ak;->a(I)V

    return-void
.end method

.method private a(JLcom/android/calendar/as;Lcom/android/calendar/as;)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 1385
    .line 1386
    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/EditEventActivity;->c:Landroid/net/Uri;

    sget-object v2, Lcom/android/calendar/event/EditEventActivity;->e:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "event_id = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1389
    if-eqz v1, :cond_1

    .line 1390
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1391
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 1392
    array-length v2, v0

    invoke-static {v0, v6, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1394
    iput-object v0, p3, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    .line 1395
    iput-object v0, p4, Lcom/android/calendar/as;->ak:Landroid/graphics/Bitmap;

    .line 1397
    const/4 v0, 0x1

    .line 1400
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1402
    :goto_1
    return v0

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/ak;JLcom/android/calendar/as;Lcom/android/calendar/as;)Z
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/ak;->a(JLcom/android/calendar/as;Lcom/android/calendar/as;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/calendar/event/ak;Z)Z
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/android/calendar/event/ak;->y:Z

    return p1
.end method

.method static synthetic b(Lcom/android/calendar/event/ak;I)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/android/calendar/event/ak;->h:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/android/calendar/event/ak;->h:I

    return v0
.end method

.method static synthetic b(Lcom/android/calendar/event/ak;)Lcom/android/calendar/as;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/ak;I)I
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lcom/android/calendar/event/ak;->q:I

    return p1
.end method

.method static synthetic c(Lcom/android/calendar/event/ak;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/event/ak;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/android/calendar/event/ak;->u:J

    return-wide v0
.end method

.method static synthetic e(Lcom/android/calendar/event/ak;)J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/android/calendar/event/ak;->v:J

    return-wide v0
.end method

.method static synthetic f(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/au;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    return-object v0
.end method

.method static synthetic g(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/at;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    return-object v0
.end method

.method static synthetic h(Lcom/android/calendar/event/ak;)Lcom/android/calendar/aq;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    return-object v0
.end method

.method static synthetic i(Lcom/android/calendar/event/ak;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/ak;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/calendar/event/ak;->m()V

    return-void
.end method

.method static synthetic k(Lcom/android/calendar/event/ak;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/android/calendar/event/ak;->q:I

    return v0
.end method

.method private k()V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v3, 0x0

    const-wide/16 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 542
    iput-object v2, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    .line 543
    iput-wide v10, p0, Lcom/android/calendar/event/ak;->u:J

    .line 544
    iput-wide v10, p0, Lcom/android/calendar/event/ak;->v:J

    .line 545
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    if-eqz v0, :cond_a

    .line 546
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v4, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_7

    .line 547
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v4, v4, Lcom/android/calendar/aq;->c:J

    iput-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 548
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v4, v4, Lcom/android/calendar/aq;->c:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    .line 553
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    .line 559
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->a:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v6, v8

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    iput-boolean v0, v4, Lcom/android/calendar/as;->aq:Z

    .line 570
    :cond_2
    :goto_2
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_3

    iget-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_3

    .line 572
    iget-object v0, p0, Lcom/android/calendar/event/ak;->i:Lcom/android/calendar/event/av;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/android/calendar/event/av;->a(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    .line 573
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    .line 576
    :cond_3
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_5

    iget-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_4

    iget-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_5

    .line 578
    :cond_4
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    .line 580
    :cond_5
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    iget-wide v6, p0, Lcom/android/calendar/event/ak;->u:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_6

    .line 582
    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    invoke-static {v4, v5}, Lcom/android/calendar/event/av;->b(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    .line 586
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    if-nez v0, :cond_d

    move v0, v1

    .line 587
    :goto_3
    if-nez v0, :cond_e

    .line 588
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput v3, v0, Lcom/android/calendar/as;->ah:I

    .line 589
    const/16 v0, 0x1f

    iput v0, p0, Lcom/android/calendar/event/ak;->h:I

    .line 594
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    iget-object v3, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    invoke-static {}, Lcom/android/calendar/event/av;->a()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    :goto_4
    return-void

    .line 551
    :cond_7
    iget-object v4, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v6, v0, Lcom/android/calendar/aq;->p:J

    const-wide/16 v8, 0x10

    cmp-long v0, v6, v8

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    iput-boolean v0, v4, Lcom/android/calendar/as;->G:Z

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_5

    :cond_9
    move v0, v3

    .line 559
    goto/16 :goto_1

    .line 560
    :cond_a
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    if-eqz v0, :cond_2

    .line 561
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v4, v0, Lcom/android/calendar/event/at;->a:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_b

    .line 562
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v4, v4, Lcom/android/calendar/event/at;->a:J

    iput-wide v4, v0, Lcom/android/calendar/as;->b:J

    .line 563
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v4, v4, Lcom/android/calendar/event/at;->a:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->t:Landroid/net/Uri;

    .line 565
    :cond_b
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v4, v0, Lcom/android/calendar/event/at;->b:J

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    .line 566
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v4, v0, Lcom/android/calendar/event/at;->c:J

    iput-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    .line 567
    iget-object v4, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-wide v6, v0, Lcom/android/calendar/event/at;->d:J

    const-wide/32 v8, 0x200000

    cmp-long v0, v6, v8

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    iput-boolean v0, v4, Lcom/android/calendar/as;->aq:Z

    goto/16 :goto_2

    :cond_c
    move v0, v3

    goto :goto_6

    :cond_d
    move v0, v3

    .line 586
    goto :goto_3

    .line 602
    :cond_e
    iput v12, p0, Lcom/android/calendar/event/ak;->h:I

    .line 606
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, p0, Lcom/android/calendar/event/ak;->u:J

    iput-wide v4, v0, Lcom/android/calendar/as;->y:J

    .line 607
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, p0, Lcom/android/calendar/event/ak;->v:J

    iput-wide v4, v0, Lcom/android/calendar/as;->B:J

    .line 608
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iput v1, v0, Lcom/android/calendar/as;->X:I

    .line 611
    invoke-static {}, Lcom/android/calendar/dz;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 612
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND account_name!=\'docomo\' AND deleted!=1"

    move v1, v12

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :goto_7
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/calendar/event/ak;->q:I

    goto/16 :goto_4

    .line 617
    :cond_f
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->C(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 618
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND calendar_displayName!=\'Direct Share\' AND deleted!=1"

    move v1, v12

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 625
    :cond_10
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/dz;->a(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 626
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1"

    move v1, v12

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 632
    :cond_11
    iget-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/calendar/event/av;->f:[Ljava/lang/String;

    const-string v5, "calendar_access_level>=500 AND deleted!=1 AND account_type!=\'com.osp.app.signin\'"

    move v1, v12

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/au;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method static synthetic l(Lcom/android/calendar/event/ak;)Landroid/content/AsyncQueryHandler;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->C:Landroid/content/AsyncQueryHandler;

    return-object v0
.end method

.method private l()V
    .locals 12

    .prologue
    .line 771
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x3

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 772
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v0, Lcom/android/calendar/as;->b:J

    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v4, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    iget-object v5, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/android/calendar/event/av;->a(Ljava/util/ArrayList;JLjava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    .line 776
    if-nez v0, :cond_0

    .line 793
    :goto_0
    return-void

    .line 780
    :cond_0
    new-instance v3, Lcom/android/calendar/ag;

    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/android/calendar/ag;-><init>(Landroid/content/Context;)V

    .line 781
    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v6

    const-wide/16 v8, 0x0

    move-object v7, v1

    invoke-virtual/range {v3 .. v9}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Ljava/lang/String;Ljava/util/ArrayList;J)V

    .line 783
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v1, Lcom/android/calendar/as;->b:J

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 784
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 785
    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 786
    :goto_1
    iget-object v1, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->H:Z

    if-eq v0, v1, :cond_1

    .line 787
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 788
    const-string v1, "hasAlarm"

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 789
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/ag;->a(ILjava/lang/Object;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;J)V

    .line 792
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f0f039b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 785
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 788
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic m(Lcom/android/calendar/event/ak;)Lcom/android/calendar/event/av;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/calendar/event/ak;->i:Lcom/android/calendar/event/av;

    return-object v0
.end method

.method private m()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1422
    new-instance v0, Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/android/calendar/as;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    .line 1424
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1425
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 1430
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1431
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    .line 1436
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1437
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 1442
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v0, v0, Lcom/android/calendar/aq;->c:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_3

    .line 1447
    const-string v0, "UTC"

    .line 1448
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v2, v2, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1449
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v3, v3, Lcom/android/calendar/as;->E:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1451
    iget-object v3, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v3, Lcom/android/calendar/as;->y:J

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1452
    iput v6, v1, Landroid/text/format/Time;->hour:I

    .line 1453
    iput v6, v1, Landroid/text/format/Time;->minute:I

    .line 1454
    iput v6, v1, Landroid/text/format/Time;->second:I

    .line 1455
    iput-object v0, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1456
    iget-object v3, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    invoke-virtual {v1, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/android/calendar/as;->y:J

    .line 1458
    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v4, v1, Lcom/android/calendar/as;->B:J

    invoke-virtual {v2, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1459
    iput v6, v2, Landroid/text/format/Time;->hour:I

    .line 1460
    iput v6, v2, Landroid/text/format/Time;->minute:I

    .line 1461
    iput v6, v2, Landroid/text/format/Time;->second:I

    .line 1462
    iput-object v0, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 1465
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    invoke-virtual {v2, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 1467
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iput-boolean v7, v0, Lcom/android/calendar/as;->G:Z

    .line 1475
    :goto_3
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    iput-wide v2, v0, Lcom/android/calendar/as;->b:J

    .line 1477
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->ah:I

    iput v1, v0, Lcom/android/calendar/as;->ah:I

    .line 1479
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/android/calendar/as;->av:Ljava/util/ArrayList;

    .line 1480
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget v1, v1, Lcom/android/calendar/as;->X:I

    iput v1, v0, Lcom/android/calendar/as;->X:I

    .line 1482
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_4

    .line 1483
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const-string v1, "preference_defaultCalendar"

    const-string v2, "My calendar"

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1486
    iget-object v1, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iput-object v0, v1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    .line 1487
    iget-object v1, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iput-object v0, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 1488
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    const-string v1, ""

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 1493
    :goto_4
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->l:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->l:Ljava/lang/String;

    .line 1495
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->r:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->r:Ljava/lang/String;

    .line 1496
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->s:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->s:Ljava/lang/String;

    .line 1497
    return-void

    .line 1427
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 1433
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->q:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->q:Ljava/lang/String;

    goto/16 :goto_1

    .line 1439
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    goto/16 :goto_2

    .line 1470
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->G:Z

    iput-boolean v1, v0, Lcom/android/calendar/as;->G:Z

    .line 1471
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->y:J

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 1472
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->B:J

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    goto/16 :goto_3

    .line 1490
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->n:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->n:Ljava/lang/String;

    .line 1491
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, v1, Lcom/android/calendar/as;->t:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->t:Ljava/lang/String;

    goto :goto_4
.end method

.method private n()V
    .locals 3

    .prologue
    .line 1599
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1600
    const-string v1, "com.android.provider.calendar.ACCOUNT_DELETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1601
    iget-object v1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/calendar/event/ak;->E:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1602
    return-void
.end method

.method static synthetic n(Lcom/android/calendar/event/ak;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/android/calendar/event/ak;->g:Z

    return v0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1605
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->E:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1606
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 796
    iget v0, p0, Lcom/android/calendar/event/ak;->q:I

    if-nez v0, :cond_2

    .line 797
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 798
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-boolean v5, v0, Lcom/android/calendar/as;->w:Z

    .line 799
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->az:Ljava/lang/String;

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 804
    if-eqz v4, :cond_4

    .line 807
    if-nez v5, :cond_0

    if-eqz v6, :cond_3

    .line 810
    :cond_0
    new-array v0, v1, [Ljava/lang/CharSequence;

    move-object v2, v0

    move v1, v3

    .line 823
    :goto_0
    if-nez v5, :cond_7

    if-nez v6, :cond_7

    .line 824
    add-int/lit8 v0, v1, 0x1

    iget-object v7, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v8, 0x7f0f0435

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v2, v1

    .line 826
    :goto_1
    add-int/lit8 v1, v0, 0x1

    iget-object v1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v7, 0x7f0f0048

    invoke-virtual {v1, v7}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v2, v0

    .line 829
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 830
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 831
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    .line 833
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0190

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/calendar/event/an;

    invoke-direct {v1, p0, v4, v5, v6}, Lcom/android/calendar/event/an;-><init>(Lcom/android/calendar/event/ak;ZZZ)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    .line 867
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    new-instance v1, Lcom/android/calendar/event/ao;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/ao;-><init>(Lcom/android/calendar/event/ak;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 882
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 884
    :cond_2
    return-void

    .line 812
    :cond_3
    new-array v0, v7, [Ljava/lang/CharSequence;

    move-object v2, v0

    move v1, v3

    goto :goto_0

    .line 815
    :cond_4
    if-nez v5, :cond_5

    if-eqz v6, :cond_6

    .line 816
    :cond_5
    new-array v0, v7, [Ljava/lang/CharSequence;

    .line 820
    :goto_2
    iget-object v2, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v7, 0x7f0f02e6

    invoke-virtual {v2, v7}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v3

    move-object v2, v0

    goto :goto_0

    .line 818
    :cond_6
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/CharSequence;

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public a(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    .line 1530
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/event/ay;->a(JLjava/lang/String;)V

    .line 1532
    :cond_0
    return-void
.end method

.method public a(Lcom/android/calendar/aq;)V
    .locals 4

    .prologue
    .line 1313
    iget-wide v0, p1, Lcom/android/calendar/aq;->a:J

    const-wide/16 v2, 0x20

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/event/ak;->y:Z

    if-eqz v0, :cond_0

    .line 1314
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1315
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/as;->a(I)V

    .line 1316
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    .line 1319
    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    const v0, 0x7f0f03d4

    const v4, 0x104000a

    .line 888
    .line 892
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->q()Ljava/lang/String;

    move-result-object v1

    .line 894
    if-eqz v1, :cond_0

    .line 895
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x5f0

    if-ge v2, v3, :cond_1

    .line 906
    :cond_0
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 907
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v3, Lcom/android/calendar/event/aq;

    invoke-direct {v3, p0, p1, v1}, Lcom/android/calendar/event/aq;-><init>(Lcom/android/calendar/event/ak;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f00a4

    new-instance v3, Lcom/android/calendar/event/ap;

    invoke-direct {v3, p0}, Lcom/android/calendar/event/ap;-><init>(Lcom/android/calendar/event/ak;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 960
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->o:Landroid/app/AlertDialog;

    .line 962
    return-void

    .line 898
    :cond_1
    const v0, 0x7f0f03d2

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/ay;->c(Z)V

    .line 1503
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1535
    const/4 v0, 0x0

    .line 1536
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, p1}, Lcom/android/calendar/event/ay;->h(Ljava/lang/String;)Z

    move-result v0

    .line 1539
    :cond_0
    return v0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 965
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-nez v1, :cond_1

    .line 977
    :cond_0
    :goto_0
    return v0

    .line 969
    :cond_1
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->r()Ljava/util/ArrayList;

    move-result-object v1

    .line 971
    const-string v2, "EditEventFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkMSGAttendees smsAttendeeList.size() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 973
    invoke-virtual {p0, v1}, Lcom/android/calendar/event/ak;->a(Ljava/util/ArrayList;)V

    .line 974
    const/4 v0, 0x1

    goto :goto_0
.end method

.method c()Z
    .locals 10

    .prologue
    const-wide/32 v8, 0x5265c00

    const-wide/16 v6, 0x0

    .line 1159
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->y:J

    iget-object v2, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->y:J

    sub-long/2addr v0, v2

    .line 1160
    iget-object v2, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->B:J

    iget-object v4, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    iget-wide v4, v4, Lcom/android/calendar/as;->B:J

    sub-long/2addr v2, v4

    .line 1162
    cmp-long v4, v0, v6

    if-ltz v4, :cond_0

    rem-long/2addr v0, v8

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    :cond_0
    cmp-long v0, v2, v6

    if-ltz v0, :cond_1

    rem-long v0, v2, v8

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x1

    .line 1330
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    if-nez v0, :cond_0

    .line 1355
    :goto_0
    return-void

    .line 1334
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->a(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->d(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1335
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1336
    iget v0, p0, Lcom/android/calendar/event/ak;->q:I

    if-nez v0, :cond_2

    .line 1337
    iput v1, p0, Lcom/android/calendar/event/ak;->q:I

    .line 1339
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/as;->a(I)V

    .line 1340
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    goto :goto_0

    .line 1342
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/as;->a(I)V

    .line 1343
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    goto :goto_0

    .line 1345
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/event/av;->c(Lcom/android/calendar/as;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-wide v0, v0, Lcom/android/calendar/as;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1348
    invoke-direct {p0}, Lcom/android/calendar/event/ak;->l()V

    .line 1349
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/as;->a(I)V

    .line 1350
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    goto :goto_0

    .line 1352
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0, v4}, Lcom/android/calendar/event/as;->a(I)V

    .line 1353
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/as;->a(I)V

    .line 1359
    iget-object v0, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-virtual {v0}, Lcom/android/calendar/event/as;->run()V

    .line 1360
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 1406
    iget-object v0, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    if-nez v0, :cond_1

    .line 1419
    :cond_0
    :goto_0
    return-void

    .line 1410
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    .line 1411
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    .line 1413
    iget-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->l:Lcom/android/calendar/as;

    invoke-virtual {v0, v1}, Lcom/android/calendar/as;->c(Lcom/android/calendar/as;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1414
    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->d()V

    goto :goto_0

    .line 1416
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f0f01d8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 1305
    const-wide/16 v0, 0x200

    return-wide v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    .line 1507
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->u()V

    .line 1509
    :cond_0
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 1512
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 1513
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/ak;->D:Landroid/view/View;

    .line 1515
    :cond_0
    return-void
.end method

.method public j()V
    .locals 3

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/android/calendar/event/ak;->D:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/android/calendar/event/ak;->D:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1520
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1522
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_0

    .line 1523
    iget-object v1, p0, Lcom/android/calendar/event/ak;->D:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1526
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 663
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 664
    iput-object p1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    .line 666
    new-instance v0, Lcom/android/calendar/event/av;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/event/av;-><init>(Landroid/content/Context;Lcom/android/calendar/as;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->i:Lcom/android/calendar/event/av;

    .line 667
    new-instance v0, Lcom/android/calendar/event/au;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/au;-><init>(Lcom/android/calendar/event/ak;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->m:Lcom/android/calendar/event/au;

    .line 668
    new-instance v0, Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->A:Landroid/content/Intent;

    invoke-direct {v0, p1, v1}, Lcom/android/calendar/as;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    .line 669
    invoke-direct {p0}, Lcom/android/calendar/event/ak;->k()V

    .line 670
    new-instance v0, Lcom/android/calendar/event/al;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/al;-><init>(Lcom/android/calendar/event/ak;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->C:Landroid/content/AsyncQueryHandler;

    .line 672
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 704
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 706
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v1, v1, Lcom/android/calendar/event/ay;->I:Landroid/widget/AutoCompleteTextView;

    const v2, 0x7f0c009c

    const v3, 0x7f0c009d

    invoke-static {v0, v1, v2, v3}, Lcom/android/calendar/event/ay;->a(Landroid/content/Context;Landroid/widget/AutoCompleteTextView;II)V

    .line 710
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v0

    .line 712
    iget-boolean v1, p0, Lcom/android/calendar/event/ak;->f:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getCalendarViewShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 713
    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v1

    .line 714
    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    .line 715
    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v0

    .line 717
    iget-object v3, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v3, v3, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v3, v0, v2, v1}, Lcom/android/calendar/common/extension/a;->updateDate(III)V

    .line 721
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->aj:Lcom/android/calendar/common/extension/c;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/c;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 722
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 723
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 724
    new-instance v1, Lcom/android/calendar/event/am;

    invoke-direct {v1, p0, v0}, Lcom/android/calendar/event/am;-><init>(Lcom/android/calendar/event/ak;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 733
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->W:Lcom/android/calendar/event/AttendeesView;

    invoke-virtual {v0}, Lcom/android/calendar/event/AttendeesView;->b()V

    .line 734
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->b()V

    .line 736
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 740
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 741
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f0a000a

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->c(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ak;->f:Z

    .line 742
    if-eqz p1, :cond_6

    .line 743
    monitor-enter p0

    .line 744
    :try_start_0
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 745
    const-string v0, "key_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/event/ak;->k:Lcom/android/calendar/as;

    .line 748
    :cond_0
    const-string v0, "key_edit_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const-string v0, "key_edit_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/event/ak;->q:I

    .line 751
    :cond_1
    const-string v0, "key_edit_on_launch"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 752
    const-string v0, "key_edit_on_launch"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ak;->c:Z

    .line 755
    :cond_2
    const-string v0, "key_event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 756
    const-string v0, "key_event"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/at;

    iput-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    .line 759
    :cond_3
    const-string v0, "key_read_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 760
    const-string v0, "key_read_only"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/calendar/event/ak;->z:Z

    .line 762
    :cond_4
    const-string v0, "key_original_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 763
    const-string v0, "key_original_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/as;

    iput-object v0, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    .line 766
    :cond_5
    monitor-exit p0

    .line 768
    :cond_6
    return-void

    .line 766
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 676
    const v0, 0x7f04003c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 677
    new-instance v1, Lcom/android/calendar/event/ay;

    iget-object v2, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    iget-object v3, p0, Lcom/android/calendar/event/ak;->x:Lcom/android/calendar/event/as;

    invoke-direct {v1, v2, v0, v3}, Lcom/android/calendar/event/ay;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/event/ax;)V

    iput-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    .line 678
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget v2, p0, Lcom/android/calendar/event/ak;->q:I

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ay;->a(I)V

    .line 679
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->d()V

    .line 681
    if-eqz p3, :cond_0

    .line 682
    const-string v1, "key_press_done_new_dialog"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 683
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    const-string v2, "key_press_done_new_dialog"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/calendar/event/ay;->a(Z)V

    .line 688
    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/ak;->n()V

    .line 693
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1238
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/ay;->a(Lcom/android/calendar/as;)V

    .line 1240
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->n()V

    .line 1241
    iput-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    .line 1243
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 1244
    iget-object v0, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1245
    iput-object v1, p0, Lcom/android/calendar/event/ak;->n:Landroid/app/AlertDialog;

    .line 1248
    :cond_1
    invoke-static {}, Lcom/android/calendar/dz;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1249
    iget-object v0, p0, Lcom/android/calendar/event/ak;->o:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 1250
    iget-object v0, p0, Lcom/android/calendar/event/ak;->o:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1251
    iput-object v1, p0, Lcom/android/calendar/event/ak;->o:Landroid/app/AlertDialog;

    .line 1255
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->p:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 1256
    iget-object v0, p0, Lcom/android/calendar/event/ak;->p:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1257
    iput-object v1, p0, Lcom/android/calendar/event/ak;->p:Landroid/app/AlertDialog;

    .line 1260
    :cond_3
    invoke-static {}, Lcom/android/calendar/event/hm;->a()V

    .line 1261
    invoke-direct {p0}, Lcom/android/calendar/event/ak;->o()V

    .line 1262
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 1263
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-static {v0}, Lcom/android/calendar/al;->b(Landroid/content/Context;)V

    .line 699
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 700
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3

    .prologue
    .line 647
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 649
    if-nez p1, :cond_0

    .line 650
    const-string v0, "EditEventFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hidden: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "EditTaskFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/event/fq;

    .line 653
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v1, :cond_0

    .line 654
    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v2, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v2, v2, Lcom/android/calendar/hh;->h:Ljava/lang/String;

    iget-object v0, v0, Lcom/android/calendar/event/fq;->a:Lcom/android/calendar/hh;

    iget-object v0, v0, Lcom/android/calendar/hh;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 8

    .prologue
    const v7, 0x7f12013f

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1169
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    if-eqz v0, :cond_5

    .line 1172
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->a()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1173
    if-eqz v0, :cond_0

    .line 1174
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1178
    :cond_0
    iput-boolean v6, p0, Lcom/android/calendar/event/ak;->g:Z

    .line 1179
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-boolean v0, v0, Lcom/android/calendar/event/ay;->as:Z

    if-eqz v0, :cond_1

    .line 1180
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iput-boolean v3, v0, Lcom/android/calendar/event/ay;->as:Z

    .line 1183
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 1184
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    move v2, v3

    .line 1185
    :goto_0
    if-ge v2, v4, :cond_4

    .line 1186
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->R:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1187
    const v1, 0x7f12014b

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 1189
    const v5, 0x7f12014c

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1191
    if-eqz v1, :cond_2

    .line 1192
    invoke-virtual {v1}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1194
    :cond_2
    if-eqz v0, :cond_3

    .line 1195
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1185
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1200
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iput-boolean v6, v0, Lcom/android/calendar/event/ay;->aC:Z

    .line 1202
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1203
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0, v3}, Lcom/android/calendar/event/ay;->c(Z)V

    .line 1207
    :cond_5
    iget-boolean v0, p0, Lcom/android/calendar/event/ak;->f:Z

    if-eqz v0, :cond_a

    .line 1208
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f12012a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1209
    if-eqz v0, :cond_6

    .line 1210
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1213
    :cond_6
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f120124

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1214
    if-eqz v0, :cond_7

    .line 1215
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1218
    :cond_7
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    const v1, 0x7f12013d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1219
    if-eqz v0, :cond_8

    .line 1220
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1223
    :cond_8
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1224
    if-eqz v0, :cond_9

    .line 1225
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1228
    :cond_9
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    invoke-virtual {v0, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 1229
    if-eqz v0, :cond_a

    .line 1230
    invoke-virtual {v0}, Landroid/widget/Spinner;->twDismissSpinnerPopup()V

    .line 1233
    :cond_a
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 1234
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1364
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 1365
    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/calendar/dz;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1366
    invoke-virtual {p0}, Lcom/android/calendar/event/ak;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1369
    :cond_0
    iput-boolean v2, p0, Lcom/android/calendar/event/ak;->g:Z

    .line 1370
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->m()V

    .line 1372
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iput-boolean v2, v0, Lcom/android/calendar/event/ay;->as:Z

    .line 1373
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->am:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 1375
    iget-object v0, p0, Lcom/android/calendar/event/ak;->w:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->f()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1376
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-boolean v1, p0, Lcom/android/calendar/event/ak;->B:Z

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/ay;->a(ZI)V

    .line 1378
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->s()V

    .line 1380
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->b()V

    .line 1381
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1271
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1272
    iget-boolean v0, p0, Lcom/android/calendar/event/ak;->f:Z

    if-eqz v0, :cond_1

    .line 1273
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    if-eqz v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ai:Lcom/android/calendar/common/extension/a;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/a;->onSaveInstanceState()Landroid/os/Bundle;

    .line 1276
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ak:Lcom/android/calendar/common/extension/k;

    if-eqz v0, :cond_1

    .line 1277
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    iget-object v0, v0, Lcom/android/calendar/event/ay;->ak:Lcom/android/calendar/common/extension/k;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/k;->onSaveInstanceState()Landroid/os/Bundle;

    .line 1280
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v0}, Lcom/android/calendar/event/ay;->c()Z

    .line 1281
    const-string v0, "key_model"

    iget-object v1, p0, Lcom/android/calendar/event/ak;->a:Lcom/android/calendar/as;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1282
    const-string v0, "key_edit_state"

    iget v1, p0, Lcom/android/calendar/event/ak;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1283
    iget-object v0, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    if-eqz v0, :cond_2

    .line 1284
    const-string v0, "key_original_model"

    iget-object v1, p0, Lcom/android/calendar/event/ak;->j:Lcom/android/calendar/as;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1286
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    if-eqz v0, :cond_5

    .line 1287
    new-instance v0, Lcom/android/calendar/event/at;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/calendar/event/at;-><init>(Lcom/android/calendar/event/al;)V

    iput-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    .line 1288
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v2, v1, Lcom/android/calendar/aq;->c:J

    iput-wide v2, v0, Lcom/android/calendar/event/at;->a:J

    .line 1289
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    if-eqz v0, :cond_3

    .line 1290
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v1, v1, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/event/at;->b:J

    .line 1292
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v0, v0, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    if-eqz v0, :cond_4

    .line 1293
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-object v1, v1, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/event/at;->c:J

    .line 1295
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    iget-object v1, p0, Lcom/android/calendar/event/ak;->r:Lcom/android/calendar/aq;

    iget-wide v2, v1, Lcom/android/calendar/aq;->a:J

    iput-wide v2, v0, Lcom/android/calendar/event/at;->d:J

    .line 1297
    :cond_5
    const-string v0, "key_edit_on_launch"

    iget-boolean v1, p0, Lcom/android/calendar/event/ak;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1298
    const-string v0, "key_event"

    iget-object v1, p0, Lcom/android/calendar/event/ak;->s:Lcom/android/calendar/event/at;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1299
    const-string v0, "key_read_only"

    iget-boolean v1, p0, Lcom/android/calendar/event/ak;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1300
    const-string v0, "key_press_done_new_dialog"

    iget-object v1, p0, Lcom/android/calendar/event/ak;->b:Lcom/android/calendar/event/ay;

    invoke-virtual {v1}, Lcom/android/calendar/event/ay;->e()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1301
    return-void
.end method
