.class Lcom/android/calendar/event/ks;
.super Landroid/content/AsyncQueryHandler;
.source "SelectStickerFragment.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/SelectStickerFragment;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectStickerFragment;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 658
    if-nez p3, :cond_1

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 662
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 663
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 667
    :cond_2
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 668
    sget-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 669
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    .line 671
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0, p3}, Lcom/android/calendar/event/SelectStickerFragment;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    .line 672
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 673
    invoke-static {}, Lcom/android/calendar/event/SelectStickerFragment;->f()Landroid/util/LruCache;

    move-result-object v0

    sget-object v1, Lcom/android/calendar/event/SelectStickerFragment;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->trimToSize(I)V

    .line 675
    :cond_4
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 676
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->g(Lcom/android/calendar/event/SelectStickerFragment;)V

    .line 677
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-boolean v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->f:Z

    if-eqz v0, :cond_5

    .line 678
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 679
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v2, v2, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 680
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v1, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget v1, v1, Lcom/android/calendar/event/SelectStickerFragment;->c:I

    invoke-virtual {v0, v1}, Lcom/android/calendar/event/SelectStickerFragment;->c(I)V

    .line 681
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->c()V

    .line 682
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-virtual {v0}, Lcom/android/calendar/event/SelectStickerFragment;->b()V

    .line 685
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/ks;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v0}, Lcom/android/calendar/event/SelectStickerFragment;->e(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_0
.end method
