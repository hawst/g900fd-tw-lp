.class Lcom/android/calendar/event/gc;
.super Ljava/lang/Object;
.source "EditTaskView.java"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# instance fields
.field final synthetic a:Landroid/animation/ObjectAnimator;

.field final synthetic b:Lcom/android/calendar/event/fx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/fx;Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 1176
    iput-object p1, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    iput-object p2, p0, Lcom/android/calendar/event/gc;->a:Landroid/animation/ObjectAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1195
    :cond_0
    return-void
.end method

.method public startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1179
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->k(Lcom/android/calendar/event/fx;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->i(Lcom/android/calendar/event/fx;)Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStagger(IJ)V

    .line 1181
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->i(Lcom/android/calendar/event/fx;)Landroid/animation/LayoutTransition;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/gc;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v4, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 1182
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->i(Lcom/android/calendar/event/fx;)Landroid/animation/LayoutTransition;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 1184
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v1}, Lcom/android/calendar/event/fx;->i(Lcom/android/calendar/event/fx;)Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1186
    iget-object v0, p0, Lcom/android/calendar/event/gc;->b:Lcom/android/calendar/event/fx;

    invoke-static {v0}, Lcom/android/calendar/event/fx;->j(Lcom/android/calendar/event/fx;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1188
    :cond_0
    return-void
.end method
