.class Lcom/android/calendar/event/jo;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/l;


# instance fields
.field a:Landroid/content/Intent;

.field b:Landroid/app/Activity;

.field c:F

.field final synthetic d:Lcom/android/calendar/event/SelectMapActivity;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/SelectMapActivity;Landroid/app/Activity;Landroid/content/Intent;F)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 507
    iput-object p1, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 503
    iput-object v0, p0, Lcom/android/calendar/event/jo;->a:Landroid/content/Intent;

    .line 504
    iput-object v0, p0, Lcom/android/calendar/event/jo;->b:Landroid/app/Activity;

    .line 505
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/android/calendar/event/jo;->c:F

    .line 508
    iput-object p2, p0, Lcom/android/calendar/event/jo;->b:Landroid/app/Activity;

    .line 509
    iput-object p3, p0, Lcom/android/calendar/event/jo;->a:Landroid/content/Intent;

    .line 510
    iput p4, p0, Lcom/android/calendar/event/jo;->c:F

    .line 511
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 515
    if-eqz p1, :cond_1

    .line 516
    const-string v0, "SelectMapActivity"

    const-string v2, "onSnapshotReady"

    invoke-static {v0, v2}, Lcom/android/calendar/ey;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 522
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 524
    iget-object v3, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapActivity;->i(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v3

    if-le v2, v3, :cond_4

    .line 525
    iget-object v3, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v3}, Lcom/android/calendar/event/SelectMapActivity;->i(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v2, 0x2

    .line 526
    iget-object v2, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v2}, Lcom/android/calendar/event/SelectMapActivity;->i(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v2

    .line 529
    :goto_0
    iget-object v4, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v4}, Lcom/android/calendar/event/SelectMapActivity;->j(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v4

    if-le v0, v4, :cond_0

    .line 530
    iget-object v1, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->j(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x2

    .line 531
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->j(Lcom/android/calendar/event/SelectMapActivity;)I

    move-result v0

    .line 533
    :cond_0
    iget-object v4, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {p1, v3, v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 535
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->k(Lcom/android/calendar/event/SelectMapActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/jo;->a:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 536
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 537
    iget-object v1, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v1}, Lcom/android/calendar/event/SelectMapActivity;->k(Lcom/android/calendar/event/SelectMapActivity;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x32

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 538
    iget-object v1, p0, Lcom/android/calendar/event/jo;->a:Landroid/content/Intent;

    const-string v2, "map"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/android/calendar/event/jo;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/android/calendar/event/SelectMapActivity;->setResult(ILandroid/content/Intent;)V

    .line 546
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    const-string v1, "preferences_maps_zoom_level"

    iget v2, p0, Lcom/android/calendar/event/jo;->c:F

    float-to-int v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 549
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 550
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;)Lcom/google/android/gms/maps/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->c()V

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->k(Lcom/android/calendar/event/SelectMapActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 553
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    invoke-static {v0}, Lcom/android/calendar/event/SelectMapActivity;->k(Lcom/android/calendar/event/SelectMapActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 554
    iget-object v0, p0, Lcom/android/calendar/event/jo;->d:Lcom/android/calendar/event/SelectMapActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/event/SelectMapActivity;->a(Lcom/android/calendar/event/SelectMapActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/jo;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 557
    return-void

    .line 540
    :catch_0
    move-exception v0

    .line 541
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_4
    move v3, v1

    goto/16 :goto_0
.end method
