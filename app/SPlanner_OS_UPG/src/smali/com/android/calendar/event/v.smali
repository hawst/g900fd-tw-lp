.class public Lcom/android/calendar/event/v;
.super Ljava/lang/Object;
.source "EasyEditEventView.java"

# interfaces
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# instance fields
.field public a:Landroid/widget/EditText;

.field public b:Lcom/android/calendar/common/extension/a;

.field private c:Landroid/app/Activity;

.field private d:Lcom/android/calendar/event/ab;

.field private e:Z

.field private f:Lcom/android/calendar/as;

.field private g:Lcom/android/calendar/event/av;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/text/format/Time;

.field private o:Landroid/text/format/Time;

.field private p:Ljava/lang/String;

.field private q:Lcom/android/calendar/common/extension/EasyTimePicker;

.field private r:Lcom/android/calendar/common/extension/EasyTimePicker;

.field private s:Landroid/widget/CheckBox;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/EditText;

.field private v:Landroid/text/format/Time;

.field private w:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/android/calendar/aq;Lcom/android/calendar/as;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-boolean v6, p0, Lcom/android/calendar/event/v;->e:Z

    .line 103
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/v;->v:Landroid/text/format/Time;

    .line 104
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/v;->w:Landroid/text/format/Time;

    .line 107
    iput-object p1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    .line 109
    iput-object p4, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    .line 110
    new-instance v0, Lcom/android/calendar/event/ab;

    iget-object v1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/event/ab;-><init>(Lcom/android/calendar/event/v;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/android/calendar/event/v;->d:Lcom/android/calendar/event/ab;

    .line 111
    new-instance v0, Lcom/android/calendar/event/av;

    invoke-direct {v0, p1, v3}, Lcom/android/calendar/event/av;-><init>(Landroid/content/Context;Lcom/android/calendar/as;)V

    iput-object v0, p0, Lcom/android/calendar/event/v;->g:Lcom/android/calendar/event/av;

    .line 113
    if-eqz p3, :cond_0

    .line 114
    iget-object v0, p3, Lcom/android/calendar/aq;->e:Landroid/text/format/Time;

    iget-object v1, p3, Lcom/android/calendar/aq;->f:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/text/format/Time;Landroid/text/format/Time;)V

    .line 119
    :goto_0
    const v0, 0x7f1200ca

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/v;->h:Landroid/widget/Button;

    .line 120
    const v0, 0x7f1200cb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lcom/android/calendar/event/v;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-direct {p0, v1}, Lcom/android/calendar/event/v;->a(Landroid/text/format/Time;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v1}, Lcom/android/calendar/event/v;->a(Landroid/text/format/Time;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/android/calendar/event/v;->h:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setSelected(Z)V

    .line 124
    iget-object v0, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setSelected(Z)V

    .line 126
    const v0, 0x7f1200cc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/common/extension/EasyTimePicker;

    iput-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    .line 127
    const v0, 0x7f1200cd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/common/extension/EasyTimePicker;

    iput-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    .line 129
    const v0, 0x7f1200c6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/v;->j:Landroid/widget/Button;

    .line 130
    const v0, 0x7f1200c8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/calendar/event/v;->k:Landroid/widget/Button;

    .line 131
    const v0, 0x7f1200c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/v;->l:Landroid/view/View;

    .line 132
    const v0, 0x7f1200c9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/v;->m:Landroid/view/View;

    .line 134
    iget-object v0, p0, Lcom/android/calendar/event/v;->j:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/ae;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/event/ae;-><init>(Lcom/android/calendar/event/v;Lcom/android/calendar/event/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/android/calendar/event/v;->k:Landroid/widget/Button;

    new-instance v1, Lcom/android/calendar/event/ae;

    invoke-direct {v1, p0, v3}, Lcom/android/calendar/event/ae;-><init>(Lcom/android/calendar/event/v;Lcom/android/calendar/event/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v0, p0, Lcom/android/calendar/event/v;->j:Landroid/widget/Button;

    invoke-static {v6}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 138
    iget-object v0, p0, Lcom/android/calendar/event/v;->j:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 139
    iget-object v0, p0, Lcom/android/calendar/event/v;->k:Landroid/widget/Button;

    iget-object v1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 141
    invoke-static {p1, v3}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 143
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 144
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 146
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 147
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, p0}, Lcom/android/calendar/common/extension/EasyTimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 149
    const v0, 0x7f1200c5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    .line 152
    new-instance v1, Lcom/android/calendar/en;

    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-direct {v1, v0}, Lcom/android/calendar/en;-><init>(Landroid/content/Context;)V

    .line 153
    iget-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    new-array v2, v7, [Landroid/text/InputFilter;

    aput-object v1, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 156
    iget-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    new-instance v2, Lcom/android/calendar/event/w;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/w;-><init>(Lcom/android/calendar/event/v;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 168
    const v0, 0x7f1200d0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    .line 169
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 170
    const v0, 0x7f1200ce

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/v;->t:Landroid/view/View;

    .line 171
    iget-object v0, p0, Lcom/android/calendar/event/v;->t:Landroid/view/View;

    new-instance v2, Lcom/android/calendar/event/x;

    invoke-direct {v2, p0}, Lcom/android/calendar/event/x;-><init>(Lcom/android/calendar/event/v;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    const v0, 0x7f1200d3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    .line 180
    iget-object v0, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    new-array v2, v7, [Landroid/text/InputFilter;

    aput-object v1, v2, v6

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 184
    iget-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    new-instance v1, Lcom/android/calendar/event/y;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/y;-><init>(Lcom/android/calendar/event/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 205
    invoke-virtual {p0, v3}, Lcom/android/calendar/event/v;->a(Lcom/android/calendar/as;)V

    .line 206
    return-void

    .line 116
    :cond_0
    invoke-direct {p0, v3, v3}, Lcom/android/calendar/event/v;->a(Landroid/text/format/Time;Landroid/text/format/Time;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/android/calendar/event/v;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    return-object v0
.end method

.method private a(Landroid/text/format/Time;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 552
    new-instance v0, Lcom/android/calendar/event/ac;

    invoke-direct {v0, p0, p1}, Lcom/android/calendar/event/ac;-><init>(Lcom/android/calendar/event/v;Landroid/text/format/Time;)V

    return-object v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 11

    .prologue
    .line 209
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 210
    const/4 v0, 0x0

    .line 259
    :goto_0
    return-object v0

    .line 213
    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    .line 215
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0004

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    if-eqz v1, :cond_9

    .line 217
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ko"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 219
    :cond_1
    const-string v0, "MDY"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 220
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f02a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 241
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/calendar/hj;->a(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 242
    invoke-static {v0, p1, p2}, Lcom/android/calendar/event/ay;->a(Ljava/lang/String;J)J

    move-result-wide v8

    .line 244
    const-string v0, "EasyEditEventView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tzOffet:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millies:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    sub-long v0, p1, v8

    const-wide v2, -0x1f3a565e880L

    cmp-long v0, v0, v2

    if-gez v0, :cond_8

    .line 247
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 248
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x76e

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 249
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    .line 250
    const-string v0, "EasyEditEventView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after min tzOffet:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millies:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_2
    :goto_2
    sub-long v0, p1, v8

    invoke-static {v7, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 221
    :cond_3
    const-string v0, "YMD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 222
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0490

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 226
    :cond_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "th"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 227
    const-string v0, "E"

    invoke-static {v0, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v1, "dd/MM/yy"

    invoke-static {v1, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 229
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 231
    goto/16 :goto_1

    .line 232
    :cond_6
    const-string v2, "MDY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 233
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 234
    :cond_7
    const-string v2, "YMD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 235
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 251
    :cond_8
    sub-long v0, p1, v8

    const-wide v2, 0x1ec4d45f520L

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 252
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 253
    const/4 v1, 0x0

    const/16 v2, 0x3b

    const/16 v3, 0x17

    const/16 v4, 0x1f

    const/16 v5, 0xb

    const/16 v6, 0x7f4

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 254
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide p1

    .line 255
    const-string v0, "EasyEditEventView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "after max tzOffet:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " millies:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/calendar/ey;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    move-object v7, v0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/android/calendar/event/v;J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JJ)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 488
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    check-cast v0, Lcom/android/calendar/event/EditEventActivity;

    invoke-virtual {v0}, Lcom/android/calendar/event/EditEventActivity;->f()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 495
    :goto_0
    return-void

    .line 492
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lcom/android/calendar/event/v;->b(JJ)Landroid/net/Uri;

    move-result-object v3

    .line 493
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/calendar/event/v;->c(JJ)Ljava/lang/String;

    move-result-object v5

    .line 494
    iget-object v0, p0, Lcom/android/calendar/event/v;->d:Lcom/android/calendar/event/ab;

    const/4 v1, 0x0

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/android/calendar/event/ab;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/text/format/Time;Landroid/text/format/Time;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 264
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    .line 265
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    .line 268
    if-nez p1, :cond_1

    .line 270
    iget-object v0, p0, Lcom/android/calendar/event/v;->g:Lcom/android/calendar/event/av;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/calendar/event/av;->a(J)J

    move-result-wide v0

    .line 274
    :goto_0
    iget-object v2, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 276
    if-eqz p2, :cond_0

    invoke-virtual {p2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 278
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 279
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 284
    :goto_1
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 285
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 286
    return-void

    .line 272
    :cond_1
    invoke-virtual {p1, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    goto :goto_0

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, p2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    goto :goto_1
.end method

.method private a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 306
    iget v0, p2, Landroid/text/format/Time;->hour:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 307
    iget v0, p2, Landroid/text/format/Time;->minute:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 308
    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/v;Landroid/widget/TimePicker;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    return-void
.end method

.method static synthetic a(Lcom/android/calendar/event/v;Z)Z
    .locals 0

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/calendar/event/v;->e:Z

    return p1
.end method

.method private static b(JJ)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 498
    sget-object v0, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 499
    invoke-static {v0, p0, p1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 500
    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 501
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/android/calendar/event/v;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic c(Lcom/android/calendar/event/v;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    return-object v0
.end method

.method private c(JJ)Ljava/lang/String;
    .locals 7

    .prologue
    .line 505
    const-string v0, "visible=1 AND deleted!=1"

    .line 506
    iget-object v1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/calendar/hj;->g(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND selfAttendeeStatus!=2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 510
    :cond_0
    iget-object v1, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/android/calendar/hj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 512
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND allDay!=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND begin!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND end!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 517
    iget-object v1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-boolean v1, v1, Lcom/android/calendar/as;->aq:Z

    if-nez v1, :cond_1

    .line 518
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND event_id!="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    :cond_1
    return-object v0
.end method

.method static synthetic d(Lcom/android/calendar/event/v;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->j:Landroid/widget/Button;

    return-object v0
.end method

.method private d()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 395
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 396
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 398
    invoke-static {}, Lcom/android/calendar/dz;->r()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/android/calendar/dz;->s()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 399
    :cond_0
    cmp-long v4, v2, v6

    if-nez v4, :cond_1

    .line 400
    iget-object v2, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v2

    .line 402
    :cond_1
    cmp-long v4, v0, v6

    if-nez v4, :cond_2

    .line 403
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/hj;->e(Landroid/text/format/Time;)J

    move-result-wide v0

    .line 407
    :cond_2
    iget-object v4, p0, Lcom/android/calendar/event/v;->h:Landroid/widget/Button;

    invoke-direct {p0, v2, v3}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 408
    iget-object v2, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 411
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 412
    return-void
.end method

.method static synthetic e(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 455
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 460
    iget-object v1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    if-nez v1, :cond_0

    .line 485
    :goto_0
    return-void

    .line 464
    :cond_0
    if-eqz v0, :cond_1

    .line 465
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 466
    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 467
    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 468
    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 469
    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 470
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 471
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 473
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 474
    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 475
    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 476
    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 477
    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 478
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 479
    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    const-wide/32 v4, 0x5265c00

    add-long/2addr v0, v4

    .line 484
    :goto_1
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/android/calendar/event/v;->a(JJ)V

    goto :goto_0

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 482
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    goto :goto_1
.end method

.method static synthetic f(Lcom/android/calendar/event/v;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 544
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    const v1, 0x7f0f0344

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 545
    return-void
.end method

.method static synthetic g(Lcom/android/calendar/event/v;)Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->l:Landroid/view/View;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 548
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    const v1, 0x7f0f01be

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 549
    return-void
.end method

.method static synthetic h(Lcom/android/calendar/event/v;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->h:Landroid/widget/Button;

    return-object v0
.end method

.method private h()Z
    .locals 8

    .prologue
    const-wide/32 v6, 0x5265c00

    const/high16 v2, 0x60000

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 756
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->o:Ljava/lang/String;

    .line 757
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 758
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v0, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/as;->p:Ljava/lang/String;

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v2}, Lcom/android/calendar/common/extension/EasyTimePicker;->setDescendantFocusability(I)V

    .line 763
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v2}, Lcom/android/calendar/common/extension/EasyTimePicker;->setDescendantFocusability(I)V

    .line 764
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->clearFocus()V

    .line 765
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0}, Lcom/android/calendar/common/extension/EasyTimePicker;->clearFocus()V

    .line 767
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/android/calendar/as;->G:Z

    .line 768
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-boolean v0, v0, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_2

    .line 771
    const-string v0, "UTC"

    iput-object v0, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 772
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 773
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 774
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 775
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 776
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 778
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->hour:I

    .line 779
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->minute:I

    .line 780
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iput v4, v0, Landroid/text/format/Time;->second:I

    .line 781
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 784
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    add-long/2addr v0, v6

    .line 785
    iget-object v2, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-wide v2, v2, Lcom/android/calendar/as;->y:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 787
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-wide v2, v1, Lcom/android/calendar/as;->y:J

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    .line 798
    :goto_0
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/calendar/as;->E:Ljava/lang/String;

    .line 799
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    invoke-static {v0}, Lcom/android/calendar/hj;->a(Lcom/android/calendar/as;)V

    .line 800
    return v5

    .line 789
    :cond_1
    iget-object v2, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iput-wide v0, v2, Lcom/android/calendar/as;->B:J

    goto :goto_0

    .line 792
    :cond_2
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 793
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 794
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->y:J

    .line 795
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/calendar/as;->B:J

    goto :goto_0
.end method

.method static synthetic i(Lcom/android/calendar/event/v;)Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/android/calendar/event/v;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic k(Lcom/android/calendar/event/v;)Lcom/android/calendar/common/extension/EasyTimePicker;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    return-object v0
.end method

.method static synthetic l(Lcom/android/calendar/event/v;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->k:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic m(Lcom/android/calendar/event/v;)Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic n(Lcom/android/calendar/event/v;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/calendar/event/v;->f()V

    return-void
.end method

.method static synthetic o(Lcom/android/calendar/event/v;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/calendar/event/v;->e()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/calendar/as;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 311
    if-nez p1, :cond_0

    .line 392
    :goto_0
    return-void

    .line 314
    :cond_0
    iput-object p1, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    .line 316
    iget-wide v0, p1, Lcom/android/calendar/as;->y:J

    .line 317
    iget-wide v2, p1, Lcom/android/calendar/as;->B:J

    .line 318
    iget-object v4, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 320
    invoke-static {}, Lcom/android/calendar/dz;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 321
    iget-boolean v4, p1, Lcom/android/calendar/as;->G:Z

    if-eqz v4, :cond_1

    const-string v4, "Africa/Casablanca"

    iget-object v5, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 322
    const-string v4, "UTC"

    iput-object v4, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 326
    :cond_1
    iget-object v4, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iget-object v5, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v5, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 327
    iget-object v4, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v4, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 328
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 330
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 331
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 332
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 333
    iget-boolean v0, p1, Lcom/android/calendar/as;->G:Z

    if-eqz v0, :cond_5

    .line 334
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 335
    invoke-virtual {p0, v6}, Lcom/android/calendar/event/v;->a(Z)V

    .line 341
    :goto_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 342
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 343
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 351
    :goto_2
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 352
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 354
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    new-instance v1, Lcom/android/calendar/event/aa;

    invoke-direct {v1, p0}, Lcom/android/calendar/event/aa;-><init>(Lcom/android/calendar/event/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 379
    iget-object v0, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 380
    iget-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/calendar/as;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 382
    iget-object v1, p0, Lcom/android/calendar/event/v;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 385
    :cond_2
    iget-object v0, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 386
    iget-object v0, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/android/calendar/as;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v0, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    .line 388
    iget-object v1, p0, Lcom/android/calendar/event/v;->u:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 391
    :cond_3
    invoke-direct {p0}, Lcom/android/calendar/event/v;->d()V

    goto/16 :goto_0

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 345
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 347
    iget-object v0, p1, Lcom/android/calendar/as;->E:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    .line 348
    iget-object v0, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 349
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->p:Ljava/lang/String;

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    goto :goto_2
.end method

.method protected a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 804
    if-eqz p1, :cond_3

    .line 805
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_1

    .line 806
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 807
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 812
    iget-object v2, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->before(Landroid/text/format/Time;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 813
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 814
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 816
    :cond_0
    iget-object v2, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 817
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 819
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v5}, Lcom/android/calendar/common/extension/EasyTimePicker;->setEnabled(Z)V

    .line 820
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v5}, Lcom/android/calendar/common/extension/EasyTimePicker;->setEnabled(Z)V

    .line 822
    iget-object v0, p0, Lcom/android/calendar/event/v;->c:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 823
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 824
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 841
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/android/calendar/event/v;->e()V

    .line 842
    return-void

    .line 827
    :cond_3
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->hour:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->minute:I

    if-nez v0, :cond_5

    .line 828
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 829
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-static {v0}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 830
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 831
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    iget v1, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/text/format/Time;->hour:I

    .line 834
    :cond_4
    iget-object v0, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 835
    iget-object v2, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 836
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 838
    :cond_5
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v4}, Lcom/android/calendar/common/extension/EasyTimePicker;->setEnabled(Z)V

    .line 839
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {v0, v4}, Lcom/android/calendar/common/extension/EasyTimePicker;->setEnabled(Z)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/android/calendar/event/v;->e:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/android/calendar/event/v;->f:Lcom/android/calendar/as;

    if-nez v0, :cond_0

    .line 750
    const/4 v0, 0x0

    .line 752
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/calendar/event/v;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 845
    invoke-direct {p0}, Lcom/android/calendar/event/v;->d()V

    .line 846
    return-void
.end method

.method public onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 668
    iget-object v0, p0, Lcom/android/calendar/event/v;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_3

    .line 670
    iget-object v2, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    .line 671
    iget-object v3, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    .line 672
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 679
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 681
    iget v0, v3, Landroid/text/format/Time;->hour:I

    iget v1, v2, Landroid/text/format/Time;->hour:I

    sub-int/2addr v0, v1

    .line 682
    iget v1, v3, Landroid/text/format/Time;->minute:I

    iget v5, v2, Landroid/text/format/Time;->minute:I

    sub-int/2addr v1, v5

    .line 684
    iput p2, v2, Landroid/text/format/Time;->hour:I

    .line 685
    iput p3, v2, Landroid/text/format/Time;->minute:I

    .line 686
    iput-boolean v7, v2, Landroid/text/format/Time;->allDay:Z

    .line 687
    invoke-virtual {v4, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 688
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 689
    invoke-static {v2, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/android/calendar/event/v;->v:Landroid/text/format/Time;

    invoke-static {v5, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v5

    if-lez v5, :cond_0

    .line 691
    iget v4, v4, Landroid/text/format/Time;->hour:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v2, Landroid/text/format/Time;->hour:I

    .line 692
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 696
    :cond_0
    iget v4, v2, Landroid/text/format/Time;->hour:I

    add-int/2addr v0, v4

    iput v0, v3, Landroid/text/format/Time;->hour:I

    .line 697
    add-int v0, p3, v1

    iput v0, v3, Landroid/text/format/Time;->minute:I

    .line 698
    iput-boolean v7, v3, Landroid/text/format/Time;->allDay:Z

    .line 700
    invoke-static {v3}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    invoke-virtual {v3, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 705
    :cond_1
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 735
    :cond_2
    :goto_0
    iput-object v2, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    .line 736
    iput-object v3, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    .line 738
    iget-object v2, p0, Lcom/android/calendar/event/v;->v:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 739
    iget-object v2, p0, Lcom/android/calendar/event/v;->w:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 741
    iget-object v2, p0, Lcom/android/calendar/event/v;->i:Landroid/widget/Button;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 742
    iget-object v0, p0, Lcom/android/calendar/event/v;->q:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->n:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 743
    iget-object v0, p0, Lcom/android/calendar/event/v;->r:Lcom/android/calendar/common/extension/EasyTimePicker;

    iget-object v1, p0, Lcom/android/calendar/event/v;->o:Landroid/text/format/Time;

    invoke-direct {p0, v0, v1}, Lcom/android/calendar/event/v;->a(Landroid/widget/TimePicker;Landroid/text/format/Time;)V

    .line 744
    invoke-direct {p0}, Lcom/android/calendar/event/v;->e()V

    .line 746
    :cond_3
    return-void

    .line 708
    :cond_4
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    .line 709
    iput p2, v3, Landroid/text/format/Time;->hour:I

    .line 710
    iput p3, v3, Landroid/text/format/Time;->minute:I

    .line 711
    iput-boolean v7, v3, Landroid/text/format/Time;->allDay:Z

    .line 713
    invoke-virtual {v4, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 714
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 715
    invoke-static {v3, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v5

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/android/calendar/event/v;->w:Landroid/text/format/Time;

    invoke-static {v5, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v5

    if-lez v5, :cond_5

    .line 717
    iget v0, v4, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v3, Landroid/text/format/Time;->hour:I

    .line 718
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 723
    :cond_5
    invoke-static {v2, v3}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v4

    if-lez v4, :cond_2

    .line 724
    invoke-virtual {v3, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 725
    iget v0, v2, Landroid/text/format/Time;->hour:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Landroid/text/format/Time;->hour:I

    .line 726
    invoke-direct {p0}, Lcom/android/calendar/event/v;->g()V

    .line 727
    invoke-static {v3}, Lcom/android/calendar/el;->a(Landroid/text/format/Time;)I

    move-result v0

    if-ne v0, v6, :cond_6

    .line 728
    invoke-virtual {v3, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 731
    :cond_6
    invoke-virtual {v3, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    goto :goto_0
.end method
