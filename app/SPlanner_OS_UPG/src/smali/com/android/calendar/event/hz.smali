.class Lcom/android/calendar/event/hz;
.super Ljava/lang/Object;
.source "EventViewUtils.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/android/calendar/event/hp;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/hp;)V
    .locals 0

    .prologue
    .line 690
    iput-object p1, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 694
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 718
    :goto_0
    :pswitch_0
    return v3

    .line 696
    :pswitch_1
    invoke-static {}, Lcom/android/calendar/event/hm;->g()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 698
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    invoke-virtual {p1}, Landroid/view/View;->callOnClick()Z

    goto :goto_0

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0, v3}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Z)Z

    .line 702
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0}, Lcom/android/calendar/event/hp;->f(Lcom/android/calendar/event/hp;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 703
    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 704
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0, p1}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Landroid/view/View;)V

    goto :goto_0

    .line 710
    :pswitch_2
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-static {v0, v2}, Lcom/android/calendar/event/hp;->a(Lcom/android/calendar/event/hp;Z)Z

    .line 711
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    invoke-virtual {v0}, Lcom/android/calendar/event/id;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    iget-object v0, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    invoke-virtual {v0}, Lcom/android/calendar/event/id;->interrupt()V

    .line 713
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/calendar/event/hp;->b:Lcom/android/calendar/event/id;

    .line 715
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/hz;->a:Lcom/android/calendar/event/hp;

    invoke-virtual {v0}, Lcom/android/calendar/event/hp;->a()V

    goto :goto_0

    .line 694
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
