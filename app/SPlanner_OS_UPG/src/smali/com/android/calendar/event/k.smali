.class Lcom/android/calendar/event/k;
.super Landroid/content/AsyncQueryHandler;
.source "AttendeesView.java"


# instance fields
.field final synthetic a:Lcom/android/calendar/event/AttendeesView;


# direct methods
.method public constructor <init>(Lcom/android/calendar/event/AttendeesView;Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 676
    iput-object p1, p0, Lcom/android/calendar/event/k;->a:Lcom/android/calendar/event/AttendeesView;

    .line 677
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 678
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 682
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    check-cast p2, Lcom/android/calendar/event/aw;

    .line 691
    const/4 v0, -0x1

    :try_start_0
    invoke-interface {p3, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v2, v1

    move v0, v1

    .line 695
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 696
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 698
    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 701
    if-lez v0, :cond_5

    .line 703
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_2
    move v2, v1

    move v1, v0

    move v0, v3

    .line 712
    goto :goto_1

    .line 713
    :cond_2
    if-eqz v0, :cond_3

    .line 715
    if-lez v1, :cond_3

    iget v0, p2, Lcom/android/calendar/event/aw;->d:I

    if-ge v0, p1, :cond_3

    .line 716
    iput p1, p2, Lcom/android/calendar/event/aw;->d:I

    .line 717
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 720
    iget-object v1, p0, Lcom/android/calendar/event/k;->a:Lcom/android/calendar/event/AttendeesView;

    invoke-static {v1}, Lcom/android/calendar/event/AttendeesView;->a(Lcom/android/calendar/event/AttendeesView;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/calendar/event/l;

    invoke-direct {v2, p0, p2}, Lcom/android/calendar/event/l;-><init>(Lcom/android/calendar/event/k;Lcom/android/calendar/event/aw;)V

    invoke-static {v1, p2, v2, v0}, Lcom/android/calendar/bi;->a(Landroid/content/Context;Lcom/android/calendar/event/aw;Ljava/lang/Runnable;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 730
    :cond_3
    if-eqz p3, :cond_0

    .line 731
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 730
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_4

    .line 731
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_2
.end method
