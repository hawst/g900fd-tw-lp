.class Lcom/android/calendar/event/ky;
.super Ljava/lang/Object;
.source "SelectStickerFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/android/calendar/event/kx;


# direct methods
.method constructor <init>(Lcom/android/calendar/event/kx;I)V
    .locals 0

    .prologue
    .line 900
    iput-object p1, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iput p2, p0, Lcom/android/calendar/event/ky;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7

    .prologue
    const/16 v2, 0x42

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 905
    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 906
    invoke-virtual {p1, v6}, Landroid/view/View;->setPressed(Z)V

    move v0, v6

    .line 919
    :goto_0
    return v0

    .line 908
    :cond_0
    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v6, :cond_2

    .line 909
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 910
    iget-object v0, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    invoke-static {v0}, Lcom/android/calendar/event/kx;->a(Lcom/android/calendar/event/kx;)Landroid/widget/GridView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 911
    iget-object v0, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iget-object v0, v0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->h:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v1, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget v3, p0, Lcom/android/calendar/event/ky;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    int-to-long v4, v2

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :goto_1
    move v0, v6

    .line 916
    goto :goto_0

    .line 913
    :cond_1
    iget-object v0, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iget-object v0, v0, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    iget-object v0, v0, Lcom/android/calendar/event/SelectStickerFragment;->i:Landroid/widget/AdapterView$OnItemClickListener;

    iget-object v1, p0, Lcom/android/calendar/event/ky;->b:Lcom/android/calendar/event/kx;

    iget-object v1, v1, Lcom/android/calendar/event/kx;->a:Lcom/android/calendar/event/SelectStickerFragment;

    invoke-static {v1}, Lcom/android/calendar/event/SelectStickerFragment;->b(Lcom/android/calendar/event/SelectStickerFragment;)Landroid/widget/GridView;

    move-result-object v1

    iget v3, p0, Lcom/android/calendar/event/ky;->a:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    int-to-long v4, v2

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_1

    .line 918
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
